/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file builds codes for one-electron operators with GTOs by using the
   recurrence relation compiler.

   2020-06-07, Bin Gao:
   * rewrite based on the developed RecurCompiler

   2018-05-07, Bin Gao:
   * first version
*/

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"
#include "tIntegral/Recurrence/RecurCppTranslator.hpp"
#include "tIntegral/Recurrence/RecurConverterFactory.hpp"
#include "tIntegral/Recurrence/RecurBasisFunction.hpp"
#include "tIntegral/Recurrence/RecurOperator.hpp"
#include "tIntegral/Recurrence/RecurExpression.hpp"
#include "tIntegral/Recurrence/RecurRelation.hpp"
#include "tIntegral/Recurrence/RecurCompiler.hpp"
#include "tIntegral/Utility/make_recur_indices.hpp"

int main()
{
    using namespace tIntegral;

    /* Set the log handler */
    auto logger = get_logger();
    //auto tglue_logger = std::make_shared<tGlueCore::Logger>();
    logger->insert(std::make_shared<tGlueCore::Logger>(), &tGlueCore::Logger::ostream_logger);

    /* Types */
    auto real_type = std::string("RealType");
    auto centre_type = std::string("CentreType");

    /* Set the recurrence relation converter factory
       . Triangle traversal, which takes x...x as the first corner and z...z
         the last corner
       . Using C++ recurrence relation translator */
    auto converter_factory = std::make_shared<RecurConverterFactory>(
        RecurTraversal(RecurDirection::X, RecurDirection::Z),
        std::make_shared<RecurCppTranslator>(real_type)
    );

    /* Set the recurrence relation compiler */
    auto vector_wise = false;
    auto compiler = RecurCompiler(converter_factory, vector_wise);

    /* Create recurrence-relation indices for angular momentums on Hermite
       Gaussians */
    auto hgto_indices = make_recur_indices(
        std::vector<std::pair<OneElecIndex,std::string>>({
            std::make_pair(OneElecIndex::BraAngular, std::string("AngBra")),
            std::make_pair(OneElecIndex::KetAngular, std::string("AngKet"))
        })
    );

    /* Set Hermite Gaussians, positions of exponent indices will not be used in
       recurrence relation code generation so it is OK that they take the same
       values as indices angular momentum numbers */
    auto idx_exponent_bra = std::make_shared<RecurVectorIndex>(
        std::string("ExptBra"), 0, RecurVector(0)
    );
    auto bra_hgto = std::make_shared<RecurGaussianFunction>(
        idx_exponent_bra, hgto_indices[OneElecIndex::BraAngular], std::string("braBasis"), centre_type, real_type
    );
    auto idx_exponent_ket = std::make_shared<RecurVectorIndex>(
        std::string("ExptKet"), 1, RecurVector(0)
    );
    auto ket_hgto = std::make_shared<RecurGaussianFunction>(
        idx_exponent_ket, hgto_indices[OneElecIndex::KetAngular], std::string("ketBasis"), centre_type, real_type
    );

    /* Build codes for Cartesian multipole moments with Hermite Gaussians */
    {
        logger->write(tGlueCore::MessageType::Output,
                      "Build codes for Cartesian multipole moments with Hermite Gaussians");
        /* Create recurrence-relation indices for Cartesian  multipole moments */
        auto oper_indices = make_recur_indices(
            std::vector<std::pair<OneElecIndex,std::string>>({
                std::make_pair(OneElecIndex::MultipoleMoment, std::string("MultMoment")),
                std::make_pair(OneElecIndex::ElecDerivative, std::string("ElecDeriv"))
            }),
            hgto_indices.size()
        );
        auto one_oper = std::make_shared<RecurCartMultMoment>(
            oper_indices[OneElecIndex::MultipoleMoment],
            oper_indices[OneElecIndex::ElecDerivative],
            std::string("oneOper"),
            centre_type,
            real_type
        );
        auto expressions = RecurRelation().get_expression(bra_hgto, one_oper, ket_hgto);
        if (!compiler.build(std::vector<std::shared_ptr<RecurFunction>>({bra_hgto, one_oper, ket_hgto}),
                            expressions,
                            std::string("CartMultMomentHGTOIntegration"),
                            std::vector<std::string>({centre_type, real_type}),
                            std::string("Integration class for Cartesian multipole moments with Hermite Gaussians"),
                            true)) {
            logger->write(tGlueCore::MessageType::Error,
                          "main() failed to build codes for Cartesian multipole moments with HGTOs");
            return -1;
        }
    }

    /* Build codes for unprojected part of effective core potential with Hermite Gaussians */
    {
        logger->write(tGlueCore::MessageType::Output,
                      "Build codes for unprojected part of effective core potential with Hermite Gaussians");
        /* Create recurrence-relation indices for unprojected part of effective core potential */
        auto oper_indices = make_recur_indices(
            std::vector<std::pair<OneElecIndex,std::string>>({
                std::make_pair(OneElecIndex::RadialPower, std::string("RadialPower")),
                std::make_pair(OneElecIndex::BesselBra, std::string("Bessel")),
                std::make_pair(OneElecIndex::OperGeometrical, std::string("OperGeo"))
            }),
            hgto_indices.size()
        );
        auto one_oper = std::make_shared<RecurECPUnprojected>(
            oper_indices[OneElecIndex::RadialPower],
            oper_indices[OneElecIndex::BesselBra],
            oper_indices[OneElecIndex::OperGeometrical],
            std::string("oneOper"),
            centre_type,
            real_type
        );
        auto expressions = RecurRelation().get_expression(bra_hgto, one_oper, ket_hgto);
        if (!compiler.build(std::vector<std::shared_ptr<RecurFunction>>({bra_hgto, one_oper, ket_hgto}),
                            expressions,
                            std::string("ECPUnprojectedHGTOIntegration"),
                            std::vector<std::string>({centre_type, real_type}),
                            std::string("Integration class for unprojected part of ECP with Hermite Gaussians"),
                            true,
                            true)) {
            logger->write(tGlueCore::MessageType::Error,
                          "main() failed to build codes for unprojected part of ECP with HGTOs");
            return -1;
        }
    }

    /* Build codes for projected part of effective core potential with Hermite Gaussians */
    {
        logger->write(tGlueCore::MessageType::Output,
                      "Build codes for projected part of effective core potential with Hermite Gaussians");
        /* Create recurrence-relation indices for projected part of effective core potential */
        auto oper_indices = make_recur_indices(
            std::vector<std::pair<OneElecIndex,std::string>>({
                std::make_pair(OneElecIndex::RadialPower, std::string("RadialPower")),
                std::make_pair(OneElecIndex::BesselBra, std::string("BesselBra")),
                std::make_pair(OneElecIndex::BesselKet, std::string("BesselKet")),
                std::make_pair(OneElecIndex::HarmonicBra, std::string("HarmonicBra")),
                std::make_pair(OneElecIndex::HarmonicKet, std::string("HarmonicKet")),
                std::make_pair(OneElecIndex::OperGeometrical, std::string("OperGeo"))
            }),
            hgto_indices.size()
        );
        auto one_oper = std::make_shared<RecurECPProjected>(
            oper_indices[OneElecIndex::RadialPower],
            oper_indices[OneElecIndex::BesselBra],
            oper_indices[OneElecIndex::BesselKet],
            oper_indices[OneElecIndex::HarmonicBra],
            oper_indices[OneElecIndex::HarmonicKet],
            oper_indices[OneElecIndex::OperGeometrical],
            std::string("oneOper"),
            centre_type,
            real_type
        );
        auto expressions = RecurRelation().get_expression(bra_hgto, one_oper, ket_hgto);
        if (!compiler.build(std::vector<std::shared_ptr<RecurFunction>>({bra_hgto, one_oper, ket_hgto}),
                            expressions,
                            std::string("ECPProjectedHGTOIntegration"),
                            std::vector<std::string>({centre_type, real_type}),
                            std::string("Integration class for projected part of ECP with Hermite Gaussians"),
                            true,
                            true)) {
            logger->write(tGlueCore::MessageType::Error,
                          "main() failed to build codes for projected part of ECP with HGTOs");
            return -1;
        }
    }

    return 0;
}
