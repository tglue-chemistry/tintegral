/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file illustrates how to compute different mathematical functions.

   2022-02-27, Bin Gao:
   * first version
*/

#ifdef TINTEGRAL_CPU_TIME
#include <ctime>
#endif
#include <valarray>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/MathFunction.hpp"

int main()
{
    using namespace tIntegral;

    /* Set the log handler */
    auto logger = get_logger();
    //auto tglue_logger = std::make_shared<tGlueCore::Logger>();
    logger->insert(std::make_shared<tGlueCore::Logger>(), &tGlueCore::Logger::ostream_logger);

    Settings::logger->write(tGlueCore::MessageType::Output, "Which function do you want to compute?");
    Settings::logger->write(tGlueCore::MessageType::Output, "1) Boys function");
    Settings::logger->write(tGlueCore::MessageType::Output, "2) G function");
    Settings::logger->write(tGlueCore::MessageType::Output,
                            "3) Exponentially weighted modified spherical Bessel function of the first kind");
    int fun_type;
    std::cin >> fun_type;

    int max_order = -1;
    while (max_order<0) {
        Settings::logger->write(tGlueCore::MessageType::Output, "Give the maximum order");
        std::cin >> max_order;
    }

    int num_args = -1;
    while (num_args<1) {
        Settings::logger->write(tGlueCore::MessageType::Output, "Number of arguments");
        std::cin >> num_args;
    }
    //std::valarray<double> args(num_args);
    auto args = new (std::nothrow) double[num_args];
    for (int iarg=0; iarg<num_args; ++iarg) {
        Settings::logger->write(tGlueCore::MessageType::Output, "Give ", iarg, "-th argument");
        std::cin >> args[iarg];
    }

    if (fun_type==1) {
#ifdef TINTEGRAL_CPU_TIME
        std::clock_t c_start = std::clock();
#endif
        //auto vals = get_boys(max_order, args);
        auto vals = new (std::nothrow) double[num_args*(max_order+1)];
        get_boys(max_order, num_args, args, vals);
#ifdef TINTEGRAL_CPU_TIME
        std::clock_t c_end = std::clock();
        long double time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
        Settings::logger->write(tGlueCore::MessageType::Output,
                                "CPU time used for computing Boys functions: ", time_elapsed_ms, " ms");
#endif
        //auto begin_val = std::begin(vals);
        auto begin_val = &vals[0];
        auto end_val = begin_val+max_order+1;
        for (int iarg=0; iarg<num_args; ++iarg) {
            Settings::logger->write(tGlueCore::MessageType::Output,
                                    "Maximum order = ",
                                    max_order,
                                    ", argument = ",
                                    args[iarg],
                                    ", Boys functions = ",
                                    tGlueCore::stringify(begin_val, end_val));
            begin_val = end_val;
            end_val += max_order+1;
        }
        char do_error;
        Settings::logger->write(tGlueCore::MessageType::Output,
                                "Compute upper bounds of relative errors of Boys functions [y/n]?");
        std::cin >> do_error;
        if (do_error=='y' || do_error=='Y') {
#ifdef TINTEGRAL_CPU_TIME
            std::clock_t c_start = std::clock();
#endif
            //auto errors = get_boys_errors(max_order, args, vals);
            auto errors = new (std::nothrow) double[num_args*(max_order+1)];
            get_boys_errors(max_order, num_args, args, vals, errors);
#ifdef TINTEGRAL_CPU_TIME
            std::clock_t c_end = std::clock();
            long double time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
            Settings::logger->write(tGlueCore::MessageType::Output,
                                    "CPU time used for computing relative errors: ", time_elapsed_ms, " ms");
#endif
            auto begin_err = &errors[0];
            auto end_err = begin_err+max_order+1;
            for (int iarg=0; iarg<num_args; ++iarg) {
                Settings::logger->write(tGlueCore::MessageType::Output,
                                        "Maximum order = ",
                                        max_order,
                                        ", argument = ",
                                        args[iarg],
                                        ", relative errors <= ",
                                        tGlueCore::stringify(begin_err, end_err));
                begin_val = end_err;
                end_err += max_order+1;
            }
            delete[] errors;
        }
        delete[] vals;
    }
    else if (fun_type==2) {
//#ifdef TINTEGRAL_CPU_TIME
//        std::clock_t c_start = std::clock();
//#endif
//        auto vals = get_gfun(max_order, args);
//#ifdef TINTEGRAL_CPU_TIME
//        std::clock_t c_end = std::clock();
//        long double time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
//        Settings::logger->write(tGlueCore::MessageType::Output, "CPU time used: ", time_elapsed_ms, " ms");
//#endif
//        auto begin_val = std::begin(vals);
//        auto end_val = begin_val+max_order+1;
//        for (int iarg=0; iarg<args.size(); ++iarg) {
//            Settings::logger->write(tGlueCore::MessageType::Output,
//                                    "Maximum order = ",
//                                    max_order,
//                                    ", argument = ",
//                                    args[iarg],
//                                    ", G functions = ",
//                                    tGlueCore::stringify(begin_val, end_val));
//            begin_val = end_val;
//            end_val += max_order+1;
//        }
    }
    delete[] args;

    return 0;
}
