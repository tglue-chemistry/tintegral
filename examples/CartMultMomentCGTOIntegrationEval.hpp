/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates Cartesian multipole moments integrals with Gaussian
   functions.

   2020-06-19, Bin Gao:
   * first version
*/

template<typename CentreType, typename RealType>
void CartMultMomentCGTOIntegration<CentreType,RealType>::eval() noexcept
{
    /* Compute stem:[\pi^{3/2}], may be changed if C++20 become popular */
    const RealType sqrt_pi3 = RealType(5.56832799683170784528);
    /* Get inverse of total exponents stem:[\frac{1}{a_{i}+b_{j}}] */
    auto inv_total_exponents = m_braBasis->inverse_total_exponents(m_ketBasis);
    /* Get pre-exponential factors stem:[\exp(-\frac{a_{i}b_{j}}{a_{i}+b_{j}}R_{\kappa\lambda}^{2})]. */
    auto expt_factors = m_braBasis->get_expt_factors(m_ketBasis);
    /* Comput integrals of s-shell Cartesian GTOs */
    for (auto const& each_node: m_recur_arrays[2].get_input_nodes()) {
#ifdef TINTEGRAL_DEBUG
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "CartMultMomentCGTOIntegration::eval() processes LHS node ",
                                each_node->to_string());
#endif
        auto val_lhs = m_buffer->get_element()+each_node->get_offset();
#if defined(USE_VALARRAY_BUFFER)
        *val_lhs = sqrt_pi3*expt_factors*std::pow(inv_total_exponents, 1.5);
#else
        for (unsigned int ival=0; ival<m_buffer->get_size_element(); ++ival) {
            (*val_lhs)[ival] = sqrt_pi3*expt_factors[ival]*std::pow(inv_total_exponents[ival], 1.5);
        }
#endif
    }
}
