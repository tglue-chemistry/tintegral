/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file illustrates how to build integral codes (without derivatives) for
   Cartesian multipole moments with Cartesian GTOs by using the recurrence
   relation compiler.

   2020-09-04, Bin Gao:
   * first version
*/

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"
#include "tIntegral/Recurrence/RecurCppTranslator.hpp"
#include "tIntegral/Recurrence/RecurConverterFactory.hpp"
#include "tIntegral/Recurrence/RecurBasisFunction.hpp"
#include "tIntegral/Recurrence/RecurOperator.hpp"
#include "tIntegral/Recurrence/RecurExpression.hpp"
#include "tIntegral/Recurrence/RecurCompiler.hpp"
#include "tIntegral/Utility/make_recur_indices.hpp"

int main()
{
    using namespace tIntegral;

    /* Set the log handler */
    auto logger = get_logger();
    //auto tglue_logger = std::make_shared<tGlueCore::Logger>();
    logger->insert(std::make_shared<tGlueCore::Logger>(), &tGlueCore::Logger::ostream_logger);

    /* Types */
    auto real_type = std::string("RealType");
    auto centre_type = std::string("CentreType");

    /* Set the recurrence relation converter factory
       . Triangle traversal, which takes x...x as the first corner and z...z
         the last corner
       . Using C++ recurrence relation translator */
    auto converter_factory = std::make_shared<RecurConverterFactory>(
        RecurTraversal(RecurDirection::X, RecurDirection::Z),
        std::make_shared<RecurCppTranslator>(real_type)
    );

    /* Set the recurrence relation compiler */
    auto vector_wise = false;
    auto compiler = RecurCompiler(converter_factory, vector_wise);

    /* Create recurrence-relation indices for angular momentums on Cartesian
       Gaussians */
    auto cgto_indices = make_recur_indices(
        std::vector<std::pair<OneElecIndex,std::string>>({
            std::make_pair(OneElecIndex::BraAngular, std::string("AngBra")),
            std::make_pair(OneElecIndex::KetAngular, std::string("AngKet"))
        })
    );

    /* Set Cartesian Gaussians, positions of exponent indices will not be used in
       recurrence relation code generation so it is OK that they take the same
       values as indices angular momentum numbers */
    auto idx_exponent_bra = std::make_shared<RecurVectorIndex>(
        std::string("ExptBra"), 0, RecurVector(0)
    );
    auto bra_cgto = std::make_shared<RecurGaussianFunction>(
        idx_exponent_bra, cgto_indices[OneElecIndex::BraAngular], std::string("braBasis"), centre_type, real_type
    );
    auto idx_exponent_ket = std::make_shared<RecurVectorIndex>(
        std::string("ExptKet"), 1, RecurVector(0)
    );
    auto ket_cgto = std::make_shared<RecurGaussianFunction>(
        idx_exponent_ket, cgto_indices[OneElecIndex::KetAngular], std::string("ketBasis"), centre_type, real_type
    );

    /* Build codes for Cartesian multipole moments with Cartesian GTOs */
    {
        logger->write(tGlueCore::MessageType::Output,
                      "Build codes for Cartesian multipole moments with Cartesian GTOs");
        /* Create recurrence-relation indices for Cartesian  multipole moments */
        auto oper_indices = make_recur_indices(
            std::vector<std::pair<OneElecIndex,std::string>>({
                std::make_pair(OneElecIndex::MultipoleMoment, std::string("MultMoment")),
                std::make_pair(OneElecIndex::ElecDerivative, std::string("ElecDeriv"))
            }),
            cgto_indices.size()
        );
        auto one_oper = std::make_shared<RecurCartMultMoment>(
            oper_indices[OneElecIndex::MultipoleMoment],
            oper_indices[OneElecIndex::ElecDerivative],
            std::string("oneOper"),
            centre_type,
            real_type
        );
        /* Indices involved in the integral */
        auto idx_angular_bra = bra_cgto->get_idx_angular();
        auto idx_angular_ket = ket_cgto->get_idx_angular();
        auto idx_moment = one_oper->get_idx_moment();
        /* Increments and decrements of indices */
        auto zero_angular_bra = idx_angular_bra->get_copy();
        zero_angular_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_bra = idx_angular_bra->get_copy();
        dec_angular_bra->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_bra = idx_angular_bra->get_copy();
        dec2_angular_bra->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_bra = idx_angular_bra->get_copy();
        inc_angular_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto dec_angular_ket = idx_angular_ket->get_copy();
        dec_angular_ket->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_ket = idx_angular_ket->get_copy();
        dec2_angular_ket->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto dec_moment = idx_moment->get_copy();
        dec_moment->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        /* Recurrence relations of the operator with Cartesian GTOs */
        std::vector<RecurExpression> expressions;
        expressions.reserve(3);
        /* Assign the recurrence relation of Cartesian multipole moments */
        expressions.push_back(RecurExpression(
            /* Most consecutive index comes last, indices regarding exponents
               do not need to be involved in the recurrence relation code
               generation because they are represented by std::valarray */
            std::vector<std::shared_ptr<RecurIndex>>({idx_moment, idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurTerm>(
                    std::vector<std::shared_ptr<RecurIndex>>({
                        dec_moment,
                        inc_angular_bra
                    })
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurCartesianVar>(
                        std::string("oper_to_bra"),
                        bra_cgto,
                        std::string("from_centre"),
                        std::set<std::shared_ptr<RecurSymbol>>({
                            std::make_shared<RecurCartesianVar>(
                                std::string("oper_origin"),
                                one_oper,
                                std::string("get_origin"),
                                std::set<std::shared_ptr<RecurSymbol>>(),
                                RecurDirection::X
                            )
                        }),
                        RecurDirection::X
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            dec_moment,
                            zero_angular_bra
                        })
                    )
                )
            )
        ));
        /* Assign the recurrence relation of Cartesian GTO on ket centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_angular_ket, idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurCartesianVec>(
                        std::string("ket_to_cc"),
                        ket_cgto,
                        std::string("bra_to_centre_of_charge"),
                        std::set<std::shared_ptr<RecurSymbol>>({bra_cgto}),
                        RecurDirection::X,
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({dec_angular_ket, zero_angular_bra})
                    )
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurNumber>(2),
                        std::make_shared<RecurScalarVec>(
                            std::string("inv_total_expts"),
                            bra_cgto,
                            std::string("inverse_total_exponents"),
                            std::set<std::shared_ptr<RecurSymbol>>({ket_cgto}),
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                        )
                    ),
                    std::make_shared<RecurParentheses>(
                        std::make_shared<RecurAddition>(
                            std::make_shared<RecurMultiplication>(
                                std::make_shared<RecurIdxOrder>(idx_angular_bra),
                                std::make_shared<RecurTerm>(
                                    std::vector<std::shared_ptr<RecurIndex>>({
                                        dec_angular_ket,
                                        dec_angular_bra
                                    })
                                )
                            ),
                            std::make_shared<RecurMultiplication>(
                                std::make_shared<RecurIdxOrder>(idx_angular_ket),
                                std::make_shared<RecurTerm>(
                                    std::vector<std::shared_ptr<RecurIndex>>({
                                        dec2_angular_ket,
                                        zero_angular_bra
                                    })
                                )
                            )
                        )
                    )
                )
            )
        ));
        /* Assign the recurrence relation of Cartesian GTO on bra centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurCartesianVec>(
                        std::string("bra_to_cc"),
                        bra_cgto,
                        std::string("bra_to_centre_of_charge"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket_cgto}),
                        RecurDirection::X,
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(std::vector<std::shared_ptr<RecurIndex>>({dec_angular_bra}))
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurNumber>(2),
                            std::make_shared<RecurIdxOrder>(idx_angular_bra)
                        ),
                        std::make_shared<RecurScalarVec>(
                            std::string("inv_total_expts"),
                            bra_cgto,
                            std::string("inverse_total_exponents"),
                            std::set<std::shared_ptr<RecurSymbol>>({ket_cgto}),
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                        )
                    ),
                    std::make_shared<RecurTerm>(std::vector<std::shared_ptr<RecurIndex>>({dec2_angular_bra}))
                )
            )
        ));
        /* Build integral codes */
        if (!compiler.build(std::vector<std::shared_ptr<RecurFunction>>({bra_cgto, one_oper, ket_cgto}),
                            expressions,
                            std::string("CartMultMomentCGTOIntegration"),
                            std::vector<std::string>({centre_type, real_type}),
                            std::string("Integration class for Cartesian multipole moments with Cartesian GTOs"),
                            true)) {
            logger->write(tGlueCore::MessageType::Error,
                          "main() failed to build codes for Cartesian multipole moments with Cartesian GTOs");
            return -1;
        }
    }

    return 0;
}
