/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file illustrates how to compute integrals of one-electron operators
   with Hermite Gaussians.

   2020-07-01, Bin Gao:
   * first version
*/

#include <array>
#include <ctime>
#include <iterator>
#include <memory>
#include <utility>
#include <valarray>
#include <vector>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"
#include "tBasisSet/GaussianFunction.hpp"
#include "tMolecular/Atom.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurNode.hpp"
#include "tIntegral/Recurrence/RecurArray.hpp"
#include "tIntegral/Utility/make_recur_indices.hpp"
#include "tIntegral/Utility/make_recur_nodes.hpp"

#include "tIntegral/CartMultMoment.hpp"
#include "tIntegral/CartMultMomentHGTOIntegration.hpp"
//#include "tIntegral/ECPUnprojected.hpp"
//#include "tIntegral/ECPUnprojectedHGTOIntegration.hpp"

int main()
{
    using namespace tIntegral;

    /* Set the log handler */
    auto logger = get_logger();
    //auto tglue_logger = std::make_shared<tGlueCore::Logger>();
    logger->insert(std::make_shared<tGlueCore::Logger>(), &tGlueCore::Logger::ostream_logger);

    /* Types */
    using real_type = double;
    using centre_type = tMolecular::Atom<real_type>;

    /* We use hydrogen fluoride molecule */
    auto hydrogen = std::make_shared<centre_type>(0, 1, 1.0, std::array<real_type,3>({0,0,0}));
    auto fluorine = std::make_shared<centre_type>(1, 9, 19.0, std::array<real_type,3>({1.05,1.05,1.05}));

    /* Create Gaussian functions on hydrogen and fluorine */
    auto bra_gaussian = std::make_shared<tBasisSet::GaussianFunction<centre_type,real_type>>(
        hydrogen,
        std::valarray<real_type>({
            //100.00,
            // 10.00,
            //  1.00,
            //  0.10,
              0.01
        })
    );
    auto ket_gaussian = std::make_shared<tBasisSet::GaussianFunction<centre_type,real_type>>(
        fluorine,
        std::valarray<real_type>({
            //200.00,
            // 20.00,
            //  2.00,
            //  0.20,
              0.02
        })
    );

    /* Create Cartesian multipole moment operator */
    auto one_oper = std::make_shared<CartMultMoment<centre_type,real_type>>(
        std::make_shared<centre_type>(-1, 0, 0.0, std::array<real_type,3>({0,0,0})),
        1.0,
        3,
        0
    );

    /* Create recurrence-relation indices for angular momentums on Hermite
       Gaussians and multipole moments */
    auto one_indices = std::vector<std::pair<OneElecIndex,std::string>>({
        std::make_pair(OneElecIndex::BraAngular, std::string("AngBra")),
        std::make_pair(OneElecIndex::KetAngular, std::string("AngKet")),
        std::make_pair(OneElecIndex::MultipoleMoment, std::string("MultMoment")),
        std::make_pair(OneElecIndex::ElecDerivative, std::string("ElecDeriv"))
    });
    unsigned int output_idx = 2;
    std::vector<std::shared_ptr<RecurIndex>> all_indices;
    for (auto& each_index: make_recur_indices(one_indices)) {
        all_indices.push_back(each_index.second);
    }

    /* Create buffer for storing intermediate and final integrals */
    auto buffer = std::make_shared<RecurBuffer<real_type>>(0, bra_gaussian->size()*ket_gaussian->size());

    /* Create output nodes for Hertmite Gaussians */
    unsigned int bra_angular = 7;
    unsigned int ket_angular = 7;
    auto output_nodes = make_recur_nodes(
        all_indices,
        output_idx,
        //std::distance(all_indices.begin(), all_indices.find(OneElecIndex::MultipoleMoment)),
        //FIXME: sort??
        std::vector<std::vector<unsigned int>>({
            std::vector<unsigned int>({
                bra_angular, ket_angular, one_oper->get_multipole_order(), one_oper->get_electronic_order()
            })
        }),
        buffer
    );

    /* Create integration */
    auto hgto_integration = CartMultMomentHGTOIntegration<centre_type,real_type>(
        bra_gaussian, one_oper, ket_gaussian
    );
    Settings::logger->write(tGlueCore::MessageType::Output,
                            "OneElecHGTO calls top-down procedure for Cartesian multipole moment operator");
    /* CPU time before calling the top-down procedure */
    auto cpu_start = std::clock();
    if (hgto_integration.top_down(std::move(output_nodes), all_indices, buffer)) {
        /* CPU time after calling the top-down procedure */
        auto cpu_end = std::clock();
        Settings::logger->write(tGlueCore::MessageType::Output,
                                "OneElecHGTO uses CPU time (ms) ",
                                1000.0*(cpu_end-cpu_start)/CLOCKS_PER_SEC,
                                " for the top-down procedure of Cartesian multipole moment operator with HGTOs");
        if (buffer->allocate()) {
            Settings::logger->write(tGlueCore::MessageType::Output,
                                    "OneElecHGTO calls bottom-up procedure for Cartesian multipole moment operator");
            if (!hgto_integration.bottom_up()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "OneElecHGTO failed to perform bottom-up procedure");
                return -1;
            }
            /* CPU time after calling the bottom-up procedure */
            auto cpu_end = std::clock();
            Settings::logger->write(tGlueCore::MessageType::Output,
                                    "OneElecHGTO uses CPU time (ms) ",
                                    1000.0*(cpu_end-cpu_start)/CLOCKS_PER_SEC,
                                    " for the integration of Cartesian multipole moment operator with HGTOs");
            //auto results = buffer->get_element();
            ///* Compute how many results */
            //auto size_results = all_indices[0]->get_num_components(bra_angular)
            //                  * all_indices[1]->get_num_components(ket_angular)
            //                  * all_indices[2]->get_num_components(one_oper->get_multipole_order())
            //                  * all_indices[3]->get_num_components(one_oper->get_electronic_order());
            //Settings::logger->write(tGlueCore::MessageType::Output,
            //                       "OneElecHGTO get ",
            //                       size_results,
            //                       " results for Cartesian multipole moment operator");
            //for (unsigned int each_result=0; each_result<size_results; ++each_result,++results) {
            //    Settings::logger->write(tGlueCore::MessageType::Output,
            //                           "OneElecHGTO get ",
            //                           each_result,
            //                           "-th result for Cartesian multipole moment operator ",
            //                           tGlueCore::stringify(*results, *results+buffer->get_size_element()));
            //}
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "OneElecHGTO failed to allocate buffer");
            return -1;
        }
    }
    else {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "OneElecHGTO failed to perform top-down procedure");
        return -1;
    }
    return 0;
}
