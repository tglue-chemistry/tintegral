/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of the implementation of the abstract
   one-electron property.

   2014-06-22, Bin Gao:
   * first version
*/

#if !defined(ONE_PROP_IMPL_H)
#define ONE_PROP_IMPL_H

/* geometric derivatives */
#include "derivatives/gen1int_geometric.h"
/* magnetic derivatives */
#include "derivatives/gen1int_magnetic.h"
/* derivatives w.r.t. total rotational angular momentum */
#include "derivatives/gen1int_rotational.h"
/* relativistic corrections */
#include "derivatives/gen1int_relativistic.h"

/* abstract one-electron property */
#include "gen1int_one_prop.h"

/* contracted Gaussian type orbitals (GTOs) */
#include "basis/gen1int_contr_gto.h"

/* abstract one-electron property */
typedef struct _p_OneProp _p_OneProp;

/* specific functions related to different one-electron properties */
typedef struct {
    /* # magnetic derivatives */
    GErrorCode (*onepropmagsetorder)(OneProp*,const GInt,const GInt,const GInt);
    GErrorCode (*onepropmaglpfcreate)(OneProp*,const GReal[],const GReal[]);
    GErrorCode (*onepropmaglpfsetgiao)(OneProp*);
    GErrorCode (*onepropmaglpfsetgiop)(OneProp*);
    /* # derivatives w.r.t. total rotational angular momentum */
    GErrorCode (*oneproprotsetorder)(OneProp*,const GInt,const GInt,const GInt);
    GErrorCode (*oneproprotlpfcreate)(OneProp*,const GReal[],const GReal[],const GReal[]);
    GErrorCode (*oneproprotlpfsetciao)(OneProp*);
    GErrorCode (*oneproprotlpfsetciop)(OneProp*);
    /* # scalar relativistic (SR) and spin-orbit (SO) corrections */
    GErrorCode (*onepropsetsrc)(OneProp*);
    GErrorCode (*onepropsetsoc)(OneProp*);
    /* # writes the context of the one-electron property */
    GErrorCode (*onepropwrite)(const OneProp,FILE*);
    /* # calculates the integral of the one-electron property */
    GErrorCode (*onepropgetcontrintegral)(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GBool,
                                          GReal*);
    /* # calculates the integrand of the one-electron property at given points */
    GErrorCode (*onepropgetcontrintegrand)(const OneProp,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GInt,
                                           const GReal[],
                                           const ContrGTO*,
                                           const GInt,
                                           const GReal[],
                                           const ContrGTO*,
                                           const GInt,
                                           const GReal*,
                                           const GBool,
                                           GReal*);
#if defined(GEN1INT_AO_SHELL)
    /* # calculates the integral matrices of the one-electron property */
    GErrorCode (*onepropgetshellintegral)(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const AOShell*,
                                          const AOShell*,
                                          const GInt*,
                                          const GInt*,
                                          const GInt,
                                          const GInt,
                                          const GBool,
                                          QMat*[]);
    /* # calculates the integrand matrices of the one-electron property at given points */
    GErrorCode (*onepropgetshellintegrand)(const OneProp,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const AOShell*,
                                           const AOShell*,
                                           const GInt,
                                           const GReal*,
                                           const GInt*,
                                           const GInt*,
                                           const GInt,
                                           const GInt,
                                           const GBool,
                                           QMat*[]);
#endif
    /* # duplicates the context of the one-electron property */
    GErrorCode (*onepropduplicate)(const OneProp,OneProp*);
    /* # destroys the context of the one-electron property */
    GErrorCode (*onepropdestroy)(OneProp*);
} OnePropFun;

/* linked list structure of one-electron property */
struct _p_OneProp {
    GVoid *data;                   /* implementation-specific data of the one-electron property */
    /* the following members should in general hidden from the individual
       implementation-specific function, which if necessary can access by
       the functions defined in this header file */
    OnePropFun *one_prop_fun;      /* implementation-specific functions of the one-electron property */
    GBool assembled;               /* indicates if the one-electron property is assembled or not */
    GDataType data_type;           /* data type of the one-electron property */
    GSymType sym_type;             /* symmetry type of the one-electron property */
    GInt num_idx;                  /* number of indices */
    GInt *size_idx;                /* size of each index */
    struct _p_OneProp *next_node;  /* pointer to the next node */
};

/* the following functions should in general not to be called by users */
/* (1) functions related to the implementation-specific functions of a node */
extern GErrorCode OnePropFunAssemble(OneProp);
extern GErrorCode OnePropFunDuplicate(const OneProp,OneProp);
extern GErrorCode OnePropFunDestroy(OneProp);
/* (2) functions related to the indices of a node */
extern GErrorCode OnePropIdxCreate(OneProp);
extern GErrorCode OnePropIdxSetDataType(OneProp,const GDataType);
extern GErrorCode OnePropIdxSetSOC(OneProp);
extern GErrorCode OnePropIdxSetElecDeriv(OneProp,const GInt);
extern GErrorCode OnePropIdxSetCartMM(OneProp,const GInt);
extern GErrorCode OnePropIdxSetMagDeriv(OneProp,const GInt,const GInt,const GInt);
extern GErrorCode OnePropIdxSetRotDeriv(OneProp,const GInt,const GInt,const GInt);
extern GErrorCode OnePropIdxSetGeoOper(OneProp,const GInt,const GInt);
extern GErrorCode OnePropIdxIsComplex(const OneProp,GBool*);
extern GErrorCode OnePropIdxIsSOC(const OneProp,GBool*);
extern GErrorCode OnePropIdxGetSize(const OneProp,GInt*);
/* (3) functions related to the context of a node */
extern GErrorCode OnePropNodeCreateHEAD(OneProp*);
extern GErrorCode OnePropNodeDuplicate(const OneProp, OneProp);
extern GErrorCode OnePropNodeDestroy(OneProp);
/* (4) functions related to the whole linked list */
extern GErrorCode OnePropListInsertAfterHEAD(OneProp,OneProp);

#endif
