/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of Cartesian multipole moments.

   2014-06-25, Bin Gao:
   * first version
*/

#if !defined(CARTMM_IMPL_H)
#define CARTMM_IMPL_H

/* implementation of the abstract one-electron property */
#include "impls/one_prop_impl.h"

/* context of Cartesian multipole moments */
typedef struct {
    /* magnetic derivatives */
    GInt order_mag;         /* order of magnetic derivatives */
    GInt order_mag_bra;     /* order of partial magnetic derivatives on the bra center */
    GInt order_mag_ket;     /* order of partial magnetic derivatives on the ket center */
    MagLPF *mag_LPF;        /* London phase factor for magnetic derivatives */
    /* derivatives w.r.t. total rotational angular momentum (TRAM) */
    GInt order_rot;         /* order of TRAM derivatives */
    GInt order_rot_bra;     /* order of partial TRAM derivatives on the bra center */
    GInt order_rot_ket;     /* order of partial TRAM derivatives on the ket center */
    RotLPF *rot_LPF;        /* London phase factor for TRAM derivatives */
    /* relativistic corrections */
    RCType RC_type;         /* type of relativistic corrections */
    DCDType DCD_type;       /* type of the differentiated charge distributions */
    /* function a*\partial_{D}^{L}r_{D}^{m}\partial_{r}^{n} */
    GReal scal_const;       /* scale constant, a */
    GInt idx_diporg;        /* index of the dipole origin, D */
    GReal coord_diporg[3];  /* coordinates of the dipole origin, R_{D} */
    GInt order_geo_diporg;  /* order of geometric derivatives on the dipole origin, L */
    GInt order_cart_mm;     /* order of Cartesian multipole moments, m */
    GInt order_elec_deriv;  /* order of electronic derivatives, n */
} CartMM;

/* implementation-specific functions of the Cartesian multipole moments */
/* # magnetic derivatives */
extern GErrorCode CartMMMagSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode CartMMMagLPFCreate(OneProp*,const GReal[],const GReal[]);
extern GErrorCode CartMMMagLPFSetGIAO(OneProp*);
extern GErrorCode CartMMMagLPFSetGIOP(OneProp*);
/* # derivatives w.r.t. total rotational angular momentum */
extern GErrorCode CartMMRotSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode CartMMRotLPFCreate(OneProp*,const GReal[],const GReal[],const GReal[]);
extern GErrorCode CartMMRotLPFSetCIAO(OneProp*);
extern GErrorCode CartMMRotLPFSetCIOP(OneProp*);
/* # scalar relativistic (SR) and spin-orbit (SO) corrections */
extern GErrorCode CartMMSetSRC(OneProp*);
extern GErrorCode CartMMSetSOC(OneProp*);
/* # writes the context of the one-electron property */
extern GErrorCode CartMMWrite(const OneProp,FILE*);
/* # calculates the integral of the one-electron property */
extern GErrorCode CartMMGetContrIntegral(const OneProp,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GInt,
                                         const GReal[],
                                         const ContrGTO*,
                                         const GInt,
                                         const GReal[],
                                         const ContrGTO*,
                                         const GBool,
                                         GReal*);
/* # calculates the integrand of the one-electron property at given points */
extern GErrorCode CartMMGetContrIntegrand(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal*,
                                          const GBool,
                                          GReal*);
#if defined(GEN1INT_AO_SHELL)
/* # calculates the integral matrices of the one-electron property */
extern GErrorCode CartMMGetShellIntegral(const OneProp,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const AOShell*,
                                         const AOShell*,
                                         const GInt*,
                                         const GInt*,
                                         const GInt,
                                         const GInt,
                                         const GBool,
                                         QMat*[]);
/* # calculates the integrand matrices of the one-electron property at given points */
extern GErrorCode CartMMGetShellIntegrand(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const AOShell*,
                                          const AOShell*,
                                          const GInt,
                                          const GReal*,
                                          const GInt*,
                                          const GInt*,
                                          const GInt,
                                          const GInt,
                                          const GBool,
                                          QMat*[]);
#endif
/* # duplicates the context of the one-electron property */
extern GErrorCode CartMMDuplicate(const OneProp,OneProp*);
/* # destroys the context of the one-electron property */
extern GErrorCode CartMMDestroy(OneProp*);

#endif
