/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of nuclear attraction potential.

   2014-06-23, Bin Gao:
   * first version
*/

#if !defined(NUCPOT_IMPL_H)
#define NUCPOT_IMPL_H

/* implementation of the abstract one-electron property */
#include "impls/one_prop_impl.h"

/* context of nuclear attraction potential */
typedef struct {
    /* magnetic derivatives */
    GInt order_mag;         /* order of magnetic derivatives */
    GInt order_mag_bra;     /* order of partial magnetic derivatives on the bra center */
    GInt order_mag_ket;     /* order of partial magnetic derivatives on the ket center */
    MagLPF *mag_LPF;        /* London phase factor for magnetic derivatives */
    /* derivatives w.r.t. total rotational angular momentum (TRAM) */
    GInt order_rot;         /* order of TRAM derivatives */
    GInt order_rot_bra;     /* order of partial TRAM derivatives on the bra center */
    GInt order_rot_ket;     /* order of partial TRAM derivatives on the ket center */
    RotLPF *rot_LPF;        /* London phase factor for TRAM derivatives */
    /* relativistic corrections */
    RCType RC_type;         /* type of relativistic corrections */
    DCDType DCD_type;       /* type of the differentiated charge distributions */
    /* function \partial_{D}^{L_{D}}r_{D}^{m}
       \sum_{C}^{N_{C}}\partial_{C}^{L_{C}}(Z_{C}/r_{C})\partial_{r}^{n} */
    GInt idx_diporg;        /* index of the dipole origin, D */
    GReal coord_diporg[3];  /* coordinates of the dipole origin, R_{D} */
    GInt order_geo_diporg;  /* order of geometric derivatives on the dipole origin, L_{D} */
    GInt order_cart_mm;     /* order of Cartesian multipole moments, m */
    GInt num_nuclei;        /* number of nuclei, N_{C} */
    GInt *idx_nuclei;       /* indices of the nuclei */
    GReal *charge_nuclei;   /* charges of the nuclei, Z_{C} */
    GReal *coord_nuclei;    /* coordinates of the nuclei, R_{C} */
    GInt order_geo_nuc;     /* order of geometric derivatives with respect to the nuclei, L_{C} */
    GInt order_elec_deriv;  /* order of electronic derivatives, n */
} NucPot;

/* implementation-specific functions of the nuclear attraction potential */
/* # magnetic derivatives */
extern GErrorCode NucPotMagSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode NucPotMagLPFCreate(OneProp*,const GReal[],const GReal[]);
extern GErrorCode NucPotMagLPFSetGIAO(OneProp*);
extern GErrorCode NucPotMagLPFSetGIOP(OneProp*);
/* # derivatives w.r.t. total rotational angular momentum */
extern GErrorCode NucPotRotSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode NucPotRotLPFCreate(OneProp*,const GReal[],const GReal[],const GReal[]);
extern GErrorCode NucPotRotLPFSetCIAO(OneProp*);
extern GErrorCode NucPotRotLPFSetCIOP(OneProp*);
/* # scalar relativistic (SR) and spin-orbit (SO) corrections */
extern GErrorCode NucPotSetSRC(OneProp*);
extern GErrorCode NucPotSetSOC(OneProp*);
/* # writes the context of the one-electron property */
extern GErrorCode NucPotWrite(const OneProp,FILE*);
/* # calculates the integral of the one-electron property */
extern GErrorCode NucPotGetContrIntegral(const OneProp,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GInt,
                                         const GReal[],
                                         const ContrGTO*,
                                         const GInt,
                                         const GReal[],
                                         const ContrGTO*,
                                         const GBool,
                                         GReal*);
/* # calculates the integrand of the one-electron property at given points */
extern GErrorCode NucPotGetContrIntegrand(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal*,
                                          const GBool,
                                          GReal*);
#if defined(GEN1INT_AO_SHELL)
/* # calculates the integral matrices of the one-electron property */
extern GErrorCode NucPotGetShellIntegral(const OneProp,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const GeoDeriv*,
                                         const AOShell*,
                                         const AOShell*,
                                         const GInt*,
                                         const GInt*,
                                         const GInt,
                                         const GInt,
                                         const GBool,
                                         QMat*[]);
/* # calculates the integrand matrices of the one-electron property at given points */
extern GErrorCode NucPotGetShellIntegrand(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const AOShell*,
                                          const AOShell*,
                                          const GInt,
                                          const GReal*,
                                          const GInt*,
                                          const GInt*,
                                          const GInt,
                                          const GInt,
                                          const GBool,
                                          QMat*[]);
#endif
/* # duplicates the context of the one-electron property */
extern GErrorCode NucPotDuplicate(const OneProp,OneProp*);
/* # destroys the context of the one-electron property */
extern GErrorCode NucPotDestroy(OneProp*);

#endif
