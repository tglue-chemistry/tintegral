/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of the abstract two-electron property.

   2014-06-26, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_TWO_PROP_H)
#define GEN1INT_TWO_PROP_H

/* geometric derivatives */
#include "derivatives/gen1int_geometric.h"
/* contracted Gaussian type orbitals (GTOs) */
#include "basis/gen1int_contr_gto.h"

#if defined(GEN1INT_AO_SHELL)
/* QMatrix library */
#include "qmatrix.h"
/* atomic orbital (AO) shell */
#include "basis/gen1int_ao_shell.h"
#endif

/* abstract two-electron property, inspired by the PETSc library (http://www.mcs.anl.gov/petsc/) */
typedef struct _p_TwoProp* TwoProp;

/* different two-electron operators
   COUL: electron-electron Coulomb interation
   EXCH: electron-electron exchange interaction
 */

#endif
