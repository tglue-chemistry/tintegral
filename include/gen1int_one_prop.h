/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of the abstract one-electron property.

   2014-06-21, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_ONE_PROP_H)
#define GEN1INT_ONE_PROP_H

/* geometric derivatives */
#include "derivatives/gen1int_geometric.h"
/* contracted Gaussian type orbitals (GTOs) */
#include "basis/gen1int_contr_gto.h"

#if defined(GEN1INT_AO_SHELL)
/* QMatrix library */
#include "qmatrix.h"
/* atomic orbital (AO) shell */
#include "basis/gen1int_ao_shell.h"
#endif

/* abstract one-electron property, inspired by the PETSc library (http://www.mcs.anl.gov/petsc/) */
typedef struct _p_OneProp* OneProp;

/* (1) functions not to touch the implementation-specific data of the one-electron property */
/* # assembles the one-electron property and checks its validity, should be called
     before any function QnePropGet...(), otherwise the results might be incorrect */
extern GErrorCode OnePropAssemble(OneProp*);
/* # gets the data and symmetry types of the one-electron property */
extern GErrorCode OnePropGetDataType(const OneProp,GDataType*);
extern GErrorCode OnePropGetSymType(const OneProp,GSymType*);
/* # gets the rank, indices, shape and size of the one-electron property */
extern GErrorCode OnePropGetRank(const OneProp,GInt*);
extern GErrorCode OnePropGetIndices(const OneProp,const GInt,GIdxType*);
extern GErrorCode OnePropGetShape(const OneProp,const GInt,GInt*);
extern GErrorCode OnePropGetSize(const OneProp,GInt*);
extern GErrorCode OnePropIsSOC(const OneProp,GBool*);

/* (2) functions that need to touch the implementation-specific data of the one-electron property */
/* # magnetic derivatives */
extern GErrorCode OnePropMagSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode OnePropMagLPFCreate(OneProp*,const GReal[],const GReal[]);
extern GErrorCode OnePropMagLPFSetGIAO(OneProp*);
extern GErrorCode OnePropMagLPFSetGIOP(OneProp*);
/*extern GErrorCode OnePropMagSetZeroFields(OneProp*,const GBool);*/
/* # derivatives w.r.t. total rotational angular momentum */
extern GErrorCode OnePropRotSetOrder(OneProp*,const GInt,const GInt,const GInt);
extern GErrorCode OnePropRotLPFCreate(OneProp*,const GReal[],const GReal[],const GReal[]);
extern GErrorCode OnePropRotLPFSetCIAO(OneProp*);
extern GErrorCode OnePropRotLPFSetCIOP(OneProp*);
/*extern GErrorCode OnePropRotSetZeroFields(OneProp*,const GBool);*/
/* # scalar relativistic (SR) and spin-orbit (SO) corrections */
extern GErrorCode OnePropSetSRC(OneProp*);
extern GErrorCode OnePropSetSOC(OneProp*);
/* # writes the context of the one-electron property */
extern GErrorCode OnePropWrite(const OneProp,const GChar*);
/* # calculates the integral of the one-electron property */
extern GErrorCode OnePropGetContrIntegral(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GInt,
                                          const GReal[],
                                          const ContrGTO*,
                                          const GBool,
                                          const GInt,
                                          GReal*);
/* # calculates the integrand of the one-electron property at given points */
extern GErrorCode OnePropGetContrIntegrand(const OneProp,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GInt,
                                           const GReal[],
                                           const ContrGTO*,
                                           const GInt,
                                           const GReal[],
                                           const ContrGTO*,
                                           const GInt,
                                           const GReal*,
                                           const GBool,
                                           const GInt,
                                           GReal*);
#if defined(GEN1INT_AO_SHELL)
/* # calculates the integral matrices of the one-electron property */
extern GErrorCode OnePropGetShellIntegral(const OneProp,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const GeoDeriv*,
                                          const AOShell*,
                                          const AOShell*,
                                          const GInt,
                                          const GInt*,
                                          const GInt*,
                                          const GInt,
                                          const GInt,
                                          const GBool,
                                          const GInt,
                                          QMat*[]);
/* # calculates the integrand matrices of the one-electron property at given points */
extern GErrorCode OnePropGetShellIntegrand(const OneProp,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const GeoDeriv*,
                                           const AOShell*,
                                           const AOShell*,
                                           const GInt,
                                           const GReal*,
                                           const GInt,
                                           const GInt*,
                                           const GInt*,
                                           const GInt,
                                           const GInt,
                                           const GBool,
                                           const GInt,
                                           QMat*[]);
#endif
/* # duplicates the context of the one-electron property */
extern GErrorCode OnePropDuplicate(const OneProp,OneProp*);
/* # destroys the context of the one-electron property,
     should be called at the end */
extern GErrorCode OnePropDestroy(OneProp*);
/* (2.1) different implemented basic one-electron properties,
         should be called first and only once */
/* # Cartesian multipole moments */
extern GErrorCode OnePropCartMMCreate(OneProp*,
                                      const GReal,
                                      const GInt,
                                      const GReal[],
                                      const GInt,
                                      const GInt,
                                      const GInt);
/* # Dirac delta function */
/* extern GErrorCode OnePropDeltaCreate(); */
/* # diamagnetic spin-orbit coupling */
/* extern GErrorCode OnePropDSOCreate(); */
/* # effective core potential */
/* extern GErrorCode OnePropECPCreate(); */
/* # Gaussian charge potential */
/* extern GErrorCode OnePropGauPotCreate(); */
/* # inverse square distance potential */
/* extern GErrorCode OnePropISDPotCreate(); */
/* # model core potential Version 1 */
/* extern GErrorCode OnePropMCP1Create(); */
/* # nuclear attraction potential */
extern GErrorCode OnePropNucPotCreate(OneProp*,
                                      const GInt,
                                      const GReal[],
                                      const GInt,
                                      const GInt,
                                      const GInt,
                                      const GInt*,
                                      const GReal*,
                                      const GReal*,
                                      const GInt,
                                      const GInt,
                                      const GBool);
/* (2.2) different implemented advanced one-electron properties,
         together with the functions in (2.1) should be called
         first and only once */
/* # overlap integrals */
extern GErrorCode OnePropOverlapCreate(OneProp*,const GInt,const GReal[]);

#endif
