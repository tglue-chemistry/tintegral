/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of tIntegral library.

   2019-04-06, Bin Gao:
   * first version
*/

#pragma once

#include "tIntegral/BasicTypes.hpp"

#include "tIntegral/OneElec/OneElecOper.hpp"
#include "tIntegral/OneElec/OneElecEngine.hpp"
#include "tIntegral/OneElec/OneElecIntegrator.hpp"
#include "tIntegral/OneElec/OneElecIntegration.hpp"
#include "tIntegral/OneElec/OneElecGTOIntegration.hpp"
#include "tIntegral/OneElec/CartMultMoment.hpp"
#include "tIntegral/OneElec/NucAttractPotential.hpp"

#include "tIntegral/OneElec/OneElecGTOCompiler.hpp"
