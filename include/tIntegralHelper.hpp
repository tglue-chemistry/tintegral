/* tIntegral: not only an integral computation library
   Copyright 2017 Bin Gao

   tIntegral is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   tIntegral is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with tIntegral. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of tIntegral helper classes.

   2017-06-26, Bin Gao:
   * first version
*/

#if !defined(TINTEGRAL_HELPER_HPP)
#define TINTEGRAL_HELPER_HPP

const tGlueFunctionHelper _tMolConstructorHelper(
    "tMol", "()", "Constructor.",
    3,
    "int", "numAtoms", "Number of atoms.",
    "tReal*", "chargeAtoms", "Charges of atoms.",
    "tReal*", "coordAtoms", "Coordinates of atoms"
);

const tGlueFunctionHelper _tMolPrintHelper(
    "void", "print", "Print the molecular system.",
    1,
    "int", "ioUnit", "Unit of I/O."
);

const tGlueClassHelper _tMolHelper(
    "tMol", "Molecular system.",
    2, &_tMolConstructorHelper, &_tMolPrintHelper
);

const tGlueFunctionHelper _tRecurRelationHelper(
    "tReal*", "tRecurRelation", "Perform recurrence relations.",
    2,
    "int", "numRecur", "Number of recurrence relations.",
    "tReal*", "coeff", "Cofficients"
);

const tGlueVariableHelper _PiHelper(
  "tReal", "Pi", "Mathematical pi."
);

const tGlueNamespaceHelper _tIntegralHelper(
  "tIntegral", "an integral-computatoin and code-generation library.",
  0,
  1, &_PiHelper,
  1, &_tRecurRelationHelper,
  1, &_tMolHelper
);

#endif
