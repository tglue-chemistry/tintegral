/* tIntegral: not only an integral computation library
   Copyright 2018 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron STO integrators.

   2018-05-05, Bin Gao:
   * first version
*/

#if !defined(T_ONE_INTEGRATOR_HPP)
#define T_ONE_INTEGRATOR_HPP

#include "RecurIndex.hpp"
#include "RecurBuffer.hpp"
//#include "RecurRelation.hpp"

namespace tIntegral
{
    /* Abstract template-class for all one-electron STO integrators */
    template<typename OperatorType> class tOneSTOIntegrator: public tOneIntegrator<OperatorType>
    {
    };

    /* Cartesian multipole moment and STO integrator */
    class tCartMultMomentSTOIntegrator: public tOneSTOIntegrator<tCartMultMoment>
    {
        public:
            getPrimHSTOIntegral(const tCartMultMoment& oneOper) noexcept;
        private:
    };

    /* Nuclear attraction potential and STO integrator */
    class tNucAttractPotentialSTOIntegrator: public tOneSTOIntegrator
    {
    };
}

#endif
