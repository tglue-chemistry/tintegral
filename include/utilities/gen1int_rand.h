/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file defines some simple functions for random numbers.

   2014-06-23, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_RAND_H)
#define GEN1INT_RAND_H

/* uses the function rand() */
#include <stdlib.h>
/* uses GReal */
#include "common/gen1int_type.h"

/* random integer on [a,b] */
#define GRandInt(a,b) ((rand()%(b-a+1))+a)
/* random real number on [a,b] */
#define GRandReal(a,b) ((rand()/(GReal)RAND_MAX)*(b-a)+a)

#endif
