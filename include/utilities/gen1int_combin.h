/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of functions related to combinatorics.

   2014-06-23, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_COMBIN_H)
#define GEN1INT_COMBIN_H

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"
/* arithmetic constants and functions */
#include "utilities/gen1int_arith.h"

/* # calculates the binomial coefficient */
extern GErrorCode BinomCoefficient(const GInt,const GInt,GULong*);

/* # integer type for representing the combinations */
#if defined(SHORT_BIT_COMBIN)
typedef GUShort GBitInt;
#define GBITINT_MAX USHRT_MAX
#elif defined(INT_BIT_COMBIN)
typedef GUInt GBitInt;
#define GBITINT_MAX UINT_MAX
#else
typedef GULong GBitInt;
#define GBITINT_MAX ULONG_MAX
#endif

/* converts an GBitInt integer to a string in binary format, for writing */
extern const GChar *GBitIntToString(const GBitInt);

/* # maximum number for calling BitCombinNext() instead of BitCombinSetRank() */
#if !defined(BITCOMBIN_MAX_NEXT)
#define BITCOMBIN_MAX_NEXT 100
#endif

/* context of combinations represented by unsigned integers */
typedef struct {
    /* set by input arguments */
    GInt size_n;              /* the size of a set from which k elements will be chosen */
    GInt k_combin;            /* the number k */
    /* details of combinations */
    GULong num_combin;        /* number of combinations, \binom{n}{k} */
    GULong rank_combin;       /* rank of the current combination */
    GInt size_bit_int;        /* size of the integer for representing the combinations */
    GInt num_bit_int;         /* number of integers for the combinations */
    GBitInt *combin_int;      /* integers for the combinations */
} BitCombin;

/* functions related to the combinations represented by struct BitCombin */
extern GErrorCode BitCombinCreate(BitCombin*,const GInt,const GInt);
extern GErrorCode BitCombinSetK(BitCombin*,const GInt);
extern GErrorCode BitCombinStart(BitCombin*);
extern GULong BitCombinGetNumCombin(const BitCombin*);
extern GULong BitCombinGetRank(const BitCombin*);
extern GErrorCode BitCombinNext(BitCombin*,const GULong);
extern GErrorCode BitCombinGetIdxFromRange(const BitCombin*,const GInt,GInt*);
extern GErrorCode BitCombinGetIdxFromSet(const BitCombin*,const GInt*,GInt*);
extern GErrorCode BitCombinCheckRank(const BitCombin*);
extern GErrorCode BitCombinSetRank(BitCombin*,const GULong);
extern GErrorCode BitCombinWrite(const BitCombin*,FILE*);
extern GErrorCode BitCombinDuplicate(const BitCombin*,BitCombin*);
extern GErrorCode BitCombinDestroy(BitCombin*);

/* ranks a combination, may be just for tests */
extern GErrorCode RankCombinOneBasedIdx(const GInt,const GInt*,GULong*);
extern GErrorCode RankCombinZeroBasedIdx(const GInt,const GInt*,GULong*);

/* # context of multinomial expansion */
typedef struct {
  GInt sum_power;        /* power of the sum */
  GInt num_sum_terms;    /* number of terms in the sum */
  GULong num_exp_terms;  /* number of terms in the expansion */
  GULong *mult_coef;     /* multinomial coefficients, size \var{num_exp_terms} */
  GInt **powers;         /* powers of terms in the expansion,
                            size \var{num_exp_terms}*\var{num_sum_terms} */
} MultExp;

/* functions related to the multinomial expansion */
extern GVoid NextComposition(const GInt,const GInt,const GULong,GULong*,GInt*);
extern GErrorCode MultExpCreate(MultExp*,const GInt,const GInt);
extern GErrorCode MultExpGetSize(const MultExp*,GULong*);
extern GErrorCode MultExpGetAllTerms(const MultExp*,GULong*,GInt*);
extern GErrorCode MultExpGetTerm(const MultExp*,const GULong,GULong*,GInt*);
extern GErrorCode MultExpGetPowers(const MultExp*,const GULong,GInt*);
extern GErrorCode MultExpGetCoef(const MultExp*,const GULong,GULong*);
extern GErrorCode MultExpWrite(const MultExp*,FILE*);
extern GErrorCode MultExpDestroy(MultExp*);

/* # generates the next permutation for a given integer sequence */
extern GBool NextPermutation(GInt*,GInt*);

#endif
