/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This is the header file of arithmetic.

   2014-06-26, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_ARITH_H)
#define GEN1INT_ARITH_H

#include <math.h>

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"

/* constant PI */
#if !defined(M_PI)
#define M_PI 3.141592653589793238462643383279
#endif

/* absolute value of a number */
#define GAbs(a) (((a)>=0) ? (a) : -(a))
/* maximum of two numbers */
#define GMax(a,b) (((a)>b) ? (a) : (b))
/* minimum of two numbers */
#define GMin(a,b) (((a)<b) ? (a) : (b))

/* rounds to integer */
extern GErrorCode GIntRint(const GReal,GInt*);
extern GErrorCode GULongRint(const GReal,GULong*);

/* pow function for integers */
extern GErrorCode GIntPow(const GInt,const GInt,GInt*);

#endif
