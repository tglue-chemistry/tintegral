/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This is the header file of sorting.

   2014-06-27, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_SORT_H)
#define GEN1INT_SORT_H

/* basic types used in Gen1Int */
#include "common/gen1int_type.h"

/* minimum integer for an atomic center */
#define GMIN_IDX_ATOM 1
/* maximum integer for a non-atomic center */
#define GMAX_IDX_NAT 0

/* sorts given indices in ascending order */
extern GVoid SortIdxAscending(const GInt,GInt*);
extern GVoid SortTagIdxAscending(const GInt,GInt*,GInt*);

#endif
