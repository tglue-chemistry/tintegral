/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of atomic orbital (AO) shells.

   2014-08-19, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_AO_SHELL_H)
#define GEN1INT_AO_SHELL_H

/* contracted Gaussian type orbitals (GTOs) */
#include "basis/gen1int_contr_gto.h"

/* AO shell */
typedef struct {
    GInt idx_cent;           /* index of atomic center where the AO shell locates */
    GReal coord_cent[3];     /* coordinates of the atomic center */
    GInt num_contr_GTO;      /* number of contracted GTOs */
    GInt counter_contr_GTO;  /* counter of contracted GTOs */
    ContrGTO *contr_GTO;     /* contracted GTOs */
    GInt num_AO;             /* number of AOs */
} AOShell;

/* functions of AO shell */
extern GErrorCode AOShellCreate(AOShell*,
                                const GInt,
                                const GReal[3],
                                const GInt);
extern GErrorCode AOShellAddContrGTO(AOShell*,
                                     const GTOType,
                                     const GInt,
                                     const GInt,
                                     const GReal*,
                                     const GInt,
                                     const GReal*);
extern GErrorCode AOShellNormalize(AOShell*);
extern GErrorCode AOShellGetNumAO(const AOShell*,GInt*);
extern GErrorCode AOShellWrite(const AOShell*,const GChar*);
/*extern GErrorCode AOShellGetValues(const AOShell*,
                                   const GReal[],
                                   const GInt,
                                   const GReal*);*/
extern GErrorCode AOShellDestroy(AOShell*);

#endif
