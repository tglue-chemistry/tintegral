/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of contracted Gaussian type orbitals (GTOs).

   2014-06-22, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_CONTR_GTO_H)
#define GEN1INT_CONTR_GTO_H

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"

/* type of Gaussian type orbitals (GTOs) */
typedef enum {
    CARTESIAN_GTO=0,  /* Cartesian GTOs */
    SPHERICAL_GTO=1   /* spherical GTOs, do not change (otherwise, the function
                         ContrGTOCreate() may need to be modified) */
} GTOType;

/* contracted GTOs */
typedef struct {
    GTOType GTO_type;      /* type of GTOs */
    GInt angular_num;      /* angular number */
    GInt num_prim;         /* number of primitive GTOs */
    GReal *exponents;      /* exponents of primitive GTOs */
    GInt num_contr;        /* number of contractions */
    GReal *contr_coef;     /* contraction coefficients */
} ContrGTO;

/* functions of contracted GTOs */
extern GErrorCode ContrGTOCreate(ContrGTO*,
                                 const GTOType,
                                 const GInt,
                                 const GInt,
                                 const GReal*,
                                 const GInt,
                                 const GReal*);
extern GErrorCode ContrGTONormalize(ContrGTO*);
extern GErrorCode ContrGTOGetNumPrim(const ContrGTO*,GInt*);
extern GErrorCode ContrGTOGetNumContr(const ContrGTO*,GInt*);
extern GErrorCode ContrGTOGetNumAO(const ContrGTO*,GInt*);
extern GErrorCode ContrGTOGetExponents(const ContrGTO*,const GInt,GReal*);
extern GErrorCode ContrGTOGetCoefficients(const ContrGTO*,const GInt,GReal*);
extern GErrorCode ContrGTOWrite(const ContrGTO*,const GChar*);
/*extern GErrorCode ContrGTOGetValues(const ContrGTO*,
                                    const GReal[],
                                    const GInt,
                                    const GReal*);*/
extern GErrorCode ContrGTODestroy(ContrGTO*);

#endif
