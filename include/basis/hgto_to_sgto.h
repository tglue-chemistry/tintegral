    ! inverse of constant Pi
    real(REALK), parameter :: INV_PI = 1.0_REALK/PI
    ! normalization constant of s-shell spherical Gaussians
    real(REALK), parameter :: NRM_S_SGTO = sqrt(INV_PI)*0.5_REALK
    ! normalization constant of p-shell spherical Gaussians
    real(REALK), parameter :: NRM_P_SGTO = sqrt(3.0_REALK)*NRM_S_SGTO
    ! normalization constant of d-shell spherical Gaussians
    real(REALK), parameter :: NRM_D_SGTO(3) = (/                &
      sqrt(15.0_REALK)*NRM_S_SGTO, sqrt(3.75_REALK)*NRM_S_SGTO, &
      sqrt(1.25_REALK)*NRM_S_SGTO/)
