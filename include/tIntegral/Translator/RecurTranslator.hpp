/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file generates source codes for the recurrence relation compiler and
   generator.

   2019-06-13, Bin Gao:
   * first version
*/

#pragma once

#include <iostream>
#include <regex>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "tIntegral/Symbol/RecurDirection.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"

namespace tIntegral
{
    /* Recurrence relation translator */
    class RecurTranslator
    {
        protected:
            /* End of a statement */
            virtual std::string end_of_statement(const bool newLine=true) const noexcept = 0;
            /* Declaration specifier */
            virtual std::string declaration_specifier(const RecurTypeSpecifier declSpecifier) const noexcept = 0;
            /* Declarators */
            virtual std::string get_declarator(const RecurDeclarator declarator) const noexcept = 0;
            /* Access a member of an object */
            virtual std::string member_of_object() const noexcept = 0;
            /* Access a member of a pointer */
            virtual std::string member_of_pointer() const noexcept = 0;
            /* Return a comparison operator */
            virtual std::string comparison_operator(const RecurComparison oper) const noexcept = 0;
            /* Return a logical operator */
            virtual std::string logical_operator(const RecurLogical oper) const noexcept = 0;
            /* Return an increment or decrement operator */
            virtual std::string inc_dec_operator(const RecurIncDec oper) const noexcept = 0;
            /* Return an arithmetic operator */
            virtual std::string arithmetic_operator(const RecurArithmetic oper) const noexcept = 0;
            /* Return an assignment operator */
            virtual std::string assignment_operator(const RecurAssignment oper) const noexcept = 0;
            /* Increase indent */
            inline void increase_indent(std::string& indent) const noexcept
            {
                indent += m_default_indent;
            }
            /* Decrease indent */
            inline void decrease_indent(std::string& indent) const noexcept
            {
                indent.resize(indent.size()-m_len_default_indent);
            }
            /* Make the parameter list of a function */
            inline std::string make_parameter_list(const std::vector<std::string>& parameters,
                                                   const std::string& indent=std::string(),
                                                   const bool newLine=false) const noexcept
            {
                /* Get the total length of parameters */
                unsigned int len_params = 0;
                for (auto const& each_param: parameters) len_params += each_param.size();
                /* If the total length of parameters is too large, we will put them into different lines */
                std::string str_params;
                if (len_params>m_maxlen_string) {
                    for (unsigned int iparam=0; iparam<parameters.size(); ++iparam) {
                        auto each_param = std::regex_replace(parameters[iparam], std::regex("\n"), "\n"+indent);
                        str_params += iparam==0 ? "\n"+indent+each_param : ",\n"+indent+each_param;
                    }
                }
                else {
                    for (unsigned int iparam=0; iparam<parameters.size(); ++iparam) {
                        auto each_param = std::regex_replace(parameters[iparam], std::regex("\n"), "\n"+indent);
                        str_params += iparam==0 ? (newLine ? "\n"+indent+each_param : each_param) : ", "+each_param;
                    }
                }
                return str_params;
            }
        public:
            explicit RecurTranslator(const unsigned int lenIndent=4,
                                     const unsigned int maxLenString=72) noexcept:
                m_len_default_indent(lenIndent),
                m_default_indent(lenIndent, ' '),
                m_maxlen_string(maxLenString) {}
            virtual ~RecurTranslator() noexcept = default;
            /* Open source file(s) */
            virtual bool open_file(const std::string& nameIntegration) noexcept = 0;
            /* Close source files(s) */
            virtual void close_file() noexcept = 0;
            /* Add ifdef directive */
            virtual void ifdef_directive(const std::string& identifier) noexcept = 0;
            /* Add endif directive */
            virtual void endif_directive() noexcept = 0;
            /* Boolean type */
            virtual std::string type_boolean() const noexcept = 0;
            /* Boolean literals */
            virtual std::string boolean_literal(const bool literal=true) const noexcept = 0;
            /* Integer types */
            virtual std::string type_integer(const bool isUnsigned=false) const noexcept = 0;
            /* Floating-point type numbers */
            virtual std::string type_floating_point(const bool isPointer=false) const noexcept = 0;
            /* Return void */
            virtual std::string type_void() const noexcept = 0;
            /* A type to represent the size of any object in bytes */
            virtual std::string type_size() const noexcept = 0;
            /* Null pointer */
            virtual std::string null_pointer() const noexcept = 0;
            /* Shared pointer */
            virtual std::string type_shared_pointer(const std::string& typeObject) const noexcept = 0;
            /* Dereference a pointer */
            virtual std::string dereference_pointer(const std::string& pointer) const noexcept = 0;
            /* Return an rvalue object */
            virtual std::string get_rvalue_object(const std::string& object) const noexcept = 0;
            /* Return a string literal */
            virtual std::string string_literal(const std::string characters) const noexcept = 0;
            /* Array */
            virtual std::string type_array(const std::string& typeElements,
                                           const unsigned int sizeArray) const noexcept = 0;
            /* Valarray */
            virtual std::string type_valarray(const std::string& typeElements) const noexcept = 0;
            /* Vector */
            virtual std::string type_vector(const std::string& typeElements,
                                            const unsigned int dimension=1) const noexcept = 0;
            /* A member object */
            virtual std::string member_object(const std::string& nameObject) const noexcept
            {
                return "m_"+nameObject;
            }
            /* Access a member using the subscript operator */
            virtual std::string subscript_operation(const std::string& operand,
                                                    const unsigned int position=0) const noexcept = 0;
            virtual std::string subscript_operation(const std::string& operand,
                                                    const std::string& position) const noexcept = 0;
            virtual std::string subscript_operation(const std::string& operand,
                                                    const std::vector<std::string>& position) const noexcept = 0;
            /* Vector constructor */
            virtual std::string vector_constructor(const std::string& typeElements,
                                                   const std::vector<std::string>& elements) const noexcept = 0;
            virtual std::string vector_constructor(const std::vector<unsigned int>& elements) const noexcept = 0;
            /* Access component of a Cartesian vector */
            virtual std::string cartesian_component(const std::string& vectorName,
                                                    const RecurDirection vectorDirection,
                                                    const RecurDirection recurDirection) const noexcept
            {
                return subscript_operation(vectorName,
                                           transform_direction_indexed(recurDirection, vectorDirection));
            }
            /* Jagged array */
            virtual std::string jagged_array_constructor(const std::vector<std::vector<int>>& jagged_array) const noexcept = 0;
            /* Make a declaration */
            virtual std::string make_declaration(const std::string& typeName,
                                                 const std::string& objectName,
                                                 const bool isStatement=false,
                                                 const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None,
                                                 const RecurDeclarator declarator=RecurDeclarator::None) const noexcept = 0;
            /* template-id */
            virtual std::string template_id(const std::string name,
                                            const std::vector<std::string>& parameters=std::vector<std::string>(),
                                            const std::string& indent=std::string()) const noexcept = 0;
            /* Information of the integrand function */
            virtual std::string recur_function_type(const std::string& typeFunction) const noexcept
            {
                return type_shared_pointer(typeFunction);
            }
            /* RecurIntegration class */
            virtual std::string recur_integration_type() const noexcept = 0;
            /* Information of the RecurNode class */
            virtual std::string recur_node_type(const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None) const noexcept = 0;
            virtual std::string index_nodes_object(const unsigned int position) const noexcept
            {
                return member_object("idx"+std::to_string(position)+"_nodes");
            }
            /* Information of the RecurArray class */
            virtual std::string recur_array_type() const noexcept = 0;
            virtual std::string recur_relation_type(const unsigned int numRelations) const noexcept = 0;
            virtual std::string recur_relation_object() const noexcept
            {
                return member_object(std::string("recur_arrays"));
            }
            virtual std::string recur_array_object(const unsigned int position) const noexcept
            {
                return subscript_operation(recur_relation_object(), position);
            }
            virtual RecurOperandType recur_array_access() const noexcept
            {
                return RecurOperandType::ObjectOperand;
            }
            virtual void recur_array_begin_loop(const unsigned int position) noexcept = 0;
            virtual std::string recur_array_node_left(const unsigned int position) const noexcept = 0;
            virtual std::string recur_array_current_node(const unsigned int position) const noexcept = 0;
            virtual void recur_array_next_node(const unsigned int position) noexcept = 0;
            virtual std::string recur_array_get_input(const unsigned int position,
                                                      const std::string& outputIndex) const noexcept = 0;
            virtual std::string recur_array_input_type() const noexcept = 0;
            /* Information of the RecurBuffer class */
            virtual std::string recur_buffer_type() const noexcept = 0;
            virtual std::string recur_buffer_name() const noexcept { return std::string("buffer"); }
            virtual std::string recur_buffer_object() const noexcept
            {
                return member_object(recur_buffer_name());
            }
            virtual RecurOperandType recur_buffer_access() const noexcept
            {
                return RecurOperandType::PointerOperand;
            }
            virtual std::string recur_buffer_get() const noexcept = 0;
            /* Information of the RecurIndex class */
            virtual std::string recur_index_type() const noexcept = 0;
            virtual std::string recur_index_vector() const noexcept = 0;
            virtual std::string all_indices_name() const noexcept { return std::string("allIndices"); }
            virtual std::string output_nodes_name() const noexcept { return std::string("outputNodes"); }
            /* Get the type of an LHS node */
            virtual std::string lhs_node_type() const noexcept = 0;
            /* Get an LHS node object */
            virtual std::string lhs_node_object(const bool isDeclaration=false,
                                                const bool isStatement=false) const noexcept
            {
                std::string object("lhs_node");
                return isDeclaration ? make_declaration(lhs_node_type(), object, isStatement) : object;
            }
            /* Get the offset of an LHS node */
            virtual std::string lhs_node_offset(const bool isDeclaration=false,
                                                const bool isStatement=false) const noexcept
            {
                std::string object("offset_lhs");
                return isDeclaration ? make_declaration(type_size(), object, isStatement) : object;
            }
            /* Get the value of an LHS node */
            virtual std::string lhs_node_value(const bool isDeclaration=false,
                                               const bool isStatement=false,
                                               const bool dereference=false,
                                               const bool useValarray=true) const noexcept
            {
                std::string object("val_lhs");
                return isDeclaration
                    ? make_declaration(useValarray ? type_valarray(type_floating_point()) : type_floating_point(true),
                                       object,
                                       isStatement,
                                       RecurTypeSpecifier::None,
                                       RecurDeclarator::Pointer)
                    : (dereference ? dereference_pointer(object) : object);
            }
            virtual RecurOperandType lhs_node_access() const noexcept
            {
                return RecurOperandType::PointerOperand;
            }
            /* Call the get_offset() function of an LHS node */
            virtual std::string lhs_get_offset() const noexcept = 0;
            /* Call the get_order() function of an LHS node */
            virtual std::string lhs_get_order(const std::string& indices, const unsigned int position) const noexcept = 0;
            /* Call the get_rhs() function of an LHS node */
            virtual std::string lhs_get_rhs() const noexcept = 0;
            /* Get the type of RHS nodes */
            virtual std::string rhs_nodes_type() const noexcept = 0;
            /* Get the object of RHS nodes */
            virtual std::string rhs_nodes_object(const bool isDeclaration=false,
                                                 const bool isStatement=false) const noexcept
            {
                std::string object("rhs_nodes");
                return isDeclaration ? make_declaration(rhs_nodes_type(), object, isStatement) : object;
            }
            virtual RecurOperandType rhs_nodes_access() const noexcept
            {
                return RecurOperandType::ObjectOperand;
            }
            /* Get the size of RHS nodes */
            virtual std::string rhs_nodes_size() const noexcept = 0;
            /* Get a RHS node object at a given position */
            virtual std::string rhs_nodes_at(const unsigned int position) const noexcept
            {
                return rhs_nodes_at(std::to_string(position));
            }
            virtual std::string rhs_nodes_at(const std::string& position) const noexcept
            {
                return subscript_operation(rhs_nodes_object(), position);
            }
            virtual RecurOperandType rhs_node_access() const noexcept
            {
                return RecurOperandType::PointerOperand;
            }
            /* Call the get_offset() function of a RHS node */
            virtual std::string rhs_get_offset(const unsigned int position) const noexcept
            {
                return rhs_get_offset(std::to_string(position));
            }
            virtual std::string rhs_get_offset(const std::string& position) const noexcept = 0;
            /* Get the offset of a RHS node along a given direction */
            virtual std::string rhs_node_offset(const unsigned int rhsPosition,
                                                const RecurDirection rhsDirection=RecurDirection::XYZ,
                                                const bool isDeclaration=false,
                                                const bool isStatement=false) const noexcept
            {
                std::string object = "offset_rhs"
                                   + std::to_string(rhsPosition)
                                   + "_"
                                   + stringify_direction(rhsDirection);
                return isDeclaration ? make_declaration(type_size(), object, isStatement) : object;
            }
            /* Get the value of a RHS node along a given direction */
            virtual std::string rhs_node_value(const unsigned int rhsPosition,
                                               const RecurDirection rhsDirection=RecurDirection::X,
                                               const bool isDeclaration=false,
                                               const bool isStatement=false,
                                               const bool dereference=false,
                                               const bool useValarray=true) const noexcept
            {
                std::string object = "val_rhs"
                                   + std::to_string(rhsPosition)
                                   + "_"
                                   + stringify_direction(rhsDirection);
                return isDeclaration
                    ? make_declaration(useValarray ? type_valarray(type_floating_point()) : type_floating_point(true),
                                       object,
                                       isStatement,
                                       RecurTypeSpecifier::None,
                                       RecurDeclarator::Pointer)
                    : (dereference ? dereference_pointer(object) : object);
            }
            /* Get the order of a component of an index, where
               RecurDirection::XYZ is the order of the index */
            virtual std::string index_order(const std::string& idxName,
                                            const RecurDirection component=RecurDirection::XYZ,
                                            const bool isDeclaration=false,
                                            const bool isStatement=false) const noexcept
            {
                std::string object = "order_"+idxName+"_"+stringify_direction(component);
                return isDeclaration ? make_declaration(type_integer(false), object, isStatement) : object;
            }
            ///* Get the order of a component of an index from its given
            //   direction and a given recurrence relation direction */
            //virtual std::string index_order(const std::string& idxName,
            //                                const RecurDirection orderDirection,
            //                                const RecurDirection recurDirection,
            //                                const bool isDeclaration=false) const noexcept
            //{
            //    return index_order(idxName,
            //                       transform_direction(recurDirection, orderDirection),
            //                       isDeclaration);
            //}
            /* Get names of functions for performing recurrence relations */
            //FIXME: return function-name+() for some case??
            virtual std::string top_down_function() const noexcept { return std::string("top_down"); }
            virtual std::string bottom_up_function() const noexcept { return std::string("bottom_up"); }
            virtual std::string eval_function() const noexcept { return std::string("eval"); }
            virtual std::string input_node_function() const noexcept { return std::string("get_input_nodes"); }
            /* Get the name of the top-down or bottom-up procedure of an index
               (recurrence relation) */
            virtual std::string idx_recur_function(const std::string& idxName, const bool isTopDown=true) const noexcept
            {
                return isTopDown ? idxName+"_"+top_down_function() : idxName+"_"+bottom_up_function();
            }
            /* Return a comparison operation */
            virtual std::string comparison_operation(const std::string& lhs,
                                                     const RecurComparison oper,
                                                     const std::string& rhs) const noexcept
            {
                return lhs+comparison_operator(oper)+rhs;
            }
            /* Return a logical operation */
            virtual std::string logical_operation(const RecurLogical oper,
                                                  const std::string& rhs) const noexcept
            {
                return logical_operator(oper)+rhs;
            }
            virtual std::string logical_operation(const std::string& lhs,
                                                  const RecurLogical oper,
                                                  const std::string& rhs) const noexcept
            {
                return lhs+logical_operator(oper)+rhs;
            }
            /* Return an increment or decrement operation */
            virtual std::string inc_dec_operation(const RecurIncDec oper, const std::string& var) const noexcept
            {
                return inc_dec_operator(oper)+var;
            }
            /* Write an increment or decrement statement */
            virtual void inc_dec_statement(const RecurIncDec oper, const std::string& var) noexcept = 0;
            /* Return an arithmetic operation */
            virtual std::string arithmetic_operation(const RecurArithmetic oper,
                                                     const std::string& rhs) const noexcept
            {
                return arithmetic_operator(oper)+rhs;
            }
            virtual std::string arithmetic_operation(const std::string& lhs,
                                                     const RecurArithmetic oper,
                                                     const std::string& rhs) const noexcept
            {
                return lhs+arithmetic_operator(oper)+rhs;
            }
            /* Return a parenthesized expression */
            virtual std::string parenthesize_expression(const std::string& expression) const noexcept = 0;
            /* Return an assignment operation */
            virtual std::string assignment_operation(const std::string& lhs,
                                                     const RecurAssignment oper,
                                                     const std::string& rhs) const noexcept
            {
                return lhs+assignment_operator(oper)+rhs;
            }
            /* Write an assignment statement */
            virtual void assignment_statement(const std::string& lhs,
                                              const RecurAssignment oper,
                                              const std::string& rhs,
                                              const bool rvalueRHS=false) noexcept = 0;
            /* Write a declaration assignment statement */
            virtual void write_declaration_assignment(const std::string& typeName,
                                                      const std::string& objectName,
                                                      const RecurAssignment oper,
                                                      const std::string& rhs,
                                                      const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None,
                                                      const RecurDeclarator declarator=RecurDeclarator::None,
                                                      const bool rvalueRHS=false) noexcept = 0;
            /* Write a comment */
            virtual void write_comment(const std::string& comment) noexcept = 0;
            /* Include header files */
            virtual void include_header_files(const std::vector<std::string>& headerFiles,
                                              const bool systemHeaders=false) noexcept = 0;
            /* Begin a namespace */
            virtual void begin_namespace(const std::string& scopeName,
                                         const std::string& description,
                                         const std::vector<std::string>& typeNames,
                                         const bool doQuadrature=false) noexcept = 0;
            /* End a namespace */
            virtual void end_namespace() noexcept = 0;
            /* Access a class, object or function in a namespace */
            virtual std::string namespace_member(const std::string& scopeName,
                                                 const std::string& memberName) const noexcept = 0;
            /* Begin a class with private members and initialization list,
               where \var(memberObjects) contains type, name and initialization
               value of each member object, and whether the initialization
               value is an argument */
            virtual void begin_class(
                const std::string& className,
                const std::vector<std::string>& tparameters,
                const std::vector<std::tuple<std::string,std::string,std::string,bool>>& memberObjects,
                const std::string& description,
                const std::string& baseClass=std::string(),
                const std::vector<std::string>& constructorStatements=std::vector<std::string>(),
                const std::vector<std::string>& destructorStatements=std::vector<std::string>()) noexcept = 0;
            /* End a class */
            virtual void end_class() noexcept = 0;
            /* Access a class member */
            virtual std::string class_member(const std::string& className,
                                             const std::string& memberName) const noexcept = 0;
            /* Begin a public member section of a class */
            virtual void begin_public_section() noexcept = 0;
            /* Begin a protected member section of a class */
            virtual void begin_protected_section() noexcept = 0;
            /* Begin a private member section of a class */
            virtual void begin_private_section() noexcept = 0;
            /* End a section of a class */
            virtual void end_class_section() noexcept = 0;
            /* Declare a function */
            virtual void declare_function(const std::string& functionName,
                                          const std::string& returnType,
                                          const std::vector<std::string>& tparameters,
                                          const std::vector<std::string>& parameters,
                                          const std::string& description,
                                          const RecurFunctionSpecifier specifier=RecurFunctionSpecifier::OverrideFunction) noexcept = 0;
            /* Begin a function */
            virtual void begin_function(const std::string& functionName,
                                        const std::string& returnType,
                                        const std::vector<std::string>& tparameters,
                                        const std::vector<std::string>& parameters,
                                        const std::string& description,
                                        const RecurFunctionSpecifier specifier=RecurFunctionSpecifier::OverrideFunction) noexcept = 0;
            /* End a function */
            virtual void end_function() noexcept = 0;
            /* Call a free function */
            virtual std::string call_function(const std::string& functionName,
                                              const std::vector<std::string>& parameters,
                                              const std::string& indent=std::string(),
                                              const bool isTemplate=false) const noexcept = 0;
            /* Call a member function */
            virtual std::string call_function(const std::string& operandName,
                                              const std::string& functionName,
                                              const std::vector<std::string>& parameters,
                                              const RecurOperandType operandType=RecurOperandType::ObjectOperand,
                                              const std::string& indent=std::string(),
                                              const bool isTemplate=false) const noexcept = 0;
            /* Statement of calling a free function */
            virtual void call_function_statement(const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const bool isTemplate=false) noexcept = 0;
            /* Statement of calling a member function */
            virtual void call_function_statement(const std::string& operandName,
                                                 const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const RecurOperandType operandType=RecurOperandType::ObjectOperand,
                                                 const bool isTemplate=false) noexcept = 0;
            /* Return statement */
            virtual void return_statement(const std::string& expression) noexcept = 0;
            /* Handle an error */
            virtual void handle_error(const std::vector<std::string>& message) noexcept = 0;
            /* Handle debug */
            virtual void handle_debug(const std::vector<std::string>& message) noexcept = 0;
            /* Begin an if statement */
            virtual void begin_if_statement(const std::string& condition,
                                            const bool elifStatement=false) noexcept = 0;
            /* Begin an else statement */
            virtual void begin_else_statement() noexcept = 0;
            /* End an if-else statement */
            virtual void end_elif_statement() noexcept = 0;
            /* Begin a for loop */
            virtual void begin_for_loop(const std::string& typeInitialization,
                                        const std::vector<std::pair<std::string,std::string>>& varInitialization,
                                        const std::string& condition,
                                        const std::vector<std::string>& iteration) noexcept = 0;
            /* End a for loop */
            virtual void end_for_loop() noexcept = 0;
            /* Begin a while loop */
            virtual void begin_while_loop(const std::string& condition) noexcept = 0;
            /* End a while loop */
            virtual void end_while_loop() noexcept = 0;
            /* Begin a switch statement of order of an index */
            virtual void switch_index_order(const std::string& idxName) noexcept = 0;
            /* End a switch statement */
            virtual void end_switch() noexcept = 0;
            /* Begin a case statement of order of an index */
            virtual void case_index_order(const int order) noexcept = 0;
            /* Begin a default case statement */
            virtual void case_default() noexcept = 0;
            /* End a case statement */
            virtual void end_case() noexcept = 0;

            /* High level APIs */

            /* Invoke a boolean function and invoke error handler */
            //FIXME: change errorMessage to std::vector<std::string>??
            virtual void invoke_boolean_function(const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const std::string& errorMessage=std::string(),
                                                 const bool isTemplate=false) noexcept
            {
                begin_if_statement(
                    logical_operation(RecurLogical::NOT,
                                      call_function(functionName, parameters, std::string(), isTemplate))
                );
                handle_error(std::vector<std::string>({string_literal(errorMessage)}));
                end_elif_statement();
            }
            virtual void invoke_boolean_function(const std::string& operandName,
                                                 const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const RecurOperandType operandType=RecurOperandType::ObjectOperand,
                                                 const std::string& errorMessage=std::string(),
                                                 const bool isTemplate=false) noexcept
            {
                begin_if_statement(
                    logical_operation(
                        RecurLogical::NOT,
                        call_function(operandName, functionName, parameters, operandType, std::string(), isTemplate)
                    )
                );
                handle_error(std::vector<std::string>({string_literal(errorMessage)}));
                end_elif_statement();
            }
            /* Assign a given order to a component */
            virtual void assign_component_order(const std::string& idxName,
                                                const RecurDirection component,
                                                const int order,
                                                const bool isDeclaration=false) noexcept
            {
                assignment_statement(index_order(idxName, component, isDeclaration),
                                     RecurAssignment::BasicAssignment,
                                     std::to_string(order));
            }
            /* Assign the order of another component of an index to a component
               of the index, and with an adjustment if it is non zero */
            virtual void assign_component_order(const std::string& idxName,
                                                const RecurDirection component,
                                                const RecurDirection another,
                                                const int adjustment,
                                                const bool isDeclaration=false) noexcept
            {
                assign_component_order(idxName, component, another, RecurDirection::Null, adjustment, isDeclaration);
            }
            /* Assign the difference of orders between the first and second
               components of an index to a component of the index, and with an
               adjustment if it is non zero */
            virtual void assign_component_order(const std::string& idxName,
                                                const RecurDirection component,
                                                const RecurDirection first,
                                                const RecurDirection second,
                                                const int adjustment,
                                                const bool isDeclaration=false) noexcept
            {
                auto rhs = index_order(idxName, first);
                if (second!=RecurDirection::Null) {
                    rhs = arithmetic_operation(rhs, RecurArithmetic::Subtraction, index_order(idxName, second));
                }
                if (adjustment>0) {
                    rhs = arithmetic_operation(rhs, RecurArithmetic::Addition, std::to_string(adjustment));
                }
                else if (adjustment<0) {
                    rhs = arithmetic_operation(rhs, RecurArithmetic::Subtraction, std::to_string(-adjustment));
                }
                assignment_statement(index_order(idxName, component, isDeclaration),
                                     RecurAssignment::BasicAssignment,
                                     rhs);
            }
            /* Decrease order of a component */
            virtual void decrease_component_order(const std::string& idxName,
                                                  const RecurDirection component) noexcept
            {
                inc_dec_statement(RecurIncDec::Decrement, index_order(idxName, component));
            }
            ///* Decrease order of a component towards a given edge */
            //virtual void decrease_component_order(const std::string& idxName,
            //                                      const RecurDirection firstComponent,
            //                                      const RecurDirection lastComponent) noexcept
            //{
            //    inc_dec_statement(RecurIncDec::Decrement,
            //                      index_order(idxName, get_triangle_corner(firstComponent, lastComponent)));
            //}
            /* Begin a for loop over a component by increment */
            virtual void begin_loop_component(const std::string& idxName,
                                              const RecurDirection component,
                                              const int beginOrder,
                                              const RecurComparison endCondition,
                                              const RecurDirection endDirection,
                                              const RecurDirection adjustDirection,
                                              const int endAdjustment,
                                              const std::vector<std::pair<RecurDirection,RecurIncDec>>& compIteration,
                                              const bool iterDeclared=false) noexcept
            {
                /* Set the iterator of the for loop */
                std::string iter_component = index_order(idxName, component);
                /* Set the end order */
                auto end_order = index_order(idxName, endDirection);
                if (adjustDirection!=RecurDirection::Null) {
                    end_order = arithmetic_operation(end_order,
                                                     RecurArithmetic::Subtraction,
                                                     index_order(idxName, adjustDirection));
                }
                if (endAdjustment>0) {
                    end_order = arithmetic_operation(end_order,
                                                     RecurArithmetic::Addition,
                                                     std::to_string(endAdjustment));
                }
                else if (endAdjustment<0) {
                    end_order = arithmetic_operation(end_order,
                                                     RecurArithmetic::Subtraction,
                                                     std::to_string(-endAdjustment));
                }
                /* Set iteration */
                std::vector<std::string> iteration;
                for (auto const& each_comp: compIteration) {
                    iteration.push_back(inc_dec_operation(each_comp.second, index_order(idxName, each_comp.first)));
                }
                iteration.push_back(inc_dec_operation(RecurIncDec::Increment, iter_component));
                /* Write the for loop */
                begin_for_loop(iterDeclared ? type_integer() : std::string(),
                               std::vector<std::pair<std::string,std::string>>({
                                   std::make_pair(iter_component, std::to_string(beginOrder))
                               }),
                               comparison_operation(iter_component, endCondition, end_order),
                               iteration);
            }
            /* Begin a for loop over a component by decrement */
            virtual void begin_loop_component(const std::string& idxName,
                                              const RecurDirection component,
                                              const RecurDirection endDirection,
                                              const RecurDirection adjustDirection,
                                              const int beginAdjustment,
                                              const RecurComparison endCondition,
                                              const int endOrder,
                                              const std::vector<std::pair<RecurDirection,RecurIncDec>>& compIteration,
                                              const bool iterDeclared=false) noexcept
            {
                /* Set the iterator of the for loop */
                std::string iter_component = index_order(idxName, component);
                /* Set the begin order */
                auto begin_order = index_order(idxName, endDirection);
                if (adjustDirection!=RecurDirection::Null) {
                    begin_order = arithmetic_operation(begin_order,
                                                       RecurArithmetic::Subtraction,
                                                       index_order(idxName, adjustDirection));
                }
                if (beginAdjustment>0) {
                    begin_order = arithmetic_operation(begin_order,
                                                       RecurArithmetic::Addition,
                                                       std::to_string(beginAdjustment));
                }
                else if (beginAdjustment<0) {
                    begin_order = arithmetic_operation(begin_order,
                                                       RecurArithmetic::Subtraction,
                                                       std::to_string(-beginAdjustment));
                }
                /* Set iteration */
                std::vector<std::string> iteration;
                for (auto const& each_comp: compIteration) {
                    iteration.push_back(inc_dec_operation(each_comp.second, index_order(idxName, each_comp.first)));
                }
                iteration.push_back(inc_dec_operation(RecurIncDec::Decrement, iter_component));
                /* Write the for loop */
                begin_for_loop(iterDeclared ? type_integer() : std::string(),
                               std::vector<std::pair<std::string,std::string>>({
                                   std::make_pair(iter_component, begin_order)
                               }),
                               comparison_operation(iter_component, endCondition, std::to_string(endOrder)),
                               iteration);
            }
            /* Type of functor class */
            virtual std::string type_functor(const std::string returnType,
                                             const std::vector<std::string>& paramTypes) const noexcept = 0;
            /* Invoke a functor class */
            virtual void invoke_functor(const std::string quadrature, const std::string integrand) noexcept = 0;
        private:
            /* Length of default indent of code*/
            unsigned int m_len_default_indent;
            /* Default indent of code */
            std::string m_default_indent;
            /* Maximum length of a string content */
            unsigned int m_maxlen_string;
    };
}
