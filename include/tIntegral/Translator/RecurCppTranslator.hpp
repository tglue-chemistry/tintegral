/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file generates C++ source codes for the recurrence relation compiler
   and generator.

   2019-06-13, Bin Gao:
   * first version
*/

#pragma once

#include <fstream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"

namespace tIntegral
{
    class RecurCppTranslator: virtual public RecurTranslator
    {
        protected:
            /* Error notation */
            virtual std::string error_notation() const noexcept;
            /* End of a statement */
            virtual std::string end_of_statement(const bool newLine=true) const noexcept override;
            /* Declaration specifier */
            virtual std::string declaration_specifier(const RecurTypeSpecifier declSpecifier) const noexcept override;
            /* Declarators */
            virtual std::string get_declarator(const RecurDeclarator declarator) const noexcept override;
            /* Access a member of an object */
            virtual std::string member_of_object() const noexcept override;
            /* Access a member of a pointer */
            virtual std::string member_of_pointer() const noexcept override;
            /* Return a comparison operator */
            virtual std::string comparison_operator(const RecurComparison oper) const noexcept override;
            /* Return a logical operator */
            virtual std::string logical_operator(const RecurLogical oper) const noexcept override;
            /* Return an increment or decrement operator */
            virtual std::string inc_dec_operator(const RecurIncDec oper) const noexcept override;
            /* Return an arithmetic operator */
            virtual std::string arithmetic_operator(const RecurArithmetic oper) const noexcept override;
            /* Return an assignment operator */
            virtual std::string assignment_operator(const RecurAssignment oper) const noexcept override;
            /* Scope resolution operator */
            virtual std::string scope_resolution_operator() const noexcept;
            /* Make a template */
            virtual std::string make_template(const std::vector<std::string>& parameters,
                                              const std::string& indent=std::string()) const noexcept;
            /* Make a comment */
            virtual std::string make_comment(const std::string& comment) const noexcept;
            /* this pointer */
            virtual std::string this_pointer() const noexcept;
            /* Make captures for lambda expressions */
            virtual std::string make_captures(const std::vector<std::string>& captures) const noexcept;
        public:
            explicit RecurCppTranslator(const std::string& realType=std::string("RealType"),
                                        const unsigned int lenIndent=4) noexcept:
                RecurTranslator(lenIndent, 72),
                m_real_type(realType),
                m_section_on(false) {}
            virtual ~RecurCppTranslator() noexcept = default;
            /* Open source file(s) */
            virtual bool open_file(const std::string& nameIntegration) noexcept override;
            /* Close source files(s) */
            virtual void close_file() noexcept override;
            /* Add ifdef directive */
            virtual void ifdef_directive(const std::string& identifier) noexcept override;
            /* Add endif directive */
            virtual void endif_directive() noexcept override;
            /* Boolean type */
            virtual std::string type_boolean() const noexcept override;
            /* Boolean literals */
            virtual std::string boolean_literal(const bool literal=true) const noexcept override;
            /* Integer types */
            virtual std::string type_integer(const bool isUnsigned=false) const noexcept override;
            /* Floating-point type numbers */
            virtual std::string type_floating_point(const bool isPointer=false) const noexcept override;
            /* Return void */
            virtual std::string type_void() const noexcept override;
            /* A type to represent the size of any object in bytes */
            virtual std::string type_size() const noexcept override;
            /* Null pointer */
            virtual std::string null_pointer() const noexcept override;
            /* Shared pointer */
            virtual std::string type_shared_pointer(const std::string& typeObject) const noexcept override;
            /* Dereference a pointer */
            virtual std::string dereference_pointer(const std::string& pointer) const noexcept override;
            /* Return an rvalue object */
            virtual std::string get_rvalue_object(const std::string& object) const noexcept override;
            /* Return a string literal */
            virtual std::string string_literal(const std::string characters) const noexcept override;
            /* Array */
            virtual std::string type_array(const std::string& typeElements,
                                           const unsigned int sizeArray) const noexcept override;
            /* Valarray */
            virtual std::string type_valarray(const std::string& typeElements) const noexcept override;
            /* Vector */
            virtual std::string type_vector(const std::string& typeElements) const noexcept override;
            /* Access a member using the subscript operator */
            virtual std::string subscript_operation(const std::string& operand,
                                                    const unsigned int position=0) const noexcept override;
            virtual std::string subscript_operation(const std::string& operand,
                                                    const std::string& position) const noexcept override;
            virtual std::string subscript_operation(const std::string& operand,
                                                    const std::vector<std::string>& position) const noexcept override;
            /* Vector constructor */
            virtual std::string vector_constructor(const std::string& typeElements,
                                                   const std::vector<std::string>& elements) const noexcept override;
            virtual std::string vector_constructor(const std::vector<unsigned int>& elements) const noexcept override;
            /* Jagged array */
            virtual std::string jagged_array_constructor(const std::vector<std::vector<int>>& jagged_array) const noexcept override;
            /* Make a declaration */
            virtual std::string make_declaration(const std::string& typeName,
                                                 const std::string& objectName,
                                                 const bool isStatement=false,
                                                 const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None,
                                                 const RecurDeclarator declarator=RecurDeclarator::None) const noexcept override;
            /* template-id */
            virtual std::string template_id(const std::string name,
                                            const std::vector<std::string>& parameters=std::vector<std::string>(),
                                            const std::string& indent=std::string()) const noexcept override;
            /* RecurIntegration class */
            virtual std::string recur_integration_type() const noexcept override;
            /* Information of the RecurNode class */
            virtual std::string recur_node_type(const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None) const noexcept override;
            /* Information of the RecurArray class */
            virtual std::string recur_array_type() const noexcept override;
            virtual std::string recur_relation_type(const unsigned int numRelations) const noexcept override;
            virtual void recur_array_begin_loop(const unsigned int position) noexcept override;
            virtual std::string recur_array_node_left(const unsigned int position) const noexcept override;
            virtual std::string recur_array_current_node(const unsigned int position) const noexcept override;
            virtual void recur_array_next_node(const unsigned int position) noexcept override;
            virtual std::string recur_array_get_input(const unsigned int position,
                                                      const std::string& outputIndex) const noexcept override;
            virtual std::string recur_array_input_type() const noexcept override;
            /* Information of the RecurBuffer class */
            virtual std::string recur_buffer_type() const noexcept override;
            virtual std::string recur_buffer_get() const noexcept override;
            /* Information of the RecurIndex class */
            virtual std::string recur_index_type() const noexcept override;
            virtual std::string recur_index_vector() const noexcept override;
            /* Get the type of an LHS node */
            virtual std::string lhs_node_type() const noexcept override;
            /* Call the get_offset() function of an LHS node */
            virtual std::string lhs_get_offset() const noexcept override;
            /* Call the get_order() function of an LHS node */
            virtual std::string lhs_get_order(const std::string& indices,
                                              const unsigned int position) const noexcept override;
            /* Call the get_rhs() function of an LHS node */
            virtual std::string lhs_get_rhs() const noexcept override;
            /* Get the type of RHS nodes */
            virtual std::string rhs_nodes_type() const noexcept override;
            /* Get the size of RHS nodes */
            virtual std::string rhs_nodes_size() const noexcept override;
            /* Call the get_offset() function of a RHS node */
            virtual std::string rhs_get_offset(const std::string& position) const noexcept override;
            /* Write an increment or decrement statement */
            virtual void inc_dec_statement(const RecurIncDec oper, const std::string& var) noexcept override;
            /* Return a parenthesized expression */
            virtual std::string parenthesize_expression(const std::string& expression) const noexcept override;
            /* Write an assignment statement */
            virtual void assignment_statement(const std::string& lhs,
                                              const RecurAssignment oper,
                                              const std::string& rhs,
                                              const bool rvalueRHS=false) noexcept override;
            /* Write a declaration assignment statement */
            virtual void write_declaration_assignment(const std::string& typeName,
                                                      const std::string& objectName,
                                                      const RecurAssignment oper,
                                                      const std::string& rhs,
                                                      const RecurTypeSpecifier declSpecifier=RecurTypeSpecifier::None,
                                                      const RecurDeclarator declarator=RecurDeclarator::None,
                                                      const bool rvalueRHS=false) noexcept override;
            /* Write a comment */
            virtual void write_comment(const std::string& comment) noexcept override;
            /* Include header files */
            virtual void include_header_files(const std::vector<std::string>& headerFiles,
                                              const bool systemHeaders=false) noexcept override;
            /* Begin a namespace */
            virtual void begin_namespace(const std::string& scopeName,
                                         const std::string& description,
                                         const std::vector<std::string>& typeNames,
                                         const bool doQuadrature=false) noexcept override;
            /* End a namespace */
            virtual void end_namespace() noexcept override;
            /* Access a class, object or function in a namespace */
            virtual std::string namespace_member(const std::string& scopeName,
                                                 const std::string& memberName) const noexcept override;
            /* Begin a class with private members and initialization list */
            virtual void begin_class(const std::string& className,
                                     const std::vector<std::string>& tparameters,
                                     const std::vector<std::tuple<std::string,std::string,std::string,bool>>& memberObjects,
                                     const std::string& description,
                                     const std::string& baseClass=std::string()) noexcept override;
            /* End a class */
            virtual void end_class() noexcept override;
            /* Access a class member */
            virtual std::string class_member(const std::string& className,
                                             const std::string& memberName) const noexcept override;
            /* Begin a public member section of a class */
            virtual void begin_public_section() noexcept override;
            /* Begin a protected member section of a class */
            virtual void begin_protected_section() noexcept override;
            /* Begin a private member section of a class */
            virtual void begin_private_section() noexcept override;
            /* End a section of a class */
            virtual void end_class_section() noexcept override;
            /* Declare a function */
            virtual void declare_function(const std::string& functionName,
                                          const std::string& returnType,
                                          const std::vector<std::string>& tparameters,
                                          const std::vector<std::string>& parameters,
                                          const std::string& description,
                                          const RecurFunctionSpecifier specifier=RecurFunctionSpecifier::OverrideFunction) noexcept override;
            /* Begin a function */
            virtual void begin_function(const std::string& functionName,
                                        const std::string& returnType,
                                        const std::vector<std::string>& tparameters,
                                        const std::vector<std::string>& parameters,
                                        const std::string& description,
                                        const RecurFunctionSpecifier specifier=RecurFunctionSpecifier::OverrideFunction) noexcept override;
            /* End a function */
            virtual void end_function() noexcept override;
            /* Call a free function */
            virtual std::string call_function(const std::string& functionName,
                                              const std::vector<std::string>& parameters,
                                              const std::string& indent=std::string(),
                                              const bool isTemplate=false) const noexcept override;
            /* Call a member function */
            virtual std::string call_function(const std::string& operandName,
                                              const std::string& functionName,
                                              const std::vector<std::string>& parameters,
                                              const RecurOperandType operandType=RecurOperandType::ObjectOperand,
                                              const std::string& indent=std::string(),
                                              const bool isTemplate=false) const noexcept override;
            /* Statement of calling a free function */
            virtual void call_function_statement(const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const bool isTemplate=false) noexcept override;
            /* Statement of calling a member function */
            virtual void call_function_statement(const std::string& operandName,
                                                 const std::string& functionName,
                                                 const std::vector<std::string>& parameters,
                                                 const RecurOperandType operandType=RecurOperandType::ObjectOperand,
                                                 const bool isTemplate=false) noexcept override;
            /* Return statement */
            virtual void return_statement(const std::string& expression) noexcept override;
            /* Handle an error */
            virtual void handle_error(const std::vector<std::string>& message) noexcept override;
            /* Handle debug */
            virtual void handle_debug(const std::vector<std::string>& message) noexcept override;
            /* Begin an if statement */
            virtual void begin_if_statement(const std::string& condition,
                                            const bool elifStatement=false) noexcept override;
            /* Begin an else statement */
            virtual void begin_else_statement() noexcept override;
            /* End an if-else statement */
            virtual void end_elif_statement() noexcept override;
            /* Begin a for loop */
            virtual void begin_for_loop(const std::string& typeInitialization,
                                        const std::vector<std::pair<std::string,std::string>>& varInitialization,
                                        const std::string& condition,
                                        const std::vector<std::string>& iteration) noexcept override;
            /* End a for loop */
            virtual void end_for_loop() noexcept override;
            /* Begin a while loop */
            virtual void begin_while_loop(const std::string& condition) noexcept override;
            /* End a while loop */
            virtual void end_while_loop() noexcept override;
            /* Begin a switch statement of order of an index */
            virtual void switch_index_order(const std::string& idxName) noexcept override;
            /* End a switch statement */
            virtual void end_switch() noexcept override;
            /* Begin a case statement of order of an index */
            virtual void case_index_order(const int order) noexcept override;
            /* Begin a default case statement */
            virtual void case_default() noexcept override;
            /* End a case statement */
            virtual void end_case() noexcept override;
            /* Type of functor class */
            virtual std::string type_functor(const std::string returnType,
                                             const std::vector<std::string>& paramTypes) const noexcept override;
            /* Make a call of quadrature method */
            virtual void make_quadrature_call(const std::string quadrature, const std::string integrand) noexcept override;
        private:
            /* Type of real numbers */
            std::string m_real_type;
            /* Header file */
            std::ofstream m_header_file;
            /* Indentation */
            std::string m_header_indent;
            /* #pragma once */
            bool m_pragma_once;
            ///* Whether a namespace begins */
            //bool m_namespace_on;
            /* Whether a class section begins */
            bool m_section_on;
    };
}
