/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of analyzer for recurrence relations.

   2020-09-24, Bin Gao:
   * change class name to RecurParser

   2019-06-26, Bin Gao:
   * moved from RecurCompiler
*/

#pragma once

#include <algorithm>
#include <array>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <utility>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Symbol/RecurDirection.hpp"
#include "tIntegral/Symbol/RecurVector.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Index/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/SymbolVisitor/RecurSymbolVisitor.hpp"

namespace tIntegral
{
    /* Analyzer for recurrence relations */
    class RecurAnalyzer: virtual public RecurSymbolVisitor
    {
        public:
            explicit RecurAnalyzer(const RecurTraversal triangleTraversal) noexcept:
                m_triangle_traversal(triangleTraversal) {}
            virtual ~RecurAnalyzer() noexcept = default;
            /* Assign a new recurrence relation */
            bool assign(const std::shared_ptr<RecurIndex> outputIndex,
                        const std::shared_ptr<RecurSymbol> RHS) noexcept;
            /* FIXME: may be rewritten based on pattern matching */
            virtual bool dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurTerm> term) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurAddition> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurDivision> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurParentheses> operation) noexcept override;
            /* Get the number of RHS terms */
            inline unsigned int get_num_rhs() const noexcept
            {
                return m_rhs_decrements.front().second.size();
            }
            /* Reset directions of RHS terms to RecurDirection::XYZ */
            inline bool reset_rhs_directions() noexcept
            {
                for (auto& idx_directions: m_rhs_directions) {
                    for (auto& rhs_direction: idx_directions) {
                        rhs_direction = RecurDirection::XYZ;
                    }
                }
                return true;
            }
            /* Reset direction(s) of each RHS term that will contribute to the
               given recurrence relation according to a given allowed decrement
               (of components), an index and the triangle traversal */
            inline bool reset_rhs_directions(const std::shared_ptr<RecurIndex>& index,
                                             const RecurVector& decrement) noexcept
            {
                bool invalid_index = true;
                /* Directions of RHS terms contribute after considering the
                   decrement of the given index */
                std::vector<RecurDirection>::iterator rhs_direction_after;
                /* Directions of RHS terms contribute before considering the
                   decrement of the given index */
                std::vector<RecurDirection>::iterator rhs_direction_before;
                std::vector<RecurVector>::const_iterator rhs_decrement, end_decrement;
                auto idx_directions = m_rhs_directions.begin();
                auto idx_decrements = m_rhs_decrements.cbegin();
                /* Try to find the given index */
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(index)) {
                        invalid_index = false;
                        rhs_direction_after = idx_directions->begin();
                        ++idx_directions;
                        rhs_direction_before = idx_directions->begin();
                        rhs_decrement = idx_decrements->second.cbegin();
                        end_decrement = idx_decrements->second.cend();
                        break;
                    }
                    ++idx_directions;
                    ++idx_decrements;
                }
                if (invalid_index) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "RecurAnalyzer::reset_rhs_directions() encounters an invalid index ",
                                            index->to_string(),
                                            ", valid indices include ",
                                            tGlueCore::stringify(m_indices.cbegin(), m_indices.cend()));
                    return false;
                }
                for (; rhs_decrement!=end_decrement; ++rhs_direction_after,++rhs_direction_before,++rhs_decrement) {
                    *rhs_direction_after = *rhs_direction_before;
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "RecurAnalyzer::reset_rhs_directions() is working on index ",
                                            index->to_string(),
                                            " with RHS decrement ",
                                            rhs_decrement->to_string(),
                                            " and direction(s) ",
                                            stringify_direction(*rhs_direction_after));
#endif
                    /* For an index in a RHS term with decrement [i,j,k], transformed
                       decrements along different recurrence relation directions are:

                       # x direction [i,j,k]
                       # y direction [k,i,j]
                       # z direction [j,k,i]

                       For an allowed decrement [l,m,n], it will not contribute if any
                       of its negative components is greater than that of the above
                       (transformed) decrement */
                    for (unsigned int ixyz=0; ixyz<3; ++ixyz) {
                        if (*rhs_direction_after==RecurDirection::Null) break;
                        if (decrement.any_greater(rhs_decrement->get_transformed(m_triangle_traversal[ixyz]))) {
                            remove_direction(*rhs_direction_after, m_triangle_traversal[ixyz]);
                        }
                    }
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "RecurAnalyzer::reset_rhs_directions() gets direction(s) ",
                                            stringify_direction(*rhs_direction_after),
                                            " after removal");
#endif
                }
                return true;
            }
            /* Reset direction(s) of each RHS term that will contribute to the
               given recurrence relation before a given index */
            inline bool reset_rhs_directions(const std::shared_ptr<RecurIndex>& index) noexcept
            {
                bool invalid_index = true;
                /* Directions of RHS terms contribute before considering the given index */
                auto rhs_directions_before = m_rhs_directions.begin();
                /* Try to find the given index and directions before the given index */
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(index)) {
                        invalid_index = false;
                        break;
                    }
                    ++rhs_directions_before;
                }
                if (invalid_index) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "RecurAnalyzer::reset_rhs_directions() encounters an invalid index ",
                                            index->to_string(),
                                            ", valid indices include ",
                                            tGlueCore::stringify(m_indices.cbegin(), m_indices.cend()));
                    return false;
                }
                /* Directions of RHS terms contribute after considering the given index */
                auto rhs_directions_after = rhs_directions_before;
                ++rhs_directions_before;
                rhs_directions_after->assign(rhs_directions_before->cbegin(), rhs_directions_before->cend());
                return true;
            }
            /* Whether the order of an index is needed in the given recurrence relation */
            inline std::array<bool,3> is_order_needed(const std::shared_ptr<RecurIndex>& index,
                                                      const RecurDirection& recurDirection) const noexcept
            {
                auto order_needed = std::array<bool,3>({false, false, false});
                for (auto const& each_index: m_order_indices) {
                    if (index->equal_to(each_index.first)) {
                        if (recurDirection==RecurDirection::XYZ) {
                            order_needed = std::array<bool,3>({true, true, true});
                        }
                        else {
                            for (auto const& each_direction: each_index.second) {
                                order_needed[transform_direction_indexed(each_direction, recurDirection)] = true;
                            }
                        }
                        break;
                    }
                }
                return order_needed;
            }
            /* Get the loop category of an index */
            inline RecurVector get_loop_category(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto idx_decrements = m_rhs_decrements.cbegin();
                /* Try to find the given index */
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(index)) return idx_decrements->first;
                    ++idx_decrements;
                }
                /* Return an invalid vector otherwise */
                return make_invalid_vector();
            }
            /* Get RHS norms (lengths) */
            inline std::vector<std::vector<int>> get_rhs_norms(const bool reverse=false) const noexcept
            {
                if (reverse) {
                    auto rhs_norms = m_rhs_norms;
                    for (auto& idx_norms: rhs_norms) {
                        std::reverse(idx_norms.begin(), idx_norms.end());
                    }
                    return rhs_norms;
                }
                else {
                    return m_rhs_norms;
                }
            }
            /* Get directions of RHS terms along which they will contribute to
               the given recurrence relation */
            inline std::vector<RecurDirection> get_rhs_directions() const noexcept
            {
                /* front() contains directions of RHS terms after considering
                   all indices */
                return m_rhs_directions.front();
            }
            /* Get transformed RHS decrements of an index */
            inline std::vector<std::vector<RecurVector>> get_trans_decrements(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto each_decrements = m_trans_decrements.cbegin();
                /* Try to find the given index */
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(index)) return *each_decrements;
                    ++each_decrements;
                }
                /* Return an empty vector otherwise */
                return std::vector<std::vector<RecurVector>>();
            }
            /* Get variables used in the recurrence relation */
            inline std::set<std::shared_ptr<RecurSymbol>> get_variables() const noexcept
            {
                return m_variables;
            }
            /* Get decrements in RHS terms of the output index */
            inline std::vector<RecurVector> get_output_decrements() const noexcept
            {
                auto idx_decrements = m_rhs_decrements.cbegin();
                /* Try to find the given index */
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(m_output_index)) return idx_decrements->second;
                    ++idx_decrements;
                }
                /* Return an empty vector otherwise */
                return std::vector<RecurVector>();
            }
            /* Get the output index */
            inline std::shared_ptr<RecurIndex> get_output_index() const noexcept
            {
                return m_output_index;
            }
            /* Get the position of output index in recurrence-relation indices */
            inline int get_output_position(const bool reverse=false) const noexcept
            {
                for (int position=0; position<m_indices.size(); ++position) {
                    if (m_indices[position]->equal_to(m_output_index)) {
                        return reverse ? m_indices.size()-1-position : position;
                    }
                }
                return -1;
            }
            /* Get the position of an index in recurrence-relation indices */
            inline int get_index_position(const std::shared_ptr<RecurIndex>& index,
                                          const bool reverse=false) const noexcept
            {
                for (int position=0; position<m_indices.size(); ++position) {
                    if (m_indices[position]->equal_to(index)) {
                        return reverse ? m_indices.size()-1-position : position;
                    }
                }
                return -1;
            }
        protected:
            /* Check if a RHS term is non-zero by verifying its decrement of the output index */
            inline bool is_nnz_output(const RecurVector& decrement) const noexcept
            {
                return decrement.equal_to({-1,0,0}) ||
                       decrement.equal_to({0,-1,0}) ||
                       decrement.equal_to({0,0,-1});
            }
            /* Check if the vector is the decrement of an output index */
            inline bool is_output_decrement(const RecurVector& decrement) const noexcept
            {
                return decrement.is_non_positive() && decrement.get_norm()<0;
            }
            /* Transform RHS decrements according to the triangle traversal
               L (L[0]->L[1]-L[2]) and different directions of the recurrence
               relation.

               For a RHS decrement t, its transformed decrement due to the
               triangle traversal is {t[L[0]], t[L[1]], t[L[2]]}, which will be
               further transformed for different directions of the recurrence
               relation:

               # {t[L[0]], t[L[1]], t[L[2]]} along the x direction
               # {t[L[2]], t[L[0]], t[L[1]]} along the y direction
               # {t[L[1]], t[L[2]], t[L[0]]} along the z direction */
            inline void transform_rhs_decrements() noexcept
            {
                m_trans_decrements.assign(m_indices.size(), std::vector<std::vector<RecurVector>>());
                /* Loop over indices */
                for (unsigned int iidx=0; iidx<m_indices.size(); ++iidx) {
                    /* Skip the output index */
                    if (m_indices[iidx]->equal_to(m_output_index)) continue;
                    /* Loop over the RHS terms for the current index */
                    for (auto const& rhs_decrement: m_rhs_decrements[iidx].second) {
                        /* Get the transformed decrements {t[L[0]],t[L[1]],t[L[2]]} */
                        auto triang_decrement = rhs_decrement.get_transformed(
                            m_triangle_traversal[0], m_triangle_traversal[1]
                        );
                        /* Transform according to different directions */
                        std::vector<RecurVector> trans_decrements;
                        for (unsigned int ixyz=0; ixyz<3; ++ixyz) {
                            trans_decrements.push_back(
                                triang_decrement.get_transformed(m_triangle_traversal[ixyz])
                            );
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "RecurAnalyzer::transform_rhs_decrements() will process ",
                                                    ixyz,
                                                    "-th direction (",
                                                    stringify_direction(m_triangle_traversal[ixyz]),
                                                    ") of ",
                                                    iidx,
                                                    "-th index with the transformed decrement ",
                                                    trans_decrements[ixyz].to_string(),
                                                    " and the original RHS decrement ",
                                                    rhs_decrement.to_string());
#endif
                        }
                        /* Store transformed RHS decrements of the current index */
                        m_trans_decrements[iidx].push_back(trans_decrements);
                    }
                }
            }
        private:
            /* Triangle traversal */
            RecurTraversal m_triangle_traversal;
            /* Ouput index of the given recurrence relation */
            std::shared_ptr<RecurIndex> m_output_index;
            /* Variables used in the recurrence relation */
            std::set<std::shared_ptr<RecurSymbol>> m_variables;
            /* Indices whose orders are needed in the given recurrence relation */
            std::vector<std::pair<std::shared_ptr<RecurIndex>,std::set<RecurDirection>>> m_order_indices;
            /* Indices involved in the given recurrence relation */
            std::vector<std::shared_ptr<RecurIndex>> m_indices;
            /* If a non-zero RHS term found in the recurrence relation, which
               should have the decrement of the output index as

               # {-1,0,0},
               # {0,-1,0}, or
               # {0,0,-1},

               and increments of other indices >= 0 */
            bool m_nnz_rhs;
            /* Norms of index decrements of all RHS terms */
            std::vector<std::vector<int>> m_rhs_norms;
            /* Information extracted from RHS terms of the recurrence relation
               for each index, including

               # loop category of an index (as a prioritized vector of all its
                 decrements and increments in the RHS terms), and
               # all decrements or increments of the index in the RHS terms */
            std::vector<std::pair<RecurVector,std::vector<RecurVector>>> m_rhs_decrements;
            /* Transformed RHS decrements of non-output indices, the innermost
               vector contains transformed RHS decrements along different
               directions, and the second-innermost vector contains transformed
               RHS decrements of an index */
            std::vector<std::vector<std::vector<RecurVector>>> m_trans_decrements;
            /* Which directions of a RHS term contributes to the given
               recurrence relation resulted from the decrement of an index */
            std::vector<std::vector<RecurDirection>> m_rhs_directions;
    };
}
