/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for processing variables in recurrence relations.

   2020-03-19, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <iterator>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/SymbolVisitor/RecurSymbolVisitor.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"

namespace tIntegral
{
    /* @class:RecurVarProcessor[Recurrence relation variable processor class.] */
    //FIXME: we may remove RecurVarProcessor by using the pattern matching
    class RecurVarProcessor: virtual public RecurSymbolVisitor
    {
        public:
            /* @fn:RecurVarProcessor()[Constructor.] */
            explicit RecurVarProcessor(const std::shared_ptr<RecurTranslator> translator) noexcept:
                m_translator(translator) {}
            /* @fn:~RecurVarProcessor()[Deconstructor.] */
            virtual ~RecurVarProcessor() noexcept = default;
            /* Set the recurrence relation translator */
            inline void set_translator(const std::shared_ptr<RecurTranslator> translator) noexcept
            {
                m_translator = translator;
            }
            /* Generate code to process variables in a recurrence relation */
            bool build(const std::vector<std::shared_ptr<RecurIntegrand>> integrand,
                       const std::set<std::shared_ptr<RecurSymbol>>& variables) noexcept;
            /* FIXME: may be rewritten based on pattern matching */
            virtual bool dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept override;
            /* @fn:dispatch[Dispatch `RecurTerm` class.]
               @param[in]:recurTerm[The recurrence relation term.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<RecurTerm> term) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurAddition> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurDivision> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurParentheses> operation) noexcept override;
        protected:
            /* Find all variables from dependency of a given variable */
            inline bool find_all_variables(const std::shared_ptr<RecurSymbol>& variable) noexcept
            {
                /* Check if the variable has already been found */
                if (std::find(m_all_variables.begin(), m_all_variables.end(), variable)==m_all_variables.end()) {
                    auto var_dependency = variable->get_dependency();
                    if (var_dependency.empty()) {
                        m_all_variables.push_back(variable);
                    }
                    /* The new variable has dependency that will be processed first */
                    else {
                        m_cyclic_variables.insert(variable);
                        for (auto each_dep=var_dependency.cbegin(); each_dep!=var_dependency.cend(); ++each_dep) {
                            /* Cyclic dependency encountered */
                            if (m_cyclic_variables.find(*each_dep)!=m_cyclic_variables.end()) {
                                Settings::logger->write(
                                    tGlueCore::MessageType::Error,
                                    "RecurVarProcessor::find_all_variables() encountered cyclic dependency when processing ",
                                    std::distance(var_dependency.cbegin(), each_dep),
                                    "-th dependency ",
                                    (*each_dep)->to_string(),
                                    " of ",
                                    variable->to_string(),
                                    ", and all variables found are ",
                                    tGlueCore::stringify(m_cyclic_variables.cbegin(), m_cyclic_variables.cend())
                                );
                                return false;
                            }
                            /* Process a dependency */
                            if (!find_all_variables(*each_dep)) {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "RecurVarProcessor::find_all_variables() failed to process ",
                                                        std::distance(var_dependency.cbegin(), each_dep),
                                                        "-th dependency of ",
                                                        variable->to_string());
                                return false;
                            }
                        }
                        m_all_variables.push_back(variable);
                        /* The new variable and its dependency have been
                           processed so that it can be removed from the set of
                           cyclic dependency */
                        m_cyclic_variables.erase(variable);
                    }
                }
                return true;
            }
            /* Find names of integrand and dependency for a given variable */
            inline bool find_integrand_parameters(const std::shared_ptr<RecurVariable>& variable) noexcept
            {
                /* Get the name of integrand */
                auto var_integrand = variable->get_integrand();
                auto integrand_name = var_integrand->get_name();
                for (auto const& each_fun: m_integrand) {
                    /* Class member's name may be changed */
                    if (var_integrand==each_fun) {
                        integrand_name = m_translator->member_object(integrand_name);
                        break;
                    }
                }
                m_integrand_names.push_back(std::move(integrand_name));
                /* Process variable dependency */
                m_method_parameters.push_back(std::vector<std::string>());
                auto var_dependency = variable->get_dependency();
                for (auto const& each_dep: var_dependency) {
                    /* Check whether the dependency is an integrand */
                    bool is_integrand = false;
                    for (auto const& each_fun: m_integrand) {
                        if (each_dep==each_fun) {
                            is_integrand = true;
                            break;
                        }
                    }
                    if (is_integrand) {
                        m_method_parameters.back().push_back(m_translator->member_object(each_dep->get_name()));
                    }
                    else {
                        /* Invoke callee function to get the dependency */
                        if (m_declared_variables.find(each_dep)==m_declared_variables.end()) {
                            if (!each_dep->accept(this)) {
                                Settings::logger->write(
                                    tGlueCore::MessageType::Error,
                                    "RecurVarProcessor::find_integrand_parameters() failed to process the dependency of variable ",
                                    variable->to_string()
                                );
                                return false;
                            }
                        }
                        /* Simply use the name of already declared variable */
                        else {
                            m_method_parameters.back().push_back(each_dep->get_name());
                        }
                    }
                }
                return true;
            }
            /* Dispatch RecurVariable derived classes */
            inline bool dispatch_variable(const std::shared_ptr<RecurVariable>& variable) noexcept
            {
                if (m_rhs_begun) {
                    if (find_integrand_parameters(variable)) {
                        auto fun_string = m_translator->call_function(m_integrand_names.back(),
                                                                      variable->get_integrand_method(),
                                                                      m_method_parameters.back(),
                                                                      RecurOperandType::PointerOperand);
                        /* Remove the name of integrand and parameters that the
                           current variable depends on */
                        m_integrand_names.pop_back();
                        m_method_parameters.pop_back();
                        /* Add the callee function of getting the current varaible */
                        m_method_parameters.back().push_back(std::move(fun_string));
                        return true;
                    }
                    else {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "RecurVarProcessor::dispatch_variable() failed to dispatch a RHS variable ",
                            variable->to_string()
                        );
                        return false;
                    }
                }
                else {
                    m_rhs_begun = true;
                    m_integrand_names.clear();
                    m_method_parameters.clear();
                    if (find_integrand_parameters(variable)) {
                        m_rhs_begun = false;
                        return true;
                    }
                    else {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "RecurVarProcessor::dispatch_variable() failed to dispatch a LHS variable ",
                            variable->to_string()
                        );
                        return false;
                    }
                }
            }
        private:
            /* Recurrence relation translator */
            std::shared_ptr<RecurTranslator> m_translator;
            /* Integrand */
            std::vector<std::shared_ptr<RecurIntegrand>> m_integrand;
            /* All sorted variables including their dependency that each
               variable is after all its dependency */
            std::vector<std::shared_ptr<RecurSymbol>> m_all_variables;
            /* Used to check cyclic dependency */
            std::set<std::shared_ptr<RecurSymbol>> m_cyclic_variables;
            /* Variables already declared */
            std::set<std::shared_ptr<RecurSymbol>> m_declared_variables;
            /* Name of integrand that a variable depends on */
            std::vector<std::string> m_integrand_names;
            /* Parameters that a variable's method needed */
            std::vector<std::vector<std::string>> m_method_parameters;
            /* Indicates if the right hand side of an assignment begins */
            bool m_rhs_begun;
    };
}
