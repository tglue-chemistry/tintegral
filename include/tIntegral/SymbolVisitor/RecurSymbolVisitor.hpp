/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for recurrence relation symbols vistor.

   2020-10-23, Bin Gao:
   * change class name RecurExprVisitor to RecurSymbolVisitor

   2020-02-26, Bin Gao:
   * moved from the RecurSymbol header file
*/

#pragma once

#include <memory>

#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Symbol/RecurBasisFunction.hpp"
#include "tIntegral/Symbol/RecurOperator.hpp"

namespace tIntegral
{
    /* @class:RecurSymbolVisitor[Symbol visitor class.] */
    //FIXME: we may remove RecurSymbolVisitor by using the pattern matching
    class RecurSymbolVisitor
    {
        public:
            /* @fn:RecurSymbolVisitor()[Constructor.] */
            explicit RecurSymbolVisitor() noexcept = default;
            /* @fn:~RecurSymbolVisitor()[Deconstructor.] */
            virtual ~RecurSymbolVisitor() noexcept = default;
            virtual bool dispatch(std::shared_ptr<RecurNumber> number) noexcept
            {
                return true;
            }
            /* @fn:dispatch[Dispatch `RecurScalarVar` class.]
               @param[in]:variable[The scalar variable of a function.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept = 0;
            /* @fn:dispatch[Dispatch `RecurTerm` class.]
               @param[in]:recurTerm[The recurrence relation term.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<RecurTerm> term) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurAddition> operation) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurDivision> operation) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<RecurParentheses> operation) noexcept = 0;
            /* Usually RecurIntegrand derived classes are not treated */
            virtual bool dispatch(std::shared_ptr<RecurGaussianFunction> function) noexcept
            {
                return true;
            }
            virtual bool dispatch(std::shared_ptr<RecurCartMultMoment> function) noexcept
            {
                return true;
            }
            virtual bool dispatch(std::shared_ptr<RecurECPUnprojected> function) noexcept
            {
                return true;
            }
            virtual bool dispatch(std::shared_ptr<RecurECPProjected> function) noexcept
            {
                return true;
            }
    };
}
