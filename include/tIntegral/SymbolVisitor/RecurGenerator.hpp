/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation generator.

   2019-05-29, Bin Gao:
   * first version
*/

#pragma once

#include <array>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Symbol/RecurDirection.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Index/RecurIndex.hpp"
#include "tIntegral/SymbolVisitor/RecurSymbolVisitor.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"

//Interpreter, visitor (generate c++, GPU, validify & optimization, visualization, etc.), flyweight, template method

namespace tIntegral
{
    class RecurGenerator: public RecurSymbolVisitor
    {
        public:
            explicit RecurGenerator(const std::shared_ptr<RecurTranslator> translator) noexcept:
                m_translator(translator),
                m_iter_vector("ival") {}
            virtual ~RecurGenerator() noexcept = default;
            /* Set the recurrence relation translator */
            inline void set_translator(const std::shared_ptr<RecurTranslator> translator) noexcept
            {
                m_translator = translator;
            }
            /* Generate code to perform a recurrence relation with given
               direction of the recurrence relation, right-hand-side
               expression, and contributing directions of RHS terms */
            std::vector<unsigned int>
            build(const std::string& nameFunction,
                  const RecurDirection recurDirection,
                  const std::shared_ptr<RecurSymbol> RHS,
                  const std::vector<RecurDirection> rhsDirections,
                  const std::vector<std::pair<std::shared_ptr<RecurIndex>,std::array<bool,3>>> zeroOrders,
                  const bool vectorWise=true) noexcept;
            /* FIXME: may be rewritten based on pattern matching */
            virtual bool dispatch(std::shared_ptr<RecurNumber> number) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurTerm> term) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurAddition> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurDivision> operation) noexcept override;
            virtual bool dispatch(std::shared_ptr<RecurParentheses> operation) noexcept override;
        protected:
            /* Dispatch arithmetic operation +, -, * and / */
            inline bool dispatch_arithmetic_operation(const std::shared_ptr<RecurArithOperation>& operation,
                                                      const RecurArithmetic& oper) noexcept
            {
                auto higher_precedence = oper==RecurArithmetic::Multiplication || oper==RecurArithmetic::Division
                                       ? true
                                       : false;
                if (m_rhs_contribut) {
                    std::string saved_rhs_string;
                    saved_rhs_string.swap(m_rhs_string);
                    /* Get the LHS */
                    if (operation->get_lhs()->accept(this)) {
                        auto oper_lhs_contribut = m_rhs_contribut;
                        if (oper_lhs_contribut || !higher_precedence) {
                            std::string str_oper_lhs;
                            if (oper_lhs_contribut) {
                                str_oper_lhs.swap(m_rhs_string);
                            }
                            /* For addition and subtraction, we still need to consider their RHS */
                            else {
                                m_rhs_string.clear();
                            }
                            m_rhs_contribut = true;
                            /* Get the RHS */
                            if (operation->get_rhs()->accept(this)) {
                                if (m_rhs_contribut) {
                                    std::string str_oper_rhs;
                                    str_oper_rhs.swap(m_rhs_string);
                                    if (oper_lhs_contribut) {
                                        m_rhs_string = saved_rhs_string
                                                     + m_translator->arithmetic_operation(str_oper_lhs,
                                                                                          oper,
                                                                                          str_oper_rhs);
                                    }
                                    /* For addition and subtraction with non-contributing LHS */
                                    else {
                                        if (oper==RecurArithmetic::Addition) {
                                            m_rhs_string = saved_rhs_string+str_oper_rhs;
                                        }
                                        else {
                                            m_rhs_string = saved_rhs_string
                                                         + m_translator->parenthesize_expression(
                                                               m_translator->arithmetic_operation(
                                                                   RecurArithmetic::Negation,
                                                                   str_oper_rhs
                                                               )
                                                           );
                                        }
                                    }
                                }
                                else {
                                    switch (oper) {
                                        case RecurArithmetic::Addition:
                                        case RecurArithmetic::Subtraction:
                                            m_rhs_string = saved_rhs_string+str_oper_lhs;
                                            /* Reset to true if the LHS of
                                               addition or subtraction
                                               contributes */
                                            m_rhs_contribut = oper_lhs_contribut;
                                            break;
                                        case RecurArithmetic::Multiplication:
                                            /* The multiplication will not
                                               contribute if the multiplicand
                                               is not a contributed term */
                                            m_rhs_string.swap(saved_rhs_string);
                                            break;
                                        case RecurArithmetic::Division:
                                            /* The division is invalid if the
                                               divisor is not a contributed term */
                                            Settings::logger->write(
                                                tGlueCore::MessageType::Error,
                                                "RecurGenerator::dispatch_arithmetic_operation() has an invalid divisor"
                                            );
                                            return false;
                                        default:
                                            Settings::logger->write(
                                                tGlueCore::MessageType::Error,
                                                "RecurGenerator::dispatch_arithmetic_operation() is given an invalid operation ",
                                                stringify_recur_arithmetic(oper)
                                            );
                                            return false;
                                    }
                                }
                            }
                            else {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "RecurGenerator::dispatch_arithmetic_operation() called for RHS ",
                                                        operation->get_rhs()->to_string(),
                                                        " of arithmetic operation ",
                                                        stringify_recur_arithmetic(oper));
                                return false;
                            }
                        }
                        /* The multiplication or division will not contribute
                           if its LHS is not a contributing term, but we still
                           need to visit them */
                        else {
                            m_rhs_string.swap(saved_rhs_string);
                            if (!operation->get_rhs()->accept(this)) {
                                Settings::logger->write(
                                    tGlueCore::MessageType::Error,
                                    "RecurGenerator::dispatch_arithmetic_operation() called for RHS ",
                                    operation->get_rhs()->to_string(),
                                    " of arithmetic operation ",
                                    stringify_recur_arithmetic(oper)
                                );
                                return false;
                            }
                        }
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "RecurGenerator::dispatch_arithmetic_operation() called for LHS ",
                                                operation->get_lhs()->to_string(),
                                                " of arithmetic operation ",
                                                stringify_recur_arithmetic(oper));
                        return false;
                    }
                }
                /* For non-contributing symbols, we only visit them but do not
                   generate codes */
                else {
                    if (!operation->get_lhs()->accept(this)) {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "RecurGenerator::dispatch_arithmetic_operation() called for LHS ",
                                                operation->get_lhs()->to_string(),
                                                " of arithmetic operation ",
                                                stringify_recur_arithmetic(oper));
                        return false;
                    }
                    if (!operation->get_rhs()->accept(this)) {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "RecurGenerator::dispatch_arithmetic_operation() called for RHS ",
                                                operation->get_rhs()->to_string(),
                                                " of arithmetic operation ",
                                                stringify_recur_arithmetic(oper));
                        return false;
                    }
                }
                return true;
            }
        private:
            /* Recurrence relation translator */
            std::shared_ptr<RecurTranslator> m_translator;
            /* Whether generated codes for vector variables are vectorwise */
            bool m_vector_wise;
            /* Iterator of vector variables for non-vectorwise codes */
            std::string m_iter_vector;
            /* Direction of the recurrence relation */
            RecurDirection m_recur_direction;
            /* Contributing directions of RHS terms */
            std::vector<RecurDirection> m_rhs_directions;
            /* Which RHS term */
            unsigned int m_which_rhs;
            /* Whether indices with necessarily zeroth orders along X, Y and Z directions */
            std::vector<std::pair<std::shared_ptr<RecurIndex>,std::array<bool,3>>> m_zero_orders;
            /* String of right hand side of a given recurrence relation and
               direction */
            std::string m_rhs_string;
            /* Indicate if a RHS symbol contributes or not */
            bool m_rhs_contribut;
            /* Positions of contributing RHS terms */
            std::vector<unsigned int> m_rhs_positions;
    };
}
