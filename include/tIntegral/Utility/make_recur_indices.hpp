/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file contains functions creating recurrence-relation indices.

   2020-07-04, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"

namespace tIntegral
{
    /* . angular momentum on the bra center,
       . angular momentum on the ket center,
       . multipole moments around the origin of London phase factor on the bra center,
       . multipole moments around the origin of London phase factor on the ket center,
       . multipole moments around the origin of London phase factor on the operator center(s),
       . geometrical derivatives on the bra center,
       . geometrical derivatives on the ket center,
       . geometrical derivatives on the operator center(s),
       . magnetic derivatives on the bra center,
       . magnetic derivatives on the ket center,
       . magnetic derivatives on the operator center(s),
       . derivatives with respect to total rotational angular momentum on the bra center,
       . derivatives with respect to total rotational angular momentum on the ket center,
       . derivatives with respect to total rotational angular momentum on the operator center(s)
     */
    //FIXME: probably reordering
    enum class OneElecIndex
    {
        BraAngular,
        KetAngular,
        BraLondon,
        KetLondon,
        OperLondon,
        BraGeometrical,
        KetGeometrical,
        OperGeometrical,
        BraMagnetic,
        KetMagnetic,
        OperMagnetic,
        BraRotational,
        KetRotational,
        OperRotational,
        MultipoleMoment,
        ElecDerivative,
        BoysFunction,
        RadialPower,
        BesselBra,
        BesselKet,
        HarmonicBra,
        HarmonicKet
    };

    /* Create a map of recurrence-relation indices according to their positions
       in enumeration OneElecIndex */
    inline std::map<OneElecIndex,std::shared_ptr<RecurIndex>>
    make_recur_indices(std::vector<std::pair<OneElecIndex,std::string>> oneIndices,
                       const unsigned int offset=0) noexcept
    {
        //FIXME: sort or not??
        //std::sort(oneIndices.begin(),
        //          oneIndices.end(),
        //          [&](std::pair<OneElecIndex,std::string> left, std::pair<OneElecIndex,std::string> right)
        //          { return left.first<right.first; });
        std::map<OneElecIndex,std::shared_ptr<RecurIndex>> recur_indices;
        for (unsigned int idx=0; idx<oneIndices.size(); ++idx) {
            if (oneIndices[idx].first==OneElecIndex::BoysFunction ||
                oneIndices[idx].first==OneElecIndex::RadialPower ||
                oneIndices[idx].first==OneElecIndex::BesselBra ||
                oneIndices[idx].first==OneElecIndex::BesselKet) {
                recur_indices.emplace(
                    oneIndices[idx].first,
                    std::make_shared<RecurScalarIndex>(oneIndices[idx].second, idx+offset, RecurVector(0))
                );
            }
            else {
                recur_indices.emplace(
                    oneIndices[idx].first,
                    std::make_shared<RecurTriangularIndex>(oneIndices[idx].second, idx+offset, RecurVector(0))
                );
            }
        }
        return recur_indices;
    }

    //inline std::vector<std::shared_ptr<RecurIndex>>
}
