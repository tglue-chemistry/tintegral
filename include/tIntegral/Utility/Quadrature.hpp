/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file performs numerical integration of one-dimensional functions.

   2020-08-21, Bin Gao:
   * first version
*/

#pragma once

#include <cmath>
#include <functional>
#include <limits>
#if defined(TINTEGRAL_VALARRAY_BUFFER)
#include <valarray>
#endif

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurArray.hpp"

namespace tIntegral
{
    /* Adaptive Gauss-Chebyshev quadrature of the second kind for interval stem:[[0, \infty)]
       References Comput. Phys. Commun. 70, 271-284 (1992); J. Comput. Chem. 27, 1009-1019 (2006) */
    template<typename RealType=double> class AdaptGCQInf
    {
        public:
            explicit AdaptGCQInf(
                const unsigned int maxNumPoints=801,
                const RealType tolerance=RealType(1000)*std::numeric_limits<RealType>::epsilon()) noexcept:
                m_max_npoints((maxNumPoints-1)/2),
                m_tolerance(tolerance) {}
            ~AdaptGCQInf() noexcept = default;
            /**/
            inline void set_max_npoints(const unsigned int maxNumPoints) noexcept
            {
                m_max_npoints = (maxNumPoints-1)/2;
            }
            /**/
            inline void set_tolerance(const RealType tolerance) noexcept
            {
                m_tolerance = tolerance;
            }
            /**/
            bool operator()(RecurArray<RealType>& outputArray, std::function<void(const RealType)> f) const noexcept
            {
                /* Initialization */
                unsigned int npoint = 1;
                RealType S0 = 1;
                RealType C0 = 0;
                /* Compute the function at 0 */
                f(RealType(1));
                /* stem:[T_{2n+1}] in Comput. Phys. Commun. 70, 271-284 (1992) */
                auto T_max = outputArray.copy_output_buffer();
                if (T_max.empty()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::AdaptGCQInf() failed to allocate memory for 2n+1 quadrature"
                    );
                    return false;
                }
                /* stem:[T_{n}] in Comput. Phys. Commun. 70, 271-284 (1992) */
                auto T_n = T_max.copy_buffer();
                if (T_n.empty()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::AdaptGCQInf() failed to allocate memory for n quadrature"
                    );
                    return false;
                }
                /* stem:[T_{(n-1)/2}] in Comput. Phys. Commun. 70, 271-284 (1992) */
                auto T_min = RecurBuffer<RealType>(T_max.get_size_buffer(), T_max.get_size_element());
                if (!T_min.allocate()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::AdaptGCQInf() failed to allocate memory for (n-1)/2 quadrature"
                    );
                    return false;
                }
                /* Compute the stem:[2`npoint`+1] points quadrature */
                bool is_converged = false;
                do {
                    /* Update quantities, see Comput. Phys. Commun. 70, 271-284 (1992) */
                    for (unsigned int ielm=0; ielm<T_n.size(); ++ielm) {
                        T_min[ielm] = T_n[ielm]+Tn[ielm];
                        T_n[ielm] = T_max[ielm]+T_max[ielm];
                    }
                    RealType C1 = C0;
                    RealType S1 = S0;
                    C0 = std::sqrt((1+C1)*0.5);
                    S0 = S1/(C0+C0);
                    RealType sin_i = S0;
                    RealType cos_i = C0;
                    /* Loop over quadrature points */
                    for (unsigned int ipoint=1; ipoint<=npoint; ipoint+=2) {
                        /* stem:[\sin^{4}(\frac{(2i+1)\pi}{2(n+1)})] */
                        RealType sin_i_fourth = std::pow(sin_i, RealType(4));
                        /* Compute the new abscissa */
                        RealType abscissa = 1+RealType(0.21220659078919378103)*sin_i*cos_i*(3+2*sin_i*sin_i)
                                          - RealType(ipoint)/RealType(npoint+1);
                        /* Convert the new abscissa using logarithmic transformation */
                        RealType absc_converted = RealType(1.44269504088896340736)
                                                * std::log1p(RealType(2)/(RealType(1)-abscissa));
                        /* Compute the function at the converted new abscissa */
                        f(absc_converted);
                        outputArray.axpy_output_buffer(sin_i_fourth, T_max);
                        /* Convert the negative of the new abscissa using logarithmic transformation */
                        RealType absc_converted = RealType(1.44269504088896340736)
                                                * std::log1p(RealType(2)/(RealType(1)+abscissa));
                        /* Compute the function at the converted negative of the new abscissa */
                        f(absc_converted);
                        outputArray.axpy_output_buffer(sin_i_fourth, T_max);
                        sin_i_fourth = sin_i;
                        sin_i = sin_i*C1+cos_i*S1;
                        cos_i = cos_i*C1-sin_i_fourth*S1;
                    }
                    /* Check the convergence */
                    is_converged = true;
                    RealType scaled_tolerance = RealType(3*(npoint+1))*tolerance;
                    for (unsigned int ielm=0; ielm<T_n.size(); ++ielm) {
#if defined(TINTEGRAL_VALARRAY_BUFFER)
                        for (unsigned int ival=0; ival<T_n.get_size_element(); ++ival) {
                            auto diff_max_n = T_max[ielm][ival]-T_n[ielm][ival];
                            is_converged = RealType(16)*diff_max_n*diff_max_n
                                         > scaled_tolerance*std::abs(T_max[ielm][ival]-T_min[ielm][ival]);
                            if (!is_converged) break;
                        }
#else
                        auto diff_max_n = T_max[ielm]-T_n[ielm];
                        is_converged = RealType(16)*diff_max_n*diff_max_n
                                     > scaled_tolerance*std::abs(T_max[ielm]-T_min[ielm]);
#endif
                        if (!is_converged) break;
                    }
                    if (is_converged) break;
                    /* Replace `npoint` by stem:[2`npoint`+1] */
                    npoint = npoint+npoint+1;
                } while (npoint<=m_max_npoints);
                /* Compute the final integrals */
                RealType weight_factor = RealType(16)/RealType(3*(npoint+1));
                outputArray.set_output_buffer(weight_factor, T_max);
                return is_converged;
            }
        private:
            unsigned int m_max_npoints;
            RealType m_tolerance;
    };
}
