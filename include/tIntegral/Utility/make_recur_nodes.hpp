/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file contains functions creating recurrence-relation nodes.

   2019-09-26, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/IndexOrder.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurNode.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"

namespace tIntegral
{
    /* Construct recurrence relation nodes from given orders of indices, which
       can be sent to different integration classes as their output nodes */
    template<typename RealType=double>
    inline std::vector<std::vector<std::shared_ptr<RecurNode>>>
    make_recur_nodes(const std::vector<std::shared_ptr<RecurIndex>>& allIndices,
                     const unsigned int outputIdx,
                     const std::vector<std::vector<unsigned int>>& idxOrders,
                     const std::shared_ptr<RecurBuffer<RealType>>& buffer) noexcept
    {
        std::vector<std::vector<std::shared_ptr<RecurNode>>> nodes;
        /* Get the maximum order of the output index */
        unsigned int max_output_order = 0;
        for (auto const& node_orders: idxOrders) {
            if (max_output_order<node_orders[outputIdx]) max_output_order = node_orders[outputIdx];
        }
        nodes.resize(max_output_order+1);
        /* Put nodes into appropriate levels */
        std::vector<std::size_t> offset_node(1, 0);
        std::vector<std::shared_ptr<std::size_t>> base_level(1, std::make_shared<std::size_t>(0));
        for (auto const& node_orders: idxOrders) {
            IndexOrder converted_orders;
            if (converted_orders.template assign(allIndices.size(), node_orders.cbegin())) {
                /* Strides and stepsizes of the node will be computed when the
                   output nodes are sent to different integration classes, so
                   that we only need to construct the node */
                nodes[node_orders[outputIdx]].push_back(std::make_shared<RecurNode>(
                    converted_orders, base_level[0], offset_node[0]
                ));
                /* Compute the size of the current node */
                std::size_t size_node = 1;
                for (unsigned int position=0; position<allIndices.size(); ++position) {
                    size_node *= allIndices[position]->get_num_components(node_orders[position]);
                }
                /* Update the offet of nodes */
                offset_node[0] += size_node;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "make_recur_nodes() could not convert the orders ",
                                        tGlueCore::stringify(node_orders.cbegin(), node_orders.cend()));
                return std::vector<std::vector<std::shared_ptr<RecurNode>>>();
            }
        }
        /* Set base address of the level */
        if (buffer->template set_offsets(1,
                                         std::vector<std::pair<std::size_t,std::size_t>>(),
                                         1,
                                         offset_node.begin(),
                                         base_level.begin())) {
            return nodes;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "make_recur_nodes() could not set base address of the level");
            return std::vector<std::vector<std::shared_ptr<RecurNode>>>();
        }
    }

    ///* Create recurrence relation nodes by transferring the order of the first
    //   index to the second of given output nodes */
    //template<typename RealType=double>
    //inline std::vector<std::vector<std::shared_ptr<RecurNode>>>
    //make_recur_nodes(const std::vector<std::shared_ptr<RecurIndex>>& indices,
    //                 const std::shared_ptr<RecurIndex>& firstIndex,
    //                 const std::shared_ptr<RecurIndex>& secondIndex,
    //                 const std::vector<std::shared_ptr<RecurNode>>& outputNodes,
    //                 const std::shared_ptr<RecurBuffer<RealType>> buffer) noexcept
    //{
    //    std::vector<std::vector<std::shared_ptr<RecurNode>>> nodes;
    //    recurNodes.reserve(outputNodes.size());
    //    std::vector<std::size_t> offset_node(1, 0);
    //    std::vector<std::shared_ptr<std::size_t>> base_level(1, std::make_shared<std::size_t>(0));
    //    for (auto const& output_node: outputNodes) {
    //        auto transferred_node = RecurNode(output_node, firstIndex,
    //                                                            secondIndex,
    //                                                            base_level[0],
    //                                                            offset_node[0]);
    //        if (transferred_node) {
    //            recurNodes.push_back(std::make_shared<RecurNode>(std::move(transferred_node)));
    //            /* Update the offet of nodes */
    //            offset_node[0] += recurNodes.back()->get_size();
    //        }
    //        else {
    //            Settings::logger->write(tGlueCore::MessageType::Error,
    //                                    "make_recur_nodes() could not transfer the order of the index ",
    //                                    firstIndex.to_string(),
    //                                    " to the index ",
    //                                    secondIndex.to_string(),
    //                                    " for the node "
    //                                    output_node->to_string());
    //            return false;
    //        }
    //    }
    //    /* Set base address of the level */
    //    buffer->template set_offsets(1, true, 1, offset_node.begin(), base_level.begin());
    //    return true;
    //}
}
