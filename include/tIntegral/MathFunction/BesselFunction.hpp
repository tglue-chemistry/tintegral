/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates different Bessel functions.

   2020-08-16, Bin Gao:
   * first version
*/

#pragma once

#include <cmath>
#include <limits>
#include <valarray>

#include "tGlueCore/Logger.hpp"
#ifdef TINTEGRAL_DEBUG
#include "tGlueCore/Stringify.hpp"
#endif

#include "tIntegral/Settings.hpp"

namespace MathFunction
{
    //FIXME: try recurrence relations
    /* Exponentially weighted modified spherical Bessel function of the first
       kind stem:[i_{n}(z)\mathrm{e}^{-z}], reference: J. Comput. Chem. 27, 1009-1019 (2005). */
    template<typename RealType> std::valarray<std::valarray<RealType>> spherical_ine_v(
        const unsigned int maxOrder,
        const std::valarray<RealType> arguments,
        const RealType tolerance=std::numeric_limits<RealType>::epsilon()) noexcept
    {
#ifdef TINTEGRAL_DEBUG
        tIntegral::Settings::logger->write(tGlueCore::MessageType::Debug,
                                           "spherical_ine_v() will compute up to order ",
                                           maxOrder,
                                           " with the arguments ",
                                           tGlueCore::stringify(std::begin(arguments), std::end(arguments)));
#endif
        std::valarray<std::valarray<RealType>> results(std::valarray<RealType>(arguments.size()), maxOrder+1);
        for (unsigned int order=0; order<=maxOrder; ++order) {
            if (arguments>16) {
                /* stem:[i_{n}(z)\mathrm{e}^{-z}\approx-\sum_{k=0}^{n}\frac{(n+k)!}{k!(n-k)!(-2z)^{k+1}}]
                   stem:[-\frac{(n+k+1)!}{(k+1)!(n-k-1)!(-2z)^{k+2}}
                       =-\frac{(n+k+1)(n-k)}{(k+1)(-2z)}\frac{(n+k)!}{k!(n-k)!(-2z)^{k+1}}
                   ]
                   stem:[-\frac{(n+0)!}{0!(n-0)!(-2z)^{0+1}}=\frac{1}{2z}] for all different stem:[n] */
                RealType neg_inv2_arg = RealType(1)/(arguments+arguments);
                std::valarray<RealType> results(neg_inv2_arg, maxOrder+1);
                std::valarray<RealType> expn_term(neg_inv2_arg, maxOrder+1);
                neg_inv2_arg = -neg_inv2_arg;
                for (unsigned int k=1; k<=maxOrder; ++k) {
                    for (unsigned int order=k; order<=maxOrder; ++order) {
                        expn_term[order] *= RealType(order+k)*RealType(order-k+1)*neg_inv2_arg/RealType(k);
                        results[order] += expn_term[order];
                    }
                }
            }
            else if (arguments<0.0000001) {
                /* stem:[i_{n}(z)\mathrm{e}^{-z}
                       \apporx \frac{(1-z)z^{n}}{(2n+1)!!}
                       = \frac{z}{2n+1}\frac{(1-z)z^{n-1}}{(2n-1)!!}
                       = \frac{z}{2n+1}i_{n-1}(z)\mathrm{e}^{-z}
                   ],
                   with stem:[i_{0}(z)\mathrm{e}^{-z}\approx 1-z]. */
                std::valarray<RealType> results(maxOrder+1);
                results[0] = RealType(1)-arguments;
                for (unsigned int order=0; order<maxOrder; ++order) {
                    results[order+1] = arguments*results[order]/RealType(order+order+3);
                }
            }
            else {
                /* stem:[i_{n}(z)\mathrm{e}^{-z}=z^{n}\mathrm{e}^{-z}\sum_{k=0}\frac{(z^{2}/2)^{k}}{k!(2k+2n+1)!!}]
                   stem:[\frac{(z^{2}/2)^{k+1}}{(k+1)!(2(k+1)+2n+1)!!}
                       =\frac{(z^{2}/2)}{(k+1)(2k+2n+3)}\frac{(z^{2}/2)^{k}}{k!(2k+2n+1)!!}
                   ]
                   stem:[z^{n}\mathrm{e}^{-z}\frac{(z^{2}/2)^{0}}{0!(0+2n+1)!!}=\frac{z^{n}\mathrm{e}^{-z}}{(2n+1)!!}] */
                std::valarray<RealType> results(maxOrder+1);
                results[0] = std::exp(-arguments);
                for (unsigned int order=0; order<maxOrder; ++order) {
                    results[order+1] = arguments*results[order]/RealType(order+order+3);
                }
                RealType sq2_arg = 0.5*arguments*arguments;
                for (unsigned int order=0; order<=maxOrder; ++order) {
                    auto expn_term = results[order];
                    unsigned int sum1_order2 = order+order+1;
                    unsigned int k = 0;
                    while (expn_term>tolerance) {
                        ++k;
                        expn_term *= sq2_arg/RealType(k*(k+k+sum1_order2));
                        results[order] += expn_term;
                    }
                }
            }
        }
        return results;
    }
}
