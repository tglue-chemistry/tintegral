/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates Boys functions.

   2022-02-26, Bin Gao:
   * first version
*/

#pragma once

#include <cmath>
#include <limits>
#include <valarray>

#include "tGlueCore/Logger.hpp"
#ifdef TINTEGRAL_DEBUG
#include "tGlueCore/Stringify.hpp"
#endif

#include "tIntegral/Settings.hpp"

namespace tIntegral
{
    /* Compute Boys functions */
    template<typename RealType=double> std::valarray<std::valarray<RealType>> boys_v(
        const unsigned int maxOrder,
        const std::valarray<RealType> args) noexcept
    {
#ifdef TINTEGRAL_DEBUG
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "boys_v() will compute up to order ",
                                maxOrder,
                                " with arguments ",
                                tGlueCore::stringify(std::begin(args), std::end(args)));
#endif
        std::valarray<std::valarray<RealType>> vals(maxOrder+1);
        for (std::size_t n=0; n<=maxOrder; ++n) {
            vals[n].resize(args.size());
        }
        for (std::size_t i=0; i<args.size(); ++i) {
            /* Rational approximation of the zeroth order Boys function */
            if (args[i]<=1) {
                vals[0][i] = std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          7.7431289765679954e-6,
                                                                          5.8013130589625492e-4),
                                                                 8.6585028922695218e-3),
                                                        1.0891198266892342e-1),
                                               4.2131665053371023e-1),
                                      2.2699378135344061)
                           / std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          1.0298955801000000e-4,
                                                                          2.8048200242871830e-3),
                                                                 3.6432514971616124e-2),
                                                        2.7457239744000644e-1),
                                               1.1779625883785794),
                                      2.2699378135344052);
            }
            else if (args[i]<7) {
                vals[0][i] = std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   std::fma(args[i],
                                                                                            3.5960048693138910e-9,
                                                                                            7.1643556269285254e-7),
                                                                                   1.5870305976743249e-5),
                                                                          4.2490684401893097e-4),
                                                                 3.7633480042344360e-3),
                                                        4.9552928157715852e-2),
                                               1.6290857280445508e-1),
                                      1.0009574989081021)
                           / std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   std::fma(args[i],
                                                                                            8.9566896774000000e-8,
                                                                                            4.1127103555045216e-6),
                                                                                   9.8889354639715169e-5),
                                                                          1.5377757781070642e-3),
                                                                 1.6265407498397557e-2),
                                                        1.1497753571617642e-1),
                                               4.9656107244530340e-1),
                                      1.0009574989081711);
            }
            else if (args[i]<=14) {
                vals[0][i] = std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   7.9475160723469155e-8,
                                                                                   3.0021547331061438e-5),
                                                                          8.0758016599267036e-4),
                                                                 5.7523103435856937e-3),
                                                        9.2620173573973509e-2),
                                               2.6336316471821378e-1),
                                      1.8164260035138938)
                           / std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   2.6822538886900000e-6,
                                                                                   2.3110686851335755e-4),
                                                                          2.2813747204096204e-3),
                                                                 3.0362848990260044e-2),
                                                        1.9755461915739980e-1),
                                               8.7280278995828052e-1),
                                      1.8140634810945542);
            }
            else if (args[i]<28.187164187414548) {
                vals[0][i] = std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   std::fma(args[i],
                                                                                            -3.0879448640403457e-10,
                                                                                            -3.5315086742141157e-7),
                                                                                   -3.4995109206623610e-5),
                                                                          -2.0246703468529315e-4),
                                                                 9.2119565755768653e-3),
                                                        -4.9325712476547629e-2),
                                               -1.5831764702387535e-3),
                                      2.3345239538654938)
                           / std::fma(args[i],
                                      std::fma(args[i],
                                               std::fma(args[i],
                                                        std::fma(args[i],
                                                                 std::fma(args[i],
                                                                          std::fma(args[i],
                                                                                   std::fma(args[i],
                                                                                            -1.7759035156800000e-8,
                                                                                            -5.0928707847441459e-6),
                                                                                   -1.7377005556847808e-4),
                                                                          1.8290259077650788e-3),
                                                                 1.3627508719122853e-2),
                                                        -2.1198429074023575e-1),
                                               1.2700844262750420),
                                      1.7880831432504771);
            }
            /* Asymptotic approximation of the zeroth order Boys function
               stem:[\frac{1}{2}\sqrt{\frac{\pi}{x}}] */
            else {
                vals[0][i] = 0.886226925452758/std::sqrt(args[i]);
            }
#ifdef TINTEGRAL_DEBUG
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "boys_v() gets the zeroth order Boys function ",
                                    vals[0][i],
                                    " for the argument ",
                                    args[i]);
#endif
            if (maxOrder==0) continue;
            /* Find the maximum order of the upward recurrence relation using
               binary search algorithm */
            int nup_max;
            if (args[i]<=14) {
                if (args[i]<=1) {
                    nup_max = args[i]<1.8227993151040120e-2
                        ? 0
                        : int(round(2.4626157599144523*args[i]+0.9014896211617622));
                }
                else {
                    nup_max = args[i]<7
                        ? int(round((-3.958731600436549e-2*args[i]+1.6776994192555685)*args[i]+1.815269629703272))
                        : int(round(1.1559382401308103*args[i]+3.78365305701053));
                }
            }
            else {
                if (args[i]<164.56821028423799) {
                    if (args[i]<28.187164187414548) {
                        nup_max = int(round(1.0277255883753522*args[i]+4.000253798587592));
                    }
                    else {
                        nup_max = args[i]<32.102511634612173
                            ? int(round((-6.358678844078643e-1*args[i]+41.76161090060233)*args[i]-645.8143359657548))
                            : int(round((-2.780445151604754e-4*args[i]+1.0224462243996915)*args[i]+7.642946358249691));
                    }
                }
                else {
                    if (args[i]<433.81659399125958) {
                        nup_max = int(round((-2.531999260846358e-4*args[i]+1.0407111502467767)*args[i]+3.617218884225029));
                    }
                    else {
                        nup_max =  args[i]<520.42323182048845
                            ? int(round((-4.421882296961987e-3*args[i]+4.737753666818327)*args[i]-817.6377123865682))
                            : 450;
                    }
                }
            }
            int n_upward = maxOrder<=nup_max ? maxOrder : nup_max;
#ifdef TINTEGRAL_DEBUG
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "boys_v() gets the maximum order of upward recurrence relation ",
                                    n_upward,
                                    " for the argument ",
                                    args[i]);
#endif
            if (args[i]<=708) {
                /* Compute the n-th order Boys function by using the upward
                   recurrence relation */
                RealType exp_factor = 0.5*std::exp(-args[i]);
                RealType val_exp = -exp_factor/args[i];
                for (std::size_t n=0; n<n_upward; ++n) {
                    vals[n+1][i] = std::fma((n+0.5)/args[i], vals[n][i], val_exp);
                }
                if (maxOrder>nup_max) {
                    /* Power series according to approximate algorithm error */
                    RealType val_divisor = maxOrder+0.5;
                    RealType val_term = 1/val_divisor;
                    vals[maxOrder][i] = val_term;
                    val_divisor += 1;
                    val_term *= args[i]/val_divisor;
                    while (val_term>1e-13*vals[maxOrder][i]) {
                        vals[maxOrder][i] += val_term;
                        val_divisor += 1;
                        val_term *= args[i]/val_divisor;
                    }
                    vals[maxOrder][i] *= exp_factor;
#ifdef TINTEGRAL_DEBUG
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "boys_v() gets Boys function ",
                                            vals[maxOrder][i],
                                            " of the order ",
                                            maxOrder,
                                            " and the argument ",
                                            args[i],
                                            " by using power series");
#endif
                    /* Compute the n-th order Boys function by using the
                       downward recurrence relation */
                    for (std::size_t n=maxOrder-1; n>n_upward; --n) {
                        vals[n][i] = std::fma(args[i], vals[n+1][i], exp_factor)/(n+0.5);
                    }
                }
            }
            else {
                /* Asymptotic approximation of Boys functions
                   stem:[\frac{\left(\frac{1}{2}\right)^{(n)}\sqrt{\pi}}{2x^{n+\frac{1}{2}}}] */
                for (std::size_t n=0; n<n_upward; ++n) vals[n+1][i] = (n+0.5)*vals[n][i]/args[i];
                /* Underflow */
#ifdef TINTEGRAL_DEBUG
                if (n_upward+1<=maxOrder) {
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "boys_v() encounters underflow with the maximum order ",
                                            maxOrder,
                                            " and the argument ",
                                            args[i]);
                }
#endif
                for (std::size_t n=n_upward+1; n<=maxOrder; ++n) vals[n][i] = 0;
            }
        }
        return vals;
    }

    /* Compute the upper bounds of approximation errors of Boys functions */
    template<typename RealType=double> std::valarray<std::valarray<RealType>> boys_error_v(
        const unsigned int maxOrder,
        const std::valarray<RealType> args,
        const std::valarray<std::valarray<RealType>> vals) noexcept
    {
    }
}
