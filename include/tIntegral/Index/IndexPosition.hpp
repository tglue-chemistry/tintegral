/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of positions of indices involved in recurrence
   relations.

   2019-09-27, Bin Gao:
   * file name changed to IndexPosition.hpp

   2018-06-16, Bin Gao:
   * changed to C++ class

   2017-06-09, Bin Gao:
   * first version
*/

#pragma once

#include <string>

#include "tIntegral/Recurrence/IndexSettings.hpp"

namespace tIntegral
{
    /* Forward declarations */
    class IndexOrder;

    /* Internal representation for the position of an index in the memory */
    class IndexPosition
    {
        public:
            explicit IndexPosition(const unsigned int position) noexcept
            {
                m_bit_position = position%IndexSettings::order_nidx;
                m_integer_position = (position-m_bit_position)/IndexSettings::order_nidx;
            }
            ~IndexPosition() noexcept = default;
            inline std::string to_string() const noexcept
            {
                return "{bit: "+std::to_string(m_bit_position)
                    +", integer: "+std::to_string(m_integer_position)
                    +", position: "+std::to_string(get());
            }
            /* Set the internal position */
            inline void set(const unsigned int position) noexcept
            {
                m_bit_position = position%IndexSettings::order_nidx;
                m_integer_position = (position-m_bit_position)/IndexSettings::order_nidx;
            }
            /* Get the position of an index from its internal representation */
            inline unsigned int get() const noexcept
            {
                return m_bit_position+m_integer_position*IndexSettings::order_nidx;
            }
            /* Function for equality comparison */
            inline bool equal_to(const IndexPosition& other) const noexcept
            {
                if (m_integer_position==other.m_integer_position) {
                    return m_bit_position==other.m_bit_position;
                }
                else {
                    return false;
                }
            }
            /* Function for less-than inequality comparison */
            inline bool less(const IndexPosition& other) const noexcept
            {
                if (m_integer_position<other.m_integer_position) {
                    return true;
                }
                else if (m_integer_position==other.m_integer_position) {
                    return m_bit_position<other.m_bit_position;
                }
                else {
                    return false;
                }
            }
        private:
            friend class IndexOrder;
            /* Position of integer for representing an index */
            unsigned int m_integer_position;
            /* Position of bits in the integer for the index */
            unsigned int m_bit_position;
    };
}
