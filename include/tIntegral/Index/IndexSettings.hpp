/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of internal constants used by classes
   IndexPosition and IndexOrder.

   2019-09-27, Bin Gao:
   * file name changed to IndexSettings.hpp

   2018-06-16, Bin Gao:
   * changed to C++ class

   2017-06-09, Bin Gao:
   * first version
*/

#pragma once

#include <limits>

namespace tIntegral
{
    /* Internal constants used by classes IndexPosition and IndexOrder */
    namespace IndexSettings
    {
        /* Integral type used to represent orders or increments */
        typedef unsigned long order_type;
        /* Number of bits to represent the order or the increment of an index,
           in which the highest bit is for overflow and the second highest one
           for the sign.

           The use of overflow bit prevents the carry of an addition of any two
           terms.

           The sign bit and two's complement of any increment enable us to only
           use addition operation for computing the orders instead of the
           subtraction. */
        static const unsigned int order_nbit = 9;
        /* Number of indices represented by the integral type; -1 is to prevent
           overflow when computing \var(bit_reset_overflow) and \var(bit_get_signs) */
        static const unsigned int order_nidx = (std::numeric_limits<unsigned char>::digits*sizeof(order_type)-1)/order_nbit;
        /* Maximum order or increment of an index by using \var(order_nbit)-2
           bits, its opposite number is the minimum order or increment of an
           index by using \var(order_nbit)-1 bits */
        static const unsigned int bit_max_order = ((order_type)1<<(order_nbit-2))-1;
        /* Used to extract the rightmost order or increment with the sign bit
           by using the bitwise AND operation */
        static const order_type bit_get_rightmost = ((order_type)1<<(order_nbit-1))-1;
        /* Used to reset overflow bits (=0) by using the bitwise AND operation */
        static const order_type bit_reset_overflow = ~(((order_type)1<<(order_nbit-1))*((((order_type)1<<(order_nbit*order_nidx))-1)/(((order_type)1<<order_nbit)-1)));
        /* Used to get all sign bits, and can be used to check if any order or
           increment is negative by the bitwise AND operation */
        static const order_type bit_get_signs = ((order_type)1<<(order_nbit-2))*((((order_type)1<<(order_nbit*order_nidx))-1)/(((order_type)1<<order_nbit)-1));
    }
}
