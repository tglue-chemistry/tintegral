/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of orders of indices involved in recurrence
   relations.

   2019-09-27, Bin Gao:
   * file name changed to IndexOrder.hpp

   2018-06-16, Bin Gao:
   * changed to C++ class

   2017-06-09, Bin Gao:
   * first version
*/

#pragma once

#if defined(TINTEGRAL_DEBUG)
#include <algorithm>
#endif
#include <iterator>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/IndexSettings.hpp"
#include "tIntegral/Recurrence/IndexPosition.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"

namespace tIntegral
{
    /* Orders or increments of indices of a term in a recurrence relation */
    class IndexOrder
    {
        public:
            explicit IndexOrder() noexcept:
                m_overflow(false),
                m_num_indices(0) {}
            /* Get the number of indices */
            inline unsigned int get_num_indices() const noexcept { return m_num_indices; }
            /* Assign increments for all indices */
            template<typename IncrementIterator>
            inline typename std::enable_if<std::is_integral<typename std::iterator_traits<IncrementIterator>::value_type>::value, bool>::type
            assign(const unsigned int numIndices, IncrementIterator beginIncrement) noexcept
            {
                using namespace IndexSettings;
                /* Resize and clean previous content */
                m_num_indices = numIndices;
                /* Number of integers to represent the orders or increments of
                   a term, the following formula works because m_num_indices is
                   positive */
                std::size_t num_integers = (m_num_indices-1)/order_nidx+1;
                m_idx_orders.resize(num_integers, 0);
                for (unsigned int iidx=0,iint=0; iidx<m_num_indices; ++iint) {
                    for (unsigned int ibit=0; iidx<m_num_indices && ibit<order_nidx; ++iidx,++ibit,++beginIncrement) {
                        if (*beginIncrement>0) {
                            if (*beginIncrement>bit_max_order) {
                                m_overflow = true;
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "Overflow in IndexOrder::assign() for index ",
                                                        iidx,
                                                        " with the increment as ",
                                                        *beginIncrement,
                                                        ", and maximum allowed order is ",
                                                        bit_max_order);
                                return false;
                            }
                            order_type idx_order = *beginIncrement;
                            m_idx_orders[iint] += idx_order<<(order_nbit*ibit);
                        }
                        else if (*beginIncrement<0) {
                            if (*beginIncrement<-bit_max_order) {
                                m_overflow = true;
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "Overflow in IndexOrder::assign() for index ",
                                                        iidx,
                                                        " with the increment as ",
                                                        *beginIncrement,
                                                        ", and minimum allowed order is ",
                                                        -bit_max_order);
                                return false;
                            }
                            /* Keep only the last order_nbit-1 bits of the two's
                               complement of the order or increment */
                            order_type idx_order = *beginIncrement & bit_get_rightmost;
                            m_idx_orders[iint] += idx_order<<(order_nbit*ibit);
                        }
                    }
                }
                return true;
            }
            /* Assign increments of given indices */
            template<typename IndexIterator, typename IncrementIterator>
            inline typename std::enable_if<std::is_same<typename std::iterator_traits<IndexIterator>::value_type,
                                                        std::shared_ptr<RecurIndex>>::value &&
                                           std::is_integral<typename std::iterator_traits<IncrementIterator>::value_type>::value,
                                           bool>::type
            assign(const unsigned int numIndices,
                   const unsigned int numIncrIndices,
                   IndexIterator beginIncrIndex,
                   IncrementIterator beginIncrement) noexcept
            {
                using namespace IndexSettings;
                /* Resize and clean previous content */
                m_num_indices = numIndices;
                /* Number of integers to represent the orders or increments of
                   a term, the following formula works because m_num_indices is
                   positive */
                std::size_t num_integers = (m_num_indices-1)/order_nidx+1;
                m_idx_orders.resize(num_integers, 0);
                for (unsigned int iidx=0; iidx<numIncrIndices; ++iidx,++beginIncrIndex,++beginIncrement) {
                    auto idx_position = (*beginIncrIndex)->get_index_position();
                    if (*beginIncrement>0) {
                        if (*beginIncrement>bit_max_order) {
                            m_overflow = true;
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "Overflow in IndexOrder::assign() for index ",
                                                    iidx,
                                                    " with the increment as ",
                                                    *beginIncrement,
                                                    ", and maximum allowed order is ",
                                                    bit_max_order);
                            return false;
                        }
                        order_type idx_order = *beginIncrement;
                        m_idx_orders.at(idx_position.m_integer_position) += idx_order<<(order_nbit*idx_position.m_bit_position);
                    }
                    else if (*beginIncrement<0) {
                        if (*beginIncrement<-bit_max_order) {
                            m_overflow = true;
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "Overflow in IndexOrder::assign() for index ",
                                                    iidx,
                                                    " with the increment as ",
                                                    *beginIncrement,
                                                    ", and minimum allowed order is ",
                                                    -bit_max_order);
                            return false;
                        }
                        /* Keep only the last order_nbit-1 bits of the two's
                           complement of the order or increment */
                        order_type idx_order = *beginIncrement & bit_get_rightmost;
                        m_idx_orders.at(idx_position.m_integer_position) += idx_order<<(order_nbit*idx_position.m_bit_position);
                    }
                }
                return true;
            }
            /* Return if the orders/increments are valid */
            inline operator bool() const { return m_num_indices>0 && !m_overflow; }
            /* Write to string */
            inline std::string to_string() const noexcept
            {
                if (*this) {
                    using namespace IndexSettings;
                    std::string str_orders = "[";
                    for (unsigned int iidx=0,iint=0; iidx<m_num_indices; ++iint) {
                        for (unsigned int ibit=0; iidx<m_num_indices && ibit<order_nidx; ++iidx,++ibit) {
                            if (is_negative(iint, ibit)) {
                                str_orders += std::to_string(get_negative_order(iint, ibit))+',';
                            }
                            else {
                                str_orders += std::to_string(get_nonnegative_order(iint, ibit))+',';
                            }
                        }
                    }
                    if (str_orders.size()==1) {
                        str_orders += ']';
                    }
                    else {
                        str_orders.back() = ']';
                    }
                    return str_orders;
                }
                else {
                    return "{num-indices: "+std::to_string(m_num_indices)
                        + ", overflow: "+std::to_string(m_overflow)+'}';
                }
            }
            /* Check if there is any negative order */
            inline bool any_negative() const noexcept
            {
                using namespace IndexSettings;
                for (auto iint=m_idx_orders.cbegin(); iint!=m_idx_orders.cend(); ++iint) {
                    if ((*iint & bit_get_signs)!=0) return true;
                }
                return false;
            }
            /* Check if an index has negative order */
            inline bool is_negative(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto idx_position = index->get_index_position();
                return is_negative(idx_position.m_integer_position, idx_position.m_bit_position);
            }
            /* Get a non-negative order of an index */
            inline unsigned int get_nonnegative_order(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto idx_position = index->get_index_position();
                return get_nonnegative_order(idx_position.m_integer_position, idx_position.m_bit_position);
            }
            /* Get a negative order of an index */
            inline int get_negative_order(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto idx_position = index->get_index_position();
                return get_negative_order(idx_position.m_integer_position, idx_position.m_bit_position);
            }
            /* Get the order of an index */
            inline int get_order(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                auto idx_position = index->get_index_position();
                return is_negative(idx_position.m_integer_position, idx_position.m_bit_position)
                    ? get_negative_order(idx_position.m_integer_position, idx_position.m_bit_position)
                    : int(get_nonnegative_order(idx_position.m_integer_position, idx_position.m_bit_position));
            }
            /* Return whether given orders are equal to the current ones */
            inline bool equal_to(const IndexOrder& other) const noexcept
            {
                if (m_idx_orders.size()!=other.m_idx_orders.size()) return false;
                for (unsigned int iint=0; iint<m_idx_orders.size(); ++iint) {
                    if (m_idx_orders[iint]!=other.m_idx_orders[iint]) return false;
                }
                return true;
            }
            /* Return new orders after adding given increments */
            inline IndexOrder add_increment(const IndexOrder& addend) const noexcept
            {
                using namespace IndexSettings;
                IndexOrder sum;
                if (m_num_indices!=addend.m_num_indices) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "IndexOrder::add_increment() called with the number of indices as ",
                                            m_num_indices,
                                            ", and that of the addend as ",
                                            addend.m_num_indices);
                }
                else {
                    sum.m_num_indices = m_num_indices;
                    sum.m_idx_orders.resize(m_idx_orders.size());
                    for (unsigned int iint=0; iint<sum.m_idx_orders.size(); ++iint) {
                        sum.m_idx_orders[iint] = (m_idx_orders[iint]+addend.m_idx_orders[iint])
                                               & bit_reset_overflow;
                        /* Get signs of augend, addend and sum for overflow verification */
                        auto signs_augend = m_idx_orders[iint] & bit_get_signs;
                        auto signs_addend = addend.m_idx_orders[iint] & bit_get_signs;
                        auto signs_sum = sum.m_idx_orders[iint] & bit_get_signs;
                        /* Overflow may happen when we add two positive or two negative
                           numbers, which can be verified by checking if the sum has
                           different sign from both the augend and the addend (by using
                           the XOR operation) */
                        if ((signs_sum^signs_augend)&(signs_sum^signs_addend)) {
                            sum.m_overflow = true;
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "Overflow in IndexOrder::add_increment() with the augend[",
                                                    iint,
                                                    "] = ",
                                                    m_idx_orders[iint],
                                                    ", and the addend[",
                                                    iint,
                                                    "] = ",
                                                    addend.m_idx_orders[iint],
                                                    ", and the sum = ",
                                                    sum.m_idx_orders[iint]);
                            break;
                        }
                    }
                }
                return sum;
            }
            /* Transfer the order of the first index to the second */
            inline IndexOrder transfer_order(const std::shared_ptr<RecurIndex>& firstIndex,
                                             const std::shared_ptr<RecurIndex>& secondIndex) const noexcept
            {
                using namespace IndexSettings;
                IndexOrder new_orders;
                new_orders.m_num_indices = m_num_indices;
                new_orders.m_idx_orders.assign(m_idx_orders.cbegin(), m_idx_orders.cend());
                /* Get the position and order of the first index */
                auto pos_first = firstIndex->get_index_position();
                int order_first = is_negative(pos_first.m_integer_position, pos_first.m_bit_position)
                                ? get_negative_order(pos_first.m_integer_position, pos_first.m_bit_position)
                                : get_nonnegative_order(pos_first.m_integer_position, pos_first.m_bit_position);
                if (order_first!=0) {
                    /* Get the position and order of the second index */
                    auto pos_second = secondIndex->get_index_position();
                    int order_second = is_negative(pos_second.m_integer_position, pos_second.m_bit_position)
                                     ? get_negative_order(pos_second.m_integer_position, pos_second.m_bit_position)
                                     : get_nonnegative_order(pos_second.m_integer_position, pos_second.m_bit_position);
                    /* Check the sum of orders of the first and the second indices */
                    if (order_first+order_second>bit_max_order ||
                        order_first+order_second<-bit_max_order) {
                        new_orders.m_overflow = true;
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "Overflow in IndexOrder::transfer_order() when transferring the order ",
                                                order_first,
                                                " of the index ",
                                                pos_first.get(),
                                                " to the index ",
                                                pos_second.get(),
                                                " with the order ",
                                                order_second,
                                                ", and maximum/minimum allowed orders are +/-",
                                                bit_max_order);
                    }
                    /* Set the order of the first index as 0, and that of the second as the sum */
                    if (order_first>0) {
                        order_type addend = (-order_first) & bit_get_rightmost;
                        new_orders.m_idx_orders.at(pos_first.m_integer_position) += addend<<(order_nbit*pos_first.m_bit_position);
                        addend = order_first;
                        new_orders.m_idx_orders.at(pos_second.m_integer_position) += addend<<(order_nbit*pos_second.m_bit_position);
                    }
                    else {
                        order_type addend = -order_first;
                        new_orders.m_idx_orders.at(pos_first.m_integer_position) += addend<<(order_nbit*pos_first.m_bit_position);
                        addend = order_first & bit_get_rightmost;
                        new_orders.m_idx_orders.at(pos_second.m_integer_position) += addend<<(order_nbit*pos_second.m_bit_position);
                    }
                }
                return new_orders;
            }
            /* Return a new order after resetting the order of an index */
            inline IndexOrder reset_order(const std::shared_ptr<RecurIndex>& index, const int newOrder) const noexcept
            {
                using namespace IndexSettings;
                IndexOrder new_orders;
                new_orders.m_num_indices = m_num_indices;
                new_orders.m_idx_orders.assign(m_idx_orders.cbegin(), m_idx_orders.cend());
                /* Check the order */
                auto pos_index = index->get_index_position();
                if (newOrder>bit_max_order || newOrder<-bit_max_order) {
                    new_orders.m_overflow = true;
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Overflow in IndexOrder::reset_order() for the index ",
                                            pos_index.get(),
                                            " with the new order ",
                                            newOrder,
                                            ", and maximum/minimum allowed orders are +/-",
                                            bit_max_order);
                }
                /* Reset the order of the index */
                else {
                    int order_addend = is_negative(pos_index.m_integer_position, pos_index.m_bit_position)
                                     ? newOrder-get_negative_order(pos_index.m_integer_position, pos_index.m_bit_position)
                                     : newOrder-get_nonnegative_order(pos_index.m_integer_position, pos_index.m_bit_position);
                    order_type addend = order_addend>0 ? order_addend : order_addend & bit_get_rightmost;
                    new_orders.m_idx_orders.at(pos_index.m_integer_position) += addend<<(order_nbit*pos_index.m_bit_position);
                }
                return new_orders;
            }
            /* Get strides and size of given indices */
            template<typename IndexIterator>
            inline typename std::enable_if<std::is_same<typename std::iterator_traits<IndexIterator>::value_type,
                                                        std::shared_ptr<RecurIndex>>::value,
                                           bool>::type
            get_stride(IndexIterator beginIndex,
                       IndexIterator beginStrideIndex,
                       IndexIterator endStrideIndex,
                       std::size_t& idxSize,
                       std::vector<std::size_t>& idxStrides) const noexcept
            {
                using namespace IndexSettings;
                /* Number of stride indices */
                auto num_stride_indices = std::distance(beginStrideIndex, endStrideIndex);
                if (num_stride_indices<1 || num_stride_indices>m_num_indices) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Number of stride indices provided to IndexOrder::get_stride() is ",
                                            num_stride_indices,
                                            ", which is not allowed with the number of indices ",
                                            m_num_indices);
                    return false;
                }
#if defined(TINTEGRAL_DEBUG)
                if (!std::is_sorted(beginStrideIndex,
                                    endStrideIndex,
                                    [](const std::shared_ptr<RecurIndex>& lhs, const std::shared_ptr<RecurIndex>& rhs)
                                    { return lhs->less(rhs); })) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Stride indices provided to IndexOrder::get_stride() are ",
                                            tGlueCore::stringify(beginStrideIndex, endStrideIndex),
                                            ", which must be in ascending order");
                    return false;
                }
#endif
                if (any_negative()) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "IndexOrder::get_stride() called with negative orders ",
                                            to_string());
                    return false;
                }
                idxSize = 1;
                idxStrides.resize(num_stride_indices+2, 1);
                auto pos_stride = (*beginStrideIndex)->get_index_position();
                auto idx_stride = idxStrides.begin();
                for (unsigned int jidx=0,iint=0; jidx<m_num_indices; ++iint) {
                    for (unsigned int ibit=0; jidx<m_num_indices && ibit<order_nidx; ++jidx,++beginIndex,++ibit) {
#if defined(TINTEGRAL_DEBUG)
                        Settings::logger->write(tGlueCore::MessageType::Debug,
                                                "IndexOrder::get_stride() processes the index ",
                                                (*beginIndex)->to_string());
#endif
                        /* Match a stride-index, update the size of all indices
                           and begin to compute the next stride */
                        if (iint==pos_stride.m_integer_position && ibit==pos_stride.m_bit_position) {
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(
                                tGlueCore::MessageType::Debug,
                                "IndexOrder::get_stride() got ",
                                (*beginIndex)->get_num_components(get_nonnegative_order(iint, ibit)),
                                " components for the index ",
                                (*beginStrideIndex)->to_string(),
                                " with order ",
                                get_nonnegative_order(iint, ibit)
                            );
#endif
                            idxSize *= (*beginIndex)->get_num_components(get_nonnegative_order(iint, ibit));
                            /* The last element is the size of all strides */
                            idxStrides.back() *= *idx_stride;
                            ++idx_stride;
                            ++beginStrideIndex;
                            if (beginStrideIndex!=endStrideIndex) {
                                pos_stride = (*beginStrideIndex)->get_index_position();
                            }
                        }
                        else {
                            *idx_stride *= (*beginIndex)->get_num_components(get_nonnegative_order(iint, ibit));
                        }
                    }
                }
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "IndexOrder::get_stride() got strides ",
                                        tGlueCore::stringify(idxStrides.cbegin(), idxStrides.cend()));
#endif
                /* Update the size of all indices */
                idxSize *= idxStrides.back();
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "IndexOrder::get_stride() got the size of all indices ",
                                        idxSize);
#endif
                return true;
            }
            /* Get size of given indices */
            template<typename IndexIterator>
            inline typename std::enable_if<std::is_same<typename std::iterator_traits<IndexIterator>::value_type,
                                                        std::shared_ptr<RecurIndex>>::value,
                                           std::size_t>::type
            get_size(IndexIterator beginSizeIndex, IndexIterator endSizeIndex) const noexcept
            {
                auto num_size_indices = std::distance(beginSizeIndex, endSizeIndex);
                if (num_size_indices<1 || num_size_indices>m_num_indices) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Number of size indices provided to IndexOrder::get_size() is ",
                                            num_size_indices,
                                            ", while the number of indices is ",
                                            m_num_indices);
                    return 0;
                }
                if (any_negative()) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "IndexOrder::get_size() called with negative orders ",
                                            to_string());
                    return 0;
                }
                std::size_t idx_size = 1;
                for (auto size_index=beginSizeIndex; size_index!=endSizeIndex; ++size_index) {
                    auto idx_position = (*size_index)->get_index_position();
                    idx_size *= (*size_index)->get_num_components(
                        get_nonnegative_order(idx_position.m_integer_position, idx_position.m_bit_position)
                    );
                }
                return idx_size;
            }
            ~IndexOrder() noexcept = default;
        protected:
            /* Check if an index (according to its element and bit positions)
               has negative order */
            inline bool is_negative(const unsigned int& integerPosition,
                                    const unsigned int& bitPosition) const noexcept
            {
                using namespace IndexSettings;
                return (((m_idx_orders.at(integerPosition)>>(bitPosition*order_nbit))&bit_get_rightmost)>bit_max_order);
            }
            /* Get a non-negative order of an index according to its element
               and bit positions */
            inline unsigned int get_nonnegative_order(const unsigned int& integerPosition,
                                                      const unsigned int& bitPosition) const noexcept
            {
                using namespace IndexSettings;
                return (unsigned int)((m_idx_orders.at(integerPosition)>>(bitPosition*order_nbit))&bit_max_order);
            }
            /* Get a negative order of an index according to its element and
               bit positions */
            inline int get_negative_order(const unsigned int& integerPosition,
                                          const unsigned int& bitPosition) const noexcept
            {
                using namespace IndexSettings;
                return -int((((m_idx_orders.at(integerPosition)>>(bitPosition*order_nbit))&bit_max_order)^bit_max_order)+1);
            }
        private:
            /* Overflow or not */
            bool m_overflow;
            /* Number of indices to represent */
            unsigned int m_num_indices;
            /* Orders or increments of a term */
            std::vector<IndexSettings::order_type> m_idx_orders;
    };
}
