/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of indices involved in recurrence relations.

   2020-02-25, Bin Gao:
   * add derived index classes

   2019-09-30, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>

#include "tIntegral/Recurrence/IndexPosition.hpp"
#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"

namespace tIntegral
{
    /* Base class of recurrence relation index */
    class RecurIndex
    {
        public:
            explicit RecurIndex(const std::string name,
                                const unsigned int position,
                                const RecurVector order) noexcept:
                m_name(name),
                m_position(IndexPosition(position)),
                m_order(order) {}
            virtual ~RecurIndex() noexcept = default;
            inline std::string get_name() const noexcept { return m_name; }
            inline IndexPosition get_index_position() const noexcept { return m_position; }
            inline unsigned int get_position() const noexcept { return m_position.get(); }
            /* Get the order of the index */
            inline RecurVector get_order() const noexcept { return m_order; }
            /* Set the order of the index */
            inline void set_order(const RecurVector order) noexcept { m_order = order; }
            /* Convert to a string */
            virtual std::string to_string() const noexcept = 0;
            /* Get the number of components of the index */
            virtual unsigned int get_num_components(const unsigned int order) const noexcept = 0;
            ///* Whether the components of the index are represented in Cartesian coordinate system */
            //virtual bool is_cartesian() const noexcept { return false; }
            /* Get the direction of the index */
            virtual RecurDirection get_direction() const noexcept { return RecurDirection::XYZ; }
            /* Function for equality comparison */
            //inline bool equal_to(const RecurIndex& other) const noexcept
            //{
            //    return m_position.equal_to(other.m_position) && m_name.compare(other.m_name)==0;
            //}
            inline bool equal_to(const std::shared_ptr<RecurIndex>& other) const noexcept
            {
                return m_position.equal_to(other->m_position) && m_name.compare(other->m_name)==0;
            }
            /* Function for less-than inequality comparison */
            //inline bool less(const RecurIndex& other) const noexcept
            //{
            //    return m_position.less(other.m_position);
            //}
            inline bool less(const std::shared_ptr<RecurIndex>& other) const noexcept
            {
                return m_position.less(other->m_position);
            }
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<RecurIndex> get_copy() noexcept = 0;
            /* @fn:type_id[Get the runtime type of a symbol.]
               @return:[Runtime type of the symbol.] */
            //FIXME: this function may be removed if we use pattern matching
            virtual std::type_index type_id() const noexcept
            {
                return typeid(*this);
            }
        private:
            std::string m_name;
            IndexPosition m_position;
            RecurVector m_order;
    };

    /* Scalar index */
    class RecurScalarIndex: virtual public RecurIndex
    {
        public:
            explicit RecurScalarIndex(const std::string name,
                                      const unsigned int position,
                                      const RecurVector order) noexcept:
                RecurIndex(name, position, order) {}
            virtual ~RecurScalarIndex() noexcept = default;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the number of components of the index */
            virtual unsigned int get_num_components(const unsigned int order) const noexcept override;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<RecurIndex> get_copy() noexcept override;
    };

    /* Vector index */
    class RecurVectorIndex: virtual public RecurIndex
    {
        public:
            explicit RecurVectorIndex(const std::string name,
                                      const unsigned int position,
                                      const RecurVector order) noexcept:
                RecurIndex(name, position, order) {}
            virtual ~RecurVectorIndex() noexcept = default;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the number of components of the index */
            virtual unsigned int get_num_components(const unsigned int order) const noexcept override;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<RecurIndex> get_copy() noexcept override;
    };

    /* Index with x, y and z components arranged in a triangle */
    class RecurTriangularIndex: virtual public RecurIndex
    {
        public:
            explicit RecurTriangularIndex(const std::string name,
                                          const unsigned int position,
                                          const RecurVector order,
                                          const RecurDirection direction=RecurDirection::X) noexcept:
                RecurIndex(name, position, order),
                m_direction(direction) {}
            virtual ~RecurTriangularIndex() noexcept = default;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the number of components of the index */
            virtual unsigned int get_num_components(const unsigned int order) const noexcept override;
            ///* Whether the components of the index are represented in Cartesian coordinate system */
            //virtual bool is_cartesian() const noexcept override { return true; }
            /* Get the direction of the index */
            virtual RecurDirection get_direction() const noexcept override;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<RecurIndex> get_copy() noexcept override;
        private:
            RecurDirection m_direction;
    };

    /* Spherical like index with 2l+1 components */
    class RecurSphericalIndex: virtual public RecurIndex
    {
        public:
            explicit RecurSphericalIndex(const std::string name,
                                         const unsigned int position,
                                         const RecurVector order) noexcept:
                RecurIndex(name, position, order) {}
            virtual ~RecurSphericalIndex() noexcept = default;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the number of components of the index */
            virtual unsigned int get_num_components(const unsigned int order) const noexcept override;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<RecurIndex> get_copy() noexcept override;
    };
}
