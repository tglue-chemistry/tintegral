/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file contains compile options of recurrence-relation indices.

   2019-10-01, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/Convert.hpp"

namespace tIntegral
{
    /* How the top-down and the bottom-up procedures will be built. */
    enum class IndexCompileOption
    {
        Recursion, /* Both procedures are built by the RecurCompiler */
        Transfer,  /* Both procedures of the index are built by transferring its order to another index */
        Customer,  /* Implemented by customers */
        Skip       /* Skip the index */
    };

    /* Get string of an compile option */
    inline std::string stringify_compile_option(const IndexCompileOption option) noexcept
    {

        switch (option) {
            case IndexCompileOption::Recursion:
                return std::string("Recursion");
            case IndexCompileOption::Transfer:
                return std::string("Transfer");
            case IndexCompileOption::Customer:
                return std::string("Customer");
            case IndexCompileOption::Skip:
                return std::string("Skip");
            default:
                return "??"+std::to_string(tGlueCore::to_integral(option))+"??";
        }
    }
}
