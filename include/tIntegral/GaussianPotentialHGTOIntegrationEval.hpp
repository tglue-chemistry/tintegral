/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates Cartesian multipole moments integrals with Gaussian
   functions.

   2020-06-19, Bin Gao:
   * first version
*/

template<typename CentreType, typename RealType>
void GaussianPotentialHGTOIntegration<CentreType,RealType>::eval(const int& power) noexcept
{
    switch (power) {
        case 0:
            break;
        case -1:
            break;
        case -2:
            break;
        default:
    }
    /* Comput integrals of s-shell HGTOs */
    for (auto const& each_node: m_recur_arrays[2].get_input_nodes()) {
#ifdef TINTEGRAL_DEBUG
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "GaussianPotentialHGTOIntegration::eval() processes LHS node ",
                                each_node->to_string());
#endif
        auto val_lhs = m_buffer->get_element()+each_node->get_offset();
#if defined(USE_VALARRAY_BUFFER)
        *val_lhs = sqrt_pi3*expt_factors*std::pow(inv_total_exponents, 1.5);
#else
        for (unsigned int ival=0; ival<m_buffer->get_size_element(); ++ival) {
            (*val_lhs)[ival] = sqrt_pi3*expt_factors[ival]*std::pow(inv_total_exponents[ival], 1.5);
        }
#endif
    }
}
