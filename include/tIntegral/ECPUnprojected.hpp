/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of unprojected part of effective core
   potential.

   2019-05-12, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>
#include <utility>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/QuantChem/Operator.hpp"

#include "tIntegral/ECPGaussian.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* @class:ECPUnprojected[Unprojected part of effective core potential.] */
    template<typename CentreType, typename RealType=double>
    class ECPUnprojected: virtual public tSymbolic::OneElecOperator
                          //public std::enable_shared_from_this<ECPUnprojected<CentreType,RealType>>
    {
        public:
            /* @fn:ECPUnprojected()[Constructor.]
               @param[in]:centre[The ECP center stem:[\boldsymbol{C}].]
               @param[in]:gaussians[Gaussians containg stem:[n_{kL}], stem:[c_{kL}] and stem:[d_{kL}].] */
            explicit ECPUnprojected(const std::shared_ptr<CentreType> centre,
                                    const std::vector<ECPGaussian<RealType>> gaussians) noexcept:
                m_centre(centre),
                m_gaussians(gaussians) {}
            /* @fn:~ECPUnprojected()[Deconstructor.] */
            virtual ~ECPUnprojected() noexcept = default;
            /* The following functions are required by the one-electron
               operator base class, and can be futher overridden by any derived
               class of `ECPUnprojected`.

               @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<tSymbolic::Symbol> get_copy() noexcept override
            {
                return std::make_shared<ECPUnprojected>(std::move(*this));
            }
            /* Convert to a string */
            virtual std::string to_string() const noexcept override
            {
                return "{ecp-unprojected: {centre: "+m_centre->to_string()
                    +", gaussians: "+tGlueCore::stringify(m_gaussians.cbegin(), m_gaussians.cend())+"}}";
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<tSymbolic::Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<ECPUnprojected> other_cast = std::dynamic_pointer_cast<ECPUnprojected>(other);
                    if (m_centre->equal_to(other_cast->m_centre)) {
                        if (m_gaussians.size()==other_cast->m_gaussians.size()) {
                            for (auto const& each_gausssian: m_gaussians) {
                                auto not_found = true;
                                for (auto const& other_gaussian: other_cast->m_gaussians) {
                                    if (each_gausssian==other_gaussian) {
                                        not_found = false;
                                        break;
                                    }
                                }
                                if (not_found) return false;
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            /* Last but not least, there are a few functions specialized for
               the effective core potential.

               @fn:get_centre[Get the ECP center.]
               @return:[The ECP center.] */
            inline std::shared_ptr<CentreType> get_centre() const noexcept { return m_centre; }
            /* @fn:get_gaussians[Get Gaussian of effective core potential.]
               @return:[Gaussians of effective core potential.] */
            inline std::vector<ECPGaussian<RealType>> get_gaussians() const noexcept
            {
                return m_gaussians;
            }
        protected:
            /* @fn:oper_max_diff_order[Get the maximum order of differentiation of the operator with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation of the operator.] */
            virtual unsigned int oper_max_diff_order(const std::shared_ptr<tSymbolic::Symbol>& var) const noexcept override
            {
                //FIXME: to implement
                return 0;
            }
        private:
            /*@@ ECP centre */
            std::shared_ptr<CentreType> m_centre;
            /* Arranged in power, exponent and coefficient */
            std::vector<ECPGaussian<RealType>> m_gaussians;
    };
}
