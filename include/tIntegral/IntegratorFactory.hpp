/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of integrator creation factory.

   2019-02-14, Bin Gao:
   * first version
*/

#pragma once

#include <memory>

#include "tGlueCore/Delegate.hpp"

#include "tBasisSet/ContractedGTO.hpp"
#include "tMolecular/Cluster.hpp"

#include "tIntegral/OneElecOper.hpp"
#include "tIntegral/OneElecIntegrator.hpp"
#include "tIntegral/OneElecGTOIntegration.hpp"

namespace tIntegral
{
    /*@ == One-Electron Integrator Factory

        @fn:make_one_elec_integrator[Create an integrator of given type of one-electron operator and molecular system.]
        @tparam:OneOperType[Type of the one-electron operator.]
        @param[in]:molecule[The molecular system.]
        @return:[The one-electron integrator.]

        After creation, the one-electron integrator shares the ownership of the
        molecular system. */
    template<typename OneOperType> std::shared_ptr<OneElecIntegrator<OneOperType>>
    make_one_elec_integrator(const std::shared_ptr<tMolelcular::Cluster> molecule) noexcept
    {
        std::shared_ptr<OneElecIntegrator<OneOperType>> integrator = std::make_shared<OneElecIntegrator<OneOperType>>();
        integrator->set_molecule(molecule);
        integrator->template set_integration_engine<tBasisSet::ContractedGTO>(std::make_shared<OneElecGTOIntegration<OneOperType>>());
        return integrator;
    }
    /* @fn:make_one_elec_integrator[Create an integrator of given one-electron operator and molecular system.]
       @tparam:OneOperType[Type of the one-electron operator.]
       @param[in]:molecule[The molecular system.]
       @param[inout]:oneOper[The one-electron operator.]
       @return:[The one-electron integrator.]

       After creation, the one-electron integrator shares the ownership of the
       molecular system, and the one-electron operator uses the integrator as
       integral and expectation computation engines. */
    template<typename OneOperType> std::shared_ptr<OneElecIntegrator<OneOperType>>
    make_one_elec_integrator(const std::shared_ptr<tMolelcular::Cluster> molecule,
                             OneOperType& oneOper) noexcept
    {
        auto integrator = make_one_elec_integrator(molecule);
        OneIntEngine<OneOperType> int_engine;
        OneExpEngine<OneOperType> exp_engine;
        int_engine.set(integrator, OneElecIntegrator<OneOperType>::get_integral);
        exp_engine.set(integrator, OneElecIntegrator<OneOperType>::get_expectation);
        oneOper.set_engine(int_engine, exp_engine);
        return integrator;
    }
}
