/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of nuclear attraction potential class.

   2019-04-09, Bin Gao:
   * change to visitor pattern, because usually users do not need to implement
     routines for new one-electron operators by themselves; as such, the class
     OneElecEngine is merged into OneElecOper

   2019-02-08, Bin Gao:
   * new design by introducing classes OneElecEngine and OneElecOper

   2019-02-06, Bin Gao:
   * built on top of Delegate class and Symbol class

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <array>
#include <memory>
#include <vector>

#include "tGlueCore/Delegate.hpp"
#include "tSymbolic/Physics.hpp"
#include "tMatrix/BlockMat.hpp"

#include "tIntegral/BasicTypes.hpp"
#include "tIntegral/IntegratorState.hpp"
#include "tIntegral/OneElec/OneElecOper.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* @class:NucAttractPotential[Nuclear attraction potential.] */
    class NucAttractPotential: virtual public OneElecOper,
                               public std::enable_shared_from_this<NucAttractPotential>
    {
        public:
            /* @fn:NucAttractPotential()[Constructor.] */
            explicit NucAttractPotential() noexcept {}
            /* @fn:~NucAttractPotential()[Deconstructor.] */
            virtual ~NucAttractPotential() noexcept = default;
            /* @fn:type_id[Get the runtime type of nuclear attraction potential.]
               @return:[Runtime type of nuclear attraction potential.] */
            virtual inline std::type_index type_id() const noexcept override
            {
                return typeid(*this);
            }
            /* @fn:accept[Take a visitor of `NucAttractPotential` class.]
               @param[in]:visitor[The visitor.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual inline bool accept(SymbolVisitor* visitor) noexcept override
            {
                return visitor->dispatch(shared_from_this());
            }
            /* @fn:has_dependence[Check if the operator depends on a given (physical) variable.]
               @param[in]:variable[The given (physical) variable.]
               @return:[Boolean indicates the dependence.] */
            virtual inline bool has_dependence(const std::shared_ptr<tSymbolic::Symbol> variable) const noexcept override
            {
                switch(variable->type_id()) {
                    case typeid(tSymbolic::NuclearPerturbation):
                        return true;
                    case typeid(tSymbolic::MagneticPerturbation):
                        return false;
                    case typeid(tSymbolic::RotationalPerturbation):
                        return false;
                    default:
                        return false;
                }
            }
        protected:
            /**/
            virtual inline tGlueCore::DelegateState
            invoke_int_engine(IntegratorState& stateIntegrator,
                              std::vector<tMatrix::BlockMat>& intOper) noexcept override
            {
                return m_int_engine.invoke(stateIntegrator, shared_from_this(), intOper);
            }
            virtual inline tGlueCore::DelegateState
            invoke_exp_engine(IntegratorState& stateIntegrator,
                              const std::vector<tMatrix::BlockMat>& states,
                              std::vector<tReal>& expOper) noexcept override
            {
                return m_exp_engine.invoke(stateIntegrator, shared_from_this(), states, expOper);
            }
        private:
            /*@@ Internal data for representing Cartesian multipole moments. */
            tReal m_scale_const;
            unsigned int m_order_multipole;
            unsigned int m_order_electronic;
    };
}
