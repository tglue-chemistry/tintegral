/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of Cartesian multipole moments class.

   2020-06-02, Bin Gao:
   * rewrite based on tSymbolic library and temporarily replace the visitor
     pattern with run-time type, which may be changed to pattern match

   2019-04-09, Bin Gao:
   * change to visitor pattern, because usually users do not need to implement
     routines for new one-electron operators by themselves; as such, the class
     OneElecEngine is merged into OneElecOper

   2019-02-08, Bin Gao:
   * new design by introducing classes OneElecEngine and OneElecOper

   2019-02-06, Bin Gao:
   * built on top of Delegate class and Symbol class

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>
#include <utility>

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/QuantChem/Operator.hpp"

//#include "tMatrix/BlockMat.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* == Cartesian Multipole Moments

       The first *concrete* one-electron operator is Cartesian multipole
       moments, which can also be used to compute overlap integrals.

       IMPORTANT: The validity of one-electron operators for periodic systems
       and the use of London phase factors need to be considered, either at
       the level of operators or integrators. It is still under consideration.

       @class:CartMultMoment[Cartesian multipole moments.]

       Cartesian multipole moments in the library tIntegral are defined as
       stem:[
         \bar{C}\boldsymbol{r}_{M}^{\boldsymbol{m}}
         \boldsymbol{\partial}_{\boldsymbol{r}}^{\boldsymbol{n}}
       ], where

       [horizontal]
       stem:[\bar{C}]:: Scale constant.
       stem:[\boldsymbol{M}]:: Origin of multipole expansion. The dipole's
         origin is usually taken at the center of mass to simplify the treatment
         of rotational-electronic coupling, see
         "`Theory of Molecular Rydberg States`" by M. S. Child, pp. 78 (2011).
       stem:[\boldsymbol{m}]:: Order of Cartesian multipole moments.
       stem:[\boldsymbol{n}]:: Order of derivatives with respect to electron coordinates.

       Cartesian multipole moments can be created by its constructor as follows, */
    template<typename CentreType, typename RealType=double>
    class CartMultMoment: virtual public tSymbolic::OneElecOperator
                          //public std::enable_shared_from_this<CartMultMoment>
    {
        public:
            /* @fn:CartMultMoment()[Constructor.]
               @param[in]:scaleConst[Scale constant stem:[\bar{C}].]
               @param[in]:idxOrigin[Index of the origin of multipole expansion stem:[\boldsymbol{M}].]
               @param[in]:coordOrigin[Coordinates of the origin stem:[\boldsymbol{M}].]
               @param[in]:orderMultipole[Order of Cartesian multipole moments (stem:[\boldsymbol{m}]).]
               @param[in]:orderElectronic[Order of derivatives with respect to electron coordinates (stem:[\boldsymbol{n}]).] */
            explicit CartMultMoment(const std::shared_ptr<CentreType> origin,
                                    const RealType scaleConst=1.0,
                                    const unsigned int orderMultipole=1,
                                    const unsigned int orderElectronic=0) noexcept:
                m_origin(origin),
                m_scale_const(scaleConst),
                m_order_multipole(orderMultipole),
                m_order_electronic(orderElectronic) {}
            /* @fn:~CartMultMoment()[Deconstructor.] */
            virtual ~CartMultMoment() noexcept = default;
            /* The following functions are required by the one-electron
               operator base class, and can be futher overridden by any derived
               class of `CartMultMoment`.

               @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<tSymbolic::Symbol> get_copy() noexcept override
            {
                return std::make_shared<CartMultMoment>(std::move(*this));
            }
            virtual std::string to_string() const noexcept override
            {
                return "{cart-mult-moment: {origin: "+m_origin->to_string()
                    +", scale-constant: "+std::to_string(m_scale_const)
                    +", order: "+std::to_string(m_order_multipole)
                    +", electronic-derivative: "+std::to_string(m_order_electronic)+"}}";
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<tSymbolic::Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<CartMultMoment> other_cast = std::dynamic_pointer_cast<CartMultMoment>(other);
                    return m_scale_const==other_cast->m_scale_const
                        && m_origin->equal_to(other_cast->m_origin)
                        && m_order_multipole==other_cast->m_order_multipole
                        && m_order_electronic==other_cast->m_order_electronic;
                }
                else {
                    return false;
                }
            }
            /* Last but not least, there are a few functions specialized for
               the Cartesian multipole moments.

               @fn:get_scale_const[Get scale constant.]
               @return:[The scale constant.] */
            inline RealType get_scale_const() const noexcept { return m_scale_const; }
            /* @fn:get_origin[Get origin of multipole expansion.]
               @return:[The origin of multipole expansion.] */
            inline std::shared_ptr<CentreType> get_origin() const noexcept { return m_origin; }
            /* @fn:get_multipole_order[Get order of Cartesian multipole moments.]
               @return:[Order of Cartesian multipole moments.] */
            inline unsigned int get_multipole_order() const noexcept { return m_order_multipole; }
            /* @fn:get_electronic_order[Get order of electronic derivatives.]
               @return:[Order of electronic derivatives.] */
            inline unsigned int get_electronic_order() const noexcept { return m_order_electronic; }
        protected:
            /* @fn:oper_max_diff_order[Get the maximum order of differentiation of the operator with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation of the operator.] */
            virtual unsigned int oper_max_diff_order(const std::shared_ptr<tSymbolic::Symbol>& var) const noexcept override
            {
                //FIXME: to implement
                return 0;
                //if (var->type_id()==typeid(tSymbolic::PhysObject<CentreType>)) {
                //    auto var_cast = std::dynamic_pointer_cast<tSymbolic::PhysObject<CentreType>>(var);
                //    return var_cast->has_point(m_origin) ? m_order_multipole : 0;
                //}
                //else {
                //    return 0;
                //}
            }
        private:
            /*@@ Internal data for representing Cartesian multipole moments. */
            RealType m_scale_const;
            std::shared_ptr<CentreType> m_origin;
            unsigned int m_order_multipole;
            unsigned int m_order_electronic;
            //std::shared_ptr<RecurIndex> m_idx_mult_moment;
            //std::shared_ptr<RecurIndex> m_idx_elec_derivative;
    };
}
