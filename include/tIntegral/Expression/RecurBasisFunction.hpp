/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for different basis functions used for
   recurrence relations.

   2020-01-29, Bin Gao:
   * introduce the RecurFunction class and its derived classes for deriving
     recurrence relations automatically

   2019-05-29, Bin Gao:
   * add abstract recurrence relation symbol visitor

   2018-02-08, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurSymbol.hpp"

namespace tIntegral
{
    /* Primitive Hermite or Cartesian Gaussian functions */
    class RecurGaussianFunction: virtual public RecurFunction,
                                 public std::enable_shared_from_this<RecurGaussianFunction>
    {
        public:
            explicit RecurGaussianFunction(const std::shared_ptr<RecurIndex> idxExponent,
                                           const std::shared_ptr<RecurIndex> idxAngular,
                                           const std::string name=std::string("braBasis"),
                                           const std::string centreType=std::string("CentreType"),
                                           const std::string realType=std::string("RealType")) noexcept:
                RecurFunction(name, "tBasisSet::GaussianFunction<"+centreType+","+realType+">"),
                m_idx_exponent(idxExponent),
                m_idx_angular(idxAngular) {}
            virtual ~RecurGaussianFunction() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            //FIXME: may be removed if RecurRelation::get_expression() implemented
            inline std::shared_ptr<RecurIndex> get_idx_exponent() const noexcept { return m_idx_exponent; }
            inline std::shared_ptr<RecurIndex> get_idx_angular() const noexcept { return m_idx_angular; }
        private:
            std::shared_ptr<RecurIndex> m_idx_exponent;
            std::shared_ptr<RecurIndex> m_idx_angular;
    };

    /* Contracted Gaussian type orbitals (GTO) */
    //class RecurContractedGTO: virtual public RecurFunction
    //{
    //    public:
    //        explicit RecurContractedGTO(const std::string name,
    //                                    const bool isSpherical=true) noexcept:
    //            RecurFunction(name),
    //            m_is_spherical(isSpherical) {}
    //        virtual ~RecurContractedGTO() noexcept = default;
    //        virtual std::string type_name() const noexcept override
    //        {
    //            return std::string("ContractedGTO");
    //        }
    //    private:
    //        bool m_is_spherical;
    //};
}
