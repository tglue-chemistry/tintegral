/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for recurrence relation symbols.

   2020-01-29, Bin Gao:
   * introduce the RecurFunction class and its derived classes for deriving
     recurrence relations automatically

   2019-05-29, Bin Gao:
   * add abstract recurrence relation symbol visitor

   2018-02-08, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <set>
#include <string>
#include <vector>

#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"

namespace tIntegral
{
    //@@ Forward declaration of recurrence relation expression visitor.
    class RecurExprVisitor;

    /* Base classes */
    //FIXME: rewrite based on tSymbolic library

    /* Symbols used in recurrence relations */
    class RecurSymbol
    {
        public:
            explicit RecurSymbol() noexcept = default;
            virtual ~RecurSymbol() noexcept = default;
            /* @fn:accept[Take a visitor of `RecurSymbol` class.]
               @param[in]:visitor[The visitor which should implement a member function `dispatch()`.]
               @return:[Boolean indicates if the visitor worked normally or with error.]
               FIXME: will be removed by pattern matching */
            virtual bool accept(RecurExprVisitor* visitor) noexcept = 0;
            /* Convert to a string */
            virtual std::string to_string() const noexcept = 0;
            /* Get the name of the symbol */
            virtual std::string get_name() const noexcept = 0;
            /* Get the recurrence-relation direction (stem:[x], stem:[y],
               stem:[z] or stem:[xyz]) of the symbol */
            virtual RecurDirection get_direction() const noexcept { return RecurDirection::XYZ; }
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept
            {
                return std::vector<std::shared_ptr<RecurIndex>>();
            }
            /* Get symbols that depends on */
            virtual std::set<std::shared_ptr<RecurSymbol>> get_dependency() const noexcept
            {
                return std::set<std::shared_ptr<RecurSymbol>>();
            }
            /* Get names of indices */
            inline std::vector<std::string> get_index_name() const noexcept
            {
                auto indices = get_indices();
                std::vector<std::string> idx_names;
                idx_names.reserve(indices.size());
                for (auto const& index: indices) {
                    idx_names.push_back(index->get_name());
                }
                return idx_names;
            }
            /* Get names of dependency */
            inline std::vector<std::string> get_dependency_name() const noexcept
            {
                auto dependency = get_dependency();
                std::vector<std::string> dependency_names;
                for (auto const& each_dep: dependency) {
                    dependency_names.push_back(each_dep->get_name());
                }
                return dependency_names;
            }
    };

    /* Funtions that are integrated and from which recurrence relations are derived */
    class RecurFunction: virtual public RecurSymbol
    {
        public:
            explicit RecurFunction(const std::string funName, const std::string typeName) noexcept:
                m_fun_name(funName),
                m_type_name(typeName) {}
            virtual ~RecurFunction() noexcept = default;
            /* Get name of the function */
            virtual std::string get_name() const noexcept override { return m_fun_name; }
            /* Get the type name of the function */
            virtual std::string type_name() const noexcept { return m_type_name; }
            /*FIXME: More functions involved RecurIndex, for deriving recurrence relations */
            //std::shared_ptr<RecurSymbol> left_shift(std::shared_ptr<RecurIndex>) noexcept
            //std::shared_ptr<RecurSymbol> left_shift(std::shared_ptr<RecurSymbol>) noexcept
        private:
            std::string m_fun_name;
            std::string m_type_name;
    };

    /* Variables of functions used in recurrence relations */
    class RecurVariable: virtual public RecurSymbol
    {
        public:
            explicit RecurVariable(const std::string name,
                                   const std::shared_ptr<RecurFunction> integrand,
                                   const std::string method,
                                   const std::set<std::shared_ptr<RecurSymbol>> dependency) noexcept:
                m_name(name),
                m_integrand(integrand),
                m_method(method),
                m_dependency(dependency) {}
            virtual ~RecurVariable() noexcept = default;
            /* Get the name of the variable */
            virtual std::string get_name() const noexcept override
            {
                return m_name;
            }
            /* Get symbols that depends on, and as parameters of the integrand's method */
            virtual std::set<std::shared_ptr<RecurSymbol>> get_dependency() const noexcept override
            {
                return m_dependency;
            }
            /* Get the integrand */
            inline std::shared_ptr<RecurFunction> get_integrand() const noexcept
            {
                return m_integrand;
            }
            /* Get the name of the integrand */
            inline std::string get_integrand_name() const noexcept
            {
                return m_integrand->get_name();
            }
            /* Get the name of the integrand's method */
            inline std::string get_integrand_method() const noexcept
            {
                return m_method;
            }
        private:
            std::string m_name;
            std::shared_ptr<RecurFunction> m_integrand;
            std::string m_method;
            std::set<std::shared_ptr<RecurSymbol>> m_dependency;
    };

    /* Derived classes */

    /* Numbers */
    class RecurNumber: virtual public RecurSymbol,
                       public std::enable_shared_from_this<RecurNumber>
    {
        public:
            explicit RecurNumber(const double value) noexcept:
                m_value(value) {}
            virtual ~RecurNumber() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the name of the symbol */
            virtual std::string get_name() const noexcept override;
        private:
            double m_value;
    };

    /* Scalar variable */
    class RecurScalarVar: virtual public RecurVariable,
                          public std::enable_shared_from_this<RecurScalarVar>
    {
        public:
            explicit RecurScalarVar(
                const std::string name,
                const std::shared_ptr<RecurFunction> integrand,
                const std::string method,
                const std::set<std::shared_ptr<RecurSymbol>> dependency=std::set<std::shared_ptr<RecurSymbol>>()
            ) noexcept:
                RecurVariable(name, integrand, method, dependency) {}
            virtual ~RecurScalarVar() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
    };

    /* Cartesian variable */
    class RecurCartesianVar: virtual public RecurVariable,
                             public std::enable_shared_from_this<RecurCartesianVar>
    {
        public:
            explicit RecurCartesianVar(
                const std::string name,
                const std::shared_ptr<RecurFunction> integrand,
                const std::string method,
                const std::set<std::shared_ptr<RecurSymbol>> dependency=std::set<std::shared_ptr<RecurSymbol>>(),
                const RecurDirection direction=RecurDirection::X
            ) noexcept:
                RecurVariable(name, integrand, method, dependency),
                m_direction(direction) {}
            virtual ~RecurCartesianVar() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the recurrence-relation direction (stem:[x], stem:[y],
               stem:[z] or stem:[xyz]) of the variable */
            virtual RecurDirection get_direction() const noexcept override;
        private:
            RecurDirection m_direction;
    };

    /* Vector of scalars */
    class RecurScalarVec: virtual public RecurVariable,
                          public std::enable_shared_from_this<RecurScalarVec>
    {
        public:
            explicit RecurScalarVec(
                const std::string name,
                const std::shared_ptr<RecurFunction> integrand,
                const std::string method,
                const std::set<std::shared_ptr<RecurSymbol>> dependency=std::set<std::shared_ptr<RecurSymbol>>(),
                const std::vector<std::shared_ptr<RecurIndex>> indices=std::vector<std::shared_ptr<RecurIndex>>()
            ) noexcept:
                RecurVariable(name, integrand, method, dependency),
                m_indices(indices) {}
            virtual ~RecurScalarVec() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
        private:
            std::vector<std::shared_ptr<RecurIndex>> m_indices;
    };

    /* Vector of Cartesian variables */
    class RecurCartesianVec: virtual public RecurVariable,
                              public std::enable_shared_from_this<RecurCartesianVec>
    {
        public:
            explicit RecurCartesianVec(
                const std::string name,
                const std::shared_ptr<RecurFunction> integrand,
                const std::string method,
                const std::set<std::shared_ptr<RecurSymbol>> dependency=std::set<std::shared_ptr<RecurSymbol>>(),
                const RecurDirection direction=RecurDirection::X,
                const std::vector<std::shared_ptr<RecurIndex>> indices=std::vector<std::shared_ptr<RecurIndex>>()
            ) noexcept:
                RecurVariable(name, integrand, method, dependency),
                m_direction(direction),
                m_indices(indices) {}
            virtual ~RecurCartesianVec() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the recurrence-relation direction (stem:[x], stem:[y],
               stem:[z] or stem:[xyz]) of the variable */
            virtual RecurDirection get_direction() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
        private:
            RecurDirection m_direction;
            std::vector<std::shared_ptr<RecurIndex>> m_indices;
    };

    /* Order of an index */
    class RecurIdxOrder: virtual public RecurSymbol,
                         public std::enable_shared_from_this<RecurIdxOrder>
    {
        public:
            explicit RecurIdxOrder(const std::shared_ptr<RecurIndex> index) noexcept:
                m_index(index) {}
            virtual ~RecurIdxOrder() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the name of the order */
            virtual std::string get_name() const noexcept override;
            /* Get the recurrence-relation direction of the order */
            virtual RecurDirection get_direction() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
        private:
            std::shared_ptr<RecurIndex> m_index;
    };

    /* Recurrence relation terms (on the right hand side) */
    class RecurTerm: virtual public RecurSymbol,
                     public std::enable_shared_from_this<RecurTerm>
    {
        public:
            /* Whether a vector and decrements along x, y and z directions,
               where the x direction contains the decrement if it is a scalar
               index */
            explicit RecurTerm(const std::vector<std::shared_ptr<RecurIndex>> indices) noexcept:
                m_indices(indices) {}
            virtual ~RecurTerm() noexcept = default;
            /* @fn:accept[Take a visitor of `RecurTerm` class.]
               @param[in]:visitor[The visitor.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the name of the order */
            virtual std::string get_name() const noexcept override;
            /* Get indices involved in the term */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            /* Return the order of an index */
            inline RecurVector get_order(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                for (auto const& each_index: m_indices) {
                    if (each_index->equal_to(index)) return each_index->get_order();
                }
                return make_invalid_vector();
            }
        private:
            std::vector<std::shared_ptr<RecurIndex>> m_indices;
    };

    /* Arithmetic operation */
    class RecurArithOperation: virtual public RecurSymbol
    {
        public:
            explicit RecurArithOperation(const std::shared_ptr<RecurSymbol> lhs,
                                         const std::shared_ptr<RecurSymbol> rhs,
                                         const std::string name) noexcept:
                m_name(name),
                m_lhs(lhs),
                m_rhs(rhs) {}
            virtual ~RecurArithOperation() noexcept = default;
            /* Get the name of the operation */
            virtual std::string get_name() const noexcept override { return m_name; }
            /* Get the recurrence-relation direction of the operation, which is
               either RecurDirection::XYZ or RecurDirection::X; The later
               only implies that the operation has Cartesian components and it
               does not matter if the direction is stem:[x], stem:[y] or
               stem:[z] */
            virtual RecurDirection get_direction() const noexcept override
            {
                if (m_lhs->get_direction()!=RecurDirection::XYZ) {
                    return RecurDirection::X;
                }
                else {
                    if (m_rhs->get_direction()==RecurDirection::XYZ) {
                        return RecurDirection::XYZ;
                    }
                    else {
                        return RecurDirection::X;
                    }
                }
            }
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override
            {
                /* We require that LHS and RHS have the same indices */
                return m_lhs->get_indices();
            }
            /* Get symbols that depends on */
            virtual std::set<std::shared_ptr<RecurSymbol>> get_dependency() const noexcept override
            {
                auto lhs_dependency = m_lhs->get_dependency();
                auto rhs_dependency = m_rhs->get_dependency();
                lhs_dependency.insert(rhs_dependency.cbegin(), rhs_dependency.cend());
                return lhs_dependency;
            }
            /* Get LHS */
            std::shared_ptr<RecurSymbol> get_lhs() const noexcept { return m_lhs; }
            /* Get RHS */
            std::shared_ptr<RecurSymbol> get_rhs() const noexcept { return m_rhs; }
        private:
            std::shared_ptr<RecurSymbol> m_lhs;
            std::shared_ptr<RecurSymbol> m_rhs;
            std::string m_name;
    };

    /* Addition that can be used for a variable in recurrence relations when a
       name is given */
    class RecurAddition: virtual public RecurArithOperation,
                         public std::enable_shared_from_this<RecurAddition>
    {
        public:
            explicit RecurAddition(const std::shared_ptr<RecurSymbol> augend,
                                   const std::shared_ptr<RecurSymbol> addend,
                                   const std::string name=std::string()) noexcept:
                RecurArithOperation(augend, addend, name) {}
            virtual ~RecurAddition() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
    };

    /* Subtraction that can be used for a variable in recurrence relations when a
       name is given */
    class RecurSubtraction: virtual public RecurArithOperation,
                            public std::enable_shared_from_this<RecurSubtraction>
    {
        public:
            explicit RecurSubtraction(const std::shared_ptr<RecurSymbol> minuend,
                                      const std::shared_ptr<RecurSymbol> subtrahend,
                                      const std::string name=std::string()) noexcept:
                RecurArithOperation(minuend, subtrahend, name) {}
            virtual ~RecurSubtraction() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the recurrence-relation direction of the subtraction */
            virtual RecurDirection get_direction() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            /* Get symbols that depends on */
            virtual std::set<std::shared_ptr<RecurSymbol>> get_dependency() const noexcept override;
    };

    /* Multiplication that can be used for a variable in recurrence relations when a
       name is given */
    class RecurMultiplication: virtual public RecurArithOperation,
                               public std::enable_shared_from_this<RecurMultiplication>
    {
        public:
            explicit RecurMultiplication(const std::shared_ptr<RecurSymbol> multiplier,
                                         const std::shared_ptr<RecurSymbol> multiplicand,
                                         const std::string name=std::string()) noexcept:
                RecurArithOperation(multiplier, multiplicand, name) {}
            virtual ~RecurMultiplication() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
    };

    /* Division that can be used for a variable in recurrence relations when a
       name is given */
    class RecurDivision: virtual public RecurArithOperation,
                         public std::enable_shared_from_this<RecurDivision>
    {
        public:
            explicit RecurDivision(const std::shared_ptr<RecurSymbol> dividend,
                                   const std::shared_ptr<RecurSymbol> divisor,
                                   const std::string name=std::string()) noexcept:
                RecurArithOperation(dividend, divisor, name) {}
            virtual ~RecurDivision() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
    };

    /* Parentheses */
    class RecurParentheses: virtual public RecurSymbol,
                            public std::enable_shared_from_this<RecurParentheses>
    {
        public:
            explicit RecurParentheses(const std::shared_ptr<RecurSymbol> expression,
                                      const std::string name=std::string()) noexcept:
                m_name(name),
                m_expression(expression) {}
            virtual ~RecurParentheses() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get the name of the variable */
            virtual std::string get_name() const noexcept override;
            /* Get the recurrence-relation direction of the division */
            virtual RecurDirection get_direction() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            /* Get symbols that depends on */
            virtual std::set<std::shared_ptr<RecurSymbol>> get_dependency() const noexcept override;
            inline std::shared_ptr<RecurSymbol> get_expression() const noexcept { return m_expression; }
        private:
            std::shared_ptr<RecurSymbol> m_expression;
            std::string m_name;
    };
}
