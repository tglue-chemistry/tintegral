/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for different operators used for recurrence
   relations.

   2020-06-09, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurSymbol.hpp"

namespace tIntegral
{
    //FIXME: generate tIntegral/CartMultMoment.hpp??
    /* Cartesian multipole moment operator */
    class RecurCartMultMoment: virtual public RecurFunction,
                               public std::enable_shared_from_this<RecurCartMultMoment>
    {
        public:
            explicit RecurCartMultMoment(const std::shared_ptr<RecurIndex> idxMoment,
                                         const std::shared_ptr<RecurIndex> idxElDeriv,
                                         const std::string name=std::string("oneOper"),
                                         const std::string centreType=std::string("CentreType"),
                                         const std::string realType=std::string("RealType")) noexcept:
                RecurFunction(name, "CartMultMoment<"+centreType+","+realType+">"),
                m_idx_moment(idxMoment),
                m_idx_el_deriv(idxElDeriv) {}
            virtual ~RecurCartMultMoment() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            //FIXME: may be removed if RecurRelation::get_expression() implemented
            inline std::shared_ptr<RecurIndex> get_idx_moment() const noexcept
            {
                return m_idx_moment;
            }
        private:
            std::shared_ptr<RecurIndex> m_idx_moment;
            std::shared_ptr<RecurIndex> m_idx_el_deriv;
    };

    /* Unprojected part of effective core potential */
    class RecurECPUnprojected: virtual public RecurFunction,
                               public std::enable_shared_from_this<RecurECPUnprojected>
    {
        public:
            explicit RecurECPUnprojected(const std::shared_ptr<RecurIndex> idxRadial,
                                         const std::shared_ptr<RecurIndex> idxBessel,
                                         const std::shared_ptr<RecurIndex> idxGeometrical,
                                         const std::string name=std::string("oneOper"),
                                         const std::string centreType=std::string("CentreType"),
                                         const std::string realType=std::string("RealType")) noexcept:
                RecurFunction(name, "ECPUnprojected<"+centreType+","+realType+">"),
                m_idx_radial(idxRadial),
                m_idx_bessel(idxBessel),
                m_idx_geometrical(idxGeometrical) {}
            virtual ~RecurECPUnprojected() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            //FIXME: may be removed if RecurRelation::get_expression() implemented
            inline std::shared_ptr<RecurIndex> get_idx_radial() const noexcept
            {
                return m_idx_radial;
            }
            inline std::shared_ptr<RecurIndex> get_idx_bessel() const noexcept
            {
                return m_idx_bessel;
            }
            inline std::shared_ptr<RecurIndex> get_idx_geometrical() const noexcept
            {
                return m_idx_geometrical;
            }
        private:
            /*@@ Index of the power of function stem:[r^{n'}] */
            std::shared_ptr<RecurIndex> m_idx_radial;
            /* Index of the order of the scaled modified spherical Bessel function of the first kind */
            std::shared_ptr<RecurIndex> m_idx_bessel;
            /* Index of the order of geometrical derivatives with respect to the ECP centre */
            std::shared_ptr<RecurIndex> m_idx_geometrical;
    };

    /* Projected part of effective core potential */
    class RecurECPProjected: virtual public RecurFunction,
                             public std::enable_shared_from_this<RecurECPProjected>
    {
        public:
            explicit RecurECPProjected(const std::shared_ptr<RecurIndex> idxRadial,
                                       const std::shared_ptr<RecurIndex> idxBesselBra,
                                       const std::shared_ptr<RecurIndex> idxBesselKet,
                                       const std::shared_ptr<RecurIndex> idxHarmonicBra,
                                       const std::shared_ptr<RecurIndex> idxHarmonicKet,
                                       const std::shared_ptr<RecurIndex> idxGeometrical,
                                       const std::string name=std::string("oneOper"),
                                       const std::string centreType=std::string("CentreType"),
                                       const std::string realType=std::string("RealType")) noexcept:
                RecurFunction(name, "ECPProjected<"+centreType+","+realType+">"),
                m_idx_radial(idxRadial),
                m_idx_bessel_bra(idxBesselBra),
                m_idx_bessel_ket(idxBesselKet),
                m_idx_harmonic_bra(idxHarmonicBra),
                m_idx_harmonic_ket(idxHarmonicKet),
                m_idx_geometrical(idxGeometrical) {}
            virtual ~RecurECPProjected() noexcept = default;
            virtual bool accept(RecurExprVisitor* visitor) noexcept override;
            /* Convert to a string */
            virtual std::string to_string() const noexcept override;
            /* Get indices involved in the symbol */
            virtual std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept override;
            //FIXME: may be removed if RecurRelation::get_expression() implemented
            inline std::shared_ptr<RecurIndex> get_idx_radial() const noexcept
            {
                return m_idx_radial;
            }
            inline std::shared_ptr<RecurIndex> get_idx_bessel_bra() const noexcept
            {
                return m_idx_bessel_bra;
            }
            inline std::shared_ptr<RecurIndex> get_idx_bessel_ket() const noexcept
            {
                return m_idx_bessel_ket;
            }
            inline std::shared_ptr<RecurIndex> get_idx_harmonic_bra() const noexcept
            {
                return m_idx_harmonic_bra;
            }
            inline std::shared_ptr<RecurIndex> get_idx_harmonic_ket() const noexcept
            {
                return m_idx_harmonic_ket;
            }
            inline std::shared_ptr<RecurIndex> get_idx_geometrical() const noexcept
            {
                return m_idx_geometrical;
            }
        private:
            /*@@ Index of the power of function stem:[r^{n'}] */
            std::shared_ptr<RecurIndex> m_idx_radial;
            /* Index of the order of the scaled modified spherical Bessel function of the first kind */
            std::shared_ptr<RecurIndex> m_idx_bessel_bra;
            std::shared_ptr<RecurIndex> m_idx_bessel_ket;
            /* Index of the order of the Hermite Gaussian transformed from real spherical harmonics */
            std::shared_ptr<RecurIndex> m_idx_harmonic_bra;
            std::shared_ptr<RecurIndex> m_idx_harmonic_ket;
            /* Index of the order of geometrical derivatives with respect to the ECP centre */
            std::shared_ptr<RecurIndex> m_idx_geometrical;
    };
}
