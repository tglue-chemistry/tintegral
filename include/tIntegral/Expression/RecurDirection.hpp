/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for directions of recurrence relations.

   2019-06-11, Bin Gao:
   * implement stringify functions related to directions of recurrence
     relations

   2019-06-05, Bin Gao:
   * moved from RecurSymbol header file

   2018-02-08, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/Convert.hpp"

#include <string>
#include <type_traits>
#include <vector>

namespace tIntegral
{
    /* Direction of recurrence relations */
    enum class RecurDirection
    {
        X=0,
        Y=1,
        Z=2,
        /* Internal use only */
        Null,
        XY,
        YX,
        XZ,
        ZX,
        YZ,
        ZY,
        XYZ
    };

    /* Get string of a direction */
    inline std::string stringify_direction(const RecurDirection direction) noexcept
    {
        switch (direction) {
            case RecurDirection::X:
                return std::string("x");
            case RecurDirection::Y:
                return std::string("y");
            case RecurDirection::Z:
                return std::string("z");
            case RecurDirection::Null:
                return std::string("null");
            case RecurDirection::XY:
                return std::string("xy");
            case RecurDirection::YX:
                return std::string("yx");
            case RecurDirection::XZ:
                return std::string("xz");
            case RecurDirection::ZX:
                return std::string("zx");
            case RecurDirection::YZ:
                return std::string("yz");
            case RecurDirection::ZY:
                return std::string("zy");
            case RecurDirection::XYZ:
                return std::string("xyz");
            default:
                return "??"+std::to_string(tGlueCore::to_integral(direction))+"??";
        }
    }

    /* Get string of directions */
    inline std::string stringify_direction(const std::vector<RecurDirection>& directions) noexcept
    {
        std::string str_directions("[");
        for (auto const& each_direction: directions) {
            str_directions += stringify_direction(each_direction)+',';
        }
        str_directions.back() = ']';
        return str_directions;
    }

    /* Check if the first direction is included (covered) by the second one */
    inline bool is_direction_included(const RecurDirection first, const RecurDirection second) noexcept
    {
        switch (first) {
            case RecurDirection::X:
                if (second==RecurDirection::X ||
                    second==RecurDirection::XY ||
                    second==RecurDirection::YX ||
                    second==RecurDirection::XZ ||
                    second==RecurDirection::ZX ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            case RecurDirection::Y:
                if (second==RecurDirection::Y ||
                    second==RecurDirection::XY ||
                    second==RecurDirection::YX ||
                    second==RecurDirection::YZ ||
                    second==RecurDirection::ZY ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            case RecurDirection::Z:
                if (second==RecurDirection::Z ||
                    second==RecurDirection::XZ ||
                    second==RecurDirection::ZX ||
                    second==RecurDirection::YZ ||
                    second==RecurDirection::ZY ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            case RecurDirection::Null:
                return false;
            case RecurDirection::XY:
            case RecurDirection::YX:
                if (second==RecurDirection::XY ||
                    second==RecurDirection::YX ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            case RecurDirection::XZ:
            case RecurDirection::ZX:
                if (second==RecurDirection::XZ ||
                    second==RecurDirection::ZX ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            case RecurDirection::YZ:
            case RecurDirection::ZY:
                if (second==RecurDirection::YZ ||
                    second==RecurDirection::ZY ||
                    second==RecurDirection::XYZ) {
                    return true;
                }
                else {
                    return false;
                }
            /* RecurDirection::XYZ */
            default:
                return second==RecurDirection::XYZ ? true : false;
        }
    }

    /* Remove the second direction from the first one */
    inline void remove_direction(RecurDirection& first, const RecurDirection second) noexcept
    {
        switch (second) {
            case RecurDirection::X:
                if (first==RecurDirection::X) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XY || first==RecurDirection::YX) {
                    first = RecurDirection::Y;
                }
                else if (first==RecurDirection::XZ || first==RecurDirection::ZX) {
                    first = RecurDirection::Z;
                }
                else if (first==RecurDirection::XYZ) {
                    first = RecurDirection::YZ;
                }
                break;
            case RecurDirection::Y:
                if (first==RecurDirection::Y) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XY || first==RecurDirection::YX) {
                    first = RecurDirection::X;
                }
                else if (first==RecurDirection::YZ || first==RecurDirection::ZY) {
                    first = RecurDirection::Z;
                }
                else if (first==RecurDirection::XYZ) {
                    first = RecurDirection::XZ;
                }
                break;
            case RecurDirection::Z:
                if (first==RecurDirection::Z) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XZ || first==RecurDirection::ZX) {
                    first = RecurDirection::X;
                }
                else if (first==RecurDirection::YZ || first==RecurDirection::ZY) {
                    first = RecurDirection::Y;
                }
                else if (first==RecurDirection::XYZ) {
                    first = RecurDirection::XY;
                }
                break;
            case RecurDirection::Null:
                break;
            case RecurDirection::XY:
            case RecurDirection::YX:
                if (first==RecurDirection::X || first==RecurDirection::Y ||
                    first==RecurDirection::XY || first==RecurDirection::YX) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XZ || first==RecurDirection::ZX ||
                         first==RecurDirection::YZ || first==RecurDirection::ZY ||
                         first==RecurDirection::XYZ) {
                    first = RecurDirection::Z;
                }
                break;
            case RecurDirection::XZ:
            case RecurDirection::ZX:
                if (first==RecurDirection::X || first==RecurDirection::Z ||
                    first==RecurDirection::XZ || first==RecurDirection::ZX) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XY || first==RecurDirection::YX ||
                         first==RecurDirection::YZ || first==RecurDirection::ZY ||
                         first==RecurDirection::XYZ) {
                    first = RecurDirection::Y;
                }
                break;
            case RecurDirection::YZ:
            case RecurDirection::ZY:
                if (first==RecurDirection::Y || first==RecurDirection::Z ||
                    first==RecurDirection::YZ || first==RecurDirection::ZY) {
                    first = RecurDirection::Null;
                }
                else if (first==RecurDirection::XY || first==RecurDirection::YX ||
                         first==RecurDirection::XZ || first==RecurDirection::ZX ||
                         first==RecurDirection::XYZ) {
                    first = RecurDirection::X;
                }
                break;
            /* RecurDirection::XYZ */
            default:
                first = RecurDirection::Null;
                break;
        }
    }

    /* Transform a given direction under a given direction of recurrence
       relaiton */
    inline RecurDirection transform_direction(const RecurDirection recurDirection,
                                              const RecurDirection originalDirection) noexcept
    {
        switch (recurDirection) {
            case RecurDirection::X:
                switch (originalDirection) {
                    case RecurDirection::X:
                        return RecurDirection::X;
                    case RecurDirection::Y:
                        return RecurDirection::Y;
                    /* RecurDirection::Z */
                    default:
                        return RecurDirection::Z;
                }
            case RecurDirection::Y:
                switch (originalDirection) {
                    case RecurDirection::X:
                        return RecurDirection::Y;
                    case RecurDirection::Y:
                        return RecurDirection::Z;
                    /* RecurDirection::Z */
                    default:
                        return RecurDirection::X;
                }
            /* RecurDirection::Z */
            default:
                switch (originalDirection) {
                    case RecurDirection::X:
                        return RecurDirection::Z;
                    case RecurDirection::Y:
                        return RecurDirection::X;
                    /* RecurDirection::Z */
                    default:
                        return RecurDirection::Y;
                }
        }
    }

    /* Transform a given direction under a given direction of recurrence
       relaiton, but return 0 as RecurDirection::X, 1 as RecurDirection::Y and
       2 as RecurDirection::Z */
    inline unsigned int transform_direction_indexed(const RecurDirection recurDirection,
                                                    const RecurDirection originalDirection) noexcept
    {
        return tGlueCore::to_integral(transform_direction(recurDirection, originalDirection));
        //switch (transform_direction(recurDirection, originalDirection)) {
        //    case RecurDirection::X:
        //        return 0;
        //    case RecurDirection::Y:
        //        return 1;
        //    default:
        //        return 2;
        //}
    }

    /* Return the third corner with two given corners of a triangle */
    inline RecurDirection get_triangle_corner(const RecurDirection firstCorner,
                                              const RecurDirection secondCorner) noexcept
    {
        switch (firstCorner) {
            case RecurDirection::X:
                return secondCorner==RecurDirection::Y ? RecurDirection::Z : RecurDirection::Y;
            case RecurDirection::Y:
                return secondCorner==RecurDirection::X ? RecurDirection::Z : RecurDirection::X;
            /* z corner */
            default:
                return secondCorner==RecurDirection::X ? RecurDirection::Y : RecurDirection::X;
        }
    }
}
