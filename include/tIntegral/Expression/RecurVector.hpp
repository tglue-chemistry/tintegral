/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for orders, increments and decrements of
   recurrence relations.

   2019-06-26, Bin Gao:
   * changed from previous RecurVector class
*/

#pragma once

#include <array>
#include <limits>
#include <string>

#include "tGlueCore/Stringify.hpp"

#if defined(TINTEGRAL_DEBUG)
#include "tGlueCore/Logger.hpp"
#include "tIntegral/Settings.hpp"
#endif

#include "tIntegral/Recurrence/RecurDirection.hpp"

namespace tIntegral
{
    /* Order, increment or decrement of an index */
    class RecurVector
    {
        public:
            /* Construct by transforming an array [i,j,k] under a given
               direction of recurrence relaiton, which gives

               # x direction [i,j,k]
               # y direction [k,i,j]
               # z direction [j,k,i] */
            explicit RecurVector(const std::array<int,3>& components,
                                 const RecurDirection direction=RecurDirection::X) noexcept
            {
                switch (direction) {
                    case RecurDirection::X:
                        m_components[0] = components[0];
                        m_components[1] = components[1];
                        m_components[2] = components[2];
                        break;
                    case RecurDirection::Y:
                        m_components[0] = components[2];
                        m_components[1] = components[0];
                        m_components[2] = components[1];
                        break;
                    /* RecurDirection::Z */
                    default:
                        m_components[0] = components[1];
                        m_components[1] = components[2];
                        m_components[2] = components[0];
                        break;
                }
            }
            /* Construct by transforming an array under a given triangle
               traversal (specified by its first and second corners of the
               triangle) */
            explicit RecurVector(const std::array<int,3>& components,
                                 const RecurDirection firstCorner,
                                 const RecurDirection secondCorner) noexcept
            {
                switch (firstCorner) {
                    case RecurDirection::X:
                        switch (secondCorner) {
                            /* x -> yz */
                            case RecurDirection::Y:
                                m_components[0] = components[0];
                                m_components[1] = components[1];
                                m_components[2] = components[2];
                                break;
                            /* x -> zy */
                            default:
                                m_components[0] = components[0];
                                m_components[1] = components[2];
                                m_components[2] = components[1];
                                break;
                        }
                        break;
                    case RecurDirection::Y:
                        switch (secondCorner) {
                            /* y -> xz */
                            case RecurDirection::X:
                                m_components[0] = components[1];
                                m_components[1] = components[0];
                                m_components[2] = components[2];
                                break;
                            /* y -> zx */
                            default:
                                m_components[0] = components[1];
                                m_components[1] = components[2];
                                m_components[2] = components[0];
                                break;
                        }
                        break;
                    default:
                        switch (secondCorner) {
                            /* z -> xy */
                            case RecurDirection::X:
                                m_components[0] = components[2];
                                m_components[1] = components[0];
                                m_components[2] = components[1];
                                break;
                            /* z -> yx */
                            default:
                                m_components[0] = components[2];
                                m_components[1] = components[1];
                                m_components[2] = components[0];
                                break;
                        }
                        break;
                }
            }
            /* Construct from a given order of a corner */
            explicit RecurVector(const int order,
                                 const RecurDirection corner=RecurDirection::X) noexcept
            {
                switch (corner) {
                    case RecurDirection::X:
                        m_components[0] = order;
                        m_components[1] = 0;
                        m_components[2] = 0;
                        break;
                    case RecurDirection::Y:
                        m_components[0] = 0;
                        m_components[1] = order;
                        m_components[2] = 0;
                        break;
                    /* z corner */
                    default:
                        m_components[0] = 0;
                        m_components[1] = 0;
                        m_components[2] = order;
                        break;
                }
            }
            ~RecurVector() noexcept = default;
            /* Convert to string */
            inline std::string to_string() const noexcept
            {
                return tGlueCore::stringify(m_components.cbegin(), m_components.cend());
            }
            /* Access component */
            inline int operator[](const unsigned int direction) const noexcept
            {
                return m_components.at(direction);
            }
            inline int operator[](const RecurDirection direction) const noexcept
            {
                switch (direction) {
                    case RecurDirection::X:
                        return m_components[0];
                    case RecurDirection::Y:
                        return m_components[1];
                    /* z corner */
                    default:
                        return m_components[2];
                }
            }
            /* Operator < for being a key in std::map */
            inline bool operator<(const RecurVector& another) const noexcept
            {
                return m_components<another.m_components;
            }
            /* Check if the vector < 0 on at least one direction */
            inline bool any_negative() const noexcept
            {
                return m_components[0]<0 || m_components[1]<0 || m_components[2]<0;
            }
            /* Check if the vector is non positive along all directions */
            inline bool is_non_positive() const noexcept
            {
                return m_components[0]<=0 && m_components[1]<=0 && m_components[2]<=0;
            }
            /* Check if the vector is valid or not */
            inline bool is_valid() const noexcept
            {
                return m_components[0]!=std::numeric_limits<int>::max()
                    && m_components[1]!=std::numeric_limits<int>::max()
                    && m_components[2]!=std::numeric_limits<int>::max();
            }
            /* Get the norm (length) of the vector */
            inline int get_norm() const noexcept
            {
                return m_components[0]+m_components[1]+m_components[2];
            }
            /* If the vector equals to another one */
            inline bool equal_to(const std::array<int,3>& another) const noexcept
            {
                return m_components[0]==another[0] &&
                       m_components[1]==another[1] &&
                       m_components[2]==another[2];
            }
            /* Check if the vector is greater than another one on at least one
               direction */
            inline bool any_greater(const RecurVector& another) const noexcept
            {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurVector::any_greater() has a vector ",
                                        to_string(),
                                        ", and another one ",
                                        another.to_string());
#endif
                return m_components[0]>another.m_components[0] ||
                       m_components[1]>another.m_components[1] ||
                       m_components[2]>another.m_components[2];
            }
            /* Return a prioritized vector by taking the minial of the vector
               itself and another one on all directions. This function can be
               used to determine the loop category according to two given RHS
               decrements or increments. The default of another vector is the
               zeroth decrement */
            inline RecurVector get_prioritized(const RecurVector& another=RecurVector({0,0,0})) const noexcept
            {
                return RecurVector(std::array<int,3>({
                    std::min(m_components[0], another.m_components[0]),
                    std::min(m_components[1], another.m_components[1]),
                    std::min(m_components[2], another.m_components[2])
                }));
            }
            /* Return a transformed vector under a given direction of
               recurrence relaiton */
            inline RecurVector get_transformed(const RecurDirection direction) const noexcept
            {
                return RecurVector(m_components, direction);
            }
            /* Return a transformed vector under a given triangle traversal
               (specified by its first and second corners of the triangle) */
            inline RecurVector get_transformed(const RecurDirection firstCorner,
                                               const RecurDirection secondCorner) const noexcept
            {
                return RecurVector(m_components, firstCorner, secondCorner);
            }
        private:
            std::array<int,3> m_components;
    };

    /* Return an invalid vector */
    inline RecurVector make_invalid_vector() noexcept
    {
        return RecurVector(std::array<int,3>({std::numeric_limits<int>::max(),0,0}));
    }
}
