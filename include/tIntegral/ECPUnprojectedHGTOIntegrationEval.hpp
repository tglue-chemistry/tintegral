/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates unprojected part of ECP integrals with Gaussian
   functions.

   2020-08-15, Bin Gao:
   * first version
*/

template<typename CentreType, typename RealType>
void ECPUnprojectedHGTOIntegration<CentreType,RealType>::eval() noexcept
{
    /* Compute stem:[2(a_{\kappa}R_{\kappa C}+b_{\lambda}R_{\lambda C})] */
    auto ecp_to_basis = RealType(2)*m_braBasis->from_centre_scaled(m_ketBasis, m_oneOper->get_centre());
    auto spherical_ine_arg = m_abscissa*ecp_to_basis;
    /* Find the maximum power of the function stem:[r^{n'}] */
    for (auto const& each_node: m_recur_arrays[2].get_input_nodes()) {
        each_node->get_order(m_allIndices[2]);
    }
    /* Compute the exponentially weighted modified spherical Bessel function of the first kind */
    spherical_ine_arg.apply(MathFunction::spherical_ine_v<RealType>);

    m_oneOper;

    auto inv_total_exponents = m_braBasis->inverse_total_exponents(m_ketBasis);
    /* Get pre-exponential factors stem:[\exp(-\frac{a_{i}b_{j}}{a_{i}+b_{j}}R_{\kappa\lambda}^{2})]. */
    auto expt_factors = m_braBasis->get_expt_factors(m_ketBasis);
    /* Comput integrals of s-shell HGTOs */
    for (auto const& each_node: m_recur_arrays[2].get_input_nodes()) {
#ifdef TINTEGRAL_DEBUG
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "ECPUnprojectedHGTOIntegration::eval() processes LHS node ",
                                each_node->to_string());
#endif
        auto val_lhs = m_buffer->get_element()+each_node->get_offset();
#if defined(USE_VALARRAY_BUFFER)
        *val_lhs = sqrt_pi3*expt_factors*std::pow(inv_total_exponents, 1.5);
#else
        for (unsigned int ival=0; ival<m_buffer->get_element_size(); ++ival) {
            (*val_lhs)[ival] = sqrt_pi3*expt_factors[ival]*std::pow(inv_total_exponents[ival], 1.5);
        }
#endif
    }
}
