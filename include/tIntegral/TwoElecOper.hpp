/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of two-electron integrator.

   2019-02-06, Bin Gao:
   * first version
*/

namespace tIntegral
{
    class TwoElecOper: virtual public tSymbolic::Symbol
    {
    };
}
