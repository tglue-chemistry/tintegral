/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron integrator class.

   2019-02-07, Bin Gao:
   * redesign based on the one-electron operator classes

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include "tSymbolic/Physics.hpp"
#include "tMatrix/BlockMat.hpp"
#include "tBasisSet/BasisFunction.hpp"
#include "tMolecular/Cluster.hpp"

#include "tIntegral/BasicTypes.hpp"
#include "tIntegral/IntegratorState.hpp"
#include "tIntegral/OneElec/OneElecIntegration.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* == One-Electron Integrator

       In this section, I will focus on the implmentation of one-electron
       integrator class that serves as a default computation engine for various
       one-electron operator derived classes.

       === Dispatching upon Basis Functions

       Note that one can have different algorithms for different basis
       functions and one-electron operators, the proposed integrator class are
       indeed the "`multiple dispatch`" that is not directly supported in C+\+.
       Visitor pattern can be used for double dispatch in C++, but it may not
       be a good idea to directly employ this pattern here due to the
       considerable number of one-electron operators.

       Let us first focus on the dispatching upon basis functions. In general
       there are few different types of basis functions during run time, so one
       might be able to use the visitor pattern. However, I choose another
       simple and brute way--I store pairs of `std::type_index` of basis
       functions on bra and ket, together with a corresponding
       `OneElecIntegration` class (which will be discussed later) into a
       `std::unordered_map`:

       [source,c++]
       ----
       std::unordered_map<std::pair<std::type_index,std::type_index>,
                          std::shared_ptr<OneElecIntegration>> m_integration_engine;
       ----

       The dispatching will be performed upon the pairs of `std::type_index` of
       basis function. Taking the integral computation as an example, a
       one-electron integrator class may have a member function
       `get_integral()` like:

       [source,c++]
       ----
       std::shared_ptr<tBasisSet::BasisFunction> bra_basis;  <1>
       std::shared_ptr<tBasisSet::BasisFunction> ket_basis;

       auto engine = m_integration_engine.find(std::make_pair(bra_basis->type_id(),  <2>
                                                              ket_basis->type_id()));

       if (engine==m_integration_engine.end()) {
           return IntegratorState::UnsetIntegration;  <3>
       }
       else {
           auto state_integrator = engine->integrate(oneOper, bra_basis, ket_basis, ...);  <4>
       }

       ----
       <1> `bra_basis` and `ket_basis` are shared pointers of
           `BasisFunction` class of the library
           link:https://gitlab.com/tglue-chemistry/tbasis-set[tBasisSet], and
           were given to the integrator by users.
       <2> `engine` is a registered one-electron integration instance of the
           `OneElecIntegration` class, which was set by users or initialized by
           *the template factory function* mentioned in the `OneElecOper` class.
       <3> Integration instance is not set for given operator and basis
           functions during run time.
       <4> The integration will be finally carried out by a member function
           `integrate()` of the registered one-electron integration instance
           `engine`. The exact type `OneOperType` of `oneOper` and the detailed
           design of the function `get_integral()` will be discussed later.

       The key point of such a dispatching scheme is the *virtual* method
       `type_id()` of the `BasisFunction` class:

       [source,c++]
       ----
       namespace tBasisSet
       {
           class BasisFunction
           {
               public:
                   ...
                   virtual inline std::type_index type_id() const noexcept = 0;
                   ...
           };
       }
       ----

       Because both `bra_basis` and `ket_basis` are shared *pointers*, and the
       member function `type_id()` is *virtual*, the runtime type's `type_id()`
       can be called in the above integrator's member function
       `get_integral()`.

       One benefit of the above dispatching approach is, whenever there is a
       new type of basis function introduced and its integration class
       implemented, one does not need to modify the member function
       `get_integral()`. Instead, one can simply add/replace such an
       integration into the `m_integration_engine` during run time.

       === One-Electron Integrator Template Class

       Next, let us focus on `OneOperType` in the integrator's member function
       `get_integral()`. As aforementioned, there are considerable number of
       one-electron operators. So, neither the vistor pattern nor the above
       scheme for dispatching upon basis functions is optimal for the
       dispatching upon one-electron operators.

       Moreover, the `OneElecOper` class requires computation engines take the
       one-electron operator derived class as input argument, that is
       `OneOperType`. By also noticing that the member function
       `get_integral()` can be the same for different one-electron operator
       derived classes, I therefore choose the following design strategy for
       one-electron integrator classes:

       . A *one-electron integrator template class* with the one-electron
         operator derived class as the template parameter, provides interface
         of one-electron integrators for users and takes care of most common
         functionalities required for integrators.

       . *Template (explicit) specialization* and *curiously recurring template
         pattern (CRTP)* can be used for different one-electron integrator
         derived classes, which can be set as computation engines of the
         corresponding one-electron operator derived classes.

       Based on the above strategy, it is straightforward to require the
       one-electron integration class `OneElecIntegration` -- which serves as
       the real computation engine -- to take a similar form as a template
       class `template<typename OneOperType> OneElecIntegration`.

       I will address `OneElecIntegration` class in its own section. In the
       following, I will first present the one-electron integrator template
       class in detail.

       @class:OneElecIntegrator[One-electron integrator template class.]
       @tparam:OneOperType[Type of a one-electron operator derived class.] */
    class OneElecIntegrator
    {
        public:
            /* @fn:OneOperIntegrator()[Constructor.]
               @param[in]:columnMajor[Indicate whether computations are in
                                      column-major (loop on ket first) order or
                                      in row-major (loop on bra first) order,
                                      default is `true` for column-major
                                      order.] */
            explicit OneElecIntegrator(const bool columnMajor=true) noexcept:
                m_column_major(columnMajor) {}
            /* One important functionality of one-electron integrators is to
               set a one-electron integration instance, which can be performed
               by the following two template functions.

               @fn:set_integration_engine[Add or replace an integration engine.]
               @tparam:BraBasisType[Type of basis function(s) on bra.]
               @tparam:KetBasisType[Type of basis function(s) on ket.]
               @param[in]:engine[The integration engine for given types of basis functions.]
               @param[in]:commutative[Indicate whether types of basis functions on bra and ket are commutative for the integration engine, default `true`.]

               One should note that this template function can only be used when
               types of basis functions on bra and ket are different. */
            template<typename BraBasisType, typename KetBasisType>
            inline typename std::enable_if<!std::is_same<BraBasisType,KetBasisType>::value>::type
            set_integration_engine(const std::shared_ptr<OneElecIntegration> engine,
                                   const bool commutative=true) noexcept
            {
                m_integration_engine[std::make_pair(typeid(BraBasisType),typeid(KetBasisType))] = engine;
                if (commutative) {
                    m_integration_engine[std::make_pair(typeid(KetBasisType),typeid(BraBasisType))] = engine;
                }
            }
            /* When types of basis functions on bra and ket are the same, the
               following template function should be used.

               @fn:set_integration_engine[Add or replace an integration engine.]
               @tparam:BasisType[Type of basis function(s) on bra and ket.]
               @param[in]:engine[The integration engine for given type of basis function(s).] */
            template<typename BasisType>
            inline void set_integration_engine(const std::shared_ptr<OneElecIntegration> engine) noexcept
            {
                m_integration_engine[std::make_pair(typeid(BasisType),typeid(BasisType))] = engine;
            }
            /* Sometimes one may need to modify a previously added one-electron
               integration instance, which can be accessed by the following
               template function `get_integration_engine()`.

               @fn:get_integration_engine[Get an integration engine for given types of basis functions on bra and ket.]
               @tparam:BraBasisType[Type of basis function(s) on bra.]
               @tparam:KetBasisType[Type of basis function(s) on ket.]
               @return:[The integration engine or a null pointer when there is no such an integration engine.] */
            template<typename BraBasisType, typename KetBasisType>
            inline std::shared_ptr<OneElecIntegration> get_integration_engine() const noexcept
            {
                auto engine = m_integration_engine.find(std::make_pair(typeid(BraBasisType),typeid(KetBasisType)));
                if (engine==m_integration_engine.end()) {
                    return std::shared_ptr<OneElecIntegration>(nullptr);
                }
                else {
                    return engine;
                }
            }
            /* The following functions take care of molecular system and basis
               functions needed in, for instance integral and expectation value
               computations.

               @fn:set_molecule[Assign a new molecular system to the integrator.]
               @param[in]:molecule[The molecular system.] */
            inline void set_molecule(const std::shared_ptr<tMolelcular::Cluster> molecule) noexcept
            {
                m_molecule = molecule;
                //if (m_integration_engine) m_integration_engine->set_molecule(m_molecule);
            }
            /* @fn:set_basis_function[Assign new basis functions on bra and ket.]
               @param[in]:braBasis[Basis function(s) on bra.]
               @param[in]:ketBasis[Basis function(s) on ket.] */
            inline void set_basis_function(const std::vector<std::shared_ptr<tBasisSet::BasisFunction>> braBasis,
                                           const std::vector<std::shared_ptr<tBasisSet::BasisFunction>> ketBasis) noexcept
            {
                m_bra_basis = braBasis;
                m_ket_basis = ketBasis;
            }
            /* Likewise, one can also access previously assigned molecular
               system and basis functions via the following functions.

               @fn:get_molecule[Get access of previously assigned molecular system.]
               @return:[The molecular system or a null pointer when the molecular system was not assigned.] */
            inline std::shared_ptr<tMolelcular::Cluster> get_molecule() const noexcept
            {
                return m_molecule;
            }
            /* @fn:get_basis_function[Get access of previously assigned basis function(s).]
               @param[in]:onBra[Get access of basis function(s) on bra (`true`) or ket (`false`), default `true`.]
               @return:[Vector of basis function(s) or empty when the basis function(s) were not assigned.] */
            inline std::vector<std::shared_ptr<tBasisSet::BasisFunction>>
            get_basis_function(const bool onBra=true) const noexcept
            {
                return onBra ? m_bra_basis : m_ket_basis;
            }
            /* @fn:get_integral[Evaluate integrals.]
               @param[in]:oneOper[A one-electron operator (containing derivatives).]
               @param[inout]:intOper[Integral matrices.]
               @return:[State of the integrator or the integration instance that performs the evaluation.] */
            IntegratorState get_integral(const std::shared_ptr<OneElecOper>& oneOper,
                                         std::vector<tMatrix::BlockMat>& intOper) noexcept;
            /* @fn:get_expectation[Evaluate expectation values with given states.]
               @param[in]:oneOper[A one-electron operator (containing derivatives).]
               @param[in]:states[Given states.]
               @param[inout]:expOper[Expectation values.]
               @return:[State of the integrator or the integration instance that performs the evaluation.] */
            IntegratorState get_expectation(const std::shared_ptr<OneElecOper>& oneOper,
                                            const std::vector<tMatrix::BlockMat>& states,
                                            std::vector<tReal>& expOper) noexcept;
            /* @fn:~OneOperIntegrator()[Deconstructor.] */
            virtual ~OneElecIntegrator() noexcept = default;
        protected:
            /*@@ Computations in column-major order. */
            bool m_column_major;
            /* Assigned molecular system and basis function(s). */
            std::shared_ptr<tMolelcular::Cluster> m_molecule;
            std::vector<std::shared_ptr<tBasisSet::BasisFunction>> m_bra_basis;
            std::vector<std::shared_ptr<tBasisSet::BasisFunction>> m_ket_basis;
            /* Registered one-electron integration engines */
            std::unordered_map<std::pair<std::type_index,std::type_index>,
                               std::shared_ptr<OneElecIntegration>> m_integration_engine;
    };
}
