/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron integration class.

   2019-02-14, Bin Gao:
   * redesign that serves as a bridge between one-electron integrators and
     various implementation of one-electron integration

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <array>
#include <memory>
#include <utility>
#include <vector>

#include "tCombinatorics/MultinomialExpansion.hpp"
#include "tSymbolic/Physics.hpp"
#include "tBasisSet/BasisFunction.hpp"
#include "tMolecular/Cluster.hpp"

#include "tIntegral/BasicTypes.hpp"
#include "tIntegral/IntegratorState.hpp"
#include "tIntegral/OneElec/OneElecOper.hpp"

#include "tIntegral/OneElecOperator/CartMultMomentHGTOIntegration.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    //@@ Forward declaration of one-electron operator classes.
    class CartMultMoment;
    class NucAttractPotential;
    class ECPUnprojected;
    class ECPProjected;
    //@

    /* == One-Electron Integration

       After introducing one-electron integrators, the integral computation is
       separated from one-electron operators. A futher separation is to
       implement integrations with different basis functions into other
       classes rather than into the integrators themselves.

       Let us name such an implementation as one-electron integration. There
       will be one-electron integration with contracted Gaussian type
       orbitals, Slater type orbitals, and so forth.

       The current abstract class `OneElecIntegration` thus serves as a bridge
       between one-electron integrators and various implementation of
       one-electron integration. All one-electron integration classes should be
       inherited from this abstract class, so that they can be set into
       corresponding one-electron integrators.

       @class:OneElecIntegration[Abstract class for all one-electron integration classes.] */
    template<typename RealType> class OneElecIntegration: virtual public tSymbolic::QuantChem::OneElecIntegrator
    {
        public:
            /* @fn:OneElecIntegration()[Constructor.] */
            explicit OneElecIntegration() noexcept = default;
            /* @fn:~OneElecIntegration()[Deconstructor.] */
            virtual ~OneElecIntegration() noexcept = default;
            /* @fn:integrate[Integration with given operator, basis functions, molecular system.]
               @param[in]:oneOper[A one-electron operator (containing derivatives).]
               @param[in]:braBasis[Basis function(s) on bra.]
               @param[in]:ketBasis[Basis function(s) on ket.]
               @param[in]:molecule[The molecular system.]
               @param[inout]:intOper[Integrals.]
               @return:[State of the integration.]

               This virtual function will be implemented by different classes
               of one-electron integration. */
            //virtual bool integrate(const std::shared_ptr<tSymbolic::QuantChem::OneElecOper>& oneOper,
            //                       const std::vector<std::shared_ptr<tSymbolic::QuantChem::BasisFunction>>& braBasisSet,
            //                       const std::vector<std::shared_ptr<tSymbolic::QuantChem::BasisFunction>>& ketBasisSet,
            //                       const std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>>& derivatives)??
            virtual IntegratorState integrate(const std::shared_ptr<OneElecOper>& oneOper,
                                              const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                                              const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                                              const std::shared_ptr<tMolelcular::Cluster>& molecule,
                                              std::pair<std::valarray<RealType>,std::valarray<RealType>>
                                              std::valarray<RealType>& intOper) noexcept = 0;
            /* The `OneElecIntegration` also needs to dispatch other `Symbol`
               derived classes declared in the `SymbolVisitor`. */
            using SymbolVisitor::dispatch;
            /* @fn:dispatch[Dispatch Cartesian multipole moment operator.]
               @param[in]:oneOper[Cartesian multipole moment operator.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<CartMultMoment> oneOper) noexcept = 0;
            /* @fn:dispatch[Dispatch nuclear attraction potential operator.]
               @param[in]:symbol[Nuclear attraction potential operator.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<NucAttractPotential> oneOper) noexcept = 0;
            /* @fn:dispatch[Dispatch effective core potential (ECP).]
               @param[in]:oneOper[Effective core potential.]
               @return:[Boolean indicates if the visitor worked normally or with error.] */
            virtual bool dispatch(std::shared_ptr<ECPUnprojected> oneOper) noexcept = 0;
            virtual bool dispatch(std::shared_ptr<ECPProjected> oneOper) noexcept = 0;
        protected:
            /* Derivatives from the symbolic differentiation of the
               one-electron operator should be expanded on the operator itself
               and basis functions during integration. */
            using derivative_type = std::vector<std::pair<unsigned long int,std::vector<unsigned int>>>;
            /* @fn:get_derivatives[Get derivatives on the operator and basis functions.]
               @param[in]:oneOper[A one-electron operator (containing derivatives).]
               @param[in]:braBasis[Basis function(s) on bra.]
               @param[in]:ketBasis[Basis function(s) on ket.]
               @return:[Expanded derivatives.] */
            IntegratorState
            get_derivatives(const std::shared_ptr<OneElecOper>& oneOper,
                            const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                            const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                            bool& nonZero,
                            std::vector<std::pair<std::shared_ptr<tSymbolic::Symbol>,derivative_type>& derivatives) noexcept;
            /* @fn:compare_derivatives[Compare the ordering of two physical variables.]
               @param[in]:lhs[Physical variable on the left hand side (LHS).]
               @param[in]:rhs[Physical variable on the right hand side (RHS).]
               @return:[Boolean indicates if the LHS variable goes before the RHS variable.]

               Whenever there is any new variable, this function should be
               extended. We do not make it virtual so that different
               integration derived classes can have the same ordering of
               derivatives. */
            bool compare_derivatives(const std::pair<std::shared_ptr<Symbol>,unsigned int>& lhs,
                                     const std::pair<std::shared_ptr<Symbol>,unsigned int>& rhs) noexcept;
            /* @fn:expand_derivative[Expand a derivative on the operator and basis functions.]
               @param[in]:oneOper[A one-electron operator.]
               @param[in]:braBasis[Basis function(s) on bra.]
               @param[in]:ketBasis[Basis function(s) on ket.]
               @param[in]:variable[Variable that the derivative is with respect to.]
               @param[in]:order[Order of the derivative.]
               @param[out]:nonZero[Whether the derivative is non-zero.]
               @param[out]:derivatives[Expanded derivatives with expansion coefficients and orders.] */
            virtual IntegratorState
            expand_derivative(const std::shared_ptr<OneElecOper>& oneOper,
                              const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                              const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                              const std::shared_ptr<Symbol>& variable,
                              const unsigned int order,
                              bool& nonZero,
                              derivative_type& derivatives) noexcept = 0;
    };
}
