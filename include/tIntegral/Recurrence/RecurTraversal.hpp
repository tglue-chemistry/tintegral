/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for traversal of a triangle or an edge.

   2019-06-26, Bin Gao:
   * moved from RecurDirection
*/

#pragma once

#include <array>
#include <string>

#include "tIntegral/Recurrence/RecurDirection.hpp"

namespace tIntegral
{
    class RecurTraversal
    {
        public:
            /* Set a triangle traversal from given first and last corners */
            //FIXME: maybe remove fromFirstCorner
            explicit RecurTraversal(const RecurDirection firstCorner,
                                    const RecurDirection lastCorner,
                                    const bool fromFirstCorner=true) noexcept:
                m_from_first_corner(fromFirstCorner)
            {
                switch (firstCorner) {
                    case RecurDirection::X:
                        /* From the x corner to the edge zy, and then from the corner z to y */
                        if (lastCorner==RecurDirection::Y) {
                            m_directions[0] = RecurDirection::X;
                            m_directions[1] = RecurDirection::Z;
                            m_directions[2] = RecurDirection::Y;
                            m_directions[3] = RecurDirection::ZY;
                        }
                        /* From the x corner to the edge yz, and then from the corner y to z */
                        else {
                            m_directions[0] = RecurDirection::X;
                            m_directions[1] = RecurDirection::Y;
                            m_directions[2] = RecurDirection::Z;
                            m_directions[3] = RecurDirection::YZ;
                        }
                        break;
                    case RecurDirection::Y:
                        /* From the y corner to the edge zx, and then from the corner z to x */
                        if (lastCorner==RecurDirection::X) {
                            m_directions[0] = RecurDirection::Y;
                            m_directions[1] = RecurDirection::Z;
                            m_directions[2] = RecurDirection::X;
                            m_directions[3] = RecurDirection::ZX;
                        }
                        /* From the y corner to the edge xz, and then from the corner x to z */
                        else {
                            m_directions[0] = RecurDirection::Y;
                            m_directions[1] = RecurDirection::X;
                            m_directions[2] = RecurDirection::Z;
                            m_directions[3] = RecurDirection::XZ;
                        }
                        break;
                    default:
                        /* From the z corner to the edge yx, and then from the corner y to x */
                        if (lastCorner==RecurDirection::X) {
                            m_directions[0] = RecurDirection::Z;
                            m_directions[1] = RecurDirection::Y;
                            m_directions[2] = RecurDirection::X;
                            m_directions[3] = RecurDirection::YX;
                        }
                        /* From the z corner to the edge xy, and then from the corner x to y */
                        else {
                            m_directions[0] = RecurDirection::Z;
                            m_directions[1] = RecurDirection::X;
                            m_directions[2] = RecurDirection::Y;
                            m_directions[3] = RecurDirection::XY;
                        }
                        break;
                }
            }
            ~RecurTraversal() noexcept = default;
            /* Access the traversal */
            inline RecurDirection operator[](const unsigned int which) const noexcept
            {
                return m_directions.at(which);
            }
            /* Whether the triangle traversal begins from the first corner */
            inline bool from_first_corner() const noexcept { return m_from_first_corner; }
            /* Convert to string */
            inline std::string to_string() const noexcept
            {
                return m_from_first_corner ? "["+stringify_direction(m_directions[0])+", "
                                                +stringify_direction(m_directions[1])+", "
                                                +stringify_direction(m_directions[2])+", "
                                                +stringify_direction(m_directions[3])+"]"
                                           : "["+stringify_direction(m_directions[3])+", "
                                                +stringify_direction(m_directions[1])+", "
                                                +stringify_direction(m_directions[2])+", "
                                                +stringify_direction(m_directions[0])+"]";
            }
        private:
            std::array<RecurDirection,4> m_directions;
            /* From the first corner to edge or opposite */
            bool m_from_first_corner;
    };
}
