/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation converter factory.

   2019-06-29, Bin Gao:
   * moved from RecurCompiler
*/

#pragma once

#include <memory>

#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurAnalyzer.hpp"
#include "tIntegral/Recurrence/RecurTranslator.hpp"
#include "tIntegral/Recurrence/RecurConverter.hpp"

namespace tIntegral
{
    /* Recurrence relation converter factory */
    class RecurConverterFactory
    {
        public:
            explicit RecurConverterFactory(const RecurTraversal triangleTraversal,
                                           const std::shared_ptr<RecurTranslator> translator) noexcept:
                m_triangle_traversal(triangleTraversal),
                m_translator(translator) {}
            virtual ~RecurConverterFactory() noexcept = default;
            /* Get the triangle traversal and recurrence relation translator */
            inline RecurTraversal get_traversal() const noexcept
            {
                return m_triangle_traversal;
            }
            /* Get the recurrence relation translator */
            inline std::shared_ptr<RecurTranslator> get_translator() const noexcept
            {
                return m_translator;
            }
            //FIXME: send index??
            /* Get a converter for non-output indices with a given decrement,
               direction and analyzer of a recurrence relation */
            virtual RecurNonOutputConverter
            make_non_output_converter(const std::string& nameFunction,
                                      const RecurVector& decrement,
                                      const RecurDirection recurDirection,
                                      const std::shared_ptr<RecurAnalyzer> analyzer) const noexcept;
            /* Get a converter for the output index with a given decrement, and
               analyzer of a recurrence relation */
            virtual RecurOutputConverter
            make_output_converter(const std::string& nameFunction,
                                  const RecurVector& decrement,
                                  const std::shared_ptr<RecurAnalyzer> analyzer) const noexcept;
        private:
            RecurTraversal m_triangle_traversal;
            std::shared_ptr<RecurTranslator> m_translator;
    };
}
