/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of Recursive Brutal-but-Useful First-in
   First-out Buffer.

   2020-08-01, Bin Gao:
   * support elements of the buffer using pointers instead of std::valarray

   2020-07-14, Bin Gao:
   * add parameter to reserve begin and/or end of the buffer in set_offsets()

   2020-06-19, Bin Gao:
   * change buffer elements to std::valarray

   2020-06-13, Bin Gao:
   * changed to template class

   2018-06-16, Bin Gao:
   * changed to C++ class

   2017-05-30, Bin Gao:
   * first version
*/

#pragma once

#include <iterator>
#include <memory>
#include <numeric>
#include <type_traits>
#include <utility>
#if defined(TINTEGRAL_VALARRAY_BUFFER)
#include <valarray>
#endif

#include "tGlueCore/Logger.hpp"
#if defined(TINTEGRAL_DEBUG)
#include "tGlueCore/Stringify.hpp"
#endif

#include "tIntegral/Settings.hpp"

namespace tIntegral
{

    /* Recursive Brutal-but-Useful First-in First-out Buffer */
    template<typename RealType=double> class RecurBuffer final
    {
        public:
            /* Types of elements and values */
#if defined(TINTEGRAL_VALARRAY_BUFFER)
            using ElementType = std::valarray<RealType>;
            using ValueType = std::valarray<RealType>;
#else
            using ElementType = RealType*;
            using ValueType = RealType;
#endif
            explicit RecurBuffer(const std::size_t sizeBuffer=0, const std::size_t sizeElement=1) noexcept:
                m_buffer_forward(true),
                m_size_buffer(sizeBuffer),
                m_size_element(sizeElement) {}
            ~RecurBuffer() noexcept = default;
            /* Convert to string */
            inline std::string to_string() const noexcept
            {
                return "{forward: "+std::to_string(m_buffer_forward)
                    + ", size-buffer: "+std::to_string(m_size_buffer)
                    + ", size-element: "+std::to_string(m_size_element)
                    + ", empty: "+empty()?std::to_string(false):std::to_string(true)
                    + '}';
            }
            /* Allocate memory for the buffer */
            inline bool allocate() noexcept
            {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurBuffer::allocate() will allocate the buffer with size ",
                                        m_size_buffer);
#endif

                if (m_elements) {
                    m_elements.reset(new(std::nothrow) ElementType[m_size_buffer]);
                }
                else {
                    m_elements = std::unique_ptr<ElementType[]>(new(std::nothrow) ElementType[m_size_buffer]);
                }
                return allocate_elements();
            }
            /* Check if the buffer is empty */
            inline bool empty() const noexcept
            {
#if defined(TINTEGRAL_VALARRAY_BUFFER)
                return m_elements ? false : true;
#else
                return (m_elements && m_values) ? false : true;
#endif
            }
            /* Reset the size of each element, should be called after the buffer is allocated */
            inline bool reset_size_element(const std::size_t sizeElement) noexcept
            {
                if (sizeElement<=m_size_element) {
                    m_size_element = sizeElement;
                    return true;
                }
                else {
                    m_size_element = sizeElement;
                    return allocate_elements();
                }
            }
            /* Set offsets of given levels of member arrays of a recurrence relation
               @param[in]:reserved[Offset(s) and size(s) of reserved parts of the buffer.] */
            template<typename SizeIterator, typename OffsetIterator>
            inline typename std::enable_if
            <std::is_same<typename std::iterator_traits<SizeIterator>::value_type,
                          std::size_t>::value &&
             std::is_same<typename std::iterator_traits<OffsetIterator>::value_type,
                          std::shared_ptr<std::size_t>>::value, bool>::type
            set_offsets(const unsigned int numSegments,
                        const std::vector<std::pair<std::size_t,std::size_t>>& reserved,
                        const unsigned int numLevels,
                        SizeIterator levelSize,
                        OffsetIterator levelOffset) noexcept
            {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurBuffer::set_offsets() called with number of segments ",
                                        numSegments,
                                        " and number of levels ",
                                        numLevels);
                if (!reserved.empty()) {
                    std::vector<std::size_t> offsets_reserved;
                    std::vector<std::size_t> sizes_reserved;
                    for (auto const& each_reserved: reserved) {
                        offsets_reserved.push_back(each_reserved.first);
                        sizes_reserved.push_back(each_reserved.second);
                    }
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "tIntegral::RecurBuffer::set_offsets() called with offsets of reserved parts ",
                        tGlueCore::stringify(offsets_reserved.cbegin(), offsets_reserved.cend()),
                        " and sizes of reserved parts ",
                        tGlueCore::stringify(sizes_reserved.cbegin(), sizes_reserved.cend())
                    );
                }
#endif
                /* Set begin and end of the available buffer */
                std::size_t buffer_begin = 0;
                auto buffer_end = m_size_buffer;
                for (auto const& each_reserved: reserved) {
                    /* Reserved part is at the begin of the buffer */
                    if (each_reserved.first+each_reserved.first<=m_size_buffer) {
                        auto reserved_end = each_reserved.first+each_reserved.second;
                        if (buffer_begin<reserved_end) buffer_begin = reserved_end;
                    }
                    /* Reserved part is at the end of the buffer */
                    else {
                        if (buffer_end>each_reserved.first) buffer_end = each_reserved.first;
                    }
                }
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurBuffer::set_offsets() called with segments beginning from ",
                                        buffer_begin,
                                        " and ending at ",
                                        buffer_end);
#endif
                if (buffer_begin<=buffer_end) {
                    /* Compute the size of available buffer, which is [buffer_begin, buffer_end) */
                    auto size_available = buffer_end-buffer_begin;
                    /* Compute the sizes of circular-arranged segments needed for the given
                       sizes of levels of member arrays */
                    std::vector<std::size_t> size_segments;
                    size_segments.reserve(numSegments);
                    if (numSegments<numLevels) {
                        auto size_first = levelSize;
                        std::advance(levelSize, numSegments);
                        size_segments.assign(size_first, levelSize);
                        auto iter_size = size_segments.begin();
                        for (unsigned int ilevel=numSegments; ilevel<numLevels; ++ilevel,++iter_size,++levelSize) {
                            /* Backs to the first segment in a circular manner */
                            if (iter_size==size_segments.end()) iter_size = size_segments.begin();
                            if (*iter_size<*levelSize) *iter_size = *levelSize;
                        }
                    }
                    else {
                        auto size_first = levelSize;
                        std::advance(levelSize, numLevels);
                        size_segments.assign(size_first, levelSize);
                    }
                    /* Compute the required size for segments */
                    auto size_required = std::accumulate(size_segments.cbegin(), size_segments.cend(), 0);
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "tIntegral::RecurBuffer::set_offsets() called with available size ",
                                            size_available,
                                            " and required size ",
                                            size_required,
                                            ", and sizes of segments ",
                                            tGlueCore::stringify(size_segments.cbegin(), size_segments.cend()));
#endif
                    /* Enlarge the size of buffer if needed */
                    if (size_required>size_available) {
                        auto size_additional = size_required-size_available;
                        m_size_buffer += size_additional;
                        buffer_end += size_additional;
                        size_available = size_required;
                        /* Update the offsets of levels of member arrays of
                           preceding recurrence relations assigned in the
                           backward manner */
                        for (auto& recur_offset: m_backward_offsets) {
                            for (auto& level_offset: recur_offset) {
                                *level_offset += size_additional;
                            }
                        }
                    }
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "tIntegral::RecurBuffer::set_offsets() has buffer size ",
                                            m_size_buffer,
                                            ", and segments begin from ",
                                            buffer_begin,
                                            " and end at ",
                                            buffer_end);
#endif
                    /* Assign offsets in a circular way */
                    auto iter_size = size_segments.cbegin();
                    if (m_buffer_forward) {
                        auto current_offset = buffer_begin;
                        for (unsigned int ilevel=0; ilevel<numLevels; ++ilevel,++iter_size,++levelOffset) {
                            /* Back to the first segment in a circular manner */
                            if (iter_size==size_segments.cend()) {
                                iter_size = size_segments.cbegin();
                                current_offset = buffer_begin;
                            }
                            *(*levelOffset) = current_offset;
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "tIntegral::RecurBuffer::set_offsets() set the offset of level ",
                                                    ilevel,
                                                    " as ",
                                                    *(*levelOffset));
#endif
                            current_offset += *iter_size;
                        }
                        /* Next time we will use the buffer from its end */
                        m_buffer_forward = false;
                    }
                    else {
                        /* Store offsets assigned in the backward manner */
                        m_backward_offsets.push_back(std::vector<std::shared_ptr<std::size_t>>());
                        m_backward_offsets.back().reserve(numLevels);
                        auto current_offset = buffer_end;
                        for (unsigned int ilevel=0; ilevel<numLevels; ++ilevel,++iter_size,++levelOffset) {
                            /* Back to the first segment in a circular manner */
                            if (iter_size==size_segments.cend()) {
                                iter_size = size_segments.cbegin();
                                current_offset = buffer_end;
                            }
                            /* Here we use the buffer from high address to low address */
                            current_offset -= *iter_size;
                            *(*levelOffset) = current_offset;
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "tIntegral::RecurBuffer::set_offsets() set the offset of level ",
                                                    ilevel,
                                                    " as ",
                                                    *(*levelOffset));
#endif
                            m_backward_offsets.back().push_back(*levelOffset);
                        }
                        /* Next time we will use the buffer from its beginning */
                        m_buffer_forward = true;
                    }
                    return true;
                }
                else {
                    std::vector<std::size_t> offsets_reserved;
                    std::vector<std::size_t> sizes_reserved;
                    for (auto const& each_reserved: reserved) {
                        offsets_reserved.push_back(each_reserved.first);
                        sizes_reserved.push_back(each_reserved.second);
                    }
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurBuffer::set_offsets() encounters conflicting reserved parts, offsets of reserved parts ",
                        tGlueCore::stringify(offsets_reserved.cbegin(), offsets_reserved.cend()),
                        ", sizes of reserved parts ",
                        tGlueCore::stringify(sizes_reserved.cbegin(), sizes_reserved.cend()),
                        ", from which the begin and the end of available buffer are ",
                        buffer_begin,
                        " and ",
                        buffer_end
                    );
                    return false;
                }
            }
            /* Get the stored pointer to elements of the buffer */
            inline ElementType* get_element() const noexcept { return m_elements.get(); }
            /* Get the size of the buffer (number of elements) */
            inline std::size_t get_size_buffer() const noexcept { return m_size_buffer; }
            /* Get the size of each element (number of values) */
            inline std::size_t get_size_element() const noexcept { return m_size_element; }
            /* Array subscript operator for accessing a value of the buffer */
            inline ValueType& operator[](std::size_t index) noexcept
            {
#if defined(TINTEGRAL_VALARRAY_BUFFER)
                return m_elements[index];
#else
                return m_values[index];
#endif
            }
            /* Return size (number) of values of the buffer */
            inline std::size_t size() const noexcept
            {
#if defined(TINTEGRAL_VALARRAY_BUFFER)
                return m_size_buffer;
#else
                return m_size_element*m_size_buffer;
#endif
            }
            /* Make a copy of the buffer except for offsets of recurrence relations */
            inline RecurBuffer<RealType> copy_buffer() noexcept
            {
                auto duplicated = RecurBuffer<RealType>(m_size_buffer, m_size_element);
                duplicated.m_buffer_forward = m_buffer_forward;
                if (duplicated.allocate()) {
                    for (unsigned int ibuf=0; ibuf<m_size_buffer; ++ibuf) {
                        duplicated.m_elements[ibuf] = m_elements[ibuf];
                    }
#if !defined(TINTEGRAL_VALARRAY_BUFFER)
                    for (unsigned int ival=0; ival<m_size_element*m_size_buffer; ++ival) {
                        duplicated.m_values[ival] = m_elements[ival];
                    }
#endif
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurBuffer::() failed to allocate buffer with size ",
                        m_size_buffer,
                        " and element size ",
                        m_size_element
                    );
                }
                return duplicated;
            }
        protected:
            /* Allocate memory for the buffer elements */
            inline bool allocate_elements() noexcept
            {
                if (m_elements) {
#if defined(TINTEGRAL_VALARRAY_BUFFER)
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "tIntegral::RecurBuffer::allocate_elements() will allocate each element with size ",
                        m_size_element
                    );
#endif
                    for (unsigned int ibuf=0; ibuf<m_size_buffer; ++ibuf) {
                        m_elements[ibuf].resize(m_size_element);
                    }
#else
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "tIntegral::RecurBuffer::allocate_elements() will allocate elements with size ",
                        m_size_element*m_size_buffer
                    );
#endif
                    if (m_values) {
                        m_values.reset(new(std::nothrow) ValueType[m_size_element*m_size_buffer]);
                    }
                    else {
                        m_values = std::unique_ptr<ValueType[]>(
                            new(std::nothrow) ValueType[m_size_element*m_size_buffer]
                        );
                    }
                    if (m_values) {
                        /* Let each element of the buffer point to correct address */
                        m_elements[0] = m_values.get();
                        for (unsigned int ibuf=0; ibuf<m_size_buffer-1; ++ibuf) {
                            m_elements[ibuf+1] = m_elements[ibuf]+m_size_element;
                        }
                    }
                    else {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurBuffer::allocate_elements() failed to allocate elements with size ",
                            m_size_element*m_size_buffer
                        );
                        return false;
                    }
#endif
                    return true;
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurBuffer::allocate_elements() is called without buffer allocated with size ",
                        m_size_buffer
                    );
                    return false;
                }
            }
        private:
            /* If the buffer used forward (from low address to high address) or
               backward (from high address to low address) */
            bool m_buffer_forward;
            /* Size of the buffer (number of elements) */
            std::size_t m_size_buffer;
            /* Size of each element of the buffer (number of values) */
            std::size_t m_size_element;
            /* Offsets of levels of member arrays of preceding recurrence
               relations assigned in the backward manner */
            std::vector<std::vector<std::shared_ptr<std::size_t>>> m_backward_offsets;
            /* Elements of the buffer */
            std::unique_ptr<ElementType[]> m_elements;
#if !defined(TINTEGRAL_VALARRAY_BUFFER)
            /* Values of each element of the buffer */
            std::unique_ptr<ValueType[]> m_values;
#endif
    };
}
