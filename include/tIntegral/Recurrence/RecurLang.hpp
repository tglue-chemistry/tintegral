/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file contains programming language specific information used by the
   recurrence relation compiler.

   2019-06-06, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/Convert.hpp"

#include <string>

namespace tIntegral
{
    /* Type specifiers */
    enum class RecurTypeSpecifier
    {
        None,
        Const
    };

    /* Declarators */
    enum class RecurDeclarator
    {
        None,
        LvalueReference,
        RvalueReference,
        Pointer
    };

    /* Types of operand */
    enum class RecurOperandType
    {
        ClassOperand,
        ObjectOperand,
        PointerOperand
    };

    /* Function specifier */
    enum class RecurFunctionSpecifier
    {
        FreeFunction,
        MemberFunction,
        OverrideFunction
    };

    /* Comparison operators */
    enum class RecurComparison
    {
        Equal,
        LessThan,
        LessEqual,
        GreaterThan,
        GreaterEqual
    };

    /* Logical operators */
    enum class RecurLogical
    {
        NOT,
        AND,
        OR
    };

    /* Increment and decrement operators */
    enum class RecurIncDec
    {
        Increment,
        Decrement
    };

    /* Arithmetic operations */
    enum class RecurArithmetic
    {
        Negation,
        Addition,
        Subtraction,
        Multiplication,
        Division
    };

    /* Stringify arithmetic operations */
    inline std::string stringify_recur_arithmetic(const RecurArithmetic oper) noexcept
    {
        switch (oper) {
            case RecurArithmetic::Negation:
                return std::string("RecurArithmetic::Negation");
            case RecurArithmetic::Addition:
                return std::string("RecurArithmetic::Addition");
            case RecurArithmetic::Subtraction:
                return std::string("RecurArithmetic::Subtraction");
            case RecurArithmetic::Multiplication:
                return std::string("RecurArithmetic::Multiplication");
            case RecurArithmetic::Division:
                return std::string("RecurArithmetic::Division");
            default:
                return "??"+std::to_string(tGlueCore::to_integral(oper))+"??";
        }
    }

    /* Assignment operators */
    enum class RecurAssignment
    {
        BasicAssignment,
        AdditionAssignment,
        SubtractionAssignment,
        MultiplicationAssignment,
        DivisionAssignment
    };
}
