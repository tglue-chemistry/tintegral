/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation integration.

   2019-09-12, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <vector>

#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurNode.hpp"

namespace tIntegral
{
    template<typename RealType=double> class RecurIntegration
    {
        public:
            explicit RecurIntegration() noexcept = default;
            virtual ~RecurIntegration() noexcept = default;
            /* Top-down procedure */
            virtual bool top_down(std::vector<std::vector<std::shared_ptr<RecurNode>>>&& outputNodes,
                                  const std::vector<std::shared_ptr<RecurIndex>>& allIndices,
                                  const std::shared_ptr<RecurBuffer<RealType>> buffer) noexcept = 0;
            /* Get input nodes for the integration and sorted according to the
               order of a given index. This function can be used for succeeding
               recurence relations, assignment, transformation and contraction. */
            virtual std::vector<std::vector<std::shared_ptr<RecurNode>>>
            get_input_nodes(const std::shared_ptr<RecurIndex>& index) noexcept = 0;
            /* Bottom-up procedure */
            virtual bool bottom_up() noexcept = 0;
    };
}
