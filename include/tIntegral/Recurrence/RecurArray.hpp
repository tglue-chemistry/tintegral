/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of jagged array for recurrence relations.

   2020-06-24, Bin Gao:
   * use template member function

   2019-05-20, Bin Gao:
   * change name to RecurArray and move RecurNode to another file

   2018-06-12, Bin Gao:
   * change name to RecurRelation and use jagged array data structure

   2017-05-24, Bin Gao:
   * first version
*/

#pragma once

#if defined(TINTEGRAL_DEBUG)
#include <algorithm>
#endif
#include <iterator>
#include <limits>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/IndexOrder.hpp"
#include "tIntegral/Recurrence/RecurNode.hpp"

namespace tIntegral
{
    /* Jagged array for the implementation of a recurrence relation */
    template<typename RealType=double> class RecurArray
    {
        public:
            explicit RecurArray() noexcept:
                m_current_level(1),
                m_current_node(0) {}
            virtual ~RecurArray() noexcept = default;
            /* Convert to string, FIXME: also convert nodes to string, more information */
            inline std::string to_string() const noexcept
            {
                std::string str_object = "{output: "+m_output_idx->to_string()
                                       + ", current-level: "+std::to_string(m_current_level)
                                       + ", current-node: "+std::to_string(m_current_node)
                                       + ", nodes: {";
                for (auto ilevel=m_recur_nodes.crbegin(); ilevel!=m_recur_nodes.crend(); ++ilevel) {
                    str_object += "level"
                                + std::to_string(std::distance(m_recur_nodes.crend(), ilevel))
                                + ": {";
                    for (auto inode=ilevel->cbegin(); inode!=ilevel->cend(); ++inode) {
                        str_object += "node"
                                    + std::to_string(std::distance(ilevel->cbegin(), inode))
                                    + ": "
                                    + (*inode)->to_string()
                                    + ',';
                    }
                    str_object.back() = '}';
                    str_object += ',';
                }
                str_object.back() = '}';
                return str_object;
            }
            /* Assign a recurrence relation from its output nodes */
            bool assign(std::vector<std::vector<std::shared_ptr<RecurNode>>>&& outputNodes,
                        const std::vector<std::shared_ptr<RecurIndex>>& allIndices,
                        const std::vector<unsigned int>& recurIdxPositions,
                        const unsigned int outputIdx,
                        const std::vector<std::vector<int>>& rhsIncrements,
                        const std::shared_ptr<RecurBuffer<RealType>> buffer,
                        const bool outputSaved=false) noexcept
            {
#if defined(TINTEGRAL_DEBUG)
                /* Make sure the recurrence-relation indices are sorted */
                if (!std::is_sorted(recurIdxPositions.cbegin(), recurIdxPositions.cend())) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurArray::assign() called with recurrence-relation indices ",
                        tGlueCore::stringify(recurIdxPositions.cbegin(), recurIdxPositions.cend()),
                        ", which must be in ascending order"
                    );
                    return false;
                }
                /* Check the position of the last recurrence-relation index */
                if (allIndices.size()<recurIdxPositions.back()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurArray::assign() called with recurrence-relation indices ",
                        tGlueCore::stringify(recurIdxPositions.cbegin(), recurIdxPositions.cend()),
                        ", and the last index at the position ",
                        recurIdxPositions.back(),
                        ", but number of all indices ",
                        allIndices.size()
                    );
                    return false;
                }
#endif
                m_output_idx = allIndices[recurIdxPositions[outputIdx]];
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurArray::assign() got output index ",
                                        m_output_idx->to_string());
#endif
                /* Reshape the output node due to indices of the current
                   recurrence relation; One should note that the change of
                   shape will not affect the performance of the preceding
                   recurrence relation, because these nodes are the input of
                   the preceding recurrence relation, and whose loops are
                   controlled by the LHS nodes of these input nodes */
#if defined(TINTEGRAL_DEBUG)
                for (auto const& ilevel: outputNodes) {
                    for (auto const& inode: ilevel) {
                        Settings::logger->write(tGlueCore::MessageType::Debug,
                                                "tIntegral::RecurArray::assign() got output node ",
                                                inode->to_string());
                    }
                }
#endif
                m_recur_nodes = std::move(outputNodes);
                m_size_output = 0;
                if (outputSaved) {
                    for (auto& ilevel: m_recur_nodes) {
                        for (auto& inode: ilevel) {
                            m_size_output += inode->reshape(allIndices, recurIdxPositions);
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "tIntegral::RecurArray::assign() reshaped node ",
                                                    inode->to_string());
#endif
                            m_output_nodes.push_back(inode);
                        }
                    }
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "tIntegral::RecurArray::assign() get size of output nodes ",
                                            m_size_output);
#endif
                }
                else {
                    for (auto& ilevel: m_recur_nodes) {
                        for (auto& inode: ilevel) {
                            inode->reshape(allIndices, recurIdxPositions);
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "tIntegral::RecurArray::assign() reshaped node ",
                                                    inode->to_string());
#endif
                        }
                    }
                }
                /* Performs top-down procedure */
                if (m_recur_nodes.size()>1) {
                    /* We need to reserve the parts of the buffer used by output nodes */
                    std::vector<std::pair<std::size_t,std::size_t>> buffer_reserved;
                    for (auto& ilevel: m_recur_nodes) {
                        for (auto& inode: ilevel) {
                            buffer_reserved.push_back(std::make_pair(inode->get_offset(), inode->get_size()));
                        }
                    }
                    /* Get RecurIndex for those involved in the recurrence relation */
                    std::vector<std::shared_ptr<RecurIndex>> recur_indices;
                    recur_indices.reserve(recurIdxPositions.size());
                    for (auto const& idx_position: recurIdxPositions) {
                        recur_indices.push_back(allIndices[idx_position]);
                    }
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "tIntegral::RecurArray::assign() got recurrence-relation indices ",
                                            tGlueCore::stringify(recur_indices.cbegin(), recur_indices.cend()));
#endif
                    /* Convert RHS increments, and find the maximum increments
                       for each recurrence-relation index and the minimum
                       increment for the output index */
                    std::vector<IndexOrder> rhs_increments(rhsIncrements.size());
                    std::vector<int> idx_max_increments(recurIdxPositions.size(), std::numeric_limits<int>::min());
                    int min_output_increment = 0;
                    for (unsigned int irhs=0; irhs<rhsIncrements.size(); ++irhs) {
                        if (!rhs_increments[irhs].template assign(allIndices.size(),
                                                                  recur_indices.size(),
                                                                  recur_indices.cbegin(),
                                                                  rhsIncrements[irhs].cbegin())) {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "Called by RecurArray::assign() for ",
                                                    irhs,
                                                    "-th RHS node as ",
                                                    tGlueCore::stringify(rhsIncrements[irhs].cbegin(),
                                                                         rhsIncrements[irhs].cend()));
                            return false;
                        }
                        auto each_increment = rhsIncrements[irhs].cbegin();
                        auto max_increment = idx_max_increments.begin();
                        for (; each_increment!=rhsIncrements[irhs].cend(); ++each_increment,++max_increment) {
                            if (*max_increment<*each_increment) *max_increment = *each_increment;
                        }
                        if (min_output_increment>rhsIncrements[irhs].at(outputIdx)) {
                            min_output_increment = rhsIncrements[irhs][outputIdx];
                        }
                    }
                    if (idx_max_increments[outputIdx]!=-1) {
                        std::string str_increments("[");
                        for (auto const& each_increment: rhsIncrements) {
                            str_increments += tGlueCore::stringify(each_increment.cbegin(), each_increment.cend());
                        }
                        str_increments += ']';
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "tIntegral::RecurArray::assign() called with number of indices ",
                                                recur_indices.size(),
                                                " and output index at ",
                                                outputIdx,
                                                ", given RHS nodes=",
                                                str_increments,
                                                "\nMaximum increment=-1 is required for the output index!");
                        return false;
                    }
                    /* Compute the number of segments in buffer needed for the
                       recurrence relation */
                    unsigned int num_segments = 1-min_output_increment;
                    /* The order of output index is not great enough so that we
                       do not need as many segments in buffer as the order of
                       the recurrence relation +1 */
                    auto max_output_level = m_recur_nodes.size()-1;
                    if (num_segments>max_output_level) num_segments = max_output_level;
                    /* Initialize the base address of each level, except for
                       the level of the highest order to the buffer that should
                       be already assigned */
                    std::vector<std::shared_ptr<std::size_t>> level_bases;
                    level_bases.reserve(max_output_level);
                    for (unsigned int ilevel=0; ilevel<max_output_level; ++ilevel) {
                        level_bases.push_back(std::make_shared<std::size_t>(0));
                    }
                    /* Initialize the size of nodes at each level (except for
                       the level of the highest order) */ 
                    std::vector<std::size_t> level_sizes(max_output_level, 0);
                    /* Perform the top-down procedure from the level of the
                       highest order */
                    for (auto ilevel=max_output_level; ilevel>=1; --ilevel) {
#if defined(TINTEGRAL_DEBUG)
                        Settings::logger->write(
                            tGlueCore::MessageType::Debug,
                            "tIntegral::RecurArray::assign() performs the top-down procedure for the level ",
                            ilevel
                        );
#endif
                        for (auto& lhs_node: m_recur_nodes[ilevel]) {
#if defined(TINTEGRAL_DEBUG)
                            Settings::logger->write(
                                tGlueCore::MessageType::Debug,
                                "tIntegral::RecurArray::assign() performs the top-down procedure for the node ",
                                lhs_node->to_string()
                            );
#endif
                            /* Loop over RHS increments */
                            std::vector<std::shared_ptr<RecurNode>> rhs_nodes;
                            for (auto const& each_increment: rhs_increments) {
#if defined(TINTEGRAL_DEBUG)
                                Settings::logger->write(
                                    tGlueCore::MessageType::Debug,
                                    "tIntegral::RecurArray::assign() processes RHS increment ",
                                    each_increment.to_string()
                                );
#endif
                                /* Compute orders of a possible new RHS node */
                                auto new_rhs_orders = lhs_node->add_increment(each_increment);
                                /* Zero RHS node */
                                if (new_rhs_orders.any_negative()) {
                                    rhs_nodes.push_back(nullptr);
#if defined(TINTEGRAL_DEBUG)
                                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                                            "tIntegral::RecurArray::assign() gets a zero RHS node");
#endif
                                }
                                else {
                                    /* Try to find out if the new RHS node was already created */
                                    auto rhs_level = new_rhs_orders.get_nonnegative_order(m_output_idx);
                                    bool new_node = true;
                                    for (auto irhs=m_recur_nodes[rhs_level].cbegin();
                                         irhs!=m_recur_nodes[rhs_level].cend();
                                         ++irhs) {
                                        if ((*irhs)->has_same_orders(new_rhs_orders)) {
                                            rhs_nodes.push_back(*irhs);
                                            new_node = false;
#if defined(TINTEGRAL_DEBUG)
                                            Settings::logger->write(
                                                tGlueCore::MessageType::Debug,
                                                "tIntegral::RecurArray::assign() finds the RHS node"
                                            );
#endif
                                            break;
                                        }
                                    }
                                    /* A new RHS node found */
                                    if (new_node) {
                                        rhs_nodes.push_back(std::make_shared<RecurNode>(
                                            new_rhs_orders, level_bases[rhs_level], level_sizes[rhs_level]
                                        ));
                                        /* Resize the new RHS node and update
                                           the size of this level */
                                        level_sizes[rhs_level] += rhs_nodes.back()->resize(
                                            recur_indices, lhs_node->get_strides()
                                        );
#if defined(TINTEGRAL_DEBUG)
                                        Settings::logger->write(
                                            tGlueCore::MessageType::Debug,
                                            "tIntegral::RecurArray::assign() constructs a new RHS node ",
                                            rhs_nodes.back()->to_string()
                                        );
#endif
                                        /* Store the new RHS node */
                                        m_recur_nodes[rhs_level].push_back(rhs_nodes.back());
                                    }
                                }
                            }
                            /* Push RHS nodes */
                            lhs_node->set_rhs(std::move(rhs_nodes));
                        }
                    }
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "tIntegral::RecurArray::assign() got sizes of levels (except for output nodes) ",
                        tGlueCore::stringify(level_sizes.cbegin(), level_sizes.cend())
                    );
#endif
                    /* Set base addresses of different levels */
                    if (!buffer->template set_offsets(num_segments,
                                                      buffer_reserved,
                                                      max_output_level,
                                                      level_sizes.begin(),
                                                      level_bases.begin())) {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurArray::assign() could not set base addresses of different levels"
                        );
                        return false;
                    }
#if defined(TINTEGRAL_DEBUG)
                    for (unsigned int ilevel=0; ilevel<max_output_level; ++ilevel) {
                        level_sizes[ilevel] = *level_bases[ilevel];
                    }
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "tIntegral::RecurArray::assign() got base addresses of levels (except for the max level) ",
                        tGlueCore::stringify(level_sizes.cbegin(), level_sizes.cend())
                    );
#endif
                }
                m_buffer = buffer;
                return true;
            }
            /* Get input nodes that at the level of the zeroth order. This
               function can be used for the computation of values of the input
               nodes. */
            inline std::vector<std::shared_ptr<RecurNode>> get_input_nodes() const noexcept
            {
                return m_recur_nodes.front();
            }
            /* Get input nodes that at the level of the zeroth order and sorted
               according to the order of given index. This function can be used
               for assignment, transformation and contraction. */
            inline std::vector<std::vector<std::shared_ptr<RecurNode>>>
            get_input_nodes(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                std::vector<std::vector<std::shared_ptr<RecurNode>>> input_nodes;
                /* Get the maximum order of the index */
                unsigned int max_idx_order = 0;
                for (auto const& inode: m_recur_nodes.front()) {
                    auto idx_node_order = inode->get_order(index);
                    if (max_idx_order<idx_node_order) max_idx_order = idx_node_order;
                }
                input_nodes.resize(max_idx_order+1);
                /* Put input nodes into appropriate levels */
                for (auto& inode: m_recur_nodes.front()) {
                    input_nodes[inode->get_order(index)].push_back(inode);
                }
                return input_nodes;
            }
            /* Begin the loop of nodes at different levels except for the
               zeroth order (input nodes) */
            inline void begin_loop() noexcept
            {
                m_current_level = 1;
                m_current_node = 0;
            }
            /* Whether there are nodes left in the loop */
            inline bool node_left() const noexcept
            {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurArray::node_left() has current level ",
                                        m_current_level,
                                        " and current node ",
                                        m_current_node,
                                        ", and has ",
                                        m_recur_nodes.size(),
                                        " levels of nodes");
#endif
                if (m_current_level+1<m_recur_nodes.size()) {
                    return true;
                }
                else if (m_current_level>=m_recur_nodes.size()) {
                    return false;
                }
                else {
                    return m_current_node<m_recur_nodes[m_current_level].size();
                }
            }
            /* Get the current node as const; Note that shared_ptr<T> is
               convertable to shared_ptr<const T> but not the reverse */
            inline std::shared_ptr<const RecurNode> current_node() const noexcept
            {
                return m_recur_nodes[m_current_level][m_current_node];
            }
            ///* Get the buffer holding integrals of the current node */
            //inline RecurBuffer::ElementType* current_node_buffer() const noexcept
            //{
            //    return m_buffer->get_element()+m_recur_nodes[m_current_level][m_current_node]->get_offset();
            //}
            /* Advance to the next node, without checking against bounds */
            inline void next_node() noexcept
            {
                ++m_current_node;
                if (m_current_node==m_recur_nodes[m_current_level].size()) {
                    ++m_current_level;
                    m_current_node = 0;
                }
            }
            /* Make a copy of buffer holding integrals of output nodes */
            inline RecurBuffer<RealType> copy_output_buffer() const noexcept
            {
                auto output_buffer = RecurBuffer<RealType>(m_size_output, m_buffer->get_size_element());
                if (m_output_nodes.empty()) return output_buffer;
                if (output_buffer.allocate()) {
                    auto iter_buffer = output_buffer.get_element();
                    for (auto const& each_node: m_output_nodes) {
                        auto node_buffer = m_buffer->get_element()+each_node->get_offset();
                        for (unsigned int each_element=0; each_element<each_node->get_size(); ++each_element) {
#if defined(USE_VALARRAY_BUFFER)
                            *iter_buffer = *node_buffer;
#else
                            for (unsigned int ival=0; ival<m_buffer->get_size_element(); ++ival) {
                                (*iter_buffer)[ival] = (*node_buffer)[ival];
                            }
#endif
                            ++node_buffer;
                            ++iter_buffer;
                        }
                    }
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurArray::copy_output_buffer() failed to allocate buffer with size ",
                        m_size_output,
                        " and element size ",
                        m_buffer->get_size_element()
                    );
                }
                return output_buffer;
            }
            /* Scale the integrals of output nodes and add them to a given
               buffer, like `axpy` routine of BLAS */
            inline void axpy_output_buffer(const RealType sfactor, RecurBuffer<RealType>& buffer) const noexcept
            {
                auto iter_buffer = buffer.get_element();
                for (auto const& each_node: m_output_nodes) {
                    auto node_buffer = m_buffer->get_element()+each_node->get_offset();
                    for (unsigned int each_element=0; each_element<each_node->get_size(); ++each_element) {
#if defined(USE_VALARRAY_BUFFER)
                        *iter_buffer += sfactor*(*node_buffer);
#else
                        for (unsigned int ival=0; ival<m_buffer->get_size_element(); ++ival) {
                            (*iter_buffer)[ival] += sfactor*(*node_buffer)[ival];
                        }
#endif
                        ++node_buffer;
                        ++iter_buffer;
                    }
                }
            }
            /* Scale values of a given buffer and then set to the integrals of output nodes */
            inline void set_output_buffer(const RealType sfactor, const RecurBuffer<RealType>& buffer) noexcept
            {
                auto iter_buffer = buffer.get_element();
                for (auto const& each_node: m_output_nodes) {
                    auto node_buffer = m_buffer->get_element()+each_node->get_offset();
                    for (unsigned int each_element=0; each_element<each_node->get_size(); ++each_element) {
#if defined(USE_VALARRAY_BUFFER)
                        *node_buffer = sfactor*(*iter_buffer);
#else
                        for (unsigned int ival=0; ival<m_buffer->get_size_element(); ++ival) {
                            (*node_buffer)[ival] = sfactor*(*iter_buffer)[ival];
                        }
#endif
                        ++node_buffer;
                        ++iter_buffer;
                    }
                }
            }
        private:
            /* Position of the output index */
            std::shared_ptr<RecurIndex> m_output_idx;
            /* Jagged array for the recurrence relation */
            std::vector<std::vector<std::shared_ptr<RecurNode>>> m_recur_nodes;
            /* Current level of the recurrence relation */
            unsigned int m_current_level;
            /* Current node at the current level of the recurrence relation */
            unsigned int m_current_node;
            /* Buffer holding integrals */
            std::shared_ptr<RecurBuffer<RealType>> m_buffer;
            /* Size of buffer of output nodes if needed */
            std::size_t m_size_output;
            /* Output nodes if needed */
            std::vector<std::shared_ptr<RecurNode>> m_output_nodes;
    };
}
