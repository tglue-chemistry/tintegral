/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation converters.

   2019-06-29, Bin Gao:
   * moved from RecurCompiler
*/

#pragma once

#include <array>
#include <functional>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "tGlueCore/Convert.hpp"

#include "tIntegral/Recurrence/RecurDirection.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/Recurrence/RecurVector.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurAnalyzer.hpp"
#include "tIntegral/Recurrence/RecurLang.hpp"
#include "tIntegral/Recurrence/RecurTranslator.hpp"

namespace tIntegral
{
    /* Base converter with common functions used by both non-output and output
       indices */
    class RecurConverter
    {
        public:
            explicit RecurConverter(const std::string nameFunction,
                                    const RecurTraversal triangleTraversal,
                                    const std::shared_ptr<RecurTranslator> translator) noexcept:
                m_name_function(nameFunction),
                m_triangle_traversal(triangleTraversal),
                m_translator(translator) {}
            virtual ~RecurConverter() noexcept = default;
            /* Add debug */
            inline void add_debug(const std::string& message) noexcept
            {
                m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                m_translator->handle_debug(std::vector<std::string>({
                    m_translator->string_literal(m_name_function+" works on "+message)
                }));
                m_translator->endif_directive();
            }
            /* Begin a for loop from the second corner to the last corner, i.e.
               along the last edge */
            inline std::array<bool,3> begin_corner_to_corner(const std::shared_ptr<RecurIndex>& index,
                                                             const bool onEdge,
                                                             const bool endIncluded,
                                                             const int beginOrder,
                                                             const int endAdjustment,
                                                             const std::array<bool,3>& orderNeeded,
                                                             const bool orderDeclared=false) noexcept
            {
                RecurDirection end_direction;
                RecurDirection adjust_direction;
                if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[0])]) {
                    /* The sum of orders along the second and the third corners
                       is that along RecurDirection::XYZ (on the edge) or the
                       difference between that along RecurDirection::XYZ and
                       that along the first corner (off the edge) */
                    end_direction = RecurDirection::XYZ;
                    adjust_direction = onEdge ? RecurDirection::Null : m_triangle_traversal[0];
                }
                else {
                    end_direction = onEdge ? RecurDirection::XYZ : m_triangle_traversal[3];
                    adjust_direction = RecurDirection::Null;
                }
                /* This function is called before the last corner so we do not
                   need to declare the zeroth order along the first corner */
                if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[1])]) {
                    if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[2])]) {
                        /* Assign order of components of the second corner */
                        m_translator->assign_component_order(index->get_name(),
                                                             m_triangle_traversal[1],
                                                             end_direction,
                                                             adjust_direction,
                                                             -beginOrder,
                                                             orderDeclared);
                        /* Begin a for loop by increasing the order of
                           components of the third corner */
                        m_translator->begin_loop_component(
                            index->get_name(),
                            m_triangle_traversal[2],
                            beginOrder,
                            endIncluded ? RecurComparison::LessEqual : RecurComparison::LessThan,
                            end_direction,
                            adjust_direction,
                            endAdjustment,
                            /* Decrease the order of components of the second
                               corner */
                            std::vector<std::pair<RecurDirection,RecurIncDec>>({
                                std::make_pair(m_triangle_traversal[1], RecurIncDec::Decrement)
                            }),
                            orderDeclared
                        );
                    }
                    else {
                        /* Begin a for loop by decreasing the order of
                           components of the second corner */
                        m_translator->begin_loop_component(
                            index->get_name(),
                            m_triangle_traversal[1],
                            end_direction,
                            adjust_direction,
                            -beginOrder,
                            endIncluded ? RecurComparison::GreaterEqual : RecurComparison::GreaterThan,
                            -endAdjustment,
                            std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                            orderDeclared
                        );
                    }
                }
                else {
                    /* Begin a for loop by increasing the order of components
                       of the third corner */
                    m_translator->begin_loop_component(
                        index->get_name(),
                        m_triangle_traversal[2],
                        beginOrder,
                        endIncluded ? RecurComparison::LessEqual : RecurComparison::LessThan,
                        end_direction,
                        adjust_direction,
                        endAdjustment,
                        std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                        /* If the order along the third corner is not needed,
                           we should declare it inside the loop */
                        orderNeeded[tGlueCore::to_integral(m_triangle_traversal[2])]
                            ? orderDeclared : true
                    );
                }
                /* The order along the first corner is necessarily zero on the
                   edge */
                auto zero_orders = std::array<bool,3>({false, false, false});
                zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = onEdge;
                return zero_orders;
            }
            /* End a for loop over a component */
            inline void end_loop_component() noexcept
            {
                m_translator->end_for_loop();
            }
        private:
            std::string m_name_function;
            RecurTraversal m_triangle_traversal;
            std::shared_ptr<RecurTranslator> m_translator;
    };

    /* Allowed decrement(s) and functions to build snippets before and
       after processing next index of a recurrence relation, the former returns
       a boolean type result indicating if the order of the index is
       necessarily zero along X, Y and Z directions */
    //FIXME: remove const std::shared_ptr<RecurIndex>&??
    using RecurSnippeter = std::tuple<RecurVector,
                                      std::function<std::array<bool,3>(const std::shared_ptr<RecurIndex>&,
                                                                       const std::array<bool,3>&)>,
                                      std::function<void()>>;

    /* Convert the recurrence relation of a non-output index into
       loops, where the first of the pair is the order of the index,
       and the last one must be -1 for default orders */
    using RecurNonOutputSnippeter = std::pair<int,std::vector<RecurSnippeter>>;

    /* Recurrence relation converter for non-output indices */
    class RecurNonOutputConverter: virtual public RecurConverter
    {
        public:
            explicit RecurNonOutputConverter(const std::string nameFunction,
                                             const RecurTraversal triangleTraversal,
                                             const std::shared_ptr<RecurTranslator> translator,
                                             const std::shared_ptr<RecurAnalyzer> analyzer) noexcept:
                RecurConverter(nameFunction, triangleTraversal, translator),
                m_triangle_traversal(triangleTraversal),
                m_translator(translator),
                m_analyzer(analyzer) {}
            virtual ~RecurNonOutputConverter() noexcept = default;
            /* Set recurrence relation snippeters for the converter */
            inline void set(const RecurDirection recurDirection,
                            std::vector<RecurNonOutputSnippeter>&& snippeters) noexcept
            {
                m_recur_direction = recurDirection;
                m_snippeters = std::move(snippeters);
            }
            /* Get the size of recurrence relation snippeters */
            inline unsigned int size() const noexcept { return m_snippeters.size(); }
            /* Get a recurrence relation snippeter at a given position */
            inline RecurNonOutputSnippeter operator[](const unsigned int position) const noexcept
            {
                return m_snippeters.at(position);
            }
            /* Get the last recurrence relation snippeter */
            inline RecurNonOutputSnippeter back() const noexcept { return m_snippeters.back(); }
            /* Declare the order of the first corner as a given one */
            virtual std::array<bool,3> declare_first_corner(const std::shared_ptr<RecurIndex>& index,
                                                            const int order,
                                                            const std::array<bool,3>& orderNeeded) noexcept;
            /* Declare the order of the first corner as that of its index */
            virtual std::array<bool,3> declare_first_corner(const std::shared_ptr<RecurIndex>& index,
                                                            const std::array<bool,3>& orderNeeded) noexcept;
            /* Update pointers to values of RHS terms before loop(s) */
            virtual void update_rhs_values(const std::shared_ptr<RecurIndex>& index,
                                           const bool traverseTriangle) noexcept;
            /* Update pointers to values of RHS terms after loop(s) */
            virtual void update_rhs_values() noexcept;
            /* Update orders of components along the edge between the first and
               the second corners */
            virtual std::array<bool,3> update_first_edge(const std::shared_ptr<RecurIndex>& index,
                                                         const std::array<bool,3>& orderNeeded,
                                                         const bool lastEdge=false) noexcept;
            /* Update orders of components along the last edge */
            virtual void update_last_edge(const std::shared_ptr<RecurIndex>& index,
                                          const std::array<bool,3>& orderNeeded) noexcept;
            /* Assign a given order to the first component of the last edge */
            virtual std::array<bool,3> assign_last_edge(const std::shared_ptr<RecurIndex>& index,
                                                        const int order,
                                                        const std::array<bool,3>& orderNeeded,
                                                        const bool onEdge=false) noexcept;
            /* Assign the order of a component of an index to the first
               component of the last edge, and with an adjustment if it is non
               zero */
            virtual std::array<bool,3> assign_last_edge(const std::shared_ptr<RecurIndex>& index,
                                                        const RecurDirection idxComponent,
                                                        const int adjustment,
                                                        const std::array<bool,3>& orderNeeded) noexcept;
            /* Begin for loops over a triangle by the already specified
               triangle traversal */
            virtual void begin_loop_triangle(const std::shared_ptr<RecurIndex>& index,
                                             const bool endEdge,
                                             const std::array<bool,3>& orderNeeded) noexcept;
            /* End for loops over a triangle */
            virtual void end_loop_triangle() noexcept;
            /* Begin a for loop from the first corner to the edge specified by
               the second and the last corners, can be used together with the
               for loop from the second corner to the last one */
            virtual std::array<bool,3> begin_corner_to_edge(const std::shared_ptr<RecurIndex>& index,
                                                            const bool edgeIncluded,
                                                            const int beginOrder,
                                                            const int endAdjustment,
                                                            const std::array<bool,3>& orderNeeded) noexcept;
        protected:
            /* Write stepsizes of RHS terms resulted by (increment of) an index

               After getting the transformed decrement t, we can update the
               pointers of RHS terms by

               # Before the triangle traversal,
                 RHS += \frac{\max(t[1],t[2])[\max(t[1],t[2])+1]}{2}
               # Before each edge traversal RHS += t[2]
               # After each edge traversal RHS += t[1]
               # After the triangle traversal
                 RHS += \frac{t[0][2*(order+t)-t[0]+3]}{2}
                      = \frac{t[0][2*order+2*(t[1]+t[2])+t[0]+3]}{2}
                      = t[0]*order+t[0]*(t[1]+t[2])+\frac{t[0](t[0]+3)}{2}
             */
            virtual void write_rhs_stepsizes(const std::shared_ptr<RecurIndex>& index,
                                             const bool traverseTriangle,
                                             const bool beforeTraversal) const noexcept;
        private:
            RecurTraversal m_triangle_traversal;
            std::shared_ptr<RecurTranslator> m_translator;
            std::shared_ptr<RecurAnalyzer> m_analyzer;
            RecurDirection m_recur_direction;
            /* Convert the recurrence relation of a non-output index into
               loops, where the first of the pair is the order of the index,
               and the last one must be -1 for default orders */
            std::vector<RecurNonOutputSnippeter> m_snippeters;
            /* Index and its triangle or row loop that may have stepsize */
            std::vector<std::pair<std::shared_ptr<RecurIndex>,bool>> m_idx_stepsizes;
    };

    /* Convert the recurrence relation of an output index into loops
       according to different directions */
    using RecurOutputSnippeter = std::pair<RecurDirection,std::vector<RecurSnippeter>>;

    /* Recurrence relation converter for output index */
    class RecurOutputConverter: virtual public RecurConverter
    {
        public:
            explicit RecurOutputConverter(const std::string& nameFunction,
                                          const RecurTraversal triangleTraversal,
                                          const std::shared_ptr<RecurTranslator> translator,
                                          const std::shared_ptr<RecurAnalyzer> analyzer) noexcept:
                RecurConverter(nameFunction, triangleTraversal, translator),
                m_triangle_traversal(triangleTraversal),
                m_translator(translator),
                m_analyzer(analyzer) {}
            virtual ~RecurOutputConverter() noexcept = default;
            /* Set recurrence relation snippeters for the converter */
            inline void set(std::vector<RecurOutputSnippeter>&& snippeters) noexcept
            {
                m_snippeters = std::move(snippeters);
            }
            /* Get the size of recurrence relation snippeters */
            inline unsigned int size() const noexcept { return m_snippeters.size(); }
            /* Get a recurrence relation snippeter at a given position */
            inline RecurOutputSnippeter operator[](const unsigned int position) const noexcept
            {
                return m_snippeters.at(position);
            }
            /* Declare the order of the last corner as a given one */
            virtual std::array<bool,3> declare_last_corner(const std::shared_ptr<RecurIndex>& index,
                                                           const int order,
                                                           const std::array<bool,3>& orderNeeded) noexcept;
            /* Declare the order of the last corner as that of its index */
            virtual std::array<bool,3> declare_last_corner(const std::shared_ptr<RecurIndex>& index,
                                                           const std::array<bool,3>& orderNeeded) noexcept;
            /* Begin for loops over a triangle by the already specified
               triangle traversal */
            virtual void begin_loop_triangle(const std::shared_ptr<RecurIndex>& index,
                                             const bool endEdge,
                                             const std::array<bool,3>& orderNeeded) noexcept;
            /* Assign pointer to value of RHS term along the first corner to
               the second one */
            virtual void assign_rhs_second() noexcept;
            /* Assign pointer to value of RHS term along the second corner to
               the last one */
            virtual void assign_rhs_last() noexcept;
            /* Begin an if statement for index order condition */
            virtual void begin_index_order_condition(const std::shared_ptr<RecurIndex>& index,
                                                     const int order,
                                                     const bool elifStatement=false) noexcept;
            /* Begin an else statement for index order condition */
            virtual void begin_index_order_condition() noexcept;
            /* End an index order condition statement */
            virtual void end_index_order_condition() noexcept;
        private:
            RecurTraversal m_triangle_traversal;
            std::shared_ptr<RecurTranslator> m_translator;
            std::shared_ptr<RecurAnalyzer> m_analyzer;
            /* Convert the recurrence relation of an output index into loops
               according to different directions */
            std::vector<RecurOutputSnippeter> m_snippeters;
    };
}
