/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of nodes of recurrence relations.

   2019-05-20, Bin Gao:
   * change name to RecurNode

   2017-05-24, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <utility>
#include <valarray>
#include <vector>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/IndexOrder.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"

namespace tIntegral
{
    /* Node of recurrence relations */
    class RecurNode
    {
        public:
            /* Construct a node from given orders of indices */
            explicit RecurNode(const IndexOrder idxOrders,
                               const std::shared_ptr<std::size_t> baseLevel,
                               const std::size_t offsetNode) noexcept:
                m_idx_orders(idxOrders),
                m_base_level(baseLevel),
                m_offset_node(offsetNode) {}
            ///* Construct a node by transferring the order of the first index to
            //   the second one of a given node, used to create output nodes for
            //   RecurArray */
            //explicit RecurNode(const std::shared_ptr<RecurNode>& node,
            //                   const std::shared_ptr<RecurIndex>& firstIndex,
            //                   const std::shared_ptr<RecurIndex>& secondIndex,
            //                   const std::shared_ptr<std::size_t> baseLevel,
            //                   const std::size_t offsetNode) noexcept
            //{
            //    m_idx_orders = node->m_idx_orders.transfer_order(firstIndex, secondIndex);
            //    /* Update the size and offsets */
            //    m_base_level = baseLevel;
            //    m_offset_node = offsetNode;
            //    auto first_order = m_idx_orders.get_order(firstIndex);
            //    auto second_order = m_idx_orders.get_order(secondIndex);
            //    m_size_node = node->m_size_node
            //                / firstIndex->get_num_components(first_order)
            //                / secondIndex->get_num_components(second_order)
            //                * secondIndex->get_num_components(first_order+second_order);
            //}
            //FIXME: probably at the level of RecurArray, we do not need to
            //FIXME: compute stepsizes and strides in some cases such as HGTO
            //FIXME: integration.
            /* Resize the node by setting its size, strides and stepsizes of
               indices, mostly used after the constructor
               RecurNode(const IndexOrder, const std::shared_ptr<std::size_t>, const std::size_t) */
            inline std::size_t resize(const std::vector<std::shared_ptr<RecurIndex>>& recurIndices,
                                      const std::vector<std::size_t>&& idxStrides) noexcept
            {
                m_idx_strides = std::move(idxStrides);
                m_idx_stepsizes.clear();
                /* The last is the stepsize of indices after the recurrence-relation indices */
                m_idx_stepsizes.reserve(recurIndices.size()+1);
                /* Stepsize of the first recurrence-relation index is exactly its stride */
                m_idx_stepsizes.push_back(m_idx_strides.front());
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::resize() gets the stepsize ",
                                        m_idx_stepsizes.back(),
                                        " of the index ",
                                        recurIndices[0]->to_string());
#endif
                /* Initializes the size of the node as the size of all strides */
                m_size_node = m_idx_strides.back();
                for (unsigned int index=0; index<recurIndices.size()-1; ++index) {
                    /* Compute number of components of the current recurrence-relation index */
                    auto idx_num_comps = recurIndices[index]->get_num_components(
                        m_idx_orders.get_nonnegative_order(recurIndices[index])
                    );
                    m_size_node *= idx_num_comps;
                    m_idx_stepsizes.push_back(m_idx_stepsizes.back()*idx_num_comps*m_idx_strides[index+1]);
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "RecurNode::resize() gets the stepsize ",
                                            m_idx_stepsizes.back(),
                                            " of the index ",
                                            recurIndices[index+1]->to_string());
#endif
                }
                /* Compute number of components of the last recurrence-relation index */
                auto idx_num_comps = recurIndices.back()->get_num_components(
                    m_idx_orders.get_nonnegative_order(recurIndices.back())
                );
                m_idx_stepsizes.push_back(m_idx_stepsizes.back()*idx_num_comps);
                m_size_node *= idx_num_comps;
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::resize() gets the size of the node as ",
                                        m_size_node);
#endif
                return m_size_node;
            }
            /* Reshape a node, mostly used for output nodes of a recurrence
               relation (which is input nodes of the preceding recurrence
               relation), so that the base address of the level does not change */
            inline std::size_t reshape(const std::vector<std::shared_ptr<RecurIndex>>& allIndices,
                                       const std::vector<unsigned int>& recurIdxPositions) noexcept
            {
                m_idx_strides.clear();
                m_idx_strides.resize(recurIdxPositions.size()+2, 1);
                m_idx_stepsizes.clear();
                /* The last is the stepsize of indices after the recurrence-relation indices */
                m_idx_stepsizes.reserve(recurIdxPositions.size()+1);
                /* Compute stride of the first recurrence-relation index */
                for (unsigned int position=0; position<recurIdxPositions[0]; ++position) {
                    m_idx_strides[0] *= allIndices[position]->get_num_components(
                        m_idx_orders.get_nonnegative_order(allIndices[position])
                    );
                }
                /* The last element is the size of all strides */
                m_idx_strides.back() *= m_idx_strides[0];
                /* Stepsize of the first recurrence-relation index is exactly
                   its stride */
                m_idx_stepsizes.push_back(m_idx_strides[0]);
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::reshape() gets the stride ",
                                        m_idx_strides[0],
                                        " before the index ",
                                        allIndices[recurIdxPositions[0]]->to_string());
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::reshape() gets the stepsize ",
                                        m_idx_stepsizes.back(),
                                        " of the index ",
                                        allIndices[recurIdxPositions[0]]->to_string());
#endif
                /* Compute strides and stepsizes of recurrence-relation indices
                   before the last one */
                for (unsigned int index=0; index<recurIdxPositions.size()-1; ++index) {
                    /* Initialize the stepsize of the succeeding
                       recurrence-relation index as the product of its number
                       of components and the stepsize of the current
                       recurrence-relation index */
                    m_idx_stepsizes.push_back(
                        allIndices[recurIdxPositions[index]]->get_num_components(
                            m_idx_orders.get_nonnegative_order(allIndices[recurIdxPositions[index]])
                        )
                        * m_idx_stepsizes.back()
                    );
                    /* Stride between the current recurrence-relation index and
                       the succeeding one equals to the product of numbers of
                       components of indices between them */
                    unsigned int succeeding_idx = index+1;
                    for (unsigned int position=recurIdxPositions[index]+1;
                         position<recurIdxPositions[succeeding_idx];
                         ++position) {
                        m_idx_strides[succeeding_idx] *= allIndices[position]->get_num_components(
                            m_idx_orders.get_nonnegative_order(allIndices[position])
                        );
                    }
                    m_idx_strides.back() *= m_idx_strides[succeeding_idx];
                    m_idx_stepsizes.back() *= m_idx_strides[succeeding_idx];
#if defined(TINTEGRAL_DEBUG)
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "RecurNode::reshape() gets the stride ",
                                            m_idx_strides[succeeding_idx],
                                            " before the index ",
                                            allIndices[recurIdxPositions[succeeding_idx]]->to_string());
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "RecurNode::reshape() gets the stepsize ",
                                            m_idx_stepsizes.back(),
                                            " of the index ",
                                            allIndices[recurIdxPositions[succeeding_idx]]->to_string());
#endif
                }
                /* Intialize the size of the node as the product of the
                   stepsize of the last recurrence-relation index and its
                   number of components */
                m_size_node = m_idx_stepsizes.back()*allIndices[recurIdxPositions.back()]->get_num_components(
                    m_idx_orders.get_nonnegative_order(allIndices[recurIdxPositions.back()])
                );
                m_idx_stepsizes.push_back(m_size_node);
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(
                    tGlueCore::MessageType::Debug,
                    "RecurNode::reshape() gets number of components ",
                    allIndices[recurIdxPositions.back()]->get_num_components(
                        m_idx_orders.get_nonnegative_order(allIndices[recurIdxPositions.back()])
                    ),
                    " of index ",
                    allIndices[recurIdxPositions.back()]->to_string(),
                    " with the order ",
                    m_idx_orders.get_nonnegative_order(allIndices[recurIdxPositions.back()])
                );
#endif
                /* Compute stride after the last recurrence-relation index */
                auto succeeding_idx = recurIdxPositions.size();
                for (unsigned int position=recurIdxPositions.back()+1; position<allIndices.size(); ++position) {
                    m_idx_strides[succeeding_idx] *= allIndices[position]->get_num_components(
                        m_idx_orders.get_nonnegative_order(allIndices[position])
                    );
                }
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::reshape() gets the stride ",
                                        m_idx_strides[succeeding_idx],
                                        " after the last recurrence-relation index");
#endif
                m_idx_strides.back() *= m_idx_strides[succeeding_idx];
                m_size_node *= m_idx_strides[succeeding_idx];
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "RecurNode::reshape() gets the size of the node as ",
                                        m_size_node);
#endif
                return m_size_node;
            }
            /* Set RHS nodes */
            inline void set_rhs(std::vector<std::shared_ptr<RecurNode>>&& rhs_nodes) noexcept
            {
                m_rhs_nodes = std::move(rhs_nodes);
            }
            /* Convert to string */
            template<typename RealType=double>
            inline std::string to_string(const std::shared_ptr<RecurBuffer<RealType>>& buffer=nullptr) const noexcept
            {
                std::string str_node = "{orders: "+m_idx_orders.to_string()
                                     + ", base: "+std::to_string(*m_base_level)
                                     + ", offset: "+std::to_string(m_offset_node);
                if (m_size_node>0) str_node += ", size: "+std::to_string(m_size_node);
                if (!m_idx_strides.empty()) {
                    str_node += ", strides: "+tGlueCore::stringify(m_idx_strides.cbegin(), m_idx_strides.cend());
                }
                if (!m_idx_stepsizes.empty()) {
                    str_node += ", stepsizes: "+tGlueCore::stringify(m_idx_stepsizes.cbegin(), m_idx_stepsizes.cend());
                }
                str_node += ", rhs-nodes: [";
                for (auto const& each_rhs: m_rhs_nodes) {
                    if (each_rhs) {
                        str_node += each_rhs->m_idx_orders.to_string()+", ";
                    }
                    else {
                        str_node += "[], ";
                    }
                }
                if (m_rhs_nodes.empty()) {
                    str_node += ']';
                }
                else {
                    str_node.pop_back();
                    str_node.back() = ']';
                }
                if (buffer) {
                    str_node += ", values: [";
                    auto val_node = buffer->get_element()+get_offset();
                    for (unsigned int each_val=0; each_val<get_size(); ++each_val,++val_node) {
                        str_node += tGlueCore::stringify(*val_node, *val_node+buffer->get_size_element());
                    }
                    str_node.pop_back();
                    str_node.back() = ']';
                }
                return str_node+'}';
            }
            /* Return orders of indices by adding given increments */
            inline IndexOrder add_increment(const IndexOrder& increments) const noexcept
            {
                return m_idx_orders.add_increment(increments);
            }
            /* Return if the node is valid */
            inline operator bool() const { return m_idx_orders ? true : false; }
            /* Check if there is any negative order */
            inline bool any_negative() const noexcept { return  m_idx_orders.any_negative(); }
            /* Return whether given orders equal to those of the node */
            inline bool has_same_orders(const IndexOrder& orders) const noexcept
            {
                return m_idx_orders.equal_to(orders);
            }
            /* Get the offset of the node to the buffer */
            inline std::size_t get_offset() const noexcept { return *m_base_level+m_offset_node; }
            /* Get the size of the node */
            inline std::size_t get_size() const noexcept { return m_size_node; }
            /* Get the stride of an index */
            inline std::size_t get_stride(const unsigned int index) const noexcept
            {
                //FIXME: change at() to []
                return m_idx_strides.at(index);
            }
            /* Get strides */
            inline std::vector<std::size_t> get_strides() const noexcept
            {
                return m_idx_strides;
            }
            /* Get the stepsize of an index */
            inline std::size_t get_stepsize(const unsigned int index) const noexcept
            {
                /* Return the stepsize of indices after recurrence-relation
                   indices if given index out of the range */
                return index<m_idx_stepsizes.size() ? m_idx_stepsizes[index] : m_idx_stepsizes.back();
            }
            /* Get non-negative order of an index */
            inline unsigned int get_order(const std::shared_ptr<RecurIndex>& index) const noexcept
            {
                return m_idx_orders.get_nonnegative_order(index);
            }
            /* Get const RHS nodes */
            inline std::vector<std::shared_ptr<const RecurNode>> get_rhs() const noexcept
            {
                std::vector<std::shared_ptr<const RecurNode>> rhs_nodes(m_rhs_nodes.cbegin(), m_rhs_nodes.cend());
                return rhs_nodes;
            }
            ///* Get the number of indices */
            //inline unsigned int get_num_indices() const noexcept
            //{
            //    return m_idx_orders.get_num_indices();
            //}
            virtual ~RecurNode() noexcept = default;
        private:
            /* Orders of the node */
            IndexOrder m_idx_orders;
            /* Base address of the level where the node locates */
            std::shared_ptr<std::size_t> m_base_level;
            /* Offset of the node at its level */
            std::size_t m_offset_node;
            /* Size of the node */
            std::size_t m_size_node;
            /* Stride of each index (dimension is the number of indices +1), or
               size of non-involved indices between any two
               recurrence-relation-involved indices */
            std::vector<std::size_t> m_idx_strides;
            /* Stepsizes of recurrence-relation-involved indices */
            std::vector<std::size_t> m_idx_stepsizes;
            /* Right-hand-side (RHS) nodes */
            std::vector<std::shared_ptr<RecurNode>> m_rhs_nodes;
    };
}
