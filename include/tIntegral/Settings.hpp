/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file contains settings of the library that should not be used outside.

   2019-04-11, Bin Gao:
   * first version
*/

#pragma once

#include <string>

#include "tGlueCore/Logger.hpp"

namespace tIntegral
{
    namespace Settings
    {
        /* Logger */
        tGlueCore::Logger* const logger = tGlueCore::LoggerSingleton::instance();
    }

    /* Get the logger */
    inline tGlueCore::Logger* get_logger() noexcept { return Settings::logger; }

    /* Get library name */
    inline std::string get_lib_name() noexcept { return std::string("tIntegral"); }

    /* Get library version */
    inline std::string get_lib_version() noexcept { return std::string("1.0.0"); }
}
