/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron operator integration with
   Gaussian type orbitals.

   2020-07-01, Bin Gao:
   * first version
*/

#pragma once

#include "tSymbolic/QuantChem/Operator.hpp"

#include "tIntegral/CartMultMoment.hpp"
#include "tIntegral/CartMultMomentHGTOIntegration.hpp"

namespace tIntegral
{
    template<typename RealType> class OneElecGTOIntegration
    {
        public:
            explicit OneElecGTOIntegration() noexcept = default;
            virtual ~OneElecGTOIntegration() noexcept = default;
            eval(std::shared_ptr<tBasisSet::GaussianFunction<tMolecular::Atom<RealType>,RealType>> braBasis,
                std::shared_ptr<tSymbolic::OneElecOperator> oneOper,
                std::shared_ptr<tBasisSet::GaussianFunction<tMolecular::Atom<RealType>,RealType>> ketBasis)
            {
            }
            virtual std::unique_ptr<RecurIntegration>
            make_oper_integration(const std::shared_ptr<OneElecOper> oper) const noexcept override
            {
                if (oper->type_id()==typeid(CartMultMoment)) {
                    return std::unique_ptr<RecurIntegration>(
                        new CartMultMomentHGTOIntegration(std::static_pointer_cast<CartMultMoment>(oper))
                    );
                }
                else if (oper->type_id()==typeid(NucAttractPotential)) {
                    return std::unique_ptr<RecurIntegration>(
                        new NucAttractPotentialHGTOIntegration(std::static_pointer_cast<NucAttractPotential>(oper))
                    );
                }
                else {
                    return std::unique_ptr<RecurIntegration>(nullptr);
                }
            }
            virtual std::unique_ptr<RecurIntegration>
            make_basis_integration(const std::shared_ptr<tBasisSet::BasisFunction> basis) const noexcept override
            {
                if (basis->type_id()==typeid(ContractedCGTO)) {
                    return std::unique_ptr<RecurIntegration>(
                        new ContractedCGTOIntegration(std::static_pointer_cast<ContractedCGTO>(basis))
                    );
                }
                else if (basis->type_id()==typeid(ContractedSGTO)) {
                    return std::unique_ptr<RecurIntegration>(
                        new ContractedSGTOIntegration(std::static_pointer_cast<ContractedSGTO>(basis))
                    );
                }
                else {
                    return std::unique_ptr<RecurIntegration>(nullptr);
                }
            }
    };
}
