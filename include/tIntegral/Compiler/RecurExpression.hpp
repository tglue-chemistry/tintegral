/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relations used for
   recurrence-relation compiler.

   2020-01-29, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/Index/RecurIndex.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/SymbolVisitor/RecurAnalyzer.hpp"

namespace tIntegral
{
    /* Recurrence relation of an index, where the first tells which indices are
       involved in the recurrence relation, the second tells the output index,
       and the third tells how the top-down and the bottom-up procedures will
       be built */
    class RecurExpression
    {
        public:
            /* indices should be sorted according to RecurIndex::less */
            explicit RecurExpression(const std::vector<std::shared_ptr<RecurIndex>> indices,
                                     const unsigned int outputPosition,
                                     const IndexCompileOption outputOption,
                                     const std::shared_ptr<RecurSymbol> RHS,
                                     const std::vector<unsigned int> noStride=std::vector<unsigned int>()) noexcept:
                m_indices(indices),
                m_current_index(0),
                m_output_position(outputPosition),
                m_output_option(outputOption),
                m_rhs(RHS)
            {
                m_idx_stride.assign(indices.size(), false);
                for (unsigned int pos=0; pos<indices.size()-1; ++pos) {
                    /* Two indices are not consecutive */
                    if (indices[pos]->get_position()!=indices[pos+1]->get_position()+1) {
                        if (std::find(noStride.cbegin(), noStride.cend(), pos)==noStride.cend()) {
                            m_idx_stride[pos] = true;
                        }
                    }
                }
                /* The last index */
                if (indices.back()->get_position()!=0) {
                    if (std::find(noStride.cbegin(), noStride.cend(), indices.size()-1)==noStride.cend()) {
                        m_idx_stride.back() = true;
                    }
                }
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "tIntegral::RecurExpression() gets indices ",
                                        tGlueCore::stringify(m_indices.cbegin(), m_indices.cend()),
                                        ", and stride needed ",
                                        tGlueCore::stringify(m_idx_stride.cbegin(), m_idx_stride.cend()));
#endif
            }
            virtual ~RecurExpression() noexcept = default;
            /* Get the compile option of the output index */
            inline IndexCompileOption get_output_option() const noexcept { return m_output_option; }
            /* Reset to the first index */
            inline void reset_index() noexcept { m_current_index = 0; }
            /* Move to next index */
            inline void next_index() noexcept { ++m_current_index; }
            /* Move to previous index */
            inline void prev_index() noexcept { --m_current_index; }
            /* Get the current index */
            inline std::shared_ptr<RecurIndex> get_index() const noexcept
            {
                return m_current_index>=0 && m_current_index<m_indices.size()
                    ? m_indices[m_current_index]
                    : std::shared_ptr<RecurIndex>();
            }
            /* If loop of stride of the current index is needed */
            inline bool is_stride_needed() const noexcept { return m_idx_stride[m_current_index]; }
            /* If we encounter the first index */
            inline bool is_first_index() const noexcept
            {
                return m_current_index==0;
            }
            /* If the current index is the output index */
            inline bool is_output_index() const noexcept
            {
                 return m_current_index==m_output_position;
            }
            /* Get the output index */
            inline std::shared_ptr<RecurIndex> get_output_index() const noexcept
            {
                return m_indices[m_output_position];
            }
            /* Get the position of the output index in the recurrence expression */
            inline unsigned int get_output_position(const bool reverse=false) const noexcept
            {
                return reverse ? m_indices.size()-m_output_position-1 : m_output_position;
            }
            /* Get all indices involved in the recurrence expression */
            inline std::vector<std::shared_ptr<RecurIndex>> get_indices() const noexcept
            {
                return m_indices;
            }
            /* Get the positions of indices involved in the recurrence expression */
            inline std::vector<unsigned int> get_idx_positions(const bool reverse=false) const noexcept
            {
                std::vector<unsigned int> idx_positions;
                if (reverse) {
                    for (auto const& recur_index: m_indices) {
                        idx_positions.insert(idx_positions.begin(), recur_index->get_position());
                    }
                }
                else {
                    for (auto const& recur_index: m_indices) {
                        idx_positions.push_back(recur_index->get_position());
                    }
                }
                return idx_positions;
            }
            /* Get the right hand side of the recurrence expression */
            inline std::shared_ptr<RecurSymbol> get_rhs() const noexcept { return m_rhs; }
            /* Make a recurrence-relation analyzer */
            inline bool make_analyzer(const RecurTraversal triangleTraversal) noexcept
            {
                if (m_output_option==IndexCompileOption::Recursion) {
                     m_analyzer = std::make_shared<RecurAnalyzer>(triangleTraversal);
                     if (!m_analyzer->assign(m_indices[m_output_position], m_rhs)) {
                         Settings::logger->write(tGlueCore::MessageType::Error,
                                                 "RecurExpression::make_analyzer() called for a recurrence relation ",
                                                 m_rhs->to_string(),
                                                 ", and the output index ",
                                                 m_indices[m_output_position]->to_string());
                         return false;
                     }
                }
                return true;
            }
            /* Get the recurrence-relation analyzer */
            inline std::shared_ptr<RecurAnalyzer> get_analyzer() const noexcept { return m_analyzer; }
            /* Convert the recurrence expression to string */
            inline std::string to_string() const noexcept
            {
                return "{recurrence: {indices: ["+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())
                    +"], output: "+std::to_string(m_output_position)
                    +", compile-option: "+stringify_compile_option(m_output_option)
                    +", rhs: "+m_rhs->to_string()+"}}";
            }
        private:
            std::vector<std::shared_ptr<RecurIndex>> m_indices;
            std::vector<bool> m_idx_stride;
            int m_current_index;
            unsigned int m_output_position;
            IndexCompileOption m_output_option;
            std::shared_ptr<RecurSymbol> m_rhs;
            std::shared_ptr<RecurAnalyzer> m_analyzer;
    };
}
