/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation compiler.

   2020-03-24, Bin Gao:
   * rewrite based on classes RecurIndex, RecurExpression

   2019-06-06, Bin Gao:
   * separate programming language specific parts from logical control parts
     (convert recurrence relations into loops), the former parts are put into
     individual virtual member functions, which can be overridden by derived
     classes

   2019-05-29, Bin Gao:
   * built on top of RecurGenerator

   2018-03-12, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <array>
#include <ctime>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Symbol/RecurDirection.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Recurrence/RecurTraversal.hpp"
#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/Index/RecurIndex.hpp"
#include "tIntegral/SymbolVisitor/RecurAnalyzer.hpp"
#include "tIntegral/SymbolVisitor/RecurVarProcessor.hpp"
#include "tIntegral/SymbolVisitor/RecurGenerator.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"
#include "tIntegral/Compiler/RecurExpression.hpp"
#include "tIntegral/Compiler/RecurRelation.hpp"
#include "tIntegral/Compiler/RecurConverterFactory.hpp"

//Interpreter, visitor (generate c++, GPU, validify & optimization, visualization, etc.), flyweight, template method

namespace tIntegral
{
    /* Recurrence relation compiler */
    class RecurCompiler
    {
        public:
            explicit RecurCompiler(std::shared_ptr<RecurConverterFactory> converterFactory,
                                   const bool vectorWise=true) noexcept:
                m_vector_wise(vectorWise),
                m_converter_factory(converterFactory),
                m_triangle_traversal(converterFactory->get_traversal()),
                m_translator(converterFactory->get_translator()),
                m_var_processor(converterFactory->get_translator()),
                m_generator(converterFactory->get_translator()),
                m_name_quadrature(std::string("quadrature")),
                m_name_quad_integrand(std::string("eval_at")),
                m_name_quad_abscissa(std::string("abscissa")) {}
            virtual ~RecurCompiler() noexcept = default;
            /* Set the recurrence relation converter factory */
            inline void set_converter_factory(std::shared_ptr<RecurConverterFactory> converterFactory) noexcept
            {
                m_converter_factory = converterFactory;
                /* Get the triangle traversal from the given converter factory */
                m_triangle_traversal = m_converter_factory->get_traversal();
                /* Get the recurrence relation translator */
                m_translator = m_converter_factory->get_translator();
                m_var_processor.set_translator(m_converter_factory->get_translator());
                m_generator.set_translator(m_converter_factory->get_translator());
            }
            /* Generate source codes for given functions of the integrand and indices */
            //FIXME: to implement
            //inline bool build(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
            //                  const std::vector<std::tuple<std::shared_ptr<RecurIndex>,
            //                                               std::shared_ptr<RecurIntegrand>,
            //                                               IndexCompileOption>>& indices,
            //                  const std::string& nameIntegration,
            //                  const std::string& descripIntegration,
            //                  const bool evalIntegral=true) noexcept
            //{
            //    RecurRelation recur_relation(integrand);
            //    return build(integrand,
            //                 recur_relation.get_expression(indices),
            //                 nameIntegration,
            //                 descripIntegration,
            //                 evalIntegral);
            //}
            /* Generate source codes for given recurrence expressions */
            inline bool build(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                              std::vector<RecurExpression>& expressions,
                              const std::string& nameIntegration,
                              const std::vector<std::string>& tparameters,
                              const std::string& descripIntegration,
                              const bool evalIntegral=true,
                              <std::shared_ptr<RecurIndex>,std::function<>> addon
                              //const bool doQuadrature=false) noexcept
            {
                if (m_translator->open_file(nameIntegration)) {
                    m_customer_index = false;
                    begin_namespace(integrand, doQuadrature);
                    if (build_analyzer(expressions)) {
                        if (build_integration_class(integrand,
                                                    expressions,
                                                    nameIntegration,
                                                    tparameters,
                                                    descripIntegration,
                                                    evalIntegral,
                                                    doQuadrature)) {
                            m_translator->end_class();
                            if (evalIntegral) {
                                m_translator->include_header_files(
                                    std::vector<std::string>({nameIntegration+"Eval"})
                                );
                            }
                            if (m_customer_index) {
                                m_translator->include_header_files(
                                    std::vector<std::string>({nameIntegration+"Customer"})
                                );
                            }
                            m_translator->end_namespace();
                            m_translator->close_file();
                            return true;
                        }
                        else {
                            Settings::logger->write(
                                tGlueCore::MessageType::Error,
                                "tIntegral::RecurCompiler::build() is unable to create class for the integration ",
                                nameIntegration
                            );
                            m_translator->close_file();
                            return false;
                        }
                    }
                    else {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::build() is unable to generate recurrence-relation analyzers for the integration ",
                            nameIntegration
                        );
                        return false;
                    }
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurCompiler::build() is unable to open file for the integration ",
                        nameIntegration
                    );
                    return false;
                }
            }
        protected:
            /* Begin the namespace of tIntegral */
            inline void begin_namespace(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                        const bool doQuadrature) noexcept
            {
                std::time_t current_time = std::time(nullptr);
                std::string str_copyright = "Generated by RecurCompiler of "
                                          + get_lib_name()
                                          + " ("
                                          + get_lib_version()
                                          + ")\n\n   "
                                          + std::string(std::asctime(std::localtime(&current_time)))
                                          + "\n";
                std::vector<std::string> integrand_types;
                for (auto const& each_fun: integrand) {
                    auto len_type = each_fun->type_name().find_first_of("<");
                    auto fun_type = len_type==std::string::npos
                                  ? each_fun->type_name()
                                  : each_fun->type_name().substr(0, len_type);
                    if (std::find(integrand_types.cbegin(),
                                  integrand_types.cend(),
                                  fun_type)==integrand_types.cend()) {
                        integrand_types.push_back(fun_type);
                    }
                }
                m_translator->begin_namespace(get_lib_name(), str_copyright, integrand_types, doQuadrature);
            }
            /* Generate analyzers for recurrence relations */
            inline bool build_analyzer(std::vector<RecurExpression>& expressions) noexcept
            {
                for (auto& each_expression: expressions) {
                    if (!each_expression.make_analyzer(m_triangle_traversal)) {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::build_analyzer() failed to generate analyzer for the recurrence expression ",
                            each_expression.to_string()
                        );
                        return false;
                    }
                }
                return true;
            }
            /* Build an integration class */
            bool build_integration_class(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                         std::vector<RecurExpression>& expressions,
                                         const std::string& nameIntegration,
                                         const std::vector<std::string>& tparameters,
                                         const std::string& descripIntegration,
                                         const bool evalIntegral,
                                         const bool doQuadrature) noexcept;
            /* Declare customer-implemented top-down and bottom-up functions */
            inline void declare_customer_function(const std::vector<RecurExpression>& expressions) noexcept
            {
                for (unsigned int iter_recur=0; iter_recur<expressions.size(); ++iter_recur) {
                    if (expressions[iter_recur].get_output_option()==IndexCompileOption::Customer) {
                        m_translator->declare_function(
                            m_translator->idx_recur_function(expressions[iter_recur].get_output_index()->get_name()),
                            m_translator->type_boolean(),
                            std::vector<std::string>(),
                            "Customer-implemented top-down function for the "
                                + std::to_string(expressions[iter_recur].get_output_index()->get_position())
                                + "-th index and using "
                                + m_translator->recur_array_object(iter_recur)
                        );
                        m_translator->declare_function(
                            m_translator->idx_recur_function(expressions[iter_recur].get_output_index()->get_name(),
                                                             false),
                            m_translator->type_boolean(),
                            std::vector<std::string>(),
                            "Customer-implemented bottom-up function for the "
                                + std::to_string(expressions[iter_recur].get_output_index()->get_position())
                                + "-th index and using "
                                + m_translator->recur_array_object(iter_recur)
                        );
                        /* Need to include header file containing customer-implemented functions */
                        m_customer_index = true;
                    }
                }
            }
            /* Build the top-down procedure of the integration class */
            bool build_top_down(const std::vector<RecurExpression>& expressions,
                                const std::string& nameIntegration,
                                const bool doQuadrature) noexcept;
            /* Build the bottom-up procedure of the integration class */
            bool build_bottom_up(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                 std::vector<RecurExpression>& expressions,
                                 const std::string& nameIntegration,
                                 const bool evalIntegral,
                                 const bool doQuadrature) noexcept;
            /* Loop over LHS nodes of a recurrence relation */
            inline void begin_lhs_loop(const unsigned int recurPosition) noexcept
            {
                m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                m_translator->handle_debug(std::vector<std::string>({
                    m_translator->string_literal(m_name_fexpression+" begins loops over LHS nodes")
                }));
                m_translator->endif_directive();
                m_translator->recur_array_begin_loop(recurPosition);
                m_translator->begin_while_loop(m_translator->recur_array_node_left(recurPosition));
                m_translator->assignment_statement(
                    m_translator->lhs_node_object(true),
                    RecurAssignment::BasicAssignment,
                    m_translator->recur_array_current_node(recurPosition)
                );
                m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                m_translator->handle_debug(std::vector<std::string>({
                    m_translator->string_literal(m_name_fexpression+" processes LHS node "),
                    m_translator->call_function(m_translator->lhs_node_object(),
                                                std::string("to_string"),
                                                std::vector<std::string>(),
                                                RecurOperandType::PointerOperand)
                }));
                m_translator->endif_directive();
            }
            /* Declare variables related to the current LHS node */
            virtual void declare_lhs_var() noexcept;
            /* Declare variables related to RHS nodes */
            virtual void declare_rhs_var() noexcept;
            /* Recursively convert a recurrence relation into loops over
               components of all indices
               @param[inout]:expression[Position of an index involved in the recurrence relation.] */
            bool to_loops() noexcept;
            /* End the loop over LHS nodes */
            inline void end_lhs_loop(const unsigned int recurPosition) noexcept
            {
                m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                m_translator->handle_debug(std::vector<std::string>({
                    m_translator->string_literal(m_name_fexpression+" finishes the computation of the LHS node "),
                    m_translator->call_function(m_translator->lhs_node_object(),
                                                std::string("to_string"),
                                                std::vector<std::string>({m_translator->recur_buffer_object()}),
                                                RecurOperandType::PointerOperand)
                }));
                m_translator->handle_debug(std::vector<std::string>({
                    m_translator->string_literal(m_name_fexpression+" proceeds next LHS node")
                }));
                m_translator->endif_directive();
                m_translator->recur_array_next_node(recurPosition);
                m_translator->end_while_loop();
            }
            /* Update pointers to values of the LHS and RHS nodes */
            virtual void update_node_values(const std::vector<unsigned int>& contributRHS) noexcept;
            /* Write extra loop between current index and its inner index */
            inline void write_stride_loop() noexcept
            {
                if (m_expression->is_stride_needed()) {
                    /* Get current index */
                    auto current_index = m_expression->get_index();
                    auto iter_stride = "stride_"+current_index->get_name();
                    /* Write a for loop from the stride */
                    m_translator->begin_for_loop(
                        m_translator->type_integer(),
                        std::vector<std::pair<std::string,std::string>>({
                            std::make_pair(iter_stride, std::string("0"))
                        }),
                        m_translator->comparison_operation(
                            iter_stride,
                            RecurComparison::LessThan,
                            /* Get the stride of the current index */
                            m_translator->call_function(
                                m_translator->lhs_node_object(),
                                std::string("get_stride"),
                                std::vector<std::string>({std::to_string(current_index->get_position())}),
                                RecurOperandType::PointerOperand
                            )
                        ),
                        std::vector<std::string>({
                            m_translator->inc_dec_operation(RecurIncDec::Increment, iter_stride)
                        })
                    );
                }
            }
            /* Invoke a given recurrence relation snippeter and set directions of
               RHS nodes for current index */
            inline bool invoke_snippeter(const std::vector<RecurSnippeter>& snippeters) noexcept
            {
                /* Get current index */
                auto current_index = m_expression->get_index();
                /* Find indicator of necessarily zeroth orders of the index */
                auto idx_zeros = m_zero_orders.begin();
                for (; idx_zeros!=m_zero_orders.end(); ++idx_zeros) {
                    if (current_index->equal_to(idx_zeros->first)) break;
                }
                for (auto const& snippeter: snippeters) {
                    /* (Re)set directions of RHS terms for the current allowed
                       decrements of the current index */
                    m_analyzer->reset_rhs_directions(current_index, std::get<0>(snippeter));
                    /* Invoker snippeter before proceeding the next index */
                    auto pre_snippeter = std::get<1>(snippeter);
                    if (pre_snippeter) {
                        idx_zeros->second = pre_snippeter(
                            current_index,
                            m_analyzer->is_order_needed(current_index, m_recur_direction)
                        );
                    }
                    else {
                        idx_zeros->second = std::array<bool,3>({false, false, false});
                    }
                    /* Write extra loop between the current index and its inner index */
                    write_stride_loop();
                    /* Proceed the next index of the current LHS node */
                    m_expression->next_index();
                    if (to_loops()) {
                        m_expression->prev_index();
                        auto post_snippeter = std::get<2>(snippeter);
                        if (post_snippeter) post_snippeter();
                        /* End extra loop between the current index and its inner index */
                        if (m_expression->is_stride_needed()) m_translator->end_for_loop();
                    }
                    else {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::invoke_snippeter() called with allowed decrements ",
                            (std::get<0>(snippeter)).to_string(),
                            " for the index ",
                            current_index->to_string()
                        );
                        return false;
                    }
                    /* (Re)set directions of RHS terms before considering the
                       current index */
                    m_analyzer->reset_rhs_directions(current_index);
                }
                return true;
            }
        private:
            /* Whether generated codes for vector variables are vectorwise */
            bool m_vector_wise;
            /* Recurrence relation converter factory */
            std::shared_ptr<RecurConverterFactory> m_converter_factory;
            /* The triangle traversal, and also the directions of how
               recurrence relations will be performed */
            RecurTraversal m_triangle_traversal;
            /* Get different variables used by the compiler */
            std::shared_ptr<RecurTranslator> m_translator;
            /* Variable processor */
            RecurVarProcessor m_var_processor;
            /* Recurrence relation generator */
            RecurGenerator m_generator;
            /* Whether including header file containing customer-implemented functions */
            bool m_customer_index;
            /* Iterator of expressions of recurrence relations */
            std::vector<RecurExpression>::iterator m_expression;
            /* Analyzer of the current recurrence relation  */
            std::shared_ptr<RecurAnalyzer> m_analyzer;
            /* Direction of the current recurrence relation */
            RecurDirection m_recur_direction;
            /* Whether indices with necessarily zeroth orders along X, Y and Z directions */
            std::vector<std::pair<std::shared_ptr<RecurIndex>,std::array<bool,3>>> m_zero_orders;
            /* Name of the function for each recurrence expression */
            std::string m_name_fexpression;
            /* Name of the quadrature object */
            std::string m_name_quadrature;
            /* Name of the integrand for quadrature */
            std::string m_name_quad_integrand;
            /* Name of the abscissa for quadrature */
            std::string m_name_quad_abscissa;
    };
}
