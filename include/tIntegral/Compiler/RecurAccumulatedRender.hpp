/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines classes to render RecurIntegration derived class, top-down
   and bottom-up procedures with operators accumulated.

   2020-11-23, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tIntegral/Compiler/RecurExpression.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"
#include "tIntegral/Compiler/RecurRender.hpp"

namespace tIntegral
{
    /* Render RecurIntegration derived class with operators accumulated */
    class RecurIntegrationAccRender: virtual public RecurIntegrationRender
    {
        public:
            explicit RecurIntegrationAccRender() noexcept = default;
            virtual ~RecurIntegrationAccRender() noexcept = default;
        protected:
            /* Get members of the RecurIntegration class */
            virtual std::vector<std::tuple<std::string,std::string,std::string,bool>>
            recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                      const unsigned int numExpressions,
                                      const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };

    /* Render top-down procedure with operators accumulated */
    class RecurTopDownAccRender: virtual public RecurTopDownRender
    {
        public:
            explicit RecurTopDownAccRender() noexcept = default;
            virtual ~RecurTopDownAccRender() noexcept = default;
        protected:
            /*Get arguments for the member function assign() of RecurArray */
            virtual std::vector<std::string>
            recur_assign_args(const RecurExpression& expression,
                              const unsigned int position,
                              const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };

    /* Render bottom-up procedure with operators accumulated */
    class RecurBottomUpAccRender: virtual public RecurBottomUpRender
    {
        public:
            explicit RecurBottomUpAccRender() noexcept = default;
            virtual ~RecurBottomUpAccRender() noexcept = default;
            /* Render the bottom-up procedure */
            virtual bool operator()(const std::vector<RecurExpression>& expressions,
                                    const bool evalIntegral,
                                    const std::string& nameIntegration,
                                    const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };
}
