/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines classes to render RecurIntegration derived class, top-down
   and bottom-up procedures with quadrature being used.

   2020-11-03, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Compiler/RecurExpression.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"
#include "tIntegral/Compiler/RecurRender.hpp"

namespace tIntegral
{
    /* Abstract render class with quadrature being used */
    class RecurQuadratureRender: virtual public RecurRender
    {
        public:
            explicit RecurQuadratureRender() noexcept = default;
            virtual ~RecurQuadratureRender() noexcept = default;
        protected:
            /* Return name of quadrature object */
            inline std::string quadrature_name() const noexcept
            {
                return std::string("quadrature");
            }
            /* Return name of abscissa */
            inline std::string abscissa_name() const noexcept
            {
                return std::string("abscissa");
            }
            /* Return name of the integrand evaluation function for quadrature */
            inline std::string quad_integrand_name() const noexcept
            {
                return std::string("eval_at");
            }
    };

    /* Render RecurIntegration derived class with quadrature being used */
    class RecurIntegrationQuadRender: virtual public RecurQuadratureRender,
                                      virtual public RecurIntegrationRender
    {
        public:
            explicit RecurIntegrationQuadRender() noexcept = default;
            virtual ~RecurIntegrationQuadRender() noexcept = default;
        protected:
            /* Get members of the RecurIntegration class */
            virtual std::vector<std::tuple<std::string,std::string,std::string,bool>>
            recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                      const unsigned int numExpressions,
                                      const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };

    /* Render top-down procedure with quadrature being used */
    class RecurTopDownQuadRender: virtual public RecurQuadratureRender,
                                  virtual public RecurTopDownRender
    {
        public:
            explicit RecurTopDownQuadRender() noexcept = default;
            virtual ~RecurTopDownQuadRender() noexcept = default;
        protected:
            /* Get arguments for the member function assign() of RecurArray */
            virtual std::vector<std::string>
            recur_assign_args(const RecurExpression& expression,
                              const unsigned int position,
                              const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };

    /* Render bottom-up procedure with quadrature being used */
    class RecurBottomUpQuadRender: virtual public RecurQuadratureRender,
                                   virtual public RecurBottomUpRender
    {
        public:
            explicit RecurBottomUpQuadRender() noexcept = default;
            virtual ~RecurBottomUpQuadRender() noexcept = default;
            /* Render the bottom-up procedure */
            virtual bool operator()(const std::vector<RecurExpression>& expressions,
                                    const bool evalIntegral,
                                    const std::string& nameIntegration,
                                    const std::shared_ptr<RecurTranslator>& translator) noexcept override;
    };
}
