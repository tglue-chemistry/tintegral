/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines classes to render RecurIntegration derived class, top-down
   and bottom-up procedures.

   2020-11-03, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Compiler/RecurExpression.hpp"
#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Translator/RecurTranslator.hpp"

namespace tIntegral
{
    /* Abstract render class with common functions */
    class RecurRender
    {
        public:
            explicit RecurRender() noexcept = default;
            virtual ~RecurRender() noexcept = default;
        protected:
            /* Return name of all indices */
            inline std::string all_indices_name() const noexcept
            {
                return std::string("allIndices");
            }
            /* Return name of buffer */
            inline std::string recur_buffer_name() const noexcept
            {
                return std::string("buffer");
            }
            /* Return name of RecurArray's */
            inline std::string recur_arrays_name() const noexcept
            {
                return std::string("recur_arrays");
            }
            /* Return an element of arrays of RecurArray */
            inline std::string recur_array_object(const unsigned int position,
                                                  const std::shared_ptr<RecurTranslator>& translator) noexcept
            {
                return translator->subscript_operation(
                    translator->member_object(recur_arrays_name()),
                    position
                );
            }
    };

    /* Render RecurIntegration derived class */
    class RecurIntegrationRender: virtual public RecurRender
    {
        public:
            explicit RecurIntegrationRender() noexcept = default;
            virtual ~RecurIntegrationRender() noexcept = default;
            /* Render the RecurIntegration derived class */
            virtual bool operator()(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                    const std::string& nameIntegration,
                                    const std::vector<std::string>& tparameters,
                                    const std::string& descripIntegration,
                                    const unsigned int numExpressions,
                                    const std::shared_ptr<RecurTranslator>& translator) noexcept;
        protected:
            /* Get members of the RecurIntegration class */
            virtual std::vector<std::tuple<std::string,std::string,std::string,bool>>
            recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                      const unsigned int numExpressions,
                                      const std::shared_ptr<RecurTranslator>& translator) noexcept;
    };

    /* Render top-down procedure */
    class RecurTopDownRender: virtual public RecurRender
    {
        public:
            explicit RecurTopDownRender() noexcept = default;
            virtual ~RecurTopDownRender() noexcept = default;
            /* Render the top-down procedure */
            virtual bool operator()(const std::vector<RecurExpression>& expressions,
                                    const std::string& nameIntegration,
                                    const std::shared_ptr<RecurTranslator>& translator) noexcept;
        protected:
            /* Return name of the top-down function */
            inline std::string top_down_name() const noexcept
            {
                return std::string("top_down");
            }
            /* Return name of output nodes */
            inline std::string output_nodes_name() const noexcept
            {
                return std::string("outputNodes");
            }
            /* Begin the top-down function */
            inline void begin_top_down(const std::shared_ptr<RecurTranslator>& translator) noexcept
            {
                translator->begin_function(
                    top_down_name(),
                    translator->type_boolean(),
                    std::vector<std::string>({
                        /* std::vector<std::vector<std::shared_ptr<RecurNode>>>&& */
                        translator->make_declaration(
                            translator->type_vector(
                                translator->type_recur_node(RecurTypeSpecifier::None), 2
                            ),
                            output_nodes_name(),
                            false,
                            RecurTypeSpecifier::None,
                            RecurDeclarator::RvalueReference
                        ),
                        /* const std::vector<std::shared_ptr<RecurIndex>>& */
                        translator->make_declaration(
                            translator->type_vector(translator->type_recur_index()),
                            all_indices_name(),
                            false,
                            RecurTypeSpecifier::Const,
                            RecurDeclarator::LvalueReference
                        ),
                        /* const std::shared_ptr<RecurBuffer<RealType>> */
                        translator->make_declaration(translator->type_recur_buffer(),
                                                     recur_buffer_name(),
                                                     false,
                                                     RecurTypeSpecifier::Const)
                    }),
                    std::string("Top-down procedure")
                );
            }
            /* Get arguments for the member function assign() of RecurArray */
            virtual std::vector<std::string>
            recur_assign_args(const RecurExpression& expression,
                              const unsigned int position,
                              const std::shared_ptr<RecurTranslator>& translator) noexcept;
    };

    /* Render bottom-up procedure */
    class RecurBottomUpRender: virtual public RecurRender
    {
        public:
            explicit RecurBottomUpRender() noexcept = default;
            virtual ~RecurBottomUpRender() noexcept = default;
            /* Render the bottom-up procedure */
            virtual bool operator()(const std::vector<RecurExpression>& expressions,
                                    const bool evalIntegral,
                                    const std::string& nameIntegration,
                                    const std::shared_ptr<RecurTranslator>& translator) noexcept;
        protected:
            /* Return name of the bottom-up function */
            inline std::string bottom_up_name() const noexcept
            {
                return std::string("bottom_up");
            }
            /* Return name of the bottom-up function of an expression */
            inline std::string bottom_up_name(const RecurExpression& expression) const noexcept
            {
                return expression.get_output_index()->get_name()+"_"+bottom_up_name();
            }
            /* Return name of customer-implemented function to evaluate integrals */
            inline std::string eval_function_name() const noexcept
            {
                return std::string("eval");
            }
    };
}
