/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file for building recurrence relation expressions
   from given integrand.

   2020-01-29, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/Index/RecurIndex.hpp"
#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/Symbol/RecurBasisFunction.hpp"
#include "tIntegral/Symbol/RecurOperator.hpp"
#include "tIntegral/Compiler/RecurExpression.hpp"

namespace tIntegral
{

    class RecurRelation
    {
        public:
            /* integrand is the product of given RecurIntegrand's */
            //explicit RecurRelation(const std::vector<std::shared_ptr<RecurIntegrand>> integrand) noexcept:
            //    m_integrand(integrand) {}
            explicit RecurRelation() noexcept = default;
            virtual ~RecurRelation() noexcept = default;
            /* Get recurrence relations from given indices */
            /*FIXME: An index can be either power or order of differentiation, and we try to move it outside integral */
            //std::vector<RecurExpression>
            //get_expression(const std::vector<std::pair<std::shared_ptr<RecurIndex>,IndexCompileOption>>& indices) noexcept;
            //FIXME: the following will be removed when the above function implemented
            std::vector<RecurExpression>
            get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                           const std::shared_ptr<RecurCartMultMoment>& oper,
                           const std::shared_ptr<RecurGaussianFunction>& ket) noexcept;
            std::vector<RecurExpression>
            get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                           const std::shared_ptr<RecurGaussianPotential>& oper,
                           const std::shared_ptr<RecurGaussianFunction>& ket) noexcept;
            std::vector<RecurExpression>
            get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                           const std::shared_ptr<RecurECPNonLocal>& oper,
                           const std::shared_ptr<RecurGaussianFunction>& ket) noexcept;
        //private:
        //    std::vector<std::shared_ptr<RecurIntegrand>> m_integrand;
    };
}
