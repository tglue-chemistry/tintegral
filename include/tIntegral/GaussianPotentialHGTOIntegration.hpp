/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates Gaussian weighted potential integrals with Gaussian
   functions.

   2020-11-25, Bin Gao:
   * first version
*/

#pragma once

#include <array>
#include <cmath>
#include <memory>
#include <utility>
#include <valarray>
#include <vector>

#include <functional>

#include "tGlueCore/Logger.hpp"
#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurIndex.hpp"
#include "tIntegral/Recurrence/RecurNode.hpp"
#include "tIntegral/Recurrence/RecurArray.hpp"
#include "tIntegral/Recurrence/RecurIntegration.hpp"

#include "tBasisSet/GaussianFunction.hpp"
#include "tIntegral/GaussianPotential.hpp"

namespace tIntegral
{
    /* Integration class for projected part of ECP with Hermite Gaussians */
    template<typename CentreType, typename RealType>
    class GaussianPotentialHGTOIntegration final: public RecurIntegration<RealType>
    {
        private:
            std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> m_braBasis;
            std::shared_ptr<GaussianPotential<CentreType,RealType>> m_oneOper;
            std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> m_ketBasis;
            std::shared_ptr<RecurBuffer<RealType>> m_buffer;
            std::unordered_map<int,std::shared_ptr<RecurIntegration<RealType>>> m_integration;
            std::shared_ptr<std::pair<RealType,RealType>> m_gaussian;
        public:
            explicit GaussianPotentialHGTOIntegration(
                std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> braBasis,
                std::shared_ptr<GaussianPotential<CentreType,RealType>> oneOper,
                std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> ketBasis,
                std::function<bool(RecurArray<RealType>&, std::function<void(const RealType)>)> quadrature) noexcept:
                m_braBasis(braBasis),
                m_oneOper(oneOper),
                m_ketBasis(ketBasis),
                m_quadrature(quadrature) {}
            ~GaussianPotentialHGTOIntegration() noexcept = default;
            /* Top-down procedure */
            bool top_down(
                std::vector<std::vector<std::shared_ptr<RecurNode>>>&& outputNodes,
                const std::vector<std::shared_ptr<RecurIndex>>& allIndices,
                const std::shared_ptr<RecurBuffer<RealType>> buffer) noexcept override
            {
                for (m_oneOper->begin(); !m_oneOper->end(); m_oneOper->next()) {
                }
                m_buffer=buffer;
                if (->top_down(outputNodes, allIndices, buffer)) {
                    return true;
                }
                else {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by GaussianPotentialHGTOIntegration::top_down()");
                    return false;
                }
            }
            /* Get input nodes for the integration and sorted according to the order of a given index */
            std::vector<std::vector<std::shared_ptr<RecurNode>>> get_input_nodes(const std::shared_ptr<RecurIndex>& index) noexcept override
            {
                return m_recur_arrays[4].get_input_nodes(index);
            }
            /* Bottom-up procedure */
            bool bottom_up() noexcept override
            {
                if (m_buffer->empty()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "GaussianPotentialHGTOIntegration::bottom_up called with empty buffer");
                    return false;
                }
                for (m_oneOper->begin(); !m_oneOper->end(); m_oneOper->next()) {
                    auto gaussian_potential = m_oneOper->get_gaussian_potential();
                    switch (std::get<0>(gaussian_potential)) {
                    }
                }
            }
        protected:
    };
}
