/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines the London orbital and its properties.

   2018-07-04, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/tBasicTypes.hpp"

namespace tIntegral
{
    class GaussianOrbitalIntegrator
    {
        public:
            explicit GaussianOrbitalIntegrator() noexcept;
            virtual ~GaussianOrbitalIntegrator() noexcept = default;
            virtual top_down() noexcept = 0;
            virtual bottom_up() noexcept = 0;
            make_contraction() noexcept;
    };

    class CartesianGTOIntegrator: public GaussianOrbitalIntegrator
    {
        public:
            explicit LondonOrbitalDerivative() noexcept;
            std::vector<std::shared_ptr<RecurIdxType>> get_idx_type() noexcept;
            virtual RecurRelation top_down() noexcept;
            virtual nonstd::expected<RecurBuffer,std::string> bottom_up() noexcept;
        protected:
            RecurIdxPosition idxGeo;
            RecurIdxPosition idx
    };

    class SphericalGTOIntegrator: public GaussianOrbitalIntegrator
    {
        public:
            virtual nonstd::expected<RecurBuffer,std::string> bottom_up() noexcept;
        protected:
            hgto_to_sgto() noexcept;
    };
}
