/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines the London orbital and its properties.

   2018-07-04, Bin Gao:
   * first version
*/

#pragma once

#include "tGlueCore/tBasicTypes.hpp"

namespace tIntegral
{
    /* Type of London phase factor, or exponential gauge factor */
    enum class PhasorType
    {
        NullPhasor,      /* No London phase factor */
        OrbitalPhasor,   /* London phase factor for atomic orbitals */
        OperatorPhasor,  /* London phase factor for operators */
        DualPhasor       /* Dual London phase factor for both atomic orbitals and operators */
    };

    /* London phase factor for (i) magnetic derivatives, (ii) derivatives
       with respect to total rotational angular momentum (see for example
       J. Chem. Phys. 105, 2804) */
    class LondonPhasor
    {
        public:
            explicit LondonPhasor(const PhasorType& phasorType=PhasorType::NullPhasor,
                                  const std::array<tReal,3>& phasorOrigin=std::array<tReal,3>{0.0,0.0,0.0}) noexcept:
                m_phasor_type(phasorType),
                m_phasor_origin(phasorOrigin) {}
            inline void set_phasor_type(const PhasorType& phasorType) noexcept
            {
                m_phasor_type = phasorType;
            }
            inline void set_phasor_origin(const std::array<tReal,3>& phasorOrigin) noexcept
            {
                m_phasor_origin = phasorOrigin;
            }
            inline PhasorType get_phasor_type() const noexcept
            {
                return m_phasor_type;
            }
            inline std::array<tReal,3> get_phasor_origin() const noexcept
            {
                return m_phasor_origin;
            }
            ~LondonPhasor() = default;
        private:
            /* Class LondonMomentIntegrator needs to access the origin of the
               London phase factor */
            friend LondonMomentIntegrator;
            /* Type of London phase factor */
            PhasorType m_phasor_type;
            /* Origin of the London phase factor */
            std::array<tReal,3> m_phasor_origin;
    };

    /* Multipole moments around the origin of London phase factor */
    class LondonMomentIntegrator
    {
        public:
            explicit LondonMomentIntegrator(const LondonPhasor& phasor,
                                            const tContrGTO& contrGTO) noexcept:
                m_gto_integrator(contrGTO)
            {
                m_gto_vector = contrGTO.get_coordinates();
                m_gto_exponents = contrGTO.get_exponents();
                m_gto_vector[0] -= phasor.m_phasor_origin[0];
                m_gto_vector[1] -= phasor.m_phasor_origin[1];
                m_gto_vector[2] -= phasor.m_phasor_origin[2];
            }
            RecurRelation top_down() noexcept;
            nonstd::expected<RecurBuffer,std::string> bottom_up() noexcept;
            ~LondonMomentIntegrator() noexcept = default;
        private:
            /* Integrator of contracted GTOs */
            GaussianOrbitalIntegrator m_gto_integrator;
            /* Vector from the origin of London phase factor to the center of
               contracted GTOs */
            std::array<tReal,3> m_gto_vector;
            /* Exponents of contracted GTOs */
            std::vector<tReal> m_gto_exponents;
            /**/
            RecurRelation m_recur_relation;
    };

    /* Derivative of the London phase factor for atomic orbitals */
    class LondonDerivativeIntegrator
    {
        public:
            explicit LondonDerivativeIntegrator(const std::shared_ptr<LondonMomentIntegrator>& momentIntegrator) noexcept:
                m_moment_integrator(momentIntegrator) {}
            std::vector<std::shared_ptr<RecurIdxType>> get_idx_type() noexcept;
            /* Top-down procedure */
            virtual RecurRelation top_down() noexcept;
            /* Bottom-up procedure */
            virtual nonstd::expected<RecurBuffer,std::string> bottom_up() noexcept;
            virtual ~LondonDerivativeIntegrator() = default;
        protected:
            RecurIdxPosition idxGeo;
            RecurIdxPosition idx;
            /* Integrator of multipole moments around the origin of London phase factor */
            std::shared_ptr<LondonMomentIntegrator> m_moment_integrator;
            /**/
            RecurRelation m_recur_relation;
    };

    /* Magnetic derivatives */
    class LondonMagneticIntegrator: public LondonDerivativeIntegrator
    {
        public:
            nonstd::expected<RecurBuffer,std::string> bottom_up() noexcept;
    };

    /* London phase factor for derivatives with respect to nuclear velocity,
       see for example J. Chem. Phys. 96, 5687 */

    ///* Type of relativistic corrections */
    //enum class RelativisticCorrection
    //{
    //    SR_CORRECTION=1,  /* scalar relativistic (SR) correction: 01 */
    //    SO_CORRECTION=2,  /* spin-orbit (SO) correction: 10 */
    //    SRC_AND_SOC=3     /* both SR and SO corrections: 11 */
    //};
}
