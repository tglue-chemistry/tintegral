/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of recurrence relation integration of
   contracted GTOs.

   2019-10-09, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tIntegral/Settings.hpp"
#include "tIntegral/Recurrence/RecurBuffer.hpp"
#include "tIntegral/Recurrence/RecurIntegration.hpp"

namespace tIntegral
{
    class ContractedGTOIntegration: virtual public RecurIntegration
    {
        public:
            explicit ContractedGTOIntegration(const std::shared_ptr<ContractedGTO> contractedGTO,
                                              const std::vector<RecurIndex>& recurIndices) noexcept:
                m_contracted_gto(contractedGTO),
                m_recur_indices(recurIndices) {}
            virtual ~ContractedGTOIntegration() noexcept = default;
            inline void set_integration(std::unique_ptr<RecurIntegration>&& integration) noexcept
            {
                m_integration = std::move(integration);
            }
            virtual bool top_down(std::vector<std::shared_ptr<RecurNode>>&& outputNodes,
                                  const std::vector<RecurIndex>& allIndices,
                                  const std::shared_ptr<RecurBuffer> buffer) noexcept override
            {
                m_contr_nodes = std::move(outputNodes);
                m_buffer = buffer;
                if (self_top_down()) {
                    if (m_integration) {
                        if (m_integration->top_down(std::vector<std::shared_ptr<RecurNode>>(m_prim_nodes.cbegin(),
                                                                                            m_prim_nodes.cend()),
                                                    allIndices,
                                                    buffer)) {
                            return true;
                        }
                        else {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "Called by ContractedGTOIntegration::top_down()");
                            return false;
                        }
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "No integration set for ContractedGTOIntegration::top_down()");
                        return false;
                    }
                }
                else {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by ContractedGTOIntegration::top_down()");
                    return false;
                }
            }
            virtual bool bottom_up() noexcept override
            {
                if (m_integration) {
                    m_integration->bottom_up();
                }
                else {
                }
            }
        protected:
            /* Convert contracted GTOs to primitive ones */
            bool self_top_down() noexcept
            {
                m_prim_nodes.clear();
                m_prim_nodes.reserve(m_contr_nodes.size());
                std::vector<std::size_t> offset_node(1, 0);
                std::vector<std::shared_ptr<std::size_t>> offset_level(1, std::make_shared<std::size_t>(0));
                for (auto const& contr_node: m_contr_nodes) {
                    auto prim_node = contr_node->reset_order(m_recur_indices[0],
                                                             0,
                                                             //m_contracted_gto->get_num_primitives(),
                                                             offset_level[0],
                                                             offset_node[0]);
                    if (prim_node) {
                        m_prim_nodes.push_back(std::make_shared<RecurNode>(std::move(prim_node)));
                        /* Update the offet of nodes */
                        offset_node[0] += m_prim_nodes.back()->get_size();
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "ContractedGTOIntegration::self_top_down() could not convert contractions to primitives for the index ",
                                                m_recur_indices[0].to_string());
                        return false;
                    }
                }
                /* Set offset of the level */
                m_buffer->template set_offsets(1, true, 1, offset_node.begin(), offset_level.begin());
                return true;
            }
            /* Convert primitive GTOs to contracted ones */
            bool self_bottom_up() noexcept
            {
                //std::vector<std::vector<std::vector<tReal>>>
                //stem:[\{w_{i\kappa}|\kappa;l\}]
                for (auto prim_node=m_prim_nodes.begin(),contr_node=m_contr_nodes.begin();
                     prim_node!=m_prim_nodes.end();
                     ++prim_node,++contr_node) {
                    /* Get contraction coefficients for the current node */
                    auto contr_coeff = m_contracted_gto->get_coefficients((*contr_node)->get_order(m_recur_indices[0]));
                    tReal* val_contr = m_buffer->get_buffer()+(*contr_node)->get_offset();
                    tReal* val_prim = m_buffer->get_buffer()+(*prim_node)->get_offset();
                    /* Convert primitive GTOs to contracted ones */
                    for (auto const& each_contr: contr_coeff) {
                        for (auto const& icoeff: each_contr) {
                            *val_contr += icoeff*(*val_prim);
                            ++val_prim;
                        }
                        ++val_contr;
                    }
                }
                return true;
            }
        private:
            std::shared_ptr<ContractedGTO> m_contracted_gto;
            std::vector<RecurIndex> m_recur_indices;
            std::unique_ptr<RecurIntegration> m_integration;
            std::shared_ptr<RecurBuffer> m_buffer;
            std::vector<std::shared_ptr<RecurNode>> m_contr_nodes;
            std::vector<std::shared_ptr<RecurNode>> m_prim_nodes;
    };
}
