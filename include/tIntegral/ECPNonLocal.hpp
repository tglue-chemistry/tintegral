/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of projected part of effective core potential.

   2019-05-12, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>
#include <utility>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/QuantChem/Operator.hpp"

#include "tIntegral/ECPGaussian.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* @class:ECPProjected[Projected part of effective core potential.] */
    template<typename CentreType, typename RealType=double>
    class ECPProjected: virtual public tSymbolic::OneElecOperator
                        //public std::enable_shared_from_this<ECPProjected>
    {
        public:
            /* @fn:ECPProjected()[Constructor.]
               @param[in]:centre[The ECP center stem:[\boldsymbol{C}].]
               @param[in]:powers[Powers of Gaussians (stem:[n_{kL}] and stem:[n_{kl}]).]
               @param[in]:exponents[Exponents of Gaussians (stem:[c_{kL}] and stem:[c_{kl}]).]
               @param[in]:coefficients[Coefficients of Gaussians (stem:[d_{kL}] and stem:[d_{kl}]).] */
            explicit ECPProjected(const std::shared_ptr<CentreType> centre,
                                  const std::vector<std::vector<ECPGaussian<RealType>>> gaussians) noexcept:
                m_centre(centre),
                m_gaussians(gaussians) {}
            /* @fn:~ECPProjected()[Deconstructor.] */
            virtual ~ECPProjected() noexcept = default;
            /* The following functions are required by the one-electron
               operator base class, and can be futher overridden by any derived
               class of `ECPProjected`.

               @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<tSymbolic::Symbol> get_copy() noexcept override
            {
                return std::make_shared<ECPProjected>(std::move(*this));
            }
            virtual std::string to_string() const noexcept override
            {
                str_oper = "{ecp-projected: {centre: "+m_centre->to_string()+", radial-part: [";
                for (unsigned int iang=0; iang<m_gaussians.size()-1; ++iang) {
                    str_oper += "{angular: "+std::to_string(iang)
                              + ", gaussians: "+tGlueCore::stringify(m_gaussians[iang].cbegin(),
                                                                     m_gaussians[iang].cend())
                              + "}, ";
                }
                return str_oper+"{angular: "+std::to_string(m_gaussians.size()-1)
                    +", gaussians: "+tGlueCore::stringify(m_gaussians.back().cbegin(), m_gaussians.back().cend())
                    +"}]}}";
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<tSymbolic::Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<ECPProjected> other_cast = std::dynamic_pointer_cast<ECPProjected>(other);
                    if (m_centre->equal_to(other_cast->m_centre)) {
                        if (m_gaussians.size()==other_cast->m_gaussians.size()) {
                            for (unsigned int iang=0; iang<m_gaussians.size(); ++iang) {
                                if (m_gaussians[iang].size()==other_cast->m_gaussians[iang].size()) {
                                    for (auto const& each_gausssian: m_gaussians[iang]) {
                                        auto not_found = true;
                                        for (auto const& other_gaussian: other_cast->m_gaussians[iang]) {
                                            if (each_gausssian==other_gaussian) {
                                                not_found = false;
                                                break;
                                            }
                                        }
                                        if (not_found) return false;
                                    }
                                }
                                else {
                                    return false;
                                }
                            }
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            /* Last but not least, there are a few functions specialized for
               the effective core potential.

               @fn:get_centre[Get the ECP center.]
               @return:[The ECP center.] */
            inline std::shared_ptr<CentreType> get_centre() const noexcept { return m_centre; }
            /* @fn:get_gaussians[Get Gaussian of effective core potential.]
               @return:[Gaussians of effective core potential.] */
            inline std::vector<std::vector<ECPGaussian<RealType>>> get_gaussians() const noexcept
            {
                return m_gaussians;
            }
        protected:
            /* @fn:oper_max_diff_order[Get the maximum order of differentiation of the operator with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation of the operator.] */
            virtual unsigned int oper_max_diff_order(const std::shared_ptr<tSymbolic::Symbol>& var) const noexcept override
            {
                //FIXME: to implement
                return 0;
            }
        private:
            /*@@ Internal data for representing effective core potential. */
            std::shared_ptr<CentreType> m_centre;
            /* Gaussians with angular momentum number from 0 to the largest one */
            std::vector<std::vector<ECPGaussian<RealType>>> m_gaussians;
    };
}
