/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of sum of Gaussian weighted potential.

   2020-11-12, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <set>
#include <string>
#include <tuple>
#include <typeindex>
#include <utility>
#include <vector>

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/QuantChem/Operator.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tIntegral
{
    /* @class:GaussianPotential[Sum of Gaussian weighted potential
       stem:[\sum_{k,C}d_{k}r_{C}^{n_{k}}\mathrm{e}^{-c_{k}r_{C}^{2}}] around
       centres stem:[C]'s, where stem:[n_{k}] can be 0, -1 and -2.] */
    template<typename CentreType, typename RealType=double>
    class GaussianPotential: virtual public tSymbolic::OneElecOperator
                             //public std::enable_shared_from_this<GaussianPotential<CentreType,RealType>>
    {
        public:
            using AtomicTerm = std::pair<std::shared_ptr<CentreType>,std::vector<std::tuple<int,RealType,RealType>>>;
            /* @fn:GaussianPotential()[Constructor.]
               @param[in]:potential[stem:[\sum_{k,C}d_{k}r_{C}^{n_{k}}\mathrm{e}^{-c_{k}r_{C}^{2}}].] */
            explicit GaussianPotential(const std::vector<AtomicTerm> potential) noexcept:
                m_potential(potential) {}
            /* @fn:~GaussianPotential()[Deconstructor.] */
            virtual ~GaussianPotential() noexcept = default;
            /* The following functions are required by the one-electron
               operator base class, and can be futher overridden by any derived
               class of `GaussianPotential`.

               @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<tSymbolic::Symbol> get_copy() noexcept override
            {
                return std::make_shared<GaussianPotential<CentreType,RealType>>(std::move(*this));
            }
            virtual std::string to_string() const noexcept override
            {
                std::string str_potential("{gaussian-potential: [");
                for (auto const& each_potential: m_potential) {
                    str_potential += "{centre: "+each_potential.first->to_string()+", [";
                    std::string str_gaussian;
                    for (auto const& each_gaussian: each_potential.second) {
                        str_gaussian += "{power: "+std::get<0>(each_gaussian)
                                      + ", exponent: "+std::get<1>(each_gaussian)
                                      + ", coefficient: "+std::get<2>(each_gaussian)+"}, ";
                    }
                    str_potential += str_gaussian.substr(0, str_gaussian.size()-2)+"]}, ";
                }
                return str_potential.substr(0, str_potential.size()-2)+"]}";
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<tSymbolic::Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<GaussianPotential<CentreType,RealType>> other_cast
                        = std::dynamic_pointer_cast<GaussianPotential<CentreType,RealType>>(other);
                    if (m_potential.size()==other_cast->m_potential.size()) {
                        for (auto const& each_potential: m_potential) {
                            auto potential_found = false;
                            for (auto const& other_potential: other_cast->m_potential) {
                                /* Find a sum of potential around the same
                                   centre, then check if all Gaussian weighted
                                   potential matches */
                                if (each_potential.first->equal_to(other_potential.first)) {
                                    if (each_potential.second.size()==other_potential.second.size()) {
                                        auto all_gaussian_found = true;
                                        /* Try to find matching Gaussian weighted potential */
                                        for (auto const& each_gaussian: each_potential.second) {
                                            auto gaussian_found = false;
                                            for (auto const& other_gaussian: other_potential.second) {
                                                if (each_gaussian==other_gaussian) {
                                                    gaussian_found = true;
                                                    break;
                                                }
                                            }
                                            if (!gaussian_found) {
                                                all_gaussian_found = false;
                                                break;
                                            }
                                        }
                                        /* Both the potential centre and all
                                           Gaussian weighted potential match */
                                        if (all_gaussian_found) {
                                            potential_found = true;
                                            break;
                                        }
                                    }
                                }
                                /* The sum of atomic potential is found */
                                if (potential_found) break;
                            }
                            if (!potential_found) return false;
                        }
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            /* Last but not least, there are a few functions specialized for
               the sumf of Gaussian weighted potential.

              @fn:begin[Move to the first Gaussian weighted potential.] */
            inline void begin() noexcept
            {
                m_iter_centre = m_potential.cbegin();
                m_iter_gaussian = m_iter_centre->second.cbegin();
            }
            /* @fn:left[Check if there is any Gaussian weighted potential left.]
               @return:[Boolean indicating if any Gaussian weighted potential left.] */
            inline bool left() const noexcept
            {
                return m_iter_centre!=m_potential.cend();
            }
            /* @fn:next[Move to the next Gaussian weighted potential.] */
            inline void next() noexcept
            {
                ++m_iter_gaussian;
                if (m_iter_gaussian==m_iter_centre->second.cend()) {
                    ++m_iter_centre;
                    if (m_iter_centre!=m_potential.cend()) m_iter_gaussian = m_iter_centre->second.cbegin();
                }
            }
            /* @fn:get_centre[Get the centre of current Gaussian weighted potential.] */
            inline std::shared_ptr<CentreType> get_centre() const noexcept
            {
                return m_iter_centre->first;
            }
            /* @fn:get_power[Get the power of current Gaussian weighted potential.] */
            inline int get_power() const noexcept
            {
                return std::get<0>(*m_iter_gaussian);
            }
            /* @fn:get_exponent[Get the exponent of current Gaussian weighted potential.] */
            inline RealType get_exponent() const noexcept
            {
                return std::get<1>(*m_iter_gaussian);
            }
            /* @fn:get_coefficient[Get the coefficient of current Gaussian weighted potential.] */
            inline RealType get_coefficient() const noexcept
            {
                return std::get<2>(*m_iter_gaussian);
            }
            /* @fn:get_unique_powers[Get unqiue powers.]
               @return:[Unique powers.] */
            inline std::set<int> get_unique_powers() noexcept
            {
                if (m_unique_powers.empty()) {
                    for (auto const& each_potential: m_potential) {
                        for (auto const& each_gaussian: each_potential.second) {
                            m_unique_powers.insert(std::get<0>(each_gaussian));
                        }
                    }
                }
                return m_unique_powers;
            }
        protected:
            /* @fn:oper_max_diff_order[Get the maximum order of differentiation of the operator with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation of the operator.] */
            virtual unsigned int oper_max_diff_order(const std::shared_ptr<tSymbolic::Symbol>& var) const noexcept override
            {
                //FIXME: to implement
                return 0;
            }
        private:
            /*@@ Internal data for representing the sum of Gaussian weighted potential. */
            std::vector<AtomicTerm> m_potential;
            /* Unqiue powers */
            std::set<int> m_unique_powers;
            /* Iterator of of centres */
            std::vector<AtomicTerm>::const_iterator m_iter_centre;
            /* Iterator of Gaussian weighted potential around one centre */
            std::vector<std::tuple<int,RealType,RealType>>::const_iterator m_iter_gaussian;
    };
}
