/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file declares mathematical special functions.

   2022-03-10, Bin Gao:
   * move the evaluation of the zeroth order Boys functions and maximum order
     of upward recurrence relation here

   2022-02-26, Bin Gao:
   * first version
*/

#pragma once

#include <cmath>
#include <valarray>

namespace tIntegral
{
    /* Inline function to compute the zeroth order Boys functions by using
       rational approximation or asymptotic approximation. */
#ifdef TINTEGRAL_BUILD_GPU
    __device__ inline double get_boys0(const double arg) noexcept
#else
    inline double get_boys0(const double arg) noexcept
#endif
    {
#ifndef TINTEGRAL_BUILD_GPU
        using namespace std;
#endif
        if (arg<14) {
            if (arg<7) {
                if (arg<=1) {
                    return fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg, 7.7431289765679954e-6, 5.8013130589625492e-4),
                                           8.6585028922695218e-3),
                                       1.0891198266892342e-1),
                                   4.2131665053371023e-1),
                               2.2699378135344061)
                        / fma(arg,
                              fma(arg,
                                  fma(arg,
                                      fma(arg,
                                          fma(arg, 1.0298955801000000e-4, 2.8048200242871830e-3),
                                          3.6432514971616124e-2),
                                      2.7457239744000644e-1),
                                  1.1779625883785794),
                              2.2699378135344052);
                }
                else {
                    return fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg,
                                               fma(arg,
                                                   fma(arg, 3.5960048693138910e-9, 7.1643556269285254e-7),
                                                   1.5870305976743249e-5),
                                               4.2490684401893097e-4),
                                           3.7633480042344360e-3),
                                       4.9552928157715852e-2),
                                   1.6290857280445508e-1),
                               1.0009574989081021)
                        / fma(arg,
                              fma(arg,
                                  fma(arg,
                                      fma(arg,
                                          fma(arg,
                                              fma(arg,
                                                  fma(arg, 8.9566896774000000e-8, 4.1127103555045216e-6),
                                                  9.8889354639715169e-5),
                                              1.5377757781070642e-3),
                                          1.6265407498397557e-2),
                                      1.1497753571617642e-1),
                                  4.9656107244530340e-1),
                              1.0009574989081711);
                }
            }
            else {
                return fma(arg,
                           fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg, 7.9475160723469155e-8, 3.0021547331061438e-5),
                                           8.0758016599267036e-4),
                                       5.7523103435856937e-3),
                                   9.2620173573973509e-2),
                               2.6336316471821378e-1),
                           1.8164260035138938)
                    / fma(arg,
                          fma(arg,
                              fma(arg,
                                  fma(arg,
                                      fma(arg,
                                          fma(arg, 2.6822538886900000e-6, 2.3110686851335755e-4),
                                          2.2813747204096204e-3),
                                      3.0362848990260044e-2),
                                  1.9755461915739980e-1),
                              8.7280278995828052e-1),
                          1.8140634810945542);
            }
        }
        else {
            if (arg<33.979486053733204) {
                if (arg<24) {
                    return fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg,
                                               fma(arg, 8.9542463811303144e-10, 3.6781509529341276e-6),
                                               6.5327071645625474e-4),
                                           1.5967177645639659e-2),
                                       -1.488465526890009e-2),
                                   2.5736448757234603e-1),
                               4.3764784276937068)
                        / fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg,
                                               fma(arg, 1.2054799263999999e-7, 7.1877319572422917e-5),
                                               4.7010810874638315e-3),
                                           3.3003798557026363e-2),
                                       -1.4075723480377397e-1),
                                   2.4282115916600806),
                               3.5608760360528033);
                }
                else {
                    return fma(arg,
                               fma(arg,
                                   fma(arg,
                                       fma(arg,
                                           fma(arg, 2.2383233017690159e-9, 4.4632921922756607e-6),
                                           9.4261542394334685e-4),
                                       4.5484176824780904e-2),
                                   5.512874670823531e-1),
                               1.2328661338049685)
                        / fma(arg,
                              fma(arg,
                                  fma(arg,
                                      fma(arg,
                                          fma(arg, 1.6802338392999999e-7, 8.9914265569741484e-5),
                                          8.761638447571796e-3),
                                      2.1391828920201175e-1),
                                  1.2024641836653918),
                              7.2995870525088935e-1);
                }
            }
            /* Asymptotic approximation of the zeroth order Boys function
               stem:[\frac{1}{2}\sqrt{\frac{\pi}{x}}] */
            else {
                return 0.886226925452758/sqrt(arg);
            }
        }
    }

    /* Inline function to compute the fitted maximum order of upward recurrence
       relation of Boys function with a relative error around stem:[10^{-13}]. */
#ifdef TINTEGRAL_BUILD_GPU
    __host__ __device__ inline int boys_max_upward(const double arg) noexcept
#else
    inline int boys_max_upward(const double arg) noexcept
#endif
    {
        /* Find the maximum order of the upward recurrence relation using
           binary search algorithm */
        if (arg<14) {
            if (arg<=1) {
                if (arg<0.1354269110942593) {
                    return 0;
                }
                else {
                    return arg<0.7165996410102796 ? 1 : 2;
                }
            }
            else {
                return arg<7
                    ? int(round(1.0271416199465477*arg+1.1996335202138093))
                    : int(round(0.8731303958466737*arg+2.617130843609926));
            }
        }
        else {
            if (arg<33.979486053733204) {
                if (arg<24) {
                    return int(round(0.8112918626626681*arg+3.551454609409314));
                }
                else {
                    return int(round(0.7281535504425891*arg+5.573915688568794));
                }
            }
            else {
                if (arg<59.704812307223996) {
                    return int(round((-0.012789123555662153*arg+1.6780263446165908)*arg-12.329164371572396));
                }
                else {
                    return arg<611.0987161651447
                        ? int(round(0.7276372226801865*arg+5.8418273861665915))
                        : 450;
                }
            }
        }
    }

#ifndef TINTEGRAL_BUILD_GPU
    /* Initialize an array stem:[\frac{1}{n+0.5}] from 0 to 450 for downward
       recurrence relation of Boys functions. Users do not need to care it. */
    extern double _boys_inited;
    extern double _nhalf_inverse[451];
    void _init_boys() noexcept;
#endif

    /* Compute Boys functions */
    //extern "C"
    void get_boys(const int maxOrder, const int numArgs, const double* args, double* vals) noexcept;

    /* Compute upper bounds of relative errors of Boys functions, which should
       be called after \fn(get_boys). The default relative errors of arguments
       are 1.1102230246251565e-16. */
    void get_boys_errors(
        const int maxOrder,
        const int numArgs,
        const double* args,
        const double* vals,
        double* errors,
        const double* argErrors=nullptr) noexcept;

    /* Compute G functions */
    std::valarray<double> get_gfun(const int maxOrder, const std::valarray<double>& args) noexcept;
}
