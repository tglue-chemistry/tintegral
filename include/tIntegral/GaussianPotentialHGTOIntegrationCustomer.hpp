/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements customer top-down and bottom-up functions for
   geometrical derivative of centre of the sum of Gaussian weighted potential
   integrals with Gaussian functions.

   2020-11-27, Bin Gao:
   * first version
*/

    class GaussianPotentialHGTOIntegration: virtual public RecurIntegration
    {
        private:
            GaussianInv0PotHGTOIntegration m_inv0_integration;
            GaussianInv1PotHGTOIntegration m_inv1_integration;
            GaussianInv2PotHGTOIntegration m_inv2_integration;
            std::shared_ptr<std::tuple<std::shared_ptr<CentreType>,RealType,RealType>> m_gauss_potential;

            std::shared_ptr<CentreType> m_gauss_centre;
            RealType m_gauss_exponent;
            RealType m_gauss_coefficient;
        public:
            explicit GaussianPotentialHGTOIntegration(
                std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> braBasis,
                std::shared_ptr<GaussianPotentialpotential<CentreType,RealType>> oneOper,
                std::shared_ptr<tBasisSet::GaussianFunction<CentreType,RealType>> ketBasis,
            ) noexcept:
                m_oneOper(oneOper),
            {
            }
    };

template<typename CentreType, typename RealType>
bool GaussianPotentialHGTOIntegration<CentreType,RealType>::OperGeo_top_down() noexcept
{
    for (auto const& each_power: m_oneOper->get_unique_powers()) {
        switch (each_power) {
            case 0:
                /* Recurrence relation of geometrical derivative of the
                   potential centre begins from the third jagged array for
                   different radial powers */
                if (!m_recur_arrays[2].assign(
                        m_recur_arrays[1].get_input_nodes(allIndices[?]),
                        allIndices,
                        std::vector<unsigned int>({?}),
                        0,
                        std::vector<std::vector<int>>({
                            std::vector<int>({-1}),
                            std::vector<int>({-2})}),
                        m_buffer)) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "Called by GaussianPotentialHGTOIntegration::OperGeo_top_down() for power 0"
                    );
                    return false;
                }
                break;
            case -1:
                if (!m_recur_arrays[3].assign(
                        m_recur_arrays[1].get_input_nodes(allIndices[?]),
                        allIndices,
                        std::vector<unsigned int>({??,?}),
                        1,
                        std::vector<std::vector<int>>({
                            std::vector<int>({0,-1}),
                            std::vector<int>({0,-2}),
                            std::vector<int>({1,-1}),
                            std::vector<int>({1,-2})}),
                        m_buffer)) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "Called by GaussianPotentialHGTOIntegration::OperGeo_top_down() for power -1"
                    );
                    return false;
                }
                break;
            case -2:
                if (!m_recur_arrays[4].assign(
                        m_recur_arrays[1].get_input_nodes(allIndices[?]),
                        allIndices,
                        std::vector<unsigned int>({??,?}),
                        1,
                        std::vector<std::vector<int>>({
                            std::vector<int>({0,-1}),
                            std::vector<int>({0,-2}),
                            std::vector<int>({1,-1}),
                            std::vector<int>({1,-2})}),
                        m_buffer)) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "Called by GaussianPotentialHGTOIntegration::OperGeo_top_down() for power -2"
                    );
                    return false;
                }
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Unsupported power ",
                                        each_power,
                                        " in GaussianPotentialHGTOIntegration::OperGeo_top_down()");
                return false;
        }
    }
    return true;
}

template<typename CentreType, typename RealType>
bool GaussianPotentialHGTOIntegration<CentreType,RealType>::OperGeo_bottom_up() noexcept
{
    /* Compute integrals of the first Gaussian weighted potential */
    m_oneOper->begin();
    m_gauss_centre = m_oneOper->get_centre();
    m_gauss_exponent = m_oneOper->get_exponent();
    m_gauss_coefficient = m_oneOper->get_coefficient();
    switch (m_oneOper->get_power()) {
        case 0:
            eval(0);
            RadialPower0_bottom_up();
            break;
        case -1:
            eval(-1);
            RadialPowerInv1_bottom_up();
            break;
        case -2:
            eval(-2);
            RadialPowerInv2_bottom_up();
            break;
        default:
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Unsupported power for the first Gaussian weighted potential ",
                                    m_oneOper->get_power(),
                                    " in GaussianPotentialHGTOIntegration::OperGeo_bottom_up()");
            return false;
    }
    /* Add integrals of other Gaussian weighted potential */
    if (m_oneOper->left()) {
        /* Integrals to be added belong to input nodes of the second jagged array */
        auto integrals_added = m_recur_arrays[1].copy_input_buffer();
        for (; m_oneOper->left(); m_oneOper->next()) {
            m_gauss_centre = m_oneOper->get_centre();
            m_gauss_exponent = m_oneOper->get_exponent();
            m_gauss_coefficient = m_oneOper->get_coefficient();
            switch (m_oneOper->get_power()) {
                case 0:
                    eval(0);
                    RadialPower0_bottom_up();
                    break;
                case -1:
                    eval(-1);
                    RadialPowerInv1_bottom_up();
                    break;
                case -2:
                    eval(-2);
                    RadialPowerInv2_bottom_up();
                    break;
                default:
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Unsupported power ",
                                            m_oneOper->get_power(),
                                            " in GaussianPotentialHGTOIntegration::OperGeo_bottom_up()");
                    return false;
            }
            /* Add integrals of current Gaussian weighted potential */
            m_recur_arrays[1].axpy_input_buffer(integrals_added);
        }
        /* Copy sum of integrals back */
        m_recur_arrays[1].set_input_buffer(integrals_added);
    }
    return true;
}
