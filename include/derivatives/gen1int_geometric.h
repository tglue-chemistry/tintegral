/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of geometric derivatives.

   2014-06-24, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_GEOMETRIC_H)
#define GEN1INT_GEOMETRIC_H

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"
/* uses GMIN_IDX_ATOM and GMAX_IDX_NAT */
#include "utilities/gen1int_sort.h"
/* combinatorics */
#include "utilities/gen1int_combin.h"

/* # context of geometric derivatives */
typedef struct {
    GBool assembled;           /* indicates if the context of geometric derivatives assembled or not */
    /* set by input arguments */
    GInt order_geo;            /* order of geometric derivatives */
    GInt range_num_cent[2];    /* range of the number of differentiated centers */
    GInt user_range_idx[2];    /* user specified range of indices of differentiated centers */
    GInt num_user_idx;         /* number of user specified differentiated centers */
    GInt *user_idx;            /* indices of user specified differentiated centers */
    /* the following variables are used for finding all possible geometric derivatives */
    GInt curr_num_cent;        /* current number of differentiated centers */
    BitCombin *combin_cent;    /* combinations of differentiated centers */
    BitCombin *combin_order;   /* combinations of the order of geometric derivatives partition */
    GULong rank_combin_cent;   /* rank of current combination of differentiated centers */
    GULong rank_combin_order;  /* rank of current combination of the order partition */
    GInt *combin_idx_cent;     /* indices of differentiated centers for current combination */
    GInt *combin_order_cent;   /* orders of differentiated centers for current combination */
    GULong *num_combin_cent;   /* number of combinations for choosing specific number of centers */
    GULong *num_combin_order;  /* number of combinations for partitioning the order of derivatives */
    GULong *cent_size_combin;  /* sizes of combinations of differentiated centers and order partition
                                  for different numbers of differentiated centers, computed as
                                  num_combin_cent[i]*num_combin_order[i] */
    GULong size_combin;        /* size of all the combinations of differentiated centers and order
                                  partition, computed as \sum_{i}cent_size_combin[i] */
    GULong *cent_size_deriv;   /* sizes of geometric derivatives for different numbers of
                                  differentiated centers */
    GULong size_deriv;         /* size of all the geometric derivatives */
} GeoDeriv;

/* functions related to geometric derivatives */
extern GErrorCode GeoDerivCreate(GeoDeriv*,const GInt,const GInt[]);
extern GErrorCode GeoDerivSetRangeIdx(GeoDeriv*,const GInt[]);
extern GErrorCode GeoDerivSetUserIdx(GeoDeriv*,const GInt,const GInt*,const GBool);
extern GErrorCode GeoDerivAssemble(GeoDeriv*);
extern GErrorCode GeoDerivDestroy(GeoDeriv*);
extern GErrorCode GeoDerivGetOrder(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivGetRangeNumCent(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivGetRangeIdx(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivGetNumUserIdx(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivGetUserIdx(const GeoDeriv*,const GInt,GInt*);
/* the following functions should be called after GeoDerivAssemble() */
extern GErrorCode GeoDerivGetCentSizeCombin(const GeoDeriv*,const GInt,GULong*);
extern GErrorCode GeoDerivGetSizeCombin(const GeoDeriv*,GULong*);
extern GErrorCode GeoDerivGetCentSizeDeriv(const GeoDeriv*,const GInt,GULong*);
extern GErrorCode GeoDerivGetSizeDeriv(const GeoDeriv*,GULong*);
extern GErrorCode GeoDerivNextCombin(GeoDeriv*,const GULong);
extern GErrorCode GeoDerivCombinGetNumCent(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivCombinGetIdxCent(const GeoDeriv*,const GInt,GInt*);
extern GErrorCode GeoDerivCombinGetOrderCent(const GeoDeriv*,const GInt,GInt*);
extern GErrorCode GeoDerivCombinGetNumDeriv(const GeoDeriv*,GInt*);
extern GErrorCode GeoDerivWrite(const GeoDeriv*,const GChar*);
extern GErrorCode GeoDerivDuplicate(const GeoDeriv*,GeoDeriv*);

/* internal use, no safety check inside */
extern GErrorCode GeoDerivCombinUpdateIdxCent(GeoDeriv*);
extern GErrorCode GeoDerivCombinUpdateOrderCent(GeoDeriv*);

/* # context of partial geometric derivatives */
typedef struct {
    GBool created;             /* indicates if the context of partial geometric derivatives created */
    /* information of total geometric derivatives */
    GInt geo_num_cent;         /* number of differentiated centers of geometric derivatives */
    GInt *geo_order;           /* orders of geometric derivatives */
    GInt *geo_num_repeated;    /* number of repeated differentiated centers in the integrand */
    GInt *geo_idx_integrand;   /* indices of differentiated centers in the integrand */
    /* information of integrand */
    GInt num_integrand_cent;   /* number of centers of integrand */
    GInt *size_geo_integrand;  /* size of partial geometric derivatives on the integrand */
    GInt max_repeated_cent;    /* maximum number of repeated differentiated centers in the integrand */
    GInt idx_max_repeated;     /* index of differentiated center with maximum repetitions in the integrand */
    GInt order_max_repeated;   /* order of differentiated center with maximum repetitions in the integrand */
    /* information of translational invariance */
    GBool trans_invariance;    /* if using translational invariance or not */
    GULong trans_num_terms;    /* number of terms in multinomial expansion for translational invariance */
    GULong *trans_mult_coef;   /* multinomial coefficients for translational invariance */
    GInt **trans_order_cent;   /* orders of each expansion term using translational invariance */
    GULong iter_trans;         /* iterator of expansion terms of translational invariance */
    /* information of multinomial expansions */
    MultExp **mult_exp;        /* multinomial expansions for geometric derivatives, size is
                                  \var{geo_num_cent} or \var{trans_num_terms}*\var{geo_num_cent}
                                  if translational invariance used */
    GInt *mult_exp_order;      /* orders of derivatives from multinomial expansion */
    GULong **size_mult_exp;    /* size of each multinomial expansion */
    GULong *iter_mult_exp;     /* iterators for the multinomial expansions */
    GULong size_part_geo;      /* size of partial geometric derivatives */
} PartGeo;

/* functions related to partial geometric derivatives */
extern GErrorCode PartGeoCreate(PartGeo*,const GeoDeriv*,const GInt,GInt*,GBool*);
extern GErrorCode PartGeoGetSize(const PartGeo*,GULong*);
extern GErrorCode PartGeoGetComposition(PartGeo*,GULong*,GInt*);
extern GErrorCode PartGeoNextComposition(PartGeo*);
extern GErrorCode PartGeoGetDeriv(PartGeo*,GInt*,const GInt,GReal**,GReal**);
extern GErrorCode PartGeoWrite(const PartGeo*,FILE*);
extern GErrorCode PartGeoDestroy(PartGeo*);

#endif
