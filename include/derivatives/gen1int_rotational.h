/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of derivatives w.r.t. total rotational angular momentum.

   2014-06-21, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_ROTATIONAL_H)
#define GEN1INT_ROTATIONAL_H

#include <stdio.h>

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"
/* type of London phase factor */
#include "derivatives/gen1int_london.h"

/* London phase factor for derivatives w.r.t. total rotational angular momentum */
typedef struct {
    LPFType LPF_type;           /* type of London phase factor */
    GReal center_of_mass[3];    /* center of mass of the system */
    GReal diag_inv_inertia[3];  /* diagonal elements of the inverse of inertia tensor */
    GReal origin_LPF[3];        /* origin of the London phase factor */
} RotLPF;

/* functions related to total rotational angular momentum derivatives */
extern GVoid RotLPFCreate(RotLPF*,const GReal[],const GReal[],const GReal[]);
extern GVoid RotLPFSetCIAO(RotLPF*);
extern GVoid RotLPFSetCIOP(RotLPF*);
extern GErrorCode RotLPFWrite(const RotLPF*,FILE*);
extern GVoid RotLPFDuplicate(const RotLPF*,RotLPF*);

#endif
