/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file defines the type of London phase factor.

   2014-06-22, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_LONDON_H)
#define GEN1INT_LONDON_H

/* the following defined enum types should not be changed, otherwise, the functions
   MagLPFSetGIAO(), MagLPFSetGIOP(), RotLPFSetGIAO() and RotLPFSetGIOP() may need
   to be modified */
typedef enum {
    LPF_NONE=0,     /* no London phase factor: 00 */
    LPF_ONLY_AO=1,  /* London phase factor for atomic orbitals: 01 */
    LPF_ONLY_OP=2,  /* London phase factor for operators: 10 */
    LPF_AO_OP=3     /* London phase factor for atomic orbitals and operators: 11 */
} LPFType;

#endif
