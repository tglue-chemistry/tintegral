/* Gen1Int: not only one-electron integral library
   Copyright 2014 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file is the header file of relativistic corrections.

   2014-06-25, Bin Gao:
   * first version
*/

#if !defined(GEN1INT_RELATIVISTIC_H)
#define GEN1INT_RELATIVISTIC_H

/* error handling */
#include "common/gen1int_error.h"
/* basic types used in Gen1Int */
#include "common/gen1int_type.h"

/* the enum types defined here should not be changed, otherwise, all the functions
   in src/relativistic may need to be modified */

/* type of relativistic corrections */
typedef enum {
    NR_CORRECTION=0,  /* nonrelativistic: 00 */
    SR_CORRECTION=1,  /* scalar relativistic (SR) correction: 01 */
    SO_CORRECTION=2,  /* spin-orbit (SO) correction: 10 */
    SRC_AND_SOC=3     /* both SR and SO corrections: 11 */
} RCType;

/* type of the differentiated charge distributions */
typedef enum {
    DIFF_CD_ZERO,   /* non-differentiated charge distributions */
    DIFF_CD_FIRST,  /* the first order differentiated charge distributions */
    DIFF_CD_SECOND  /* the second order differentiated charge distributions */
} DCDType;

/* functions related to relativistic corrections */
extern GVoid RelCorrectionSetType(RCType*,const RCType);
/*extern GErrorCode RelCorrectionGetOneInts(const RCType,
                                          const DCDType,);*/
/*extern GErrorCode RelCorrectionGetTwoInts(const RCType,
                                          const DCDType,);*/

#endif
