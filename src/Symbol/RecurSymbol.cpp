/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of recurrence relation symbols.

   2020-01-29, Bin Gao:
   * introduce the RecurIntegrand class and its derived classes for deriving
     recurrence relations automatically

   2019-05-29, Bin Gao:
   * add abstract recurrence relation symbol visitor

   2018-02-08, Bin Gao:
   * first version
*/

#include "tGlueCore/Stringify.hpp"

#include "tIntegral/Symbol/RecurSymbol.hpp"
#include "tIntegral/SymbolVisitor/RecurSymbolVisitor.hpp"

namespace tIntegral
{
    bool RecurNumber::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurNumber::to_string() const noexcept
    {
        return "{number: "+get_name()+"}";
    }

    std::string RecurNumber::get_name() const noexcept
    {
        return std::to_string(m_value);
    }

    bool RecurScalarVar::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurScalarVar::to_string() const noexcept
    {
        auto var_dependency = get_dependency();
        if (var_dependency.empty()) {
            return "{variable: {name: "+get_name()
                +", integrand: "+get_integrand()->to_string()
                +", method: "+get_integrand_name()
                +"}}";
        }
        else {
            return "{variable: {name: "+get_name()
                +", integrand: "+get_integrand()->to_string()
                +", method: "+get_integrand_name()
                +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                +"}}";
        }
    }

    bool RecurCartesianVar::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurCartesianVar::to_string() const noexcept
    {
        auto var_dependency = get_dependency();
        if (var_dependency.empty()) {
            return "{variable: {name: "+get_name()
                +", integrand: "+get_integrand()->to_string()
                +", method: "+get_integrand_name()
                +", direction: "+stringify_direction(m_direction)
                +"}}";
        }
        else {
            return "{variable: {name: "+get_name()
                +", integrand: "+get_integrand()->to_string()
                +", method: "+get_integrand_name()
                +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                +", direction: "+stringify_direction(m_direction)
                +"}}";
        }
    }

    RecurDirection RecurCartesianVar::get_direction() const noexcept
    {
        return m_direction;
    }

    bool RecurScalarVec::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurScalarVec::to_string() const noexcept
    {
        auto var_dependency = get_dependency();
        if (m_indices.empty()) {
            if (var_dependency.empty()) {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +"}}";
            }
            else {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                    +"}}";
            }
        }
        else {
            if (var_dependency.empty()) {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", indices: "+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())
                    +"}}";
            }
            else {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                    +", indices: "+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())
                    +"}}";
            }
        }
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurScalarVec::get_indices() const noexcept
    {
        return m_indices;
    }

    bool RecurCartesianVec::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurCartesianVec::to_string() const noexcept
    {
        auto var_dependency = get_dependency();
        if (m_indices.empty()) {
            if (var_dependency.empty()) {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", direction: "+stringify_direction(m_direction)
                    +"}}";
            }
            else {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                    +", direction: "+stringify_direction(m_direction)
                    +"}}";
            }
        }
        else {
            if (var_dependency.empty()) {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", direction: "+stringify_direction(m_direction)
                    +", indices: "+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())
                    +"}}";
            }
            else {
                return "{variable: {name: "+get_name()
                    +", integrand: "+get_integrand()->to_string()
                    +", method: "+get_integrand_name()
                    +", dependency: "+tGlueCore::stringify(var_dependency.cbegin(), var_dependency.cend())
                    +", direction: "+stringify_direction(m_direction)
                    +", indices: "+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())
                    +"}}";
            }
        }
    }

    RecurDirection RecurCartesianVec::get_direction() const noexcept
    {
        return m_direction;
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurCartesianVec::get_indices() const noexcept
    {
        return m_indices;
    }

    bool RecurIdxOrder::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurIdxOrder::to_string() const noexcept
    {
        return "{index-order: "+m_index->to_string()+'}';
    }

    std::string RecurIdxOrder::get_name() const noexcept
    {
        return m_index->get_name();
    }

    RecurDirection RecurIdxOrder::get_direction() const noexcept
    {
        return m_index->get_direction();
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurIdxOrder::get_indices() const noexcept
    {
        return std::vector<std::shared_ptr<RecurIndex>>({m_index});
    }

    bool RecurTerm::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurTerm::to_string() const noexcept
    {
        return "{order: "+tGlueCore::stringify(m_indices.cbegin(), m_indices.cend())+"}";
    }

    std::string RecurTerm::get_name() const noexcept
    {
        return std::string();
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurTerm::get_indices() const noexcept
    {
        return m_indices;
    }

    bool RecurAddition::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurAddition::to_string() const noexcept
    {
        auto oper_name = get_name();
        if (oper_name.empty()) {
            return "{addition: {augend: "+get_lhs()->to_string()+", addend: "+get_rhs()->to_string()+"}}";
        }
        else {
            return "{sum: {name: "+oper_name
                +", augend: "+get_lhs()->to_string()
                +", addend: "+get_rhs()->to_string()+"}}";
        }
    }

    bool RecurSubtraction::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurSubtraction::to_string() const noexcept
    {
        auto oper_name = get_name();
        auto minuend = get_lhs();
        if (minuend) {
            if (oper_name.empty()) {
                return "{subtraction: {minuend: "+minuend->to_string()
                    +", subtrahend: "+get_rhs()->to_string()+"}}";
            }
            else {
                return "{difference: {name: "+oper_name
                    +", minuend: "+minuend->to_string()
                    +", subtrahend: "+get_rhs()->to_string()+"}}";
            }
        }
        else {
            if (oper_name.empty()) {
                return "{negation: "+get_rhs()->to_string()+"}";
            }
            else {
                return "{negation: {name: "+oper_name+", value: "+get_rhs()->to_string()+"}}";
            }
        }
    }

    /* Get the recurrence-relation direction of the subtraction, which
       is either RecurDirection::XYZ or RecurDirection::X */
    RecurDirection RecurSubtraction::get_direction() const noexcept
    {
        if (get_lhs()) {
            return RecurArithOperation::get_direction();
        }
        else {
            if (get_rhs()->get_direction()==RecurDirection::XYZ) {
                return RecurDirection::XYZ;
            }
            else {
                return RecurDirection::X;
            }
        }
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurSubtraction::get_indices() const noexcept
    {
        /* We require that minuend and subtrahend have the same
           indices, and we allow empty minuend that means a negative
           symbol */
        return get_rhs()->get_indices();
    }

    std::set<std::shared_ptr<RecurSymbol>> RecurSubtraction::get_dependency() const noexcept
    {
        if (get_lhs()) {
            return RecurArithOperation::get_dependency();
        }
        else {
            return get_rhs()->get_dependency();
        }
    }

    bool RecurMultiplication::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurMultiplication::to_string() const noexcept
    {
        auto oper_name = get_name();
        if (oper_name.empty()) {
            return "{multiplication: {multiplier: "+get_lhs()->to_string()
                +", multiplicand: "+get_rhs()->to_string()+"}}";
        }
        else {
            return "{product: {name: "+oper_name
                +", multiplier: "+get_lhs()->to_string()
                +", multiplicand: "+get_rhs()->to_string()+"}}";
        }
    }

    bool RecurDivision::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurDivision::to_string() const noexcept
    {
        auto oper_name = get_name();
        if (oper_name.empty()) {
            return "{division: {dividend: "+get_lhs()->to_string()+", divisor: "+get_rhs()->to_string()+"}}";
        }
        else {
            return "{quotient: {name: "+oper_name
                +", dividend: "+get_lhs()->to_string()
                +", divisor: "+get_rhs()->to_string()+"}}";
        }
    }

    bool RecurParentheses::accept(RecurSymbolVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurParentheses::to_string() const noexcept
    {
        return "{parentheses: "+m_expression->to_string()+"}";
    }

    std::string RecurParentheses::get_name() const noexcept
    {
        return m_name;
    }

    RecurDirection RecurParentheses::get_direction() const noexcept
    {
        return m_expression->get_direction();
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurParentheses::get_indices() const noexcept
    {
        return m_expression->get_indices();
    }

    std::set<std::shared_ptr<RecurSymbol>> RecurParentheses::get_dependency() const noexcept
    {
        return m_expression->get_dependency();
    }
}
