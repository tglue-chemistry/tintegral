/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GBitIntToString().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief converts the GBitInt integer to a string in binary format
    \author Bin Gao
    \date 2014-07-04
    \param[GBitInt:unsigned long]{in} bit_int the GBitInt integer
    \return[GChar:char] the string in binary format
*/
const GChar *GBitIntToString(const GBitInt bit_int)
{
    static GChar bin_str[sizeof(GBitInt)*CHAR_BIT];
    GChar *ptr_str;
    GBitInt bit_one;
    bin_str[0] = '\0';
    ptr_str = bin_str;
    bit_one = 1;
    for (bit_one<<=(sizeof(GBitInt)*CHAR_BIT-1); bit_one>0; bit_one>>=1) {
        *ptr_str++ = (bit_int & bit_one) ? '1' : '0';
    }
    return bin_str;
}
