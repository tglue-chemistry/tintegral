/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function MultExpWrite().

   2014-09-08, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief writes the context of the multinomial expansion
    \author Bin Gao
    \date 2014-09-08
    \param[MultExp:struct]{in} mult_exp context of the multinomial expansion
    \param[FILE]{in} fp_exp file pointer
    \return[GErrorCode:int] error information
*/
GErrorCode MultExpWrite(const MultExp *mult_exp, FILE *fp_exp)
{
    GULong iexp;  /* incremental recorder of terms in the expansion */
    GInt isum;    /* incremental recorder of terms in the sum */
    fprintf(fp_exp,
            "MultExpWrite>> power of the sum %"GINT_FMT"\n",
            mult_exp->sum_power);
    fprintf(fp_exp,
            "MultExpWrite>> number of terms in the sum %"GINT_FMT"\n",
            mult_exp->num_sum_terms);
    fprintf(fp_exp,
            "MultExpWrite>> number of terms in the expansion %lu\n",
            mult_exp->num_exp_terms);
    fprintf(fp_exp, "MultExpWrite>> multinomial-coefficient    powers-of-terms\n");
    for (iexp=0; iexp<mult_exp->num_exp_terms; iexp++) {
        fprintf(fp_exp,
                "MultExpWrite>>          %lu                  ",
                mult_exp->mult_coef[iexp]);
        for (isum=0; isum<mult_exp->num_sum_terms; isum++) {
            fprintf(fp_exp, " %"GINT_FMT"", mult_exp->powers[iexp][isum]);
        }
        fprintf(fp_exp, "\n");
    }
    return GSUCCESS;
}
