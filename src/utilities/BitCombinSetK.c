/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinSetK().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief sets the number k of combinations, and starts the context of the combinations
    \author Bin Gao
    \date 2014-07-03
    \param[BitCombin:struct]{inout} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \param[GInt:int]{in} k_combin the number k
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinSetK(BitCombin *bit_combin, const GInt k_combin)
{
    GErrorCode ierr;  /* error information */
    /* checks if the integers for combinations are allocated */
    if (bit_combin->combin_int==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL integers for combinations");
    }
    /* checks the number k */
    if (k_combin<1) {
        printf("BitCombinSetK>> the number k %"GINT_FMT"\n", k_combin);
        GErrorExit(FILE_AND_LINE, "invalid number");
    }
    bit_combin->k_combin = k_combin;
    /* there will be no combinations if k>n */
    if (bit_combin->k_combin>bit_combin->size_n) {
        bit_combin->num_combin = 0;
    }
    else {
        ierr = BinomCoefficient(bit_combin->size_n,
                                bit_combin->k_combin,
                                &bit_combin->num_combin);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
    }
    /* starts the context of the combinations */
    ierr = BitCombinStart(bit_combin);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinStart");
    return GSUCCESS;
}
