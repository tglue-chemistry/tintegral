/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GIntPow().

   2014-06-29, Bin Gao:
   * first version
*/

#include "utilities/gen1int_arith.h"

/*% \brief pow function for integers using exponentiation by squaring
    \author Bin Gao
    \date 2014-06-29
    \param[GInt:int]{in} x base (integer)
    \param[GInt:int]{in} y exponentiation (integer)
    \param[GInt:int]{out} result x^y
    \return[GErrorCode:int] error information
*/
GErrorCode GIntPow(const GInt x, const GInt y, GInt *result)
{
    GReal real_base;
    GInt exp;
    GReal real_result;
    GErrorCode ierr;
    real_base = x;
    exp = y;
    real_result = 1;
    while (exp) {
        if (exp & 1) real_result *= real_base;
        exp >>= 1;
        real_base *= real_base;
    }
    ierr = GIntRint(real_result, result);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GIntRint");
    return GSUCCESS;
}
