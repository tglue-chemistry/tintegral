/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function SortTagIdxAscending().

   2014-06-23, Bin Gao:
   * first version
*/

#include "utilities/gen1int_sort.h"

/*% \brief sorts given centers in ascending order according to their indices
    \author Bin Gao
    \date 2014-06-23
    \param[GInt:int]{in} num_cent number of centers
    \param[GInt:int]{inout} idx_cent indices of the centers
    \param[GInt:int]{inout} tag_cent tag for the positions of the centers before sorting
*/
GVoid SortTagIdxAscending(const GInt num_cent, GInt *idx_cent, GInt *tag_cent)
{
    GInt idx_insert;  /* inserting index in insertion sort algorithm */
    GInt tag_insert;  /* tag of inserting index in insertion sort algorithm */
    GInt i,j,k;       /* incremental recorders */
    /* sorts the centers' indices using insertion sort algorithm */
    for (i=1; i<num_cent; i++) {
        idx_insert = idx_cent[i];
        tag_insert = tag_cent[i];
        j = i-1;
        /* when an index is less than the previous one, loops down to insert it
           at its right place indicated by \var(j)+1 */
        while ((j>=0) && (idx_insert<idx_cent[j])) {
            k = j;
            idx_cent[++k] = idx_cent[j];
            tag_cent[k] = tag_cent[j];
            j--;
        }
        idx_cent[++j] = idx_insert;
        tag_cent[j] = tag_insert;
    }
}
