/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BinomCoefficient().

   2014-07-06, Bin Gao:
   * moves the converting to integer to another general function GULongRint()

   2014-07-05, Bin Gao:
   * removes the defined maximum allowed order of binomial power, by first
     calculating using real number, then converting to the integer; overflow
     and inexact rounding checks are added

   2014-06-29, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief calculates the binomial coefficient \f$\binom{n}{k}=\frac{n!}{k!(n-k)!}\f$
    \author Bin Gao
    \date 2014-06-29
    \param[GInt:int]{in} n number \f$n\f$
    \param[GInt:int]{in} k number \f$k\f$
    \param[GULong:unsigned long]{out} n_choose_k n choose k
    \return[GErrorCode:int] error information
*/
GErrorCode BinomCoefficient(const GInt n, const GInt k, GULong *n_choose_k)
{
    GReal binom_nk;   /* real number for calculation, avoid overflow for integer */
    GInt n_plus_one;  /* number \f$n+1\f$ */
    GInt inum;        /* incremental recorder */
    GErrorCode ierr;  /* error information */
    if (n<0 || k<0) {
        printf("BinomCoefficient>> n %"GINT_FMT"\n", n);
        printf("BinomCoefficient>> k %"GINT_FMT"\n", k);
        GErrorExit(FILE_AND_LINE, "negative numbers");
    }
    /* some simple cases */
    if (k>n) {
        *n_choose_k = 0;
    }
    else if (k==0 || k==n) {
        *n_choose_k = 1;
    }
    else if (k==1 || k==n-1) {
        *n_choose_k = n;
    }
    /* general case: can also handle k==0 || k==n and k==1 || k==n-1 */
    else {
        binom_nk = 1;
        n_plus_one = n+1;
        if (k<n-k) {
            for (inum=n; inum>n-k; inum--) {
                binom_nk = (binom_nk*inum)/(n_plus_one-inum);
            }
        }
        else {
            for (inum=n; inum>k; inum--) {
                binom_nk = (binom_nk*inum)/(n_plus_one-inum);
            }
        }
        ierr = GULongRint(binom_nk, n_choose_k);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GULongRint");
    }
    return GSUCCESS;
}
