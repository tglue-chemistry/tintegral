/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GULongRint().

   2014-07-06, Bin Gao:
   * first version
*/

/* uses fetestexcept() and FE_INEXACT for detecting floating-point exception */
#include <fenv.h>

#include "utilities/gen1int_arith.h"

/*% \brief rounds a real number to an unsigned long integer
    \author Bin Gao
    \date 2014-06-29
    \param[GReal:real]{in} real_num the real number
    \param[GULong:unsigned long]{out} ul_num the unsigned long integer
    \return[GErrorCode:int] error information
*/
GErrorCode GULongRint(const GReal real_num, GULong *ul_num)
{
    if (real_num>GULONG_MAX || real_num<0) {
        printf("GULongRint>> real number %f\n", real_num);
        printf("GULongRint>> GULONG_MAX %lu\n", GULONG_MAX);
        GErrorExit(FILE_AND_LINE, "overflow happened");
    }
    else {
        *ul_num = lrint(real_num);
        if (fetestexcept(FE_INEXACT)) {
            printf("GULongRint>> real number %f\n", real_num);
            printf("GULongRint>> rounding number %lu\n", *ul_num);
            printf("GULongRint>> FE_INEXACT raised");
            GErrorExit(FILE_AND_LINE, "inexact rounding number");
        }
    }
    return GSUCCESS;
}
