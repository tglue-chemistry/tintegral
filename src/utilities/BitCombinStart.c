/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinRestart().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief starts the context of the combinations by initializing the iterator
        and generating the first combination
    \author Bin Gao
    \date 2014-07-03
    \param[BitCombin:struct]{inout} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinStart(BitCombin *bit_combin)
{
    GInt k_left;  /* number of left elements of the k-combination represented by the integers */
    GInt inum;    /* incremental recorder */
    /* checks if the integers for combinations are allocated */
    if (bit_combin->combin_int==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL integers for combinations");
    }
    /* initializes the rank of current combination */
    bit_combin->rank_combin = 0;
    /* there will be no combinations if k>n */
    if (bit_combin->k_combin>bit_combin->size_n) {
        for (inum=0; inum<bit_combin->num_bit_int; inum++) {
            bit_combin->combin_int[inum] = 0;
        }
    }
    else {
        if (bit_combin->k_combin<bit_combin->size_bit_int) {
            /* the first integer is 0...01...1, where the rightmost
               bit_combin->k_combin bits are 1 */
            bit_combin->combin_int[0] = 1;
            bit_combin->combin_int[0] <<= bit_combin->k_combin;
            --bit_combin->combin_int[0];
            /* other integers are 0...0 */
            for (inum=1; inum<bit_combin->num_bit_int; inum++) {
                bit_combin->combin_int[inum] = 0;
            }
        }
        else if (bit_combin->k_combin==bit_combin->size_bit_int) {
            /* the first integer is 1...1 */
            bit_combin->combin_int[0] = GBITINT_MAX;
            /* others are 0...0 */
            for (inum=1; inum<bit_combin->num_bit_int; inum++) {
                bit_combin->combin_int[inum] = 0;
            }
        }
        /* number of elements in the k-combination is greater than
           the size of an integer */
        else {
            k_left = bit_combin->k_combin;
            inum = 0;
            /* the first few integers are 1...1 */
            do {
                bit_combin->combin_int[inum++] = GBITINT_MAX;
                k_left -= bit_combin->size_bit_int;
            } while (k_left>bit_combin->size_bit_int);
            /* the number of left elements is less than the size of an unsigned
               long integer, this one becomes 0...01...1, the rightmost k_left
               bits are 1 */
            bit_combin->combin_int[inum] = 1;
            bit_combin->combin_int[inum] <<= k_left;
            --bit_combin->combin_int[inum++];
            /* other integers are 0...0 */
            while (inum<bit_combin->num_bit_int) {
                bit_combin->combin_int[inum++] = 0;
            };
        }
    }
    return GSUCCESS;
}
