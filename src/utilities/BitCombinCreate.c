/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinCreate().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief creates the context of combinations represented by integers
    \author Bin Gao
    \date 2014-07-03
    \param[BitCombin:struct]{inout} bit_combin context of the combinations
    \param[GInt:int]{in} size_n the size of a set from which k elements will be chosen
    \param[GInt:int]{in} k_combin the number k
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinCreate(BitCombin *bit_combin,
                           const GInt size_n,
                           const GInt k_combin)
{
    GErrorCode ierr;  /* error information */
    if (size_n<1) {
        printf("BitCombinCreate>> size of a set %"GINT_FMT"\n", size_n);
        GErrorExit(FILE_AND_LINE, "invalid size");
    }
    bit_combin->size_n = size_n;
    /* size of the integer for combinations */
    bit_combin->size_bit_int = sizeof(GBitInt)*CHAR_BIT;
    /* number of integers for the combinations */
    if (bit_combin->size_n<=bit_combin->size_bit_int) {
        bit_combin->num_bit_int = 1;
    }
    else {
        bit_combin->num_bit_int = 0;
        do {
            ++bit_combin->num_bit_int;
            bit_combin->size_n -= bit_combin->size_bit_int;
        } while (bit_combin->size_n>0);
        bit_combin->size_n = size_n;
    }
    /* allocates memory for integers for combinations */
    bit_combin->combin_int = (GBitInt *)malloc(bit_combin->num_bit_int*sizeof(GBitInt));
    if (bit_combin->combin_int==NULL) {
        printf("BitCombinCreate>> number of integers for combinations %"GINT_FMT"\n",
               bit_combin->num_bit_int);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for integers for combinations");
    }
    /* sets the details of combinations */
    ierr = BitCombinSetK(bit_combin, k_combin);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetK");
    return GSUCCESS;
}
