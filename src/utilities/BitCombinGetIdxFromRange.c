/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinGetIdxFromRange().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief gets indices of current combination by giving a minimum index
    \author Bin Gao
    \date 2014-07-04
    \param[BitCombin:struct]{in} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \param[GInt:int]{in} min_idx the minimum index
    \param[GInt:int]{out} combin_idx the generated indices of current combination,
        size is bit_combin->k_combin
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinGetIdxFromRange(const BitCombin *bit_combin,
                                    const GInt min_idx,
                                    GInt *combin_idx)
{
    GBitInt bit_one;        /* temporary variable for checking the bit is 1 or not */
    GInt offset_idx;        /* offset of generated indices  */
    GInt ibit, inum, jnum;  /* incremental recorders */
    GErrorCode ierr;        /* error information */
    /* checks if the integers for combinations are allocated */
    if (bit_combin->combin_int==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL integers for combinations");
    }
    /* loops over all integers for the combinations */
    for (inum=0,jnum=0,offset_idx=min_idx; inum<bit_combin->num_bit_int; inum++) {
        /* loops over all the bits of the integer */
        for (ibit=0,bit_one=1; ibit<bit_combin->size_bit_int; ibit++) {
            /* checks if current bit is 1 */
            if ((bit_combin->combin_int[inum] & bit_one)!=0) {
                combin_idx[jnum++] = ibit+offset_idx;
                /* we get all the bit 1's */
                if (jnum==bit_combin->k_combin) {
                    return GSUCCESS;
                }
            }
            bit_one <<= 1;
        }
        offset_idx += bit_combin->size_bit_int;
    }
    /* we did not get enough bit 1's */
    ierr = BitCombinWrite(bit_combin, stdout);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
    GErrorExit(FILE_AND_LINE, "not enough bit 1's");
}
