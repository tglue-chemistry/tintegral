/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinNext().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief finds the next combination after given steps, by extending the Gosper's hack
        (see for example, http://en.wikipedia.org/wiki/Combinatorial_number_system)
        to manipulate several integers
    \author Bin Gao
    \date 2014-07-03
    \param[BitCombin:struct]{inout} bit_combin context of the combinations, should
        be at least created by BitCombinCreate()
    \param[GULong:unsigned long]{in} num_steps the given steps
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinNext(BitCombin *bit_combin, const GULong num_steps)
{
    GULong rank_new_combin;  /* rank of the combination after moving given steps */
    GBitInt u;               /* temporary variables for bitwise operations */
    GBitInt v;
    GBitInt w;
    GBitInt num_one;         /* number of bit 1's if overflow happened */
    GInt innz;               /* position of the rightmost non-zero integer */
    GULong istep;            /* incremental recorder for moving */
    GInt inum,jnum;          /* incremental recorders */
    GBool got_next;          /* indicates if the next combination got */
    GErrorCode ierr;         /* error information */
    if (num_steps==0) {
        printf("BitCombinNext>> warning! no step to move!\n");
        /**next_combin = (bit_combin->rank_combin+1<bit_combin->num_combin) ? GTRUE : GFALSE;*/
        return GSUCCESS;
    }
    /* checks if given number of steps is OK */
    rank_new_combin = bit_combin->rank_combin+num_steps;
    if (rank_new_combin>=bit_combin->num_combin) {
        printf("BitCombinNext>> number of combinations %lu\n", bit_combin->num_combin);
        printf("BitCombinNext>> rank of current combination %lu\n",
               bit_combin->rank_combin);
        printf("BitCombinNext>> required number of steps to move %lu\n", num_steps);
        GErrorExit(FILE_AND_LINE, "too many steps");
    }
    /* if there are too many steps to move (>BITCOMBIN_MAX_NEXT), we call
       BitCombinSetRank() to get the combination directly using greedy algorithm */
    if (num_steps>BITCOMBIN_MAX_NEXT) {
        ierr = BitCombinSetRank(bit_combin, rank_new_combin);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetRank");
    }
    /* moves several steps to get the combination */
    else {
        bit_combin->rank_combin = rank_new_combin;
        /* there is only one integer for the combination, original Gosper's hack used */
        if (bit_combin->num_bit_int==1) {
            for (istep=0; istep<num_steps; istep++) {
                /* extracts rightmost bit 1 */
                u = bit_combin->combin_int[0] & -bit_combin->combin_int[0];
                /* sets last non-trailing bit 0, and clear to the right; unlike the original
                   Gosper's hack (http://en.wikipedia.org/wiki/Combinatorial_number_system),
                   we do not detect the overflow or the case of bit_combin->combin_int[0]==0
                   by checking if v==0, the number of times calling this function BitCombinNext()
                   is controlled by bit_combin->rank_combin and bit_combin->num_combin */
                v = u + bit_combin->combin_int[0];
                /* adds lost bit 1's (((v^bit_combin->combin_int[0])/u)>>2) to the right */
                bit_combin->combin_int[0] = v + (((v^bit_combin->combin_int[0])/u)>>2);
            }
        }
        /* more than one integer for the combinations */
        else {
            for (istep=0; istep<num_steps; istep++) {
                /* finds the rightmost non-zero integer */
                innz = 0;
                do {
                    if (bit_combin->combin_int[innz]!=0) {
                        break;
                    }
                    innz++;
                } while (innz<bit_combin->num_bit_int);
                /* all the integers are zero */
                if (innz==bit_combin->num_bit_int && bit_combin->combin_int[innz]==0) {
                    ierr = BitCombinWrite(bit_combin, stdout);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
                    GErrorExit(FILE_AND_LINE, "all integers are zero");
                }
                /* extracts rightmost bit 1 of the rightmost non-zero integer */
                w = bit_combin->combin_int[innz];
                u = w & -w;
                /* sets last non-trailing bit 0, and clear to the right: v = u + w,
                   also notice that all bit_combin->combin_int[inum] (inum<innz) are 0 */
                v = u + w;
                /* since both u and bit_combin->combin_int[innz] are not zero,
                   if v is zero, there is overflow */
                if (v==0) {
                    /* calculates how many consecutive 1's in
                       bit_combin->combin_int[inum], where inum>=innz */
                    num_one = 0;
                    while (w) {
                        num_one++;
                        w &= (w-1);
                    }
                    got_next = GFALSE;
                    for (inum=innz+1; inum<bit_combin->num_bit_int; inum++) {
                        if (bit_combin->combin_int[inum]==GBITINT_MAX) {
                            num_one += bit_combin->size_bit_int;
                        }
                        else {
                            /* checks if the rightmost bit of bit_combin->combin_int[inum]
                               is 1 or not */
                            if ((bit_combin->combin_int[inum] & 1)==0) {
                                /* move one bit up of the leftmost 1 in the consecutive 1's */
                                bit_combin->combin_int[inum]++;
                            }
                            else {
                                /* adds 1 to the right of the rightmost 1 */
                                w = bit_combin->combin_int[inum] | (bit_combin->combin_int[inum]-1);
                                /* sets the consecutive 1's to zero, and its left 0 to 1 */
                                w++;
                                /* saves this number (removing consecutive 1's) for later use */
                                u = w;
                                /* sets the consecutive 1's to zero, and changes its left to 0 */
                                w &= bit_combin->combin_int[inum];
                                /* now w contains the consecutive 1's */
                                w = bit_combin->combin_int[inum]-w;
                                while (w) {
                                    num_one++;
                                    w &= (w-1);
                                }
                                /* move one bit up of the leftmost 1 in the consecutive 1's */
                                bit_combin->combin_int[inum] = u;
                            }
                            /* move other 1's to the rightmost */
                            num_one--;
                            jnum = 0;
                            while (num_one>bit_combin->size_bit_int) {
                                bit_combin->combin_int[jnum] = GBITINT_MAX;
                                num_one -= bit_combin->size_bit_int;
                                jnum++;
                            }
                            /* there are some 1's left, we need to put 1...1 to
                               bit_combin->combin_int[jnum] */
                            if (num_one!=0) {
                                w = 1;
                                w <<= num_one;  /* w = 10...0 */
                                w--;            /* w = 01...1 */
                                if (jnum==inum) {
                                    bit_combin->combin_int[jnum] += w;
                                }
                                else {
                                    bit_combin->combin_int[jnum++] = w;
                                }
                            }
                            /* sets other bit_combin->combin_int[jnum]=0 (jnum<inum) */
                            while (jnum<inum) {
                                bit_combin->combin_int[jnum++] = 0;
                            }
                            /* next combination got */
                            got_next = GTRUE;
                            break;
                        }
                    }
                    /* if the code runs into here, means something really wrong */
                    if (got_next==GFALSE) {
                        ierr = BitCombinWrite(bit_combin, stdout);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
                        printf("BitCombinNext>> invoked after no combination left\n");
                        GErrorExit(FILE_AND_LINE, "please report to the author");
                    }
                }
                /* no overflow, we can perform similar procedure as the Gosper's hack */
                else {
                    /* adds the lost bit 1's to the rightmost integer */
                    if (innz==0) {
                        bit_combin->combin_int[0] = v + (((v^bit_combin->combin_int[0])/u)>>2);
                    }
                    else {
                        /* notice that bit_combin->combin_int[inum]=0 (inum<innz) */
                        bit_combin->combin_int[0] = ((v^bit_combin->combin_int[innz])/u)>>2;
                        bit_combin->combin_int[innz] = v;
                    }
                }
            }
        }
        /* checks if there are still some combinations left */
        /**next_combin = (bit_combin->rank_combin+1<bit_combin->num_combin) ? GTRUE : GFALSE;*/
    }
    return GSUCCESS;
}
