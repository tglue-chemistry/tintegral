/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function MultExpGetAllTerms().

   2014-09-16, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief gets all expansion terms of the multinomial expansion
    \author Bin Gao
    \date 2014-09-16
    \param[MultExp:struct]{in} mult_exp context of the multinomial expansion
    \param[GULong:unsigned long]{out} mult_coef multinomial coefficients
    \param[GInt:int]{out} powers powers of terms
    \return[GErrorCode:int] error information
*/
GErrorCode MultExpGetAllTerms(const MultExp *mult_exp,
                              GULong *mult_coef,
                              GInt *powers)
{
    GULong iexp;  /* incremental recorder of terms in the expansion */
    GInt isum;    /* incremental recorder of terms in the sum */
    for (iexp=0; iexp<mult_exp->num_exp_terms; iexp++) {
        mult_coef[iexp] = mult_exp->mult_coef[iexp];
        for (isum=0; isum<mult_exp->num_sum_terms; isum++) {
            powers[iexp*mult_exp->num_sum_terms+isum] = mult_exp->powers[iexp][isum];
        }
    }
    return GSUCCESS;
}
