/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function RankCombinZeroBasedIdx().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief ranks a given k-combination (zero-based indexing),
        see http://en.wikipedia.org/wiki/Combinatorial_number_system
    \author Bin Gao
    \date 2014-07-03
    \param[GInt:int]{in} k the number k
    \param[GInt:int]{in} combin the given combination, should be sorted
        in ascending order of the set {0,1,...}
    \param[GULong:unsigned long]{out} rank the rank of the given k-combination
    \return[GErrorCode:int] error information
*/
GErrorCode RankCombinZeroBasedIdx(const GInt k, const GInt *combin, GULong *rank)
{
    GULong binom_k;   /* \binom{combin_{i}}{i} */
    GInt inum;        /* incremental recorder */
    GErrorCode ierr;  /* error information */
    *rank = 0;
    for (inum=0; inum<k; inum++) {
        ierr = BinomCoefficient(combin[inum], inum+1, &binom_k);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
        *rank += binom_k;
    }
    return GSUCCESS;
}
