/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinCheckRank().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief checks the rank of current combination, used only for test
    \author Bin Gao
    \date 2014-07-04
    \param[BitCombin:struct]{in} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinCheckRank(const BitCombin *bit_combin)
{
    GULong rank_combin;     /* computed rank of current combination */
    GULong binom_k;         /* \binom{combin_{i}}{i} */
    GBitInt bit_one;        /* temporary variable for checking the bit is 1 or not */
    GInt offset_idx;        /* offset of generated indices  */
    GInt ibit, inum, jnum;  /* incremental recorders */
    GErrorCode ierr;        /* error information */
    rank_combin = 0;
    /* checks if the integers for combinations are allocated */
    if (bit_combin->combin_int==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL integers for combinations");
    }
    /* loops over all integers for the combinations */
    for (inum=0,jnum=1,offset_idx=0; inum<bit_combin->num_bit_int; inum++) {
        /* loops over all the bits of the integer */
        for (ibit=0,bit_one=1; ibit<bit_combin->size_bit_int; ibit++) {
            /* checks if current bit is 1 */
            if ((bit_combin->combin_int[inum] & bit_one)!=0) {
                /* computes the rank, see http://en.wikipedia.org/wiki/Combinatorial_number_system */
                ierr = BinomCoefficient(ibit+offset_idx, jnum, &binom_k);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
                rank_combin += binom_k;
                ++jnum;
                /* we get all the bit 1's */
                if (jnum>bit_combin->k_combin) {
                    /* checks the computed rank */
                    if (rank_combin!=bit_combin->rank_combin) {
                        ierr = BitCombinWrite(bit_combin, stdout);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
                        printf("BitCombinCheckRank>> computed rank %lu\n", rank_combin);
                        GErrorExit(FILE_AND_LINE, "incorrect computed rank");
                    }
                    else {
                        return GSUCCESS;
                    }
                }
            }
            bit_one <<= 1;
        }
        offset_idx += bit_combin->size_bit_int;
    }
    /* we did not get enough bit 1's */
    ierr = BitCombinWrite(bit_combin, stdout);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
    GErrorExit(FILE_AND_LINE, "not enough bit 1's");
}
