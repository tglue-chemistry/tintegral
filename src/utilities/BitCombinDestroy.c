/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinDestroy().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief destroys the context of combinations
    \author Bin Gao
    \date 2014-07-04
    \param[BitCombin:struct]{inout} bit_combin context of the combinations
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinDestroy(BitCombin *bit_combin)
{
    bit_combin->size_n = 0;
    bit_combin->k_combin = 0;
    bit_combin->num_combin = 0;
    bit_combin->rank_combin = 0;
    bit_combin->size_bit_int = 0;
    bit_combin->num_bit_int = 0;
    if (bit_combin->combin_int!=NULL) {
        free(bit_combin->combin_int);
        bit_combin->combin_int = NULL;
    }
    return GSUCCESS;
}
