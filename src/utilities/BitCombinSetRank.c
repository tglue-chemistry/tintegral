/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinSetRank().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief sets the k-combination to a given rank by using greedy algorithm,
        see, for example http://en.wikipedia.org/wiki/Combinatorial_number_system
    \author Bin Gao
    \date 2014-07-04
    \param[BitCombin:struct]{inout} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \param[GULong:unsigned long]{in} rank_combin the given rank of the k-combination
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinSetRank(BitCombin *bit_combin, const GULong rank_combin)
{
#if defined(NO_TABLE_BINOM)
#define TAB_MAX_K_COMBIN 0
#else
#include "utilities/gen1int_table_binom.h"
#endif
    GInt size_n;       /* the size of the set */
    GBitInt bit_one;   /* temporary variable for bitwise operations */
    GULong curr_rank;  /* used in greedy algorithm, recording the updated rank */
    GInt knum, ibit;   /* incremental recorders for the number k and bit */
    GULong binom_k;    /* binomial coefficient */
    GInt inum;         /* incremental recorder over integers */
    GErrorCode ierr;   /* error information */
    /* checks if the integers for combinations are allocated */
    if (bit_combin->combin_int==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL integers for combinations");
    }
    /* checks the rank */
    if (rank_combin<0 || rank_combin>=bit_combin->num_combin) {
        ierr = BitCombinWrite(bit_combin, stdout);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
        printf("BitCombinSetRank>> given rank %lu\n", rank_combin);
        GErrorExit(FILE_AND_LINE, "incorrect given rank");
    }
    /* if it is the first combination */
    if (rank_combin==0) {
        ierr = BitCombinStart(bit_combin);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinStart");
    }
    /* the last combination */
    else if (rank_combin==bit_combin->num_combin-1) {
        bit_combin->rank_combin = rank_combin;
        /* initializes the integers for the combinations */
        for (inum=0; inum<bit_combin->num_bit_int; inum++) {
            bit_combin->combin_int[inum] = 0;
        }
        /* finds the integer in which the bits 1...1 to put */
        size_n = bit_combin->size_n;
        inum = 0;
        while (size_n>bit_combin->size_bit_int) {
            size_n -= bit_combin->size_bit_int;
            ++inum;
        }
        /* only puts 1...1 into this integer, from the size_n-th bit to the right */
        if (bit_combin->k_combin<size_n) {
            bit_combin->combin_int[inum] = 1;
            bit_combin->combin_int[inum] <<= bit_combin->k_combin;
            --bit_combin->combin_int[inum];
            bit_combin->combin_int[inum] <<= size_n-bit_combin->k_combin;
        }
        else {
            bit_combin->combin_int[inum] = 1;
            bit_combin->combin_int[inum] <<= size_n;
            --bit_combin->combin_int[inum];
            /* we also need to put bit_combin->k_combin-size_n 1's to other integers */
            size_n = bit_combin->k_combin-size_n;
            while (size_n>bit_combin->size_bit_int) {
                bit_combin->combin_int[--inum] = GBITINT_MAX;
                size_n -= bit_combin->size_bit_int;
            }
            if (size_n!=0) {
                bit_combin->combin_int[--inum] = 1;
                bit_combin->combin_int[inum] <<= size_n;
                --bit_combin->combin_int[inum];
                bit_combin->combin_int[inum] <<= bit_combin->size_bit_int-size_n;
            }

        }
    }
    /* gets the combination (neither the first nor the last) using greedy algorithm */
    else {
        bit_combin->rank_combin = rank_combin;
        /* initializes the integers for the combinations */
        for (inum=0; inum<bit_combin->num_bit_int; inum++) {
            bit_combin->combin_int[inum] = 0;
        }
        /* starts the greedy algorithm */
        curr_rank = rank_combin;
        /* the number k (k-combination) is greater than those in the table */
        for (knum=bit_combin->k_combin; knum>TAB_MAX_K_COMBIN; knum--) {
            for (ibit=knum-1; ibit<=bit_combin->size_n; ibit++) {
                ierr = BinomCoefficient(ibit, knum, &binom_k);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
                if (binom_k==curr_rank) {
                    curr_rank = 0;
                    /* sets ibit-th bit as 1 */
                    inum = 0;
                    while (ibit>=bit_combin->size_bit_int) {
                        ibit -= bit_combin->size_bit_int;
                        ++inum;
                    }
                    bit_one = 1;
                    bit_combin->combin_int[inum] |= bit_one<<ibit;
                    break;
                }
                else if (binom_k>curr_rank) {
                    /* \binom{ibit-1}{knum} = \binom{ibit}{knum}*(ibit-knum)/ibit */
                    curr_rank -= binom_k*(ibit-knum)/ibit;  /* important to do multiplication first */
                    --ibit;
                    /* sets ibit-th bit as 1 */
                    inum = 0;
                    while (ibit>=bit_combin->size_bit_int) {
                        ibit -= bit_combin->size_bit_int;
                        ++inum;
                    }
                    bit_one = 1;
                    bit_combin->combin_int[inum] |= bit_one<<ibit;
                    break;
                }
            }
        }
#if !defined(NO_TABLE_BINOM)
        /* looks up the table */
        for (knum=GMin(bit_combin->k_combin,TAB_MAX_K_COMBIN); knum>0; knum--) {
            for (ibit=knum-1; ibit<TAB_MAX_SIZE_N; ibit++) {
                inum = knum-1;
                binom_k = TABLE_BINOM[inum][ibit];
                if (binom_k==curr_rank) {
                    curr_rank = 0;
                    /* sets ibit-th bit as 1 */
                    inum = 0;
                    while (ibit>=bit_combin->size_bit_int) {
                        ibit -= bit_combin->size_bit_int;
                        ++inum;
                    }
                    bit_one = 1;
                    bit_combin->combin_int[inum] |= bit_one<<ibit;
                    break;
                }
                else if (binom_k>curr_rank) {
                    curr_rank -= TABLE_BINOM[inum][--ibit];
                    /* sets ibit-th bit as 1 */
                    inum = 0;
                    while (ibit>=bit_combin->size_bit_int) {
                        ibit -= bit_combin->size_bit_int;
                        ++inum;
                    }
                    bit_one = 1;
                    bit_combin->combin_int[inum] |= bit_one<<ibit;
                    break;
                }
            }
            /* we did not find the required binomial coefficient in the table */
            if (ibit==TAB_MAX_SIZE_N) {
                for (ibit=TAB_MAX_SIZE_N; ibit<=bit_combin->size_n; ibit++) {
                    ierr = BinomCoefficient(ibit, knum, &binom_k);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
                    if (binom_k==curr_rank) {
                        curr_rank = 0;
                        /* sets ibit-th bit as 1 */
                        inum = 0;
                        while (ibit>=bit_combin->size_bit_int) {
                            ibit -= bit_combin->size_bit_int;
                            ++inum;
                        }
                        bit_one = 1;
                        bit_combin->combin_int[inum] |= bit_one<<ibit;
                        break;
                    }
                    else if (binom_k>curr_rank) {
                        /* \binom{ibit-1}{knum} = \binom{ibit}{knum}*(ibit-knum)/ibit */
                        curr_rank -= binom_k*(ibit-knum)/ibit;
                        --ibit;
                        /* sets ibit-th bit as 1 */
                        inum = 0;
                        while (ibit>=bit_combin->size_bit_int) {
                            ibit -= bit_combin->size_bit_int;
                            ++inum;
                        }
                        bit_one = 1;
                        bit_combin->combin_int[inum] |= bit_one<<ibit;
                        break;
                    }
                }
            }
        }
#endif
    }
    return GSUCCESS;
}
