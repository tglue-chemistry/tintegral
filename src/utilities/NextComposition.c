/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NextComposition().

   2014-09-07, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief generates all the compositions of multinomial expansion
    \author Bin Gao
    \date 2014-09-07
    \param[MultExp:struct]{inout} mult_exp context of the multinomial expansion
    \param[GInt:int]{in} sum_power power of the sum
    \param[GInt:int]{in} num_sum_terms number of terms in the sum
    \param[GInt:int]{in} num_exp_terms number of terms in the expansion
    \param[GULong:unsigned long]{out} mult_coef multinomial coefficients
    \param[GInt:int]{out} term_powers powers of terms in the expansion
*/
GVoid NextComposition(const GInt sum_power,
                      const GInt num_sum_terms,
                      const GULong num_exp_terms,
                      GULong *mult_coef,
                      GInt *term_powers)
{
    GInt nnz_power;    /* the first nonzero part in the composition */
    GInt idx_nnz;      /* the index of the first nonzero part */
    GULong iexp,jexp;  /* incremental recorders of terms in the expansion */
    GInt isum,jsum;    /* incremental recorders of terms in the sum */
    /* the first expansion term */
    term_powers[0] = sum_power;
    for (isum=1; isum<num_sum_terms; isum++) {
        term_powers[isum] = 0;
    }
    mult_coef[0] = 1;
    nnz_power = sum_power;
    idx_nnz = -1;
    /* sets the left multinomial coefficients and powers of terms */
    for (iexp=1,jexp=0; iexp<num_exp_terms; iexp++,jexp++) {
        /* generates the next composition of the power of the sum, modified based
           on "Combinatorial Algorithms For Computers and Calculators", Second Edition,
           by Albert Nijenhuis and Herbert S. Wilf, pp. 46-51 */
        if (nnz_power>1) idx_nnz = -1;
        idx_nnz++;
        nnz_power = term_powers[jexp*num_sum_terms+idx_nnz];
        for (isum=1; isum<idx_nnz; isum++) {
            term_powers[iexp*num_sum_terms+isum] = term_powers[jexp*num_sum_terms+isum];
        }
        term_powers[iexp*num_sum_terms+idx_nnz] = 0;
        term_powers[iexp*num_sum_terms] = nnz_power-1;
        isum = idx_nnz+1;
        term_powers[iexp*num_sum_terms+isum] = term_powers[jexp*num_sum_terms+isum]+1;
        for (jsum=isum+1; jsum<num_sum_terms; jsum++) {
            term_powers[iexp*num_sum_terms+jsum] = term_powers[jexp*num_sum_terms+jsum];
        }
        /* computes the multinomial coefficient as
           mult_coef[iexp] = mult_coef[jexp]
                           * term_powers[jexp*num_sum_terms]!
                           * term_powers[jexp*num_sum_terms+idx_nnz]
                           / (term_powers[jexp*num_sum_terms+idx_nnz+1]+1) */
        mult_coef[iexp] = mult_coef[jexp];
        if (idx_nnz==0) {
            mult_coef[iexp] *= term_powers[jexp*num_sum_terms];
            mult_coef[iexp] /= term_powers[iexp*num_sum_terms+1];
        }
        else {
            for (jsum=1; jsum<=term_powers[jexp*num_sum_terms]; jsum++) {
                mult_coef[iexp] *= jsum;
            }
            mult_coef[iexp] *= term_powers[jexp*num_sum_terms+idx_nnz];
            mult_coef[iexp] /= term_powers[iexp*num_sum_terms+isum];
        }
    }
}
