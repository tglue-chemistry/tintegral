/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function MultExpDestroy().

   2014-09-08, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief destroys the context of the multinomial expansion
    \author Bin Gao
    \date 2014-09-08
    \param[MultExp:struct]{inout} mult_exp context of the multinomial expansion
    \return[GErrorCode:int] error information
*/
GErrorCode MultExpDestroy(MultExp *mult_exp)
{
    GULong iexp;  /* incremental recorder of terms in the expansion */
    free(mult_exp->powers[0]);
    for (iexp=0; iexp<mult_exp->num_exp_terms; iexp++) {
        mult_exp->powers[iexp] = NULL;
    }
    free(mult_exp->powers);
    mult_exp->powers = NULL;
    free(mult_exp->mult_coef);
    mult_exp->mult_coef = NULL;
    mult_exp->num_exp_terms = 0;
    mult_exp->num_sum_terms = 0;
    mult_exp->sum_power = 0;
    return GSUCCESS;
}
