/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function MultExpCreate().

   2014-09-07, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief creates the context of multinomial expansion
    \author Bin Gao
    \date 2014-09-07
    \param[MultExp:struct]{inout} mult_exp context of the multinomial expansion
    \param[GInt:int]{in} sum_power power of the sum
    \param[GInt:int]{in} num_sum_terms number of terms in the sum
    \return[GErrorCode:int] error information
*/
GErrorCode MultExpCreate(MultExp *mult_exp,
                         const GInt sum_power,
                         const GInt num_sum_terms)
{
    GULong iexp;      /* incremental recorder of terms in the expansion */
    GErrorCode ierr;  /* error information */
    if (sum_power<0) {
        printf("MultExpCreate>> power of the sum %"GINT_FMT"\n", sum_power);
        GErrorExit(FILE_AND_LINE, "invalid power");
    }
    if (num_sum_terms<1) {
        printf("MultExpCreate>> number of terms in the sum %"GINT_FMT"\n",
               num_sum_terms);
        GErrorExit(FILE_AND_LINE, "invalid number of terms");
    }
    mult_exp->sum_power = sum_power;
    mult_exp->num_sum_terms = num_sum_terms;
    /* calculates the number of terms in the expansion */
    ierr = BinomCoefficient(mult_exp->sum_power+mult_exp->num_sum_terms-1,
                            mult_exp->sum_power,
                            &mult_exp->num_exp_terms);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
    /* allocates memory for the multinomial coefficients */
    mult_exp->mult_coef = (GULong *)malloc(mult_exp->num_exp_terms*sizeof(GULong));
    if (mult_exp->mult_coef==NULL) {
        printf("MultExpCreate>> number of terms in the expansion %lu\n",
               mult_exp->num_exp_terms);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for multinomial coefficients");
    }
    /* allocates memory for the powers of terms in the expansion */
    mult_exp->powers = (GInt **)malloc(mult_exp->num_exp_terms*sizeof(GInt *));
    if (mult_exp->powers==NULL) {
        printf("MultExpCreate>> number of terms in the expansion %lu\n",
               mult_exp->num_exp_terms);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp->powers");
    }
    /* allocates memory for the two-dimensional array at one time */
    mult_exp->powers[0] = (GInt *)malloc(mult_exp->num_exp_terms*mult_exp->num_sum_terms*sizeof(GInt));
    if (mult_exp->powers[0]==NULL) {
        printf("MultExpCreate>> number of terms in the expansion %lu\n",
               mult_exp->num_exp_terms);
        printf("MultExpCreate>> number of terms in the sum %"GINT_FMT"\n",
               mult_exp->num_sum_terms);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp->powers[0]");
    }
    /* make the pointer of each expansion term point to the correct memory */
    for (iexp=0; iexp<mult_exp->num_exp_terms-1; iexp++) {
        mult_exp->powers[iexp+1] = mult_exp->powers[iexp]+mult_exp->num_sum_terms;
    }
    /* generates all the compositions of the multinomial expansion */
    NextComposition(mult_exp->sum_power,
                    mult_exp->num_sum_terms,
                    mult_exp->num_exp_terms,
                    mult_exp->mult_coef,
                    mult_exp->powers[0]);
    return GSUCCESS;
}
