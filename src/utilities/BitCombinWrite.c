/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinWrite().

   2014-07-04, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief writes the context of combinations
    \author Bin Gao
    \date 2014-07-04
    \param[BitCombin:struct]{in} bit_combin context of the combinations
    \param[FILE]{in} fp_bc file pointer
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinWrite(const BitCombin *bit_combin, FILE *fp_bc)
{
    GInt inum;
    if (fp_bc==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL file pointer");
    }
    fprintf(fp_bc, "BitCombinWrite>> size of a set %"GINT_FMT"\n", bit_combin->size_n);
    fprintf(fp_bc, "BitCombinWrite>> number k %"GINT_FMT"\n", bit_combin->k_combin);
    fprintf(fp_bc,
            "BitCombinWrite>> number of combinations %lu\n",
            bit_combin->num_combin);
    fprintf(fp_bc,
            "BitCombinWrite>> rank of current combination %lu\n",
            bit_combin->rank_combin);
    fprintf(fp_bc,
            "BitCombinWrite>> size of integer for combinations %"GINT_FMT"\n",
            bit_combin->size_bit_int);
    fprintf(fp_bc,
            "BitCombinWrite>> number of integers for combinations %"GINT_FMT"\n",
            bit_combin->num_bit_int);
    if (bit_combin->combin_int!=NULL) {
        for (inum=bit_combin->num_bit_int-1; inum>=0; inum--) {
            fprintf(fp_bc,
                    "BitCombinWrite>> %s\n",
                    GBitIntToString(bit_combin->combin_int[inum]));
        }
    }
    return GSUCCESS;
}
