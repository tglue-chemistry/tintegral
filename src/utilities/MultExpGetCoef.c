/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function MultExpGetCoef().

   2014-09-16, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief gets coefficient of an expansion term of the multinomial expansion
    \author Bin Gao
    \date 2014-09-16
    \param[MultExp:struct]{in} mult_exp context of the multinomial expansion
    \param[GULong:unsigned long]{in} idx_exp_term index of the expansion term
    \param[GULong:unsigned long]{out} mult_coef multinomial coefficient of
        the expansion term
    \return[GErrorCode:int] error information
*/
GErrorCode MultExpGetCoef(const MultExp *mult_exp,
                          const GULong idx_exp_term,
                          GULong *mult_coef)
{
    if (idx_exp_term>=mult_exp->num_exp_terms) {
        printf("MultExpGetCoef>> index of the expansion term %lu\n",
               idx_exp_term);
        printf("MultExpGetCoef>> number of terms in the expansion %lu\n",
               mult_exp->num_exp_terms);
        GErrorExit(FILE_AND_LINE, "invalid index of the expansion term");
    }
    *mult_coef = mult_exp->mult_coef[idx_exp_term];
    return GSUCCESS;
}
