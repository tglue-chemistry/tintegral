/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinDuplicate().

   2014-07-06, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief duplicates the context of combinations
    \author Bin Gao
    \date 2014-07-06
    \param[BitCombin:struct]{in} bit_combin context of the combinations
    \param[BitCombin:struct]{out} dup_combin the duplication, should not
        be created before, or be destroyed by BitCombinDestroy()
    \return[GErrorCode:int] error information
*/
GErrorCode BitCombinDuplicate(const BitCombin *bit_combin, BitCombin *dup_combin)
{
    GInt inum;
    dup_combin->size_n = bit_combin->size_n;
    dup_combin->k_combin = bit_combin->k_combin;
    dup_combin->num_combin = bit_combin->num_combin;
    dup_combin->rank_combin = bit_combin->rank_combin;
    dup_combin->size_bit_int = bit_combin->size_bit_int;
    dup_combin->num_bit_int = bit_combin->num_bit_int;
    if (bit_combin->combin_int!=NULL) {
        dup_combin->combin_int = (GBitInt *)malloc(dup_combin->num_bit_int*sizeof(GBitInt));
        if (dup_combin->combin_int==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_combin->combin_int");
        }
        for (inum=0; inum<dup_combin->num_bit_int; inum++) {
            dup_combin->combin_int[inum] = bit_combin->combin_int[inum];
        }
    }
    else {
        dup_combin->combin_int = NULL;
    }
    return GSUCCESS;
}
