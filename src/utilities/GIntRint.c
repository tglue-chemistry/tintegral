/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GIntRint().

   2014-07-06, Bin Gao:
   * first version
*/

/* uses fetestexcept() and FE_INEXACT for detecting floating-point exception */
#include <fenv.h>

#include "utilities/gen1int_arith.h"

/*% \brief rounds a real number to an integer
    \author Bin Gao
    \date 2014-06-29
    \param[GReal:real]{in} real_num the real number
    \param[GInt:int]{out} int_num the integer
    \return[GErrorCode:int] error information
*/
GErrorCode GIntRint(const GReal real_num, GInt *int_num)
{
    if (real_num>GINT_MAX || real_num<GINT_MIN) {
        printf("GIntRint>> real number %f\n", real_num);
        printf("GIntRint>> GINT_MAX %"GINT_FMT"\n", GINT_MAX);
        printf("GIntRint>> GINT_MIN %"GINT_FMT"\n", GINT_MIN);
        GErrorExit(FILE_AND_LINE, "overflow happened");
    }
    else {
        *int_num = rint(real_num);
        if (fetestexcept(FE_INEXACT)) {
            printf("GIntRint>> real number %f\n", real_num);
            printf("GIntRint>> rounding number %"GINT_FMT"\n", *int_num);
            printf("GIntRint>> FE_INEXACT raised");
            GErrorExit(FILE_AND_LINE, "inexact rounding number");
        }
    }
    return GSUCCESS;
}
