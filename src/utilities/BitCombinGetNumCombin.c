/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function BitCombinGetNumCombin().

   2014-07-03, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief gets the number of combinations, \binom{n}{k}
    \author Bin Gao
    \date 2014-07-03
    \param[BitCombin:struct]{in} bit_combin context of the combinations,
        should be at least created by BitCombinCreate()
    \return[GULong:unsigned long] the number of combinations
*/
GULong BitCombinGetNumCombin(const BitCombin *bit_combin)
{
    return bit_combin->num_combin;
}
