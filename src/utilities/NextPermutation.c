/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NextPermutation().

   2014-06-29, Bin Gao:
   * first version
*/

#include "utilities/gen1int_combin.h"

/*% \brief generates the next permutation for a given integer sequence, modified
        from function \fn(next_permutation) in C++ library, see for example,
        http://marknelson.us/2002/03/01/next-permutation/
    \author Bin Gao
    \date 2014-06-29
    \param[GInt:int]{inout} first points to the first element in the sequence
    \param[GInt:int]{inout} last points one past the last element
    \return[GBool:enum] GTRUE when there are some permutations left, GFALSE otherwise
*/
GBool NextPermutation(GInt *first, GInt *last)
{
    GInt *idx, *jdx, *kdx;  /* iterators */
    GInt tmp_num;           /* temporary number for swapping */
    if (first==last) {
        return GFALSE;
    }
    idx = first;
    if (++idx==last) {
        idx = NULL;
        return GFALSE;
    }
    idx = last;
    --idx;
    for (;;) {
        jdx = idx--;
        if (*idx<*jdx) {
            kdx = last;
            while (!(*idx<*--kdx));
            /* exchanges numbers pointed by \var(idx) and \var(kdx) */
            tmp_num = *idx;
            *idx = *kdx;
            *kdx = tmp_num;
            idx = NULL;
            /* reverses the order of numbers in the range of [\var(jdx),\var(last)) */
            kdx = last;
            --kdx;
            for (; jdx<kdx; jdx++,kdx--) {
                tmp_num = *jdx;
                *jdx = *kdx;
                *kdx = tmp_num;
            }
            jdx = NULL;
            kdx = NULL;
            return GTRUE;
        }
        if (idx==first) {
            /* reverses the order of numbers in the range of [\var(first),\var(last)) */
            jdx = last;
            --jdx;
            for (; idx<jdx; idx++,jdx--) {
                tmp_num = *idx;
                *idx = *jdx;
                *jdx = tmp_num;
            }
            idx = NULL;
            jdx = NULL;
            return GFALSE;
        }
    }
}
