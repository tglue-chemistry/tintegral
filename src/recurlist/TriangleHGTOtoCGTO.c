/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleHGTOtoCGTO().

   2014-11-20, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleHGTOtoCGTO(TriangleRecur *triangle_recur,
                              const GInt which_recur,
                              const GReal exponent_ket)
{
    RecurDAG *cur_relation;  /* current recurrence relation */
    RecurVertex *cur_node;          /* current triangle node */
    GReal neg_ratio_braket;          /* negative ratio between exponents of bra and ket centers */
    GInt idx_geo_bra;                /* index of HGTOs/geometric derivatives on the bra center */
    GInt idx_geo_ket;                /* index of HGTOs/geometric derivatives on the ket center */
    GInt iorder;                     /* incremental recorder over orders */
    GInt inode;                      /* incremental recorder over nodes */
    GReal *ptr_LHS;                  /* pointer to the value of LHS */
    GReal *ptr_RHS;                  /* pointer to the value of RHS */
    GInt order_bra_RHS;              /* order of HGTOs (bra) of RHS */
    GInt size_yz_bra_RHS;            /* size of [y...y, z...z] of HGTOs (bra) of RHS */
    GInt size_bra_LHS;               /* size of HGTOs (bra) of LHS */
    GInt size_bra_RHS;               /* size of HGTOs (bra) of RHS */
    GInt size_ket_RHS;               /* size of HGTOs (ket) of RHS */
    GInt order_yz_bra_RHS;           /* order of yz components of HGTOs (bra) of RHS */
    GInt order_z_bra_RHS;            /* order of z component of HGTOs (bra) of RHS */
    GInt inc_bra_RHS;                /* incremental recorder over values of HGTOs (bra) of RHS */
    GInt inc_ket_RHS;                /* incremental recorder over values of HGTOs (ket) of RHS */

    /* points to the recurrence relation */
    if (which_recur>=triangle_recur->num_recur || which_recur<0) {
        printf("TriangleHGTOtoCGTO>> given recurrence relation %"GINT_FMT"\n",
               which_recur);
        printf("TriangleHGTOtoCGTO>> number of added recurrence relations %"GINT_FMT"\n",
               triangle_recur->num_recur);
        GErrorExit(FILE_AND_LINE, "invalid given recurrence relation");
    }
    cur_relation = &triangle_recur->relations[which_recur];

    /* 1st and high orders output index */
    if (cur_relation->num_output_orders>1) {
        /* computes the negative ratio between exponents of bra and ket centers */
        neg_ratio_braket = -exponent_bra/exponent_ket;
        /* gets the indices of HGTOs/geometric derivatives on the bra and ket centers */
        idx_geo_bra = triangle_recur->num_indices-2;
        idx_geo_ket = triangle_recur->num_indices-1;
        /* loops over orders of output index */
        for (iorder=1; iorder<cur_relation->num_output_orders; iorder++) {
            /* loops over different nodes of the current order of output index */
            cur_node = cur_relation->head_vtx[iorder]->suc_LHS;
            for (inode=0; inode<cur_relation->num_vtx[iorder]; inode++) {
                /* gets the orders and sizes of HGTOs of the RHS and LHS terms */
                order_bra_RHS = cur_node->idx_orders[idx_geo_bra]+1;
                size_yz_bra_RHS = order_bra_RHS+1;
                size_bra_LHS = order_bra_RHS*size_yz_bra_RHS/2;
                size_bra_RHS = size_bra_LHS+size_yz_bra_RHS;
                size_ket_RHS = cur_node->idx_orders[idx_geo_ket]
                             * (cur_node->idx_orders[idx_geo_ket]+1)/2;
                /* # recurrence relation along the x direction (ket)
                     [xx...x, xz...z] <= [x...x, z...z] */
                ptr_LHS = &triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk];
                ptr_RHS = &triangle_recur->work_array[cur_node->RHS_terms[0]->idx_wrk][cur_node->RHS_terms[0]->addr_wrk];
                for (inc_ket_RHS=0; inc_ket_RHS<size_ket_RHS; inc_ket_RHS++) {
                    /* ## recurrence relation along the x direction (bra) */
                    for (inc_bra_RHS=0; inc_bra_RHS<size_bra_LHS; inc_bra_RHS++) {
                        *ptr_LHS = neg_ratio_braket*(*ptr_RHS);
                        ptr_LHS++;
                        ptr_RHS++;
                    }
                    ptr_RHS += size_yz_bra_RHS;  /* skips [y...y, z...z] (bra) */
                }
                /* # recurrence relation along the y direction (ket)
                     [yy...y, yz...z] <= [y...y, z...z] */
                ptr_RHS -= cur_node->idx_orders[idx_geo_ket]*size_bra_RHS;
                for (inc_ket_RHS=0; inc_ket_RHS<cur_node->idx_orders[idx_geo_ket]; inc_ket_RHS++) {
                    /* ## recurrence relation along the y direction (bra) */
                    ptr_RHS++;  /* skips x...x (bra) */
                    for (order_yz_bra_RHS=1; order_yz_bra_RHS<=order_bra_RHS; order_yz_bra_RHS++) {
                        for (order_z_bra_RHS=0; order_z_bra_RHS<order_yz_bra_RHS; order_z_bra_RHS++) {
                            *ptr_LHS = neg_ratio_braket*(*ptr_RHS);
                            ptr_LHS++;
                            ptr_RHS++;
                        }
                        ptr_RHS++;  /* skips x...xz...z (bra) */
                    }
                }
                /* # recurrence relation along the z direction (ket)
                     [zz...z] <= [z...z] */
                ptr_RHS -= size_bra_RHS-1;  /* -1 skips x...x (bra) */
                /* ## recurrence relation along the z direction (bra) */
                for (order_yz_bra_RHS=1; order_yz_bra_RHS<=order_bra_RHS; order_yz_bra_RHS++) {
                    ptr_RHS++;  /* skips x...xy...z (bra) */
                    for (order_z_bra_RHS=1; order_z_bra_RHS<=order_yz_bra_RHS; order_z_bra_RHS++) {
                        *ptr_LHS = neg_ratio_braket*(*ptr_RHS);
                        ptr_LHS++;
                        ptr_RHS++;
                    }
                }
                /* moves to the next node */
                cur_node = cur_node->suc_LHS;
            }
        }
    }
    return GSUCCESS;
}
