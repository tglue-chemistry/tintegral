/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurCreate().

   2014-10-19, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleRecurCreate(TriangleRecur *triangle_recur,
                               const GInt num_indices,
                               const RecurIdxType *idx_types,
                               const GInt *idx_orders)
{
    GInt irec;  /* incremental recorder */
    triangle_recur->num_wrk_array = 2;
    triangle_recur->size_wrk_array = 1;
    triangle_recur->num_indices = num_indices;
    triangle_recur->num_recur = num_indices;
    triangle_recur->cur_recur = 0;
    /* sets the types and orders of indices of recurrence relations */
    triangle_recur->idx_types = (RecurIdxType *)malloc(num_indices*sizeof(RecurIdxType));
    if (triangle_recur->idx_types==NULL) {
        printf("TriangleRecurCreate>> number of indices %"GINT_FMT"\n", num_indices);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for triangle_recur->idx_types");
    }
    triangle_recur->idx_orders = (GInt *)malloc(num_indices*sizeof(GInt));
    if (triangle_recur->idx_orders==NULL) {
        printf("TriangleRecurCreate>> number of indices %"GINT_FMT"\n", num_indices);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for triangle_recur->idx_orders");
    }
    for (irec=0; irec<num_indices; irec++) {
        triangle_recur->idx_types[irec] = idx_types[irec];
        triangle_recur->idx_orders[irec] = idx_orders[irec];
        switch (idx_types[irec]) {
        case RECUR_IDX_SCALAR:
            if (idx_orders[irec]<=0) {
                printf("TriangleRecurCreate>> index %"GINT_FMT" of recurrence relations: %"GINT_FMT"\n",
                       irec,
                       idx_orders[irec]);
                GErrorExit(FILE_AND_LINE, "invalid index");
            }
            triangle_recur->size_wrk_array *= idx_orders[irec];
            break;
        case RECUR_IDX_CARTESIAN:
            if (idx_orders[irec]<0) {
                printf("TriangleRecurCreate>> index %"GINT_FMT" of recurrence relations: %"GINT_FMT"\n",
                       irec,
                       idx_orders[irec]);
                GErrorExit(FILE_AND_LINE, "invalid index");
            }
            triangle_recur->size_wrk_array *= (idx_orders[irec]+1)*(idx_orders[irec]+2)/2;
            break;
        case RECUR_IDX_SPHERICAL:
            if (idx_orders[irec]<0) {
                printf("TriangleRecurCreate>> index %"GINT_FMT" of recurrence relations: %"GINT_FMT"\n",
                       irec,
                       idx_orders[irec]);
                GErrorExit(FILE_AND_LINE, "invalid index");
            }
            triangle_recur->size_wrk_array *= (2*idx_orders[irec]+1);
            /* SGTO contractions reduce the order of SGTOs and geometric derivatives */
            triangle_recur->num_recur--;
            break;
        default:
            printf("TriangleRecurCreate>> type of index %"GINT_FMT" of recurrence relations: %d\n",
                   irec,
                   idx_types[irec]);
            GErrorExit(FILE_AND_LINE, "invalid type");
        }
    }
    triangle_recur->work_array = NULL;
    triangle_recur->relations = (RecurDAG *)malloc(triangle_recur->num_recur*sizeof(RecurDAG));
    if (triangle_recur->relations==NULL) {
        printf("TriangleRecurCreate>> number of recurrence relations %"GINT_FMT"\n",
               triangle_recur->num_recur);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for triangle_recur->relations");
    }
    for (irec=0; irec<triangle_recur->num_recur; irec++) {
        triangle_recur->relations[irec].num_recur_indices = 0;
#if defined(GEN1INT_DEBUG)
        triangle_recur->relations[irec].output_idx = 0;
        triangle_recur->relations[irec].num_RHS_terms = 0;
#endif
        triangle_recur->relations[irec].num_wrk_array = 0;
        triangle_recur->relations[irec].num_output_orders = 0;
        triangle_recur->relations[irec].recur_indices = NULL;
#if defined(GEN1INT_DEBUG)
        triangle_recur->relations[irec].RHS_recur_orders = NULL;
#endif
        triangle_recur->relations[irec].num_vtx = NULL;
        triangle_recur->relations[irec].head_vtx = NULL;
    }
    return GSUCCESS;
}
