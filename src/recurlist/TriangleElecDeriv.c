/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleElecDeriv().

   2014-11-23, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleElecDeriv(TriangleRecur *triangle_recur,
                             const GInt which_recur)
{
    RecurDAG *cur_relation;  /* current recurrence relation */
    RecurVertex *cur_node;          /* current triangle node */
    GInt idx_geo_bra;                /* index of HGTOs/geometric derivatives on the bra center */
    GInt size_outermost;             /* size of all other operators and contractions, etc. */
    GInt size_geo_bra;               /* size of HGTOs/geometric derivatives on the bra center */
    GInt inode;                      /* incremental recorder over nodes */
    GInt idx;                        /* incremental recorder over indices */
    GErrorCode ierr;                 /* error information */
    /* points to the recurrence relation */
    if (which_recur>=triangle_recur->num_recur || which_recur<0) {
        printf("TriangleElecDeriv>> given recurrence relation %"GINT_FMT"\n",
               which_recur);
        printf("TriangleElecDeriv>> number of added recurrence relations %"GINT_FMT"\n",
               triangle_recur->num_recur);
        GErrorExit(FILE_AND_LINE, "invalid given recurrence relation");
    }
    cur_relation = &triangle_recur->relations[which_recur];
    /* checks the number of indices in the recurrence relation */
    if (cur_relation->num_recur_indices!=2) {
        printf("TriangleElecDeriv>> number of indices in the recurrence relation %"GINT_FMT"\n",
               cur_relation->num_recur_indices);
        GErrorExit(FILE_AND_LINE, "invalid number of indices");
    }
    /* there are electronic derivatives */
    if (cur_relation->num_output_orders==2) {
        /* gets the indices of HGTOs/geometric derivatives on the bra center */
        idx_geo_bra = triangle_recur->num_indices-2;
        /* loops over different nodes */
        cur_node = cur_relation->head_vtx[1]->suc_LHS;
        for (inode=0; inode<cur_relation->num_vtx[1]; inode++) {
            /* gets the size of all other operators and contractions, etc. */
            size_outermost = 1;
            for (idx=0; idx<idx_geo_bra; idx++) {
                if (idx==cur_relation->recur_indices[0] ||
                    cur_node->idx_orders[idx]==0) continue;
                switch (triangle_recur->idx_types[idx]) {
                case RECUR_IDX_SCALAR:
                    size_outermost *= cur_node->idx_orders[idx];
                    break;
                case RECUR_IDX_CARTESIAN:
                    size_outermost *= (cur_node->idx_orders[idx]+1)
                                    * (cur_node->idx_orders[idx]+2)/2;
                    break;
                case RECUR_IDX_SPHERICAL:
                    size_outermost *= (2*cur_node->idx_orders[idx]+1);
                    break;
                default:
                    printf("TriangleElecDeriv>> recurrence relation %"GINT_FMT"\n",
                           which_recur);
                    printf("TriangleElecDeriv>> type of index %"GINT_FMT" of recurrence relations: %d\n",
                           idx,
                           triangle_recur->idx_types[idx]);
                    GErrorExit(FILE_AND_LINE, "invalid type");
                }
            }
            /* gets the size of HGTOs/geometric derivatives on the bra center */
            size_geo_bra = (cur_node->idx_orders[idx_geo_bra]+1)
                         * (cur_node->idx_orders[idx_geo_bra]+2)/2;
            /* shifts order of HGTOs/geometric derivatives on the ket center to electronic derivatives */
            ierr = TriangleOrderLShift(0,
                                       cur_node->idx_orders[cur_relation->recur_indices[1]],
                                       cur_node->idx_orders[cur_relation->recur_indices[0]],
                                       size_outermost,
                                       1,
                                       size_geo_bra,
                                       &triangle_recur->work_array[cur_node->RHS_terms[0]->idx_wrk][cur_node->RHS_terms[0]->addr_wrk],
                                       &triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleOrderLShift");
            /* moves to the next node */
            cur_node = cur_node->suc_LHS;
        }
    }
    else if (cur_relation->num_output_orders>2) {
        printf("TriangleElecDeriv>> number of output index orders %"GINT_FMT"\n",
               cur_relation->num_output_orders);
        GErrorExit(FILE_AND_LINE, "invalid number of output index orders");
    }
    return GSUCCESS;
}
