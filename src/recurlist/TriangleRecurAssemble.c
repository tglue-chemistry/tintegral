/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurAssemble().

   2014-11-08, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleRecurAssemble(TriangleRecur *triangle_recur)
{
    GInt size_wrk_array;  /* size of all work arrays */
    GInt iwrk;            /* incremental recorder */
    triangle_recur->work_array = (GReal **)malloc(triangle_recur->num_wrk_array*sizeof(GReal *));
    if (triangle_recur->work_array==NULL) {
        printf("TriangleRecurAssemble>> number of work arrays %"GINT_FMT"\n",
               triangle_recur->num_wrk_array);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for work arrays");
    }
    /* allocates memory for the two-dimensional work array at one time */
    size_wrk_array = triangle_recur->num_wrk_array*triangle_recur->size_wrk_array;
    triangle_recur->work_array[0] = (GReal *)malloc(size_wrk_array*sizeof(GReal));
    if (triangle_recur->work_array[0]==NULL) {
        printf("TriangleRecurAssemble>> number of work arrays %"GINT_FMT"\n",
               triangle_recur->num_wrk_array);
        printf("TriangleRecurAssemble>> size of work arrays %"GINT_FMT"\n",
               triangle_recur->size_wrk_array);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for work arrays[0]");
    }
    /* make the pointer of each work array to the correct memory */
    for (iwrk=0; iwrk<triangle_recur->num_wrk_array-1; iwrk++) {
        triangle_recur->work_array[iwrk+1] = triangle_recur->work_array[iwrk]
                                           + triangle_recur->size_wrk_array;
    }
    return GSUCCESS;
}
