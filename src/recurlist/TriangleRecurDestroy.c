/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurDestroy().

   2014-10-21, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleRecurDestroy(TriangleRecur *triangle_recur)
{
    RecurVertex *cur_vertex;  /* current vertex of the DAG */
    GInt irec;                /* incremental recorder over recurrence relations */
    GInt iterm;               /* incremental recorder over terms */
    GInt iorder;              /* incremental recorder over orders */
    free(triangle_recur->idx_types);
    triangle_recur->idx_types = NULL;
    free(triangle_recur->idx_orders);
    triangle_recur->idx_orders = NULL;
    free(triangle_recur->work_array[0]);
    for (iterm=0; iterm<triangle_recur->num_wrk_array; iterm++) {
        triangle_recur->work_array[iterm] = NULL;
    }
    free(triangle_recur->work_array);
    triangle_recur->work_array = NULL;
    for (irec=0; irec<triangle_recur->num_recur; irec++) {
        free(triangle_recur->relations[irec].recur_indices);
        triangle_recur->relations[irec].recur_indices = NULL;
#if defined(GEN1INT_DEBUG)
        free(triangle_recur->relations[irec].RHS_recur_orders);
        triangle_recur->relations[irec].RHS_recur_orders = NULL;
#endif
        /* cleans the number of vertices */
        free(triangle_recur->relations[irec].num_vtx);
        triangle_recur->relations[irec].num_vtx = NULL;
        /* first cleans the vertex of the DAGs with zeroth order of output index */
        cur_vertex = triangle_recur->relations[irec].head_vtx[0]->suc_LHS;
        free(triangle_recur->relations[irec].head_vtx[0]);
        triangle_recur->relations[irec].head_vtx[0] = NULL;
        while (cur_vertex!=NULL) {
            triangle_recur->relations[irec].head_vtx[0] = cur_vertex->suc_LHS;
            free(cur_vertex->idx_orders);
            cur_vertex->idx_orders = NULL;
            cur_vertex->suc_LHS = NULL;
            cur_vertex = triangle_recur->relations[irec].head_vtx[0];
        }
        /* cleans other vertices of the DAGs order by order */
        for (iorder=1; iorder<triangle_recur->relations[irec].num_output_orders; iorder++) {
            cur_vertex = triangle_recur->relations[irec].head_vtx[iorder]->suc_LHS;
            free(triangle_recur->relations[irec].head_vtx[iorder]);
            triangle_recur->relations[irec].head_vtx[iorder] = NULL;
            while (cur_vertex!=NULL) {
                triangle_recur->relations[irec].head_vtx[iorder] = cur_vertex->suc_LHS;
                free(cur_vertex->idx_orders);
                cur_vertex->idx_orders = NULL;
                cur_vertex->suc_LHS = NULL;
                for (iterm=0; iterm<cur_vertex->num_RHS_terms; iterm++) {
                    cur_vertex->RHS_terms[iterm] = NULL;
                }
                free(cur_vertex->RHS_terms);
                cur_vertex->RHS_terms = NULL;
                cur_vertex = triangle_recur->relations[irec].head_vtx[iorder];
            }
        }
        free(triangle_recur->relations[irec].head_vtx);
        triangle_recur->relations[irec].head_vtx = NULL;
    }
    return GSUCCESS;
}
