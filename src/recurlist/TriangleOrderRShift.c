/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleOrderRShift().

   2014-10-01, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

/*% \brief shifts order of input values right
    \author Bin Gao
    \date 2014-10-01
    \param{GInt:int}{in}{order_left} order of the left triangle to be shifted
    \param{GInt:int}{in}{order_right} order of the right triangle
    \param{GInt:int}{in}{order_shift} order to be shifted
    \param{GInt:int}{in}{size_outermost} size of the outermost dimension
    \param{GInt:int}{in}{size_interv} size of the dimension between the right and left triangles
    \param{GInt:int}{in}{size_innermost} size of the innermost dimension
    \param{GReal:real}{in}{val_inp} input values in the shape of
        [size_outermost][order_left+order_shift][size_interv][order_right][size_innermost]
    \param{GReal:real}{out}{val_out} output values in the shape of
        [size_outermost][order_left][size_interv][order_right+order_shift][size_innermost]
    \return{GErrorCode:int} error information
*/
GErrorCode TriangleOrderRShift(const GInt order_left,
                               const GInt order_right,
                               const GInt order_shift,
                               const GInt size_outermost,
                               const GInt size_interv,
                               const GInt size_innermost,
                               GReal *val_inp,
                               GReal *val_out)
{
    GInt order_left_shift;   /* sum of orders of left triangle and shifted */
    GInt offset_outer;       /* offset of the outermost dimension */
    GInt offset_left;        /* offset of the components of left triangle */
    GInt offset_inter;       /* offset of the dimension between the right and left triangles */
    GInt offset_x_right;     /* offset due to skip the components x...x -> xz...z of right triangle */
    GInt offset_y_right;     /* offset due to skip the components x...x -> yz...z of right triangle */
    GInt offset_yz_shift;    /* offset of order shifted along yz direction */
    GReal *ptr_out;          /* pointer to the output values */
    GReal *ptr_outer;        /* pointer to the outermost dimension */
    GReal *ptr_left;         /* pointer to the components of left triangle */
    GReal *ptr_inter;        /* pointer to the dimension between the right and left triangles */
    GReal *ptr_shift;        /* pointer to the components of order shifted */
    GReal *ptr_right;        /* pointer to the right triangle */
    GInt inc_outer;          /* incremental recorder over the outermost dimension */
    GInt inc_inter;          /* incremental recorder over the dimension between the right and left triangles */
    GInt inc_inner;          /* incremental recorder over the innermost dimension */
    GInt order_yz_left;      /* order of the yz components of left triangle */
    GInt order_z_left;       /* order of the z component of left triangle */
    GInt order_yz_shift;     /* order of the yz components of shifted order */
    GInt order_z_shift;      /* order of the z component of shifted order */
    GInt order_yz_right;     /* order of the yz components of right triangle */
    GInt order_z_right;      /* order of the z component of right triangle */
    /* offset of the dimension between the right and left triangles */
    offset_inter = (order_right+1)*(order_right+2)/2*size_innermost;
    /* offset of the components of left triangle */
    offset_left = size_interv*offset_inter;
    /* offset of the outermost dimension */
    order_left_shift = order_left+order_shift;
    offset_outer = (order_left_shift+1)*(order_left_shift+2)/2*offset_left;
    /* offset due to skip the components x...x -> xz...z of right triangle */
    offset_x_right = order_right*(order_right+1)/2*size_innermost;
    /* offset due to skip the components x...x -> yz...z of right triangle */
    offset_y_right = offset_x_right+order_right*size_innermost;
    /* pointer to the output values */
    ptr_out = val_out;
    /* pointer to the outermost dimension */
    ptr_outer = val_inp;
    /* loops over the outermost dimension */
    for (inc_outer=0; inc_outer<size_outermost; inc_outer++) {
        /* pointer to the components of left trianlge */
        ptr_left = ptr_outer;
        /* offset of order shifted along yz direction */
        offset_yz_shift = offset_left;
        /* all components of left triangle */
        for (order_yz_left=0; order_yz_left<=order_left; order_yz_left++) {
            for (order_z_left=0; order_z_left<=order_yz_left; order_z_left++) {
                /* pointer to the dimension between the right and left triangles */
                ptr_inter = ptr_left;
                /* loops over the dimension between the right and left triangles */
                for (inc_inter=0; inc_inter<size_interv; inc_inter++) {
                    /* component x...x of order shifted */
                    ptr_shift = ptr_inter;
                    /* pointer to the components of right triangle */
                    ptr_right = ptr_shift;
                    /* all components of right triangle */
                    for (order_yz_right=0; order_yz_right<=order_right; order_yz_right++) {
                        for (order_z_right=0; order_z_right<=order_yz_right; order_z_right++) {
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                    }
                    /* other components of order shifted */
                    for (order_yz_shift=1; order_yz_shift<=order_shift; order_yz_shift++) {
                        /* component x..xy...y of order shifted */
                        ptr_shift += offset_yz_shift;
                        /* skips the components x...x -> xz...z of right triangle */
                        ptr_right = ptr_shift+offset_x_right;
                        for (order_z_right=0; order_z_right<=order_right; order_z_right++) {
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                        /* components x...xy...z -> x...xz...z of order shifted */
                        for (order_z_shift=1; order_z_shift<=order_yz_shift; order_z_shift++) {
                            /* moves to the next component of order shifted */
                            ptr_shift += offset_left;
                            /* skips the components x...x -> yz...z of right triangle */
                            ptr_right = ptr_shift+offset_y_right;
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                    }
                    ptr_inter += offset_inter;
                }
                /* moves to the next component of left triangle */
                ptr_left += offset_left;
            }
            offset_yz_shift += offset_left;
        }
        ptr_outer += offset_outer;
    }
    return GSUCCESS;
}
