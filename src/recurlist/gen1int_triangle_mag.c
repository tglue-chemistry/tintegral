/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file initializes recurrence relations for magnetic derivatives.

   2014-11-17, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle_mag.h"

/* initializes recurrence relations for magnetic derivatives */
const GInt RHS_TERMS_LAO_TO_MAG[] = {-1,1,0, -1,1,-1};
const GInt RHS_TERMS_CGTO_TO_LAO[] = {0,-1,0, 0,-1,-1, 1,-1,0};
const GInt RHS_TERMS_HGTO_TO_LAO[] = {-1,0, -1,-1, -1,1};
