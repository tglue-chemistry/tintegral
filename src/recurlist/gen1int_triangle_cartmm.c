/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file initializes recurrence relations for Cartesian multipole moments.

   2014-10-27, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle_cartmm.h"

/* initializes recurrence relations for Cartesian multipole moments */
const GInt CARTMM_RHS_TERMS_CART_MM[] = {-1,0,0, -1,-1,0, -1,0,-1, -2,0,0};
const GInt CARTMM_RHS_TERMS_HGTO_KET[] = {1,-1};
const GInt CARTMM_RHS_TERMS_HGTO_BRA[] = {-1, -2};
