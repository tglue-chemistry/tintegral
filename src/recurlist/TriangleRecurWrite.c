/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurWrite().

   2014-10-21, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleRecurWrite(const TriangleRecur *triangle_recur, FILE *fp_trec)
{
    RecurVertex *cur_vertex;  /* current vertex of the DAG */
    GInt last_index;          /* position of the last index */
    GInt irec;                /* incremental recorder over recurrence relations */
    GInt iorder;              /* incremental recorder over orders */
    GInt ivtx;                /* incremental recorder over vertices */
    GInt iterm;               /* incremental recorder over terms */
    GInt idx;                 /* incremental recorder over indices */
    fprintf(fp_trec,
            "TriangleRecurWrite>> number of work arrays %"GINT_FMT"\n",
            triangle_recur->num_wrk_array);
    fprintf(fp_trec,
            "TriangleRecurWrite>> size of work arrays %"GINT_FMT"\n",
            triangle_recur->size_wrk_array);
    fprintf(fp_trec,
            "TriangleRecurWrite>> number of indices %"GINT_FMT"\n",
            triangle_recur->num_indices);
    fprintf(fp_trec,
            "TriangleRecurWrite>> number of recurrence relations %"GINT_FMT"\n",
            triangle_recur->num_recur);
    last_index = triangle_recur->num_indices-1;
    fprintf(fp_trec, "TriangleRecurWrite>> types of indices {");
    for (idx=0; idx<last_index; idx++) {
        fprintf(fp_trec, "%d,", triangle_recur->idx_types[idx]);
    }
    fprintf(fp_trec, "%d}\n", triangle_recur->idx_types[last_index]);
    for (irec=0; irec<triangle_recur->num_recur; irec++) {
        fprintf(fp_trec, "TriangleRecurWrite>>\n");
        fprintf(fp_trec,
                "TriangleRecurWrite>> recurrence relation %"GINT_FMT"\n",
                irec);
        fprintf(fp_trec,
                "TriangleRecurWrite>> number of indices in the recurrence relation %"GINT_FMT"\n",
                triangle_recur->relations[irec].num_recur_indices);
        fprintf(fp_trec,
                "TriangleRecurWrite>> number of work arrays for current recurrence relation %"GINT_FMT"\n",
                triangle_recur->relations[irec].num_wrk_array);
        fprintf(fp_trec,
                "TriangleRecurWrite>> indices involved in the recurrence relation ");
        for (idx=0; idx<triangle_recur->relations[irec].num_recur_indices; idx++) {
            fprintf(fp_trec,
                    "[%"GINT_FMT"]",
                    triangle_recur->relations[irec].recur_indices[idx]);
        }
        fprintf(fp_trec, "\n");
#if defined(GEN1INT_DEBUG)
        fprintf(fp_trec,
                "TriangleRecurWrite>> output index %"GINT_FMT"\n",
                triangle_recur->relations[irec].output_idx);
        fprintf(fp_trec,
                "TriangleRecurWrite>> number of right hand side terms %"GINT_FMT"\n",
                triangle_recur->relations[irec].num_RHS_terms);
        fprintf(fp_trec,
                "TriangleRecurWrite>> RHS term    recurrence orders of indices\n");
        for (iterm=0,iorder=0; iterm<triangle_recur->relations[irec].num_RHS_terms; iterm++) {
            fprintf(fp_trec, "TriangleRecurWrite>>    %"GINT_FMT"        {", iterm);
            for (idx=0; idx<triangle_recur->relations[irec].num_recur_indices-1; idx++) {
                fprintf(fp_trec,
                        "%"GINT_FMT",",
                        triangle_recur->relations[irec].RHS_recur_orders[iorder++]);
            }
            fprintf(fp_trec,
                    "%"GINT_FMT"}\n",
                    triangle_recur->relations[irec].RHS_recur_orders[iorder++]);
        }
#endif
        fprintf(fp_trec,
                "TriangleRecurWrite>> number of output index orders %"GINT_FMT"\n",
                triangle_recur->relations[irec].num_output_orders);
        for (iorder=triangle_recur->relations[irec].num_output_orders-1; iorder>=0; iorder--) {
            fprintf(fp_trec,
                    "TriangleRecurWrite>> level of the output index %"GINT_FMT"\n",
                    iorder);
            fprintf(fp_trec,
                    "TriangleRecurWrite>> ID   [index][address]     orders        RHS vertices\n");
            cur_vertex = triangle_recur->relations[irec].head_vtx[iorder]->suc_LHS;
            for (ivtx=0; ivtx<triangle_recur->relations[irec].num_vtx[iorder]; ivtx++) {
                fprintf(fp_trec,
                        "TriangleRecurWrite>> %"GINT_FMT"   [%"GINT_FMT"][%"GINT_FMT"]        {",
                        ivtx,
                        cur_vertex->idx_wrk,
                        cur_vertex->addr_wrk);
                for (idx=0; idx<last_index; idx++) {
                    fprintf(fp_trec, "%"GINT_FMT",", cur_vertex->idx_orders[idx]);
                }
                fprintf(fp_trec, "%"GINT_FMT"}", cur_vertex->idx_orders[last_index]);
                for (iterm=0; iterm<cur_vertex->num_RHS_terms; iterm++) {
                    if (cur_vertex->RHS_terms[iterm]!=NULL) {
                        fprintf(fp_trec, "   {");
                        for (idx=0; idx<last_index; idx++) {
                            fprintf(fp_trec,
                                    "%"GINT_FMT",",
                                    cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                        }
                        fprintf(fp_trec,
                                "%"GINT_FMT"}",
                                cur_vertex->RHS_terms[iterm]->idx_orders[last_index]);
                    }
                }
                fprintf(fp_trec, "\n");
                cur_vertex = cur_vertex->suc_LHS;
            }
        }
    }
    return GSUCCESS;
}
