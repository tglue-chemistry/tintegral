/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleCartMM().

   2014-11-20, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleCartMM(TriangleRecur *triangle_recur,
                          const GReal coord_bra[3],
                          const GReal exponent_bra,
                          const GReal coord_ket[3],
                          const GReal exponent_ket,
                          const GReal dipole_origin[3])
{
    RecurDAG *cur_relation;  /* current recurrence relation */
    RecurVertex *cur_node;          /* current triangle node */
    GReal hrp_total_expnt;           /* half reciprocal of the total exponent */
    GReal cc_wrt_diporg[3];          /* relative coordinates of center-of-charge w.r.t. dipole origin */
    GInt idx_geo_bra;                /* index of HGTOs/geometric derivatives on the bra center */
    GInt idx_geo_ket;                /* index of HGTOs/geometric derivatives on the ket center */
    GInt idx_cart_mm;                /* index of the Cartesian multipole moments */
    GInt iorder;                     /* incremental recorder over orders */
    GInt inode;                      /* incremental recorder over nodes */
    GReal *ptr_LHS;                  /* pointer to the value of LHS */
    GReal *ptr_RHS1;                 /* pointer to the value of the first RHS term */
    GReal *ptr_RHS2;                 /* pointer to the value of the second RHS term */
    GReal *ptr_RHS3;                 /* pointer to the value of the third RHS term */
    GReal *ptr_RHS4;                 /* pointer to the value of the fourth RHS term */
    GInt order_mm_RHS1;              /* order of Cartesian multipole moments of the first RHS term */
    GInt order_ket_LHS;              /* order of HGTOs (ket) of the LHS term */
    GInt order_bra_LHS;              /* order of HGTOs (bra) of the LHS term */
    GInt order_yz_mm_RHS1;           /* order of yz components of Cartesian multipole moments of the first RHS term */
    GInt order_x_mm_RHS1;            /* order of x component of Cartesian multipole moments of the first RHS term */
    GInt order_z_mm_RHS1;            /* order of z component of Cartesian multipole moments of the first RHS term */
    GInt order_yz_ket_LHS;           /* order of yz components of HGTOs (ket) of the LHS term */
    GInt order_x_ket_LHS;            /* order of x component of HGTOs (ket) of the LHS term */
    GInt order_z_ket_LHS;            /* order of z component of HGTOs (ket) of the LHS term */
    GInt order_yz_bra_LHS;           /* order of yz components of HGTOs (bra) of the LHS term */
    GInt order_x_bra_LHS;            /* order of x component of HGTOs (bra) of the LHS term */
    GInt order_z_bra_LHS;            /* order of z component of HGTOs (bra) of the LHS term */
    GInt size_bra_RHS2;              /* size of HGTOs (bra) of the second RHS term */
    GInt size_ket_RHS3;              /* size of HGTOs (ket) of the third RHS term */
    GInt size_bra_LHS;               /* size of HGTOs (bra) of the LHS term */
    GInt size_ket_LHS;               /* size of HGTOs (ket) of the LHS term  */
    GInt size_braket_LHS;            /* size of HGTOs of the LHS term  */
    GInt size_yz_mm_RHS4;            /* size of yz components of Cartesian multipole moments of the fourth RHS term */
    GInt size_braket_RHS2;           /* size of HGTOs of the second RHS term */
    GInt size_braket_RHS3;           /* size of HGTOs of the third RHS term */
    GInt order_y_mm_RHS1;            /* order of y component of Cartesian multipole moments of the first RHS term */
    GInt order_y_ket_LHS;            /* order of y component of HGTOs (ket) of the LHS term */
    GInt order_y_bra_LHS;            /* order of y component of HGTOs (bra) of the LHS term */
    GInt inc_ket_LHS;                /* incremental recorder over HGTOs (ket) of the LHS term */
    GInt inc_bra_LHS;                /* incremental recorder over HGTOs (bra) of the LHS term */
    /* Cartesian multipole moments uses the third last recurrence relation */
    cur_relation = &triangle_recur->relations[triangle_recur->num_recur-3];
    /* 1st and high orders output index */
    if (cur_relation->num_output_orders>1) {
        /* computes the relative coordinates of center-of-charge w.r.t. dipole origin */
        hrp_total_expnt = 1.0/(exponent_bra+exponent_ket);
        for (iorder=0; iorder<3; iorder++) {
            cc_wrt_diporg[iorder] = hrp_total_expnt*(exponent_bra*coord_bra[iorder]
                                  + exponent_ket*coord_ket[iorder])
                                  - dipole_origin[iorder];
        }
        /* computes the half reciprocal of total exponent */
        hrp_total_expnt *= 0.5;
        /* gets the indices of HGTOs/geometric derivatives on the bra and ket centers */
        idx_geo_bra = triangle_recur->num_indices-2;
        idx_geo_ket = triangle_recur->num_indices-1;
        /* index of the Cartesian multipole moments is the first one of the recurrence relation */
        idx_cart_mm = cur_relation->recur_indices[0];
        /* loops over orders of output index */
        for (iorder=1; iorder<cur_relation->num_output_orders; iorder++) {
            /* loops over different nodes of the current order of output index */
            cur_node = cur_relation->head_vtx[iorder]->suc_LHS;
            for (inode=0; inode<cur_relation->num_vtx[iorder]; inode++) {
                /* gets the order of Cartesian multipole moments of the first RHS term */
                order_mm_RHS1 = cur_node->idx_orders[idx_cart_mm]-1;
                /* gets the orders of HGTOs of the LHS term */
                order_ket_LHS = cur_node->idx_orders[idx_geo_ket];
                order_bra_LHS = cur_node->idx_orders[idx_geo_bra];
                /* pointer to the LHS term */
                ptr_LHS = &triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk];
                /* pointer to the first RHS term */
                ptr_RHS1 = &triangle_recur->work_array[cur_node->RHS_terms[0]->idx_wrk][cur_node->RHS_terms[0]->addr_wrk];
                if (cur_node->RHS_terms[1]!=NULL) {
                    /* pointer to the second RHS term */
                    ptr_RHS2 = &triangle_recur->work_array[cur_node->RHS_terms[1]->idx_wrk][cur_node->RHS_terms[1]->addr_wrk];
                    if (cur_node->RHS_terms[2]!=NULL) {
                        /* pointer to the third RHS term */
                        ptr_RHS3 = &triangle_recur->work_array[cur_node->RHS_terms[2]->idx_wrk][cur_node->RHS_terms[2]->addr_wrk];
                        /* 1, 2, 3, 4 RHS terms */
                        if (cur_node->RHS_terms[3]!=NULL) {
                            /* pointer to the fourth RHS term */
                            ptr_RHS4 = &triangle_recur->work_array[cur_node->RHS_terms[3]->idx_wrk][cur_node->RHS_terms[3]->addr_wrk];
                            /* # recurrence relation along the x direction (Cartesian multipole moments)
                                 [xx...x, xxz...z] <= [x...x, xz...z] */
                            for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<order_mm_RHS1; order_yz_mm_RHS1++) {
                                order_x_mm_RHS1 = order_mm_RHS1-order_yz_mm_RHS1;
                                for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                    /* ## recurrence relation along the x direction (ket)
                                          [x...x, xz...z] <= [x...x, xz...z] */
                                    for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                        order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                        for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                            /* ### recurrence relation along the x direction (bra)
                                                   [x...x, xz...z] <= [x...x, xz...z] */
                                            for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                                order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                                for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                             + hrp_total_expnt*(order_x_bra_LHS*(*ptr_RHS2)
                                                             + order_x_ket_LHS*(*ptr_RHS3)
                                                             + order_x_mm_RHS1*(*ptr_RHS4));
                                                    ptr_LHS++;
                                                    ptr_RHS1++;
                                                    ptr_RHS2++;
                                                    ptr_RHS3++;
                                                    ptr_RHS4++;
                                                }
                                            }
                                            /* ### recurrence relation along the x direction (bra)
                                                   [y...y, z...z] <= [y...y, z...z] */
                                            for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                         + hrp_total_expnt*(order_x_ket_LHS*(*ptr_RHS3)
                                                         + order_x_mm_RHS1*(*ptr_RHS4));
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS3++;
                                                ptr_RHS4++;
                                            }
                                        }
                                    }
                                    /* ## recurrence relation along the x direction (ket)
                                          [y...y, z...z] <= [y...y, z...z] */
                                    for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                        /* ### recurrence relation along the x direction (bra)
                                               [x...x, xz...z] <= [x...x, xz...z] */
                                        for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                            order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                            for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                         + hrp_total_expnt*(order_x_bra_LHS*(*ptr_RHS2)
                                                         + order_x_mm_RHS1*(*ptr_RHS4));
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS2++;
                                                ptr_RHS4++;
                                            }
                                        }
                                        /* ### recurrence relation along the x direction (bra)
                                               [y...y, z...z] <= [y...y, z...z] */
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_x_mm_RHS1*(*ptr_RHS4);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS4++;
                                        }
                                    }
                                }
                            }
                            /* # recurrence relation along the x direction (Cartesian multipole moments)
                                 [xy...y, xz...z] <= [y...y, z...z] */
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the x direction (ket)
                                      [x...x, xz...z] <= [x...x, xz...z] */
                                for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                    order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                    for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                        /* ### recurrence relation along the x direction (bra)
                                               [x...x, xz...z] <= [x...x, xz...z] */
                                        for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                            order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                            for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                         + hrp_total_expnt*(order_x_bra_LHS*(*ptr_RHS2)
                                                         + order_x_ket_LHS*(*ptr_RHS3));
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS2++;
                                                ptr_RHS3++;
                                            }
                                        }
                                        /* ### recurrence relation along the x direction (bra)
                                               [y...y, z...z] <= [y...y, z...z] */
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_x_ket_LHS*(*ptr_RHS3);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS3++;
                                        }
                                    }
                                }
                                /* ## recurrence relation along the x direction (ket)
                                      [y...y, z...z] <= [y...y, z...z] */
                                for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, xz...z] <= [x...x, xz...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                        order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_x_bra_LHS*(*ptr_RHS2);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                        }
                                    }
                                    /* ### recurrence relation along the x direction (bra)
                                           [y...y, z...z] <= [y...y, z...z] */
                                    for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                    }
                                }
                            }
                            /* # recurrence relation along the y direction (Cartesian multipole moments)
                                 [yy...y, yyz...z] <= [y...y, yz...z] */
                            size_bra_RHS2 = order_bra_LHS*(order_bra_LHS+1)/2;
                            size_ket_RHS3 = order_ket_LHS*(order_ket_LHS+1)/2;
                            size_bra_LHS = size_bra_RHS2+order_bra_LHS+1;
                            size_ket_LHS = size_ket_RHS3+order_ket_LHS+1;
                            size_braket_LHS = size_bra_LHS*size_ket_LHS;
                            size_yz_mm_RHS4 = order_mm_RHS1*size_braket_LHS;
                            ptr_RHS1 -= size_yz_mm_RHS4+size_braket_LHS;
                            ptr_RHS4 -= size_yz_mm_RHS4;
                            size_braket_RHS2 = size_ket_LHS*size_bra_RHS2;
                            ptr_RHS2 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS2;
                            size_braket_RHS3 = size_ket_RHS3*size_bra_LHS;
                            ptr_RHS3 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS3;
                            for (order_y_mm_RHS1=order_mm_RHS1; order_y_mm_RHS1>0; order_y_mm_RHS1--) {
                                /* ## recurrence relation along the y direction (ket)
                                      [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                    /* ## recurrence relation along the y direction (ket)
                                          [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                    for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...x, z...z] <= [x...x, z...z] */
                                        for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                            /* ### recurrence relation along the y direction (bra)
                                                   [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                            for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                         + hrp_total_expnt*(order_y_bra_LHS*(*ptr_RHS2)
                                                         + order_y_ket_LHS*(*ptr_RHS3)
                                                         + order_y_mm_RHS1*(*ptr_RHS4));
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS2++;
                                                ptr_RHS3++;
                                                ptr_RHS4++;
                                            }
                                            /* ### recurrence relation along the y direction (bra)
                                                   [x...xz...z] <= [x...xz...z] */
                                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_y_ket_LHS*(*ptr_RHS3)
                                                     + order_y_mm_RHS1*(*ptr_RHS4));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS3++;
                                            ptr_RHS4++;
                                        }
                                    }
                                    /* ## recurrence relation along the y direction (ket)
                                          [x...xz...z] <= [x...xz...z] */
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                        for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_y_bra_LHS*(*ptr_RHS2)
                                                     + order_y_mm_RHS1*(*ptr_RHS4));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                            ptr_RHS4++;
                                        }
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xz...z] <= [x...xz...z] */
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_y_mm_RHS1*(*ptr_RHS4);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS4++;
                                    }
                                }
                            }
                            /* # recurrence relation along the y direction (Cartesian multipole moments)
                                 [yz...z] <= [z...z] */
                            /* ## recurrence relation along the y direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                        for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_y_bra_LHS*(*ptr_RHS2)
                                                     + order_y_ket_LHS*(*ptr_RHS3));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                            ptr_RHS3++;
                                        }
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xz...z] <= [x...xz...z] */
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_y_ket_LHS*(*ptr_RHS3);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                    }
                                }
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xz...z] <= [x...xz...z] */
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                    for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_y_bra_LHS*(*ptr_RHS2);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                    }
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xz...z] <= [x...xz...z] */
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                            /* # recurrence relation along the z direction (Cartesian multipole moments)
                                 [zz...z] <= [z...z] */
                            ptr_RHS1 -= size_braket_LHS;
                            ptr_RHS4 -= size_braket_LHS;
                            ptr_RHS2 -= size_braket_RHS2;
                            ptr_RHS3 -= size_braket_RHS3;
                            /* ## recurrence relation along the z direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                /* ## recurrence relation along the z direction (ket)
                                      [x...xy...y] <= [x...xy...y] */
                                /* ### recurrence relation along the z direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...xy...y] <= [x...xy...y] */
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_mm_RHS1*(*ptr_RHS4);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS4++;
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                    for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                 + hrp_total_expnt*(order_z_bra_LHS*(*ptr_RHS2)
                                                 + order_mm_RHS1*(*ptr_RHS4));
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                        ptr_RHS4++;
                                    }
                                }
                                /* ## recurrence relation along the z direction (ket)
                                      [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                for (order_z_ket_LHS=1; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                        /* ### recurrence relation along the z direction (bra)
                                               [x...xy...y] <= [x...xy...y] */
                                        *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                 + hrp_total_expnt*(order_z_ket_LHS*(*ptr_RHS3)
                                                 + order_mm_RHS1*(*ptr_RHS4));
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                        ptr_RHS4++;
                                        /* ### recurrence relation along the z direction (bra)
                                               [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                        for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_z_bra_LHS*(*ptr_RHS2)
                                                     + order_z_ket_LHS*(*ptr_RHS3)
                                                     + order_mm_RHS1*(*ptr_RHS4));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                            ptr_RHS3++;
                                            ptr_RHS4++;
                                        }
                                    }
                                }
                            }
                        }
                        /* 1, 2, 3 RHS terms */
                        else {
                            /* # recurrence relation along the x direction (Cartesian multipole moments)
                                 [xx...x, xz...z] <= [x...x, z...z] */
                            for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<=order_mm_RHS1; order_yz_mm_RHS1++) {
                                for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                    /* ## recurrence relation along the x direction (ket)
                                          [x...x, xz...z] <= [x...x, xz...z] */
                                    for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                        order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                        for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                            /* ### recurrence relation along the x direction (bra)
                                                   [x...x, xz...z] <= [x...x, xz...z] */
                                            for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                                order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                                for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                             + hrp_total_expnt*(order_x_bra_LHS*(*ptr_RHS2)
                                                             + order_x_ket_LHS*(*ptr_RHS3));
                                                    ptr_LHS++;
                                                    ptr_RHS1++;
                                                    ptr_RHS2++;
                                                    ptr_RHS3++;
                                                }
                                            }
                                            /* ### recurrence relation along the x direction (bra)
                                                   [y...y, z...z] <= [y...y, z...z] */
                                            for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                         + hrp_total_expnt*order_x_ket_LHS*(*ptr_RHS3);
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS3++;
                                            }
                                        }
                                    }
                                    /* ## recurrence relation along the x direction (ket)
                                          [y...y, z...z] <= [y...y, z...z] */
                                    for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                        /* ### recurrence relation along the x direction (bra)
                                               [x...x, xz...z] <= [x...x, xz...z] */
                                        for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                            order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                            for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                         + hrp_total_expnt*order_x_bra_LHS*(*ptr_RHS2);
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS2++;
                                            }
                                        }
                                        /* ### recurrence relation along the x direction (bra)
                                               [y...y, z...z] <= [y...y, z...z] */
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                        }
                                    }
                                }
                            }
                            /* # recurrence relation along the y direction (Cartesian multipole moments)
                                 [yy...y, yz...z] <= [y...y, z...z] */
                            size_bra_RHS2 = order_bra_LHS*(order_bra_LHS+1)/2;
                            size_ket_RHS3 = order_ket_LHS*(order_ket_LHS+1)/2;
                            size_bra_LHS = size_bra_RHS2+order_bra_LHS+1;
                            size_ket_LHS = size_ket_RHS3+order_ket_LHS+1;
                            size_braket_LHS = size_bra_LHS*size_ket_LHS;
                            ptr_RHS1 -= cur_node->idx_orders[idx_cart_mm]*size_braket_LHS;
                            size_braket_RHS2 = size_ket_LHS*size_bra_RHS2;
                            ptr_RHS2 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS2;
                            size_braket_RHS3 = size_ket_RHS3*size_bra_LHS;
                            ptr_RHS3 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS3;
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the y direction (ket)
                                      [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                    /* ## recurrence relation along the y direction (ket)
                                          [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                    for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...x, z...z] <= [x...x, z...z] */
                                        for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                            /* ### recurrence relation along the y direction (bra)
                                                   [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                            for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                         + hrp_total_expnt*(order_y_bra_LHS*(*ptr_RHS2)
                                                         + order_y_ket_LHS*(*ptr_RHS3));
                                                ptr_LHS++;
                                                ptr_RHS1++;
                                                ptr_RHS2++;
                                                ptr_RHS3++;
                                            }
                                            /* ### recurrence relation along the y direction (bra)
                                                   [x...xz...z] <= [x...xz...z] */
                                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_y_ket_LHS*(*ptr_RHS3);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS3++;
                                        }
                                    }
                                    /* ## recurrence relation along the y direction (ket)
                                          [x...xz...z] <= [x...xz...z] */
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                        for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_y_bra_LHS*(*ptr_RHS2);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                        }
                                        /* ### recurrence relation along the y direction (bra)
                                               [x...xz...z] <= [x...xz...z] */
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                    }
                                }
                            }
                            /* # recurrence relation along the z direction (Cartesian multipole moments)
                                 [zz...z] <= [z...z] */
                            ptr_RHS1 -= size_braket_LHS;
                            ptr_RHS2 -= size_braket_RHS2;
                            ptr_RHS3 -= size_braket_RHS3;
                            /* ## recurrence relation along the z direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                /* ## recurrence relation along the z direction (ket)
                                      [x...xy...y] <= [x...xy...y] */
                                /* ### recurrence relation along the z direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...xy...y] <= [x...xy...y] */
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                    for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_z_bra_LHS*(*ptr_RHS2);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                    }
                                }
                                /* ## recurrence relation along the z direction (ket)
                                      [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                for (order_z_ket_LHS=1; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the z direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                        /* ### recurrence relation along the z direction (bra)
                                               [x...xy...y] <= [x...xy...y] */
                                        *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_z_ket_LHS*(*ptr_RHS3);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                        /* ### recurrence relation along the z direction (bra)
                                               [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                        for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_z_bra_LHS*(*ptr_RHS2)
                                                     + order_z_ket_LHS*(*ptr_RHS3));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                            ptr_RHS3++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /* 1, 2, 4 RHS terms */
                    else if (cur_node->RHS_terms[3]!=NULL) {
                        /* pointer to the fourth RHS term */
                        ptr_RHS4 = &triangle_recur->work_array[cur_node->RHS_terms[3]->idx_wrk][cur_node->RHS_terms[3]->addr_wrk];
                        /* size of the HGTOs (ket) of the LHS term */
                        size_ket_LHS = (order_ket_LHS+1)*(order_ket_LHS+2)/2;
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xx...x, xxz...z] <= [x...x, xz...z] */
                        for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<order_mm_RHS1; order_yz_mm_RHS1++) {
                            order_x_mm_RHS1 = order_mm_RHS1-order_yz_mm_RHS1;
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the x direction (ket)
                                      [x...x, z...z] <= [x...x, z...z] */
                                for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, xz...z] <= [x...x, xz...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                        order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_x_bra_LHS*(*ptr_RHS2)
                                                     + order_x_mm_RHS1*(*ptr_RHS4));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                            ptr_RHS4++;
                                        }
                                    }
                                    /* ### recurrence relation along the x direction (bra)
                                           [y...y, z...z] <= [y...y, z...z] */
                                    for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_x_mm_RHS1*(*ptr_RHS4);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS4++;
                                    }
                                }
                            }
                        }
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xy...y, xz...z] <= [y...y, z...z] */
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the x direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                /* ### recurrence relation along the x direction (bra)
                                       [x...x, xz...z] <= [x...x, xz...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                    order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                    for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_x_bra_LHS*(*ptr_RHS2);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                    }
                                }
                                /* ### recurrence relation along the x direction (bra)
                                       [y...y, z...z] <= [y...y, z...z] */
                                for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yy...y, yyz...z] <= [y...y, yz...z] */
                        size_bra_RHS2 = order_bra_LHS*(order_bra_LHS+1)/2;
                        size_bra_LHS = size_bra_RHS2+order_bra_LHS+1;
                        size_braket_LHS = size_bra_LHS*size_ket_LHS;
                        size_yz_mm_RHS4 = order_mm_RHS1*size_braket_LHS;
                        ptr_RHS1 -= size_yz_mm_RHS4+size_braket_LHS;
                        ptr_RHS4 -= size_yz_mm_RHS4;
                        size_braket_RHS2 = size_ket_LHS*size_bra_RHS2;
                        ptr_RHS2 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS2;
                        for (order_y_mm_RHS1=order_mm_RHS1; order_y_mm_RHS1>0; order_y_mm_RHS1--) {
                            /* ## recurrence relation along the y direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                    for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*(order_y_bra_LHS*(*ptr_RHS2)
                                                 + order_y_mm_RHS1*(*ptr_RHS4));
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                        ptr_RHS4++;
                                    }
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xz...z] <= [x...xz...z] */
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_y_mm_RHS1*(*ptr_RHS4);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS4++;
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yz...z] <= [z...z] */
                        /* ## recurrence relation along the y direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the y direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                /* ### recurrence relation along the y direction (bra)
                                       [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_y_bra_LHS*(*ptr_RHS2);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS2++;
                                }
                                /* ### recurrence relation along the y direction (bra)
                                       [x...xz...z] <= [x...xz...z] */
                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                            }
                        }
                        /* # recurrence relation along the z direction (Cartesian multipole moments)
                             [zz...z] <= [z...z] */
                        ptr_RHS1 -= size_braket_LHS;
                        ptr_RHS4 -= size_braket_LHS;
                        ptr_RHS2 -= size_braket_RHS2;
                        /* ## recurrence relation along the z direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the z direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                /* ### recurrence relation along the z direction (bra)
                                       [x...xy...y] <= [x...xy...y] */
                                *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                         + hrp_total_expnt*order_mm_RHS1*(*ptr_RHS4);
                                ptr_LHS++;
                                ptr_RHS1++;
                                ptr_RHS4++;
                                /* ### recurrence relation along the z direction (bra)
                                       [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                             + hrp_total_expnt*(order_z_bra_LHS*(*ptr_RHS2)
                                             + order_mm_RHS1*(*ptr_RHS4));
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS2++;
                                    ptr_RHS4++;
                                }
                            }
                        }
                    }
                    /* 1, 2 RHS terms */
                    else {
                        /* size of the HGTOs (ket) of the LHS term */
                        size_ket_LHS = (order_ket_LHS+1)*(order_ket_LHS+2)/2;
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xx...x, xz...z] <= [x...x, z...z] */
                        for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<=order_mm_RHS1; order_yz_mm_RHS1++) {
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the x direction (ket)
                                      [x...x, z...z] <= [x...x, z...z] */
                                for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, xz...z] <= [x...x, xz...z] */
                                    for (order_yz_bra_LHS=0; order_yz_bra_LHS<order_bra_LHS; order_yz_bra_LHS++) {
                                        order_x_bra_LHS = order_bra_LHS-order_yz_bra_LHS;
                                        for (order_z_bra_LHS=0; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_x_bra_LHS*(*ptr_RHS2);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS2++;
                                        }
                                    }
                                    /* ### recurrence relation along the x direction (bra)
                                           [y...y, z...z] <= [y...y, z...z] */
                                    for (order_z_bra_LHS=0; order_z_bra_LHS<=order_bra_LHS; order_z_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                    }
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yy...y, yz...z] <= [y...y, z...z] */
                        size_bra_RHS2 = order_bra_LHS*(order_bra_LHS+1)/2;
                        size_bra_LHS = size_bra_RHS2+order_bra_LHS+1;
                        size_braket_LHS = size_bra_LHS*size_ket_LHS;
                        ptr_RHS1 -= cur_node->idx_orders[idx_cart_mm]*size_braket_LHS;
                        size_braket_RHS2 = size_ket_LHS*size_bra_RHS2;
                        ptr_RHS2 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS2;
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the y direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                    for (order_y_bra_LHS=order_yz_bra_LHS; order_y_bra_LHS>0; order_y_bra_LHS--) {
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_y_bra_LHS*(*ptr_RHS2);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS2++;
                                    }
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...xz...z] <= [x...xz...z] */
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                        }
                        /* # recurrence relation along the z direction (Cartesian multipole moments)
                             [zz...z] <= [z...z] */
                        ptr_RHS1 -= size_braket_LHS;
                        ptr_RHS2 -= size_braket_RHS2;
                        /* ## recurrence relation along the z direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the z direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_bra_LHS=0; order_yz_bra_LHS<=order_bra_LHS; order_yz_bra_LHS++) {
                                /* ### recurrence relation along the z direction (bra)
                                       [x...xy...y] <= [x...xy...y] */
                                *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                                /* ### recurrence relation along the z direction (bra)
                                       [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                                for (order_z_bra_LHS=1; order_z_bra_LHS<=order_yz_bra_LHS; order_z_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_z_bra_LHS*(*ptr_RHS2);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS2++;
                                }
                            }
                        }
                    }
                }
                else if (cur_node->RHS_terms[2]!=NULL) {
                    /* pointer to the third RHS term */
                    ptr_RHS3 = &triangle_recur->work_array[cur_node->RHS_terms[2]->idx_wrk][cur_node->RHS_terms[2]->addr_wrk];
                    /* 1, 3, 4 RHS terms */
                    if (cur_node->RHS_terms[3]!=NULL) {
                        /* pointer to the fourth RHS term */
                        ptr_RHS4 = &triangle_recur->work_array[cur_node->RHS_terms[3]->idx_wrk][cur_node->RHS_terms[3]->addr_wrk];
                        /* size of the HGTOs (bra) of the LHS term */
                        size_bra_LHS = (order_bra_LHS+1)*(order_bra_LHS+2)/2;
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xx...x, xxz...z] <= [x...x, xz...z] */
                        for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<order_mm_RHS1; order_yz_mm_RHS1++) {
                            order_x_mm_RHS1 = order_mm_RHS1-order_yz_mm_RHS1;
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the x direction (ket)
                                      [x...x, xz...z] <= [x...x, xz...z] */
                                for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                    order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                    for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                        /* ### recurrence relation along the x direction (bra)
                                               [x...x, z...z] <= [x...x, z...z] */
                                        for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*(order_x_ket_LHS*(*ptr_RHS3)
                                                     + order_x_mm_RHS1*(*ptr_RHS4));
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS3++;
                                            ptr_RHS4++;
                                        }
                                    }
                                }
                                /* ## recurrence relation along the x direction (ket)
                                      [y...y, z...z] <= [y...y, z...z] */
                                for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_x_mm_RHS1*(*ptr_RHS4);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS4++;
                                    }
                                }
                            }
                        }
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xy...y, xz...z] <= [y...y, z...z] */
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the x direction (ket)
                                  [x...x, xz...z] <= [x...x, xz...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_x_ket_LHS*(*ptr_RHS3);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                    }
                                }
                            }
                            /* ## recurrence relation along the x direction (ket)
                                  [y...y, z...z] <= [y...y, z...z] */
                            for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                /* ### recurrence relation along the x direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yy...y, yyz...z] <= [y...y, yz...z] */
                        size_ket_RHS3 = order_ket_LHS*(order_ket_LHS+1)/2;
                        size_ket_LHS = size_ket_RHS3+order_ket_LHS+1;
                        size_braket_LHS = size_bra_LHS*size_ket_LHS;
                        size_yz_mm_RHS4 = order_mm_RHS1*size_braket_LHS;
                        ptr_RHS1 -= size_yz_mm_RHS4+size_braket_LHS;
                        ptr_RHS4 -= size_yz_mm_RHS4;
                        size_braket_RHS3 = size_ket_RHS3*size_bra_LHS;
                        ptr_RHS3 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS3;
                        for (order_y_mm_RHS1=order_mm_RHS1; order_y_mm_RHS1>0; order_y_mm_RHS1--) {
                            /* ## recurrence relation along the y direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*(order_y_ket_LHS*(*ptr_RHS3)
                                                 + order_y_mm_RHS1*(*ptr_RHS4));
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                        ptr_RHS4++;
                                    }
                                }
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xz...z] <= [x...xz...z] */
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_y_mm_RHS1*(*ptr_RHS4);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS4++;
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yz...z] <= [z...z] */
                        /* ## recurrence relation along the y direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                            /* ## recurrence relation along the y direction (ket)
                                  [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                            for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_y_ket_LHS*(*ptr_RHS3);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS3++;
                                }
                            }
                            /* ## recurrence relation along the y direction (ket)
                                  [x...xz...z] <= [x...xz...z] */
                            /* ### recurrence relation along the y direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                            }
                        }
                        /* # recurrence relation along the z direction (Cartesian multipole moments)
                             [zz...z] <= [z...z] */
                        ptr_RHS1 -= size_braket_LHS;
                        ptr_RHS4 -= size_braket_LHS;
                        ptr_RHS3 -= size_braket_RHS3;
                        /* ## recurrence relation along the z direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                            /* ## recurrence relation along the z direction (ket)
                                  [x...xy...y] <= [x...xy...y] */
                            /* ### recurrence relation along the z direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                         + hrp_total_expnt*order_mm_RHS1*(*ptr_RHS4);
                                ptr_LHS++;
                                ptr_RHS1++;
                                ptr_RHS4++;
                            }
                            /* ## recurrence relation along the z direction (ket)
                                  [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                            for (order_z_ket_LHS=1; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                /* ### recurrence relation along the z direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                             + hrp_total_expnt*(order_z_ket_LHS*(*ptr_RHS3)
                                             + order_mm_RHS1*(*ptr_RHS4));
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS3++;
                                    ptr_RHS4++;
                                }
                            }
                        }
                    }
                    /* 1, 3 RHS terms */
                    else {
                        /* size of the HGTOs (bra) of the LHS term */
                        size_bra_LHS = (order_bra_LHS+1)*(order_bra_LHS+2)/2;
                        /* # recurrence relation along the x direction (Cartesian multipole moments)
                             [xx...x, xz...z] <= [x...x, z...z] */
                        for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<=order_mm_RHS1; order_yz_mm_RHS1++) {
                            for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                                /* ## recurrence relation along the x direction (ket)
                                      [x...x, xz...z] <= [x...x, xz...z] */
                                for (order_yz_ket_LHS=0; order_yz_ket_LHS<order_ket_LHS; order_yz_ket_LHS++) {
                                    order_x_ket_LHS = order_ket_LHS-order_yz_ket_LHS;
                                    for (order_z_ket_LHS=0; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                        /* ### recurrence relation along the x direction (bra)
                                               [x...x, z...z] <= [x...x, z...z] */
                                        for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                            *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                                     + hrp_total_expnt*order_x_ket_LHS*(*ptr_RHS3);
                                            ptr_LHS++;
                                            ptr_RHS1++;
                                            ptr_RHS3++;
                                        }
                                    }
                                }
                                /* ## recurrence relation along the x direction (ket)
                                      [y...y, z...z] <= [y...y, z...z] */
                                for (order_z_ket_LHS=0; order_z_ket_LHS<=order_ket_LHS; order_z_ket_LHS++) {
                                    /* ### recurrence relation along the x direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                    }
                                }
                            }
                        }
                        /* # recurrence relation along the y direction (Cartesian multipole moments)
                             [yy...y, yz...z] <= [y...y, z...z] */
                        size_ket_RHS3 = order_ket_LHS*(order_ket_LHS+1)/2;
                        size_ket_LHS = size_ket_RHS3+order_ket_LHS+1;
                        size_braket_LHS = size_bra_LHS*size_ket_LHS;
                        ptr_RHS1 -= cur_node->idx_orders[idx_cart_mm]*size_braket_LHS;
                        size_braket_RHS3 = size_ket_RHS3*size_bra_LHS;
                        ptr_RHS3 -= cur_node->idx_orders[idx_cart_mm]*size_braket_RHS3;
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the y direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xy...y, x...xyz...z] <= [x...xy...y, x...xyz...z] */
                                for (order_y_ket_LHS=order_yz_ket_LHS; order_y_ket_LHS>0; order_y_ket_LHS--) {
                                    /* ### recurrence relation along the y direction (bra)
                                           [x...x, z...z] <= [x...x, z...z] */
                                    for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                        *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                                 + hrp_total_expnt*order_y_ket_LHS*(*ptr_RHS3);
                                        ptr_LHS++;
                                        ptr_RHS1++;
                                        ptr_RHS3++;
                                    }
                                }
                                /* ## recurrence relation along the y direction (ket)
                                      [x...xz...z] <= [x...xz...z] */
                                /* ### recurrence relation along the y direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                        }
                        /* # recurrence relation along the z direction (Cartesian multipole moments)
                             [zz...z] <= [z...z] */
                        ptr_RHS1 -= size_braket_LHS;
                        ptr_RHS3 -= size_braket_RHS3;
                        /* ## recurrence relation along the z direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (order_yz_ket_LHS=0; order_yz_ket_LHS<=order_ket_LHS; order_yz_ket_LHS++) {
                            /* ## recurrence relation along the z direction (ket)
                                  [x...xy...y] <= [x...xy...y] */
                            /* ### recurrence relation along the z direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                            }
                            /* ## recurrence relation along the z direction (ket)
                                  [x...xy...yz, x...xz...z] <= [x...xy...yz, x...xz...z] */
                            for (order_z_ket_LHS=1; order_z_ket_LHS<=order_yz_ket_LHS; order_z_ket_LHS++) {
                                /* ### recurrence relation along the z direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_z_ket_LHS*(*ptr_RHS3);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS3++;
                                }
                            }
                        }
                    }
                }
                /* 1, 4 RHS terms */
                else if (cur_node->RHS_terms[3]!=NULL) {
                    /* pointer to the fourth RHS term */
                    ptr_RHS4 = &triangle_recur->work_array[cur_node->RHS_terms[3]->idx_wrk][cur_node->RHS_terms[3]->addr_wrk];
                    /* sizes of the HGTOs of the LHS term */
                    size_bra_LHS = (order_bra_LHS+1)*(order_bra_LHS+2)/2;
                    size_ket_LHS = (order_ket_LHS+1)*(order_ket_LHS+2)/2;
                    /* # recurrence relation along the x direction (Cartesian multipole moments)
                         [xx...x, xxz...z] <= [x...x, xz...z] */
                    for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<order_mm_RHS1; order_yz_mm_RHS1++) {
                        order_x_mm_RHS1 = order_mm_RHS1-order_yz_mm_RHS1;
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the x direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                /* ### recurrence relation along the x direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1)
                                             + hrp_total_expnt*order_x_mm_RHS1*(*ptr_RHS4);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                    ptr_RHS4++;
                                }
                            }
                        }
                    }
                    /* # recurrence relation along the x direction (Cartesian multipole moments)
                         [xy...y, xz...z] <= [y...y, z...z] */
                    for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                        /* ## recurrence relation along the x direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the x direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                            }
                        }
                    }
                    /* # recurrence relation along the y direction (Cartesian multipole moments)
                         [yy...y, yyz...z] <= [y...y, yz...z] */
                    size_braket_LHS = size_bra_LHS*size_ket_LHS;
                    size_yz_mm_RHS4 = order_mm_RHS1*size_braket_LHS;
                    ptr_RHS1 -= size_yz_mm_RHS4+size_braket_LHS;
                    ptr_RHS4 -= size_yz_mm_RHS4;
                    for (order_y_mm_RHS1=order_mm_RHS1; order_y_mm_RHS1>0; order_y_mm_RHS1--) {
                        /* ## recurrence relation along the y direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the x direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1)
                                         + hrp_total_expnt*order_y_mm_RHS1*(*ptr_RHS4);
                                ptr_LHS++;
                                ptr_RHS1++;
                                ptr_RHS4++;
                            }
                        }
                    }
                    /* # recurrence relation along the y direction (Cartesian multipole moments)
                         [yz...z] <= [z...z] */
                    /* ## recurrence relation along the y direction (ket)
                          [x...x, z...z] <= [x...x, z...z] */
                    for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                        /* ### recurrence relation along the x direction (bra)
                               [x...x, z...z] <= [x...x, z...z] */
                        for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                            *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                            ptr_LHS++;
                            ptr_RHS1++;
                        }
                    }
                    /* # recurrence relation along the z direction (Cartesian multipole moments)
                         [zz...z] <= [z...z] */
                    ptr_RHS1 -= size_braket_LHS;
                    ptr_RHS4 -= size_braket_LHS;
                    /* ## recurrence relation along the z direction (ket)
                          [x...x, z...z] <= [x...x, z...z] */
                    for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                        /* ### recurrence relation along the x direction (bra)
                               [x...x, z...z] <= [x...x, z...z] */
                        for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                            *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1)
                                     + hrp_total_expnt*order_mm_RHS1*(*ptr_RHS4);
                            ptr_LHS++;
                            ptr_RHS1++;
                            ptr_RHS4++;
                        }
                    }
                }
                /* 1 RHS term */
                else {
                    /* sizes of the HGTOs of the LHS term */
                    size_bra_LHS = (order_bra_LHS+1)*(order_bra_LHS+2)/2;
                    size_ket_LHS = (order_ket_LHS+1)*(order_ket_LHS+2)/2;
                    /* # recurrence relation along the x direction (Cartesian multipole moments)
                         [xx...x, xz...z] <= [x...x, z...z] */
                    for (order_yz_mm_RHS1=0; order_yz_mm_RHS1<=order_mm_RHS1; order_yz_mm_RHS1++) {
                        for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_yz_mm_RHS1; order_z_mm_RHS1++) {
                            /* ## recurrence relation along the x direction (ket)
                                  [x...x, z...z] <= [x...x, z...z] */
                            for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                                /* ### recurrence relation along the x direction (bra)
                                       [x...x, z...z] <= [x...x, z...z] */
                                for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                    *ptr_LHS = cc_wrt_diporg[0]*(*ptr_RHS1);
                                    ptr_LHS++;
                                    ptr_RHS1++;
                                }
                            }
                        }
                    }
                    /* # recurrence relation along the y direction (Cartesian multipole moments)
                         [yy...y, yz...z] <= [y...y, z...z] */
                    size_braket_LHS = size_bra_LHS*size_ket_LHS;
                    ptr_RHS1 -= cur_node->idx_orders[idx_cart_mm]*size_braket_LHS;
                    for (order_z_mm_RHS1=0; order_z_mm_RHS1<=order_mm_RHS1; order_z_mm_RHS1++) {
                        /* ## recurrence relation along the y direction (ket)
                              [x...x, z...z] <= [x...x, z...z] */
                        for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                            /* ### recurrence relation along the x direction (bra)
                                   [x...x, z...z] <= [x...x, z...z] */
                            for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                                *ptr_LHS = cc_wrt_diporg[1]*(*ptr_RHS1);
                                ptr_LHS++;
                                ptr_RHS1++;
                            }
                        }
                    }
                    /* # recurrence relation along the z direction (Cartesian multipole moments)
                         [zz...z] <= [z...z] */
                    ptr_RHS1 -= size_braket_LHS;
                    /* ## recurrence relation along the z direction (ket)
                          [x...x, z...z] <= [x...x, z...z] */
                    for (inc_ket_LHS=0; inc_ket_LHS<size_ket_LHS; inc_ket_LHS++) {
                        /* ### recurrence relation along the x direction (bra)
                               [x...x, z...z] <= [x...x, z...z] */
                        for (inc_bra_LHS=0; inc_bra_LHS<size_bra_LHS; inc_bra_LHS++) {
                            *ptr_LHS = cc_wrt_diporg[2]*(*ptr_RHS1);
                            ptr_LHS++;
                            ptr_RHS1++;
                        }
                    }
                }
                /* moves to the next node */
                cur_node = cur_node->suc_LHS;
            }
        }
    }
    return GSUCCESS;
}
