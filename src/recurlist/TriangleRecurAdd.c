/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurAdd().

   2014-10-19, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"
#include "utilities/gen1int_arith.h"

GErrorCode TriangleRecurAdd(TriangleRecur *triangle_recur,
                            const GInt num_recur_indices,
                            const GInt *recur_indices,
                            const GInt output_idx,
                            const GInt num_RHS_terms,
                            const GInt *RHS_recur_orders)
{
    RecurDAG *cur_relation;        /* pointer to the current recurrence relation */
    RecurVertex *cur_vertex;       /* current vertex of the DAG */
    RecurVertex *new_vertex;       /* new vertex of the DAG */
    RecurVertex *prec_vertex;      /* preceding vertex */
    RecurVertex *suc_vertex;       /* succeeding vertex */
    GInt min_recur_order;          /* minimal recurrence-relation order of the output index */
    GInt max_recur_order;          /* maximal recurrence-relation order of the output index */
    GInt prec_recur;               /* recorder for the preceding recurrence relation */
    GInt min_order_output;         /* minimal order of the output index */
    GInt max_order_output;         /* maximal order of the output index */
    const GInt UNINIT_IDX_WRK=-1;  /* uninitialized index of work array */
    GInt idx_wrk_output;           /* index of reserved work array for the output of the
                                      current recurrence relation that will not be used by
                                      other vertices */
    GInt order_output_idx;         /* order of output index */
    GBool found_RHS;               /* indicates if found the vertex of a RHS term */
    GBool insert_vertex;           /* indicates if the vertex will be inserted into the DAG */
    GInt est_num_wrk;              /* estimated number of work arrays for the current
                                      recurrence relation */
    GInt num_used_wrk;             /* number of used work arrays for the current
                                      recurrence relation */
    GBool num_wrk_changed;         /* indicates if the number of work arrays is changed
                                      for all recurrence relations */
    GBool idx_wrk_changed;         /* indicates if the indices of work arrays have been changed */
    GInt size_mem_vertex;          /* size of memory used for a vertex */
    GInt iorder,jorder;            /* incremental recorders over orders */
    GInt ivtx;                     /* incremental recorder over vertices */
    GInt iterm,jterm;              /* incremental recorders over terms */
    GInt idx;                      /* incremental recorder over indices */
    /* checks the current recurrence relation */
    if (triangle_recur->cur_recur>=triangle_recur->num_recur) {
        printf("TriangleRecurAdd>> number of recurrence relations %"GINT_FMT"\n",
               triangle_recur->num_recur);
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        GErrorExit(FILE_AND_LINE, "too many recurrence relations to add");
    }
    cur_relation = &triangle_recur->relations[triangle_recur->cur_recur];
    cur_relation->num_recur_indices = num_recur_indices;
#if defined(GEN1INT_DEBUG)
    cur_relation->output_idx = output_idx;
    cur_relation->num_RHS_terms = num_RHS_terms;
#endif
    /* output index should be one of the indices involved in the recurrence relation */
    for (idx=0; idx<num_recur_indices; idx++) {
        if (output_idx==recur_indices[idx]) break;
    }
    if (idx==num_recur_indices) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        printf("TriangleRecurAdd>> output index %"GINT_FMT"\n", output_idx);
        printf("TriangleRecurAdd>> indices involved in the recurrence relation {");
        for (idx=0; idx<num_recur_indices-1; idx++) {
            printf("%"GINT_FMT",", recur_indices[idx]);
        }
        printf("%"GINT_FMT"}\n", recur_indices[num_recur_indices-1]);
        GErrorExit(FILE_AND_LINE, "output index not involved in the recurrence relation");
    }
    /* gets the minimal and maximal recurrence-relation orders of the
       output index, notice that \var{idx} is the position of the output
       index in the given \var{recur_indices} */
    min_recur_order = GINT_MIN;
    max_recur_order = GINT_MAX;
    for (iterm=0; iterm<num_RHS_terms; iterm++,idx+=num_recur_indices) {
        if (RHS_recur_orders[idx]>min_recur_order) {
            min_recur_order = RHS_recur_orders[idx];
        }
        if (RHS_recur_orders[idx]<max_recur_order) {
            max_recur_order = RHS_recur_orders[idx];
        }
    }
    min_recur_order = -min_recur_order;
    max_recur_order = -max_recur_order;
    if (min_recur_order<=0 || max_recur_order<=0) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        for (iterm=0,iorder=0; iterm<num_RHS_terms; iterm++) {
            printf("TriangleRecurAdd>> RHS term %"GINT_FMT" {", iterm);
            for (idx=0; idx<num_recur_indices-1; idx++,iorder++) {
                printf("%"GINT_FMT",", RHS_recur_orders[iorder]);
            }
            printf("%"GINT_FMT"}\n", RHS_recur_orders[num_recur_indices-1]);
        }
        printf("TriangleRecurAdd>> minimal recurrence-relation order of the output index %"GINT_FMT"\n",
               min_recur_order);
        printf("TriangleRecurAdd>> maximal recurrence-relation order of the output index %"GINT_FMT"\n",
               max_recur_order);
        GErrorExit(FILE_AND_LINE, "recurrence-relation order of the output index should be positive number");
    }
    /* computes the number of work arrays used for the LHS and RHS terms,
       +1 due to the LHS term */
    cur_relation->num_wrk_array = max_recur_order-min_recur_order+2;
    /* gets the minimal and maximal orders of the output index */
    prec_recur = triangle_recur->cur_recur-1;
    if (prec_recur<0) {
        min_order_output = triangle_recur->idx_orders[output_idx];
        max_order_output = min_order_output;
        /* we do not need to reserve any work array for the output of the
           first recurrence relation */
        idx_wrk_output = UNINIT_IDX_WRK;
    }
    else {
        /* root vertices of the current recurrence relation come from
           the leaf vertices of the preceding recurrence relation */
        cur_vertex = triangle_recur->relations[prec_recur].head_vtx[0]->suc_LHS;
        /* gets the index of the reserved work array for the output of the
           current recurrence relation, which is that of the leaf vertices
           of the preceding recurrence relation; also gets the corresponding
           minimal and maximal orders of the output index */
        idx_wrk_output = cur_vertex->idx_wrk;
        min_order_output = cur_vertex->idx_orders[output_idx];
        max_order_output = min_order_output;
        for (ivtx=1; ivtx<triangle_recur->relations[prec_recur].num_vtx[0]; ivtx++) {
            cur_vertex = cur_vertex->suc_LHS;
#if defined(GEN1INT_DEBUG)
            /* we require that the leaf vertices of each DAG use one same work array */
            if (idx_wrk_output!=cur_vertex->idx_wrk) {
                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                       triangle_recur->cur_recur);
                printf("TriangleRecurAdd>> index of reserved work array %"GINT_FMT"\n",
                       idx_wrk_output);
                printf("TriangleRecurAdd>> index of work array of vertex %"GINT_FMT" is %"GINT_FMT"\n",
                       ivtx,
                       cur_vertex->idx_wrk);
                GErrorExit(FILE_AND_LINE, "invalid index of work array of the vertex");
            }
#endif
            if (min_order_output>cur_vertex->idx_orders[output_idx]) {
                min_order_output = cur_vertex->idx_orders[output_idx];
            }
            else if (max_order_output<cur_vertex->idx_orders[output_idx]) {
                max_order_output = cur_vertex->idx_orders[output_idx];
            }
        }
        /* we do not need to reserve a work array for output with only one order */
        if (min_order_output==max_order_output) {
            idx_wrk_output = UNINIT_IDX_WRK;
        }
    }
    /* computes the number of output-index orders */
    cur_relation->num_output_orders = max_order_output/min_recur_order+1;
    /* we may need another work array to save results if more than one output-index order needed */
    if (cur_relation->num_output_orders>min_order_output/min_recur_order+cur_relation->num_wrk_array) {
        cur_relation->num_wrk_array++;
    }
    /* copies the indices involved in the recurrence relation */
    cur_relation->recur_indices = (GInt *)malloc(num_recur_indices*sizeof(GInt));
    if (cur_relation->recur_indices==NULL) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        printf("TriangleRecurAdd>> number of indices involved in the recurrence relation %"GINT_FMT"\n",
               num_recur_indices);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->recur_indices");
    }
    for (idx=0; idx<num_recur_indices; idx++) {
        cur_relation->recur_indices[idx] = recur_indices[idx];
    }
#if defined(GEN1INT_DEBUG)
    /* copies the orders of the RHS terms in the recurrence relation */
    cur_relation->RHS_recur_orders = (GInt *)malloc(num_RHS_terms*num_recur_indices*sizeof(GInt));
    if (cur_relation->RHS_recur_orders==NULL) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        printf("TriangleRecurAdd>> number of RHS terms %"GINT_FMT"\n",
               num_RHS_terms);
        printf("TriangleRecurAdd>> number of indices involved in the recurrence relation %"GINT_FMT"\n",
               num_recur_indices);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->RHS_recur_orders");
    }
    for (iterm=0,iorder=0; iterm<num_RHS_terms; iterm++) {
        for (idx=0; idx<num_recur_indices; idx++,iorder++) {
            cur_relation->RHS_recur_orders[iorder] = RHS_recur_orders[iorder];
        }
    }
#endif
    /* allocates memory for the number of vertices of each order */
    cur_relation->num_vtx = (GInt *)malloc(cur_relation->num_output_orders*sizeof(GInt));
    if (cur_relation->num_vtx==NULL) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        printf("TriangleRecurAdd>> number of output-index orders %"GINT_FMT"\n",
               cur_relation->num_output_orders);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->num_vtx");
    }
    /* allocates the head vertices of each order of the output index,
       which contain information of the use of work arrays */
    cur_relation->head_vtx = (RecurVertex **)malloc(cur_relation->num_output_orders*sizeof(RecurVertex *));
    if (cur_relation->head_vtx==NULL) {
        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        printf("TriangleRecurAdd>> number of output-index orders %"GINT_FMT"\n",
               cur_relation->num_output_orders);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->head_vtx");
    }
    /* initializes head vertices with lower-order output index */
    for (iorder=0; iorder<cur_relation->num_output_orders-1; iorder++) {
        cur_relation->num_vtx[iorder] = 0;
        cur_relation->head_vtx[iorder] = (RecurVertex *)malloc(sizeof(RecurVertex));
        if (cur_relation->head_vtx[iorder]==NULL) {
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            printf("TriangleRecurAdd>> current output index %"GINT_FMT"\n",
                   iorder);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->head_vtx[]");
        }
        /* the index of work array will be determined at the end of this function */
        cur_relation->head_vtx[iorder]->idx_wrk = UNINIT_IDX_WRK;
        /* for lower-order output index, we save the size of its integrals
           into \var{addr_wrk} of its head vertex; except for the first DAG,
           the size of output of the DAG is neglected because it was already
           counted as the input of the preceding DAG */
        cur_relation->head_vtx[iorder]->addr_wrk = 0;
        cur_relation->head_vtx[iorder]->idx_orders = NULL;
        cur_relation->head_vtx[iorder]->suc_LHS = NULL;
        cur_relation->head_vtx[iorder]->RHS_terms = NULL;
    }
    /* (1) inserts the left hand side terms into correct places */
    if (triangle_recur->cur_recur==0) {
        /* head vertex of the maximum order of the output index, notice that \var{iorder}
           equals to \var{cur_relation->num_output_orders}-1 */
        cur_relation->num_vtx[iorder] = 1;
        cur_relation->head_vtx[iorder] = (RecurVertex *)malloc(sizeof(RecurVertex));
        if (cur_relation->head_vtx[iorder]==NULL) {
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            printf("TriangleRecurAdd>> last output index %"GINT_FMT"\n",
                   iorder);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->head_vtx[]");
        }
        /* uses the first work array for the final results */
        cur_relation->head_vtx[iorder]->idx_wrk = 0;
        /* \var{triangle_recur->size_wrk_array} is the size of the final results,
           that was determined by the function TriangleRecurCreate() */
        cur_relation->head_vtx[iorder]->addr_wrk = triangle_recur->size_wrk_array;
        cur_relation->head_vtx[iorder]->idx_orders = NULL;
        cur_relation->head_vtx[iorder]->RHS_terms = NULL;
        /* creates the vertex for the final results */
        new_vertex = (RecurVertex *)malloc(sizeof(RecurVertex));
        if (new_vertex==NULL) {
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex");
        }
        /* uses the first work array for the final results, from the beginning of
           the work array */
        new_vertex->idx_wrk = 0;
        new_vertex->addr_wrk = 0;
        new_vertex->idx_orders = (GInt *)malloc(triangle_recur->num_indices*sizeof(GInt));
        if (new_vertex->idx_orders==NULL) {
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            printf("TriangleRecurAdd>> number of indices %"GINT_FMT"\n",
                   triangle_recur->num_indices);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->idx_orders");
        }
        for (idx=0; idx<triangle_recur->num_indices; idx++) {
            new_vertex->idx_orders[idx] = triangle_recur->idx_orders[idx];
        }
        new_vertex->suc_LHS = NULL;
        /* gets the order of the output index */
        order_output_idx = new_vertex->idx_orders[output_idx];
        /* initializes vertices for the RHS terms */
        if (order_output_idx>0) {
            new_vertex->num_RHS_terms = num_RHS_terms;
            new_vertex->RHS_terms = (RecurVertex **)malloc(num_RHS_terms*sizeof(RecurVertex *));
            if (new_vertex->RHS_terms==NULL) {
                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                       triangle_recur->cur_recur);
                printf("TriangleRecurAdd>> number of RHS terms %"GINT_FMT"\n",
                       num_RHS_terms);
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->RHS_terms");
            }
            for (jterm=0; jterm<num_RHS_terms; jterm++) {
                new_vertex->RHS_terms[jterm] = NULL;
            }
        }
        /* no recurrence relation will be performed for the zeroth-order output index */
        else {
            new_vertex->num_RHS_terms = 0;
            new_vertex->RHS_terms = NULL;
        }
        /* inserts the new vertex after the head vertex at the same order */
        cur_relation->head_vtx[iorder]->suc_LHS = new_vertex;
    }
    else {
        /* head vertex of the maximum order of the output index, notice that \var{iorder}
           equals to \var{cur_relation->num_output_orders}-1 */
        cur_relation->num_vtx[iorder] = 0;
        cur_relation->head_vtx[iorder] = (RecurVertex *)malloc(sizeof(RecurVertex));
        if (cur_relation->head_vtx[iorder]==NULL) {
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            printf("TriangleRecurAdd>> last output index %"GINT_FMT"\n",
                   iorder);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cur_relation->head_vtx[]");
        }
        /* root vertices (with maximum order of the output index) of the current DAG
           directly come from some leaf vertices of the preceding DAG instead of generating
           from the recurrence relation; so we simply copy the index of the work array
           used by the leaf vertices of the preceding DAG */
        cur_vertex = triangle_recur->relations[prec_recur].head_vtx[0];
        cur_relation->head_vtx[iorder]->idx_wrk = cur_vertex->idx_wrk;
        /* differently from lower-order output indices, we copy the size of integrals
           of the leaf vertices of the preceding DAG to the head vertex of the level
           of the maximum-order output index, this size may be used at the end to
           determine the use of work arrays by the lower-order output indices */
        cur_relation->head_vtx[iorder]->addr_wrk = cur_vertex->addr_wrk;
        cur_relation->head_vtx[iorder]->idx_orders = NULL;
        cur_relation->head_vtx[iorder]->suc_LHS = NULL;
        cur_relation->head_vtx[iorder]->RHS_terms = NULL;
        /* loops over the leaf vertices of the preceding recurrence relation */
        for (ivtx=0; ivtx<triangle_recur->relations[prec_recur].num_vtx[0]; ivtx++) {
            /* moves to the next leaf vertex, which is the output of the
               current recurrence relation */
            cur_vertex = cur_vertex->suc_LHS;
            /* we have to copy the current leaf vertex to another vertex of the
               current recurrence relation (including the use of work array and
               orders of indices), because the vertex of the current recurrence
               relation could have another different succeeding LHS term and
               different RHS terms */
            new_vertex = (RecurVertex *)malloc(sizeof(RecurVertex));
            if (new_vertex==NULL) {
                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                       triangle_recur->cur_recur);
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex");
            }
            new_vertex->idx_wrk = cur_vertex->idx_wrk;
            new_vertex->addr_wrk = cur_vertex->addr_wrk;
            new_vertex->idx_orders = (GInt *)malloc(triangle_recur->num_indices*sizeof(GInt));
            if (new_vertex->idx_orders==NULL) {
                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                       triangle_recur->cur_recur);
                printf("TriangleRecurAdd>> number of indices %"GINT_FMT"\n",
                       triangle_recur->num_indices);
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->idx_orders");
            }
            for (idx=0; idx<triangle_recur->num_indices; idx++) {
                new_vertex->idx_orders[idx] = cur_vertex->idx_orders[idx];
            }
            new_vertex->suc_LHS = NULL;
            /* gets the order of the output index */
            order_output_idx = new_vertex->idx_orders[output_idx];
            /* initializes vertices for the RHS terms */
            if (order_output_idx>0) {
                new_vertex->num_RHS_terms = num_RHS_terms;
                new_vertex->RHS_terms = (RecurVertex **)malloc(num_RHS_terms*sizeof(RecurVertex *));
                if (new_vertex->RHS_terms==NULL) {
                    printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                           triangle_recur->cur_recur);
                    printf("TriangleRecurAdd>> number of RHS terms %"GINT_FMT"\n",
                           num_RHS_terms);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->RHS_terms");
                }
                for (jterm=0; jterm<num_RHS_terms; jterm++) {
                    new_vertex->RHS_terms[jterm] = NULL;
                }
            }
            /* no recurrence relation will be performed for the zeroth-order output index */
            else {
                new_vertex->num_RHS_terms = 0;
                new_vertex->RHS_terms = NULL;
            }
            /* the level at which the new vertex is */
            jorder = order_output_idx/min_recur_order;
            /* inserts the new vertex at the tail of the linked list of its order */
            prec_vertex = cur_relation->head_vtx[jorder];
            while (prec_vertex->suc_LHS!=NULL) {
                prec_vertex = prec_vertex->suc_LHS;
            }
            prec_vertex->suc_LHS = new_vertex;
            /* updates the number of vertices */
            cur_relation->num_vtx[jorder]++;
        }
        /* output of the current recurrence relation contains zeroth-order output index */
        if (cur_relation->num_vtx[0]>0) {
            /* we require that all the leaf vertices of each DAG use one same work array,
               and we will use the same work array for the leaf vertices of the current
               and preceding DAGs, and size of work array could be increased in case that
               there are new RHS terms generated with zeroth-order output index */
            cur_vertex = triangle_recur->relations[prec_recur].head_vtx[0];
            cur_relation->head_vtx[0]->idx_wrk = cur_vertex->idx_wrk;
            cur_relation->head_vtx[0]->addr_wrk = cur_vertex->addr_wrk;
        }
    }
    /* (2) generates the DAG, from the maximum order of the output index down to 1 */
    for (ivtx=cur_relation->num_output_orders-2; ivtx>=0; ivtx--) {
        cur_vertex = cur_relation->head_vtx[ivtx+1]->suc_LHS;
        while (cur_vertex!=NULL) {
            /* loops over RHS terms for the current vertex */
            for (iterm=0,iorder=0; iterm<num_RHS_terms; iterm++) {
                found_RHS = GTRUE;
                /* computes orders of indices of the current RHS term,
                   and saves them temporarily in \var{triangle_recur->idx_orders} */
                for (idx=0; idx<triangle_recur->num_indices; idx++) {
                    triangle_recur->idx_orders[idx] = cur_vertex->idx_orders[idx];
                }
                for (idx=0; idx<num_recur_indices; idx++) {
                    triangle_recur->idx_orders[recur_indices[idx]] += RHS_recur_orders[iorder++];
                    /* term with negative order does not contribute */
                    if (triangle_recur->idx_orders[recur_indices[idx]]<0) {
                        found_RHS = GFALSE;
                        iorder += num_recur_indices-(idx+1);  /* makes sure pointing to the correct place */
                        break;
                    }
                }
                /* found a contributed RHS term to the recurrence relation, tries to find
                   it in the DAG; if not, creates a new vertex and inserts */
                if (found_RHS==GTRUE) {
                    /* gets the order of the output index */
                    order_output_idx = triangle_recur->idx_orders[output_idx];
                    /* the level at which the RHS term will be */
                    jorder = order_output_idx/min_recur_order;
                    /* loops over vertices with the same output-index order of the RHS term */
                    prec_vertex = cur_relation->head_vtx[jorder];
                    suc_vertex = prec_vertex->suc_LHS;
                    while (suc_vertex!=NULL) {
                        found_RHS = GTRUE;
                        insert_vertex = GFALSE;
                        for (idx=0; idx<triangle_recur->num_indices; idx++) {
                            /* vertices with the same output-index order are sorted
                               by lexicographical order on the orders of their indices,
                               so we do not need to compare the orders of all indices */
                            if (triangle_recur->idx_orders[idx]<suc_vertex->idx_orders[idx]) {
                                insert_vertex = GTRUE;
                                break;
                            }
                            else if (triangle_recur->idx_orders[idx]>suc_vertex->idx_orders[idx]) {
                                found_RHS = GFALSE;
                                break;
                            }
                        }
                        /* creates a new vertex for the RHS term and inserts it before \var{suc_vertex} */
                        if (insert_vertex==GTRUE) {
                            new_vertex = (RecurVertex *)malloc(sizeof(RecurVertex));
                            if (new_vertex==NULL) {
                                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                       triangle_recur->cur_recur);
                                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex");
                            }
                            new_vertex->idx_wrk = cur_relation->head_vtx[jorder]->idx_wrk;
                            new_vertex->addr_wrk = cur_relation->head_vtx[jorder]->addr_wrk;
                            new_vertex->idx_orders = (GInt *)malloc(triangle_recur->num_indices*sizeof(GInt));
                            if (new_vertex->idx_orders==NULL) {
                                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                       triangle_recur->cur_recur);
                                printf("TriangleRecurAdd>> number of indices %"GINT_FMT"\n",
                                       triangle_recur->num_indices);
                                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->idx_orders");
                            }
                            size_mem_vertex = 1;
                            for (idx=0; idx<triangle_recur->num_indices; idx++) {
                                new_vertex->idx_orders[idx] = triangle_recur->idx_orders[idx];
                                if (triangle_recur->idx_orders[idx]==0) continue;
                                /* gets the size of memory used for the new vertex */
                                switch (triangle_recur->idx_types[idx]) {
                                case RECUR_IDX_SCALAR:
                                    size_mem_vertex *= new_vertex->idx_orders[idx];
                                    break;
                                case RECUR_IDX_CARTESIAN:
                                    size_mem_vertex *= (new_vertex->idx_orders[idx]+1)
                                                     * (new_vertex->idx_orders[idx]+2)/2;
                                    break;
                                case RECUR_IDX_SPHERICAL:
                                    size_mem_vertex *= (2*new_vertex->idx_orders[idx]+1);
                                    break;
                                default:
                                    printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                           triangle_recur->cur_recur);
                                    printf("TriangleRecurAdd>> type of index %"GINT_FMT" of recurrence relations: %d\n",
                                           idx,
                                           triangle_recur->idx_types[idx]);
                                    GErrorExit(FILE_AND_LINE, "invalid type");
                                }
                            }
                            /* updates the size of work array used for the current level */
                            cur_relation->head_vtx[jorder]->addr_wrk += size_mem_vertex;
                            new_vertex->suc_LHS = suc_vertex;
                            /* initializes RHS terms of the new vertex */
                            if (order_output_idx>0) {
                                new_vertex->num_RHS_terms = num_RHS_terms;
                                new_vertex->RHS_terms = (RecurVertex **)malloc(num_RHS_terms*sizeof(RecurVertex *));
                                if (new_vertex->RHS_terms==NULL) {
                                    printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                           triangle_recur->cur_recur);
                                    printf("TriangleRecurAdd>> number of RHS terms %"GINT_FMT"\n",
                                           num_RHS_terms);
                                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->RHS_terms");
                                }
                                for (jterm=0; jterm<num_RHS_terms; jterm++) {
                                    new_vertex->RHS_terms[jterm] = NULL;
                                }
                            }
                            /* no recurrence relation will be performed for the zeroth-order output index */
                            else {
                                new_vertex->num_RHS_terms = 0;
                                new_vertex->RHS_terms = NULL;
                            }
                            /* inserts the new vertex */
                            prec_vertex->suc_LHS = new_vertex;
                            /* points to the new vertex for the RHS term */
                            cur_vertex->RHS_terms[iterm] = new_vertex;
                            /* updates the number of vertices for this level */
                            cur_relation->num_vtx[jorder]++;
                            break;
                        }
                        /* \var{suc_vertex} is the vertex for the RHS term */
                        else if (found_RHS==GTRUE) {
                            cur_vertex->RHS_terms[iterm] = suc_vertex;
                            break;
                        }
                        /* moves to the next sibling vertex of this level of the DAG */
                        prec_vertex = suc_vertex;
                        suc_vertex = prec_vertex->suc_LHS;
                    }
                    /* we did not find a vertex for the RHS term, so we create a new vertex
                       for it and inserts it at the tail of this level of the DAG */
                    if (suc_vertex==NULL) {
                        new_vertex = (RecurVertex *)malloc(sizeof(RecurVertex));
                        if (new_vertex==NULL) {
                            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                   triangle_recur->cur_recur);
                            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex");
                        }
                        new_vertex->idx_wrk = cur_relation->head_vtx[jorder]->idx_wrk;
                        new_vertex->addr_wrk = cur_relation->head_vtx[jorder]->addr_wrk;
                        new_vertex->idx_orders = (GInt *)malloc(triangle_recur->num_indices*sizeof(GInt));
                        if (new_vertex->idx_orders==NULL) {
                            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                   triangle_recur->cur_recur);
                            printf("TriangleRecurAdd>> number of indices %"GINT_FMT"\n",
                                   triangle_recur->num_indices);
                            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->idx_orders");
                        }
                        size_mem_vertex = 1;
                        for (idx=0; idx<triangle_recur->num_indices; idx++) {
                            new_vertex->idx_orders[idx] = triangle_recur->idx_orders[idx];
                            if (triangle_recur->idx_orders[idx]==0) continue;
                            /* gets the size of memory used for the new vertex */
                            switch (triangle_recur->idx_types[idx]) {
                            case RECUR_IDX_SCALAR:
                                size_mem_vertex *= new_vertex->idx_orders[idx];
                                break;
                            case RECUR_IDX_CARTESIAN:
                                size_mem_vertex *= (new_vertex->idx_orders[idx]+1)
                                                 * (new_vertex->idx_orders[idx]+2)/2;
                                break;
                            case RECUR_IDX_SPHERICAL:
                                size_mem_vertex *= (2*new_vertex->idx_orders[idx]+1);
                                break;
                            default:
                                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                       triangle_recur->cur_recur);
                                printf("TriangleRecurAdd>> type of index %"GINT_FMT" of recurrence relations: %d\n",
                                       idx,
                                       triangle_recur->idx_types[idx]);
                                GErrorExit(FILE_AND_LINE, "invalid type");
                            }
                        }
                        /* updates the size of work array used for the current level */
                        cur_relation->head_vtx[jorder]->addr_wrk += size_mem_vertex;
                        new_vertex->suc_LHS = NULL;
                        /* initializes RHS terms of the new vertex */
                        if (order_output_idx>0) {
                            new_vertex->num_RHS_terms = num_RHS_terms;
                            new_vertex->RHS_terms = (RecurVertex **)malloc(num_RHS_terms*sizeof(RecurVertex *));
                            if (new_vertex->RHS_terms==NULL) {
                                printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                       triangle_recur->cur_recur);
                                printf("TriangleRecurAdd>> number of RHS terms %"GINT_FMT"\n",
                                       num_RHS_terms);
                                GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_vertex->RHS_terms");
                            }
                            for (jterm=0; jterm<num_RHS_terms; jterm++) {
                                new_vertex->RHS_terms[jterm] = NULL;
                            }
                        }
                        /* no recurrence relation will be performed for the zeroth-order output index */
                        else {
                            new_vertex->num_RHS_terms = 0;
                            new_vertex->RHS_terms = NULL;
                        }
                        /* inserts the new vertex */
                        prec_vertex->suc_LHS = new_vertex;
                        /* points to the new vertex for the RHS term */
                        cur_vertex->RHS_terms[iterm] = new_vertex;
                        /* updates the number of vertices for this level */
                        cur_relation->num_vtx[jorder]++;
                    }
                }
            }
            /* moves to next vertex of the current order of output index */
            cur_vertex = cur_vertex->suc_LHS;
        }
        /* updates the size of work arrays for all recurrence relations */
        if (cur_relation->head_vtx[ivtx]->addr_wrk>triangle_recur->size_wrk_array) {
#if defined(GEN1INT_DEBUG)
            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                   triangle_recur->cur_recur);
            printf("TriangleRecurAdd>> level of output index %"GINT_FMT"\n",
                   ivtx);
            printf("TriangleRecurAdd>> new size of work array %"GINT_FMT"\n",
                   cur_relation->head_vtx[ivtx]->addr_wrk);
#endif
            triangle_recur->size_wrk_array = cur_relation->head_vtx[ivtx]->addr_wrk;
        }
    }
    /* (3) sets the work arrays */
    /* sets the estimated number of work arrays for the current recurrence relation */
    est_num_wrk = cur_relation->num_wrk_array;
    /* more work arrays may be needed for the current recurrence relation */
    if (triangle_recur->num_wrk_array<cur_relation->num_wrk_array) {
        cur_relation->num_wrk_array = triangle_recur->num_wrk_array;
        /* changes the number of work arrays for all recurrence relations */
        triangle_recur->num_wrk_array = est_num_wrk;
        num_wrk_changed = GTRUE;
    }
    else {
        num_wrk_changed = GFALSE;
    }
    /* computes the number of used work arrays, because integrals at different
       levels can be put into one work array if the work array is big enough,
       and that we can reduce the number of used work arrays */
    num_used_wrk = 1;
    iorder = cur_relation->num_output_orders-2;
    jorder = iorder+1;
    for (; iorder>0; iorder--,jorder--) {
        /* integrals of lower-order output index (\var{iorder}) can use
           the same work array as that of integrals of higher-order output
           index (\var{jorder}) */
        if (cur_relation->head_vtx[jorder]->idx_wrk!=idx_wrk_output &&
            triangle_recur->size_wrk_array>=cur_relation->head_vtx[iorder]->addr_wrk
                                           +cur_relation->head_vtx[jorder]->addr_wrk) {
            cur_relation->head_vtx[iorder]->idx_wrk = cur_relation->head_vtx[jorder]->idx_wrk;
            /* now the head vertex contains the size of the used size of
               work array for the current level, and the address of each
               vertex will be added by the offset \var{size_mem_vertex} */
            size_mem_vertex = cur_relation->head_vtx[jorder]->addr_wrk;
            cur_relation->head_vtx[iorder]->addr_wrk += size_mem_vertex;
            /* updates the indices and addresses of vertices of the current level */
            cur_vertex = cur_relation->head_vtx[iorder];
            for (ivtx=0; ivtx<cur_relation->num_vtx[iorder]; ivtx++) {
                cur_vertex = cur_vertex->suc_LHS;
                if (cur_vertex->idx_wrk==UNINIT_IDX_WRK) {
                    cur_vertex->idx_wrk = cur_relation->head_vtx[iorder]->idx_wrk;
                    cur_vertex->addr_wrk += size_mem_vertex;
                }
            }
        }       
        /* integrals of lower-order output index (\var{iorder}) will be put into
           another work array, and their addresses will not be changed */
        else {      
            /* we use the work array in a cyclic order */
            cur_relation->head_vtx[iorder]->idx_wrk = cur_relation->head_vtx[jorder]->idx_wrk+1;
            if (cur_relation->head_vtx[iorder]->idx_wrk>=triangle_recur->num_wrk_array) {
                cur_relation->head_vtx[iorder]->idx_wrk -= est_num_wrk;
            }
            if (cur_relation->head_vtx[iorder]->idx_wrk==idx_wrk_output) {
                cur_relation->head_vtx[iorder]->idx_wrk++;
            }
            /* updates the indices of vertices of the current level */
            cur_vertex = cur_relation->head_vtx[iorder];
            for (ivtx=0; ivtx<cur_relation->num_vtx[iorder]; ivtx++) {
                cur_vertex = cur_vertex->suc_LHS;
                if (cur_vertex->idx_wrk==UNINIT_IDX_WRK) {
                    cur_vertex->idx_wrk = cur_relation->head_vtx[iorder]->idx_wrk;
                }
            }
            /* updates the number of used work arrays */
            num_used_wrk++;
        }
    }
    /* sets the indices and addresses of vertices with the zeroth-order
       output index if they do not use the same work array as that of
       the leaf vertices of the preceding DAG */
    if (cur_relation->head_vtx[0]->idx_wrk==UNINIT_IDX_WRK) {
        /* integrals of the zeroth-order output index can use the same
           work array as that of integrals of the first-order output index */
        if (cur_relation->head_vtx[1]->idx_wrk!=idx_wrk_output &&
            triangle_recur->size_wrk_array>=cur_relation->head_vtx[0]->addr_wrk
                                           +cur_relation->head_vtx[1]->addr_wrk) {
            cur_relation->head_vtx[0]->idx_wrk = cur_relation->head_vtx[1]->idx_wrk;
            /* now the head vertex contains the size of the used size of
               work array for the current level, and the address of each
               vertex will be added by the offset \var{size_mem_vertex} */
            size_mem_vertex = cur_relation->head_vtx[1]->addr_wrk;
            cur_relation->head_vtx[0]->addr_wrk += size_mem_vertex;
            /* updates the indices and addresses of vertices of the current level */
            cur_vertex = cur_relation->head_vtx[0];
            for (ivtx=0; ivtx<cur_relation->num_vtx[0]; ivtx++) {
                cur_vertex = cur_vertex->suc_LHS;
                if (cur_vertex->idx_wrk==UNINIT_IDX_WRK) {
                    cur_vertex->idx_wrk = cur_relation->head_vtx[0]->idx_wrk;
                    cur_vertex->addr_wrk += size_mem_vertex;
                }
            }
        }
        /* integrals of the zeroth-order output index will be put into
           another work array, and their addresses will not be changed */
        else {
            cur_relation->head_vtx[0]->idx_wrk = cur_relation->head_vtx[1]->idx_wrk+1;
            if (cur_relation->head_vtx[0]->idx_wrk>=triangle_recur->num_wrk_array) {
                cur_relation->head_vtx[0]->idx_wrk -= est_num_wrk;
            }
            if (cur_relation->head_vtx[0]->idx_wrk==idx_wrk_output) {
                cur_relation->head_vtx[0]->idx_wrk++;
            }
            /* updates the indices of vertices of the current level */
            cur_vertex = cur_relation->head_vtx[0];
            for (ivtx=0; ivtx<cur_relation->num_vtx[0]; ivtx++) {
                cur_vertex = cur_vertex->suc_LHS;
                if (cur_vertex->idx_wrk==UNINIT_IDX_WRK) {
                    cur_vertex->idx_wrk = cur_relation->head_vtx[0]->idx_wrk;
                }
            }
            /* updates the number of used work arrays */
            num_used_wrk++;
        }
    }
    /* the number of work arrays for all recurrence relations has been changed,
       we are going to check if we need to change it back */
    if (num_wrk_changed==GTRUE) {
        /* the number of used work arrays for the current recurrence relation
           becomes less, we will therefore change the number of work arrays
           for all recurrence relations back */
        if (num_used_wrk<triangle_recur->num_wrk_array) {
            /* resets the number of work arrays for all recurrence relations,
               where \var{cur_relation->num_wrk_array} is the previous number
               of work arrays for all recurrence relations */
            triangle_recur->num_wrk_array = GMax(cur_relation->num_wrk_array, num_used_wrk);
            /* indicates the indices of work arrays have not been changed */
            idx_wrk_changed = GFALSE;
            /* changes the indices of work arrays used by the current DAG if needed,
               the zeroth-order output index will be treated differently */
            for (iorder=cur_relation->num_output_orders-2; iorder>0; iorder--) {
                /* previously, we set the number of work arrays as \var{est_num_wrk},
                   and they may be used as in a cyclic order:
                   [\var{idx_wrk_output}, \var{idx_wrk_output}+1, ..., \var{est_num_wrk}-1,  (*)
                    0, 1, ..., \var{num_used_wrk}-\var{est_num_wrk}+\var{idx_wrk_output}-1], (**)
                   where we should have indices at:
                   (*) <= \var{idx_wrk_output}+\var{num_used_wrk}-1,                             (a)
                   (**) < \var{idx_wrk_output}-1 (because \var{num_used_wrk}<\var{est_num_wrk}); (b)

                   now there are max(\var{num_used_wrk},\var{cur_relation->num_wrk_array})
                   work arrays, and the current recurrence relation needs
                   \var{num_used_wrk} work arrays; so we change the index \var{idx_wrk}
                   and its following indices of lower level to (in a cyclic order):
                   (1) \var{idx_wrk}-\var{num_used_wrk} if it is >= \var{triangle_recur->num_wrk_array},
                   (2) \var{idx_wrk}+\var{est_num_wrk}-\var{num_used_wrk} otherwise;

                   because only indices at (*) could be >= \var{triangle_recur->num_wrk_array},
                   and their new indices (1) will be <= \var{idx_wrk_output}-1 because (a);
                   for indices at (**), their new indices (2) will be <= \var{idx_wrk_output}-1;
                   for other indices at (*) and their new indices (2) will be
                   > \var{idx_wrk_output}, because \var{est_num_wrk}-\var{num_used_wrk}>0;

                   so none of new indices will become \var{idx_wrk_output} if they were not */
                if (cur_relation->head_vtx[iorder]->idx_wrk==triangle_recur->num_wrk_array) {
                    /* changes indices of work arrays except for the zeroth-order output index */
                    for (jorder=iorder; jorder>0; jorder--) {
                        if (cur_relation->head_vtx[jorder]->idx_wrk>=triangle_recur->num_wrk_array) {
                            cur_relation->head_vtx[jorder]->idx_wrk -= num_used_wrk;
                        }
                        else {
                            cur_relation->head_vtx[jorder]->idx_wrk += est_num_wrk-num_used_wrk;
                        }
#if defined(GEN1INT_DEBUG)
                        if (cur_relation->head_vtx[jorder]->idx_wrk==idx_wrk_output) {
                            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                   triangle_recur->cur_recur);
                            printf("TriangleRecurAdd>> level of output index %"GINT_FMT"\n",
                                   jorder);
                            printf("TriangleRecurAdd>> number of work arrays %"GINT_FMT"\n",
                                   triangle_recur->num_wrk_array);
                            printf("TriangleRecurAdd>> number of used work arrays %"GINT_FMT"\n",
                                   num_used_wrk);
                            printf("TriangleRecurAdd>> estimated number of work arrays %"GINT_FMT"\n",
                                   est_num_wrk);
                            printf("TriangleRecurAdd>> index of reserved work array %"GINT_FMT"\n",
                                   idx_wrk_output);
                            GErrorExit(FILE_AND_LINE, "invalid number of used work arrays");
                        }
#endif
                        /* loops over vertices of the current order of output index */
                        cur_vertex = cur_relation->head_vtx[jorder];
                        for (ivtx=0; ivtx<cur_relation->num_vtx[jorder]; ivtx++) {
                            cur_vertex = cur_vertex->suc_LHS;
                            /* output of the current recurrence relation uses the reserved work array */
                            if (cur_vertex->idx_wrk==idx_wrk_output) {
                                continue;
                            }
                            else {
                                cur_vertex->idx_wrk = cur_relation->head_vtx[jorder]->idx_wrk;
                            }
                        }
                    }
                    /* changes the index of work array for the zeroth-order output index
                       if there is no output at this level */
                    if (cur_relation->head_vtx[0]->idx_wrk!=idx_wrk_output) {
                        if (cur_relation->head_vtx[0]->idx_wrk>=triangle_recur->num_wrk_array) {
                            cur_relation->head_vtx[0]->idx_wrk -= num_used_wrk;
                        }
                        else {
                            cur_relation->head_vtx[0]->idx_wrk += est_num_wrk-num_used_wrk;
                        }
#if defined(GEN1INT_DEBUG)
                        if (cur_relation->head_vtx[0]->idx_wrk==idx_wrk_output) {
                            printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                                   triangle_recur->cur_recur);
                            printf("TriangleRecurAdd>> zeroth-order output index\n");
                            printf("TriangleRecurAdd>> number of work arrays %"GINT_FMT"\n",
                                   triangle_recur->num_wrk_array);
                            printf("TriangleRecurAdd>> number of used work arrays %"GINT_FMT"\n",
                                   num_used_wrk);
                            printf("TriangleRecurAdd>> estimated number of work arrays %"GINT_FMT"\n",
                                   est_num_wrk);
                            printf("TriangleRecurAdd>> index of reserved work array %"GINT_FMT"\n",
                                   idx_wrk_output);
                            GErrorExit(FILE_AND_LINE, "invalid number of used work arrays");
                        }
#endif
                        /* loops over vertices of the zeroth-order output index */
                        cur_vertex = cur_relation->head_vtx[0];
                        for (ivtx=0; ivtx<cur_relation->num_vtx[0]; ivtx++) {
                            cur_vertex = cur_vertex->suc_LHS;
                            /* there is no output at this level */
                            cur_vertex->idx_wrk = cur_relation->head_vtx[0]->idx_wrk;
                        }
                    }
                    /* indicates the indices of work arrays have been changed */
                    idx_wrk_changed = GTRUE;
                    break;
                }
            }
            /* indices of work arrays used by higher-order output indices have not
               been changed; we check if we need to change the index of work array
               used by the zeroth-order output index (if there is no output at this
               level) */
            if (idx_wrk_changed==GFALSE) {
                /* notice that if the index of work array used by the level of the
                   zeroth-order output index is \var{idx_wrk_output}, then it is
                   < \var{triangle_recur->num_wrk_array} */
                if (cur_relation->head_vtx[0]->idx_wrk>=triangle_recur->num_wrk_array) {
                    cur_relation->head_vtx[0]->idx_wrk -= num_used_wrk;
#if defined(GEN1INT_DEBUG)
                    if (cur_relation->head_vtx[0]->idx_wrk==idx_wrk_output) {
                        printf("TriangleRecurAdd>> current recurrence relation %"GINT_FMT"\n",
                               triangle_recur->cur_recur);
                        printf("TriangleRecurAdd>> zeroth-order output index\n");
                        printf("TriangleRecurAdd>> number of work arrays %"GINT_FMT"\n",
                               triangle_recur->num_wrk_array);
                        printf("TriangleRecurAdd>> number of used work arrays %"GINT_FMT"\n",
                               num_used_wrk);
                        printf("TriangleRecurAdd>> estimated number of work arrays %"GINT_FMT"\n",
                               est_num_wrk);
                        printf("TriangleRecurAdd>> index of reserved work array %"GINT_FMT"\n",
                               idx_wrk_output);
                        GErrorExit(FILE_AND_LINE, "invalid number of used work arrays");
                    }
#endif
                    /* loops over vertices of the zeroth-order output index */
                    cur_vertex = cur_relation->head_vtx[0];
                    for (ivtx=0; ivtx<cur_relation->num_vtx[0]; ivtx++) {
                        cur_vertex = cur_vertex->suc_LHS;
                        /* there is no output at this level */
                        cur_vertex->idx_wrk = cur_relation->head_vtx[0]->idx_wrk;
                    }
                }
            }
            /* resets the number of work arrays for the current DAG */
            cur_relation->num_wrk_array = num_used_wrk;
        }
        /* the number of used work arrays for the current recurrence relation
           is unchanged, we will therefore only reset the number of work arrays
           for the current DAG */
        else {
            cur_relation->num_wrk_array = triangle_recur->num_wrk_array;
        }
    }
    /* the number of work arrays for all recurrence relations has not been
       changed, we only need to reset the number of work arrays for the
       current DAG if needed */
    else {
        if (num_used_wrk<cur_relation->num_wrk_array) {
            cur_relation->num_wrk_array = num_used_wrk;
        }
    }
    /* updates the recorder for the current recurrence relation */
    triangle_recur->cur_recur++;
    return GSUCCESS;
}
