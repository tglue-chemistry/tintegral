/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleCartMMHGTOBra().

   2014-11-09, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"
#include "utilities/gen1int_arith.h"

GErrorCode TriangleCartMMHGTOBra(TriangleRecur *triangle_recur,
                                 const GReal scal_const,
                                 const GInt order_elec_deriv,
                                 const GReal coord_bra[3],
                                 const GReal exponent_bra,
                                 const GReal coord_ket[3],
                                 const GReal exponent_ket)
{
    RecurDAG *cur_relation;  /* current recurrence relation */
    RecurVertex *cur_node;          /* current triangle node */
    GReal recip_total_expnt;         /* reciprocal of the total exponent*/
    GReal reduced_expnt;             /* reduced exponent */
    GReal dist_bra_ket;              /* distance between the coordinates of bra and ket centers */
    GReal sq_bra_ket;                /* square of distance between bra and ket centers */
    GReal cc_wrt_bra[3];             /* relative coordinates of center-of-charge w.r.t. the bra center */
    GReal param_RHS2;                /* parameter used for the second RHS term of the recurrence relation */
    GInt idx_geo_bra;                /* index of HGTOs/geometric derivatives on the bra center */
    GInt iorder;                     /* incremental recorder over orders */
    GReal *ptr_LHS;                  /* pointer to the value of LHS */
    GReal *ptr_RHS1;                 /* pointer to the value of the first RHS term */
    GReal *ptr_RHS2;                 /* pointer to the value of the second RHS term */
    GInt order_RHS1;                 /* order of the first RHS term */
    GInt order_yz_RHS1;              /* order of yz components of the first RHS term */
    GInt order_x_RHS1;               /* order of x component of the first RHS term */
    GInt order_z_RHS1;               /* order of z component of the first RHS term */
    GInt order_y_RHS1;               /* order of y component of the first RHS term */
    /* computes the reciprocal of the total exponent */
    recip_total_expnt = 1.0/(exponent_bra+exponent_ket);
    /* computes the reduced exponent */
    reduced_expnt = exponent_bra*exponent_ket*recip_total_expnt;
    /* computes the square of distance between bra and ket centers */
    sq_bra_ket = 0;
    for (iorder=0; iorder<3; iorder++) {
        dist_bra_ket = coord_ket[iorder]-coord_bra[iorder];
        sq_bra_ket += dist_bra_ket*dist_bra_ket;
    }
    /* HGTO (bra) uses the last recurrence relation */
    cur_relation = &triangle_recur->relations[triangle_recur->num_recur-1];
    /* 0th order of output index */
    cur_node = cur_relation->head_vtx[0]->suc_LHS;
    if (order_elec_deriv==0) {
        triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk]
          = scal_const*exp(-reduced_expnt*sq_bra_ket)*pow(M_PI*recip_total_expnt, 1.5);
    }
    else if (order_elec_deriv==1) {
        triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk]
          = -scal_const*(exponent_ket+exponent_ket)
          * exp(-reduced_expnt*sq_bra_ket)*pow(M_PI*recip_total_expnt, 1.5);
    }
    else {
        triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk]
          = scal_const*pow(-exponent_ket-exponent_ket, order_elec_deriv)
          * exp(-reduced_expnt*sq_bra_ket)*pow(M_PI*recip_total_expnt, 1.5);
    }
    /* 1st and high orders of output index */
    if (cur_relation->num_output_orders>1) {
        /* computes \${-\frac{exponent_ket}{exponent_bra+exponent_ket}} */
        param_RHS2 = -exponent_ket*recip_total_expnt;
        /* computes the relative coordinates of center-of-charge w.r.t. the bra center */
        for (iorder=0; iorder<3; iorder++) {
            cc_wrt_bra[iorder] = param_RHS2*(coord_bra[iorder]-coord_ket[iorder]);
        }
        /* 1st order of output index */
        cur_node = cur_relation->head_vtx[1]->suc_LHS;
        ptr_LHS = &triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk];
        ptr_RHS1 = &triangle_recur->work_array[cur_node->RHS_terms[0]->idx_wrk][cur_node->RHS_terms[0]->addr_wrk];
        *ptr_LHS = cc_wrt_bra[0]*(*ptr_RHS1);
        *(++ptr_LHS) = cc_wrt_bra[1]*(*ptr_RHS1);
        *(++ptr_LHS) = cc_wrt_bra[2]*(*ptr_RHS1);
        /* high orders of output index */
        if (cur_relation->num_output_orders>2) {
            /* computes the parameter used for the second RHS term of the recurrence relation */
            param_RHS2 *= 0.5/exponent_bra;
            /* gets the index of HGTOs/geometric derivatives on the bra center */
            idx_geo_bra = triangle_recur->num_indices-2;
            for (iorder=2; iorder<cur_relation->num_output_orders; iorder++) {
                cur_node = cur_relation->head_vtx[iorder]->suc_LHS;
                /* gets the order of HGTO (bra) of the first RHS term */
                order_RHS1 = cur_node->idx_orders[idx_geo_bra]-1;
                /* # recurrence relation along the x direction
                     [xx...x, xxz...z] <= [x...x, xz...z] */
                ptr_LHS = &triangle_recur->work_array[cur_node->idx_wrk][cur_node->addr_wrk];
                ptr_RHS1 = &triangle_recur->work_array[cur_node->RHS_terms[0]->idx_wrk][cur_node->RHS_terms[0]->addr_wrk];
                ptr_RHS2 = &triangle_recur->work_array[cur_node->RHS_terms[1]->idx_wrk][cur_node->RHS_terms[1]->addr_wrk];
                for (order_yz_RHS1=0; order_yz_RHS1<order_RHS1; order_yz_RHS1++) {
                    order_x_RHS1 = order_RHS1-order_yz_RHS1;
                    for (order_z_RHS1=0; order_z_RHS1<=order_yz_RHS1; order_z_RHS1++) {
                        *ptr_LHS = cc_wrt_bra[0]*(*ptr_RHS1)+order_x_RHS1*param_RHS2*(*ptr_RHS2);
                        ptr_LHS++;
                        ptr_RHS1++;
                        ptr_RHS2++;
                    }
                }
                /* # recurrence relation along the x direction
                     [xy...y, xz...z] <= [y...y, z...z] */
                for (order_z_RHS1=0; order_z_RHS1<=order_RHS1; order_z_RHS1++) {
                    *ptr_LHS = cc_wrt_bra[0]*(*ptr_RHS1);
                    ptr_LHS++;
                    ptr_RHS1++;
                }
                /* # recurrence relation along the y direction
                     [yy...y, yyz...z] <= [y...y, yz...z] */
                ptr_RHS1 -= cur_node->idx_orders[idx_geo_bra];
                ptr_RHS2 -= order_RHS1;
                for (order_y_RHS1=order_RHS1; order_y_RHS1>0; order_y_RHS1--) {
                    *ptr_LHS = cc_wrt_bra[1]*(*ptr_RHS1)+order_y_RHS1*param_RHS2*(*ptr_RHS2);
                    ptr_LHS++;
                    ptr_RHS1++;
                    ptr_RHS2++;
                }
                /* # recurrence relation along the y direction
                     [yz...z] <= [z...z] */
                *ptr_LHS = cc_wrt_bra[1]*(*ptr_RHS1);
                ptr_LHS++;
                /* # recurrence relation along the z direction
                     [zz...z] <= [z...z] */
                ptr_RHS2--;
                *ptr_LHS = cc_wrt_bra[2]*(*ptr_RHS1)+order_RHS1*param_RHS2*(*ptr_RHS2);
            }
        }
    }
    return GSUCCESS;
}
