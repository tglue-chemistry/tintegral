/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleOrderLShift().

   2014-10-01, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

/*% \brief shifts order of input values left
    \author Bin Gao
    \date 2014-10-01
    \param{GInt:int}{in}{order_left} order of the left triangle
    \param{GInt:int}{in}{order_right} order of the right triangle to be shifted
    \param{GInt:int}{in}{order_shift} order to be shifted
    \param{GInt:int}{in}{size_outermost} size of the outermost dimension
    \param{GInt:int}{in}{size_interv} size of the dimension between the right and left triangles
    \param{GInt:int}{in}{size_innermost} size of the innermost dimension
    \param{GReal:real}{in}{val_inp} input values in the shape of
        [size_outermost][order_left][size_interv][order_right+order_shift][size_innermost]
    \param{GReal:real}{out}{val_out} output values in the shape of
        [size_outermost][order_left+order_shift][size_interv][order_right][size_innermost]
    \return{GErrorCode:int} error information
*/
GErrorCode TriangleOrderLShift(const GInt order_left,
                               const GInt order_right,
                               const GInt order_shift,
                               const GInt size_outermost,
                               const GInt size_interv,
                               const GInt size_innermost,
                               GReal *val_inp,
                               GReal *val_out)
{
    GInt order_right_shift;  /* sum of orders of right triangle and shifted */
    GInt offset_outer;       /* offset of the outermost dimension */
    GInt offset_left;        /* offset of the components of left triangle */
    GInt offset_inter;       /* offset of the dimension between the right and left triangles */
    GInt offset_x_shift;     /* offset due to skip the components x...x -> xz...z of order shifted */
    GInt offset_y_shift;     /* offset due to skip the components x...x -> yz...z of order shifted */
    GInt offset_yz_right;    /* offset of right triangle along yz direction */
    GReal *ptr_out;          /* pointer to the output values */
    GReal *ptr_outer;        /* pointer to the outermost dimension */
    GReal *ptr_left;         /* pointer to the components of left triangle */
    GReal *ptr_inter;        /* pointer to the dimension between the right and left triangles */
    GReal *ptr_shift;        /* pointer to the components of order shifted */
    GReal *ptr_right;        /* pointer to the right triangle */
    GInt inc_outer;          /* incremental recorder over the outermost dimension */
    GInt inc_inter;          /* incremental recorder over the dimension between the right and left triangles */
    GInt inc_inner;          /* incremental recorder over the innermost dimension */
    GInt order_yz_left;      /* order of the yz components of left triangle */
    GInt order_z_left;       /* order of the z component of left triangle */
    GInt order_yz_shift;     /* order of the yz components of shifted order */
    GInt order_z_shift;      /* order of the z component of shifted order */
    GInt order_yz_right;     /* order of the yz components of right triangle */
    GInt order_z_right;      /* order of the z component of right triangle */
    /* offset of the dimension between the right and left triangles */
    order_right_shift = order_right+order_shift;
    offset_inter = (order_right_shift+1)*(order_right_shift+2)/2*size_innermost;
    /* offset of the components of left triangle */
    offset_left = size_interv*offset_inter;
    /* offset of the outermost dimension */
    offset_outer = (order_left+1)*(order_left+2)/2*offset_left;
    /* offset due to skip the components x...x -> xz...z of order shifted */
    offset_x_shift = order_shift*(order_shift+1)/2*size_innermost;
    /* offset due to skip the components x...x -> yz...z of order shifted */
    offset_y_shift = offset_x_shift+order_shift*size_innermost;
    /* pointer to the output values */
    ptr_out = val_out;
    /* pointer to the outermost dimension */
    ptr_outer = val_inp;
    /* loops over the outermost dimension */
    for (inc_outer=0; inc_outer<size_outermost; inc_outer++) {
        /* component x...x of order of left trianlge */
        ptr_left = ptr_outer;
        /* pointer to the components of order shifted */
        ptr_shift = ptr_left;
        /* offset of order of right triangle along yz direction */
        offset_yz_right = 0;
        /* all components of order shifted */
        for (order_yz_shift=0; order_yz_shift<=order_shift; order_yz_shift++) {
            for (order_z_shift=0; order_z_shift<=order_yz_shift; order_z_shift++) {
                /* pointer to the dimension between the right and left triangles */
                ptr_inter = ptr_shift;
                /* loops over the dimension between the right and left triangles */
                for (inc_inter=0; inc_inter<size_interv; inc_inter++) {
                    /* pointer to the right triangle */
                    ptr_right = ptr_inter;
                    /* all components of right triangle */
                    for (order_yz_right=0; order_yz_right<=order_right; order_yz_right++) {
                        for (order_z_right=0; order_z_right<=order_yz_right; order_z_right++) {
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                        ptr_right += offset_yz_right;
                    }
                    ptr_inter += offset_inter;
                }
                /* moves to the next component of order shifted */
                ptr_shift += size_innermost;
            }
            offset_yz_right += size_innermost;
        }
        /* for components y...y -> z...z of order shifted, the offset of right triangle
           along yz direction is order_shift*size_innermost */
        offset_yz_right = order_shift*size_innermost;
        /* other components of left triangle */
        for (order_yz_left=1; order_yz_left<=order_left; order_yz_left++) {
            /* component x..xy...y of left triangle */
            /* moves to the next component of left triangle */
            ptr_left += offset_left;
            /* skips the components x...x -> xz...z of order shifted */
            ptr_shift = ptr_left+offset_x_shift;
            for (order_z_shift=0; order_z_shift<=order_shift; order_z_shift++) {
                /* pointer to the dimension between the right and left triangles */
                ptr_inter = ptr_shift;
                /* loops over the dimension between the right and left triangles */
                for (inc_inter=0; inc_inter<size_interv; inc_inter++) {
                    /* pointer to the right triangle */
                    ptr_right = ptr_inter;
                    /* all components of right triangle */
                    for (order_yz_right=0; order_yz_right<=order_right; order_yz_right++) {
                        for (order_z_right=0; order_z_right<=order_yz_right; order_z_right++) {
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                        ptr_right += offset_yz_right;
                    }
                    ptr_inter += offset_inter;
                }
                /* moves to the next component of order shifted */
                ptr_shift += size_innermost;
            }
            /* components x...xy...z -> x...xz...z of left triangle */
            for (order_z_left=1; order_z_left<=order_yz_left; order_z_left++) {
                /* moves to the next component of left triangle */
                ptr_left += offset_left;
                /* skips the components x...x -> yz...z of order shifted */
                ptr_shift = ptr_left+offset_y_shift;
                /* pointer to the dimension between the right and left triangles */
                ptr_inter = ptr_shift;
                /* loops over the dimension between the right and left triangles */
                for (inc_inter=0; inc_inter<size_interv; inc_inter++) {
                    /* pointer to the right triangle */
                    ptr_right = ptr_inter;
                    /* all components of right triangle */
                    for (order_yz_right=0; order_yz_right<=order_right; order_yz_right++) {
                        for (order_z_right=0; order_z_right<=order_yz_right; order_z_right++) {
                            /* loops over the innermost dimension */
                            for (inc_inner=0; inc_inner<size_innermost; inc_inner++) {
                                /* assigns the output value */
                                *ptr_out = *ptr_right;
                                ptr_out++;
                                ptr_right++;
                            }
                        }
                        ptr_right += offset_yz_right;
                    }
                    ptr_inter += offset_inter;
                }
            }
        }
        ptr_outer += offset_outer;
    }
    return GSUCCESS;
}
