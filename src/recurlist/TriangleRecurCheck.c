/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function TriangleRecurCheck().

   2014-11-18, Bin Gao:
   * first version
*/

#include "triangle/gen1int_triangle.h"

GErrorCode TriangleRecurCheck(const TriangleRecur *triangle_recur,
                              const GInt which_recur,
                              const GInt num_RHS_terms,
                              const GInt *RHS_recur_orders)
{
    RecurDAG *cur_relation;   /* pointer to the current recurrence relation */
    RecurVertex *cur_vertex;  /* current vertex of the DAG */
    GBool found_RHS;          /* indicates if found the vertex of a RHS term */
    GBool check_failed;       /* indicates if the checking failed */
    GBool check_wrk;          /* indicates if checking the work array */
    GInt size_vtx_work;       /* size of work array for a vertex */
    GInt iorder,jorder;       /* incremental recorders over orders */
    GInt ivtx;                /* incremental recorder over vertices */
    GInt iterm,jterm;         /* incremental recorders over terms */
    GInt idx,jdx;             /* incremental recorders over indices */
    /* points to the current recurrence relation */
    if (which_recur>=triangle_recur->cur_recur || which_recur<0) {
        printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
               which_recur);
        printf("TriangleRecurCheck>> number of added recurrence relations %"GINT_FMT"\n",
               triangle_recur->cur_recur);
        GErrorExit(FILE_AND_LINE, "invalid recurrence relation to check");
    }
    cur_relation = &triangle_recur->relations[which_recur];
    /* loops over different orders of output index */
    for (iorder=cur_relation->num_output_orders-1; iorder>=0; iorder--) {
        cur_vertex = cur_relation->head_vtx[iorder]->suc_LHS;
        /* loops over different vertices */
        for (ivtx=0; ivtx<cur_relation->num_vtx[iorder]; ivtx++) {
            /* loops over RHS terms */
            for (iterm=0,jorder=0; iterm<num_RHS_terms; iterm++) {
                found_RHS = GTRUE;
                /* computes the orders of the indices of the RHS term */
                for (idx=0; idx<triangle_recur->num_indices; idx++) {
                    triangle_recur->idx_orders[idx] = cur_vertex->idx_orders[idx];
                }
                for (idx=0; idx<cur_relation->num_recur_indices; idx++,jorder++) {
                    triangle_recur->idx_orders[cur_relation->recur_indices[idx]] += RHS_recur_orders[jorder];
                    /* term with negative order does not contribute */
                    if (triangle_recur->idx_orders[cur_relation->recur_indices[idx]]<0) found_RHS = GFALSE;
                }
                /* checks the RHS term */
                check_failed = GTRUE;
                check_wrk = GFALSE;
                if (found_RHS==GTRUE) {
                    if (cur_vertex->RHS_terms!=NULL) {
                        if (cur_vertex->RHS_terms[iterm]!=NULL) {
                            check_failed = GFALSE;
                            check_wrk = GTRUE;
                        }
                    }
                }
                else {
                    if (cur_vertex->RHS_terms==NULL) {
                        check_failed = GFALSE;
                    }
                    else {
                        if (cur_vertex->RHS_terms[iterm]==NULL) check_failed = GFALSE;
                    }
                }
                /* checking failed */
                if (check_failed==GTRUE) {
                    printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                           which_recur);
                    printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                           iorder);
                    printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                           ivtx);
                    printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                           iterm);
                    printf("TriangleRecurCheck>> orders {");
                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                        printf("%"GINT_FMT",", cur_vertex->idx_orders[idx]);
                    }
                    printf("%"GINT_FMT"}\n",
                           cur_vertex->idx_orders[triangle_recur->num_indices-1]);
                    printf("TriangleRecurCheck>> RHS {");
                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                        printf("%"GINT_FMT",", triangle_recur->idx_orders[idx]);
                    }
                    printf("%"GINT_FMT"}\n",
                           triangle_recur->idx_orders[triangle_recur->num_indices-1]);
                    if (cur_vertex->RHS_terms!=NULL) {
                        if (cur_vertex->RHS_terms[iterm]!=NULL) {
                            printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                            for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                printf("%"GINT_FMT",",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                        }
                    }
                    GErrorExit(FILE_AND_LINE, "checking RHS term failed");
                }
                /* there is a RHS term, checks its orders of indices and use of work arrays */
                else if (check_wrk==GTRUE) {
                    /* compares the orders of indices */
                    for (idx=0; idx<triangle_recur->num_indices; idx++) {
                        if (triangle_recur->idx_orders[idx]!=cur_vertex->RHS_terms[iterm]->idx_orders[idx]) {
                            printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                   which_recur);
                            printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                   iorder);
                            printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                   ivtx);
                            printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                   iterm);
                            printf("TriangleRecurCheck>> orders {");
                            for (jdx=0; jdx<triangle_recur->num_indices-1; jdx++) {
                                printf("%"GINT_FMT",", cur_vertex->idx_orders[jdx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   cur_vertex->idx_orders[triangle_recur->num_indices-1]);
                            printf("TriangleRecurCheck>> RHS {");
                            for (jdx=0; jdx<triangle_recur->num_indices-1; jdx++) {
                                printf("%"GINT_FMT",", triangle_recur->idx_orders[jdx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   triangle_recur->idx_orders[triangle_recur->num_indices-1]);
                            printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                            for (jdx=0; jdx<triangle_recur->num_indices-1; jdx++) {
                                printf("%"GINT_FMT",",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[jdx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                            GErrorExit(FILE_AND_LINE, "checking orders failed");
                        }
                    }
                    /* checks the index and address of work arrays */
                    if (cur_vertex->idx_wrk==cur_vertex->RHS_terms[iterm]->idx_wrk) {
                        if (cur_vertex->addr_wrk<cur_vertex->RHS_terms[iterm]->addr_wrk) {
                            /* gets the size of memory used for the current vertex */
                            size_vtx_work = 1;
                            for (idx=0; idx<triangle_recur->num_indices; idx++) {
                                if (cur_vertex->idx_orders[idx]==0) continue;
                                switch (triangle_recur->idx_types[idx]) {
                                case RECUR_IDX_SCALAR:
                                    size_vtx_work *= cur_vertex->idx_orders[idx];
                                    break;
                                case RECUR_IDX_CARTESIAN:
                                    size_vtx_work *= (cur_vertex->idx_orders[idx]+1)
                                                   * (cur_vertex->idx_orders[idx]+2)/2;
                                    break;
                                case RECUR_IDX_SPHERICAL:
                                    size_vtx_work *= (2*cur_vertex->idx_orders[idx]+1);
                                    break;
                                default:
                                    printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                           which_recur);
                                    printf("TriangleRecurCheck>> order %"GINT_FMT"\n", 
                                           iorder);
                                    printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                           ivtx);
                                    printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                           iterm);
                                    printf("TriangleRecurCheck>> type of index %"GINT_FMT": %d\n",
                                           idx,
                                           triangle_recur->idx_types[idx]);
                                    GErrorExit(FILE_AND_LINE, "invalid type");
                                }
                            }
                            if (cur_vertex->addr_wrk+size_vtx_work>cur_vertex->RHS_terms[iterm]->addr_wrk) {
                                printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                       which_recur);
                                printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                       iorder);
                                printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                       ivtx);
                                printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                       iterm);
                                printf("TriangleRecurCheck>> orders {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",", cur_vertex->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",",
                                           cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->idx_wrk,
                                       cur_vertex->addr_wrk);
                                printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->RHS_terms[iterm]->idx_wrk,
                                       cur_vertex->RHS_terms[iterm]->addr_wrk);
                                GErrorExit(FILE_AND_LINE, "incorrect index and address");
                            }
                        }
                        else if (cur_vertex->addr_wrk==cur_vertex->RHS_terms[iterm]->addr_wrk) {
                            printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                   which_recur);
                            printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                   iorder);
                            printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                   ivtx);
                            printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                   iterm);
                            printf("TriangleRecurCheck>> orders {");
                            for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                printf("%"GINT_FMT",", cur_vertex->idx_orders[idx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   cur_vertex->idx_orders[triangle_recur->num_indices-1]);
                            printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                            for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                printf("%"GINT_FMT",",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                            }
                            printf("%"GINT_FMT"}\n",
                                   cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                            printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                   cur_vertex->idx_wrk,
                                   cur_vertex->addr_wrk);
                            printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                   cur_vertex->RHS_terms[iterm]->idx_wrk,
                                   cur_vertex->RHS_terms[iterm]->addr_wrk);
                            GErrorExit(FILE_AND_LINE, "incorrect index and address");
                        }
                        else {
                            /* gets the size of memory used for the current vertex */
                            size_vtx_work = 1;
                            for (idx=0; idx<triangle_recur->num_indices; idx++) {
                                if (cur_vertex->RHS_terms[iterm]->idx_orders[idx]==0) continue;
                                switch (triangle_recur->idx_types[idx]) {
                                case RECUR_IDX_SCALAR:
                                    size_vtx_work *= cur_vertex->RHS_terms[iterm]->idx_orders[idx];
                                    break;
                                case RECUR_IDX_CARTESIAN:
                                    size_vtx_work *= (cur_vertex->RHS_terms[iterm]->idx_orders[idx]+1)
                                                   * (cur_vertex->RHS_terms[iterm]->idx_orders[idx]+2)/2;
                                    break;
                                case RECUR_IDX_SPHERICAL:
                                    size_vtx_work *= (2*cur_vertex->RHS_terms[iterm]->idx_orders[idx]+1);
                                    break;
                                default:
                                    printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                           which_recur);
                                    printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                           iorder);
                                    printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                           ivtx);
                                    printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                           iterm);
                                    printf("TriangleRecurCheck>> type of index %"GINT_FMT": %d\n",
                                           idx,
                                           triangle_recur->idx_types[idx]);
                                    GErrorExit(FILE_AND_LINE, "invalid type");
                                }
                            }
                            if (cur_vertex->addr_wrk<cur_vertex->RHS_terms[iterm]->addr_wrk+size_vtx_work) {
                                printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                       which_recur);
                                printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                       iorder);
                                printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                       ivtx);
                                printf("TriangleRecurCheck>> RHS %"GINT_FMT"\n",
                                       iterm);
                                printf("TriangleRecurCheck>> orders {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",", cur_vertex->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",",
                                           cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->idx_wrk,
                                       cur_vertex->addr_wrk);
                                printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->RHS_terms[iterm]->idx_wrk,
                                       cur_vertex->RHS_terms[iterm]->addr_wrk);
                                GErrorExit(FILE_AND_LINE, "incorrect index and address");
                            }
                        }
                    }
                    /* compares with other RHS terms */
                    for (jterm=0; jterm<iterm; jterm++) {
                        if (cur_vertex->RHS_terms[jterm]==NULL) continue;
                        /* checks the index and address of work arrays */
                        if (cur_vertex->RHS_terms[jterm]->idx_wrk==cur_vertex->RHS_terms[iterm]->idx_wrk) {
                            if (cur_vertex->RHS_terms[jterm]->addr_wrk<cur_vertex->RHS_terms[iterm]->addr_wrk) {
                                /* gets the size of memory used for the current vertex */
                                size_vtx_work = 1;
                                for (idx=0; idx<triangle_recur->num_indices; idx++) {
                                    if (cur_vertex->RHS_terms[jterm]->idx_orders[idx]==0) continue;
                                    switch (triangle_recur->idx_types[idx]) {
                                    case RECUR_IDX_SCALAR:
                                        size_vtx_work *= cur_vertex->RHS_terms[jterm]->idx_orders[idx];
                                        break;
                                    case RECUR_IDX_CARTESIAN:
                                        size_vtx_work *= (cur_vertex->RHS_terms[jterm]->idx_orders[idx]+1)
                                                       * (cur_vertex->RHS_terms[jterm]->idx_orders[idx]+2)/2;
                                        break;
                                    case RECUR_IDX_SPHERICAL:
                                        size_vtx_work *= (2*cur_vertex->RHS_terms[jterm]->idx_orders[idx]+1);
                                        break;
                                    default:
                                        printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                               which_recur);
                                        printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                               iorder);
                                        printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                               ivtx);
                                        printf("TriangleRecurCheck>> RHS %"GINT_FMT",%"GINT_FMT"\n",
                                               iterm,
                                               jterm);
                                        printf("TriangleRecurCheck>> type of index %"GINT_FMT": %d\n",
                                               idx,
                                               triangle_recur->idx_types[idx]);
                                        GErrorExit(FILE_AND_LINE, "invalid type");
                                    }
                                }
                                if (cur_vertex->RHS_terms[jterm]->addr_wrk+size_vtx_work>cur_vertex->RHS_terms[iterm]->addr_wrk) {
                                    printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                           which_recur);
                                    printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                           iorder);
                                    printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                           ivtx);
                                    printf("TriangleRecurCheck>> RHS %"GINT_FMT",%"GINT_FMT"\n",
                                           iterm,
                                           jterm);
                                    printf("TriangleRecurCheck>> orders {");
                                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                        printf("%"GINT_FMT",", cur_vertex->RHS_terms[jterm]->idx_orders[idx]);
                                    }
                                    printf("%"GINT_FMT"}\n",
                                           cur_vertex->RHS_terms[jterm]->idx_orders[triangle_recur->num_indices-1]);
                                    printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                        printf("%"GINT_FMT",",
                                               cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                                    }
                                    printf("%"GINT_FMT"}\n",
                                           cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                                    printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                           cur_vertex->RHS_terms[jterm]->idx_wrk,
                                           cur_vertex->RHS_terms[jterm]->addr_wrk);
                                    printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                           cur_vertex->RHS_terms[iterm]->idx_wrk,
                                           cur_vertex->RHS_terms[iterm]->addr_wrk);
                                    GErrorExit(FILE_AND_LINE, "incorrect index and address");
                                }
                            }
                            else if (cur_vertex->RHS_terms[jterm]->addr_wrk==cur_vertex->RHS_terms[iterm]->addr_wrk) {
                                printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                       which_recur);
                                printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                       iorder);
                                printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                       ivtx);
                                printf("TriangleRecurCheck>> RHS %"GINT_FMT",%"GINT_FMT"\n",
                                       iterm,
                                       jterm);
                                printf("TriangleRecurCheck>> orders {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",", cur_vertex->RHS_terms[jterm]->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->RHS_terms[jterm]->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                                for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                    printf("%"GINT_FMT",",
                                           cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                                }
                                printf("%"GINT_FMT"}\n",
                                       cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                                printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->RHS_terms[jterm]->idx_wrk,
                                       cur_vertex->RHS_terms[jterm]->addr_wrk);
                                printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                       cur_vertex->RHS_terms[iterm]->idx_wrk,
                                       cur_vertex->RHS_terms[iterm]->addr_wrk);
                                GErrorExit(FILE_AND_LINE, "incorrect index and address");
                            }
                            else {
                                /* gets the size of memory used for the current vertex */
                                size_vtx_work = 1;
                                for (idx=0; idx<triangle_recur->num_indices; idx++) {
                                    if (cur_vertex->RHS_terms[iterm]->idx_orders[idx]==0) continue;
                                    switch (triangle_recur->idx_types[idx]) {
                                    case RECUR_IDX_SCALAR:
                                        size_vtx_work *= cur_vertex->RHS_terms[iterm]->idx_orders[idx];
                                        break;
                                    case RECUR_IDX_CARTESIAN:
                                        size_vtx_work *= (cur_vertex->RHS_terms[iterm]->idx_orders[idx]+1)
                                                       * (cur_vertex->RHS_terms[iterm]->idx_orders[idx]+2)/2;
                                        break;
                                    case RECUR_IDX_SPHERICAL:
                                        size_vtx_work *= (2*cur_vertex->RHS_terms[iterm]->idx_orders[idx]+1);
                                        break;
                                    default:
                                        printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                               which_recur);
                                        printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                               iorder);
                                        printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                               ivtx);
                                        printf("TriangleRecurCheck>> RHS %"GINT_FMT",%"GINT_FMT"\n",
                                               iterm,
                                               jterm);
                                        printf("TriangleRecurCheck>> type of index %"GINT_FMT": %d\n",
                                               idx,
                                               triangle_recur->idx_types[idx]);
                                        GErrorExit(FILE_AND_LINE, "invalid type");
                                    }
                                }
                                if (cur_vertex->RHS_terms[jterm]->addr_wrk<cur_vertex->RHS_terms[iterm]->addr_wrk+size_vtx_work) {
                                    printf("TriangleRecurCheck>> recurrence relation to check %"GINT_FMT"\n",
                                           which_recur);
                                    printf("TriangleRecurCheck>> order %"GINT_FMT"\n",
                                           iorder);
                                    printf("TriangleRecurCheck>> vertex %"GINT_FMT"\n",
                                           ivtx);
                                    printf("TriangleRecurCheck>> RHS %"GINT_FMT",%"GINT_FMT"\n",
                                           iterm,
                                           jterm);
                                    printf("TriangleRecurCheck>> orders {");
                                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                        printf("%"GINT_FMT",", cur_vertex->RHS_terms[jterm]->idx_orders[idx]);
                                    }
                                    printf("%"GINT_FMT"}\n",
                                           cur_vertex->RHS_terms[jterm]->idx_orders[triangle_recur->num_indices-1]);
                                    printf("TriangleRecurCheck>> RHS from TriangleRecurAdd() {");
                                    for (idx=0; idx<triangle_recur->num_indices-1; idx++) {
                                        printf("%"GINT_FMT",",
                                               cur_vertex->RHS_terms[iterm]->idx_orders[idx]);
                                    }
                                    printf("%"GINT_FMT"}\n",
                                           cur_vertex->RHS_terms[iterm]->idx_orders[triangle_recur->num_indices-1]);
                                    printf("TriangleRecurCheck>> vertex work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                           cur_vertex->RHS_terms[jterm]->idx_wrk,
                                           cur_vertex->RHS_terms[jterm]->addr_wrk);
                                    printf("TriangleRecurCheck>> RHS work array [%"GINT_FMT"][%"GINT_FMT"]\n",
                                           cur_vertex->RHS_terms[iterm]->idx_wrk,
                                           cur_vertex->RHS_terms[iterm]->addr_wrk);
                                    GErrorExit(FILE_AND_LINE, "incorrect index and address");
                                }
                            }
                        }
                    }
                }
            }
            /* moves to the succeeding LHS term */
            cur_vertex = cur_vertex->suc_LHS;
        }
    }
    return GSUCCESS;
}
