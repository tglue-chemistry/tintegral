/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoGetComposition().

   2014-09-29, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief gets the multinomial coefficient and orders of derivatives on
        the integrand centers of the current composition
    \author Bin Gao
    \date 2014-09-29
    \param[PartGeo:struct]{in} part_geo the context of partial geometric derivatives
    \param[GULong:unsigned long}{out} mult_coef the multinomial coefficient
    \param[GInt:int]{out} order_integrand_cent orders of derivatives w.r.t. the centers
        of the integrand (operator and basis sets)
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoGetComposition(PartGeo *part_geo,
                                 GULong *mult_coef,
                                 GInt *order_integrand_cent)
{
    GULong mult_exp_coef;  /* multinomial coefficient of geometric derivatives on each center */
    GInt ptr_idx;          /* pointer to the index of the center */
    GInt icent;            /* incremental recorder over centers */
    GInt inum;             /* incremental recorder over number of centers */
    GErrorCode ierr;       /* error information */
    /* initializes */
    if (part_geo->trans_invariance==GTRUE) {
        *mult_coef = part_geo->trans_mult_coef[part_geo->iter_trans];
    }
    else {
        *mult_coef = 1;
    }
    for (icent=0; icent<part_geo->num_integrand_cent; icent++) {
        order_integrand_cent[icent] = 0;
    }
    /* loops over differentiated centers */
    for (icent=0,ptr_idx=0; icent<part_geo->geo_num_cent; icent++) {
        /* gets the orders of derivatives of current composition */
        ierr = MultExpGetTerm(&part_geo->mult_exp[part_geo->iter_trans][icent],
                              part_geo->iter_mult_exp[icent],
                              &mult_exp_coef,
                              part_geo->mult_exp_order);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpGetTerm");
        *mult_coef *= mult_exp_coef;
        for (inum=0; inum<part_geo->geo_num_repeated[icent]; inum++,ptr_idx++) {
            order_integrand_cent[part_geo->geo_idx_integrand[ptr_idx]] += part_geo->mult_exp_order[inum];
        }
    }
    /* sets the orders of derivatives from multinomial expansion */
    for (icent=0; icent<part_geo->num_integrand_cent; icent++) {
        part_geo->mult_exp_order[icent] = order_integrand_cent[icent];
    }
    return GSUCCESS;
}
