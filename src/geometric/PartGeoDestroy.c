/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoDestroy().

   2014-09-28, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief destroys the context of partial geometric derivatives
    \author Bin Gao
    \date 2014-09-28
    \param[PartGeo:struct]{inout} part_geo the context of partial geometric derivatives
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoDestroy(PartGeo *part_geo)
{
    GInt icent;       /* incremental recorder over centers */
    GULong iexp;      /* incremental recorder of terms in multinomial expansion */
    GErrorCode ierr;  /* error information */
    if (part_geo->created==GTRUE) {
        free(part_geo->geo_order);
        part_geo->geo_order = NULL;
        free(part_geo->geo_num_repeated);
        part_geo->geo_num_repeated = NULL;
        free(part_geo->geo_idx_integrand);
        part_geo->geo_idx_integrand = NULL;
        free(part_geo->size_geo_integrand);
        part_geo->size_geo_integrand = NULL;
        if (part_geo->trans_invariance==GTRUE) {
            free(part_geo->trans_mult_coef);
            part_geo->trans_mult_coef = NULL;
            free(part_geo->trans_order_cent[0]);
            for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
                part_geo->trans_order_cent[iexp] = NULL;
            }
            free(part_geo->trans_order_cent);
            part_geo->trans_order_cent = NULL;
            part_geo->trans_invariance = GFALSE;
        }
        for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
            for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                ierr = MultExpDestroy(&part_geo->mult_exp[iexp][icent]);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpDestroy");
            }
        }
        free(part_geo->mult_exp[0]);
        for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
            part_geo->mult_exp[iexp] = NULL; 
        }
        free(part_geo->mult_exp);
        part_geo->mult_exp = NULL; 
        free(part_geo->mult_exp_order);
        part_geo->mult_exp_order = NULL;
        free(part_geo->size_mult_exp[0]);
        for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
            part_geo->size_mult_exp[iexp] = NULL;
        }
        free(part_geo->size_mult_exp);
        part_geo->size_mult_exp = NULL;
        free(part_geo->iter_mult_exp);
        part_geo->iter_mult_exp = NULL;
        part_geo->size_part_geo = 0;
        part_geo->created = GFALSE;
    }
    return GSUCCESS;
}
