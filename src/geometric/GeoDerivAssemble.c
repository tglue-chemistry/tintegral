/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivAssemble().

   2014-07-06, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief assembles the context of geometric derivatives
     \author Bin Gao
     \date 2014-07-06
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivAssemble(GeoDeriv *geo_deriv)
{
    GInt num_all_cent;         /* number of possible differentiated centers */
    GInt how_many_ncent;       /* how many different numbers of differentiated centers */
    GReal real_size_combin;    /* size of combinations of differentiated centers and order partition (real) */
    GInt partition_num_deriv;  /* number of geometric derivatives for each order partition */
    GULong combin_num_deriv;   /* number of derivatives for different combinations of order partition */
    GULong num_combin_order;   /* number of combinations of different order partitions */
    GULong irank;              /* incremental recorder over combinations */
    GInt icent;                /* incremental recorder */
    GErrorCode ierr;           /* error information */
    if (geo_deriv->order_geo<0) {
        printf("GeoDerivAssemble>> order of geometric derivatives %"GINT_FMT"\n",
               geo_deriv->order_geo);
        GErrorExit(FILE_AND_LINE, "invalid order");
    }
    else if (geo_deriv->order_geo==0) {
        geo_deriv->curr_num_cent = 0;  /* important to set as 0 */
        geo_deriv->size_combin = 1;
        geo_deriv->size_deriv = 1;
    }
    else {
        if (geo_deriv->range_num_cent[0]<1 ||
            geo_deriv->range_num_cent[0]>geo_deriv->range_num_cent[1] ||
            geo_deriv->range_num_cent[1]>geo_deriv->order_geo) {
            printf("GeoDerivAssemble>> order of geometric derivatives %"GINT_FMT"\n",
                   geo_deriv->order_geo);
            printf("GeoDerivAssemble>> range of the number of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
                   geo_deriv->range_num_cent[0],
                   geo_deriv->range_num_cent[1]);
            GErrorExit(FILE_AND_LINE, "invalid range");
        }
        /* computes the number of possible differentiated centers */
        if (geo_deriv->user_idx!=NULL) {
            if (geo_deriv->num_user_idx<0) {
                printf("GeoDerivAssemble>> number of given differentiated centers %"GINT_FMT"\n",
                       geo_deriv->num_user_idx);
                GErrorExit(FILE_AND_LINE, "invalid number");
            }
            num_all_cent = geo_deriv->num_user_idx;
        }
        else {
            if (geo_deriv->user_range_idx[0]<GMIN_IDX_ATOM ||
                geo_deriv->user_range_idx[0]>geo_deriv->user_range_idx[1]) {
                printf("GeoDerivAssemble>> range of indices of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
                       geo_deriv->user_range_idx[0],
                       geo_deriv->user_range_idx[1]);
                GErrorExit(FILE_AND_LINE, "invalid range");
            }
            num_all_cent = geo_deriv->user_range_idx[1]-geo_deriv->user_range_idx[0]+1;
        }
        /* checks the maximum number of differentiated centers */
        if (geo_deriv->range_num_cent[1]>num_all_cent) {
            printf("GeoDerivAssemble>> maximum number of differentiated centers %"GINT_FMT"\n",
                   geo_deriv->range_num_cent[1]);
            printf("GeoDerivAssemble>> number of possible differentiated centers %"GINT_FMT"\n",
                   num_all_cent);
            GErrorExit(FILE_AND_LINE, "invalid maximum number");
        }
        /* sets the combinations of minimum number of differentiated centers */
        geo_deriv->combin_cent = (BitCombin *)malloc(sizeof(BitCombin));
        if (geo_deriv->combin_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for combin_cent");
        }
        ierr = BitCombinCreate(geo_deriv->combin_cent,
                               num_all_cent,
                               geo_deriv->range_num_cent[0]);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinCreate");
        /* sets the combinations of the order of geometric derivatives partition */
        if (geo_deriv->order_geo>1 &&
            (geo_deriv->range_num_cent[0]>1 || geo_deriv->range_num_cent[1]>1)) {
            geo_deriv->combin_order = (BitCombin *)malloc(sizeof(BitCombin));
            if (geo_deriv->combin_order==NULL) {
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for combin_order");
            }
            /* here the number k-combination does not matter, we will anyway reset
               it using BitCombinSetK() during calculations */
            ierr = BitCombinCreate(geo_deriv->combin_order,
                                   geo_deriv->order_geo-1,
                                   GMax(geo_deriv->range_num_cent[0]-1,1));
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinCreate");
        }
        /* allocates memory for the indices of centers of a specific combination */
        geo_deriv->combin_idx_cent = (GInt *)malloc(geo_deriv->range_num_cent[1]*sizeof(GInt));
        if (geo_deriv->combin_idx_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for idx_cent");
        }
        /* updates the indices of differentiated centers */
        ierr = GeoDerivCombinUpdateIdxCent(geo_deriv);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateIdxCent");
        /* allocates memory for the orders of centers of a specific combination */
        geo_deriv->combin_order_cent = (GInt *)malloc(geo_deriv->range_num_cent[1]*sizeof(GInt));
        if (geo_deriv->combin_order_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for order_cent");
        }
        /* computes how many different numbers of differentiated centers */
        how_many_ncent = geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1;
        /* computes the numbers of combinations for choosing specific number of
           centers and for partitioning the order of derivatives */
        geo_deriv->num_combin_cent = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (geo_deriv->num_combin_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for num_combin_cent");
        }
        geo_deriv->num_combin_order = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (geo_deriv->num_combin_order==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for num_combin_order");
        }
        /* computes the numbers of combinations for the minimum number of differentiated centers */
        geo_deriv->num_combin_cent[0] = BitCombinGetNumCombin(geo_deriv->combin_cent);
        if (geo_deriv->combin_order!=NULL && geo_deriv->range_num_cent[0]>1) {
            geo_deriv->num_combin_order[0] = BitCombinGetNumCombin(geo_deriv->combin_order);
        }
        /* either it is the first order geometric derivatives, or the number of
           differentiated centers is 1, so there is only one combination */
        else {
            geo_deriv->num_combin_order[0] = 1;
        }
        /* other numbers of differentiated centers */
        for (icent=1; icent<how_many_ncent; icent++) {
            ierr = BinomCoefficient(num_all_cent,
                                    geo_deriv->range_num_cent[0]+icent,
                                    &geo_deriv->num_combin_cent[icent]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
            ierr = BinomCoefficient(geo_deriv->order_geo-1,
                                    geo_deriv->range_num_cent[0]+icent-1,
                                    &geo_deriv->num_combin_order[icent]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
        }
        /* computes the sizes of combinations of differentiated centers and order partition */
        geo_deriv->cent_size_combin = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (geo_deriv->cent_size_combin==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cent_size_combin");
        }
        real_size_combin = 0;
        for (icent=0; icent<how_many_ncent; icent++) {
            geo_deriv->cent_size_combin[icent] = geo_deriv->num_combin_cent[icent]
                                              * geo_deriv->num_combin_order[icent];
            real_size_combin += geo_deriv->cent_size_combin[icent];
        }
        ierr = GULongRint(real_size_combin, &geo_deriv->size_combin);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GULongRint");
        /* calculates the size of all the geometric derivatives */
        geo_deriv->cent_size_deriv = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (geo_deriv->cent_size_deriv==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cent_size_deriv");
        }
        geo_deriv->size_deriv = 0;
        /* minimum number of differentiated centers */
        geo_deriv->curr_num_cent = geo_deriv->range_num_cent[0];
        for (icent=0; icent<how_many_ncent; icent++) {
            if (geo_deriv->curr_num_cent==1) {
                combin_num_deriv = (geo_deriv->order_geo+1)*(geo_deriv->order_geo+2)/2;
            }
            else {
                combin_num_deriv = 0;
                /* loops over the combinations of order partition, and calculates
                   the number of geometric derivatives for each partition */
                ierr = BitCombinSetK(geo_deriv->combin_order, geo_deriv->curr_num_cent-1);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetK");
                num_combin_order = BitCombinGetNumCombin(geo_deriv->combin_order);
                /* gets the orders of centers for the current combination */
                ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
                /* computes the number of geometric derivatives for current partition */
                ierr = GeoDerivCombinGetNumDeriv(geo_deriv, &partition_num_deriv);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumDeriv");
                combin_num_deriv += partition_num_deriv;
                for (irank=1; irank<num_combin_order; irank++) {
                    /* moves to the next combination */
                    ierr = BitCombinNext(geo_deriv->combin_order, 1LU);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
                    /* gets the orders of centers for the current combination */
                    ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
                    /* computes the number of geometric derivatives for current partition */
                    ierr = GeoDerivCombinGetNumDeriv(geo_deriv, &partition_num_deriv);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumDeriv");
                    combin_num_deriv += partition_num_deriv;
                }
            }
            geo_deriv->cent_size_deriv[icent] = combin_num_deriv
                                             * geo_deriv->num_combin_cent[icent];
            geo_deriv->size_deriv += geo_deriv->cent_size_deriv[icent];
            geo_deriv->curr_num_cent++;
        }
        /* resets the current number of differentiated centers */
        geo_deriv->curr_num_cent = geo_deriv->range_num_cent[0];
        /* resets the combinations of the order of geometric derivatives partition */
        if (geo_deriv->combin_order!=NULL) {
            ierr = BitCombinSetK(geo_deriv->combin_order,
                                 GMax(geo_deriv->range_num_cent[0]-1,1));
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetK");
            /* updates the orders of differentiated centers */
            if (geo_deriv->range_num_cent[0]>1) {
                ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
            }
            else {
                geo_deriv->combin_order_cent[0] = geo_deriv->order_geo;
            }
        }
        else {
            geo_deriv->combin_order_cent[0] = geo_deriv->order_geo;
        }
        /* sets the ranks of current combinations of centers and order partition */
        geo_deriv->rank_combin_cent = 0;
        geo_deriv->rank_combin_order = 0;
    }
    geo_deriv->assembled = GTRUE;
    return GSUCCESS;
}
