/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivCombinUpdateIdxCent().

   2014-07-06, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief updates the indices of centers for a specific combination
    \author Bin Gao
    \date 2014-07-06
    \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
    \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivCombinUpdateIdxCent(GeoDeriv *geo_deriv)
{
    GErrorCode ierr;  /* error information */
    /* user specified differentiated centers */
    if (geo_deriv->user_idx!=NULL) {
        ierr = BitCombinGetIdxFromSet(geo_deriv->combin_cent,
                                      geo_deriv->user_idx,
                                      geo_deriv->combin_idx_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinGetIdxFromSet");
    }
    /* given range of indices of differentiated centers */
    else {
        ierr = BitCombinGetIdxFromRange(geo_deriv->combin_cent,
                                        geo_deriv->user_range_idx[0],
                                        geo_deriv->combin_idx_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinGetIdxFromRange");
    }
    return GSUCCESS;
}
