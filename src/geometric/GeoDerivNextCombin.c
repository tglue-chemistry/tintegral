/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivNextCombin().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief finds the next combination of geometric derivatives after given steps
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \param[GULong:unsigned long]{in} num_steps the given steps
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivNextCombin(GeoDeriv *geo_deriv, const GULong num_steps)
{
    GInt itr_num_cent;      /* iterator for the number of differentiated centers */
    GULong rank_new_cent;   /* rank of new combination of differentiated centers */
    GULong rank_new_order;  /* rank of new combination of the order partition */
    GInt how_many_ncent;    /* how many different numbers of differentiated centers */
    GErrorCode ierr;        /* error information */
    if (geo_deriv->assembled==GFALSE) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    if (num_steps==0) {
        printf("GeoDerivNextCombin>> warning! no step to move!\n");
    }
    else if (geo_deriv->order_geo==0) {
        printf("GeoDerivNextCombin>> warning! zeroth order geometric derivatives!\n");
    }
    else {
        itr_num_cent = geo_deriv->curr_num_cent-geo_deriv->range_num_cent[0];
        rank_new_order = geo_deriv->rank_combin_order+num_steps;
        /* we need to move several steps for the order partition */
        if (rank_new_order<geo_deriv->num_combin_order[itr_num_cent]) {
            ierr = BitCombinNext(geo_deriv->combin_order, num_steps);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
            /* updates the orders of differentiated centers */
            ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
            /* updates the rank of current combination of the order partition */
            geo_deriv->rank_combin_order = rank_new_order;
        }
        else {
            rank_new_order += geo_deriv->rank_combin_cent
                            * geo_deriv->num_combin_order[itr_num_cent];
            /* we need to move to a new combination of differentiated centers, but
               with the same number of differentiated centers */
            if (rank_new_order<geo_deriv->cent_size_combin[itr_num_cent]) {
                /* gets the new ranks of current combinations */
                rank_new_cent = rank_new_order/geo_deriv->num_combin_order[itr_num_cent];
                rank_new_order -= rank_new_cent*geo_deriv->num_combin_order[itr_num_cent];
                /* moves the combination of differentiated centers */
                ierr = BitCombinNext(geo_deriv->combin_cent,
                                     rank_new_cent-geo_deriv->rank_combin_cent);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
                /* updates the indices of differentiated centers */
                ierr = GeoDerivCombinUpdateIdxCent(geo_deriv);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateIdxCent");
                /* updates the rank of current combination of differentiated centers */
                geo_deriv->rank_combin_cent = rank_new_cent;
                /* gets the new combination of the order partition */
                if (rank_new_order>geo_deriv->rank_combin_order) {
                    /* moves the combination of the order partition */
                    ierr = BitCombinNext(geo_deriv->combin_order,
                                         rank_new_order-geo_deriv->rank_combin_order);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
                    /* updates the orders of differentiated centers */
                    ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
                    /* updates the ranks */
                    geo_deriv->rank_combin_order = rank_new_order;
                }
                else if (rank_new_order<geo_deriv->rank_combin_order) {
#if defined(NO_BITCOMBIN_SET_RANK)
                    /* moves to the first combination, and then moves to the new one */
                    ierr = BitCombinStart(geo_deriv->combin_order);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinStart");
                    ierr = BitCombinNext(geo_deriv->combin_order, rank_new_order);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
#else
                    /* gets the combination directly using greedy algorithm */
                    ierr = BitCombinSetRank(geo_deriv->combin_order, rank_new_order);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetRank");
#endif
                    /* updates the orders of differentiated centers */
                    ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
                    /* updates the ranks */
                    geo_deriv->rank_combin_order = rank_new_order;
                }
            }
            /* we need to move to a new combination of differentiated centers, and
               the number of differentiated centers is also different */
            else {
                rank_new_order -= geo_deriv->cent_size_combin[itr_num_cent]-1;
                /* computes how many different numbers of differentiated centers */
                how_many_ncent = geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1;
                /* tries to find the number of differentiated centers */
                ++itr_num_cent;
                for (; itr_num_cent<how_many_ncent; itr_num_cent++) {
                    if (rank_new_order>geo_deriv->cent_size_combin[itr_num_cent]) {
                        rank_new_order -= geo_deriv->cent_size_combin[itr_num_cent];
                    }
                    else {
                        break;
                    }
                }
                if (itr_num_cent<how_many_ncent) {
                    /* gets current number of differentiated centers */
                    geo_deriv->curr_num_cent = geo_deriv->range_num_cent[0]+itr_num_cent;
                    /* there is 1 difference between the rank and steps of moving */
                    --rank_new_order;
                    /* computes the new ranks */
                    geo_deriv->rank_combin_cent = rank_new_order
                                                / geo_deriv->num_combin_order[itr_num_cent];
                    geo_deriv->rank_combin_order = rank_new_order
                                                 - geo_deriv->rank_combin_cent
                                                 * geo_deriv->num_combin_order[itr_num_cent];
                    /* resets the combination of differentiated centers */
                    ierr = BitCombinSetK(geo_deriv->combin_cent,
                                         geo_deriv->curr_num_cent);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetK");
                    /* moves the combination of differentiated centers */
                    if (geo_deriv->rank_combin_cent>0) {
                        ierr = BitCombinNext(geo_deriv->combin_cent,
                                             geo_deriv->rank_combin_cent);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
                    }
                    /* updates the indices of differentiated centers */
                    ierr = GeoDerivCombinUpdateIdxCent(geo_deriv);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateIdxCent");
                    /* resets the combination of the order partition */
                    if (geo_deriv->curr_num_cent>1) {
                        ierr = BitCombinSetK(geo_deriv->combin_order,
                                             geo_deriv->curr_num_cent-1);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinSetK");
                        /* moves the combination of the order partition */
                        if (geo_deriv->rank_combin_order>0) {
                            ierr = BitCombinNext(geo_deriv->combin_order,
                                                 geo_deriv->rank_combin_order);
                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinNext");
                        }
                        /* updates the orders of differentiated centers */
                        ierr = GeoDerivCombinUpdateOrderCent(geo_deriv);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinUpdateOrderCent");
                    }
                    /* this is one center geometric derivatives */
                    else {
                        geo_deriv->combin_order_cent[0] = geo_deriv->order_geo;
                    }
                }
                else {
                    printf("GeoDerivNextCombin>> size of all the combinations %lu\n",
                           geo_deriv->size_combin);
                    /* computes the rank of current combination in all the combinations */
                    how_many_ncent = geo_deriv->curr_num_cent-geo_deriv->range_num_cent[0];
                    rank_new_order = geo_deriv->rank_combin_cent
                                   * geo_deriv->num_combin_order[how_many_ncent]
                                   + geo_deriv->rank_combin_order;
                    for (itr_num_cent=0; itr_num_cent<how_many_ncent; itr_num_cent++) {
                        rank_new_order += geo_deriv->cent_size_combin[itr_num_cent];
                    }
                    printf("GeoDerivNextCombin>> rank of current combination %lu\n",
                           rank_new_order);
                    printf("GeoDerivNextCombin>> required number of steps to move %lu\n",
                           num_steps);
                    GErrorExit(FILE_AND_LINE, "too many steps");
                }
            }
        }
    }
    return GSUCCESS;
}
