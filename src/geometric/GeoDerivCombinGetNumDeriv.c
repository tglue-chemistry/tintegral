/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivCombinGetNumDeriv().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief gets the number of geometric derivatives for the current combination,
         should be called after GeoDerivAssemble()
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GInt:int]{out} combin_num_deriv the number of geometric derivatives
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivCombinGetNumDeriv(const GeoDeriv *geo_deriv,
                                     GInt *combin_num_deriv)
{
    GInt icent;
    if (geo_deriv->combin_order_cent==NULL) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    *combin_num_deriv = 1;
    /* for zeroth order geometric derivatives, we have set geo_deriv->curr_num_cent
       as zero in GeoDerivAssemble() */
    for (icent=0; icent<geo_deriv->curr_num_cent; icent++) {
        *combin_num_deriv *= (geo_deriv->combin_order_cent[icent]+1)
                           * (geo_deriv->combin_order_cent[icent]+2)/2;
    }
    return GSUCCESS;
}
