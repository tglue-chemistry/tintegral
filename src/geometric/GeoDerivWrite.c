/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivWrite().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief writes the context of geometric derivatives
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GChar:char]{in} file_name the name of the file
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivWrite(const GeoDeriv *geo_deriv, const GChar *file_name)
{
    FILE *fp_geo;         /* file pointer */
    GInt how_many_ncent;  /* how many different numbers of differentiated centers */
    GInt icent;           /* incremental recorder */
    GErrorCode ierr;      /* error information */
    if (geo_deriv->assembled==GFALSE) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    /* opens the file */
    fp_geo = fopen(file_name, "a");
    if (fp_geo==NULL) {
        printf("GeoDerivWrite>> file: %s\n", file_name);
        GErrorExit(FILE_AND_LINE, "failed to open the file in appending mode");
    }
    fprintf(fp_geo, "\nGen1Int library compiled at %s, %s\n", __TIME__, __DATE__);
    fprintf(fp_geo,
            "GeoDerivWrite>> order of geometric derivatives %"GINT_FMT"\n",
            geo_deriv->order_geo);
    fprintf(fp_geo,
            "GeoDerivWrite>> range of the number of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
            geo_deriv->range_num_cent[0],
            geo_deriv->range_num_cent[1]);
    if (geo_deriv->user_idx!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> number of user specified differentiated centers %"GINT_FMT"\n",
                geo_deriv->num_user_idx);
        fprintf(fp_geo,
                "GeoDerivWrite>> indices of user specified differentiated centers");
        for (icent=0; icent<geo_deriv->num_user_idx; icent++) {
            if (icent%10==0) fprintf(fp_geo, "\n");
            fprintf(fp_geo, "  %"GINT_FMT"", geo_deriv->user_idx[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    else {
        fprintf(fp_geo,
                "GeoDerivWrite>> user specified range of indices of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
                geo_deriv->user_range_idx[0],
                geo_deriv->user_range_idx[1]);
    }
    fprintf(fp_geo,
            "GeoDerivWrite>> current number of differentiated centers %"GINT_FMT"\n",
            geo_deriv->curr_num_cent);
    if (geo_deriv->combin_cent!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> combinations of differentiated centers\n");
        ierr = BitCombinWrite(geo_deriv->combin_cent, fp_geo);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
        fprintf(fp_geo,
                "GeoDerivWrite>> rank of current combination of differentiated centers %lu\n",
                geo_deriv->rank_combin_cent);
    }
    if (geo_deriv->combin_order!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> combinations of the order of geometric derivatives partition\n");
        ierr = BitCombinWrite(geo_deriv->combin_order, fp_geo);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinWrite");
        fprintf(fp_geo,
                "GeoDerivWrite>> rank of current combination of the order partition %lu\n",
                geo_deriv->rank_combin_order);
    }
    if (geo_deriv->combin_idx_cent!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> indices of differentiated centers for current combination\n");
        for (icent=0; icent<geo_deriv->curr_num_cent; icent++) {
            fprintf(fp_geo, "  %"GINT_FMT"", geo_deriv->combin_idx_cent[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    if (geo_deriv->combin_order_cent!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> orders of differentiated centers for current combination\n");
        for (icent=0; icent<geo_deriv->curr_num_cent; icent++) {
            fprintf(fp_geo, "  %"GINT_FMT"", geo_deriv->combin_order_cent[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    /* computes how many different numbers of differentiated centers */
    how_many_ncent = geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1;
    if (geo_deriv->num_combin_cent!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> number of combinations for choosing specific number of centers\n");
        for (icent=0; icent<how_many_ncent; icent++) {
            fprintf(fp_geo, "  %lu", geo_deriv->num_combin_cent[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    if (geo_deriv->num_combin_order!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> number of combinations for partitioning the order of derivatives\n");
        for (icent=0; icent<how_many_ncent; icent++) {
            fprintf(fp_geo, "  %lu", geo_deriv->num_combin_order[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    if (geo_deriv->cent_size_combin!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> sizes of combinations of centers and order partition\n");
        for (icent=0; icent<how_many_ncent; icent++) {
            fprintf(fp_geo, "  %lu", geo_deriv->cent_size_combin[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    fprintf(fp_geo,
            "GeoDerivWrite>> size of all combinations of centers and order partition %lu\n",
            geo_deriv->size_combin);
    if (geo_deriv->cent_size_deriv!=NULL) {
        fprintf(fp_geo,
                "GeoDerivWrite>> sizes of geometric derivatives for different numbers of centers\n");
        for (icent=0; icent<how_many_ncent; icent++) {
            fprintf(fp_geo, "  %lu", geo_deriv->cent_size_deriv[icent]);
        }
        fprintf(fp_geo, "\n");
    }
    fprintf(fp_geo,
            "GeoDerivWrite>> size of all geometric derivatives %lu\n",
            geo_deriv->size_deriv);
    fprintf(fp_geo, "\n");
    fclose(fp_geo);
    return GSUCCESS;
}
