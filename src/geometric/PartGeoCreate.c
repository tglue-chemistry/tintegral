/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoCreate().

   2014-09-04, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"
#include "utilities/gen1int_sort.h"

/*% \brief creates the context of partial geometric derivatives
    \author Bin Gao
    \date 2014-09-04
    \param[PartGeo:struct]{inout} part_geo the context of partial geometric derivatives
    \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
    \param[GInt:int]{in} num_integrand_cent number of centers of integrand
    \param[GInt:int]{inout} idx_integrand_cent indices of centers of integrand (operator
        and basis set centers)
    \param[GBool:enum]{out} zero_deriv indicates if partial geometric derivatives are zero
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoCreate(PartGeo *part_geo,
                         const GeoDeriv *geo_deriv,
                         const GInt num_integrand_cent,
                         GInt *idx_integrand_cent,
                         GBool *zero_deriv)
{
    GInt *tag_idx_cent;        /* tag for the positions of integrand centers before sorting */
    GInt num_diff_cent;        /* number of differentiated centers in the integrand */
    GInt last_integrand_cent;  /* last integrand center during searching for differentiated centers */
    GInt *num_non_repeated;    /* number of repeated non-differentiated centers in the integrand */
    GInt *idx_non_cent;        /* indices of non-differentiated centers in the integrand */
    GInt num_unique_non;       /* number of unique non-differentiated centers in the integrand */
    GInt num_non_cent;         /* number of non-differentiated centers in the integrand */
    GULong tran_size_exp;      /* size of each expansion term for using translational invariance */
    GInt ptr_idx;              /* pointer to the index of center */
    GInt icent,jcent,kcent;    /* incremental recorders over centers */
    GULong iexp,jexp;          /* incremental recorders of terms in multinomial expansion */
    GErrorCode ierr;           /* error information */
    if (num_integrand_cent<1) {
        printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
               num_integrand_cent);
        GErrorExit(FILE_AND_LINE, "invalid number of centers");
    }
    /* more differentiated centers than those of the integrand */
    if (geo_deriv->curr_num_cent>num_integrand_cent) {
        *zero_deriv = GTRUE;
        part_geo->created = GFALSE;
    }
    else {
        *zero_deriv = GFALSE;
        /* sorts the centers of integrand */
        tag_idx_cent = (GInt *)malloc(num_integrand_cent*sizeof(GInt));
        if (tag_idx_cent==NULL) {
            printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                   num_integrand_cent);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for tag_idx_cent");
        }
        for (icent=0; icent<num_integrand_cent; icent++) {
            tag_idx_cent[icent] = icent;
        }
        SortTagIdxAscending(num_integrand_cent, idx_integrand_cent, tag_idx_cent);
        /* allocates memory for the information of total geometric derivatives and integrand */
        part_geo->num_integrand_cent = num_integrand_cent;
        part_geo->geo_order = (GInt *)malloc(part_geo->num_integrand_cent*sizeof(GInt));
        if (part_geo->geo_order==NULL) {
            printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                   part_geo->num_integrand_cent);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for geo_order");
        }
        part_geo->geo_num_repeated = (GInt *)malloc(part_geo->num_integrand_cent*sizeof(GInt));
        if (part_geo->geo_num_repeated==NULL) {
            printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                   part_geo->num_integrand_cent);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for geo_num_repeated");
        }
        part_geo->geo_idx_integrand = (GInt *)malloc(part_geo->num_integrand_cent*sizeof(GInt));
        if (part_geo->geo_idx_integrand==NULL) {
            printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                   part_geo->num_integrand_cent);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for geo_idx_integrand");
        }
        /* initializes the maximum number of repeated differentiated centers in the integrand */
        part_geo->max_repeated_cent = 0;
        num_diff_cent = 0;
        /* loops over all differentiated centers in geometric derivatives in reverse order */
        last_integrand_cent = part_geo->num_integrand_cent-1;
        for (icent=geo_deriv->curr_num_cent-1,jcent=0; icent>=0; icent--,jcent++) {
            part_geo->geo_order[jcent] = geo_deriv->combin_order_cent[icent];
            part_geo->geo_num_repeated[jcent] = 0;
            /* tries to find the current differentiated center in the integrand */
            for (kcent=last_integrand_cent; kcent>=0; kcent--) {
                /* found the differentiated center in the integrand */
                if (geo_deriv->combin_idx_cent[icent]==idx_integrand_cent[kcent]) {
                    part_geo->geo_num_repeated[jcent]++;
                    part_geo->geo_idx_integrand[num_diff_cent++] = kcent;
                }
                /* since both \var(combin_idx_cent) and \var(idx_integrand_cent) are
                   sorted in ascending order, there are no more differentiated centers
                   left in the integrand */
                else if (geo_deriv->combin_idx_cent[icent]>idx_integrand_cent[kcent]) {
                    last_integrand_cent = kcent;
                    break;
                }
            }
            /* zero geometric derivative if the differentiated center is not in the integrand */
            if (part_geo->geo_num_repeated[jcent]==0) {
                free(tag_idx_cent);
                tag_idx_cent = NULL;
                free(part_geo->geo_order);
                part_geo->geo_order = NULL;
                free(part_geo->geo_num_repeated);
                part_geo->geo_num_repeated = NULL;
                free(part_geo->geo_idx_integrand);
                part_geo->geo_idx_integrand = NULL;
                *zero_deriv = GTRUE;
                part_geo->created = GFALSE;
                break;
            }
            /* updates the differentiated center with maximum repetitions in the integrand */
            else if (part_geo->max_repeated_cent<=part_geo->geo_num_repeated[jcent]) {
                part_geo->max_repeated_cent = part_geo->geo_num_repeated[jcent];
                part_geo->idx_max_repeated = jcent;
            }
        }
        if (*zero_deriv==GFALSE) {
            /* using translational invariance */
            if (2*part_geo->max_repeated_cent>part_geo->num_integrand_cent) {
                part_geo->trans_invariance = GTRUE;
                /* gets the order of the differentiated center with maximum repetitions */
                part_geo->order_max_repeated = part_geo->geo_order[part_geo->idx_max_repeated];
                /* tries to find all non-differentiated centers in the integrand
                   [non-diff] [diff][non-diff][diff]...[diff][non-diff][diff] [non-diff]
                      (a)                           (b)                          (c) */
                num_non_repeated = (GInt *)malloc(part_geo->max_repeated_cent*sizeof(GInt));
                if (num_non_repeated==NULL) {
                    printf("PartGeoCreate>> maximum number of repeated differentiated centers %"GINT_FMT"\n",
                           part_geo->max_repeated_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for num_non_repeated");
                }
                idx_non_cent = (GInt *)malloc(part_geo->max_repeated_cent*sizeof(GInt));
                if (idx_non_cent==NULL) {
                    printf("PartGeoCreate>> maximum number of repeated differentiated centers %"GINT_FMT"\n",
                           part_geo->max_repeated_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for idx_non_cent");
                }
                /* (c) */
                num_unique_non = 0;
                num_non_cent = 0;
                num_non_repeated[num_unique_non] = 0;
                for (kcent=part_geo->num_integrand_cent-1;
                     kcent>part_geo->geo_idx_integrand[0];
                     kcent--) {
                    num_non_repeated[num_unique_non]++;
                    idx_non_cent[num_non_cent++] = tag_idx_cent[kcent];
                    if (idx_integrand_cent[kcent]!=idx_integrand_cent[kcent-1]) {
                        num_non_repeated[++num_unique_non] = 0;
                    }
                }
                /* (b) */
                ptr_idx = part_geo->geo_num_repeated[0]-1;
                for (icent=1,jcent=0; icent<geo_deriv->curr_num_cent; icent++,jcent++) {
                    for (kcent=part_geo->geo_idx_integrand[ptr_idx]-1;
                         kcent>part_geo->geo_idx_integrand[ptr_idx+1];
                         kcent--) {
                        num_non_repeated[num_unique_non]++;
                        idx_non_cent[num_non_cent++] = tag_idx_cent[kcent];
                        if (idx_integrand_cent[kcent]!=idx_integrand_cent[kcent-1]) {
                            num_non_repeated[++num_unique_non] = 0;
                        }
                    }
                    ptr_idx += part_geo->geo_num_repeated[icent];
                }
                /* (a) */
                if (part_geo->geo_idx_integrand[ptr_idx]>0) {
                    for (kcent=part_geo->geo_idx_integrand[ptr_idx]; kcent>0; kcent--) {
                        num_non_repeated[num_unique_non]++;
                        idx_non_cent[num_non_cent++] = tag_idx_cent[kcent];
                        if (idx_integrand_cent[kcent]!=idx_integrand_cent[kcent-1]) {
                            num_non_repeated[++num_unique_non] = 0;
                        }
                    }
                    /* the first center */
                    num_non_repeated[num_unique_non++]++;
                    idx_non_cent[num_non_cent++] = tag_idx_cent[0];
                }
                /* gets greater indices of differentiated centers in the integrand */
                for (icent=0,ptr_idx=0; icent<part_geo->idx_max_repeated; icent++) {
                    for (jcent=0; jcent<part_geo->geo_num_repeated[icent]; jcent++,ptr_idx++) {
                        part_geo->geo_idx_integrand[ptr_idx] = tag_idx_cent[part_geo->geo_idx_integrand[ptr_idx]];
                    }
                }
                /* copies the indices of non-differentiated centers in the integrand */
                for (icent=0; icent<num_non_cent; icent++,ptr_idx++) {
                    part_geo->geo_idx_integrand[ptr_idx] = idx_non_cent[icent];
                }
                free(idx_non_cent);
                idx_non_cent = NULL;
                /* gets smaller indices of differentiated centers and moves them
                   after the non-differentiated centers */
                for (icent=ptr_idx-num_non_cent+part_geo->max_repeated_cent;
                     icent<num_diff_cent;
                     icent++,ptr_idx++) {
                    part_geo->geo_idx_integrand[ptr_idx] = tag_idx_cent[part_geo->geo_idx_integrand[icent]];
                }
                free(tag_idx_cent);
                tag_idx_cent = NULL;
                /* number of differentiated centers of geometric derivatives after
                   performing translational invariance */
                part_geo->geo_num_cent = geo_deriv->curr_num_cent+num_unique_non-1;
                /* moves information of geometric derivatives of smaller indices of
                   differentiated centers at the end */
                for (icent=geo_deriv->curr_num_cent-1,jcent=part_geo->geo_num_cent-1;
                     icent>part_geo->idx_max_repeated;
                     icent--,jcent--) {
                    part_geo->geo_order[jcent] = part_geo->geo_order[icent];
                    part_geo->geo_num_repeated[jcent] = part_geo->geo_num_repeated[icent];
                }
                /* puts the non-differentiated centers back */
                if (num_unique_non>0) {
                    jcent = part_geo->idx_max_repeated;
                    part_geo->geo_order[jcent] = 0;
                    part_geo->geo_num_repeated[jcent++] = num_non_repeated[0];
                    for (icent=1; icent<num_unique_non; icent++) {
                        part_geo->geo_order[jcent] = 0;
                        part_geo->geo_num_repeated[jcent++] = num_non_repeated[icent];
                    }
                }
                free(num_non_repeated);
                num_non_repeated = NULL;
                /* sets \var{idx_max_repeated} to the last non-differentiated center */
                part_geo->idx_max_repeated += num_unique_non-1;
                /* gets the number of terms in multinomial expansion for translational invariance */
                ierr = BinomCoefficient(part_geo->order_max_repeated+part_geo->geo_num_cent-1,
                                        part_geo->order_max_repeated,
                                        &part_geo->trans_num_terms);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling BinomCoefficient");
                /* allocates memory for the multinomial coefficients for translational invariance */
                part_geo->trans_mult_coef = (GULong *)malloc(part_geo->trans_num_terms*sizeof(GULong));
                if (part_geo->trans_mult_coef==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for trans_mult_coef");
                }
                /* allocates memory for the orders of each expansion term using translational invariance */
                part_geo->trans_order_cent = (GInt **)malloc(part_geo->trans_num_terms*sizeof(GInt *));
                if (part_geo->trans_order_cent==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for trans_order_cent");
                }
                /* allocates memory for the two-dimensional array at one time */
                tran_size_exp = part_geo->trans_num_terms*part_geo->geo_num_cent;
                part_geo->trans_order_cent[0] = (GInt *)malloc(tran_size_exp*sizeof(GInt));
                if (part_geo->trans_order_cent[0]==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for trans_order_cent[0]");
                }
                /* allocates memory for multinomial expansions */
                part_geo->mult_exp = (MultExp **)malloc(part_geo->trans_num_terms*sizeof(MultExp *));
                if (part_geo->mult_exp==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp");
                }
                part_geo->mult_exp[0] = (MultExp *)malloc(tran_size_exp*sizeof(MultExp));
                if (part_geo->mult_exp[0]==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp[0]");
                }
                /* allocates memory for the sizes of multinomial expansions */
                part_geo->size_mult_exp = (GULong **)malloc(part_geo->trans_num_terms*sizeof(GULong *));
                if (part_geo->size_mult_exp==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for size_mult_exp");
                }
                part_geo->size_mult_exp[0] = (GULong *)malloc(tran_size_exp*sizeof(GULong));
                if (part_geo->size_mult_exp[0]==NULL) {
                    printf("PartGeoCreate>> number of terms in multinomial expansion %lu\n",
                           part_geo->trans_num_terms);
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for size_mult_exp[0]");
                }
                /* allocates memory for the iterators of multinomial expansions */
                part_geo->iter_mult_exp = (GULong *)malloc(part_geo->geo_num_cent*sizeof(GULong));
                if (part_geo->iter_mult_exp==NULL) {
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for iter_mult_exp");
                }
                /* make the pointers to the correct memory */
                for (iexp=1,jexp=0; iexp<part_geo->trans_num_terms; iexp++,jexp++) {
                    part_geo->trans_order_cent[iexp] = part_geo->trans_order_cent[jexp]
                                                     + part_geo->geo_num_cent;
                    part_geo->mult_exp[iexp] = part_geo->mult_exp[jexp]
                                             + part_geo->geo_num_cent;
                    part_geo->size_mult_exp[iexp] = part_geo->size_mult_exp[jexp]
                                                  + part_geo->geo_num_cent;
                }
                /* generates all compositions of the multinomial expansion for translational invariance */
                NextComposition(part_geo->order_max_repeated,
                                part_geo->geo_num_cent,
                                part_geo->trans_num_terms,
                                part_geo->trans_mult_coef,
                                part_geo->trans_order_cent[0]);
                /* initializes the iterators */
                part_geo->iter_trans = 0;
                for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                    part_geo->iter_mult_exp[icent] = 0;
                }
                /* sets the multinomial expansions */
                part_geo->size_part_geo = 0;
                for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
                    tran_size_exp = 1;
                    for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                        ierr = MultExpCreate(&part_geo->mult_exp[iexp][icent],
                                             part_geo->geo_order[icent]+part_geo->trans_order_cent[iexp][icent],
                                             part_geo->geo_num_repeated[icent]);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpCreate");
                        ierr = MultExpGetSize(&part_geo->mult_exp[iexp][icent],
                                              &part_geo->size_mult_exp[iexp][icent]);
                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpGetSize");
                        /* updates the size of each expansion term for using translational invariance */
                        tran_size_exp *= part_geo->size_mult_exp[iexp][icent];
                    }
                    /* updates the size of partial geometric derivatives */
                    part_geo->size_part_geo += tran_size_exp;
                }
            }
            /* only multinomial expansion used */
            else {
                part_geo->geo_num_cent = geo_deriv->curr_num_cent;
                /* gets indices of differentiated centers in the integrand */
                for (icent=0; icent<num_diff_cent; icent++) {
                    part_geo->geo_idx_integrand[icent] = tag_idx_cent[part_geo->geo_idx_integrand[icent]];
                }
                free(tag_idx_cent);
                tag_idx_cent = NULL;
                part_geo->trans_invariance = GFALSE;
                part_geo->trans_num_terms = 1;
                part_geo->trans_mult_coef = NULL;
                part_geo->trans_order_cent = NULL;
                part_geo->iter_trans = 0;
                /* allocates memory for multinomial expansions */
                part_geo->mult_exp = (MultExp **)malloc(sizeof(MultExp *));
                if (part_geo->mult_exp==NULL) {
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp");
                }
                part_geo->mult_exp[0] = (MultExp *)malloc(part_geo->geo_num_cent*sizeof(MultExp));
                if (part_geo->mult_exp[0]==NULL) {
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp[0]");
                }
                /* allocates memory for the sizes of multinomial expansions */
                part_geo->size_mult_exp = (GULong **)malloc(sizeof(GULong *));
                if (part_geo->size_mult_exp==NULL) {
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for size_mult_exp");
                }
                part_geo->size_mult_exp[0] = (GULong *)malloc(part_geo->geo_num_cent*sizeof(GULong));
                if (part_geo->size_mult_exp[0]==NULL) {
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for size_mult_exp[0]");
                }
                /* allocates memory for the iterators of multinomial expansions */
                part_geo->iter_mult_exp = (GULong *)malloc(part_geo->geo_num_cent*sizeof(GULong));
                if (part_geo->iter_mult_exp==NULL) {
                    printf("PartGeoCreate>> number of differentiated centers %"GINT_FMT"\n",
                           part_geo->geo_num_cent);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for iter_mult_exp");
                }
                /* sets the multinomial expansions */
                part_geo->size_part_geo = 1;
                for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                    ierr = MultExpCreate(&part_geo->mult_exp[0][icent],
                                         part_geo->geo_order[icent],
                                         part_geo->geo_num_repeated[icent]);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpCreate");
                    ierr = MultExpGetSize(&part_geo->mult_exp[0][icent],
                                          &part_geo->size_mult_exp[0][icent]);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpGetSize");
                    /* initializes the iterators and updates the size of partial geometric derivatives */
                    part_geo->iter_mult_exp[icent] = 0;
                    part_geo->size_part_geo *= part_geo->size_mult_exp[0][icent];
                }
            }
            /* allocates memory for variables used during calculations */
            part_geo->size_geo_integrand = (GInt *)malloc(part_geo->num_integrand_cent*sizeof(GInt));
            if (part_geo->size_geo_integrand==NULL) {
                printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                       num_integrand_cent);
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for size_geo_integrand");
            }
            part_geo->mult_exp_order = (GInt *)malloc(part_geo->num_integrand_cent*sizeof(GInt));
            if (part_geo->mult_exp_order==NULL) {
                printf("PartGeoCreate>> number of centers of integrand %"GINT_FMT"\n",
                       num_integrand_cent);
                GErrorExit(FILE_AND_LINE, "failed to allocate memory for mult_exp_order");
            }
            part_geo->created = GTRUE;
        }
    }
    return GSUCCESS;
}
