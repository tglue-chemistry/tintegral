/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoGetDeriv().

   2014-09-12, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"
/* triangle based functions */
#include "triangle/gen1int_triangle.h"

/*% \brief gets total geometric derivatives from partial geometric derivatives
    \author Bin Gao
    \date 2014-09-12
    \param[PartGeo:struct]{inout} part_geo the context of partial geometric derivatives
    \param[GInt:int]{inout} order_integrand_cent orders of derivatives w.r.t. centers of
        integrand (operator and basis sets)
    \param[GInt:int]{in} dim_integrals dimension of integrals (basis sets and operators)
    \param[GReal:real]{inout} val_inp input values
    \param[GReal:real]{inout} val_out output values
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoGetDeriv(PartGeo *part_geo,
                           GInt *order_integrand_cent,
                           const GInt dim_integrals,
                           GReal **val_inp,
                           GReal **val_out)
{
    GInt size_total_geo;       /* size of total geometric derivatives */
    GInt size_geo_integrand;   /* size of partial geometric derivatives on the integrand */
    GInt size_integrals;       /* size of integrals (operator, basis sets and partial geometric derivatives ) */
    GReal *_p_val_inp;         /* pointer to the input values */
    GReal *_p_val_out;         /* pointer to the output values */
    GInt real_geo_order;       /* real order of geometric derivatives */
    GInt real_order_max;       /* real order of differentiated center with maximum repetitions in the integrand */
    GInt idx_integrand;        /* index of the center in the integrand */
    GInt ptr_idx;              /* pointer to the index of the center */
    GInt icent,jcent;          /* incremental recorders over centers */
    GInt inum;                 /* incremental recorder over number of centers */
    GErrorCode ierr;           /* error information */
    /* computes the sizes of partial geometric derivatives w.r.t. centers of integrand */
    for (icent=0; icent<part_geo->num_integrand_cent; icent++) {
        part_geo->size_geo_integrand[icent] = (order_integrand_cent[icent]+1)
                                            * (order_integrand_cent[icent]+2)/2;
    }
    /* points to input and output values */
    _p_val_inp = *val_inp;
    _p_val_out = *val_out;
    /* for multinomial expansion, the shape of the integrals takes the following form:
       [size_total_geo][num_idx_geo][size_geo_integrand][num_idx_part][size_integrals]
                            /|\                               |
                             |________________________________|
    */
    size_total_geo = 1;
    for (icent=0,ptr_idx=0; icent<part_geo->geo_num_cent; icent++) {
        /* loops over each repeated differentiated center */
        real_geo_order = 0;
        for (inum=0; inum<part_geo->geo_num_repeated[icent]; inum++) {
            /* index of the differentiated center in the integrand */
            idx_integrand = part_geo->geo_idx_integrand[ptr_idx++];
            /* non-zero order from multinomial expansion */
            if (part_geo->mult_exp_order[idx_integrand]>0) {
                /* computes \var{size_geo_integrand} */
                size_geo_integrand = 1;
                for (jcent=idx_integrand+1; jcent<part_geo->num_integrand_cent; jcent++) {
                    size_geo_integrand *= part_geo->size_geo_integrand[jcent];
                }
#if defined(GEN1INT_DEBUG)
                printf("PartGeoGetDeriv>> order_integrand_cent[idx_integrand] %"GINT_FMT"\n",
                       order_integrand_cent[idx_integrand]);
                printf("PartGeoGetDeriv>> real_geo_order %"GINT_FMT"\n", real_geo_order);
                printf("PartGeoGetDeriv>> mult_exp_order[idx_integrand] %"GINT_FMT"\n",
                       part_geo->mult_exp_order[idx_integrand]);
#endif
                /* no need to shift if this is the first time to transfer order from partial
                   geometric derivatives and \var{size_geo_integrand}==1 */
                if (order_integrand_cent[idx_integrand]!=part_geo->mult_exp_order[idx_integrand] ||
                    real_geo_order!=0 ||
                    size_geo_integrand!=1) {
                    /* computes \var{size_integrals} */
                    size_integrals = dim_integrals;
                    for (jcent=0; jcent<idx_integrand; jcent++) {
                        size_integrals *= part_geo->size_geo_integrand[jcent];
                    }
                    /* transfers the order from partial geometric derivatives to total geometric derivatives */
#if defined(GEN1INT_DEBUG)
                    printf("PartGeoGetDeriv>> size_total_geo %"GINT_FMT"\n",
                           size_total_geo);
                    printf("PartGeoGetDeriv>> size_geo_integrand %"GINT_FMT"\n",
                           size_geo_integrand);
                    printf("PartGeoGetDeriv>> size_integrals %"GINT_FMT"\n",
                           size_integrals);
#endif
                    real_order_max = order_integrand_cent[idx_integrand]
                                   - part_geo->mult_exp_order[idx_integrand];
                    ierr = TriangleOrderLShift(real_order_max,
                                               real_geo_order,
                                               part_geo->mult_exp_order[idx_integrand],
                                               size_total_geo,
                                               size_geo_integrand,
                                               size_integrals,
                                               _p_val_inp,
                                               _p_val_out);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleOrderLShift");
                    /* switches input and output values */
                    *val_inp = _p_val_inp;
                    _p_val_inp = _p_val_out;
                    _p_val_out = *val_inp;
                }
                /* updates the order and size of partial geometric derivatives */
                order_integrand_cent[idx_integrand] -= part_geo->mult_exp_order[idx_integrand];
                part_geo->size_geo_integrand[idx_integrand] = (order_integrand_cent[idx_integrand]+1)
                                                            * (order_integrand_cent[idx_integrand]+2)/2;
                /* updates the order of total geometric derivatives */
                real_geo_order += part_geo->mult_exp_order[idx_integrand];
            }
        }
        /* checks the order of total geometric derivatives */
        if (part_geo->trans_invariance==GTRUE) {
            if (real_geo_order!=part_geo->geo_order[icent]+part_geo->trans_order_cent[part_geo->iter_trans][icent]) {
                printf("PartGeoGetDeriv>> center %"GINT_FMT"\n", icent);
                printf("PartGeoGetDeriv>> order of geometric derivatives %"GINT_FMT"\n",
                       part_geo->geo_order[icent]+part_geo->trans_order_cent[part_geo->iter_trans][icent]);
                printf("PartGeoGetDeriv>> real order %"GINT_FMT"\n", real_geo_order);
                GErrorExit(FILE_AND_LINE, "incorrect real order");
            }
        }
        else {
            if (real_geo_order!=part_geo->geo_order[icent]) {
                printf("PartGeoGetDeriv>> center %"GINT_FMT"\n", icent);
                printf("PartGeoGetDeriv>> order of geometric derivatives %"GINT_FMT"\n",
                       part_geo->geo_order[icent]);
                printf("PartGeoGetDeriv>> real order %"GINT_FMT"\n", real_geo_order);
                GErrorExit(FILE_AND_LINE, "incorrect real order");
            }
        }
        /* updates the size of total geometric derivatives */
        size_total_geo *= (real_geo_order+1)*(real_geo_order+1)/2;
    }
    /* translational invariance used */
    if (part_geo->trans_invariance==GTRUE) {
        /* total geometric derivatives are arranged as:

                    0 <-- idx_max_repeated,            idx_max_repeated+1 --> geo_num_cent
           greatest index & non-differentiated centers        ------>         least index

           the shape of the integrals for indices [0,idx_max_repeated]
           takes the following form:

           [size_total_geo][num_idx_part][size_geo_integrand][num_idx_geo][size_integrals]
                                  |                              /|\
                                  |_______________________________|
        */
        size_total_geo = 1;
        for (icent=0; icent<=part_geo->idx_max_repeated; icent++) {
            real_geo_order = part_geo->geo_order[icent]
                           + part_geo->trans_order_cent[part_geo->iter_trans][icent];
            size_total_geo *= (real_geo_order+1)*(real_geo_order+2)/2;
        }
        size_geo_integrand = 1;
        size_integrals = dim_integrals;
        for (icent=part_geo->idx_max_repeated+1; icent<part_geo->geo_num_cent; icent++) {
            real_geo_order = part_geo->geo_order[icent]
                           + part_geo->trans_order_cent[part_geo->iter_trans][icent];
            size_integrals *= (real_geo_order+1)*(real_geo_order+2)/2;
        }
        for (icent=0; icent<part_geo->num_integrand_cent; icent++) {
            size_integrals *= part_geo->size_geo_integrand[icent];
        }
        /* initializes the order of differentiated center with maximum repetitions in the integrand */
        real_order_max = 0;
        for (icent=part_geo->idx_max_repeated; icent>=0; icent--) {
            /* updates \var{size_total_geo} */
            real_geo_order = part_geo->geo_order[icent]
                           + part_geo->trans_order_cent[part_geo->iter_trans][icent];
            size_total_geo /= (real_geo_order+1)*(real_geo_order+2)/2;
            if (part_geo->trans_order_cent[part_geo->iter_trans][icent]>0) {
#if defined(GEN1INT_DEBUG)
                printf("PartGeoGetDeriv>>TriangleOrderRShift>> part_geo->geo_order[icent] %"GINT_FMT"\n",
                       part_geo->geo_order[icent]);
                printf("PartGeoGetDeriv>>TriangleOrderRShift>> real_order_max %"GINT_FMT"\n",
                       real_order_max);
                printf("PartGeoGetDeriv>>TriangleOrderRShift>> trans_order_cent[icent] %"GINT_FMT"\n",
                       part_geo->trans_order_cent[part_geo->iter_trans][icent]);
#endif
                /* transfers the order of geometric derivatives of current differentiated center */
                if (real_geo_order!=part_geo->trans_order_cent[part_geo->iter_trans][icent] ||
                    real_order_max!=0 ||
                    size_geo_integrand!=1) {
#if defined(GEN1INT_DEBUG)
                    printf("PartGeoGetDeriv>>TriangleOrderRShift>> size_total_geo %"GINT_FMT"\n",
                           size_total_geo);
                    printf("PartGeoGetDeriv>>TriangleOrderRShift>> size_geo_integrand %"GINT_FMT"\n",
                           size_geo_integrand);
                    printf("PartGeoGetDeriv>>TriangleOrderRShift>> size_integrals %"GINT_FMT"\n",
                           size_integrals);
#endif
                    ierr = TriangleOrderRShift(part_geo->geo_order[icent],
                                               real_order_max,
                                               part_geo->trans_order_cent[part_geo->iter_trans][icent],
                                               size_total_geo,
                                               size_geo_integrand,
                                               size_integrals,
                                               _p_val_inp,
                                               _p_val_out);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleOrderRShift");
                    /* switches input and output values */
                    *val_inp = _p_val_inp;
                    _p_val_inp = _p_val_out;
                    _p_val_out = *val_inp;
                }
                /* updates the order of differentiated center with maximum repetitions in the integrand */
                real_order_max += part_geo->trans_order_cent[part_geo->iter_trans][icent];
            }
            /* updates \var{size_geo_integrand}  */
            size_geo_integrand *= (part_geo->geo_order[icent]+1)
                                * (part_geo->geo_order[icent]+2)/2;
        }
        /* the shape of the integrals for indices [idx_max_repeated+1,geo_num_cent]
           takes the following form:

           [size_total_geo][num_idx_geo][size_geo_integrand][num_idx_part][size_integrals]
                                 /|\                              |
                                  |_______________________________|
        */
        size_total_geo = size_geo_integrand;
        size_geo_integrand = 1;
        for (icent=part_geo->idx_max_repeated+1; icent<part_geo->geo_num_cent; icent++) {
            /* updates \var{size_integrals} */
            real_geo_order = part_geo->geo_order[icent]
                           + part_geo->trans_order_cent[part_geo->iter_trans][icent];
            size_integrals /= (real_geo_order+1)*(real_geo_order+2)/2;
            if (part_geo->trans_order_cent[part_geo->iter_trans][icent]>0) {
#if defined(GEN1INT_DEBUG)
                printf("PartGeoGetDeriv>>TriangleOrderLShift>> part_geo->geo_order[icent] %"GINT_FMT"\n",
                       part_geo->geo_order[icent]);
                printf("PartGeoGetDeriv>>TriangleOrderLShift>> real_order_max %"GINT_FMT"\n",
                       real_order_max);
                printf("PartGeoGetDeriv>>TriangleOrderLShift>> trans_order_cent[icent] %"GINT_FMT"\n",
                       part_geo->trans_order_cent[part_geo->iter_trans][icent]);
#endif
                /* transfers the order of geometric derivatives of current differentiated center */
                if (real_geo_order!=part_geo->trans_order_cent[part_geo->iter_trans][icent] ||
                    real_order_max!=0 ||
                    size_geo_integrand!=1) {
#if defined(GEN1INT_DEBUG)
                    printf("PartGeoGetDeriv>>TriangleOrderLShift>> size_total_geo %"GINT_FMT"\n",
                           size_total_geo);
                    printf("PartGeoGetDeriv>>TriangleOrderLShift>> size_geo_integrand %"GINT_FMT"\n",
                           size_geo_integrand);
                    printf("PartGeoGetDeriv>>TriangleOrderLShift>> size_integrals %"GINT_FMT"\n",
                           size_integrals);
#endif
                    ierr = TriangleOrderLShift(part_geo->geo_order[icent],
                                               real_order_max,
                                               part_geo->trans_order_cent[part_geo->iter_trans][icent],
                                               size_total_geo,
                                               size_geo_integrand,
                                               size_integrals,
                                               _p_val_inp,
                                               _p_val_out);
                    GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleOrderLShift");
                    /* switches input and output values */
                    *val_inp = _p_val_inp;
                    _p_val_inp = _p_val_out;
                    _p_val_out = *val_inp;
                }
                /* updates the order of center idx_max_repeated */
                real_order_max += part_geo->trans_order_cent[part_geo->iter_trans][icent];
            }
            /* updates \var{size_geo_integrand}  */
            size_geo_integrand *= (part_geo->geo_order[icent]+1)
                                * (part_geo->geo_order[icent]+2)/2;
        }
        /* checks the order of differentiated center with maximum repetitions in the integrand */
        if (real_order_max!=part_geo->order_max_repeated) {
            printf("PartGeoGetDeriv>> order of center with maximum repetitions %"GINT_FMT"\n",
                   part_geo->order_max_repeated);
            printf("PartGeoGetDeriv>> real order %"GINT_FMT"\n", real_order_max);
            GErrorExit(FILE_AND_LINE, "incorrect real order");
        }
    }
    /* assigns the output values */
    *val_out = _p_val_inp;
    return GSUCCESS;
}
