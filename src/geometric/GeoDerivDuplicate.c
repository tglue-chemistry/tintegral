/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivDuplicate().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief duplicates the context of geometric derivatives
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GeoDeriv:struct]{inout} dup_deriv the duplication, should not be
         created before, or be destroyed by GeoDerivDestroy()
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivDuplicate(const GeoDeriv *geo_deriv, GeoDeriv *dup_deriv)
{
    GInt how_many_ncent;  /* how many different numbers of differentiated centers */
    GInt icent;           /* incremental recorder */
    GErrorCode ierr;      /* error information */
    dup_deriv->assembled = geo_deriv->assembled;
    dup_deriv->order_geo = geo_deriv->order_geo;
    dup_deriv->range_num_cent[0] = geo_deriv->range_num_cent[0];
    dup_deriv->range_num_cent[1] = geo_deriv->range_num_cent[1];
    dup_deriv->user_range_idx[0] = geo_deriv->user_range_idx[0];
    dup_deriv->user_range_idx[1] = geo_deriv->user_range_idx[1];
    dup_deriv->num_user_idx = geo_deriv->num_user_idx;
    if (geo_deriv->user_idx!=NULL) {
        dup_deriv->user_idx = (GInt *)malloc(dup_deriv->num_user_idx*sizeof(GInt));
        if (dup_deriv->user_idx==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->user_idx");
        }
        for (icent=0; icent<dup_deriv->num_user_idx; icent++) {
            dup_deriv->user_idx[icent] = geo_deriv->user_idx[icent];
        }
    }
    else {
        dup_deriv->user_idx = NULL;
    }
    dup_deriv->curr_num_cent = geo_deriv->curr_num_cent;
    if (geo_deriv->combin_cent!=NULL) {
        dup_deriv->combin_cent = (BitCombin *)malloc(sizeof(BitCombin));
        if (dup_deriv->combin_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->combin_cent");
        }
        ierr = BitCombinDuplicate(geo_deriv->combin_cent, dup_deriv->combin_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinDuplicate");
    }
    else {
        dup_deriv->combin_cent = NULL;
    }
    if (geo_deriv->combin_order!=NULL) {
        dup_deriv->combin_order = (BitCombin *)malloc(sizeof(BitCombin));
        if (dup_deriv->combin_order==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->combin_order");
        }
        ierr = BitCombinDuplicate(geo_deriv->combin_order, dup_deriv->combin_order);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinDuplicate");
    }
    else {
        dup_deriv->combin_order = NULL;
    }
    dup_deriv->rank_combin_cent = geo_deriv->rank_combin_cent;
    dup_deriv->rank_combin_order = geo_deriv->rank_combin_order;
    if (geo_deriv->combin_idx_cent!=NULL) {
        dup_deriv->combin_idx_cent = (GInt *)malloc(geo_deriv->range_num_cent[1]*sizeof(GInt));
        if (dup_deriv->combin_idx_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->combin_idx_cent");
        }
        for (icent=0; icent<geo_deriv->range_num_cent[1]; icent++) {
            dup_deriv->combin_idx_cent[icent] = geo_deriv->combin_idx_cent[icent];
        }
    }
    else {
        dup_deriv->combin_idx_cent = NULL;
    }
    if (geo_deriv->combin_order_cent!=NULL) {
        dup_deriv->combin_order_cent = (GInt *)malloc(geo_deriv->range_num_cent[1]*sizeof(GInt));
        if (dup_deriv->combin_order_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->combin_order_cent");
        }
        for (icent=0; icent<geo_deriv->range_num_cent[1]; icent++) {
            dup_deriv->combin_order_cent[icent] = geo_deriv->combin_order_cent[icent];
        }
    }
    else {
        dup_deriv->combin_order_cent = NULL;
    }
    /* computes how many different numbers of differentiated centers */
    how_many_ncent = geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1;
    if (geo_deriv->num_combin_cent!=NULL) {
        dup_deriv->num_combin_cent = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (dup_deriv->num_combin_cent==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->num_combin_cent");
        }
        for (icent=0; icent<how_many_ncent; icent++) {
            dup_deriv->num_combin_cent[icent] = geo_deriv->num_combin_cent[icent];
        }
    }
    else {
        dup_deriv->num_combin_cent = NULL;
    }
    if (geo_deriv->num_combin_order!=NULL) {
        dup_deriv->num_combin_order = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (dup_deriv->num_combin_order==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->num_combin_order");
        }
        for (icent=0; icent<how_many_ncent; icent++) {
            dup_deriv->num_combin_order[icent] = geo_deriv->num_combin_order[icent];
        }
    }
    else {
        dup_deriv->num_combin_order = NULL;
    }
    if (geo_deriv->cent_size_combin!=NULL) {
        dup_deriv->cent_size_combin = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (dup_deriv->cent_size_combin==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->cent_size_combin");
        }
        for (icent=0; icent<how_many_ncent; icent++) {
            dup_deriv->cent_size_combin[icent] = geo_deriv->cent_size_combin[icent];
        }
    }
    else {
        dup_deriv->cent_size_combin = NULL;
    }
    dup_deriv->size_combin = geo_deriv->size_combin;
    if (geo_deriv->cent_size_deriv!=NULL) {
        dup_deriv->cent_size_deriv = (GULong *)malloc(how_many_ncent*sizeof(GULong));
        if (dup_deriv->cent_size_deriv==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for dup_deriv->cent_size_deriv");
        }
        for (icent=0; icent<how_many_ncent; icent++) {
            dup_deriv->cent_size_deriv[icent] = geo_deriv->cent_size_deriv[icent];
        }
    }
    else {
        dup_deriv->cent_size_deriv = NULL;
    }
    dup_deriv->size_deriv = geo_deriv->size_deriv;
    return GSUCCESS;
}
