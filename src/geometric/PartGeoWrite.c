/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoWrite().

   2014-09-29, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief writes the context of partial geometric derivatives
    \author Bin Gao
    \date 2014-09-29
    \param[PartGeo:struct]{in} part_geo the context of partial geometric derivatives
    \param[FILE]{in} fp_part_geo file pointer
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoWrite(const PartGeo *part_geo, FILE *fp_part_geo)
{
    GULong iexp;       /* incremental recorder of terms in multinomial expansion */
    GInt ptr_idx;      /* pointer to the index of the center */
    GInt icent,jcent;  /* incremental recorders over centers */
    GErrorCode ierr;   /* error information */
    if (part_geo->created==GTRUE) {
        fprintf(fp_part_geo,
                "PartGeoWrite>> number of differentiated centers %"GINT_FMT"\n",
                part_geo->geo_num_cent);
        fprintf(fp_part_geo,
                "PartGeoWrite>> No.   order   repetitions   indices\n");
        for (icent=0,ptr_idx=0; icent<part_geo->geo_num_cent; icent++) {
            fprintf(fp_part_geo,
                    "PartGeoWrite>> %"GINT_FMT"     %"GINT_FMT"       %"GINT_FMT"            ",
                    icent,
                    part_geo->geo_order[icent],
                    part_geo->geo_num_repeated[icent]);
            for (jcent=0; jcent<part_geo->geo_num_repeated[icent]; jcent++,ptr_idx++) {
                fprintf(fp_part_geo,
                        " %"GINT_FMT"",
                        part_geo->geo_idx_integrand[ptr_idx]);
            }
            fprintf(fp_part_geo, "\n");
        }
        if (part_geo->trans_invariance==GTRUE) {
            fprintf(fp_part_geo, "PartGeoWrite>> translational invariance used\n");
            fprintf(fp_part_geo,
                    "PartGeoWrite>> number of expansion terms for translational invariance %lu\n",
                    part_geo->trans_num_terms);
            fprintf(fp_part_geo,
                    "PartGeoWrite>> No.   multinomial-coefficients   orders-of-each-expansion-term\n");
            for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
                fprintf(fp_part_geo,
                        "PartGeoWrite>> %lu                %lu              ",
                        iexp,
                        part_geo->trans_mult_coef[iexp]);
                for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                    fprintf(fp_part_geo,
                            " %"GINT_FMT"",
                            part_geo->trans_order_cent[iexp][icent]);
                }
                fprintf(fp_part_geo, "\n");
            }
            fprintf(fp_part_geo,
                    "PartGeoWrite>> iterator for translational invariance %lu\n",
                    part_geo->iter_trans);
        }
        for (iexp=0; iexp<part_geo->trans_num_terms; iexp++) {
            for (icent=0; icent<part_geo->geo_num_cent; icent++) {
                fprintf(fp_part_geo,
                        "PartGeoWrite>> size of multinomial expansion (%lu,%"GINT_FMT") %lu\n",
                        iexp,
                        icent,
                        part_geo->size_mult_exp[iexp][icent]);
                ierr = MultExpWrite(&part_geo->mult_exp[iexp][icent], fp_part_geo);
                GErrorCheckCode(ierr, FILE_AND_LINE, "calling MultExpWrite");
            }
        }
        fprintf(fp_part_geo, "PartGeoWrite>> iterators for the multinomial expansions");
        for (icent=0; icent<part_geo->geo_num_cent; icent++) {
            fprintf(fp_part_geo, " %lu", part_geo->iter_mult_exp[icent]);
        }
        fprintf(fp_part_geo, "\n");
        fprintf(fp_part_geo,
                "PartGeoWrite>> size of partial geometric derivatives %lu\n",
                part_geo->size_part_geo);
    }
    else {
        fprintf(fp_part_geo,
                "PartGeoWrite>> context of partial geometric derivatives not created\n");
    }
    return GSUCCESS;
}
