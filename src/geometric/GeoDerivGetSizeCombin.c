/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivGetSizeCombin().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief gets the size of all the combinations of differentiated centers and
         order partition
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GULong:unsigned long]{out} size_combin the size of all the combinations
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivGetSizeCombin(const GeoDeriv *geo_deriv, GULong *size_combin)
{
    if (geo_deriv->assembled==GFALSE) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    *size_combin = geo_deriv->size_combin;
    return GSUCCESS;
}
