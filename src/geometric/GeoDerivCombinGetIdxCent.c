/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivCombinGetIdxCent().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief gets the indices of differentiated centers for the current combination
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} num_cent number of differentiated centers
     \param[GInt:int]{out} idx_cent indices of differentiated centers
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivCombinGetIdxCent(const GeoDeriv *geo_deriv,
                                    const GInt num_cent,
                                    GInt *idx_cent)
{
    GInt icent;
    if (geo_deriv->assembled==GFALSE) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    if (geo_deriv->curr_num_cent==0 || geo_deriv->curr_num_cent!=num_cent) {
        printf("GeoDerivCombinGetIdxCent>> number of differentiated centers %"GINT_FMT"\n",
               geo_deriv->curr_num_cent);
        printf("GeoDerivCombinGetIdxCent>> input number of differentiated centers %"GINT_FMT"\n",
               num_cent);
        GErrorExit(FILE_AND_LINE, "invalid input number");
    }
    for (icent=0; icent<geo_deriv->curr_num_cent; icent++) {
        idx_cent[icent] = geo_deriv->combin_idx_cent[icent];
    }
    return GSUCCESS;
}
