/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivCreate().

   2014-06-29, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief creates the context of geometric derivatives
     \author Bin Gao
     \date 2014-06-29
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} order_geo order of geometric derivatives
     \param[GInt:int]{in} range_num_cent range of number of differentiated centers
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivCreate(GeoDeriv *geo_deriv,
                          const GInt order_geo,
                          const GInt range_num_cent[2])
{
    /* checks the validity of arguments */
    if (order_geo<0) {
        printf("GeoDerivCreate>> order of geometric derivatives %"GINT_FMT"\n", order_geo);
        GErrorExit(FILE_AND_LINE, "invalid order of geometric derivatives");
    }
    if ((range_num_cent[0]<1 ||
         range_num_cent[0]>range_num_cent[1] ||
         range_num_cent[1]>order_geo) && order_geo>0) {
        printf("GeoDerivCreate>> range of the number of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
               range_num_cent[0],
               range_num_cent[1]);
        GErrorExit(FILE_AND_LINE, "invalid range");
    }
    geo_deriv->assembled = GFALSE;
    geo_deriv->order_geo = order_geo;
    geo_deriv->range_num_cent[0] = range_num_cent[0];
    geo_deriv->range_num_cent[1] = range_num_cent[1];
    geo_deriv->user_range_idx[0] = GMAX_IDX_NAT;
    geo_deriv->user_range_idx[1] = GMAX_IDX_NAT-1;
    geo_deriv->num_user_idx = 0;
    geo_deriv->user_idx = NULL;
    geo_deriv->curr_num_cent = 0;
    geo_deriv->combin_cent = NULL;
    geo_deriv->combin_order = NULL;
    geo_deriv->rank_combin_cent = 0;
    geo_deriv->rank_combin_order = 0;
    geo_deriv->combin_idx_cent = NULL;
    geo_deriv->combin_order_cent = NULL;
    geo_deriv->num_combin_cent = NULL;
    geo_deriv->num_combin_order = NULL;
    geo_deriv->cent_size_combin = NULL;
    geo_deriv->size_combin = 0;
    geo_deriv->cent_size_deriv = NULL;
    geo_deriv->size_deriv = 0;
    return GSUCCESS;
}
