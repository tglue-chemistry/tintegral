/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivGetUserIdx().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief gets the indices of user specified differentiated centers
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} num_user_idx the number of user specified differentiated centers
     \param[GInt:int]{out} user_idx indices of user specified differentiated centers
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivGetUserIdx(const GeoDeriv *geo_deriv,
                              const GInt num_user_idx,
                              GInt *user_idx)
{
    GInt icent;
    if (num_user_idx!=geo_deriv->num_user_idx) {
        printf("GeoDerivGetUserIdx>> number of user specified differentiated centers %"GINT_FMT"\n",
               geo_deriv->num_user_idx);
        printf("GeoDerivGetUserIdx>> input number %"GINT_FMT"\n", num_user_idx);
        GErrorExit(FILE_AND_LINE, "invalid input number");
    }
    if (geo_deriv->user_idx==NULL) {
        GErrorExit(FILE_AND_LINE, "user specified differentiated centers not given");
    }
    for (icent=0; icent<geo_deriv->num_user_idx; icent++) {
        user_idx[icent] = geo_deriv->user_idx[icent];
    }
    return GSUCCESS;
}
