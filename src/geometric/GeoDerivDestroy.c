/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivDestroy().

   2014-06-29, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief destroys the context of geometric derivatives
     \author Bin Gao
     \date 2014-06-29
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivDestroy(GeoDeriv *geo_deriv)
{
    GErrorCode ierr;  /* error information */
    geo_deriv->assembled = GFALSE;
    geo_deriv->order_geo = 0;
    geo_deriv->range_num_cent[0] = 0;
    geo_deriv->range_num_cent[1] = 0;
    geo_deriv->user_range_idx[0] = GMAX_IDX_NAT;
    geo_deriv->user_range_idx[1] = GMAX_IDX_NAT-1;
    geo_deriv->num_user_idx = 0;
    if (geo_deriv->user_idx!=NULL) {
        free(geo_deriv->user_idx);
        geo_deriv->user_idx = NULL;
    }
    geo_deriv->curr_num_cent = 0;
    if (geo_deriv->combin_cent!=NULL) {
        ierr = BitCombinDestroy(geo_deriv->combin_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinDestroy");
        free(geo_deriv->combin_cent);
        geo_deriv->combin_cent = NULL;
    }
    if (geo_deriv->combin_order!=NULL) {
        ierr = BitCombinDestroy(geo_deriv->combin_order);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinDestroy");
        free(geo_deriv->combin_order);
        geo_deriv->combin_order = NULL;
    }
    geo_deriv->rank_combin_cent = 0;
    geo_deriv->rank_combin_order = 0;
    if (geo_deriv->combin_idx_cent!=NULL) {
        free(geo_deriv->combin_idx_cent);
        geo_deriv->combin_idx_cent = NULL;
    }
    if (geo_deriv->combin_order_cent!=NULL) {
        free(geo_deriv->combin_order_cent);
        geo_deriv->combin_order_cent = NULL;
    }
    if (geo_deriv->num_combin_cent!=NULL) {
        free(geo_deriv->num_combin_cent);
        geo_deriv->num_combin_cent = NULL;
    }
    if (geo_deriv->num_combin_order!=NULL) {
        free(geo_deriv->num_combin_order);
        geo_deriv->num_combin_order = NULL;
    }
    if (geo_deriv->cent_size_combin!=NULL) {
        free(geo_deriv->cent_size_combin);
        geo_deriv->cent_size_combin = NULL;
    }
    geo_deriv->size_combin = 0;
    if (geo_deriv->cent_size_deriv!=NULL) {
        free(geo_deriv->cent_size_deriv);
        geo_deriv->cent_size_deriv = NULL;
    }
    geo_deriv->size_deriv = 0;
    return GSUCCESS;
}
