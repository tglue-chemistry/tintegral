/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivSetRangeIdx().

   2014-07-06, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief sets the range of indices of differentiated centers
     \author Bin Gao
     \date 2014-07-06
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} user_range_idx given range of indices of differentiated centers
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivSetRangeIdx(GeoDeriv *geo_deriv, const GInt user_range_idx[2])
{
    /* checks the validity of arguments */
    if (user_range_idx[0]<GMIN_IDX_ATOM || user_range_idx[0]>user_range_idx[1]) {
        printf("GeoDerivSetRangeIdx>> range of indices of differentiated centers [%"GINT_FMT",%"GINT_FMT"]\n",
               user_range_idx[0],
               user_range_idx[1]);
        GErrorExit(FILE_AND_LINE, "invalid range");
    }
    geo_deriv->assembled = GFALSE;
    geo_deriv->user_range_idx[0] = user_range_idx[0];
    geo_deriv->user_range_idx[1] = user_range_idx[1];
    geo_deriv->num_user_idx = 0;
    if (geo_deriv->user_idx!=NULL) {
        free(geo_deriv->user_idx);
        geo_deriv->user_idx = NULL;
    }
    return GSUCCESS;
}
