/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivGetCentSizeCombin().

   2014-07-07, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief gets the size of combinations of differentiated centers and order
         partition for specific number of differentiated centers
     \author Bin Gao
     \date 2014-07-07
     \param[GeoDeriv:struct]{in} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} num_cent number of different numbers of differentiated
         centers, as geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1
     \param[GULong:unsigned long]{out} cent_size_combin the size of combinations
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivGetCentSizeCombin(const GeoDeriv *geo_deriv,
                                     const GInt num_cent,
                                     GULong *cent_size_combin)
{
    GInt inum;
    if (geo_deriv->assembled==GFALSE) {
        GErrorExit(FILE_AND_LINE, "geometric derivatives are not assembled");
    }
    if (geo_deriv->cent_size_combin==NULL) {
        GErrorExit(FILE_AND_LINE, "size of combinations not computed");
    }
    if (num_cent!=(geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1)) {
        printf("GeoDerivGetCentSizeCombin>> number of different numbers of differentiated centers %"GINT_FMT"\n",
               geo_deriv->range_num_cent[1]-geo_deriv->range_num_cent[0]+1);
        printf("GeoDerivGetCentSizeCombin>> input number %"GINT_FMT"\n", num_cent);
        GErrorExit(FILE_AND_LINE, "invalid input number");
    }
    for (inum=0; inum<num_cent; inum++) {
        cent_size_combin[inum] = geo_deriv->cent_size_combin[inum];
    }
    return GSUCCESS;
}
