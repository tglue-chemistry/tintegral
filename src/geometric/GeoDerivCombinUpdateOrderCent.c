/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivCombinUpdateOrderCent().

   2014-07-06, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief updates the orders of centers for a specific combination
    \author Bin Gao
    \date 2014-07-06
    \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
    \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivCombinUpdateOrderCent(GeoDeriv *geo_deriv)
{
    GInt inum;        /* incremental recorder */
    GErrorCode ierr;  /* error information */
    /* gets the order partition for the current combination */
    ierr = BitCombinGetIdxFromRange(geo_deriv->combin_order,
                                    0,
                                    geo_deriv->combin_order_cent);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling BitCombinGetIdxFromRange");
    /* updates the orders of centers */
    geo_deriv->combin_order_cent[geo_deriv->curr_num_cent-1] = geo_deriv->order_geo-1;
    for (inum=geo_deriv->curr_num_cent-2; inum>=0; inum--) {
        geo_deriv->combin_order_cent[inum+1] -= geo_deriv->combin_order_cent[inum];
    }
    geo_deriv->combin_order_cent[0]++;
    return GSUCCESS;
}
