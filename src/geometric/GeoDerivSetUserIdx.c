/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function GeoDerivSetUserIdx().

   2014-07-06, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*@% \brief sets the user specified differentiated centers
     \author Bin Gao
     \date 2014-07-06
     \param[GeoDeriv:struct]{inout} geo_deriv the context of geometric derivatives
     \param[GInt:int]{in} num_user_idx number of given differentiated centers
     \param[GInt:int]{in} user_idx the indices of given differentiated centers
     \param[GBool:enum]{in} sort_idx indicates if sorting the indices
     \return[GErrorCode:int] error information
*/
GErrorCode GeoDerivSetUserIdx(GeoDeriv *geo_deriv,
                              const GInt num_user_idx,
                              const GInt *user_idx,
                              const GBool sort_idx)
{
    GInt icent;
    /* assigns the number of given differentiated centers */
    if (num_user_idx<1) {
        printf("GeoDerivSetUserIdx>> number of given differentiated centers %"GINT_FMT"\n",
               num_user_idx);
        GErrorExit(FILE_AND_LINE, "invalid number");
    }
    geo_deriv->assembled = GFALSE;
    geo_deriv->num_user_idx = num_user_idx;
    /* allocates memory */
    if (geo_deriv->user_idx!=NULL) free(geo_deriv->user_idx);
    geo_deriv->user_idx = (GInt *)malloc(geo_deriv->num_user_idx*sizeof(GInt));
    if (geo_deriv->user_idx==NULL) {
        printf("GeoDerivSetUserIdx>> number of given differentiated centers %"GINT_FMT"\n",
               geo_deriv->num_user_idx);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for user_idx");
    }
    for (icent=0; icent<geo_deriv->num_user_idx; icent++) {
        if (user_idx[icent]<GMIN_IDX_ATOM) {
            printf("GeoDerivSetUserIdx>> given index %"GINT_FMT" (%"GINT_FMT")\n", user_idx[icent], icent);
            GErrorExit(FILE_AND_LINE, "invalid index");
        }
        geo_deriv->user_idx[icent] = user_idx[icent];
    }
    /* sorts the differentiated centers */
    if (sort_idx==GTRUE) {
        SortIdxAscending(geo_deriv->num_user_idx, geo_deriv->user_idx);
    }
    /* NB: we did not check if some of the given indices are the same; users should
       check by themselves */
    geo_deriv->user_range_idx[0] = GMAX_IDX_NAT;
    geo_deriv->user_range_idx[1] = GMAX_IDX_NAT-1;
    return GSUCCESS;
}
