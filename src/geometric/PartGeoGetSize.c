/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoGetSize().

   2014-09-16, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief gets size of partial geometric derivatives
    \author Bin Gao
    \date 2014-09-16
    \param[PartGeo:struct]{in} part_geo the context of partial geometric derivatives
    \param[GULong:unsigned long]{out} size_part_geo size of partial geometric derivatives
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoGetSize(const PartGeo *part_geo, GULong *size_part_geo)
{
    *size_part_geo = part_geo->size_part_geo;
    return GSUCCESS;
}
