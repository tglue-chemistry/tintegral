/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function PartGeoNextComposition().

   2014-09-16, Bin Gao:
   * first version
*/

#include "derivatives/gen1int_geometric.h"

/*% \brief moves to next composition of multinomial expansion
    \author Bin Gao
    \date 2014-09-16
    \param[PartGeo:struct]{inout} part_geo the context of partial geometric derivatives
    \return[GErrorCode:int] error information
*/
GErrorCode PartGeoNextComposition(PartGeo *part_geo)
{
    GInt icent,jcent;  /* incremental recorders over centers */
    /* moves to next composition */
    for (icent=0; icent<part_geo->geo_num_cent; icent++) {
        jcent = part_geo->iter_mult_exp[icent]+1;
        if (jcent<part_geo->size_mult_exp[part_geo->iter_trans][icent]) {
            part_geo->iter_mult_exp[icent] = jcent;
            for (jcent=0; jcent<icent; jcent++) {
                part_geo->iter_mult_exp[jcent] = 0;
            }
            break;
        }
    }
    /* all compositions visited */
    if (icent==part_geo->geo_num_cent) {
        /* moves to the next term of expansion of translational invariance */
        if (part_geo->trans_invariance==GTRUE) {
            part_geo->iter_trans++;
            /* all terms of expansion of translational invariance visited,
               starts from the first expansion again */
            if (part_geo->iter_trans==part_geo->trans_num_terms) {
                part_geo->iter_trans = 0;
            }
        }
        /* for each differentiated center, starts from the first expansion again */
        for (icent=0; icent<part_geo->geo_num_cent; icent++) {
            part_geo->iter_mult_exp[icent] = 0;
        }
    }
    return GSUCCESS;
}
