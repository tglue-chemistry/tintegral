/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates G functions.

   2022-02-28, Bin Gao:
   * first version
*/

#include <cmath>
#ifdef TINTEGRAL_CPU_TIME
#include <ctime>
#endif

#if defined(TINTEGRAL_CPU_TIME) || defined(TINTEGRAL_DEBUG)
#include "tGlueCore/Logger.hpp"
#include "tIntegral/Settings.hpp"
#endif

#include "tIntegral/MathFunction.hpp"

namespace tIntegral
{
    std::valarray<double> get_gfun(const int maxOrder, const std::valarray<double>& args) noexcept
    {
        std::valarray<double> vals((maxOrder+1)*args.size());
        auto iter_val = std::begin(vals);
        /* Iterator of the Boys function with the maximum order */
        auto last_val = iter_val+maxOrder;
        for (const double& arg: args) {
#ifdef TINTEGRAL_DEBUG
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "get_gfun() will compute up to order ",
                                    maxOrder,
                                    " with the argument ",
                                    arg);
#endif
            if (arg==0) {
                /* stem:[G_{n}(0)=\frac{1}{2n+1}] */
                for (int n=0; n<=maxOrder; ++n,++iter_val) *iter_val = 0.5/(n+0.5);
            }
            else {
                /* Rational approximation of the zeroth order G function */
#ifdef TINTEGRAL_CPU_TIME
                std::clock_t c_start = std::clock();
#endif
                if (arg<2852.7704985771850) {
                    if (arg<67) {
                        if (arg<=16) {
                            if (arg<8) {
                                /* (0, 1] */
                                if (arg<=1) {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                         -1.7007677275430473e-5,
                                                                         6.0395501528010746e-4),
                                                                -8.4223156297586130e-3),
                                                            1.0064562642904385e-1),
                                                        -3.8629038261673594e-1),
                                                    2.0643101942581892)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                    5.2256866390000000e-5,
                                                                    1.6810963783733405e-3),
                                                                2.4952206560163595e-2),
                                                            2.1010718366453333e-1),
                                                        9.8991641355536569e-1),
                                                    2.0643101942581895);
                                }
                                /* (1, 8) */
                                else {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                    std::fma(arg,
                                                                        std::fma(arg,
                                                                                 -4.7569856564083595e-9,
                                                                                 8.3024213060252765e-7),
                                                                        -4.7707578513699947e-6),
                                                                    7.0809761893120839e-4),
                                                                -3.6832514527760762e-3),
                                                            1.0627359133179816e-1),
                                                        -2.8258206512479540e-1),
                                                    2.5457772543554241)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                    std::fma(arg,
                                                                        std::fma(arg,
                                                                                 7.9071518377900000e-7,
                                                                                 2.6269823293656682e-5),
                                                                        5.0830485731077175e-4),
                                                                    6.6140211318006648e-3),
                                                                6.0032225084212247e-2),
                                                            3.7046809139002517e-1),
                                                        1.4146028020883928),
                                                    2.5457772489764175);
                                }
                            }
                            /* [8, 16] */
                            else {
                                *iter_val = std::fma(arg,
                                                std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                    std::fma(arg,
                                                                             7.9484136494167812e-11,
                                                                             -8.1139303316922493e-6),
                                                                    1.1939583820748837e-4),
                                                                -2.0521348154751012e-3),
                                                            1.1337314408491569e-2),
                                                        -8.9248163198338771e-2),
                                                    1.7580282812295735e-1),
                                                -9.4238241151887492e-1)
                                          / std::fma(arg,
                                                std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                std::fma(arg,
                                                                    std::fma(arg,
                                                                             -1.6204689965690000e-5,
                                                                             2.4540250098553264e-4),
                                                                    -4.1595706880234088e-3),
                                                                2.3318807736669890e-2),
                                                            -1.6848401243280139e-1),
                                                        2.5549940688065971e-1),
                                                    -1.1104346303182331),
                                                -4.5788406432083157e-1);
                            }
                        }
                        else {
                            if (arg<34) {
                                /* (16, 24) */
                                if (arg<24) {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                     1.2349864818591871e-10,
                                                                     6.1805151463726767e-4),
                                                            -1.3990000668980137e-2),
                                                        1.2173104188968433e-1),
                                                    -2.8471255191647697e-1)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                            std::fma(arg,
                                                                     1.2361403778703100e-3,
                                                                     -2.8600455080271414e-2),
                                                            2.5691751575811024e-1),
                                                        -6.8037517119093825e-1),
                                                    2.1031538981593906e-1);
                                }
                                /* [24, 34) */
                                else {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                                 -1.7503497192672269e-11,
                                                                 -1.7883337206139357e-3),
                                                        2.0262350201889137e-2),
                                                    -3.8626775388586456e-2)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                        std::fma(arg,
                                                                 -3.5766738246917600e-3,
                                                                 4.2313519079773019e-2),
                                                        -9.5746927616484515e-2),
                                                    2.3257772570770000e-2);
                                }
                            }
                            /* [34, 67) */
                            else {
                                *iter_val = std::fma(arg,
                                                std::fma(arg,
                                                    std::fma(arg,
                                                             9.6206213042739119e-13,
                                                             9.8664278707879177e-3),
                                                    -8.6328058711934851e-2),
                                                1.2925966978262159e-1)
                                          / std::fma(arg,
                                                std::fma(arg,
                                                    std::fma(arg,
                                                             1.9732856346329600e-2,
                                                             -1.8252262478745419e-1),
                                                    3.3498654975424099e-1),
                                                -6.7819081061841745e-2);
                            }
                        }
                    }
                    else {
                        if (arg<259) {
                            if (arg<109) {
                                /* [67, 83) */
                                if (arg<83) {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                             4.0615149054018683e-12,
                                                             4.4123900703206288e-3),
                                                    -1.2044852158361270e-2)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                             8.8247831030473000e-3,
                                                             -2.8502522748953134e-2),
                                                    7.6627267959539512e-3);
                                }
                                /* [83, 109) */
                                else {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                             1.8005648809860893e-12,
                                                             7.0614302083779570e-3),
                                                    -1.8893405215373377e-2)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                             1.4122862100333160e-2,
                                                             -4.4848552961199237e-2),
                                                    1.1860396897302808e-2);
                                }
                            }
                            else {
                                /* [109, 156) */
                                if (arg<156) {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                             6.0169750206563260e-13,
                                                             1.2066511105862192e-2),
                                                    -3.1680155559659932e-2)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                             2.4133022986692930e-2,
                                                             -7.5427020229035079e-2),
                                                    1.9638533548747437e-2);
                                }
                                /* [156, 259) */
                                else {
                                    *iter_val = std::fma(arg,
                                                    std::fma(arg,
                                                             1.3273105662296004e-13,
                                                             2.3738491806747545e-2),
                                                    -6.1261477652645210e-2)
                                              / std::fma(arg,
                                                    std::fma(arg,
                                                             4.7476983878142560e-2,
                                                             -1.4626155151059350e-1),
                                                    3.7543251226434540e-2);
                                }
                            }
                        }
                        else {
                            /* [259, 599) */
                            if (arg<599) {
                                *iter_val = std::fma(arg,
                                                std::fma(arg,
                                                         1.3157111091290888e-14,
                                                         6.2577018441519300e-2),
                                                -1.5910464831633610e-1)
                                          / std::fma(arg,
                                                std::fma(arg,
                                                         1.2515403693463100e-1,
                                                         -3.8078635457426909e-1),
                                                9.6542358930782610e-2);
                            }
                            /* [599, 2852.7704985771850) */
                            else {
                                *iter_val = std::fma(arg,
                                                std::fma(arg,
                                                         1.5376713492288760e-16,
                                                         1.9579745505168540e-1),
                                                -4.9246474597024198e-1)
                                          / std::fma(arg,
                                                std::fma(arg,
                                                         3.9159491010542682e-1,
                                                         -1.1807269520186981),
                                                2.9667295573919481e-1);
                            }
                        }
                    }
                }
                /* Asymptotic approximation of the zeroth order G function
                   stem:[\tfrac{1}{2}\sum_{j=0}^{J}\left(\tfrac{1}{2}\right)^{(j)}x^{-(j+1)}] */
                else {
                    double val_term = 1.0/arg;
                    if (arg<2743185.9183512875) {
                        if (arg<26616.804866920717) {
                            *iter_val = std::fma(
                                            std::fma(std::fma(0.9375, val_term, 0.375), val_term, 0.25),
                                            val_term,
                                            0.5)
                                      * val_term;
                        }
                        else {
                            *iter_val = std::fma(std::fma(0.375, val_term, 0.25), val_term, 0.5)*val_term;
                        }
                    }
                    else {
                        if (arg<5005557284949.8298) {
                            *iter_val = std::fma(0.25, val_term, 0.5)*val_term;
                        }
                        else {
                            *iter_val = 0.5*val_term;
                        }
                    }
                }
#ifdef TINTEGRAL_CPU_TIME
                std::clock_t c_end = std::clock();
                long double time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
                Settings::logger->write(tGlueCore::MessageType::Output,
                                        "CPU time used for the zeroth order G function: ",
                                        time_elapsed_ms,
                                        " ms");
#endif
#ifdef TINTEGRAL_DEBUG
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "get_gfun() gets the zeroth order G function ",
                                        *iter_val,
                                        " for the argument ",
                                        arg);
#endif
                if (maxOrder>0) {
                    /* Find the maximum order of the upward recurrence relation
                       using binary search algorithm */
#ifdef TINTEGRAL_CPU_TIME
                    c_start = std::clock();
#endif
                    int nup_max;

                    int n_upward = maxOrder<=nup_max ? maxOrder : nup_max;
#ifdef TINTEGRAL_CPU_TIME
                    c_end = std::clock();
                    time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
                    Settings::logger->write(
                        tGlueCore::MessageType::Output,
                        "CPU time used for computing the maximum order of upward recurrence relation: ",
                        time_elapsed_ms,
                        " ms"
                    );
#endif
#ifdef TINTEGRAL_DEBUG
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "get_gfun() gets the maximum order of upward recurrence relation ",
                                            n_upward,
                                            " for the argument ",
                                            arg);
#endif
                    /* Compute the n-th order G function by using the upward
                       recurrence relation */
#ifdef TINTEGRAL_CPU_TIME
                    c_start = std::clock();
#endif
                    double arg_inverse = 1.0/arg;
                    for (int n=0; n<n_upward; ++n,++iter_val) {
                        *(iter_val+1) = std::fma(-(n+0.5), *iter_val, 0.5)*arg_inverse;
                    }
#ifdef TINTEGRAL_CPU_TIME
                    c_end = std::clock();
                    time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
                    Settings::logger->write(tGlueCore::MessageType::Output,
                                            "CPU time used for upward recurrence relation: ",
                                            time_elapsed_ms,
                                            " ms");
#endif
                    if (maxOrder>nup_max) {
                        /* Power series according to approximate algorithm error */
#ifdef TINTEGRAL_CPU_TIME
                        c_start = std::clock();
#endif
                        double val_divisor = maxOrder+0.5;
                        double val_term = 0.5/val_divisor;
                        *last_val = val_term;
                        val_divisor += 1.0;
                        val_term *= -arg/val_divisor;
                        while (std::abs(val_term)>1e-13*(*last_val)) {
                            *last_val += val_term;
                            val_divisor += 1.0;
                            val_term *= -arg/val_divisor;
                        }
#ifdef TINTEGRAL_CPU_TIME
                        c_end = std::clock();
                        time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
                        Settings::logger->write(tGlueCore::MessageType::Output,
                                                "CPU time used for power series: ",
                                                time_elapsed_ms,
                                                " ms");
#endif
#ifdef TINTEGRAL_DEBUG
                        Settings::logger->write(tGlueCore::MessageType::Debug,
                                                "get_gfun() gets G function ",
                                                *last_val,
                                                " of the order ",
                                                maxOrder,
                                                " and the argument ",
                                                arg,
                                                " by using power series");
#endif
                        /* Compute the n-th order G function by using the
                           downward recurrence relation */
#ifdef TINTEGRAL_CPU_TIME
                        c_start = std::clock();
#endif
                        iter_val = last_val-1;
                        for (int n=maxOrder-1; n>n_upward; --n,--iter_val) {
                            *iter_val = std::fma(-arg, *(iter_val+1), 0.5)/(n+0.5);
                        }
#ifdef TINTEGRAL_CPU_TIME
                        c_end = std::clock();
                        time_elapsed_ms = 1000.0*(c_end-c_start)/CLOCKS_PER_SEC;
                        Settings::logger->write(tGlueCore::MessageType::Output,
                                                "CPU time used for downward recurrence relation: ",
                                                time_elapsed_ms,
                                                " ms");
#endif
                    }
                }
            }
            iter_val = last_val+1;
            last_val = iter_val+maxOrder;
        }
        return vals;
    }
}
