/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements kernel and helper functions to compute Boys functions
   by using CUDA.

   2022-03-02, Bin Gao:
   * first version
*/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "tIntegral/MathFunction.hpp"

__global__ void boys_kernel(const int maxOrder, const int sizeArgs, const double* args, double* vals)
{
    int index = threadIdx.x+blockIdx.x*blockDim.x;
    if (index<sizeArgs) {
        /* Values of Boys functions of the first argument are placed in
           contiguous memory with orders from 0 to \var(maxOrder), then the
           second argument, and so on. */
        double* iter_val = vals+index*(maxOrder+1);
        /* Iterator of the Boys function with the maximum order */
        double* last_val = iter_val+maxOrder;
        if (args[index]==0) {
            /* stem:[F_{n}(0)=\frac{1}{2n+1}] */
            for (int n=0; n<=maxOrder; ++n,++iter_val) *iter_val = 0.5/(n+0.5);
        }
        else {
            /* Evaluate the zeroth order Boys function */
            *iter_val = boys0(args[index]);
            if (maxOrder>0) {
                /* Get the maximum order of the upward recurrence relation */
                int n_upmax = boys_up_max(args[index]);
                int n_upward = maxOrder<=nup_max ? maxOrder : nup_max;
                double arg_inverse = 1.0/args[index];
                if (args[index]<=708) {
                    /* Compute the n-th order Boys function by using the upward
                       recurrence relation */
                    double exp_factor = 0.5*exp(-args[index]);
                    double val_exp = -exp_factor*arg_inverse;
                    for (int n=0; n<n_upward; ++n,++iter_val) {
                        *(iter_val+1) = fma((n+0.5)*arg_inverse, *iter_val, val_exp);
                    }
                    if (maxOrder>nup_max) {
                        /* Power series according to approximate algorithm error */
                        double val_divisor = maxOrder+0.5;
                        double val_term = 1.0/val_divisor;
                        *last_val = val_term;
                        val_divisor += 1.0;
                        val_term *= args[index]/val_divisor;
                        while (val_term>1e-13*(*last_val)) {
                            *last_val += val_term;
                            val_divisor += 1.0;
                            val_term *= args[index]/val_divisor;
                        }
                        *last_val *= exp_factor;
                        /* Compute the n-th order Boys function by using the
                           downward recurrence relation */
                        iter_val = last_val-1;
                        for (int n=maxOrder-1; n>n_upward; --n,--iter_val) {
                            *iter_val = fma(args[index], *(iter_val+1), exp_factor)/(n+0.5);
                        }
                    }
                }
                else {
                    /* Asymptotic approximation of Boys functions
                       stem:[\frac{\left(\frac{1}{2}\right)^{(n)}\sqrt{\pi}}{2x^{n+\frac{1}{2}}}] */
                    for (int n=0; n<n_upward; ++n,++iter_val) *(iter_val+1) = (n+0.5)*(*iter_val)*arg_inverse;
                    /* Underflow */
                    for (int n=n_upward+1; n<=maxOrder; ++n) {
                        ++iter_val;
                        *iter_val = 0.0;
                    }
                }
            }
        }
    }
}

namespace tIntegral
{
    //std::valarray<double> get_boys(const int maxOrder, const std::valarray<double>& args) noexcept
    void get_boys(const int maxOrder, const int numArgs, const double* args, double* vals) noexcept
    {
        /* Values of Boys functions of the first argument are placed in
           contiguous memory with orders from 0 to \var(maxOrder), then the
           second argument, and so on. */
        const int size_vals = numArgs*(maxOrder+1);
        //std::valarray<double> vals((maxOrder+1)*args.size());
        double *d_args, *d_vals;
        /* Allocate device memory */
        //cudaMalloc((void**)&d_args, sizeof(double)*args.size());
        //cudaMalloc((void**)&d_vals, sizeof(double)*vals.size());
        cudaMalloc((void**)&d_args, sizeof(double)*numArgs);
        cudaMalloc((void**)&d_vals, sizeof(double)*size_vals);
        /* Transfer arguments from host to device memory */
        //cudaMemcpy(d_args, &(*std::begin(args)), sizeof(double)*args.size(), cudaMemcpyHostToDevice);
        cudaMemcpy(d_args, args, sizeof(double)*numArgs, cudaMemcpyHostToDevice);
        /* Define number of threads in each block */
        const int block_size = 1024;
        /* Number of thread blocks */
        //const int grid_size = int(ceil(double(args.size())/block_size));
        const int grid_size = int(ceil(double(numArgs)/block_size));
        /* Execute kernel */
        //boys_kernel<<<grid_size, block_size>>>(maxOrder, args.size(), d_args, d_vals);
        boys_kernel<<<grid_size, block_size>>>(maxOrder, numArgs, d_args, d_vals);
        /* Wait for the kernel to finish */
        cudaDeviceSynchronize();
        /* Transfer Boys functions back to host memory */
        ///cudaMemcpy(&(*std::begin(vals)), d_vals, sizeof(double)*vals.size(), cudaMemcpyDeviceToHost);
        cudaMemcpy(vals, d_vals, size_vals, cudaMemcpyDeviceToHost);
        /* Deallocate device memory */
        cudaFree(d_args);
        cudaFree(d_vals);
        //return vals;
    }
}
