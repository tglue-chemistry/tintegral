/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements kernel and helper functions to compute G functions by
   using CUDA.

   2022-03-02, Bin Gao:
   * first version
*/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "tIntegral/MathFunction.hpp"

__global__ void gfun_kernel(const int maxOrder, const int sizeArgs, const double* args, double* vals)
{
    int index = threadIdx.x+blockIdx.x*blockDim.x;
    if (index<sizeArgs) {
        /* Values of Boys functions of the first argument are placed in
           contiguous memory with orders from 0 to \var(maxOrder), then the
           second argument, and so on. */
        double* iter_val = vals+index*(maxOrder+1);
        /* Iterator of the Boys function with the maximum order */
        double* last_val = iter_val+maxOrder;
        if (args[index]==0) {
            /* stem:[F_{n}(0)=\frac{1}{2n+1}] */
            for (int n=0; n<=maxOrder; ++n,++iter_val) *iter_val = 0.5/(n+0.5);
        }
        else {
            /* Rational approximation of the zeroth order Boys function */
            if (args[index]<=14) {
                if (args[index]<7) {
                    if (args[index]<=1) {
                        *iter_val = fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                             7.7431289765679954e-6,
                                                             5.8013130589625492e-4),
                                                    8.6585028922695218e-3),
                                                1.0891198266892342e-1),
                                            4.2131665053371023e-1),
                                        2.2699378135344061)
                                  / fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                             1.0298955801000000e-4,
                                                             2.8048200242871830e-3),
                                                    3.6432514971616124e-2),
                                                2.7457239744000644e-1),
                                            1.1779625883785794),
                                        2.2699378135344052);
                    }
                    else {
                        *iter_val = fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                        fma(args[index],
                                                            fma(args[index],
                                                                     3.5960048693138910e-9,
                                                                     7.1643556269285254e-7),
                                                            1.5870305976743249e-5),
                                                        4.2490684401893097e-4),
                                                    3.7633480042344360e-3),
                                                4.9552928157715852e-2),
                                            1.6290857280445508e-1),
                                        1.0009574989081021)
                                  / fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                        fma(args[index],
                                                            fma(args[index],
                                                                     8.9566896774000000e-8,
                                                                     4.1127103555045216e-6),
                                                            9.8889354639715169e-5),
                                                        1.5377757781070642e-3),
                                                    1.6265407498397557e-2),
                                                1.1497753571617642e-1),
                                            4.9656107244530340e-1),
                                        1.0009574989081711);
                    }
                }
                else {
                    *iter_val = fma(args[index],
                                    fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                             7.9475160723469155e-8,
                                                             3.0021547331061438e-5),
                                                    8.0758016599267036e-4),
                                                5.7523103435856937e-3),
                                            9.2620173573973509e-2),
                                        2.6336316471821378e-1),
                                    1.8164260035138938)
                              / fma(args[index],
                                    fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                             2.6822538886900000e-6,
                                                             2.3110686851335755e-4),
                                                    2.2813747204096204e-3),
                                                3.0362848990260044e-2),
                                            1.9755461915739980e-1),
                                        8.7280278995828052e-1),
                                    1.8140634810945542);
                }
            }
            else {
                if (args[index]<28.187164187414548) {
                    *iter_val = fma(args[index],
                                    fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                        fma(args[index],
                                                                 -3.0879448640403457e-10,
                                                                 -3.5315086742141157e-7),
                                                        -3.4995109206623610e-5),
                                                    -2.0246703468529315e-4),
                                                9.2119565755768653e-3),
                                            -4.9325712476547629e-2),
                                        -1.5831764702387535e-3),
                                    2.3345239538654938)
                              / fma(args[index],
                                    fma(args[index],
                                        fma(args[index],
                                            fma(args[index],
                                                fma(args[index],
                                                    fma(args[index],
                                                        fma(args[index],
                                                                 -1.7759035156800000e-8,
                                                                 -5.0928707847441459e-6),
                                                        -1.7377005556847808e-4),
                                                    1.8290259077650788e-3),
                                                1.3627508719122853e-2),
                                            -2.1198429074023575e-1),
                                        1.2700844262750420),
                                    1.7880831432504771);
                }
                /* Asymptotic approximation of the zeroth order Boys function
                   stem:[\frac{1}{2}\sqrt{\frac{\pi}{x}}] */
                else {
                    *iter_val = 0.886226925452758/sqrt(args[index]);
                }
            }
            if (maxOrder>0) {
                /* Find the maximum order of the upward recurrence relation
                   using binary search algorithm */
                int nup_max;
                if (args[index]<=14) {
                    if (args[index]<=1) {
                        nup_max = args[index]<1.8227993151040120e-2
                            ? 0
                            : int(round(2.4626157599144523*args[index]+0.9014896211617622));
                    }
                    else {
                        nup_max = args[index]<7
                            ? int(round((-3.958731600436549e-2*args[index]+1.6776994192555685)*args[index]+1.815269629703272))
                            : int(round(1.1559382401308103*args[index]+3.78365305701053));
                    }
                }
                else {
                    if (args[index]<164.56821028423799) {
                        if (args[index]<28.187164187414548) {
                            nup_max = int(round(1.0277255883753522*args[index]+4.000253798587592));
                        }
                        else {
                            nup_max = args[index]<32.102511634612173
                                ? int(round((-6.358678844078643e-1*args[index]+41.76161090060233)*args[index]-645.8143359657548))
                                : int(round((-2.780445151604754e-4*args[index]+1.0224462243996915)*args[index]+7.642946358249691));
                        }
                    }
                    else {
                        if (args[index]<433.81659399125958) {
                            nup_max = int(round((-2.531999260846358e-4*args[index]+1.0407111502467767)*args[index]+3.617218884225029));
                        }
                        else {
                            nup_max =  args[index]<520.42323182048845
                                ? int(round((-4.421882296961987e-3*args[index]+4.737753666818327)*args[index]-817.6377123865682))
                                : 450;
                        }
                    }
                }
                int n_upward = maxOrder<=nup_max ? maxOrder : nup_max;
                double arg_inverse = 1.0/args[index];
                if (args[index]<=708) {
                    /* Compute the n-th order Boys function by using the upward
                       recurrence relation */
                    double exp_factor = 0.5*exp(-args[index]);
                    double val_exp = -exp_factor*arg_inverse;
                    for (int n=0; n<n_upward; ++n,++iter_val) {
                        *(iter_val+1) = fma((n+0.5)*arg_inverse, *iter_val, val_exp);
                    }
                    if (maxOrder>nup_max) {
                        /* Power series according to approximate algorithm error */
                        double val_divisor = maxOrder+0.5;
                        double val_term = 1.0/val_divisor;
                        *last_val = val_term;
                        val_divisor += 1.0;
                        val_term *= args[index]/val_divisor;
                        while (val_term>1e-13*(*last_val)) {
                            *last_val += val_term;
                            val_divisor += 1.0;
                            val_term *= args[index]/val_divisor;
                        }
                        *last_val *= exp_factor;
                        /* Compute the n-th order Boys function by using the
                           downward recurrence relation */
                        iter_val = last_val-1;
                        for (int n=maxOrder-1; n>n_upward; --n,--iter_val) {
                            *iter_val = fma(args[index], *(iter_val+1), exp_factor)/(n+0.5);
                        }
                    }
                }
                else {
                    /* Asymptotic approximation of Boys functions
                       stem:[\frac{\left(\frac{1}{2}\right)^{(n)}\sqrt{\pi}}{2x^{n+\frac{1}{2}}}] */
                    for (int n=0; n<n_upward; ++n,++iter_val) *(iter_val+1) = (n+0.5)*(*iter_val)*arg_inverse;
                    /* Underflow */
                    for (int n=n_upward+1; n<=maxOrder; ++n,++iter_val) *iter_val = 0;
                }
            }
        }
    }
}

namespace tIntegral
{
    std::valarray<double> gfun_v(const int maxOrder, const std::valarray<double>& args) noexcept
    {
        /* Values of Boys functions of the first argument are placed in
           contiguous memory with orders from 0 to \var(maxOrder), then the
           second argument, and so on. */
        std::valarray<double> vals((maxOrder+1)*args.size());
        double *d_args, *d_vals;
        /* Allocate device memory */
        cudaMalloc((void**)&d_args, sizeof(double)*args.size());
        cudaMalloc((void**)&d_vals, sizeof(double)*vals.size());
        /* Transfer arguments from host to device memory */
        cudaMemcpy(d_args, &(*std::begin(args)), sizeof(double)*args.size(), cudaMemcpyHostToDevice);
        /* Execute kernel */
        boys_kernel<<<,>>>(maxOrder, args.size(), d_args, d_vals);
        /* Wait for the kernel to finish */
        cudaDeviceSynchronize();
        /* Transfer Boys functions back to host memory */
        cudaMemcpy(&(*std::begin(vals)), d_vals, sizeof(double)*vals.size(), cudaMemcpyDeviceToHost);
        /* Deallocate device memory */
        cudaFree(d_args);
        cudaFree(d_vals);
        return vals;
    }
}
