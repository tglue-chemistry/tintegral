/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file evaluates Boys functions.

   2022-03-07, Bin Gao:
   * tried std::isnormal() to detect underflow in upward recurrence relation,
     power series and asymptotic upward recurrence relation, but the
     computational cost becomes larger, so we will not detect underflow during
     calculations but rather check if the values of Boys functions are zero or
     subnormal numbers during error verification step

   2022-02-26, Bin Gao:
   * first version
*/

#ifdef TINTEGRAL_DEBUG
#include <ctime>
#include "tGlueCore/Logger.hpp"
#include "tIntegral/Settings.hpp"
#endif

#include "tIntegral/MathFunction.hpp"

namespace tIntegral
{
    double _boys_inited = false;
    double _nhalf_inverse[451];

    void _init_boys() noexcept
    {
        for (int n=0; n<=450; ++n) _nhalf_inverse[n] = 1.0/double(n+0.5);
        _boys_inited = true;
#ifdef TINTEGRAL_DEBUG
        Settings::logger->write(tGlueCore::MessageType::Debug, "_init_boys() is called and terminates normally");
#endif
    }

    void get_boys(const int maxOrder, const int numArgs, const double* args, double* vals) noexcept
    {
        if (!_boys_inited) _init_boys();
#ifdef TINTEGRAL_DEBUG
        std::clock_t c_start, c_end;
#endif
        /* Values of Boys functions of the first argument are placed in
           contiguous memory with orders from 0 to \var(maxOrder), then the
           second argument, and so on. */
        auto val_iter = &vals[0];
        /* Iterator of the Boys function with the maximum order */
        auto val_last = val_iter+maxOrder;
        for (int i=0; i<numArgs; ++i) {
#ifdef TINTEGRAL_DEBUG
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "get_boys() will compute up to order ", maxOrder,
                                    " with the ", i, "-th argument ", args[i]);
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "Beginning address of Boys functions ",
                                    val_iter-&vals[0]);
#endif
            if (args[i]==0) {
                /* stem:[F_{n}(0)=\frac{1}{2n+1}] */
                for (int n=0; n<=maxOrder; ++n,++val_iter) *val_iter = 0.5*_nhalf_inverse[n];
            }
            else {
#ifdef TINTEGRAL_DEBUG
                c_start = std::clock();
#endif
                /* Evaluate the zeroth order Boys function */
                *val_iter = get_boys0(args[i]);
#ifdef TINTEGRAL_DEBUG
                c_end = std::clock();
                Settings::logger->write(tGlueCore::MessageType::Debug,
                                        "get_boys() gets the zeroth order Boys function ", *val_iter,
                                        " for the argument ", args[i],
                                        " and uses CPU time ",
                                        1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                if (maxOrder>0) {
                    /* Get the maximum order of the upward recurrence relation */
#ifdef TINTEGRAL_DEBUG
                    c_start = std::clock();
#endif
                    auto n_upmax = boys_max_upward(args[i]);
#ifdef TINTEGRAL_DEBUG
                    c_end = std::clock();
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "get_boys() gets the maximum order ", n_upmax,
                                            " of upward recurrence relation for the argument ", args[i],
                                            " and uses CPU time ",
                                            1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                    int n_upward = maxOrder<=n_upmax ? maxOrder : n_upmax;
                    double arg_inverse = 1.0/args[i];
                    if (args[i]<=708) {
                        /* Compute the n-th order Boys function by using the
                           upward recurrence relation */
#ifdef TINTEGRAL_DEBUG
                        c_start = std::clock();
#endif
                        double exp_factor = 0.5*std::exp(-args[i]);
                        double val_exp = -exp_factor*arg_inverse;
                        for (int n=0; n<n_upward; ++n) {
                            *(val_iter+1) = std::fma((n+0.5)*arg_inverse, *val_iter, val_exp);
                            ++val_iter;
                        }
#ifdef TINTEGRAL_DEBUG
                        c_end = std::clock();
                        Settings::logger->write(tGlueCore::MessageType::Debug,
                                                "get_boys() uses CPU time for upward recurrence relation ",
                                                1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                        if (n_upward<maxOrder) {
                            /* Power series according to approximate algorithm error */
#ifdef TINTEGRAL_DEBUG
                            c_start = std::clock();
#endif
                            double val_divisor = maxOrder+0.5;
                            double val_term = 1.0/val_divisor;
                            *val_last = val_term;
                            val_divisor += 1.0;
                            val_term *= args[i]/val_divisor;
                            while (val_term>1.0e-16*(*val_last)) {
                                *val_last += val_term;
                                val_divisor += 1.0;
                                val_term *= args[i]/val_divisor;
                            }
                            *val_last *= exp_factor;
#ifdef TINTEGRAL_DEBUG
                            c_end = std::clock();
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys() gets Boys function ", *val_last,
                                                    " of the order ", maxOrder,
                                                    " and the argument ", args[i],
                                                    " by using power series and uses CPU time ",
                                                    1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                            /* Compute the n-th order Boys function by using
                               the downward recurrence relation */
#ifdef TINTEGRAL_DEBUG
                            c_start = std::clock();
#endif
                            val_iter = val_last-1;
                            for (int n=maxOrder-1; n>n_upward; --n) {
                                *val_iter = std::fma(args[i], *(val_iter+1), exp_factor)*_nhalf_inverse[n];
                                --val_iter;
                            }
#ifdef TINTEGRAL_DEBUG
                            c_end = std::clock();
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys() uses CPU time for downward recurrence relation: ",
                                                    1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                        }
                    }
                    else {
                        /* Asymptotic approximation of Boys functions
                           stem:[\frac{\left(\frac{1}{2}\right)^{(n)}\sqrt{\pi}}{2x^{n+\frac{1}{2}}}] */
#ifdef TINTEGRAL_DEBUG
                        c_start = std::clock();
#endif
                        for (int n=0; n<n_upward; ++n) {
                            *(val_iter+1) = (n+0.5)*(*val_iter)*arg_inverse;
                            ++val_iter;
                        }
#ifdef TINTEGRAL_DEBUG
                        c_end = std::clock();
                        Settings::logger->write(tGlueCore::MessageType::Debug,
                                                "get_boys() uses CPU time asymptotic approximation of Boys functions: ",
                                                1000.0*(c_end-c_start)/CLOCKS_PER_SEC, " ms");
#endif
                        /* The simplest situation of underflow in our algorithm
                           is when the maximum order required is greater than
                           the maximum order of upward recurrence relation for
                           argument>708. Notice that though values of Boys
                           functions may still be greater than the smallest
                           normal number 2.2250738585072014e-308 */
#ifdef TINTEGRAL_DEBUG
                        if (n_upward<maxOrder) {
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys() encounters underflow with orders from ",
                                                    n_upward+1, " to ", maxOrder,
                                                    ", and the argument ", args[i],
                                                    " when using asymptotic approximation");
                        }
#endif
                        /* Values of Boys functions are initialized as 0 */
                        //for (int n=n_upward+1; n<=maxOrder; ++n) {
                        //    ++val_iter;
                        //    *val_iter = 0.0;
                        //}
                    }
                }
            }
            val_iter = val_last+1;
            val_last = val_iter+maxOrder;
        }
    }

    void get_boys_errors(
        const int maxOrder,
        const int numArgs,
        const double* args,
        const double* vals,
        double* errors,
        const double* argErrors) noexcept
    {
        auto err_iter = &errors[0];
        auto val_iter = &vals[0];
        for (int i=0; i<numArgs; ++i) {
            /* When stem:[x\le n], the forward error satisfies stem:[
                   2^{-53}\frac{xF_{n+1}(x)}{F_{n}(x)}<2^{-53}x\le2^{-53}\hat{x}(1+\delta_{x})
               ], with stem:[\delta_{x}] the relative error of stem:[x].
               When stem:[x>n], the forward error satisfies stem:[
                   2^{-54}\left[2n+1-\frac{\exp(-x)}{F_{n}(x)}\right]
                   <2^{-53}\left[n+\frac{1}{2}-\frac{\exp(-x)}{2}\right]
                   \le2^{-53}\left[n+\frac{1}{2}-\frac{\exp[-\hat{x}(1+\delta_{x})]}{2}\right]
               ]. */
            auto arg_error = argErrors ? argErrors[i] : 1.1102230246251565e-16;
            auto arg_upper_bound = args[i]*(1.0+arg_error);
            auto arg_forward_error = 1.1102230246251565e-16*arg_upper_bound;
#ifdef TINTEGRAL_DEBUG
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "get_boys_errors() will compute up to order ", maxOrder,
                                    " with the ", i, "-th argument ", args[i]);
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "Beginning address of Boys functions ",
                                    val_iter-&vals[0]);
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "Beginning address of errors ",
                                    err_iter-&errors[0]);
            Settings::logger->write(tGlueCore::MessageType::Debug, "Relative error of the argument ", arg_error);
#endif
            /* stem:[F_{n}(0)=\frac{1}{2n+1}] and whose relative error
               stem:[\le2^{-53}\approx]1.1102230246251565e-16. For
               stem:[\hat{x}=0], the relative error of the argument is either
               0 (stem:[x=0]) or 1 (stem:[x\ne0]). The argument is unknown for
               the latter, so we set the forward error as stem:[2^{-53}(n+0.5)]. */
            if (std::abs(args[i])==0.0) {
                for (int n=0; n<=maxOrder; ++n,++err_iter) {
                    *err_iter = arg_error==0 ? 1.1102230246251565e-16
                        : 1.6653345369377348e-16+1.1102230246251565e-16*n;
                }
                val_iter += maxOrder+1;
            }
            else {
                if (args[i]<=708) {
                    /* stem:[\frac{1}{2}-\frac{\exp[-\hat{x}(1+\delta_{x})]}{2}] */
                    double exp_factor = 0.5-0.5*std::exp(-arg_upper_bound);
                    /* For the zeroth order Boys functions, the forward error is
                       stem:[<2^{-53}\left[\frac{1}{2}-\frac{\exp[-\hat{x}(1+\delta_{x})]}{2}\right]]]. */
                    *err_iter = 1.1102230246251565e-16*exp_factor;
                    double error_upward;
                    double error_power_series;
                    double error_downward;
                    if (args[i]<=14) {
                        if (args[i]<=1) {
                            /* For stem:[0<x<33.979486053733204], the rational
                               approximation is used to compute
                               stem:[F_{0}(x)] and the bound of approximation
                               error due to the use of rational approximation
                               and floating-point operations
                               stem:[\Delta\equiv\Delta_{r}+\Delta_{\text{fl}}]
                               is known. The relative error of the zeroth order
                               Boys functions thus satisfies
                               stem:[\le\frac{\Delta}{\hat{F}_{0}(x)-\Delta}]. */
                            *err_iter += 7.305535400317496e-16/(*val_iter-7.305535400317496e-16);
                            if (args[i]<0.1354269110942593) {
                                /* Upper bounds of relative errors of upward
                                   recurrence relation, power series and downward
                                   recurrence relation are reported in our
                                   reference paper. */
                                error_upward = 0.0;
                                error_power_series = 0.13971760424887747e-14;
                                error_downward = 0.07592778253277038e-14;
                            }
                            else {
                                if (args[i]<0.7165996410102796) {
                                    error_upward = 0.9999999999999992e-14;
                                    error_power_series = 0.19841320687825828e-14;
                                    error_downward = 0.09206325606012207e-14;
                                }
                                else {
                                    error_upward = 0.9999999999999998e-14;
                                    error_power_series = 0.2106447580526418e-14;
                                    error_downward = 0.09727798074349532e-14;
                                }
                            }
                        }
                        else {
                            if (args[i]<7) {
                                *err_iter += 5.009153520722691e-16/(*val_iter-5.009153520722691e-16);
                                error_upward = 1.087551392401333e-14;
                                error_power_series = 0.3963942870385813e-14;
                                error_downward = 0.2539793732158174e-14;
                            }
                            else {
                                *err_iter += 3.848726119600104e-16/(*val_iter-3.848726119600104e-16);
                                error_upward = 1.0343799492311372e-14;
                                error_power_series = 0.5197233048291463e-14;
                                error_downward = 0.390666883152901e-14;
                            }
                        }
                    }
                    else {
                        if (args[i]<33.979486053733204) {
                            if (args[i]<24) {
                                *err_iter += 3.3947109315643046e-16/(*val_iter-3.3947109315643046e-16);
                                error_upward = 1.0150116138015253e-14;
                                error_power_series = 0.6784225564088903e-14;
                                error_downward = 0.5743214701922851e-14;
                            }
                            else {
                                *err_iter += 1.9307987484797792e-16/(*val_iter-1.9307987484797792e-16);
                                error_upward = 0.9997815589616454e-14;
                                error_power_series = 0.8379587807240897e-14;
                                error_downward = 0.7631294586528943e-14;
                            }
                        }
                        else {
                            if (args[i]<611.0987161651447) {
                                if (args[i]<59.704812307223996) {
                                    /* The asymptotic approximation is used to
                                       compute stem:[F_{n}(x)] for
                                       stem:[x\ge33.979486053733204]. The
                                       relative error due to asymptotic
                                       approximation is a monotonically
                                       decreasing function of stem:[x]. So we
                                       take the error of the left endpoint of
                                       each interval. */
                                    *err_iter += 4.999999999999999e-16;
                                    error_upward = 1.0120029193594564e-14;
                                    error_power_series = 1.4052515403211452e-14;
                                    error_downward = 1.4432342724865255e-14;
                                }
                                else {
                                    *err_iter += 3.3306690738839883e-16;
                                    auto sub_interval = int((args[i]-59.704812307223996)*0.16666666666666666);
                                    error_upward = (2.265425586221652e-19*sub_interval+9.432692874873409e-16)
                                                 * sub_interval+1.3282674447405356e-14;
                                    error_power_series = (-7.567631644242922e-19*sub_interval+7.673853805441193e-16)
                                                       * sub_interval+1.3557751561032423e-14;
                                    error_downward = (-2.019830311227827e-19*sub_interval+9.803051756939245e-16)
                                                   * sub_interval+1.2681895803363152e-14;
                                }
                            }
                            else {
                                *err_iter += 3.3306690738754696e-16;
                                auto sub_interval = int((args[i]-611.0987161651447)*0.5);
                                error_upward = 1.002531391240669e-13;
                                error_power_series = (-3.2585763359453267e-20*sub_interval+7.025078137444195e-16)
                                                   * sub_interval+7.78642218112495e-14;
                                error_downward = (1.571314345480464e-20*sub_interval+0.11098125395965322e-14)
                                               * sub_interval+1.0124082280300994e-13;
                            }
                        }
                    }
#ifdef TINTEGRAL_DEBUG
                    Settings::logger->write(
                        tGlueCore::MessageType::Debug,
                        "get_boys_errors() gets the upper bound of relative error of the zeroth order of Boys function: ",
                        *err_iter,
                        " for the argument ", args[i]
                    );
                    Settings::logger->write(tGlueCore::MessageType::Debug,
                                            "get_boys_errors() gets the upper bounds of relative errors: ",
                                            error_upward, " (upward recurrence relation), ",
                                            error_power_series, " (power series), ",
                                            error_downward, " (downward recurrence relation) for the argument ",
                                            args[i]);
#endif
                    /* Get the maximum order of the upward recurrence relation */
                    auto n_upmax = boys_max_upward(args[i]);
                    int n_upward = maxOrder<=n_upmax ? maxOrder : n_upmax;
                    /* No underflow for stem:[0<x\le708] and stem:[1\le n\le n^{\text{up}}_{\max}]. */
                    for (int n=1; n<=n_upward; ++n) {
                        ++err_iter;
                        *err_iter = args[i]>n ? error_upward+1.1102230246251565e-16*(n+exp_factor)
                                  : error_upward+arg_forward_error;
                    }
                    val_iter += n_upward;
                    for (int n=n_upward+1; n<maxOrder; ++n) {
                        ++err_iter;
                        ++val_iter;
                        if (std::isnormal(*val_iter)) {
                            *err_iter = args[i]>n ? error_downward+1.1102230246251565e-16*(n+exp_factor)
                                      : error_downward+arg_forward_error;
                        }
                        else {
#ifdef TINTEGRAL_DEBUG
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys_errors() encounters subnormal with the order ", n,
                                                    " and argument ", args[i],
                                                    " for the downward recurrence relation");
#endif
                            *err_iter = args[i]>n ? 1.0+1.1102230246251565e-16*(n+exp_factor)
                                      : 1.0+arg_forward_error;
                            for (int n_ufl=n+1; n_ufl<maxOrder; ++n_ufl) {
                                ++err_iter;
                                *err_iter = args[i]>n_ufl ? 1.0+1.1102230246251565e-16*(n_ufl+exp_factor)
                                          : 1.0+arg_forward_error;
                            }
                            val_iter += maxOrder-n-1;
                            break;
                        }
                    }
                    if (n_upward<maxOrder) {
                        ++err_iter;
                        ++val_iter;
                        if (std::isnormal(*val_iter)) {
                            *err_iter = args[i]>maxOrder
                                      ? error_power_series+1.1102230246251565e-16*(maxOrder+exp_factor)
                                      : error_power_series+arg_forward_error;
                        }
                        else {
#ifdef TINTEGRAL_DEBUG
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys_errors() encounters subnormal with the order ", maxOrder,
                                                    " and argument ", args[i],
                                                    " for the power series");
#endif
                            *err_iter = args[i]>maxOrder ? 1.0+1.1102230246251565e-16*(maxOrder+exp_factor)
                                      : 1.0+arg_forward_error;
                        }
                    }
                }
                else {
                    /* The asymptotic approximation is used to compute
                       stem:[F_{n}(x)] for stem:[x>708] and stem:[1\le n\le450].
                       There is no underflow for stem:[F_{0}(x)] with
                       stem:[0<x\le]1.7976931348623157e+308. Moreover the
                       relative error due to asymptotic approximation becomes
                       tiny (<7.00824186836953e-310) for stem:[F_{0}(x)] with
                       stem:[x>708]. So we only need to consider the relative
                       error due to floating-point operations
                       stem:[\frac{1}{2}\sqrt{\frac{\pi}{x}}], which is
                       stem:[\le3*2^{-53}]. The forward error is
                       stem:[2^{-54}\left[1-\frac{\exp(-x)}{F_{0}(x)}\right]\approx2^{-54}]. */
                    *err_iter = 3.885780586188048e-16;
                    int n_upward = maxOrder<=450 ? maxOrder : 450;
                    for (int n=1; n<=n_upward; ++n) {
                        ++err_iter;
                        ++val_iter;
                        if (std::isnormal(*val_iter)) {
                            /* When computed Boys function is a normal number,
                               the relative error is less than the sum of the
                               upper bound of floating-point error and the
                               forward error, which satisfies stem:[
                                   \le2^{-53}\left[n+\frac{1}{2}-\frac{\exp[-\hat{x}(1+\delta_{x})]}{2}\right]
                                   \lesssim2^{-53}\left(n+\frac{1}{2}right)
                               ] by noticing stem:[x>708]. */
                            /*        = 1.0025313912365164e-13+1.1102230246251565e-16*(n+0.5) */
                            *err_iter = 1.0030865027488289e-13+1.1102230246251565e-16*n;
                        }
                        else {
#ifdef TINTEGRAL_DEBUG
                            Settings::logger->write(tGlueCore::MessageType::Debug,
                                                    "get_boys_errors() encounters subnormal with the order ", n,
                                                    " and argument ", args[i],
                                                    " for the asymptotic approximation");
#endif
                            /*        = 1.0+1.1102230246251565e-16*(n+0.5) */
                            *err_iter = 1.0+1.1102230246251565e-16*n;
                            n_upward = n;
                            break;
                        }
                    }
                    /* Underflow due to our algorithm or values of Boys
                       functions less than 2.2250738585072014e-308 */
                    for (int n=n_upward+1; n<=maxOrder; ++n) {
                        ++err_iter;
                        *err_iter = args[i]>n ? 1.0+1.1102230246251565e-16*n : 1.0+arg_forward_error;
                    }
                    if (n_upward<maxOrder) val_iter += maxOrder-n_upward;
                }
                ++err_iter;
                ++val_iter;
            }
        }
    }
}
