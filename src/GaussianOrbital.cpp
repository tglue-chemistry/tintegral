/* tintegral: not only an integral computation library
   Copyright 2018 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of the London orbital and its properties.

   2018-07-27, Bin Gao:
   * first version
*/

#include "GaussianOrbital.hpp"

namespace tIntegral
{
    CartesianGTOIntegrator::CartesianGTOIntegrator(const tContrGTO& cartGTO) noexcept
    {
    }

    nonstd::expected<RecurBuffer,std::string>
    CartesianGTOIntegrator::bottom_up() noexcept
    {
        /* Recovers Cartesian GTOs */
        /* Makes contraction */
    }

    nonstd::expected<RecurBuffer,std::string>
    SphericalGTOIntegrator::bottom_up() noexcept
    {
        /* Recovers Hermite GTOs */
        /* Makes contractions */
        /* Transforms spherical GTOs */
    }
}
