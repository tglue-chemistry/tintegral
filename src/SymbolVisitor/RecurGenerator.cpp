/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the recurrence relation generator.

   2019-05-29, Bin Gao:
   * first version
*/

#include "tGlueCore/Convert.hpp"

#include "tIntegral/SymbolVisitor/RecurGenerator.hpp"

namespace tIntegral
{
    std::vector<unsigned int>
    RecurGenerator::build(const std::string& nameFunction,
                          const RecurDirection recurDirection,
                          const std::shared_ptr<RecurSymbol> RHS,
                          const std::vector<RecurDirection> rhsDirections,
                          const std::vector<std::pair<std::shared_ptr<RecurIndex>,std::array<bool,3>>> zeroOrders,
                          const bool vectorWise) noexcept
    {
        m_rhs_string.clear();
        if (recurDirection!=RecurDirection::X &&
            recurDirection!=RecurDirection::Y &&
            recurDirection!=RecurDirection::Z) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurGenerator::build() is given an invalid recurrence relation direction ",
                                    stringify_direction(recurDirection));
            return std::vector<unsigned int>();
        }
        else {
            m_recur_direction = recurDirection;
            m_rhs_directions = rhsDirections;
            m_which_rhs = 0;
            m_zero_orders = zeroOrders;
            m_vector_wise = vectorWise;
            m_rhs_contribut = true;
            m_rhs_positions.clear();
#if defined(TINTEGRAL_DEBUG)
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "RecurGenerator::build() work on the direction ",
                                    stringify_direction(recurDirection),
                                    " and will process ",
                                    RHS->to_string());
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "RecurGenerator::build() is given directions of RHS ",
                                    stringify_direction(m_rhs_directions));
#endif
            /* Process different RHS symbols */
            if (RHS->accept(this)) {
                if (m_rhs_positions.empty()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "RecurGenerator::build() got no contributing RHS term for the recurrence relation direction ",
                        stringify_direction(m_recur_direction)
                    );
                    return m_rhs_positions;
                }
                /* Write code to perform the recurrence relation */
                m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                std::vector<std::string> param_debug;
                param_debug.push_back(m_translator->string_literal(nameFunction+" works component LHS["));
                param_debug.push_back(
                    m_translator->arithmetic_operation(
                        m_translator->arithmetic_operation(
                            m_translator->lhs_node_value(),
                            RecurArithmetic::Subtraction,
                            m_translator->call_function(
                                m_translator->recur_buffer_object(),
                                std::string("get_element"),
                                std::vector<std::string>(),
                                RecurOperandType::PointerOperand
                            )
                        ),
                        RecurArithmetic::Subtraction,
                        m_translator->call_function(
                            m_translator->lhs_node_object(),
                            std::string("get_offset"),
                            std::vector<std::string>(),
                            RecurOperandType::PointerOperand
                        )
                    )
                );
                param_debug.push_back(m_translator->string_literal("], gets components of RHS nodes "));
                for (auto const& each_rhs: m_rhs_positions) {
                    param_debug.push_back(m_translator->string_literal(", RHS"+std::to_string(each_rhs)+"["));
                    param_debug.push_back(
                        m_translator->arithmetic_operation(
                            m_translator->arithmetic_operation(
                                m_translator->rhs_node_value(each_rhs, m_recur_direction),
                                RecurArithmetic::Subtraction,
                                m_translator->call_function(
                                    m_translator->recur_buffer_object(),
                                    std::string("get_element"),
                                    std::vector<std::string>(),
                                    RecurOperandType::PointerOperand
                                )
                            ),
                            RecurArithmetic::Subtraction,
                            m_translator->call_function(
                                m_translator->rhs_nodes_at(each_rhs),
                                std::string("get_offset"),
                                std::vector<std::string>(),
                                RecurOperandType::PointerOperand
                            )
                        )
                    );
                    param_debug.push_back(m_translator->string_literal(std::string("]")));
                }
                m_translator->handle_debug(param_debug);
                m_translator->endif_directive();
                if (m_vector_wise) {
                    m_translator->assignment_statement(m_translator->lhs_node_value(false, false, true),
                                                       RecurAssignment::BasicAssignment,
                                                       m_rhs_string);
                }
                else {
                    m_translator->begin_for_loop(m_translator->type_integer(true),
                                                 std::vector<std::pair<std::string,std::string>>({
                                                     std::make_pair(m_iter_vector, std::string("0"))
                                                 }),
                                                 m_translator->comparison_operation(
                                                     m_iter_vector,
                                                     RecurComparison::LessThan,
                                                     m_translator->call_function(
                                                         m_translator->recur_buffer_object(),
                                                         std::string("get_size_element"),
                                                         std::vector<std::string>(),
                                                         RecurOperandType::PointerOperand
                                                     )
                                                 ),
                                                 std::vector<std::string>({
                                                     m_translator->inc_dec_operation(RecurIncDec::Increment,
                                                                                     m_iter_vector)
                                                 }));
                    m_translator->assignment_statement(
                        m_translator->subscript_operation(
                            m_translator->parenthesize_expression(m_translator->lhs_node_value(false, false, true)),
                            m_iter_vector
                        ),
                        RecurAssignment::BasicAssignment,
                        m_rhs_string
                    );
                    m_translator->end_for_loop();
                }
                return m_rhs_positions;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurGenerator::build() called with a recurrence relation direction ",
                                        stringify_direction(m_recur_direction));
                return std::vector<unsigned int>();
            }
        }
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurNumber> number) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a number");
#endif
        if (m_rhs_contribut) m_rhs_string += number->get_name();
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a scalar variable");
#endif
        if (m_rhs_contribut) m_rhs_string += variable->get_name();
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a Cartesian variable");
#endif
        if (m_rhs_contribut) {
            m_rhs_string += m_translator->cartesian_component(variable->get_name(),
                                                              variable->get_direction(),
                                                              m_recur_direction);
        }
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a vector of scalar variables");
#endif
        if (m_rhs_contribut) {
            m_rhs_string += m_vector_wise
                          ? variable->get_name()
                          : m_translator->subscript_operation(variable->get_name(), m_iter_vector);
        }
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a vector of Cartesian variables");
#endif
        if (m_rhs_contribut) {
            /* We expect index of the Cartesian components runs slowly in memory */
            m_rhs_string += m_vector_wise
                          ? m_translator->cartesian_component(variable->get_name(),
                                                              variable->get_direction(),
                                                              m_recur_direction)
                          : m_translator->subscript_operation(
                                m_translator->cartesian_component(variable->get_name(),
                                                                  variable->get_direction(),
                                                                  m_recur_direction),
                                m_iter_vector
                            );
        }
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process an order");
#endif
        auto invalid_index = true;
        bool zero_order;
        auto order_index = order->get_indices()[0];
        auto order_direction = transform_direction(m_recur_direction, order_index->get_direction());
        for (auto const& each_index: m_zero_orders) {
            if (order_index->equal_to(each_index.first)) {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(
                    tGlueCore::MessageType::Debug,
                    "RecurGenerator::dispatch() processes the order of index ",
                    order_index->to_string(),
                    " along direction ",
                    stringify_direction(order_direction),
                    "[",
                    each_index.second[tGlueCore::to_integral(order_direction)],
                    "]");
#endif
                zero_order = each_index.second[tGlueCore::to_integral(order_direction)];
                invalid_index = false;
                break;
            }
        }
        if (invalid_index) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurGenerator::dispatch() got order of an invalid index ",
                                    order->to_string());
            return false;
        }
        else {
            if (m_rhs_contribut) {
                if (zero_order) {
                    m_rhs_contribut = false;
                }
                else {
                    m_rhs_string += m_translator->call_function(
                        m_translator->type_floating_point(),
                        std::vector<std::string>({
                            m_translator->index_order(order_index->get_name(), order_direction)
                        })
                    );
                }
            }
            return true;
        }
    }

    //FIXME: term is not used in the function, maybe can be changed
    bool RecurGenerator::dispatch(std::shared_ptr<RecurTerm> term) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process ",
                                m_which_rhs,
                                "-th RHS term");
#endif
        if (m_rhs_contribut) {
            /* Check the iterator of contributing directions of RHS terms */
            if (m_which_rhs==m_rhs_directions.size()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurGenerator::dispatch() is given number of RHS directions ",
                                        m_rhs_directions.size(),
                                        ", but the current RHS term is ",
                                        m_which_rhs);
                return false;
            }
            /* Reset the indicator of contributed RHS term */
#if defined(TINTEGRAL_DEBUG)
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "RecurGenerator::dispatch() works along the direction ",
                                    stringify_direction(m_recur_direction),
                                    ", and allowed direction(s) of the RHS term is ",
                                    stringify_direction(m_rhs_directions[m_which_rhs]));
#endif
            if (is_direction_included(m_recur_direction, m_rhs_directions[m_which_rhs])) {
                m_rhs_string += m_vector_wise
                              ? m_translator->parenthesize_expression(
                                    m_translator->rhs_node_value(m_which_rhs, m_recur_direction, false, false, true)
                                )
                              : m_translator->subscript_operation(
                                    m_translator->parenthesize_expression(
                                        m_translator->rhs_node_value(
                                            m_which_rhs, m_recur_direction, false, false, true
                                        )
                                    ),
                                    m_iter_vector
                                );
                m_rhs_positions.push_back(m_which_rhs);
            }
            else {
                m_rhs_contribut = false;
            }
        }
        ++m_which_rhs;
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurAddition> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process an addition");
#endif
        if (dispatch_arithmetic_operation(operation, RecurArithmetic::Addition)) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurGenerator::dispatch() called for an addition ",
                                    operation->to_string());
            return false;
        }
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a subtraction");
#endif
        if (operation->get_lhs()) {
            if (!dispatch_arithmetic_operation(operation, RecurArithmetic::Subtraction)) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurGenerator::dispatch() called for a subtraction ",
                                        operation->to_string());
                return false;
            }
        }
        /* Empty minuend, which means a negative symbol */
        else {
            if (m_rhs_contribut) {
                std::string saved_rhs_string;
                saved_rhs_string.swap(m_rhs_string);
                /* Get the subtrahend */
                if (operation->get_rhs()->accept(this)) {
                    if (m_rhs_contribut) {
                        std::string str_subtrahend;
                        str_subtrahend.swap(m_rhs_string);
                        m_rhs_string = saved_rhs_string
                                     + m_translator->parenthesize_expression(
                                           m_translator->arithmetic_operation(
                                               RecurArithmetic::Negation,
                                               str_subtrahend
                                           )
                                       );
                    }
                    else {
                        m_rhs_string.swap(saved_rhs_string);
                    }
                }
                else {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "RecurGenerator::dispatch() called for a negative symbol ",
                                            operation->to_string());
                    return false;
                }
            }
            else {
                if (!operation->get_rhs()->accept(this)) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "RecurGenerator::dispatch() called for a negative symbol ",
                                            operation->to_string());
                    return false;
                }
            }
        }
        return true;
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a multiplication");
#endif
        if (dispatch_arithmetic_operation(operation, RecurArithmetic::Multiplication)) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurGenerator::dispatch() called for a multiplication ",
                                    operation->to_string());
            return false;
        }
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurDivision> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a division");
#endif
        if (dispatch_arithmetic_operation(operation, RecurArithmetic::Division)) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurGenerator::dispatch() called for a division ",
                                    operation->to_string());
            return false;
        }
    }

    bool RecurGenerator::dispatch(std::shared_ptr<RecurParentheses> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurGenerator::dispatch() will process a parenthesized expression");
#endif
        if (m_rhs_contribut) {
            std::string saved_rhs_string;
            saved_rhs_string.swap(m_rhs_string);
            if (operation->get_expression()->accept(this)) {
                if (m_rhs_contribut) {
                    std::string str_expression;
                    str_expression.swap(m_rhs_string);
                    m_rhs_string = saved_rhs_string
                                 + m_translator->parenthesize_expression(str_expression);
                }
                else {
                    m_rhs_string.swap(saved_rhs_string);
                }
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurGenerator::dispatch() called for a parenthesized expression ",
                                        operation->to_string());
                return false;
            }
        }
        else {
            if (!operation->get_expression()->accept(this)) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurGenerator::dispatch() called for a parenthesized expression ",
                                        operation->to_string());
                return false;
            }
        }
        return true;
    }
}
