/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the variable processor in recurrence relations.

   2020-04-02, Bin Gao:
   * first version
*/

#include "tIntegral/SymbolVisitor/RecurVarProcessor.hpp"

namespace tIntegral
{
    bool RecurVarProcessor::build(const std::vector<std::shared_ptr<RecurIntegrand>> integrand,
                                  const std::set<std::shared_ptr<RecurSymbol>>& variables) noexcept
    {
        m_integrand = integrand;
        m_all_variables.clear();
        m_cyclic_variables.clear();
        for (auto each_var=variables.cbegin(); each_var!=variables.cend(); ++each_var) {
            /* Find all variables including their dependency */
            if (!find_all_variables(*each_var)) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurVarProcessor::build() failed to process ",
                                        std::distance(variables.cbegin(), each_var),
                                        "-th variable of ",
                                        variables.size(),
                                        " variables");
                return false;
            }
        }
        /* Generate codes to declare and assign given variables */
        m_declared_variables.clear();
        for (auto each_var=m_all_variables.cbegin(); each_var!=m_all_variables.cend(); ++each_var) {
            /* Only given variables are to be declared and assigned */
            auto iter_var = variables.find(*each_var);
            if (iter_var!=variables.end()) {
                m_rhs_begun = false;
                if (!(*each_var)->accept(this)) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "RecurVarProcessor::build() failed to declare and assign ",
                                            std::distance(variables.begin(), iter_var),
                                            "-th variable or ",
                                            std::distance(m_all_variables.cbegin(), each_var),
                                            "-th variable ",
                                            (*each_var)->to_string(),
                                            " of ",
                                            m_all_variables.size(),
                                            " sorted variables and dependency");
                    return false;
                }
                m_declared_variables.insert(*each_var);
            }
        }
        return true;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurVarProcessor::dispatch() will process a scalar variable");
#endif
        if (!dispatch_variable(variable)) return false;
        if (!m_rhs_begun) {
            m_translator->write_declaration_assignment(
                m_translator->type_floating_point(),
                variable->get_name(),
                RecurAssignment::BasicAssignment,
                m_translator->call_function(m_integrand_names.back(),
                                            variable->get_integrand_method(),
                                            m_method_parameters.back(),
                                            RecurOperandType::PointerOperand)
            );
        }
        return true;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurVarProcessor::dispatch() will process a Cartesian variable");
#endif
        if (!dispatch_variable(variable)) return false;
        if (!m_rhs_begun) {
            m_translator->write_declaration_assignment(
                m_translator->type_array(m_translator->type_floating_point(), 3),
                variable->get_name(),
                RecurAssignment::BasicAssignment,
                m_translator->call_function(m_integrand_names.back(),
                                            variable->get_integrand_method(),
                                            m_method_parameters.back(),
                                            RecurOperandType::PointerOperand)
            );
        }
        return true;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurVarProcessor::dispatch() will process a vector of scalar variables");
#endif
        if (!dispatch_variable(variable)) return false;
        if (!m_rhs_begun) {
            m_translator->write_declaration_assignment(
                m_translator->type_valarray(m_translator->type_floating_point()),
                variable->get_name(),
                RecurAssignment::BasicAssignment,
                m_translator->call_function(m_integrand_names.back(),
                                            variable->get_integrand_method(),
                                            m_method_parameters.back(),
                                            RecurOperandType::PointerOperand)
            );
        }
        return true;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurVarProcessor::dispatch() will process a vector of Cartesian variables");
#endif
        if (!dispatch_variable(variable)) return false;
        if (!m_rhs_begun) {
            m_translator->write_declaration_assignment(
                m_translator->type_array(m_translator->type_valarray(m_translator->type_floating_point()), 3),
                variable->get_name(),
                RecurAssignment::BasicAssignment,
                m_translator->call_function(m_integrand_names.back(),
                                            variable->get_integrand_method(),
                                            m_method_parameters.back(),
                                            RecurOperandType::PointerOperand)
            );
        }
        return true;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encounters an order that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurTerm> term) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encounters a RHS term that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurAddition> operation) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encouters an addition that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encouters a subtraction that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encouters a multiplication that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurDivision> operation) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encouters a division that is invalid");
        return false;
    }

    bool RecurVarProcessor::dispatch(std::shared_ptr<RecurParentheses> operation) noexcept
    {
        Settings::logger->write(tGlueCore::MessageType::Error,
                                "RecurVarProcessor::dispatch() encouters a parenthesized expression that is invalid");
        return false;
    }
}
