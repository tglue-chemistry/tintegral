/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMWrite().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief writes the conext of Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[FILE]{in} fp_cartmm file pointer
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMWrite(const OneProp one_prop, FILE *fp_cartmm)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    GErrorCode ierr;  /* error information */
    /* checks the file pointer */
    if (fp_cartmm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL file pointer");
    }
    /* gets the context of the Cartesian multipole moments */
    cart_mm = (CartMM *)(one_prop->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    /* writes the context of the Cartesian multipole moments */
    fprintf(fp_cartmm, "\n");
    fprintf(fp_cartmm,
            "CartMMWrite>> order of magnetic derivatives %"GINT_FMT"\n",
            cart_mm->order_mag);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of partial magnetic derivatives on the bra center %"GINT_FMT"\n",
            cart_mm->order_mag_bra);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of partial magnetic derivatives on the ket center %"GINT_FMT"\n",
            cart_mm->order_mag_ket);
    if (cart_mm->mag_LPF!=NULL) {
        ierr = MagLPFWrite(cart_mm->mag_LPF, fp_cartmm);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling MagLPFWrite");
    }
    fprintf(fp_cartmm,
            "CartMMWrite>> order of total rotational angular momentum derivatives %"GINT_FMT"\n",
            cart_mm->order_rot);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of partial TRAM derivatives on the bra center %"GINT_FMT"\n",
            cart_mm->order_rot_bra);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of partial TRAM derivatives on the ket center %"GINT_FMT"\n",
            cart_mm->order_rot_ket);
    if (cart_mm->rot_LPF!=NULL) {
        ierr = RotLPFWrite(cart_mm->rot_LPF, fp_cartmm);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling RotLPFWrite");
    }
    fprintf(fp_cartmm,
            "CartMMWrite>> type of relativistic corrections %d\n",
            cart_mm->RC_type);
    fprintf(fp_cartmm,
            "CartMMWrite>> type of the differentiated charge distributions %d\n",
            cart_mm->DCD_type);
    fprintf(fp_cartmm, "CartMMWrite>> scale constant %f\n", cart_mm->scal_const);
    fprintf(fp_cartmm,
            "CartMMWrite>> index of the dipole origin %"GINT_FMT"\n",
            cart_mm->idx_diporg);
    fprintf(fp_cartmm,
            "CartMMWrite>> coordinates of the dipole origin (%e,%e,%e)\n",
            cart_mm->coord_diporg[0],
            cart_mm->coord_diporg[1],
            cart_mm->coord_diporg[2]);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of geometric derivatives on the dipole origin %"GINT_FMT"\n",
            cart_mm->order_geo_diporg);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of Cartesian multipole moments %"GINT_FMT"\n",
            cart_mm->order_cart_mm);
    fprintf(fp_cartmm,
            "CartMMWrite>> order of electronic derivatives %"GINT_FMT"\n",
            cart_mm->order_elec_deriv);
    return GSUCCESS;
}
