/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMDuplicate().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief duplicates the conext of Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[OneProp:struct]{inout} new_prop the duplicated one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMDuplicate(const OneProp one_prop, OneProp *new_prop)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    CartMM *new_mm;   /* context of duplicated Cartesian multipole moments */
    /* gets the context of the Cartesian multipole moments */
    cart_mm = (CartMM *)(one_prop->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    /* allocates memory for the duplicated Cartesian multipole moments */
    new_mm = (CartMM *)malloc(sizeof(CartMM));
    if (new_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_mm");
    }
    /* duplicates the context of the Cartesian multipole moments */
    new_mm->order_mag = cart_mm->order_mag;
    new_mm->order_mag_bra = cart_mm->order_mag_bra;
    new_mm->order_mag_ket = cart_mm->order_mag_ket;
    if (cart_mm->mag_LPF!=NULL) {
        new_mm->mag_LPF = (MagLPF *)malloc(sizeof(MagLPF));
        if (new_mm->mag_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_mm->mag_LPF");
        }
        MagLPFDuplicate(cart_mm->mag_LPF, new_mm->mag_LPF);
    }
    else {
        new_mm->mag_LPF = NULL;
    }
    new_mm->order_rot = cart_mm->order_rot;
    new_mm->order_rot_bra = cart_mm->order_rot_bra;
    new_mm->order_rot_ket = cart_mm->order_rot_ket;
    if (cart_mm->rot_LPF!=NULL) {
        new_mm->rot_LPF = (RotLPF *)malloc(sizeof(RotLPF));
        if (new_mm->rot_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_mm->rot_LPF");
        }
        RotLPFDuplicate(cart_mm->rot_LPF, new_mm->rot_LPF);
    }
    else {
        new_mm->rot_LPF = NULL;
    }
    new_mm->RC_type = cart_mm->RC_type;
    new_mm->DCD_type = cart_mm->DCD_type;
    new_mm->scal_const = cart_mm->scal_const;
    new_mm->idx_diporg = cart_mm->idx_diporg;
    new_mm->coord_diporg[0] = cart_mm->coord_diporg[0];
    new_mm->coord_diporg[1] = cart_mm->coord_diporg[1];
    new_mm->coord_diporg[2] = cart_mm->coord_diporg[2];
    new_mm->order_geo_diporg = cart_mm->order_geo_diporg;
    new_mm->order_cart_mm = cart_mm->order_cart_mm;
    new_mm->order_elec_deriv = cart_mm->order_elec_deriv;
    /* sets the implementation-specific data of the duplicated one-electron property */
    if ((*new_prop)->data!=NULL) {
        GErrorExit(FILE_AND_LINE, "previous implementation-specific data not destroyed");
    }
    (*new_prop)->data = (GVoid *)new_mm;
    return GSUCCESS;
}
