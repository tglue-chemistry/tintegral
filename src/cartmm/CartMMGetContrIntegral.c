/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMGetContrIntegral().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"
#include "triangle/gen1int_triangle.h"
#include "triangle/gen1int_triangle_gto.h"
#include "triangle/gen1int_triangle_elec_deriv.h"
#include "triangle/gen1int_triangle_cartmm.h"
#include "triangle/gen1int_triangle_mag.h"

/*% \brief calculates the integral of Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[GeoDeriv:struct]{in} geo_deriv geometric derivatives
    \param[GeoDeriv:struct]{in} geo_deriv_bra geometric derivatives on the bra center
    \param[GeoDeriv:struct]{in} geo_deriv_ket geometric derivatives on the ket center
    \param[GInt:int]{in} idx_bra index of the bra center
    \param[GReal:real]{in} coord_bra coordinates of the bra center
    \param[ContrGTO:struct]{in} contr_GTO_bra contracted GTOs on the bra center
    \param[GInt:int]{in} idx_ket index of the ket center
    \param[GReal:real]{in} coord_ket coordinates of the ket center
    \param[GBool:enum]{in} add_values indicates if adding values to the existing entries
    \param[ContrGTO:struct]{in} contr_GTO_ket contracted GTOs on the ket center
    \param[GReal:real]{out} val_int values of the integral of the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMGetContrIntegral(const OneProp one_prop,
                                  const GeoDeriv *geo_deriv,
                                  const GeoDeriv *geo_deriv_bra,
                                  const GeoDeriv *geo_deriv_ket,
                                  const GInt idx_bra,
                                  const GReal coord_bra[3],
                                  const ContrGTO *contr_GTO_bra,
                                  const GInt idx_ket,
                                  const GReal coord_ket[3],
                                  const ContrGTO *contr_GTO_ket,
                                  const GBool add_values,
                                  GReal *val_int)
{
    GBool zero_value;              /* indicates if the value is zero */
    GInt num_geo_deriv;            /* number of geometric derivatives */
    GInt num_geo_cent_bra;         /* number of differentiated centers (bra) */
    GInt num_geo_cent_ket;         /* number of differentiated centers (ket) */
    GInt idx_geo_cent[1];          /* indices of differentiated centers */
    GInt num_geo_deriv_bra;        /* number of partial geometric derivatives on the bra center */
    GInt num_geo_deriv_ket;        /* number of partial geometric derivatives on the ket center */
    GInt size_one_prop;            /* size of the one-electron property */
    GInt num_AO_bra;               /* number of AOs on the bra center */
    GInt num_AO_ket;               /* number of AOs on the ket center */
    GInt size_integrand;           /* size of the integrand (basis sets and operators) */
    GInt size_val;                 /* size of the values of the integral of the one-electron property */
    CartMM *cart_mm;               /* context of Cartesian multipole moments */
    GInt real_order_cart_mm;       /* order of Cartesian multipole moments after considering its geometric derivatives */
    GInt num_cent_integrand;       /* number of centers of the integrand */
    GInt idx_integrand[3];         /* indices of the centers of the integrand */
    GInt order_geo_integrand[3];   /* orders of geometric derivatives on the centers of the integrand */
    PartGeo part_geo;              /* context of partial geometric derivatives on the integrand */
    GULong size_part_geo;          /* size of the partial geometric derivatives on the integrand */
    GULong mult_coef;              /* multinomial coefficient of the partial geometric derivatives */
    TriangleRecur *recur_cart_mm;  /* context of the triangle based recurrence relations */
    GInt num_indices;              /* number of indices involved in recurrence relations */
    GInt idx_GTO_bra;             /* index of the CGTOs/HGTOs/SGTOs on the bra center */
    GInt idx_GTO_ket;             /* index of the CGTOs/HGTOs/SGTOs on the ket center */
    GInt idx_elec_deriv;           /* index of the electronic derivatives */
    GInt idx_cart_mm;              /* index of the Cartesian multipole moments */
    GInt idx_geo_diporg;           /* index of geometric derivatives on the dipole origin */
    GInt idx_geo_bra;              /* index of geometric derivatives on the bra center */
    GInt idx_geo_ket;              /* index of geometric derivatives on the ket center */
    GInt idx_orders[7];            /* orders of indices involved in recurrence relations */
    /* indices in the recurrence relations:
       - Cartesian multipole moments -> geometric derivatives on the dipole origin
       - HGTO (bra) -> CGTO (bra) + geometric derivatives (bra)
       - HGTO (ket) -> CGTO (ket) + geometric derivatives (ket)
       - HGTO (ket) -> electronic derivatives
       - HGTO (bra) + HGTO (ket) -> Cartesian multipole moments
       - HGTO (bra) -> HGTO (ket) */
    GInt idx_cart_deriv[CARTMM_NUM_IDX_CART_DERIV];
    GInt RHS_cart_deriv[CARTMM_NUM_IDX_CART_DERIV];
    GInt idx_HGTO_to_CGTO_bra[NUM_IDX_HGTO_TO_CGTO];
    GInt idx_HGTO_to_CGTO_ket[NUM_IDX_HGTO_TO_CGTO];
    GInt idx_ket_to_elec_deriv[NUM_IDX_KET_TO_ELEC_DERIV];
    GInt RHS_ket_to_elec_deriv[NUM_IDX_KET_TO_ELEC_DERIV];
    GInt idx_HGTO_to_cart_mm[CARTMM_NUM_IDX_CART_MM];
    GInt idx_HGTO_bra_to_ket[CARTMM_NUM_IDX_HGTO_KET];
    /* indices and orders for the recurrence relations of spherical GTOs */
    GInt num_idx_SGTO;
    GInt idx_SGTO[7];
    GInt RHS_SGTO[7];
    GInt ival;                     /* incremental recoder over values */
    GULong igeo;                   /* incremental recorder over partial geometric derivatives */
    GErrorCode ierr;               /* error information */
    /* first assumes the value is not zero */
    zero_value = GFALSE;
    /* computes the number of geometric derivatives */
    if (geo_deriv==NULL) {
        num_geo_deriv = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv, &num_geo_deriv);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv");
    }
    /* computes the number of partial geometric derivatives on the bra center */
    if (geo_deriv_bra==NULL) {
        num_geo_deriv_bra = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_bra, &num_geo_deriv_bra);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (bra)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_bra, &num_geo_cent_bra);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (bra)");
        switch (num_geo_cent_bra) {
        /* no geometric derivatives */
        case 0:
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetIdxCent(geo_deriv_bra,
                                            num_geo_cent_bra,
                                            idx_geo_cent);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetIdxCent (bra)");
            if (idx_geo_cent[0]!=idx_bra) {
                zero_value = GTRUE;
            }
            break;
        /* more than one differentiated center */
        default:
            zero_value = GTRUE;
        }
    }
    /* computes the number of partial geometric derivatives on the ket center */
    if (geo_deriv_ket==NULL) {
        num_geo_deriv_ket = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_ket, &num_geo_deriv_ket);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (ket)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_ket, &num_geo_cent_ket);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (ket)");
        switch (num_geo_cent_ket) {
        /* no geometric derivatives */
        case 0:
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetIdxCent(geo_deriv_ket,
                                            num_geo_cent_ket,
                                            idx_geo_cent);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetIdxCent (ket)");
            if (idx_geo_cent[0]!=idx_ket) {
                zero_value = GTRUE;
            }
            break;
        /* more than one differentiated center */
        default:
            zero_value = GTRUE;
        }
    }
    /* gets the size of the one-electron property, this function will
       also check the validity of the linked list */
    ierr = OnePropGetSize(one_prop, &size_one_prop);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropGetSize");
    /* gets the sizes of contracted GTOs */
    ierr = ContrGTOGetNumAO(contr_GTO_bra, &num_AO_bra);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling ContrGTOGetNumAO (bra)");
    ierr = ContrGTOGetNumAO(contr_GTO_ket, &num_AO_ket);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling ContrGTOGetNumAO (ket)");
    /* computes the size of the integrand and values */
    size_integrand = size_one_prop*num_AO_bra*num_AO_ket;
    size_val = num_geo_deriv*num_geo_deriv_bra*num_geo_deriv_ket*size_integrand;
    /* initializes the values */
    if (add_values==GFALSE) {
        for (ival=0; ival<size_val; ival++) val_int[ival] = 0;
    }
    if (zero_value==GFALSE) {
        /* gets the context of the Cartesian multipole moments */
        cart_mm = (CartMM *)(one_prop->data);
        if (cart_mm==NULL) {
            GErrorExit(FILE_AND_LINE, "NULL cart_mm");
        }
        /* gets the order of Cartesian multipole moments after considering its geometric derivatives */
        real_order_cart_mm = cart_mm->order_cart_mm-cart_mm->order_geo_diporg;
        /* higher order geometric derivatives vanish for lower order Cartesian multipole moments */
        if (real_order_cart_mm<0) return GSUCCESS;
        /* sets the number of centers in the integrand and their indices
           and orders of geometric derivatives */
        if (cart_mm->order_cart_mm==0) {
            num_cent_integrand = 2;
            idx_geo_bra = 0;
            idx_geo_ket = 1;
        }
        else {
            num_cent_integrand = 3;
            idx_integrand[0] = cart_mm->idx_diporg;
            order_geo_integrand[0] = cart_mm->order_geo_diporg;
            idx_geo_bra = 1;
            idx_geo_ket = 2;
        }
        idx_integrand[idx_geo_bra] = idx_bra;
        idx_integrand[idx_geo_ket] = idx_ket;
        if (num_geo_cent_bra==0) {
            order_geo_integrand[idx_geo_bra] = 0;
        }
        else {
            ierr = GeoDerivCombinGetOrderCent(geo_deriv_bra,
                                              1,
                                              &order_geo_integrand[idx_geo_bra]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetOrderCent (bra)");
        }
        if (num_geo_cent_ket==0) {
            order_geo_integrand[idx_geo_ket] = 0;
        }
        else {
            ierr = GeoDerivCombinGetOrderCent(geo_deriv_ket,
                                              1,
                                              &order_geo_integrand[idx_geo_ket]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetOrderCent (ket)");
        }
        /* creates the context of partial geometric derivatives on the integrand */
        ierr = PartGeoCreate(&part_geo,
                             geo_deriv,
                             num_cent_integrand,
                             idx_integrand,
                             &zero_value);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoCreate");
        /* due to geometric derivatives on the integrand, the values are zero */
        if (zero_value==GTRUE) return GSUCCESS;
        /* gets the size of partial geometric derivatives */
        ierr = PartGeoGetSize(&part_geo, &size_part_geo);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetSize");
        /* allocates memory for the context of the triangle based recurrence relations */
        recur_cart_mm = (TriangleRecur *)malloc(sizeof(TriangleRecur));
        if (recur_cart_mm==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for recur_cart_mm");
        }
        /* checks the derivatives w.r.t. magnetic field and total rotational angular momentum */
        if (cart_mm->order_mag!=0 ||
            cart_mm->order_mag_bra!=0 ||
            cart_mm->order_mag_ket!=0) {
            if (cart_mm->mag_LPF==NULL) return GSUCCESS;
            /* computes derivatives w.r.t. magnetic field and total rotational angular momentum */
            if (cart_mm->order_rot!=0 ||
                cart_mm->order_rot_bra!=0 ||
                cart_mm->order_rot_ket!=0) {
                if (cart_mm->rot_LPF==NULL) return GSUCCESS;

            }
            /* computes derivatives w.r.t. magnetic field */
            else {

            }
        }
        /* computes derivatives w.r.t. total rotational angular momentum */
        else if (cart_mm->order_rot!=0 ||
                 cart_mm->order_rot_bra!=0 ||
                 cart_mm->order_rot_ket!=0) {
            if (cart_mm->rot_LPF==NULL) return GSUCCESS;

        }
        /* no derivatives w.r.t. magnetic field and total rotational angular momentum */
        else {
//            num_indices = 7;     /* number of indices involved in recurrence relations */
//            idx_GTO_bra = 0;     /* index of the CGTOs/HGTOs/SGTOs on the bra center */
//            idx_GTO_ket = 1;     /* index of the CGTOs/HGTOs/SGTOs on the ket center */
//            idx_elec_deriv = 2;  /* index of the electronic derivatives */
//            idx_cart_mm = 3;     /* index of the Cartesian multipole moments */
//            idx_geo_diporg = 4;  /* index of geometric derivatives on the dipole origin */
//            idx_geo_bra = 5;     /* index of the geometric derivatives on the bra center */
//            idx_geo_ket = 6;     /* index of the geometric derivatives on the ket center */
//            /* assigns the orders of operator and CGTOs/SGTOs after recurrence relations */
//            idx_orders[idx_GTO_bra] = contr_GTO_bra->angular_num;
//            idx_orders[idx_GTO_ket] = contr_GTO_ket->angular_num;
//            idx_orders[idx_elec_deriv] = cart_mm->order_elec_deriv;
//            idx_orders[idx_cart_mm] = cart_mm->order_cart_mm;
//            /* indices for HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//            idx_HGTO_to_cart_mm[0] = idx_cart_mm;
//            idx_HGTO_to_cart_mm[1] = idx_geo_bra;
//            idx_HGTO_to_cart_mm[2] = idx_geo_ket;
//            /* indices for HGTO (bra) -> HGTO (ket) */
//            idx_HGTO_bra_to_ket[0] = idx_geo_bra;
//            idx_HGTO_bra_to_ket[1] = idx_geo_ket;
//            /* Cartesian GTOs on the ket center */
//            if (contr_GTO_ket->GTO_type==CARTESIAN_GTO) {
//                /* indices for Cartesian multipole moments -> geometric derivatives on the dipole origin */
//                idx_cart_deriv[0] = idx_cart_mm;
//                idx_cart_deriv[1] = idx_geo_diporg;
//                /* indices for HGTO (ket) -> CGTO (ket) + geometric derivatives (ket) */
//                idx_HGTO_to_CGTO_ket[0] = idx_GTO_ket;
//                idx_HGTO_to_CGTO_ket[1] = idx_geo_ket;
//                /* Cartesian GTOs also on the bra center */
//                if (contr_GTO_bra->GTO_type==CARTESIAN_GTO) {
//                    /* indices for HGTO (bra) -> CGTO (bra) + geometric derivatives (bra) */
//                    idx_HGTO_to_CGTO_bra[0] = idx_GTO_bra;
//                    idx_HGTO_to_CGTO_bra[1] = idx_geo_bra;
//                    /* indices for HGTO (ket) -> electronic derivatives */
//                    idx_ket_to_elec_deriv[0] = idx_elec_deriv;
//                    idx_ket_to_elec_deriv[1] = idx_geo_ket;
//                    /* right hand side for HGTO (ket) -> electronic derivatives */
//                    RHS_ket_to_elec_deriv[0] = -cart_mm->order_elec_deriv;
//                    RHS_ket_to_elec_deriv[1] = cart_mm->order_elec_deriv;
//                    /* loops over the compositions of the partial geometric derivatives */
//                    for (igeo=1; igeo<=size_part_geo; igeo++) {
//                        if (num_cent_integrand==2) {
//                            /* gets the composition of the partial geometric derivatives */
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_bra]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            /* adds the orders of geometric derivatives during recurrence relations */
//                            idx_orders[idx_geo_diporg] = 0;
//                            idx_orders[idx_geo_bra] += order_geo_integrand[0];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[1];
//                        }
//                        else {
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_diporg]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            idx_orders[idx_geo_diporg] += order_geo_integrand[0];
//                            /* higher order geometric derivatives vanish for lower
//                               order Cartesian multipole moments */
//                            if (idx_orders[idx_cart_mm]<idx_orders[idx_geo_diporg]) {
//                                continue;
//                            }
//                            idx_orders[idx_geo_bra] += order_geo_integrand[1];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[2];
//                        }
///* FIXME: for primitive GTOs only */
//                        /* creates the context of triangle based recurrence relations, include
//                           (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) contractions
//                           (1) HGTO (bra) -> CGTO (bra) + geometric derivatives (bra)
//                           (2) HGTO (ket) -> CGTO (ket) + geometric derivatives (ket)
//                           (*) HGTO (ket) -> electronic derivatives
//                           (3) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments
//                           (4) HGTO (bra) -> HGTO (ket)
//                           (5) HGTO (bra) */
//                        ierr = TriangleRecurCreate(recur_cart_mm,
//                                                   num_indices,
//                                                   idx_orders);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurCreate");
//                        /* (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (1) HGTO (bra) -> CGTO (bra) + geometric derivatives (bra) */
//                        if (idx_orders[idx_geo_diporg]>0) {
//                            RHS_cart_deriv[0] = -idx_orders[idx_geo_diporg];
//                            RHS_cart_deriv[1] = -idx_orders[idx_geo_diporg];
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    NUM_IDX_HGTO_TO_CGTO,
//                                                    idx_HGTO_to_CGTO_bra,
//                                                    idx_GTO_bra,
//                                                    //CARTMM_NUM_IDX_CART_DERIV,
//                                                    //idx_cart_deriv,
//                                                    //RHS_cart_deriv,
//                                                    NUM_RHS_HGTO_TO_CGTO,
//                                                    RHS_TERMS_HGTO_TO_CGTO);
//                        }
//                        else {
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    NUM_IDX_HGTO_TO_CGTO,
//                                                    idx_HGTO_to_CGTO_bra,
//                                                    idx_GTO_bra,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    NUM_RHS_HGTO_TO_CGTO,
//                                                    RHS_TERMS_HGTO_TO_CGTO);
//                        }
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        /* (2) HGTO (ket) -> CGTO (ket) + geometric derivatives (ket) */
//                        ierr = TriangleRecurAdd(recur_cart_mm,
//                                                NUM_IDX_HGTO_TO_CGTO,
//                                                idx_HGTO_to_CGTO_ket,
//                                                idx_GTO_ket,
//                                                //0,
//                                                //NULL,
//                                                //NULL,
//                                                NUM_RHS_HGTO_TO_CGTO,
//                                                RHS_TERMS_HGTO_TO_CGTO);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        /* (*) HGTO (ket) -> electronic derivatives
//                           (3) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//                        if (how_many_recur==4) {
//                            if (idx_orders[idx_elec_deriv]>0) {
//                                ierr = TriangleRecurAdd(recur_cart_mm,
//                                                        CARTMM_NUM_IDX_CART_MM,
//                                                        idx_HGTO_to_cart_mm,
//                                                        idx_cart_mm,
//                                                        //NUM_IDX_KET_TO_ELEC_DERIV,
//                                                        //idx_ket_to_elec_deriv,
//                                                        //RHS_ket_to_elec_deriv,
//                                                        CARTMM_NUM_RHS_CART_MM,
//                                                        CARTMM_RHS_TERMS_CART_MM);
//                            }
//                            else {
//                                ierr = TriangleRecurAdd(recur_cart_mm,
//                                                        CARTMM_NUM_IDX_CART_MM,
//                                                        idx_HGTO_to_cart_mm,
//                                                        idx_cart_mm,
//                                                        //0,
//                                                        //NULL,
//                                                        //NULL,
//                                                        CARTMM_NUM_RHS_CART_MM,
//                                                        CARTMM_RHS_TERMS_CART_MM);
//                            }
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                            /* (4) HGTO (bra) -> HGTO (ket) */
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_HGTO_KET,
//                                                    idx_HGTO_bra_to_ket,
//                                                    idx_geo_ket,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    CARTMM_NUM_RHS_HGTO_KET,
//                                                    CARTMM_RHS_TERMS_HGTO_KET);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//                        /* (*) HGTO (ket) -> electronic derivatives
//                           (4) HGTO (bra) -> HGTO (ket) */
//                        else {
//                            if (idx_orders[idx_elec_deriv]>0) {
//                                ierr = TriangleRecurAdd(recur_cart_mm,
//                                                        CARTMM_NUM_IDX_HGTO_KET,
//                                                        idx_HGTO_bra_to_ket,
//                                                        idx_geo_ket,
//                                                        //NUM_IDX_KET_TO_ELEC_DERIV,
//                                                        //idx_ket_to_elec_deriv,
//                                                        //RHS_ket_to_elec_deriv,
//                                                        CARTMM_NUM_RHS_HGTO_KET,
//                                                        CARTMM_RHS_TERMS_HGTO_KET);
//                            }
//                            else {
//                                ierr = TriangleRecurAdd(recur_cart_mm,
//                                                        CARTMM_NUM_IDX_HGTO_KET,
//                                                        idx_HGTO_bra_to_ket,
//                                                        idx_geo_ket,
//                                                        //0,
//                                                        //NULL,
//                                                        //NULL,
//                                                        CARTMM_NUM_RHS_HGTO_KET,
//                                                        CARTMM_RHS_TERMS_HGTO_KET);
//                            }
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//                        /* loops over primitive GTOs on the ket and bra centers */
//                        for (jprim=0; jprim<contr_GTO_ket->num_prim; jprim++) {
//                            for (iprim=0; iprim<contr_GTO_bra->num_prim; iprim++) {
//                                /* performs the recurrence relation of (5) HGTO (bra) */
//                                //ierr = Triangle??(num_inp_HGTO_ket, inp_orders_HGTO_ket);
//                                /* performs (4) HGTO (bra) -> HGTO (ket) */
//                                //ierr = Triangle??(&recur_HGTO_ket);
//                                /* performs (3) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//                                //ierr = Triangle??(&recur_cart_mm);
//                                /* performs (*) HGTO (ket) -> electronic derivatives */
//                                /* performs (2) HGTO (ket) -> CGTO (ket) + geometric derivatives (ket) */
//                                /* performs (1) HGTO (bra) -> CGTO (bra) + geometric derivatives (bra) */
//                            }
//                        }
//                        /* performs (*) contractions */
//                        /* performs (*) Cartesian multipole moments -> geometric derivatives on the dipole origin */
//
//                        /* gets geometric derivatives from the current composition */
//                        //if (num_cent_integrand==2) {
//                        //    ierr = PartGeoGetDeriv(&part_geo,
//                        //                           &idx_orders[idx_geo_bra],
//                        //                           size_integrand,
//                        //                           recur_cart_mm); /* ?? */
//                        //}
//                        //else {
//                        //    ierr = PartGeoGetDeriv(&part_geo,
//                        //                           &idx_orders[idx_geo_diporg],
//                        //                           size_integrand,
//                        //                           recur_cart_mm); /* ?? */
//                        //}
//                        //GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetDeriv");
//                        /* relativistic corrections */
//
//                        /* data type of the one-electron property */
//
//                        /* destroys the context of recurrence relations */
//                        ierr = TriangleRecurDestroy(recur_cart_mm);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurDestroy");
//                        /* moves to next composition of multinomial expansion of geometric derivatives */
//                        if (igeo<size_part_geo) {
//                            ierr = PartGeoNextComposition(&part_geo);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoNextComposition");
//                        }
//                    }
//                }
//                /* spherical GTOs on the bra center while Cartesian GTOs on the ket center */
//                else {
//                    /* (*) HGTO (ket) -> electronic derivatives
//                       (*) HGTO (bra) -> geometric derivatives (bra) */
//                    idx_SGTO[0] = idx_GTO_bra;
//                    RHS_SGTO[0] = -idx_orders[idx_GTO_bra];
//                    if (idx_orders[idx_elec_deriv]>0) {
//                        idx_SGTO[1] = idx_elec_deriv;
//                        idx_SGTO[2] = idx_geo_bra;
//                        idx_SGTO[3] = idx_geo_ket;
//                        RHS_SGTO[1] = -idx_orders[idx_elec_deriv];
//                        RHS_SGTO[2] = idx_orders[idx_GTO_bra];
//                        RHS_SGTO[3] = idx_orders[idx_GTO_ket]
//                                    + idx_orders[idx_elec_deriv];
//                        num_idx_SGTO = 4;
//                    }
//                    else {
//                        idx_SGTO[1] = idx_geo_bra;
//                        RHS_SGTO[1] = idx_orders[idx_GTO_bra];
//                        num_idx_SGTO = 2;
//                    }
//                    /* loops over the compositions of the partial geometric derivatives */
//                    for (igeo=1; igeo<=size_part_geo; igeo++) {
//                        if (num_cent_integrand==2) {
//                            /* gets the composition of the partial geometric derivatives */
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_bra]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            /* adds the orders of geometric derivatives during recurrence relations */
//                            idx_orders[idx_geo_diporg] = 0;
//                            idx_orders[idx_geo_bra] += order_geo_integrand[0];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[1];
//                            /* how many recurrence relations to be performed */
//                            how_many_recur = 2;
//                        }
//                        else {
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_diporg]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            idx_orders[idx_geo_diporg] += order_geo_integrand[0];
//                            /* higher order geometric derivatives vanish for lower
//                               order Cartesian multipole moments */
//                            if (idx_orders[idx_cart_mm]<idx_orders[idx_geo_diporg]) {
//                                continue;
//                            }
//                            else {
//                                if (idx_orders[idx_cart_mm]==idx_orders[idx_geo_diporg]) {
//                                    how_many_recur = 2;
//                                }
//                                else {
//                                    how_many_recur = 3;
//                                }
//                            }
//                            idx_orders[idx_geo_bra] += order_geo_integrand[1];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[2];
//                        }
//                        /* creates the context of triangle based recurrence relations, include
//                           (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) HGTO (bra) -> SGTO (bra)
//                           (*) contractions
//                           (1) HGTO (ket) -> CGTO (ket) + geometric derivatives (ket)
//                           (*) HGTO (ket) -> electronic derivatives
//                           (*) HGTO (bra) -> geometric derivatives (bra)
//                           (2) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments
//                           (3) HGTO (bra) -> HGTO (ket)
//                           (4) HGTO (bra) */
//                        ierr = TriangleRecurCreate(recur_cart_mm,
//                                                   num_indices,
//                                                   idx_orders);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurCreate");
//                        /* (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (1) HGTO (ket) -> CGTO (ket) + geometric derivatives (ket) */
//                        if (idx_orders[idx_geo_diporg]>0) {
//                            RHS_cart_deriv[0] = -idx_orders[idx_geo_diporg];
//                            RHS_cart_deriv[1] = -idx_orders[idx_geo_diporg];
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    NUM_IDX_HGTO_TO_CGTO,
//                                                    idx_HGTO_to_CGTO_ket,
//                                                    idx_GTO_ket,
//                                                    //CARTMM_NUM_IDX_CART_DERIV,
//                                                    //idx_cart_deriv,
//                                                    //RHS_cart_deriv,
//                                                    NUM_RHS_HGTO_TO_CGTO,
//                                                    RHS_TERMS_HGTO_TO_CGTO);
//                        }
//                        else {
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    NUM_IDX_HGTO_TO_CGTO,
//                                                    idx_HGTO_to_CGTO_ket,
//                                                    idx_GTO_ket,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    NUM_RHS_HGTO_TO_CGTO,
//                                                    RHS_TERMS_HGTO_TO_CGTO);
//                        }
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        /* (2) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//                        if (how_many_recur==3) {
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_CART_MM,
//                                                    idx_HGTO_to_cart_mm,
//                                                    idx_cart_mm,
//                                                    //num_idx_SGTO,
//                                                    //idx_SGTO,
//                                                    //RHS_SGTO,
//                                                    CARTMM_NUM_RHS_CART_MM,
//                                                    CARTMM_RHS_TERMS_CART_MM);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                            /* (3) HGTO (bra) -> HGTO (ket) */
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_HGTO_KET,
//                                                    idx_HGTO_bra_to_ket,
//                                                    idx_geo_ket,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    CARTMM_NUM_RHS_HGTO_KET,
//                                                    CARTMM_RHS_TERMS_HGTO_KET);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//                        else {
//                            /* (3) HGTO (bra) -> HGTO (ket) */
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_HGTO_KET,
//                                                    idx_HGTO_bra_to_ket,
//                                                    idx_geo_ket,
//                                                    //num_idx_SGTO,
//                                                    //idx_SGTO,
//                                                    //RHS_SGTO,
//                                                    CARTMM_NUM_RHS_HGTO_KET,
//                                                    CARTMM_RHS_TERMS_HGTO_KET);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//
//                        /* gets geometric derivatives from the current composition */
//                        //ierr = PartGeoGetDeriv(&part_geo,
//                        //                       idx_orders[],
//                        //                       size_integrand,
//                        //                       recur_cart_mm); /* ?? */
//                        //GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetDeriv");
//
//                        /* destroys the context of recurrence relations */
//                        ierr = TriangleRecurDestroy(recur_cart_mm);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurDestroy");
//                        /* moves to next composition of multinomial expansion of geometric derivatives */
//                        if (igeo<size_part_geo) {
//                            ierr = PartGeoNextComposition(&part_geo);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoNextComposition");
//                        }
//                    }
//                }
//            }
//            /* spherical GTOs on the ket center */
//            else {
//                /* Cartesian GTOs on the bra center */
//                if (contr_GTO_bra->GTO_type==CARTESIAN_GTO) {
//                    /* (*) HGTO (ket) -> geometric derivatives (ket) */
//                    idx_SGTO[0] = idx_GTO_ket;
//                    RHS_SGTO[0] = -idx_orders[idx_GTO_ket];
//                    /* loops over the compositions of the partial geometric derivatives */
//                    for (igeo=1; igeo<=size_part_geo; igeo++) {
//                        if (num_cent_integrand==2) {
//                            /* gets the composition of the partial geometric derivatives */
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_bra]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            /* adds the orders of geometric derivatives during recurrence relations */
//                            idx_orders[idx_geo_diporg] = 0;
//                            idx_orders[idx_geo_bra] += order_geo_integrand[0];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[1];
//                            /* how many recurrence relations to be performed */
//                            how_many_recur = 2;
//                        }
//                        else {
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_diporg]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            idx_orders[idx_geo_diporg] += order_geo_integrand[0];
//                            /* higher order geometric derivatives vanish for lower
//                               order Cartesian multipole moments */
//                            if (idx_orders[idx_cart_mm]<idx_orders[idx_geo_diporg]) {
//                                continue;
//                            }
//                            else {
//                                if (idx_orders[idx_cart_mm]==idx_orders[idx_geo_diporg]) {
//                                    how_many_recur = 2;
//                                }
//                                else {
//                                    how_many_recur = 3;
//                                }
//                            }
//                            idx_orders[idx_geo_bra] += order_geo_integrand[1];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[2];
//                        }
//                        /* creates the context of triangle based recurrence relations, include
//                           (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) HGTO (ket) -> SGTO (ket)
//                           (*) contractions
//                           (*) HGTO (ket) -> electronic derivatives
//                           (*) HGTO (ket) -> geometric derivatives (ket)
//                           (1) HGTO (bra) -> CGTO (bra) + geometric derivatives (bra)
//                           (2) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments
//                           (3) HGTO (bra) -> HGTO (ket)
//                           (4) HGTO (bra) */
//                        ierr = TriangleRecurCreate(recur_cart_mm,
//                                                   num_indices,
//                                                   idx_orders);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurCreate");
//                        /* (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) HGTO (ket) -> SGTO (ket)
//                           (*) HGTO (ket) -> electronic derivatives
//                           (*) HGTO (ket) -> geometric derivatives (ket) */
//                        if (idx_orders[idx_elec_deriv]>0) {
//                            if (idx_orders[idx_geo_diporg]>0) {
//                                idx_SGTO[1] = idx_elec_deriv;
//                                idx_SGTO[2] = idx_cart_mm;
//                                idx_SGTO[3] = idx_geo_diporg;
//                                idx_SGTO[4] = idx_geo_ket;
//                                RHS_SGTO[1] = -idx_orders[idx_elec_deriv];
//                                RHS_SGTO[2] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[3] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[4] = idx_orders[idx_GTO_ket]
//                                            + idx_orders[idx_elec_deriv];
//                                num_idx_SGTO = 5;
//                            }
//                            else {
//                                idx_SGTO[1] = idx_elec_deriv;
//                                idx_SGTO[2] = idx_geo_ket;
//                                RHS_SGTO[1] = -idx_orders[idx_elec_deriv];
//                                RHS_SGTO[2] = idx_orders[idx_GTO_ket]
//                                            + idx_orders[idx_elec_deriv];
//                                num_idx_SGTO = 3;
//                            }
//                        }
//                        else {
//                            if (idx_orders[idx_geo_diporg]>0) { 
//                                idx_SGTO[1] = idx_cart_mm;
//                                idx_SGTO[2] = idx_geo_diporg;
//                                idx_SGTO[3] = idx_geo_ket;
//                                RHS_SGTO[1] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[2] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[3] = idx_orders[idx_GTO_ket];
//                                num_idx_SGTO = 4;   
//                            }
//                            else {
//                                idx_SGTO[1] = idx_geo_ket;
//                                RHS_SGTO[1] = idx_orders[idx_GTO_ket];
//                                num_idx_SGTO = 2;
//                            }
//                        }
//                        /* (1) HGTO (bra) -> CGTO (bra) + geometric derivatives (bra) */
//                        ierr = TriangleRecurAdd(recur_cart_mm,
//                                                NUM_IDX_HGTO_TO_CGTO,
//                                                idx_HGTO_to_CGTO_bra,
//                                                idx_GTO_bra,
//                                                //num_idx_SGTO,
//                                                //idx_SGTO,
//                                                //RHS_SGTO,
//                                                NUM_RHS_HGTO_TO_CGTO,
//                                                RHS_TERMS_HGTO_TO_CGTO);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        /* (2) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//                        if (how_many_recur==3) {
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_CART_MM,
//                                                    idx_HGTO_to_cart_mm,
//                                                    idx_cart_mm,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    CARTMM_NUM_RHS_CART_MM,
//                                                    CARTMM_RHS_TERMS_CART_MM);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//                        /* (3) HGTO (bra) -> HGTO (ket) */
//                        ierr = TriangleRecurAdd(recur_cart_mm,
//                                                CARTMM_NUM_IDX_HGTO_KET,
//                                                idx_HGTO_bra_to_ket,
//                                                idx_geo_ket,
//                                                //0,
//                                                //NULL,
//                                                //NULL,
//                                                CARTMM_NUM_RHS_HGTO_KET,
//                                                CARTMM_RHS_TERMS_HGTO_KET);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//
//                        /* gets geometric derivatives from the current composition */
//                        //ierr = PartGeoGetDeriv(&part_geo,
//                        //                       idx_orders[],
//                        //                       size_integrand,
//                        //                       recur_cart_mm); /* ?? */
//                        //GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetDeriv");
//
//                        /* destroys the context of recurrence relations */
//                        ierr = TriangleRecurDestroy(recur_cart_mm);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurDestroy");
//                        /* moves to next composition of multinomial expansion of geometric derivatives */
//                        if (igeo<size_part_geo) {
//                            ierr = PartGeoNextComposition(&part_geo);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoNextComposition");
//                        }
//                    }
//                }
//                /* spherical GTOs also on the bra center */
//                else {
//                    /* (*) HGTO (ket) -> geometric derivatives (ket)
//                       (*) HGTO (bra) -> geometric derivatives (bra) */
//                    idx_SGTO[0] = idx_GTO_bra;
//                    idx_SGTO[1] = idx_GTO_ket;
//                    RHS_SGTO[0] = -idx_orders[idx_GTO_bra];
//                    RHS_SGTO[1] = -idx_orders[idx_GTO_ket];
//                    /* loops over the compositions of the partial geometric derivatives */
//                    for (igeo=1; igeo<=size_part_geo; igeo++) {
//                        if (num_cent_integrand==2) {
//                            /* gets the composition of the partial geometric derivatives */
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_bra]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            /* adds the orders of geometric derivatives during recurrence relations */
//                            idx_orders[idx_geo_diporg] = 0;
//                            idx_orders[idx_geo_bra] += order_geo_integrand[0];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[1];
//                            /* how many recurrence relations to be performed */
//                            how_many_recur = 1;
//                        }
//                        else {
//                            ierr = PartGeoGetComposition(&part_geo,
//                                                         &mult_coef,
//                                                         &idx_orders[idx_geo_diporg]);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetComposition");
//                            idx_orders[idx_geo_diporg] += order_geo_integrand[0];
//                            /* higher order geometric derivatives vanish for lower
//                               order Cartesian multipole moments */
//                            if (idx_orders[idx_cart_mm]<idx_orders[idx_geo_diporg]) {
//                                continue;
//                            }
//                            else {
//                                if (idx_orders[idx_cart_mm]==idx_orders[idx_geo_diporg]) {
//                                    how_many_recur = 1;
//                                }
//                                else {
//                                    how_many_recur = 2;
//                                }
//                            }
//                            idx_orders[idx_geo_bra] += order_geo_integrand[1];
//                            idx_orders[idx_geo_ket] += order_geo_integrand[2];
//                        }
//                        /* creates the context of triangle based recurrence relations, include
//                           (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) HGTO (bra) -> SGTO (bra)
//                           (*) HGTO (ket) -> SGTO (ket)
//                           (*) contractions
//                           (*) HGTO (ket) -> electronic derivatives
//                           (*) HGTO (ket) -> geometric derivatives (ket)
//                           (*) HGTO (bra) -> geometric derivatives (bra)
//                           (1) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments
//                           (2) HGTO (bra) -> HGTO (ket)
//                           (3) HGTO (bra) */
//                        ierr = TriangleRecurCreate(recur_cart_mm,
//                                                   num_indices,
//                                                   idx_orders);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurCreate");
//                        /* (*) Cartesian multipole moments -> geometric derivatives on the dipole origin
//                           (*) HGTO (bra) -> SGTO (bra)
//                           (*) HGTO (ket) -> SGTO (ket)
//                           (*) HGTO (ket) -> electronic derivatives
//                           (*) HGTO (ket) -> geometric derivatives (ket)
//                           (*) HGTO (bra) -> geometric derivatives (bra) */
//                        if (idx_orders[idx_elec_deriv]>0) {
//                            if (idx_orders[idx_geo_diporg]>0) {
//                                idx_SGTO[2] = idx_elec_deriv;
//                                idx_SGTO[3] = idx_cart_mm;
//                                idx_SGTO[4] = idx_geo_diporg;
//                                idx_SGTO[5] = idx_geo_bra;
//                                idx_SGTO[6] = idx_geo_ket;
//                                RHS_SGTO[2] = -idx_orders[idx_elec_deriv];
//                                RHS_SGTO[3] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[4] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[5] = idx_orders[idx_GTO_bra];
//                                RHS_SGTO[6] = idx_orders[idx_GTO_ket]
//                                            + idx_orders[idx_elec_deriv];
//                                num_idx_SGTO = 7;
//                            }
//                            else {
//                                idx_SGTO[2] = idx_elec_deriv;
//                                idx_SGTO[3] = idx_geo_bra;
//                                idx_SGTO[4] = idx_geo_ket;
//                                RHS_SGTO[2] = -idx_orders[idx_elec_deriv];
//                                RHS_SGTO[3] = idx_orders[idx_GTO_bra];
//                                RHS_SGTO[4] = idx_orders[idx_GTO_ket]
//                                            + idx_orders[idx_elec_deriv];
//                                num_idx_SGTO = 5;
//                            }
//                        }
//                        else {
//                            if (idx_orders[idx_geo_diporg]>0) {
//                                idx_SGTO[2] = idx_cart_mm;
//                                idx_SGTO[3] = idx_geo_diporg;
//                                idx_SGTO[4] = idx_geo_bra;
//                                idx_SGTO[5] = idx_geo_ket;
//                                RHS_SGTO[2] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[3] = -idx_orders[idx_geo_diporg];
//                                RHS_SGTO[4] = idx_orders[idx_GTO_bra];
//                                RHS_SGTO[5] = idx_orders[idx_GTO_ket];
//                                num_idx_SGTO = 6;
//                            }
//                            else {
//                                idx_SGTO[2] = idx_geo_bra;
//                                idx_SGTO[3] = idx_geo_ket;
//                                RHS_SGTO[2] = idx_orders[idx_GTO_bra];
//                                RHS_SGTO[3] = idx_orders[idx_GTO_ket];
//                                num_idx_SGTO = 4;
//                            }
//                        }
//                        /* (1) HGTO (bra) + HGTO (ket) -> Cartesian multipole moments */
//                        if (how_many_recur==2) {
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_CART_MM,
//                                                    idx_HGTO_to_cart_mm,
//                                                    idx_cart_mm,
//                                                    //num_idx_SGTO,
//                                                    //idx_SGTO,
//                                                    //RHS_SGTO,
//                                                    CARTMM_NUM_RHS_CART_MM,
//                                                    CARTMM_RHS_TERMS_CART_MM);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                            /* (2) HGTO (bra) -> HGTO (ket) */
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_HGTO_KET,
//                                                    idx_HGTO_bra_to_ket,
//                                                    idx_geo_ket,
//                                                    //0,
//                                                    //NULL,
//                                                    //NULL,
//                                                    CARTMM_NUM_RHS_HGTO_KET,
//                                                    CARTMM_RHS_TERMS_HGTO_KET);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//                        else {
//                            /* (2) HGTO (bra) -> HGTO (ket) */
//                            ierr = TriangleRecurAdd(recur_cart_mm,
//                                                    CARTMM_NUM_IDX_HGTO_KET,
//                                                    idx_HGTO_bra_to_ket,
//                                                    idx_geo_ket,
//                                                    //num_idx_SGTO,
//                                                    //idx_SGTO,
//                                                    //RHS_SGTO,
//                                                    CARTMM_NUM_RHS_HGTO_KET,
//                                                    CARTMM_RHS_TERMS_HGTO_KET);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurAdd");
//                        }
//
//                        /* gets geometric derivatives from the current composition */
//                        //ierr = PartGeoGetDeriv(&part_geo,
//                        //                       idx_orders[],
//                        //                       size_integrand,
//                        //                       recur_cart_mm); /* ?? */
//                        //GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoGetDeriv");
//
//                        /* destroys the context of recurrence relations */
//                        ierr = TriangleRecurDestroy(recur_cart_mm);
//                        GErrorCheckCode(ierr, FILE_AND_LINE, "calling TriangleRecurDestroy");
//                        /* moves to next composition of multinomial expansion of geometric derivatives */
//                        if (igeo<size_part_geo) {
//                            ierr = PartGeoNextComposition(&part_geo);
//                            GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoNextComposition");
//                        }
//                    }
//                }
//            }
        }
        /* frees the memory used by the context of the triangle based recurrence relations */
        free(recur_cart_mm);
        recur_cart_mm = NULL;
        /* destroys the context of partial geometric derivatives */
        ierr = PartGeoDestroy(&part_geo);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoDestroy");
    }
    return GSUCCESS;
}
