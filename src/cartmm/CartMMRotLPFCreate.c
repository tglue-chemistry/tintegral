/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMRotLPFCreate().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief creates the context of London phase factor for total rotational
        angular momentum derivatives of the Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \param[GReal:real]{in} center_of_mass center of mass of the system
    \param[GReal:real]{in} diag_inv_inertia diagonal elements of the
        inverse of inertia tensor
    \param[GReal:real]{in} origin_LPF origin of the London phase factor
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMRotLPFCreate(OneProp *one_prop,
                              const GReal center_of_mass[3],
                              const GReal diag_inv_inertia[3],
                              const GReal origin_LPF[3])
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    if (cart_mm->rot_LPF==NULL) {
        cart_mm->rot_LPF = (RotLPF *)malloc(sizeof(RotLPF));
        if (cart_mm->rot_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for cart_mm->rot_LPF");
        }
    }
    RotLPFCreate(cart_mm->rot_LPF, center_of_mass, diag_inv_inertia, origin_LPF);
    return GSUCCESS;
}
