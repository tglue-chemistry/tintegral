/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMSetSRC().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief adds the scalar relativistic (SR) correction for Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMSetSRC(OneProp *one_prop)
{
    CartMM *cart_mm;            /* context of Cartesian multipole moments */
    OneProp diporg_deriv_node;  /* new node for the derivatives with respect to the dipole origin */
    GErrorCode ierr;            /* error information */
    /* gets the context of the Cartesian multipole moments */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    /* there was no relativistic correction before */
    if (cart_mm->RC_type==NR_CORRECTION) {
        /* sets the type of relativistic corrections */
        RelCorrectionSetType(&cart_mm->RC_type, SR_CORRECTION);
        /* updates for the second order differentiated charge distributions */
        cart_mm->DCD_type = DIFF_CD_SECOND;
        cart_mm->order_elec_deriv += 2;
        /* derivatives with respect to the dipole origin */
        if (cart_mm->order_geo_diporg<cart_mm->order_cart_mm) {
            /* creates a new node */
            ierr = OnePropNodeCreateHEAD(&diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeCreateHEAD");
            /* duplicates the conext of the Cartesian multipole moments to the new node */
            ierr = OnePropNodeDuplicate(*one_prop, diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeDuplicate");
            /* updates the conext of the Cartesian multipole moments for SOC */
            cart_mm = (CartMM *)(diporg_deriv_node->data);
            cart_mm->DCD_type = DIFF_CD_FIRST;
            cart_mm->scal_const = -cart_mm->scal_const;
            cart_mm->order_geo_diporg++;
            cart_mm->order_elec_deriv--;
            /* inserts the node after the current node */
            ierr = OnePropListInsertAfterHEAD(*one_prop, diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropListInsertAfterHEAD");
            /* moves the current node to the new node */
            *one_prop = diporg_deriv_node;
        }
    }
    else {
        /* only updates the type of relativistic corrections */
        RelCorrectionSetType(&cart_mm->RC_type, SR_CORRECTION);
    }
    return GSUCCESS;
}
