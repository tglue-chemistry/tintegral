/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMGetShellIntegral().

   2014-08-31, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief calculates the integral matrices of Cartesian multipole moments
    \author Bin Gao
    \date 2014-08-31
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[GeoDeriv:struct]{in} geo_deriv geometric derivatives
    \param[GeoDeriv:struct]{in} geo_deriv_bra geometric derivatives on the bra center
    \param[GeoDeriv:struct]{in} geo_deriv_ket geometric derivatives on the ket center
    \param[AOShell:struct]{in} ao_shell_bra AO shell on the bra center
    \param[AOShell:struct]{in} ao_shell_ket AO shell on the ket center
    \param[GInt:int]{in} idx_block_row indices of the block rows
    \param[GInt:int]{in} idx_block_col indices of the block columns
    \param[GInt:int]{in} idx_first_row index of the first row to put integrals
    \param[GInt:int]{in} idx_first_col index of the first column to put integrals
    \param[GBool:enum]{in} add_values indicates if adding values to the existing entries
    \param[QMat:struct]{inout} val_int the integral matrices of the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMGetShellIntegral(const OneProp one_prop,
                                  const GeoDeriv *geo_deriv,
                                  const GeoDeriv *geo_deriv_bra,
                                  const GeoDeriv *geo_deriv_ket,
                                  const AOShell *ao_shell_bra,
                                  const AOShell *ao_shell_ket,
                                  const GInt *idx_block_row,
                                  const GInt *idx_block_col,
                                  const GInt idx_first_row,
                                  const GInt idx_first_col,
                                  const GBool add_values,
                                  QMat *val_int[])
{
    GBool zero_value;           /* indicates if the value is zero */
    GInt num_geo_deriv;         /* number of geometric derivatives */
    GInt num_geo_deriv_bra;     /* number of partial geometric derivatives on the bra center */
    GInt num_geo_deriv_ket;     /* number of partial geometric derivatives on the ket center */
    GInt geo_num_cent;          /* number of differentiated centers on the bra or ket center */
    GInt num_cartmm_cent;       /* number of centers of integrand (basis sets and operator centers) */
    GInt idx_cartmm_cent[3];    /* indices of the centers of integrand */
    GInt order_cartmm_cent[3];  /* orders of geometric derivatives on the centers of integrand */
    CartMM *cart_mm;            /* context of Cartesian multipole moments */
    GInt size_prop_deriv;       /* size of the one-electron property and derivatives */
    GInt num_AO_bra;            /* number of AOs of shell on the bra center */
    GInt num_AO_ket;            /* number of AOs of shell on the ket center */
    GDataType data_type;        /* data type of the one-electron property */
    GInt size_values;           /* size of integral values */
    GReal *values_real;         /* real part of the integral values */
    GReal *values_imag;         /* imaginary part of the integral values */
    GBool is_SOC;               /* if spin-orbit correction added */
    GInt imat,iblk;             /* incremental recoders */
    GErrorCode ierr;            /* error information */
    /* partial geometric derivatives on the bra center */
    if (geo_deriv_bra==NULL) {
        num_geo_deriv_bra = 1;
        order_cartmm_cent[0] = 0;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_bra, &num_geo_deriv_bra);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (bra)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_bra, &geo_num_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (bra)");
        switch (geo_num_cent) {
        /* no geometric derivatives */
        case 0:
            order_cartmm_cent[0] = 0;
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetOrderCent(geo_deriv_bra, 1, &order_cartmm_cent[0]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetOrderCent (bra)");
            break;
        /* more than one differentiated center */
        default:
            GErrorExit(FILE_AND_LINE, "more than one differentiated center on bra center");
        }
    }
    /* partial geometric derivatives on the ket center */
    if (geo_deriv_ket==NULL) {
        num_geo_deriv_ket = 1;
        order_cartmm_cent[1] = 0;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_ket, &num_geo_deriv_ket);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (ket)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_ket, &geo_num_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (ket)");
        switch (geo_num_cent) {
        /* no geometric derivatives */
        case 0:
            order_cartmm_cent[1] = 0;
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetOrderCent(geo_deriv_ket, 1, &order_cartmm_cent[1]);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetOrderCent (ket)");
            break;
        /* more than one differentiated center */
        default:
            GErrorExit(FILE_AND_LINE, "more than one differentiated center on ket center");
        }
    }
    /* gets the context of the Cartesian multipole moments */
    cart_mm = (CartMM *)(one_prop->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    if (cart_mm->order_cart_mm==0) {
        num_cartmm_cent = 2;
    }
    else {
        num_cartmm_cent = 3;
        order_cartmm_cent[2] = cart_mm->order_geo_diporg;
        idx_cartmm_cent[0] = cart_mm->idx_diporg;
    }
    /* first assumes the value is not zero */
    zero_value = GFALSE;
    /* geometric derivatives */
    if (geo_deriv==NULL) {
        num_geo_deriv = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv, &num_geo_deriv);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv");

        idx_cartmm_cent[1] = ao_shell_bra->idx_cent;
        idx_cartmm_cent[2] = ao_shell_ket->idx_cent;

//        /* zero for one-center geometric derivatives due to translation invairance */
//        if (geo_deriv->curr_num_cent==1 && num_diff_cent==num_integrand_cent) {
//            *zero_deriv = GTRUE;
//        }

        ierr = PartGeoCreate(part_geo,
                             geo_deriv,
                             num_cartmm_cent,
                             idx_cartmm_cent,
                             &zero_value);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling PartGeoCreate");

        /* gets the number of differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv, &geo_num_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent");
        /* more than three differentiated center */
        if (geo_num_cent>3) {
            zero_value = GTRUE;
        }
        else if (geo_num_cent>0) {
            /* checks the indices of differentiated centers */
            GeoDerivCombinSetPartGeo(geo_deriv,
                                     3,
                                     idx_cartmm_cent[3],
                                     order_cartmm_cent[3],
                                     &zero_value);

            for (icent=0; icent<geo_num_cent; icent++) {

            }
        }

    }
    /* gets the size of the one-electron property, this function will
       also check the validity of the linked list */
    ierr = OnePropGetSize(one_prop, &size_prop_deriv);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropGetSize");
    size_prop_deriv *= num_geo_deriv_bra*num_geo_deriv_ket*num_geo_deriv;
    /* due to geometric derivatives, the value is zero */
    if (zero_value==GTRUE) {
        if (add_values=GFALSE) {
            /* gets the number of AOs */
            ierr = AOShellGetNumAO(ao_shell_bra, &num_AO_bra);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling AOShellGetNumAO (bra)");
            ierr = AOShellGetNumAO(ao_shell_ket, &num_AO_ket);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling AOShellGetNumAO (ket)");
            /* gets the data type of the one-electron property */
            ierr = OnePropGetDataType(one_prop, &data_type);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropGetDataType");
            /* sets zero values */
            size_values = num_AO_bra*num_AO_ket;
            values_real = NULL;
            values_imag = NULL;
            switch (data_type) {
            /* real property */
            case GREALPROP:
                values_real = (QReal *)malloc(size_values*sizeof(QReal));
                if (values_real==NULL) {
                    printf("CartMMGetShellIntegral>> number of AOs on bra center %"GINT_FMT"\n",
                           num_AO_bra);
                    printf("CartMMGetShellIntegral>> number of AOs on ket center %"GINT_FMT"\n",
                           num_AO_ket);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for values_real");
                }
                for (ival=0; ival<size_values; ival++) {
                    values_real[ival] = 0;
                }
                break;
            /* imaginary property */
            case GIMAGPROP:
                values_imag = (QReal *)malloc(size_values*sizeof(QReal));
                if (values_imag==NULL) {
                    printf("CartMMGetShellIntegral>> number of AOs on bra center %"GINT_FMT"\n",
                           num_AO_bra);
                    printf("CartMMGetShellIntegral>> number of AOs on ket center %"GINT_FMT"\n",
                           num_AO_ket);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for values_imag");
                }
                for (ival=0; ival<size_values; ival++) {
                    values_imag[ival] = 0;
                }
                break;
            /* complex property */
            case GCMPLXPROP:
                values_real = (QReal *)malloc(size_values*sizeof(QReal));
                if (values_real==NULL) {
                    printf("CartMMGetShellIntegral>> number of AOs on bra center %"GINT_FMT"\n",
                           num_AO_bra);
                    printf("CartMMGetShellIntegral>> number of AOs on ket center %"GINT_FMT"\n",
                           num_AO_ket);
                    GErrorExit(FILE_AND_LINE, "failed to allocate memory for values_real");
                }
                for (ival=0; ival<size_values; ival++) {
                    values_real[ival] = 0;
                }
                values_imag = values_real;
                break;
            default:
                printf("CartMMGetShellIntegral>> data type of the one-electron property %"GINT_FMT"\n",
                       data_type);
                GErrorExit(FILE_AND_LINE, "invalid data type");
            }
            /* checks if spin-orbit correction added  */
            ierr = OnePropIsSOC(one_prop, &is_SOC);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIsSOC");
            if (is_SOC==GFALSE && num_blocks!=1) {
                printf("CartMMGetShellIntegral>> input number of blocks %"GINT_FMT"\n",
                       num_blocks);
                GErrorExit(FILE_AND_LINE, "invalid number of blocks");
            }
            else if (is_SOC==GTRUE && num_blocks!=4) {
                printf("CartMMGetShellIntegral>> input number of blocks %"GINT_FMT"\n",
                       num_blocks);
                GErrorExit(FILE_AND_LINE, "invalid number of blocks (SOC)");
            }
            /* loops over matrices to set values */
            for (imat=0; imat<size_prop_deriv; imat++) {
                for (iblk=0; iblk<num_blocks; iblk++) {
                    ierr = QMatSetValues(val_int[imat],
                                         idx_block_row[iblk],
                                         idx_block_col[iblk],
                                         idx_first_row,
                                         num_AO_bra,
                                         idx_first_col,
                                         num_AO_ket,
                                         values_real,
                                         values_imag);
                    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QMatSetValues");
                }
            }
            /* cleans up */
            switch (data_type) {
            /* real property */
            case GREALPROP:
                free(values_real);
                values_real = NULL;
                break;
            /* imaginary property */
            case GIMAGPROP:
                free(values_imag);
                values_imag = NULL;
                break;
            /* complex property */
            default:
                free(values_real);
                values_real = NULL;
                values_imag = NULL;
            }
        }
    }
    else {

    }
    return GSUCCESS;
}
