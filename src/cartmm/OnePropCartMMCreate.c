/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function OnePropCartMMCreate().

   2014-06-26, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*@% \brief creates the context of Cartesian multipole moments
     \author Bin Gao
     \date 2014-06-26
     \param[OneProp:struct]{inout} one_prop the Cartesian multipole moments
     \param[GInt:int]{in} scal_const scale constant
     \param[GInt:int]{in} idx_diporg index of the dipole origin
     \param[GReal:real]{in} coord_diporg coordinates of the dipole origin
     \param[GInt:int]{in} order_geo_diporg order of geometric derivatives on the dipole origin
     \param[GInt:int]{in} order_cart_mm order of Cartesian multipole moments
     \param[GInt:int]{in} order_elec_deriv order of electronic derivatives
     \return[GErrorCode:int] error information
*/
GErrorCode OnePropCartMMCreate(OneProp *one_prop,
                               const GReal scal_const,
                               const GInt idx_diporg,
                               const GReal coord_diporg[3],
                               const GInt order_geo_diporg,
                               const GInt order_cart_mm,
                               const GInt order_elec_deriv)
{
    OneProp head_node;  /* HEAD of the linked list */
    CartMM *cart_mm;    /* context of Cartesian multipole moments */
    GErrorCode ierr;    /* error information */
    /* checks the validity of arguments */
    if (order_geo_diporg<0) {
        printf("OnePropCartMMCreate>> order of geometric derivatives on the dipole origin %"GINT_FMT"\n",
               order_geo_diporg);
        GErrorExit(FILE_AND_LINE, "invalid order of geometric derivatives on the dipole origin");
    }
    if (order_cart_mm<0) {
        printf("OnePropCartMMCreate>> order of Cartesian multipole moments %"GINT_FMT"\n",
               order_cart_mm);
        GErrorExit(FILE_AND_LINE, "invalid order of Cartesian multipole moments");
    }
    if (order_geo_diporg>order_cart_mm) {
        printf("OnePropCartMMCreate>> order of geometric derivatives on the dipole origin %"GINT_FMT"\n",
               order_geo_diporg);
        printf("OnePropCartMMCreate>> order of Cartesian multipole moments %"GINT_FMT"\n",
               order_cart_mm);
        GErrorExit(FILE_AND_LINE, "zero Cartesian multipole moments");
    }
    if (order_elec_deriv<0) {
        printf("OnePropCartMMCreate>> order of electronic derivatives %"GINT_FMT"\n",
               order_elec_deriv);
        GErrorExit(FILE_AND_LINE, "invalid order of electronic derivatives");
    }
    /* initializes the Cartesian multipole moments and its different derivatives */
    cart_mm = (CartMM *)malloc(sizeof(CartMM));
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for cart_mm");
    }
    cart_mm->order_mag = 0;
    cart_mm->order_mag_bra = 0;
    cart_mm->order_mag_ket = 0;
    cart_mm->mag_LPF = NULL;
    cart_mm->order_rot = 0;
    cart_mm->order_rot_bra = 0;
    cart_mm->order_rot_ket = 0;
    cart_mm->rot_LPF = NULL;
    cart_mm->RC_type = NR_CORRECTION;
    cart_mm->DCD_type = DIFF_CD_ZERO;
    cart_mm->scal_const = scal_const;
    cart_mm->idx_diporg = idx_diporg;
    cart_mm->coord_diporg[0] = coord_diporg[0];
    cart_mm->coord_diporg[1] = coord_diporg[1];
    cart_mm->coord_diporg[2] = coord_diporg[2];
    cart_mm->order_geo_diporg = order_geo_diporg;
    cart_mm->order_cart_mm = order_cart_mm;
    cart_mm->order_elec_deriv = order_elec_deriv;
    /* allocates memory for the abstract one-electron property,
       note that this is the HEAD of the linked list */
    head_node = (_p_OneProp *)malloc(sizeof(_p_OneProp));
    if (head_node==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for head_node");
    }
    /* sets the implementation-specific data of the one-electron property */
    head_node->data = (GVoid *)cart_mm;
    /* sets the implementation-specific functions of the one-electron property */
    head_node->one_prop_fun = (OnePropFun *)malloc(sizeof(OnePropFun));
    if (head_node->one_prop_fun==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for head_node->one_prop_fun");
    }
    head_node->one_prop_fun->onepropmagsetorder       = CartMMMagSetOrder;
    head_node->one_prop_fun->onepropmaglpfcreate      = CartMMMagLPFCreate;
    head_node->one_prop_fun->onepropmaglpfsetgiao     = CartMMMagLPFSetGIAO;
    head_node->one_prop_fun->onepropmaglpfsetgiop     = CartMMMagLPFSetGIOP;
    head_node->one_prop_fun->oneproprotsetorder       = CartMMRotSetOrder;
    head_node->one_prop_fun->oneproprotlpfcreate      = CartMMRotLPFCreate;
    head_node->one_prop_fun->oneproprotlpfsetciao     = CartMMRotLPFSetCIAO;
    head_node->one_prop_fun->oneproprotlpfsetciop     = CartMMRotLPFSetCIOP;
    head_node->one_prop_fun->onepropsetsrc            = CartMMSetSRC;
    head_node->one_prop_fun->onepropsetsoc            = CartMMSetSOC;
    head_node->one_prop_fun->onepropwrite             = CartMMWrite;
    head_node->one_prop_fun->onepropgetcontrintegral  = CartMMGetContrIntegral;
    head_node->one_prop_fun->onepropgetcontrintegrand = CartMMGetContrIntegrand;
#if defined(GEN1INT_AO_SHELL)
    head_node->one_prop_fun->onepropgetshellintegral  = CartMMGetShellIntegral;
    head_node->one_prop_fun->onepropgetshellintegrand = CartMMGetShellIntegrand;
#endif
    head_node->one_prop_fun->onepropduplicate         = CartMMDuplicate;
    head_node->one_prop_fun->onepropdestroy           = CartMMDestroy;
    /* initializes the context of the Cartesian multipole moments */
    head_node->assembled = GFALSE;
    head_node->data_type = GREALPROP;
/*FIXME: is this correct: symmetric -- even order of electronic derivatives,
  anti-symmetric -- odd order of electronic derivatives */
    head_node->sym_type = ((order_elec_deriv&1)==0) ? GSYMPROP : GANTISYMPROP;
    ierr = OnePropIdxCreate(head_node);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxCreate");
    ierr = OnePropIdxSetGeoOper(head_node, order_geo_diporg, 0);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetGeoOper");
    ierr = OnePropIdxSetCartMM(head_node, order_cart_mm);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetCartMM");
    ierr = OnePropIdxSetElecDeriv(head_node, order_elec_deriv);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetElecDeriv");
    head_node->next_node = NULL;
    /* returns this property */
    *one_prop = head_node;
    return GSUCCESS;
}
