/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMMagLPFSetGIOP().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief uses gauge invariant opertor for magnetic derivatives of the Cartesian
        multipole moments, or transforms the operator by the London atomic
        orbital type gauge-including projector
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMMagLPFSetGIOP(OneProp *one_prop)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    /* the Cartesian multipole moments in general commutes with the
       position operator r, unless there are electronic derivatives;
       in the latter, we need to use the gauge invariant opertor
       (inspired by J. Chem. Phys. 136, 114110) */
    if (cart_mm->order_elec_deriv!=0) {
        if (cart_mm->mag_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "NULL cart_mm->mag_LPF");
        }
        MagLPFSetGIOP(cart_mm->mag_LPF);
    }
    return GSUCCESS;
}
