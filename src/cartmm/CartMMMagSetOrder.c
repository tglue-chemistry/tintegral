/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMMagSetOrder().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief sets the orders of magnetic derivatives for Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \param[GInt:int]{in} order_mag order of the magnetic derivatives
    \param[GInt:int]{in} order_mag_bra order of partial magnetic derivatives
        on the bra center
    \param[GInt:int]{in} order_mag_ket order of partial magnetic derivatives
        on the ket center
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMMagSetOrder(OneProp *one_prop,
                             const GInt order_mag,
                             const GInt order_mag_bra,
                             const GInt order_mag_ket)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    GErrorCode ierr;  /* error information */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    cart_mm->order_mag = order_mag;
    cart_mm->order_mag_bra = order_mag_bra;
    cart_mm->order_mag_ket = order_mag_ket;
    /* also sets the sizes of indices in the abstract one-electron property */
    ierr = OnePropIdxSetMagDeriv(*one_prop, order_mag, order_mag_bra, order_mag_ket);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetMagDeriv");
    return GSUCCESS;
}
