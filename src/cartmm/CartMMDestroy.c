/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMDestroy().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"
#include "utilities/gen1int_sort.h"

/*% \brief destroys the context of Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMDestroy(OneProp *one_prop)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    /* gets the context of the Cartesian multipole moments */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm!=NULL) {
        /* destroys the context of the Cartesian multipole moments */
        cart_mm->order_mag = 0;
        cart_mm->order_mag_bra = 0;
        cart_mm->order_mag_ket = 0;
        /* no need to destroy the information of cart_mm->mag_LPF,
           see the file derivatives/gen1int_magnetic.h */
        if (cart_mm->mag_LPF!=NULL) {
            free(cart_mm->mag_LPF);
            cart_mm->mag_LPF = NULL;
        }
        cart_mm->order_rot = 0;
        cart_mm->order_rot_bra = 0;
        cart_mm->order_rot_ket = 0;
        /* no need to destroy the information of cart_mm->rot_LPF,
           see the file derivatives/gen1int_rotational.h */
        if (cart_mm->rot_LPF!=NULL) {
            free(cart_mm->rot_LPF);
            cart_mm->rot_LPF = NULL;
        }
        cart_mm->RC_type = NR_CORRECTION;
        cart_mm->DCD_type = DIFF_CD_ZERO;
        cart_mm->scal_const = 0;
        cart_mm->idx_diporg = GMAX_IDX_NAT;
        cart_mm->coord_diporg[0] = 0;
        cart_mm->coord_diporg[1] = 0;
        cart_mm->coord_diporg[2] = 0;
        cart_mm->order_geo_diporg = 0;
        cart_mm->order_cart_mm = 0;
        cart_mm->order_elec_deriv = 0;
        free((*one_prop)->data);
        (*one_prop)->data = NULL;
    }
    return GSUCCESS;
}
