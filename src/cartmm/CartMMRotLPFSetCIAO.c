/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CartMMRotLPFSetCIAO().

   2014-06-29, Bin Gao:
   * first version
*/

#include "impls/cartmm_impl.h"

/*% \brief uses center of mass including atomic orbital (CIAO) for total rotational
        angular momentum derivatives of the Cartesian multipole moments
    \author Bin Gao
    \date 2014-06-29
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode CartMMRotLPFSetCIAO(OneProp *one_prop)
{
    CartMM *cart_mm;  /* context of Cartesian multipole moments */
    cart_mm = (CartMM *)((*one_prop)->data);
    if (cart_mm==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm");
    }
    if (cart_mm->rot_LPF==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL cart_mm->rot_LPF");
    }
    RotLPFSetCIAO(cart_mm->rot_LPF);
    return GSUCCESS;
}
