/* tIntegral: not only an integral computation library
   Copyright 2018-2022 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines C interface to tIntegral.

   2022-04-01, Bin Gao:
   * first version
*/

#include "tIntegral/MathFunction.hpp"
#include "tIntegralWrapper.h"

extern "C" {

void get_boys_c(const int maxOrder, const int numArgs, const double* args, double* vals)
{
    tIntegral::get_boys(maxOrder, numArgs, args, vals);
}

}
