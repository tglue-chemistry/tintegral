/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the one-electron integrator class.

   2019-04-10, Bin Gao:
   * first version
*/

#include "tIntegral/OneElec/OneElecIntegrator.hpp"

namespace tIntegral
{
    IntegratorState OneElecIntegrator::get_integral(const std::shared_ptr<OneElecOper>& oneOper,
                                                    std::vector<tMatrix::BlockMat>& intOper) noexcept
    {
        if (!m_molecule) return IntegratorState::EmptyMolecule;
        if (!m_bra_basis || !m_ket_basis) return IntegratorState:EmptyBasis;
        /* This function `get_integral()` takes care of loops over
           basis functions, so that parallelization can be considered here. */
        if (m_column_major) {
            auto second_basis = m_ket_basis.cbegin();
            auto second_basis_end = m_ket_basis.cend();
            auto first_basis = m_bra_basis.cbegin();
            auto first_basis_end = m_bra_basis.cend();
        }
        else {
            auto second_basis = m_bra_basis.cbegin();
            auto second_basis_end = m_bra_basis.cend();
            auto first_basis = m_ket_basis.cbegin();
            auto first_basis_end = m_ket_basis.cend();
        }
        for (; second_basis!=second_basis_end; ++second_basis) {
            for (; first_basis!=first_basis_end; ++first_basis) {
                auto engine = m_integration_engine.find(std::make_pair(first_basis->type_id(), second_basis->type_id()));
                if (engine==m_integration_engine.end()) {
                    return IntegratorState::UnsetIntegration;
                }
                else {
                    /* Once an integration instance is found for the
                       pair of basis functions on bra and ket, arguments:

                       * one-electron operator (containing derivatives),
                       * basis functions on bra and ket, and
                       * a vector containing computed integrals,

                       will be sent to the integration instance for evaluation.
                       It implies that the details of derivatives will be
                       handled inside an integration instance, but not here. */
                    std::vector<tReal> integrals;
                    auto state_integration = engine->integrate(oneOper,
                                                               first_basis,
                                                               second_basis,
                                                               m_molecule,
                                                               integrals);
                    if (state_integration==IntegratorState::Succeeded) {
                        /* After the integration instance returns non-zero
                           integrals, the integrator will call a member
                           function of the `Symbol` class to compute the
                           positions of these integrals, and finally add these
                           integrals into appropriate matrices. */
                        if (!integrals.empty()) {
                        }
                    }
                    else {
                        return state_integration;
                    }
                }
            }
        }
        return IntegratorState::Succeeded;
    }

    IntegratorState OneElecIntegrator::get_expectation(const std::shared_ptr<OneElecOper>& oneOper,
                                                       const std::vector<tMatrix::BlockMat>& states,
                                                       std::vector<tReal>& expOper) noexcept
    {
        if (!m_molecule) return IntegratorState::EmptyMolecule;
        if (!m_bra_basis || !m_ket_basis) return IntegratorState:EmptyBasis;
        /* Similar to the function `get_integral()`, parallelization
           can be considered in this function `get_expectation()` when loops
           over basis functions. */
        if (m_column_major) {
            auto second_basis = m_ket_basis.cbegin();
            auto second_basis_end = m_ket_basis.cend();
            auto first_basis = m_bra_basis.cbegin();
            auto first_basis_end = m_bra_basis.cend();
        }
        else {
            auto second_basis = m_bra_basis.cbegin();
            auto second_basis_end = m_bra_basis.cend();
            auto first_basis = m_ket_basis.cbegin();
            auto first_basis_end = m_ket_basis.cend();
        }
        for (; second_basis!=second_basis_end; ++second_basis) {
            for (; first_basis!=first_basis_end; ++first_basis) {
                auto engine = m_integration_engine.find(std::make_pair(first_basis->type_id(), second_basis->type_id()));
                if (engine==m_integration_engine.end()) {
                    return IntegratorState::UnsetIntegration;
                }
                else {
                    /* Once an integration instance is found for the
                       pair of basis functions on bra and ket, integrals will
                       be first computed following the same procedure of the
                       function `get_integral()`. */
                    std::vector<tReal> integrals;
                    auto state_integration = engine->integrate(oneOper,
                                                               first_basis,
                                                               second_basis,
                                                               m_molecule,
                                                               integrals);
                    if (state_integration==IntegratorState::Succeeded) {
                        /* For non-zero integrals, the integrator will
                           compute expectation values as the trace of product
                           of integrals and `states` as:

                           stem:[\mathrm{tr}(AB)=\sum_{i}\sum_{j}A_{ij}B_{ji}].

                           Finally, the integrator will call the member
                           function of the `Symbol` class to compute the
                           positions of expectation values, and add them into
                           appropriate places. */
                        if (!integrals.empty()) {
                        }
                    }
                    else {
                        return state_integration;
                    }
                }
            }
        }
        return IntegratorState::Succeeded;
    }
}
