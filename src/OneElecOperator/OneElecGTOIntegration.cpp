/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the one-electron integration with contracted Gaussian
   type orbitals.

   2019-04-10, Bin Gao:
   * first version
*/

#include "tIntegral/OneElec/OneElecGTOIntegration.hpp"

namespace tIntegral
{
    IntegratorState
    OneElecGTOIntegration::expand_derivative(const std::shared_ptr<OneElecOper>& oneOper,
                                             const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                                             const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                                             const std::shared_ptr<Symbol>& variable,
                                             const unsigned int order,
                                             bool& nonZero,
                                             derivative_type& derivatives) noexcept
    {
        /* Check whether the derivative is supported and non-zero */
        std::vector<bool> nnz_orders;
        std::vector<OneElecIdxName> idx_names;
        switch (variable->type_id()) {
            case typeid(tSymbolic::NuclearPosition):
                nnz_orders.push_back(braBasis->has_dependence(variable));
                nnz_orders.push_back(ketBasis->has_dependence(variable));
                nnz_orders.push_back(oneOper->has_dependence(variable));
                m_is_geometrical = nnz_orders;
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraGeometrical,
                    OneElecIdxName::KetGeometrical,
                    OneElecIdxName::OperGeometrical,
                });
                break;
            /* For derivatives with respect to magnetic field or total
               rotational angular momentum, auxiliary multipole moments around
               the origin of London phase factor are needed */
            case typeid(tSymbolic::MagneticField):
                nnz_orders.resize(6, false);
                nnz_orders[1] = braBasis->has_dependence(variable);
                nnz_orders[3] = ketBasis->has_dependence(variable);
                nnz_orders[5] = oneOper->has_dependence(variable);
                m_is_magnetic[0] = nnz_orders[1];
                m_is_magnetic[1] = nnz_orders[3];
                m_is_magnetic[2] = nnz_orders[5];
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraMagnetic,
                    OneElecIdxName::KetMagnetic,
                    OneElecIdxName::OperMagnetic,
                });
                break;
            case typeid(tSymbolic::TotalRotMomentum):
                nnz_orders.resize(6, false);
                nnz_orders[1] = braBasis->has_dependence(variable);
                nnz_orders[3] = ketBasis->has_dependence(variable);
                nnz_orders[5] = oneOper->has_dependence(variable);
                m_is_rotational[0] = nnz_orders[1];
                m_is_rotational[1] = nnz_orders[3];
                m_is_rotational[2] = nnz_orders[5];
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraRotational,
                    OneElecIdxName::KetRotational,
                    OneElecIdxName::OperRotational
                });
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Variable of ",
                                        variable->type_id()
                                        " is not supported by OneElecGTOIntegration::expand_derivative()");
                return IntegratorState::InvalidPhysicsVariable;
        }
        nonZero = std::any_of(nnz_orders.cbegin(),
                              nnz_orders.cend(),
                              [](bool is_non_zero){ return is_non_zero; });
        /* Expand the derivative on the operator and basis functions */
        if (nonZero) {
            derivatives = tCombinatorics::get_multinomial_expansion(order, nnz_orders);
            m_idx_names.insert(m_idx_names.end(), idx_names.begin(), idx_names.end());
        }
        return IntegratorState::Succeeded;
    }

    bool OneElecGTOIntegration::dispatch(std::shared_ptr<ContractedGTO> basis) noexcept
    {
        if (m_dispatch_bra) {
            m_basis_function[0] = basis;
        }
        else {
            m_basis_function[1] = basis;
        }
        return true;
    }

    IntegratorState OneElecGTOIntegration::reset(const std::shared_ptr<OneElecOper> oneOper) noexcept
    {
        m_idx_names = std::vector<IndexName>({OneElecIdxName::BraOrbital, OneElecIdxName::KetOrbital});
        m_idx_sizes.clear();
        m_is_rotational.fill(false);
        m_is_magnetic.fill(false);
        m_is_geometrical.fill(false);
        m_target_orders.clear();
        if (!m_recur_indices.set_operator(oneOper)) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::reset()");
            return IntegratorState::InvalidRecurIndex;
        }
        m_recur_relations.clear();
        return IntegratorState::Succeeded;
    }

    IntegratorState
    OneElecGTOIntegration::integrate(const std::shared_ptr<OneElecOper> oneOper,
                                     const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                                     const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                                     const std::shared_ptr<tMolelcular::Cluster>& molecule,
                                     std::vector<tReal>& intOper) noexcept
    {
        auto status_reset = reset(oneOper);
        if (status_reset!=IntegratorState::Succeeded) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::integrate()");
            return IntegratorState::InvalidRecurIndex;
        }
        /* Get derivatives expanded on the operator and basis functions */
        bool non_zero;
        std::vector<std::pair<std::shared_ptr<tSymbolic::Symbol>,derivative_type> expanded_derivatives;
        auto status_deriv = get_derivatives(oneOper,
                                            braBasis,
                                            ketBasis,
                                            non_zero,
                                            expanded_derivatives);
        if (status_deriv!=IntegratorState::Succeeded) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::integrate()");
            return status_deriv;
        }
        if (non_zero) {
            /* Check if we need the multipole moments around the origin of
               London phase factor */
            std::vector<unsigned int> london_orders;
            if (m_is_rotational[0] || m_is_magnetic[0]) {
                london_orders.push_back(0);
                m_idx_names.push_back(OneElecIdxName::BraLondon);
            }
            if (m_is_rotational[1] || m_is_magnetic[1]) {
                london_orders.push_back(0);
                m_idx_names.push_back(OneElecIdxName::KetLondon);
            }
            if (m_is_rotational[2] || m_is_magnetic[2]) {
                london_orders.push_back(0);
                m_idx_names.push_back(OneElecIdxName::OperLondon);
            }
            /* Dispatch basis functions on bra and ket centers */
            m_dispatch_bra = true;
            if (!braBasis->accept(this)) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Called by OneElecGTOIntegration::integrate()");
                return IntegratorState::InvalidBasisFunction;
            }
            m_dispatch_bra = false;
            if (!ketBasis->accept(this)) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Called by OneElecGTOIntegration::integrate()");
                return IntegratorState::InvalidBasisFunction;
            }
            /* Compute sizes of contractions and primitives */
            auto = m_basis_function[0]->get_num_contractions();
            sizeBuffer = std::accumulate(.cbegin(), .cend(), 0);
            m_size_contractions;
            m_size_primitives;

            /* Get angular momentum numbers of basis functions, and set indices
               of basis functions */
            std::array<std::vector<unsigned int>,2> angular_momentum;
            for (unsigned int ibas=0; ibas<2; ++ibas) {
                angular_momentum[ibas] = m_basis_function[ibas]->get_angular_momentum();
                IntegratorState status_idx;
                if (m_basis_function[ibas]->is_spherical()) {
                    status_idx = m_recur_indices.set_idx_sizes(m_idx_names[ibas], size_spherical_idx);
                }
                else {
                    status_idx = m_recur_indices.set_idx_sizes(m_idx_names[ibas], size_vector_idx);
                }
                if (status_idx!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the basis function ",
                                            ibas);
                    return status_idx;
                }
            }
            /* Get indices and orders of the operator, excluding orders of derivatives */
            std::vector<OneElecIdxName> oper_idx_names;
            std::vector<std::vector<unsigned int>> oper_idx_orders;
            m_recur_indices.get_oper_indices(oper_idx_names, oper_idx_orders);
            m_idx_names.insert(m_idx_names.end(), oper_idx_names.begin(), oper_idx_names.end());
            /* Get positions and size functions of all indices */
            std::vector<IndexPosition> idx_positions;
            auto status_idx = m_recur_indices.get_indices(m_idx_names, idx_positions, m_idx_sizes);
            if (status_idx!=IntegratorState::Succeeded) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Called by OneElecGTOIntegration::integrate()");
                return status_idx;
            }
            /* How many expanded derivatives and target orders */
            std::size_t num_all_derivatives = 1;
            std::vector<unsigned int> num_expanded_derivatives;
            for (auto ideriv=expanded_derivatives.cbegin(); ideriv!=expanded_derivatives.cend(); ++ideriv) {
                num_all_derivatives *= ideriv->second.size();
                num_expanded_derivatives.push_back(ideriv->second.size());
            }
            auto num_target_orders = angular_momentum[0].size()
                                   * angular_momentum[1].size()
                                   * num_all_derivatives
                                   * oper_idx_orders.size();
#if defined(TINTEGRAL_DEBUG)
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "Number of all derivatives ",
                                    num_all_derivatives,
                                    ", and number of target orders ",
                                    num_target_orders);
#endif
            /* Set target orders */
            m_target_orders.resize(num_target_orders);
            auto iter_target = m_target_orders.begin();
            std::vector<unsigned int> iter_expand;
            iter_expand.resize(expanded_derivatives.size(), 0);
            for (auto ideriv=0; ideriv<num_all_derivatives; ++ideriv) {
                for (auto ioper=oper_idx_orders.cbegin(); ioper!=oper_idx_orders.cend(); ++oper_idx_orders) {
                    for (auto iket=angular_momentum[1].cbegin(); iket!=angular_momentum[1].cend(); ++iket) {
                        for (auto ibra=angular_momentum[0].cbegin(); ibra!=angular_momentum[0].cend(); ++ibra) {
                            /* Target orders are arranged as angular momentums
                               of GTOs, multipole moments around the origin of
                               London phase factor, orders of expanded
                               derivatives and the operator */
                            std::vector<unsigned int> idx_orders;
                            idx_orders.push_back(*ibra);
                            idx_orders.push_back(*iket);
                            idx_orders.insert(idx_orders.end(), ioper.cbegin(), ioper.cend());
                            idx_orders.insert(idx_orders.end(), london_orders.cbegin(), london_orders.cend());
                            for (auto ivar=0; ivar<expanded_derivatives.size(); ++ivar) {
                                idx_orders.insert(idx_orders.end(),
                                                  expanded_derivatives[ivar].second[iter_expand[ivar]].second.cbegin(),
                                                  expanded_derivatives[ivar].second[iter_expand[ivar]].second.cend());
                            }
                            ++iter_target;
                            if (!iter_target->template assign(m_recur_indices.get_num_indices(),
                                                              idx_orders.size(),
                                                              idx_positions.cbegin(),
                                                              idx_orders.cbegin())) {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "Called by OneElecGTOIntegration::integrate() for target order ",
                                                        std::distance(m_target_orders.begin(), iter_target));
                                return IntegratorState::InvalidRecurIndex;
                            }
                        }
                    }
                    /* Increment of the multi-dimensional array of expanded derivatives */
                    for (auto ivar=0; ivar<expanded_derivatives.size(); ++ivar) {
                        ++iter_expand[ivar];
                        if (iter_expand[ivar]<num_expanded_derivatives[ivar]) {
                            break;
                        }
                        else {
                            iter_expand[ivar] = 0;
                        }
                    }
                }
            }

            /* Top-down procedure */

            /* Recurrence relations of derivatives with respect to total
               rotational angular momentum */
            if (m_is_rotational[2]) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Derivatives with respect to total rotational angular momentum on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                return IntegratorState::NotImplemented;
            }
            if (m_is_rotational[1]) {
                auto status_top_down = rotational_top_down(OneElecIdxName::KetRotational);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the ket center");
                    return status_top_down;
                }
            }
            if (m_is_rotational[0]) {
                auto status_top_down = rotational_top_down(OneElecIdxName::BraRotational);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the bra center");
                    return status_top_down;
                }
            }
            /* Recurrence relations of magnetic derivatives */
            if (m_is_magnetic[2]) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Derivatives with respect to magnetic field on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                return IntegratorState::NotImplemented;
            }
            if (m_is_magnetic[1]) {
                auto status_top_down = magnetic_top_down(OneElecIdxName::KetMagnetic);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the ket center");
                    return status_top_down;
                }
            }
            if (m_is_magnetic[0]) {
                auto status_top_down = magnetic_top_down(OneElecIdxName::BraMagnetic);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the bra center");
                    return status_top_down;
                }
            }

            /* Transfer order of geometrical derivatives to Hermite GTOs */
            if (m_basis_function[1]->is_spherical() && m_is_geometrical[1]) {

            }
            if (m_basis_function[0]->is_spherical() && m_is_geometrical[0]) {

            }

            /* Recurrence relations of multipole moments around the origin of London
               phase factor, either Cartesian GTOs or Hermite GTOs */
            if (m_is_rotational[2] || m_is_magnetic[2]) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Multipole moments around the origin of London phase factor on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                return IntegratorState::NotImplemented;
            }
            if (m_is_rotational[1] || m_is_magnetic[1]) {
                auto status_top_down = london_top_down(OneElecIdxName::KetMagnetic,
                                                       m_basis_function[1]->is_spherical());
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the ket center");
                    return status_top_down;
                }
            }
            if (m_is_rotational[0] || m_is_magnetic[0]) {
                auto status_top_down = london_top_down(OneElecIdxName::BraMagnetic,
                                                       m_basis_function[0]->is_spherical());
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the bra center");
                    return status_top_down;
                }
            }

            /* Recurrence relations of Cartesian GTOs to Hermite GTOs */
            if (!m_basis_function[1]->is_spherical()) {
                auto status_top_down = cgto_top_down(OneElecIdxName::KetOrbital);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the ket center");
                    return status_top_down;
                }
            }
            if (!m_basis_function[0]->is_spherical()) {
                auto status_top_down = cgto_top_down(OneElecIdxName::BraOrbital);
                if (status_top_down!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecGTOIntegration::integrate() for the bra center");
                    return status_top_down;
                }
            }
//FIXME: allocate buffer after all top-down procedures
            if (!m_buffer) auto is_allocated = allocate();

            /* Recurrence relations of operator with Hermite GTOs, and get
               primitive HGTO integrals following the bottom-up procedure */
            if (oneOper->accept(this)) {
                /* Get contracted Cartesian GTOs */
                for (auto ibas=0; ibas<=1; ++ibas) {
                    if (!m_basis_function[ibas]->is_spherical()) {
                        auto status_cgto = hgto_to_cgto();
                        if (status_cgto==IntegratorState::Succeeded) {
                            m_recur_relations.pop_back();
                        }
                        else {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "get primitive Cartesian GTOs on the center ",
                                                    ibas,
                                                    ", called by OneElecGTOIntegration::integrate()");
                            return status_cgto;
                        }
                        auto status_contr = make_contraction();
                        if (status_contr!=IntegratorState::Succeeded) {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "get contracted Cartesian GTOs on the center ",
                                                    ibas,
                                                    ", called by OneElecGTOIntegration::integrate()");
                            return status_contr;
                        }
                    }
                }
                /* Get multipole moments around the origin of London phase
                   factor, either Cartesian GTOs or Hermite GTOs */
                for (auto irecur=0; irecur<=1; ++irecur) {
                    if (m_is_rotational[irecur] || m_is_magnetic[irecur]) {
                        if (m_basis_function[irecur]->is_spherical()) {
                            auto status_london = hgto_to_london();
                            if (status_london==IntegratorState::Succeeded) {
                                m_recur_relations.pop_back();
                            }
                            else {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "get multipole moments around the origin of London phase factor on the center ",
                                                        ibas,
                                                        " for Hermite GTOs, called by OneElecGTOIntegration::integrate()");
                                return status_london;
                            }
                            /* Get geometrical derivatives from Hermite GTOs */

                            /* Make contractions */
                            auto status_contr = make_contraction();
                            if (status_contr!=IntegratorState::Succeeded) {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "get contracted spherical GTOs on the center ",
                                                        irecur,
                                                        ", called by OneElecGTOIntegration::integrate()");
                                return status_contr;
                            }
                        }
                        else {
                            auto status_london = cgto_to_london();
                            if (status_london==IntegratorState::Succeeded) {
                                m_recur_relations.pop_back();
                            }
                            else {
                                Settings::logger->write(tGlueCore::MessageType::Error,
                                                        "get multipole moments around the origin of London phase factor on the center ",
                                                        ibas,
                                                        " for Cartesian GTOs, called by OneElecGTOIntegration::integrate()");
                                return status_london;
                            }
                        }
                    }
                }
                if (m_is_rotational[2] || m_is_magnetic[2]) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Multipole moments around the origin of London phase factor on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                    return IntegratorState::NotImplemented;
                }
                /* Get magnetic derivatives */
                for (auto irecur=0; irecur<=1; ++irecur) {
                    if (m_is_magnetic[irecur]) {
                        auto status_magnetic = magnetic_bottom_up();
                        if (status_magnetic==IntegratorState::Succeeded) {
                            m_recur_relations.pop_back();
                        }
                        else {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "get magnetic derivatives on the center ",
                                                    irecur,
                                                    ", called by OneElecGTOIntegration::integrate()");
                            return status_magnetic;
                        }
                    }
                }
                if (m_is_magnetic[2]) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Derivatives with respect to magnetic field on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                    return IntegratorState::NotImplemented;
                }
                /* Get derivatives with respect to total rotational angular momentum */
                for (auto irecur=0; irecur<=1; ++irecur) {
                    if (m_is_rotational[irecur]) {
                        auto status_rotational = rotational_bottom_up();
                        if (status_rotational==IntegratorState::Succeeded) {
                            m_recur_relations.pop_back();
                        }
                        else {
                            Settings::logger->write(tGlueCore::MessageType::Error,
                                                    "get derivatives with respect to total rotational angular momentum on the center ",
                                                    irecur,
                                                    ", called by OneElecGTOIntegration::integrate()");
                            return status_rotational;
                        }
                    }
                }
                if (m_is_rotational[2]) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Derivatives with respect to total rotational angular momentum on the operator center(s) are not implemented in OneElecGTOIntegration::integrate()");
                    return IntegratorState::NotImplemented;
                }
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Called by OneElecGTOIntegration::integrate()");
                return IntegratorState::HGTOIntegrationError;
            }
        }
        else {
intOper = 0;
        }
        return IntegratorState::Succeeded;
    }

    IntegratorState OneElecGTOIntegration::rotational_top_down(const OneElecIdxName idxRotational) noexcept
    {
        /* Indices involved in the recurrence relation */
        std::vector<OneElecIdxName> idx_names;
        switch (idxRotational) {
            case OneElecIdxName::BraRotational:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraLondon,
                    OneElecIdxName::BraGeometrical,
                    OneElecIdxName::BraRotational
                });
                break;
            case OneElecIdxName::KetRotational:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::KetLondon,
                    OneElecIdxName::KetGeometrical,
                    OneElecIdxName::KetRotational
                });
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Invalid index name ",
                                        idxRotational,
                                        " for OneElecGTOIntegration::rotational_top_down()");
                return IntegratorState::InvalidRecurIndex;
        }
        /* Get positions of indices in the recurrence relation */
        std::vector<IndexPosition> idx_positions;
        auto status_idx = m_recur_indices.get_idx_positions(idx_names, idx_positions);
        if (status_idx==IntegratorState::Succeeded) {
            /* Perform the recurrence relation in the top-down procedure */
            unsigned int output_idx = 2;
            std::vector<std::vector<int>> rhs_increments = {{1,0,-1}, {1,0,-1}, {1,-1,-1}, {1,-1,-1}};
            RecurArray recur_rotational;
            IntegratorState status_recur;
            std::size_t size_buffer;
            if (m_recur_relations.empty()) {
                status_recur = recur_rotational.assign(m_target_orders,
                                                       m_idx_sizes,
                                                       idx_positions,
                                                       output_idx,
                                                       rhs_increments,
                                                       m_size_contractions,
                                                       size_buffer);
            }
            else {
                status_recur = recur_rotational.assign(m_recur_relations.back(),
                                                       m_idx_sizes,
                                                       idx_positions,
                                                       output_idx,
                                                       rhs_increments,
                                                       m_size_contractions,
                                                       size_buffer);
            }
            if (status_recur==IntegratorState::Succeeded) {
                if (size_buffer>m_size_buffer) m_size_buffer = size_buffer;
                m_recur_relations.push_back(std::move(recur_rotational));
                return IntegratorState::Succeeded;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Derivatives with respect to total rotational angular momentum on ",
                                        idxRotational,
                                        ", called by OneElecGTOIntegration::rotational_top_down()");
                return status_recur;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Derivatives with respect to total rotational angular momentum on ",
                                    idxRotational,
                                    ", called by OneElecGTOIntegration::rotational_top_down()");
            return status_idx;
        }
    }

    IntegratorState OneElecGTOIntegration::magnetic_top_down(const OneElecIdxName idxMagnetic) noexcept
    {
        /* Indices involved in the recurrence relation */
        std::vector<OneElecIdxName> idx_names;
        switch (idxMagnetic) {
            case OneElecIdxName::BraMagnetic:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraLondon,
                    OneElecIdxName::BraGeometrical,
                    OneElecIdxName::BraMagnetic
                });
                break;
            case OneElecIdxName::KetMagnetic:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::KetLondon,
                    OneElecIdxName::KetGeometrical,
                    OneElecIdxName::KetMagnetic
                });
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Invalid index name ",
                                        idxMagnetic,
                                        " for OneElecGTOIntegration::magnetic_top_down()");
                return IntegratorState::InvalidRecurIndex;
        }
        /* Get positions of indices in the recurrence relation */
        std::vector<IndexPosition> idx_positions;
        auto status_idx = m_recur_indices.get_idx_positions(idx_names, idx_positions);
        if (status_idx==IntegratorState::Succeeded) {
            /* Perform the recurrence relation in the top-down procedure */
            unsigned int output_idx = 2;
            std::vector<std::vector<int>> rhs_increments = {{1,0,-1}, {1,0,-1}, {1,-1,-1}, {1,-1,-1}};
            RecurArray recur_magnetic;
            IntegratorState status_recur;
            std::size_t size_buffer;
            if (m_recur_relations.empty()) {
                status_recur = recur_magnetic.assign(m_target_orders,
                                                     m_idx_sizes,
                                                     idx_positions,
                                                     output_idx,
                                                     rhs_increments,
                                                     m_size_contractions,
                                                     size_buffer);
            }
            else {
                status_recur = recur_magnetic.assign(m_recur_relations.back(),
                                                     m_idx_sizes,
                                                     idx_positions,
                                                     output_idx,
                                                     rhs_increments,
                                                     m_size_contractions,
                                                     size_buffer);
            }
            if (status_recur==IntegratorState::Succeeded) {
                if (size_buffer>m_size_buffer) m_size_buffer = size_buffer;
                m_recur_relations.push_back(std::move(recur_magnetic));
                return IntegratorState::Succeeded;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Magnetic derivatives on ",
                                        idxMagnetic,
                                        ", called by OneElecGTOIntegration::magnetic_top_down()");
                return status_recur;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Magnetic derivatives on ",
                                    idxMagnetic,
                                    ", called by OneElecGTOIntegration::magnetic_top_down()");
            return status_idx;
        }
    }

    IntegratorState OneElecGTOIntegration::london_top_down(const OneElecIdxName idxLondon,
                                                           const bool isSpherical) noexcept
    {
        /* Indices involved in the recurrence relation */
        std::vector<OneElecIdxName> idx_names;
        std::vector<std::vector<int>> rhs_increments;
        std::size_t size_basis;
        switch (idxLondon) {
            case OneElecIdxName::BraLondon:
                if (isSpherical) {
                    idx_names = std::vector<OneElecIdxName>({
                        OneElecIdxName::BraOrbital,
                        OneElecIdxName::BraLondon
                    });
                    rhs_increments = std::vector<std::vector<int>>({{0,-1}, {-1,-1}, {1,-1}});
                    size_basis = m_size_contractions;
                }
                else {
                    idx_names = std::vector<OneElecIdxName>({
                        OneElecIdxName::BraOrbital,
                        OneElecIdxName::BraLondon,
                        OneElecIdxName::BraGeometrical
                    });
                    rhs_increments = std::vector<std::vector<int>>({{0,-1,0}, {0,-1,-1}, {1,-1,0}});
                    size_basis = m_size_primitives;
                }
                break;
            case OneElecIdxName::KetLondon:
                if (isSpherical) {
                    idx_names = std::vector<OneElecIdxName>({
                        OneElecIdxName::KetOrbital,
                        OneElecIdxName::KetLondon
                    });
                    rhs_increments = std::vector<std::vector<int>>({{0,-1}, {-1,-1}, {1,-1}});
                    size_basis = m_size_contractions;
                }
                else {
                    idx_names = std::vector<OneElecIdxName>({
                        OneElecIdxName::KetOrbital,
                        OneElecIdxName::KetLondon,
                        OneElecIdxName::KetGeometrical
                    });
                    rhs_increments = std::vector<std::vector<int>>({{0,-1,0}, {0,-1,-1}, {1,-1,0}});
                    size_basis = m_size_primitives;
                }
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Invalid index name ",
                                        idxLondon,
                                        " for OneElecGTOIntegration::london_top_down()");
                return IntegratorState::InvalidRecurIndex;
        }
        /* Get positions of indices in the recurrence relation */
        std::vector<IndexPosition> idx_positions;
        auto status_idx = m_recur_indices.get_idx_positions(idx_names, idx_positions);
        if (status_idx==IntegratorState::Succeeded) {
            /* Perform the recurrence relation in the top-down procedure */
            unsigned int output_idx = 1;
            RecurArray recur_london;
            std::size_t size_buffer;
            auto status_recur = recur_london.assign(m_recur_relations.back(),
                                                    m_idx_sizes,
                                                    idx_positions,
                                                    output_idx,
                                                    rhs_increments,
                                                    size_basis,
                                                    size_buffer);
            if (status_recur==IntegratorState::Succeeded) {
                if (size_buffer>m_size_buffer) m_size_buffer = size_buffer;
                m_recur_relations.push_back(std::move(recur_london));
                return IntegratorState::Succeeded;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Multipole moments around the origin of London phase factor on ",
                                        idxLondon,
                                        ", called by OneElecGTOIntegration::london_top_down()");
                return status_recur;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Multipole moments around the origin of London phase factor on ",
                                    idxLondon,
                                    ", called by OneElecGTOIntegration::london_top_down()");
            return status_idx;
        }
    }

    IntegratorState OneElecGTOIntegration::cgto_top_down(const OneElecIdxName idxOrbital) noexcept
    {
        /* Indices involved in the recurrence relation */
        std::vector<OneElecIdxName> idx_names;
        switch (idxOrbital) {
            case OneElecIdxName::BraOrbital:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::BraOrbital,
                    OneElecIdxName::BraGeometrical
                });
                break;
            case OneElecIdxName::KetOrbital:
                idx_names = std::vector<OneElecIdxName>({
                    OneElecIdxName::KetOrbital,
                    OneElecIdxName::KetGeometrical
                });
                break;
            default:
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Invalid index name ",
                                        idxOrbital,
                                        " for OneElecGTOIntegration::cgto_top_down()");
                return IntegratorState::InvalidRecurIndex;
        }
        /* Get positions of indices in the recurrence relation */
        std::vector<IndexPosition> idx_positions;
        auto status_idx = m_recur_indices.get_idx_positions(idx_names, idx_positions);
        if (status_idx==IntegratorState::Succeeded) {
            /* Perform the recurrence relation in the top-down procedure */
            unsigned int output_idx = 0;
            std::vector<std::vector<int>> rhs_increments = {{-1,1}, {-2,0}};
            RecurArray recur_cgto;
            IntegratorState status_recur;
            std::size_t size_buffer;
            if (m_recur_relations.empty()) {
                status_recur = recur_cgto.assign(m_target_orders,
                                                 m_idx_sizes,
                                                 idx_positions,
                                                 output_idx,
                                                 rhs_increments,
                                                 m_size_primitives,
                                                 size_buffer);
            }
            else {
                status_recur = recur_cgto.assign(m_recur_relations.back(),
                                                 m_idx_sizes,
                                                 idx_positions,
                                                 output_idx,
                                                 rhs_increments,
                                                 m_size_primitives,
                                                 size_buffer);
            }
            if (status_recur==IntegratorState::Succeeded) {
                if (size_buffer>m_size_buffer) m_size_buffer = size_buffer;
                m_recur_relations.push_back(std::move(recur_cgto));
                return IntegratorState::Succeeded;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Cartesian GTOs to Hermite GTOs on ",
                                        idxOrbital,
                                        ", called by OneElecGTOIntegration::cgto_top_down()");
                return status_recur;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Cartesian GTOs to Hermite GTOs on ",
                                    idxOrbital,
                                    ", called by OneElecGTOIntegration::cgto_top_down()");
            return status_idx;
        }
    }

    bool OneElecGTOIntegration::dispatch(std::shared_ptr<CartMultMoment> oneOper) noexcept
    {
        /* Top-down procedure */
        CartMultMomentHGTOIntegration oper_hgto_integration(oneOper);
        auto status_top_down = oper_hgto_integration.top_down(m_recur_relations.back(), size_buffer);
        if (status_top_down==IntegratorState::Succeeded) {
            if (size_buffer>m_size_buffer) m_size_buffer = size_buffer;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for top-down procedure");
            return false;
        }
        /* Assemble buffer */
        RecurBuffer m_recur_buffer(size_buffer);
        m_recur_relations.assemble(m_recur_buffer);

        /* Bottom-up procedure */
        auto status_bottom_up = oper_hgto_integration.bottom_up(m_recur_buffer, m_basis_function);
        if (status_bottom_up==IntegratorState::Succeeded) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for bottom-up procedure");
            return false;
        }
    }

    bool OneElecGTOIntegration::dispatch(std::shared_ptr<ECPUnprojected> oneOper) noexcept
    {
        /* Top-down procedure */
        auto status_top_down = ECPUnprojectedTopDown(oneOper);
        if (status_top_down!=IntegratorState::Succeeded) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for top-down procedure");
            return false;
        }
        /* Assemble buffer */
        RecurBuffer recur_buffer(size_buffer);

        /* Bottom-up procedure */
        auto status_bottom_up = ECPUnprojectedBottomUp(oneOper);
        if (status_bottom_up==IntegratorState::Succeeded) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for bottom-up procedure");
            return false;
        }
    }

    bool OneElecGTOIntegration::dispatch(std::shared_ptr<ECPProjected> oneOper) noexcept
    {
        /* Top-down procedure */
        auto status_top_down = ECPProjectedTopDown(oneOper);
        if (status_top_down!=IntegratorState::Succeeded) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for top-down procedure");
            return false;
        }
        /* Assemble buffer */
        RecurBuffer recur_buffer(size_buffer);

        /* Bottom-up procedure */
        auto status_bottom_up = ECPProjectedBottomUp(oneOper);
        if (status_bottom_up==IntegratorState::Succeeded) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOIntegration::dispatch() for bottom-up procedure");
            return false;
        }
    }
}
