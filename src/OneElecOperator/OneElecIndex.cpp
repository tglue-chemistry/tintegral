/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the one-electron integration with contracted Gaussian
   type orbitals.

   2019-04-10, Bin Gao:
   * first version
*/

#include "tIntegral/OneElec/OneElecIndex.hpp"

namespace tIntegral
{
    bool OneElecIndex::set_operator(const std::shared_ptr<OneElecOper>& oneOper) noexcept
    {
        if (oneOper->accept(this)) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecIndex::set_operator()");
            return false;
        }
    }

    bool OneElecIndex::set_indices(const std::shared_ptr<tSymbolic::Chemistry::OneElecOper> oneOper) noexcept
    {
        m_oper_indices = oneOper->get_all_indices();
        switch (oneOper->type_id()) {
            case typeid(tSymbolic::Chemistry::CartMultMoment):
                return true;
            case typeid(tSymbolic::Chemistry::NucAttractPotential):
                return true;
            default:
                return false;
        }
    }

    IndexPosition OneElecIndex::get_position(const std::shared_ptr<tSymbolic::>& quantity) const noexcept
    {
        switch (quantity->type_id()) {
            case typeid(tSymbolic::Chemistry::
        }
    }

    bool OneElecIndex::dispatch(std::shared_ptr<CartMultMoment> oneOper) noexcept
    {
        m_oper_idx_names = std::vector<OneElecIdxName>({
            OneElecIdxName::ElectronicDerivative,
            OneElecIdxName::MultipoleMoment
        });
        m_oper_idx_sizes = std::vector<IdxSizeFunction>({size_vector_idx, size_vector_idx});
        m_oper_idx_orders = std::vector<std::vector<unsigned int>>({
            {oneOper->get_electronic_order(), oneOper->get_multipole_order()}
        });
        if (set_indices()) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecIndex::dispatch() for CartMultMoment");
            return false;
        }
    }

    bool OneElecIndex::dispatch(std::shared_ptr<NucAttractPotential> oneOper) noexcept
    {
        m_oper_idx_names = std::vector<OneElecIdxName>({
            OneElecIdxName::BoysFunction,
            OneElecIdxName::ElectronicDerivative,
            OneElecIdxName::MultipoleMoment
        });
        m_oper_idx_sizes = std::vector<IdxSizeFunction>({
            size_scalar_idx,
            size_vector_idx,
            size_vector_idx
        });
        m_oper_idx_orders = std::vector<std::vector<unsigned int>>({
            {0, oneOper->get_electronic_order(), oneOper->get_multipole_order()}
        });
        if (set_indices()) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecIndex::dispatch() for NucAttractPotential");
            return false;
        }
    }

    bool OneElecIndex::dispatch(std::shared_ptr<ECPProjected> oneOper) noexcept
    {
        m_oper_idx_names = std::vector<OneElecIdxName>({
            OneElecIdxName::ECPRadialPower,
            OneElecIdxName::ECPBesselBra,
            OneElecIdxName::ECPBesselKet,
            OneElecIdxName::ECPHarmonicBra,
            OneElecIdxName::ECPHarmonicKet
        });
        m_oper_idx_sizes = std::vector<IdxSizeFunction>({
           size_scalar_idx,
           size_scalar_idx,
           size_scalar_idx,
           size_vector_idx,
           size_vector_idx
        });
        m_oper_idx_orders = std::vector<std::vector<unsigned int>>({{0, 0, 0, 0, 0}});
        if (set_indices()) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecIndex::dispatch() for ECPProjected");
            return false;
        }
    }

    bool OneElecIndex::set_indices() noexcept
    {
        m_idx_names = std::vector<OneElecIdxName>({
            OneElecIdxName::BraOrbital,
            OneElecIdxName::KetOrbital
        });
        m_idx_sizes.assign(m_idx_names.size(), size_vector_idx);
        m_idx_names.insert(m_idx_names.end(), m_oper_idx_names.cbegin(), m_oper_idx_names.cend());
        m_idx_sizes.insert(m_idx_sizes.end(), m_oper_idx_sizes.cbegin(), m_oper_idx_sizes.cend());
        deriv_idx_names = std::vector<OneElecIdxName>({
            OneElecIdxName::BraLondon,
            OneElecIdxName::KetLondon,
            OneElecIdxName::OperLondon,
            OneElecIdxName::BraGeometrical,
            OneElecIdxName::KetGeometrical,
            OneElecIdxName::OperGeometrical,
            OneElecIdxName::BraMagnetic,
            OneElecIdxName::KetMagnetic,
            OneElecIdxName::OperMagnetic,
            OneElecIdxName::BraRotational,
            OneElecIdxName::KetRotational,
            OneElecIdxName::OperRotational
        });
        m_idx_names.insert(m_idx_names.end(), deriv_idx_names.cbegin(), deriv_idx_names.cend());
        m_idx_sizes.insert(m_idx_sizes.end(), deriv_idx_names.size(), size_vector_idx);
        return true;
    }

    IntegratorState OneElecIndex::set_idx_sizes(const std::vector<OneElecIdxName>& idxNames,
                                                const std::vector<IdxSizeFunction>& idxSizes) noexcept
    {
        auto idx_size = idxSizes.cbegin();
        for (auto idx_name=idxNames.cbegin(); idx_name!=idxNames.cend(); ++idx_name) {
            auto idx_found = std::find(m_idx_names.begin(), m_idx_names.end(), *idx_name);
            if (idx_found==m_idx_names.end()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Incorrect index name in OneElecIndex::set_idx_sizes() ",
                                        *idx_name,
                                        " at the position ",
                                        std::distance(idxNames.cbegin(), idx_found));
                return IntegratorState::InvalidIndexName;
            }
            else {
                m_idx_sizes[std::distance(m_idx_names.begin(), idx_found)] = *idx_size;
                ++idx_size;
            }
        }
        return IntegratorState::Succeeded;
    }

    IntegratorState OneElecIndex::get_indices(const std::vector<OneElecIdxName>& idxNames,
                                              std::vector<IndexPosition>& idxPositions,
                                              std::vector<IdxSizeFunction>& idxSizes) const noexcept
    {
        for (auto idx_name=idxNames.cbegin(); idx_name!=idxNames.cend(); ++idx_name) {
            auto idx_found = std::find(m_idx_names.begin(), m_idx_names.end(), *idx_name);
            if (idx_found==m_idx_names.end()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Incorrect index name in OneElecIndex::get_indices() ",
                                        *idx_name,
                                        " at the position ",
                                        std::distance(idxNames.cbegin(), idx_found));
                return IntegratorState::InvalidIndexName;
            }
            else {
                auto index = std::distance(m_idx_names.begin(), idx_found);
                idxPositions.push_back(IndexPosition(index));
                idxSizes.push_back(m_idx_sizes[index]);
            }
        }
        return IntegratorState::Succeeded;
    }

    IntegratorState OneElecIndex::get_idx_positions(const std::vector<OneElecIdxName>& idxNames,
                                                    std::vector<IndexPosition>& idxPositions) const noexcept
    {
        for (auto idx_name=idxNames.cbegin(); idx_name!=idxNames.cend(); ++idx_name) {
            auto idx_found = std::find(m_idx_names.begin(), m_idx_names.end(), *idx_name);
            if (idx_found==m_idx_names.end()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "Incorrect index name in OneElecIndex::get_idx_positions() ",
                                        *idx_name,
                                        " at the position ",
                                        std::distance(idxNames.cbegin(), idx_found));
                return IntegratorState::InvalidIndexName;
            }
            else {
                auto index = std::distance(m_idx_names.begin(), idx_found);
                idxPositions.push_back(IndexPosition(index));
            }
        }
        return IntegratorState::Succeeded;
    }

    void OneElecIndex::get_oper_indices(std::vector<OneElecIdxName>& operIdxNames,
                                        std::vector<std::vector<unsigned int>>& operIdxOrders) const noexcept
    {
        operIdxNames = m_oper_idx_names;
        operIdxOrders = m_oper_idx_orders;
    }
}
