/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the recurrence relation compiler for one-electron
   integration with contracted Gaussian type orbitals.

   2019-06-12, Bin Gao:
   * rewrite based on the developed RecurCompiler

   2018-05-07, Bin Gao:
   * first version
*/

#include "tIntegral/OneElec/OneElecGTOCompiler.hpp"

namespace tIntegral
{
    bool OneElecGTOCompiler::build(const std::shared_ptr<OneElecOper> oneOper,
                                   const std::string& hgtoIntegration,
                                   const bool onlyHGTO,
                                   std::shared_ptr<RecurConverterFactory> converterFactory) noexcept;
    {
        /* Set indices involved in the integration */
        if (m_recur_indices.set_operator(oneOper)) {
            /**/
            if (!onlyHGTO) {
                if (build_()) {
                }
                else {
                }
            }
            /* Assign recurrence relations of the operator with HGTOs */
            if (oneOper->accept(this)) {
                RecurCompiler recur_compiler(converterFactory);
                /* Assign recurrence relations of the operator with HGTOs to
                   the compiler */
                for (auto const& recur_relation: m_oper_relations) {
                    if (recur_compiler.assign(std::get<0>(recur_relation),
                                              std::get<1>(recur_relation),
                                              std::get<2>(recur_relation))) {
                        Settings::logger->write(tGlueCore::MessageType::Output,
                                                "OneElecGTOCompiler::build() assigned the recurrence relation ",
                                                std::get<2>(recur_relation),
                                                " with the output index ",
                                                std::get<1>(recur_relation),
                                                " and function name ",
                                                std::get<0>(recur_relation));
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "OneElecGTOCompiler::build() called when assigning the recurrence relation ",
                                                std::get<2>(recur_relation),
                                                " with the output index ",
                                                std::get<1>(recur_relation),
                                                " and function name ",
                                                std::get<0>(recur_relation));
                        return false;
                    }
                }
                /* Build codes of recurrence relaitons */
                if (recur_compiler.build(hgtoIntegration)) {
                    Settings::logger->write(tGlueCore::MessageType::Output,
                                            "OneElecGTOCompiler::build() built the integration ",
                                            hgtoIntegration,
                                            " for operator ",
                                            oneOper->type_id().name());
                    return true;
                }
                else {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "OneElecGTOCompiler::build() called when building the integration ",
                                            hgtoIntegration,
                                            " for operator ",
                                            oneOper->type_id().name());
                    return false;
                }
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "OneElecGTOCompiler::build() called when building operator ",
                                        oneOper->type_id().name());
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Called by OneElecGTOCompiler::build() for ",
                                    oneOper->type_id().name());
            return false;
        }
    }

    bool OneElecGTOCompiler::dispatch(std::shared_ptr<CartMultMoment> oneOper) noexcept
    {
        m_oper_relations.clear();
        auto idx_positions = m_recur_indices.get_idx_positions(hasDerivative=false);
                unsigned int idx_bra = 0;
                unsigned int idx_ket = 1;
                unsigned int idx_multipole = 2;
        /* Assign the recurrence relation of Cartesian multipole moments to the
           compiler */
        {
            /* Vector from the centre-of-charge to the origin of Cartesian
               multipole moments */
            auto centre_charge_origin = std::make_shared<RecurVectorConst>(
                std::string("centreChargeOrigin"),
                RecurDirection::X
            );
            auto first_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({-1,0,0}))
                })
            );
            /* Constant \frac{1}{2p} */
            auto arg_exponent = std::make_shared<RecurScalarConst>(std::string("argExponent"));
            /* Angular momentum numbers (or orders of HGTOs) on bra and ket centres */
            auto angular_bra = std::make_shared<RecurVectorOrder>(idxBra, RecurDirection::X);
            auto angular_ket = std::make_shared<RecurVectorOrder>(idxKet, RecurDirection::X);
            /* Order of Cartesian multipole moments */
            auto order_multipole = std::make_shared<RecurVectorOrder>(idxMultipole, RecurDirection::X);
            auto second_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({-1,0,0})),
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({-1,0,0}))
                })
            );
            auto third_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({-1,0,0})),
                    std::make_pair(true, RecurVector({-1,0,0}))
                })
            );
            auto fourth_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({0,0,0})),
                    std::make_pair(true, RecurVector({-2,0,0}))
                })
            );
            m_oper_relations.push_back(
                std::make_tuple(
                    std::string("multipole_moment"),
                    idx_multipole,
                    std::make_shared<RecurAddition>(
                        std::make_shared<RecurMultiplication>(centre_charge_origin, first_term),
                        std::make_shared<RecurMultiplication>(
                            arg_exponent,
                            std::make_shared<RecurParentheses>(
                                std::make_shared<RecurAddition>(
                                    std::make_shared<RecurMultiplication>(angular_bra, second_term),
                                    std::make_shared<RecurAddition>(
                                        std::make_shared<RecurMultiplication>(angular_ket, third_term),
                                        std::make_shared<RecurMultiplication>(order_multipole, fourth_term)
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
        /* Assign the recurrence relation of HGTO on ket centre to the compiler */
        {
            /* Constant -\frac{a}{b} */
            auto arg_exponent = std::make_shared<RecurScalarConst>(std::string("argExponent"));
            auto first_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({1,0,0})),
                    std::make_pair(true, RecurVector({-1,0,0}))
                })
            );
            m_oper_relations.push_back(
                std::make_tuple(std::string("hgto_ket"),
                                idx_ket,
                                std::make_shared<RecurMultiplication>(arg_exponent, first_term))
            );
        }
        /* Assign the recurrence relation of HGTO on bra centre to the compiler */
        {
            /* Vector from the centre-of-charge to bra centre */
            auto centre_charge_bra = std::make_shared<RecurVectorConst>(
                std::string("centreChargeBra"),
                RecurDirection::X
            );
            auto first_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({-1,0,0}))
                })
            );
            /* Angular momentum number (or order of HGTOs) on bra centre */
            auto angular_bra = std::make_shared<RecurVectorOrder>(idxBra, RecurDirection::X);
            /* Constant -\frac{1}{2p}\frac{b}{a} */
            auto arg_exponent = std::make_shared<RecurScalarConst>(std::string("argExponent"));
            auto second_term = std::make_shared<RecurTerm>(
                std::vector<std::pair<bool,RecurVector>>({
                    std::make_pair(true, RecurVector({-2,0,0}))
                })
            );
            m_oper_relations.push_back(
                std::make_tuple(
                    std::string("hgto_bra"),
                    idx_bra,
                    std::make_shared<RecurAddition>(
                        std::make_shared<RecurMultiplication>(centre_charge_bra, first_term),
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurMultiplication>(angular_bra, arg_exponent),
                            second_term
                        )
                    )
                )
            );
        }
        return true;
    }
}
