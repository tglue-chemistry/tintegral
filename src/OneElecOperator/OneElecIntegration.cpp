/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the one-electron integration class.

   2019-04-11, Bin Gao:
   * first version
*/

#include "tIntegral/OneElec/OneElecIntegration.hpp"

namespace tIntegral
{
    bool OneElecIntegration::compare_derivatives(const std::pair<std::shared_ptr<Symbol>,unsigned int>& lhs,
                                                 const std::pair<std::shared_ptr<Symbol>,unsigned int>& rhs) noexcept
    {
        /* For now, only geometrical derivatives, magnetic derivatives and
           derivatives w.r.t. total rotational angular momentum are considered */
        switch (lhs.first->type_id()) {
            case typeid(tSymbolic::NuclearPosition):
                return true;
            case typeid(tSymbolic::MagneticField):
                switch (rhs.first->type_id()) {
                    case typeid(tSymbolic::NuclearPosition):
                        return false;
                    default:
                        return true;
                }
            case typeid(tSymbolic::TotalRotMomentum):
                switch (rhs.first->type_id()) {
                    case typeid(tSymbolic::NuclearPosition):
                    case typeid(tSymbolic::MagneticField):
                        return false;
                    default:
                        return true;
                }
            default:
                switch (rhs.first->type_id()) {
                    case typeid(tSymbolic::NuclearPosition):
                    case typeid(tSymbolic::MagneticField):
                    case typeid(tSymbolic::TotalRotMomentum):
                        return false;
                    default:
                        return true;
                }
        }
    }

    IntegratorState
    OneElecIntegration::get_derivatives(const std::shared_ptr<OneElecOper>& oneOper,
                                        const std::shared_ptr<tBasisSet::BasisFunction>& braBasis,
                                        const std::shared_ptr<tBasisSet::BasisFunction>& ketBasis,
                                        bool& nonZero,
                                        std::vector<std::pair<std::shared_ptr<tSymbolic::Symbol>,derivative_type>& derivatives) noexcept
    {
        /* Get derivatives from symbolic differentiation */
        auto total_derivatives = oneOper->get_derivatives();
        if (total_derivatives.empty()) {
            nonZero = true;
        }
        else {
            /* Sort derivatives */
            std::sort(total_derivatives.begin(), total_derivatives.end(), compare_derivatives);
            /* Expand derivatives on the operator and basis functions */
            derivatives.resize(total_derivatives.size());
            for (auto ivar=0; ivar<total_derivatives.size(); ++ivar) {
                auto status_expansion = expand_derivative(oneOper,
                                                          braBasis,
                                                          ketBasis,
                                                          total_derivatives[ivar].first,
                                                          total_derivatives[ivar].second,
                                                          nonZero,
                                                          derivatives[ivar]);
                if (status_expansion!=IntegratorState::Succeeded) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "Called by OneElecIntegration::get_derivatives() for "
                                            ivar,
                                            "-th derivative");
                    return status_expansion;
                }
                if (!nonZero) break;
            }
        }
        return IntegratorState::Succeeded;
    }
}
