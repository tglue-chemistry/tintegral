/* tintegral: not only an integral computation library
   Copyright 2018 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of the London orbital and its properties.

   2018-07-27, Bin Gao:
   * first version
*/

#include "LondonOrbital.hpp"

namespace tIntegral
{
    RecurRelation LondonMomentIntegrator::top_down() noexcept
    {
        m_recur_relation.assign();
        m_gto_integrator.top_down(m_recur_relation);
    }

    nonstd::expected<RecurBuffer,std::string>
    LondonMomentIntegrator::bottom_up() noexcept
    {
        if (m_cgto) {
            /* Gets contracted CGTOs */
            m_gto_integrator.bottom_up();
            /* Recovers multipole moments around the origin of London phase factor */
        }
        else {
            /* Recovers multipole moments around the origin of London phase factor */

            /* Gets contracted SGTOs */
            m_gto_integrator.bottom_up();
        }
    }

    RecurRelation LondonDerivativeIntegrator::top_down(const RecurRelation& targetRelation) noexcept
    {
        m_recur_relation.assign();
        m_moment_integrator->top_down(m_recur_relation);
    }

    nonstd::expected<RecurBuffer,std::string>
    LondonMagneticIntegrator::bottom_up() noexcept
    {
        LondonMomentIntegrator m_moment_integrator;
        m_moment_integrator.bottom_up();
        /* London atomic orbital to magnetic derivatives */
    }
}
