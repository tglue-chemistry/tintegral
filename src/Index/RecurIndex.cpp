/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of indices involved in recurrence relations.

   2020-06-09, Bin Gao:
   * move implementation here

   2020-02-25, Bin Gao:
   * add derived index classes

   2019-09-30, Bin Gao:
   * first version
*/

#include <utility>

#include "tIntegral/Index/RecurIndex.hpp"

namespace tIntegral
{
    std::string RecurScalarIndex::to_string() const noexcept
    {
        return "{scalar-index: {name: "+get_name()
            +", position: "+get_index_position().to_string()
            +", order: "+std::to_string(get_order()[0])+"}}";
    }

    unsigned int RecurScalarIndex::get_num_components(const unsigned int order) const noexcept
    {
        return 1;
    }

    std::shared_ptr<RecurIndex> RecurScalarIndex::get_copy() noexcept
    {
        return std::make_shared<RecurScalarIndex>(std::move(*this));
    }

    std::string RecurVectorIndex::to_string() const noexcept
    {
        return "{vector-index: {name: "+get_name()
            +", position: "+get_index_position().to_string()
            +", order: "+std::to_string(get_order()[0])+"}}";
    }

    unsigned int RecurVectorIndex::get_num_components(const unsigned int order) const noexcept
    {
        return order>1 ? order : 1;
    }

    std::shared_ptr<RecurIndex> RecurVectorIndex::get_copy() noexcept
    {
        return std::make_shared<RecurVectorIndex>(std::move(*this));
    }

    std::string RecurTriangularIndex::to_string() const noexcept
    {
        return "{triangular-index: {name: "+get_name()
            +", position: "+get_index_position().to_string()
            +", order: "+get_order().to_string()+"}}";
    }

    unsigned int RecurTriangularIndex::get_num_components(const unsigned int order) const noexcept
    {
        return (order+1)*(order+2)/2;
    }

    RecurDirection RecurTriangularIndex::get_direction() const noexcept
    {
        return m_direction;
    }

    std::shared_ptr<RecurIndex> RecurTriangularIndex::get_copy() noexcept
    {
        return std::make_shared<RecurTriangularIndex>(std::move(*this));
    }

    std::string RecurSphericalIndex::to_string() const noexcept
    {
        return "{spherical-index: {name: "+get_name()
            +", position: "+get_index_position().to_string()
            +", order: "+std::to_string(get_order()[0])+"}}";
    }

    unsigned int RecurSphericalIndex::get_num_components(const unsigned int order) const noexcept
    {
        return 2*order+1;
    }

    std::shared_ptr<RecurIndex> RecurSphericalIndex::get_copy() noexcept
    {
        return std::make_shared<RecurSphericalIndex>(std::move(*this));
    }
}
