= tIntegral (Version 1.0.0)
Bin Gao <bin.gao@uit.no>
v1.0, 2017-06-22

Documentation of the https://gitlab.com/tglue-chemistry/tintegral[tIntegral]
library.

== Introduction

tIntegral takes care of integrals and derivatives of operators calculated with
basis functions in computational chemistry, and the basis functions are for
the time being contracted (rotational London) atomic orbitals.

tIntegral is not only an integral computation library. Rather, it can do both
integral computations and integral code generation for different electron
operators in computational chemistry.

Moreover, users can even generate their own integral-computation codes by
preparing the so-called recursion specification file, which is much easier
than writing such integral-computation codes.

If you have used tIntegral and found it is useful, please consider to cite the
references in the file *tIntegral.bib*.

== Recurrence-Relation Index and Orders

== Buffer for Recurrence Relations
