/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotWrite().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief writes the conext of nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[FILE]{in} fp_nucpot file pointer
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotWrite(const OneProp one_prop, FILE *fp_nucpot)
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    GInt inuc,jnuc;   /* incremental recorder */
    GErrorCode ierr;  /* error information */
    /* checks the file pointer */
    if (fp_nucpot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL file pointer");
    }
    /* gets the context of the nuclear attraction potential */
    nuc_pot = (NucPot *)(one_prop->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    /* writes the context of the nuclear attraction potential */
    fprintf(fp_nucpot, "\n");
    fprintf(fp_nucpot,
            "NucPotWrite>> order of magnetic derivatives %"GINT_FMT"\n",
            nuc_pot->order_mag);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of partial magnetic derivatives on the bra center %"GINT_FMT"\n",
            nuc_pot->order_mag_bra);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of partial magnetic derivatives on the ket center %"GINT_FMT"\n",
            nuc_pot->order_mag_ket);
    if (nuc_pot->mag_LPF!=NULL) {
        ierr = MagLPFWrite(nuc_pot->mag_LPF, fp_nucpot);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling MagLPFWrite");
    }
    fprintf(fp_nucpot,
            "NucPotWrite>> order of total rotational angular momentum derivatives %"GINT_FMT"\n",
            nuc_pot->order_rot);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of partial TRAM derivatives on the bra center %"GINT_FMT"\n",
            nuc_pot->order_rot_bra);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of partial TRAM derivatives on the ket center %"GINT_FMT"\n",
            nuc_pot->order_rot_ket);
    if (nuc_pot->rot_LPF!=NULL) {
        ierr = RotLPFWrite(nuc_pot->rot_LPF, fp_nucpot);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling RotLPFWrite");
    }
    fprintf(fp_nucpot,
            "NucPotWrite>> type of relativistic corrections %d\n",
            nuc_pot->RC_type);
    fprintf(fp_nucpot,
            "NucPotWrite>> type of the differentiated charge distributions %d\n",
            nuc_pot->DCD_type);
    fprintf(fp_nucpot,
            "NucPotWrite>> index of the dipole origin %"GINT_FMT"\n",
            nuc_pot->idx_diporg);
    fprintf(fp_nucpot,
            "NucPotWrite>> coordinates of the dipole origin (%e,%e,%e)\n",
            nuc_pot->coord_diporg[0],
            nuc_pot->coord_diporg[1],
            nuc_pot->coord_diporg[2]);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of geometric derivatives on the dipole origin %"GINT_FMT"\n",
            nuc_pot->order_geo_diporg);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of Cartesian multipole moments %"GINT_FMT"\n",
            nuc_pot->order_cart_mm);
    fprintf(fp_nucpot, "NucPotWrite>> number of nuclei %"GINT_FMT"\n", nuc_pot->num_nuclei);
    if (nuc_pot->idx_nuclei==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot->idx_nuclei");
    }
    if (nuc_pot->charge_nuclei==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot->charge_nuclei");
    }
    if (nuc_pot->coord_nuclei==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot->coord_nuclei");
    }
    for (inuc=0,jnuc=0; inuc<nuc_pot->num_nuclei; inuc++) {
        fprintf(fp_nucpot,
                "NucPotWrite>> atom %"GINT_FMT", %f, (%e,%e,%e)\n",
                nuc_pot->idx_nuclei[inuc],
                nuc_pot->charge_nuclei[inuc],
                nuc_pot->coord_nuclei[jnuc],
                nuc_pot->coord_nuclei[jnuc+1],
                nuc_pot->coord_nuclei[jnuc+2]);
        jnuc += 3;
    }
    fprintf(fp_nucpot,
            "NucPotWrite>> order of geometric derivatives with respect to the nuclei %"GINT_FMT"\n",
            nuc_pot->order_geo_nuc);
    fprintf(fp_nucpot,
            "NucPotWrite>> order of electronic derivatives %"GINT_FMT"\n",
            nuc_pot->order_elec_deriv);
    return GSUCCESS;
}
