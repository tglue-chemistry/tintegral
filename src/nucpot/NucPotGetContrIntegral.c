/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotGetContrIntegral().

   2014-06-28, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief calculates the integral of nuclear attraction potential
    \author Bin Gao
    \date 2014-06-28
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[GeoDeriv:struct]{in} geo_deriv geometric derivatives
    \param[GeoDeriv:struct]{in} geo_deriv_bra geometric derivatives on the bra center
    \param[GeoDeriv:struct]{in} geo_deriv_ket geometric derivatives on the ket center
    \param[GInt:int]{in} idx_bra index of the bra center
    \param[GReal:real]{in} coord_bra coordinates of the bra center
    \param[ContrGTO:struct]{in} contr_GTO_bra contracted GTOs on the bra center
    \param[GInt:int]{in} idx_ket index of the ket center
    \param[GReal:real]{in} coord_ket coordinates of the ket center
    \param[ContrGTO:struct]{in} contr_GTO_ket contracted GTOs on the ket center
    \param[GBool:enum]{in} add_values indicates if adding values to the existing entries
    \param[GReal:real]{out} val_int values of the integral of the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotGetContrIntegral(const OneProp one_prop,
                                  const GeoDeriv *geo_deriv,
                                  const GeoDeriv *geo_deriv_bra,
                                  const GeoDeriv *geo_deriv_ket,
                                  const GInt idx_bra,
                                  const GReal coord_bra[3],
                                  const ContrGTO *contr_GTO_bra,
                                  const GInt idx_ket,
                                  const GReal coord_ket[3],
                                  const ContrGTO *contr_GTO_ket,
                                  const GBool add_values,
                                  GReal *val_int)
{
    GBool zero_value;        /* indicates if the value is zero */
    GInt num_geo_deriv;      /* number of geometric derivatives */
    GInt geo_num_cent;       /* number of differentiated centers */
    GInt geo_idx_cent[1];    /* indices of differentiated centers */
    GInt num_geo_deriv_bra;  /* number of partial geometric derivatives on the bra center */
    GInt num_geo_deriv_ket;  /* number of partial geometric derivatives on the ket center */
    GInt size_one_prop;      /* size of the one-electron property */
    GInt num_AO_bra;         /* number of AOs on the bra center */
    GInt num_AO_ket;         /* number of AOs on the ket center */
    GInt size_int;           /* size of the values of the integral of the one-electron property */
    NucPot *nuc_pot;         /* context of nuclear attraction potential */
    GInt ival;               /* incremental recoder */
    GErrorCode ierr;         /* error information */
    /* first assumes the value is not zero */
    zero_value = GFALSE;
    /* computes the number of geometric derivatives */
    if (geo_deriv==NULL) {
        num_geo_deriv = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv, &num_geo_deriv);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv");
    }
    /* computes the number of partial geometric derivatives on the bra center */
    if (geo_deriv_bra==NULL) {
        num_geo_deriv_bra = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_bra, &num_geo_deriv_bra);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (bra)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_bra, &geo_num_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (bra)");
        switch (geo_num_cent) {
        /* no geometric derivatives */
        case 0:
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetIdxCent(geo_deriv_bra, geo_num_cent, geo_idx_cent);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetIdxCent (bra)");
            if (geo_idx_cent[0]!=idx_bra) {
                zero_value = GTRUE;
            }
            break;
        /* more than one differentiated center */
        default:
            zero_value = GTRUE;
        }
    }
    /* computes the number of partial geometric derivatives on the ket center */
    if (geo_deriv_ket==NULL) {
        num_geo_deriv_ket = 1;
    }
    else {
        ierr = GeoDerivCombinGetNumDeriv(geo_deriv_ket, &num_geo_deriv_ket);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivGetNumDeriv (ket)");
        /* checks the differentiated centers */
        ierr = GeoDerivCombinGetNumCent(geo_deriv_ket, &geo_num_cent);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetNumCent (ket)");
        switch (geo_num_cent) {
        /* no geometric derivatives */
        case 0:
            break;
        /* one differentiated center */
        case 1:
            ierr = GeoDerivCombinGetIdxCent(geo_deriv_ket, geo_num_cent, geo_idx_cent);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling GeoDerivCombinGetIdxCent (ket)");
            if (geo_idx_cent[0]!=idx_ket) {
                zero_value = GTRUE;
            }
            break;
        /* more than one differentiated center */
        default:
            zero_value = GTRUE;
        }
    }
    /* gets the size of the one-electron property, this function will
       also check the validity of the linked list */
    ierr = OnePropGetSize(one_prop, &size_one_prop);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropGetSize");
    /* gets the sizes of contracted GTOs */
    ierr = ContrGTOGetNumAO(contr_GTO_bra, &num_AO_bra);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling ContrGTOGetNumAO (bra)");
    ierr = ContrGTOGetNumAO(contr_GTO_ket, &num_AO_ket);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling ContrGTOGetNumAO (ket)");
    /* computes the size of the value */
    size_int = num_geo_deriv*num_geo_deriv_bra*num_geo_deriv_ket
             * size_one_prop*num_AO_bra*num_AO_ket;
    /* due to partial geometric derivatives, the value is zero */
    if (zero_value==GTRUE) {
        for (ival=0; ival<size_int; ival++) {
            val_int[ival] = 0;
        }
    }
    else {
        /* gets the context of the nuclear attraction potential */
        nuc_pot = (NucPot *)(one_prop->data);
        if (nuc_pot==NULL) {
            GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
        }
    }
    return GSUCCESS;
}
