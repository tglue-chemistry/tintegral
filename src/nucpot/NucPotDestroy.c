/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotDestroy().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief destroys the context of nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotDestroy(OneProp *one_prop)
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    /* gets the context of the nuclear attraction potential */
    nuc_pot = (NucPot *)((*one_prop)->data);
    if (nuc_pot!=NULL) {
        /* destroys the context of the nuclear attraction potential */
        nuc_pot->order_mag = 0;
        nuc_pot->order_mag_bra = 0;
        nuc_pot->order_mag_ket = 0;
        /* no need to destroy the information of nuc_pot->mag_LPF,
           see the file derivatives/gen1int_magnetic.h */
        if (nuc_pot->mag_LPF!=NULL) {
            free(nuc_pot->mag_LPF);
            nuc_pot->mag_LPF = NULL;
        }
        nuc_pot->order_rot = 0;
        nuc_pot->order_rot_bra = 0;
        nuc_pot->order_rot_ket = 0;
        /* no need to destroy the information of nuc_pot->rot_LPF,
           see the file derivatives/gen1int_rotational.h */
        if (nuc_pot->rot_LPF!=NULL) {
            free(nuc_pot->rot_LPF);
            nuc_pot->rot_LPF = NULL;
        }
        nuc_pot->RC_type = NR_CORRECTION;
        nuc_pot->DCD_type = DIFF_CD_ZERO;
        nuc_pot->idx_diporg = GMAX_IDX_NAT;
        nuc_pot->coord_diporg[0] = 0;
        nuc_pot->coord_diporg[1] = 0;
        nuc_pot->coord_diporg[2] = 0;
        nuc_pot->order_geo_diporg = 0;
        nuc_pot->order_cart_mm = 0;
        nuc_pot->num_nuclei = 0;
        if (nuc_pot->idx_nuclei!=NULL) {
            free(nuc_pot->idx_nuclei);
            nuc_pot->idx_nuclei = NULL;
        }
        if (nuc_pot->charge_nuclei!=NULL) {
            free(nuc_pot->charge_nuclei);
            nuc_pot->charge_nuclei = NULL;
        }
        if (nuc_pot->coord_nuclei!=NULL) {
            free(nuc_pot->coord_nuclei);
            nuc_pot->coord_nuclei = NULL;
        }
        nuc_pot->order_geo_nuc = 0;
        nuc_pot->order_elec_deriv = 0;
        free((*one_prop)->data);
        (*one_prop)->data = NULL;
    }
    return GSUCCESS;
}
