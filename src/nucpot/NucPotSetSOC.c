/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotSetSOC().

   2014-06-28, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief adds the spin-orbit (SO) correction for nuclear attraction potential
    \author Bin Gao
    \date 2014-06-28
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotSetSOC(OneProp *one_prop)
{
    NucPot *nuc_pot;            /* context of nuclear attraction potential */
    OneProp nuc_deriv_node;     /* new node for the derivatives w.r.t the nuclei */
    OneProp diporg_deriv_node;  /* new node for the derivatives w.r.t. the dipole origin */
    GInt inuc;                  /* incremental recorder */
    GErrorCode ierr;            /* error information */
    /* gets the context of the nuclear attraction potential */
    nuc_pot = (NucPot *)((*one_prop)->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    /* sets the size of SOC in the abstract one-electron property */
    ierr = OnePropIdxSetSOC(*one_prop);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetSOC");
    /* there was no relativistic correction before */
    if (nuc_pot->RC_type==NR_CORRECTION) {
        /* sets the type of relativistic corrections */
        RelCorrectionSetType(&nuc_pot->RC_type, SO_CORRECTION);
        /* updates for the second order differentiated charge distributions */
        nuc_pot->DCD_type = DIFF_CD_SECOND;
        nuc_pot->order_elec_deriv += 2;
        /* derivatives with respect to the nuclei */
        ierr = OnePropNodeCreateHEAD(&nuc_deriv_node);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeCreateHEAD");
        /* duplicates the conext of the nuclear attraction potential to the new node */
        ierr = OnePropNodeDuplicate(*one_prop, nuc_deriv_node);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeDuplicate");
        /* updates the conext of the nuclear attraction potential for SOC */
        nuc_pot = (NucPot *)(nuc_deriv_node->data);
        nuc_pot->DCD_type = DIFF_CD_FIRST;
        for (inuc=0; inuc<nuc_pot->num_nuclei; inuc++) {
            nuc_pot->charge_nuclei[inuc] = -nuc_pot->charge_nuclei[inuc];
        }
        nuc_pot->order_geo_nuc++;
        nuc_pot->order_elec_deriv--;
        /* inserts the node after the current node */
        ierr = OnePropListInsertAfterHEAD(*one_prop, nuc_deriv_node);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropListInsertAfterHEAD");
        /* moves the current node to the new node */
        *one_prop = nuc_deriv_node;
        /* derivatives with respect to the dipole origin */
        if (nuc_pot->order_geo_diporg<nuc_pot->order_cart_mm) {
            /* creates the new node for the derivatives w.r.t. the dipole origin */
            ierr = OnePropNodeCreateHEAD(&diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeCreateHEAD");
            /* duplicates the conext of the nuclear attraction potential to the new node */
            ierr = OnePropNodeDuplicate(*one_prop, diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropNodeDuplicate");
            /* updates the conext of the nuclear attraction potential for SOC */
            nuc_pot = (NucPot *)(diporg_deriv_node->data);
            nuc_pot->order_geo_diporg++;
            nuc_pot->order_geo_nuc--;
            /* inserts the node after the current node */
            ierr = OnePropListInsertAfterHEAD(*one_prop, diporg_deriv_node);
            GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropListInsertAfterHEAD");
            /* moves the current node to the new node */
            *one_prop = diporg_deriv_node;
        }
    }
    else {
        /* only updates the type of relativistic corrections */
        RelCorrectionSetType(&nuc_pot->RC_type, SO_CORRECTION);
    }
    return GSUCCESS;
}
