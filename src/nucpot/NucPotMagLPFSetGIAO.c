/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotMagLPFSetGIAO().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief uses gauge including atomic orbital (GIAO) for magnetic derivatives
        of the nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotMagLPFSetGIAO(OneProp *one_prop)
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    nuc_pot = (NucPot *)((*one_prop)->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    if (nuc_pot->mag_LPF==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot->mag_LPF");
    }
    MagLPFSetGIAO(nuc_pot->mag_LPF);
    return GSUCCESS;
}
