/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function OnePropNucPotCreate().

   2014-06-24, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"
#include "utilities/gen1int_sort.h"

/*@% \brief creates the context of nuclear attraction potential
     \author Bin Gao
     \date 2014-06-24
     \param[OneProp:struct]{inout} one_prop the one-electron property
     \param[GInt:int]{in} idx_diporg index of the dipole origin
     \param[GReal:real]{in} coord_diporg coordinates of the dipole origin
     \param[GInt:int]{in} order_geo_diporg order of geometric derivatives on the dipole origin
     \param[GInt:int]{in} order_cart_mm order of Cartesian multipole moments
     \param[GInt:int]{in} num_nuclei number of nuclei
     \param[GInt:int]{in} idx_nuclei indices of the nuclei
     \param[GReal:real]{in} charge_nuclei charges of the nuclei
     \param[GReal:real]{in} coord_nuclei coordinates of the nuclei
     \param[GInt:int]{in} order_geo_nuc order of geometric derivatives with respect to the nuclei
     \param[GInt:int]{in} order_elec_deriv order of electronic derivatives
     \param[GBool:enum]{in} sort_nuclei indicates if sorting the indices of nuclei
     \return[GErrorCode:int] error information
*/
GErrorCode OnePropNucPotCreate(OneProp *one_prop,
                               const GInt idx_diporg,
                               const GReal coord_diporg[3],
                               const GInt order_geo_diporg,
                               const GInt order_cart_mm,
                               const GInt num_nuclei,
                               const GInt *idx_nuclei,
                               const GReal *charge_nuclei,
                               const GReal *coord_nuclei,
                               const GInt order_geo_nuc,
                               const GInt order_elec_deriv,
                               const GBool sort_nuclei)
{
    OneProp head_node;      /* HEAD of the linked list */
    NucPot *nuc_pot;        /* context of nuclear attraction potential */
    GInt inuc, ixyz, jxyz;  /* incremental recorders */
    GInt *tag_nuclei;       /* tags marking the positions of nuclei indices before sorting */
    GErrorCode ierr;        /* error information */
    /* checks the validity of arguments */
    if (order_geo_diporg<0) {
        printf("OnePropNucPotCreate>> order of geometric derivatives on the dipole origin %"GINT_FMT"\n",
               order_geo_diporg);
        GErrorExit(FILE_AND_LINE, "invalid order of geometric derivatives on the dipole origin");
    }
    if (order_cart_mm<0) {
        printf("OnePropNucPotCreate>> order of Cartesian multipole moments %"GINT_FMT"\n",
               order_cart_mm);
        GErrorExit(FILE_AND_LINE, "invalid order of Cartesian multipole moments");
    }
    if (num_nuclei<=0) {
        printf("OnePropNucPotCreate>> number of nuclei %"GINT_FMT"\n", num_nuclei);
        GErrorExit(FILE_AND_LINE, "invalid number of nuclei");
    }
    if (order_geo_nuc<0) {
        printf("OnePropNucPotCreate>> order of geometric derivatives w.r.t. the nuclei %"GINT_FMT"\n",
               order_geo_nuc);
        GErrorExit(FILE_AND_LINE, "invalid order of geometric derivatives w.r.t. the nuclei");
    }
    if (order_elec_deriv<0) {
        printf("OnePropNucPotCreate>> order of electronic derivatives %"GINT_FMT"\n",
               order_elec_deriv);
        GErrorExit(FILE_AND_LINE, "invalid order of electronic derivatives");
    }
    /* destroys the context of the one-electron property if needed */
    /*if (*one_prop!=NULL) {
        ierr = OnePropDestroy(one_prop);
        GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropDestroy");
    }*/
    /* initializes the nuclear attraction potentital and its different derivatives */
    nuc_pot = (NucPot *)malloc(sizeof(NucPot));
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for nuc_pot");
    }
    nuc_pot->order_mag = 0;
    nuc_pot->order_mag_bra = 0;
    nuc_pot->order_mag_ket = 0;
    nuc_pot->mag_LPF = NULL;
    nuc_pot->order_rot = 0;
    nuc_pot->order_rot_bra = 0;
    nuc_pot->order_rot_ket = 0;
    nuc_pot->rot_LPF = NULL;
    nuc_pot->RC_type = NR_CORRECTION;
    nuc_pot->DCD_type = DIFF_CD_ZERO;
    /* allocates memory */
    nuc_pot->idx_nuclei = (GInt *)malloc(num_nuclei*sizeof(GInt));
    if (nuc_pot->idx_nuclei==NULL) {
        printf("OnePropNucPotCreate>> number of nuclei %"GINT_FMT"\n", num_nuclei);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for nuc_pot->idx_nuclei");
    }
    nuc_pot->charge_nuclei = (GReal *)malloc(num_nuclei*sizeof(GReal));
    if (nuc_pot->charge_nuclei==NULL) {
        printf("OnePropNucPotCreate>> number of nuclei %"GINT_FMT"\n", num_nuclei);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for nuc_pot->charge_nuclei");
    }
    nuc_pot->coord_nuclei = (GReal *)malloc(3*num_nuclei*sizeof(GReal));
    if (nuc_pot->coord_nuclei==NULL) {
        printf("OnePropNucPotCreate>> number of nuclei %"GINT_FMT"\n", num_nuclei);
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for nuc_pot->coord_nuclei");
    }
    /* sets the function \partial_{D}^{L_{D}}r_{D}^{m}
       \sum_{C}^{N_{C}}\partial_{C}^{L_{C}}(Z_{C}/r_{C})\partial_{r}^{n} */
    nuc_pot->idx_diporg = idx_diporg;
    nuc_pot->coord_diporg[0] = coord_diporg[0];
    nuc_pot->coord_diporg[1] = coord_diporg[1];
    nuc_pot->coord_diporg[2] = coord_diporg[2];
    nuc_pot->order_geo_diporg = order_geo_diporg;
    nuc_pot->order_cart_mm = order_cart_mm;
    nuc_pot->num_nuclei = num_nuclei;
    if (sort_nuclei==GTRUE) {
        /* sorts nuclei in ascending order according to their indices */
        tag_nuclei = (GInt *)malloc(num_nuclei*sizeof(GInt));
        if (tag_nuclei==NULL) {
            printf("OnePropNucPotCreate>> number of nuclei %"GINT_FMT"\n", num_nuclei);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for tag_nuclei");
        }
        for (inuc=0; inuc<nuc_pot->num_nuclei; inuc++) {
            nuc_pot->idx_nuclei[inuc] = idx_nuclei[inuc];
            tag_nuclei[inuc] = inuc;
        }
        SortTagIdxAscending(nuc_pot->num_nuclei, nuc_pot->idx_nuclei, tag_nuclei);
        for (inuc=0,ixyz=0; inuc<nuc_pot->num_nuclei; inuc++) {
            nuc_pot->charge_nuclei[inuc] = charge_nuclei[tag_nuclei[inuc]];
            jxyz = 3*tag_nuclei[inuc];
            nuc_pot->coord_nuclei[ixyz] = coord_nuclei[jxyz];      /* x */
            nuc_pot->coord_nuclei[ixyz+1] = coord_nuclei[jxyz+1];  /* y */
            nuc_pot->coord_nuclei[ixyz+2] = coord_nuclei[jxyz+2];  /* z */
            ixyz += 3;
        }
        free(tag_nuclei);
        tag_nuclei = NULL;
    }
    else {
        for (inuc=0,ixyz=0; inuc<nuc_pot->num_nuclei; inuc++) {
            nuc_pot->idx_nuclei[inuc] = idx_nuclei[inuc];
            nuc_pot->charge_nuclei[inuc] = charge_nuclei[inuc];
            nuc_pot->coord_nuclei[ixyz] = coord_nuclei[ixyz];      /* x */
            nuc_pot->coord_nuclei[ixyz+1] = coord_nuclei[ixyz+1];  /* y */
            nuc_pot->coord_nuclei[ixyz+2] = coord_nuclei[ixyz+2];  /* z */
            ixyz += 3;
        }
    }
    nuc_pot->order_geo_nuc = order_geo_nuc;
    nuc_pot->order_elec_deriv = order_elec_deriv;
    /* allocates memory for the abstract one-electron property,
       note that this is the HEAD of the linked list */
    head_node = (_p_OneProp *)malloc(sizeof(_p_OneProp));
    if (head_node==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for head_node");
    }
    /* sets the implementation-specific data of the one-electron property */
    head_node->data = (GVoid *)nuc_pot;
    /* sets the implementation-specific functions of the one-electron property */
    head_node->one_prop_fun = (OnePropFun *)malloc(sizeof(OnePropFun));
    if (head_node->one_prop_fun==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for head_node->one_prop_fun");
    }
    head_node->one_prop_fun->onepropmagsetorder       = NucPotMagSetOrder;
    head_node->one_prop_fun->onepropmaglpfcreate      = NucPotMagLPFCreate;
    head_node->one_prop_fun->onepropmaglpfsetgiao     = NucPotMagLPFSetGIAO;
    head_node->one_prop_fun->onepropmaglpfsetgiop     = NucPotMagLPFSetGIOP;
    head_node->one_prop_fun->oneproprotsetorder       = NucPotRotSetOrder;
    head_node->one_prop_fun->oneproprotlpfcreate      = NucPotRotLPFCreate;
    head_node->one_prop_fun->oneproprotlpfsetciao     = NucPotRotLPFSetCIAO;
    head_node->one_prop_fun->oneproprotlpfsetciop     = NucPotRotLPFSetCIOP;
    head_node->one_prop_fun->onepropsetsrc            = NucPotSetSRC;
    head_node->one_prop_fun->onepropsetsoc            = NucPotSetSOC;
    head_node->one_prop_fun->onepropwrite             = NucPotWrite;
    head_node->one_prop_fun->onepropgetcontrintegral  = NucPotGetContrIntegral;
    head_node->one_prop_fun->onepropgetcontrintegrand = NucPotGetContrIntegrand;
#if defined(GEN1INT_AO_SHELL)
    head_node->one_prop_fun->onepropgetshellintegral  = NucPotGetShellIntegral;
    head_node->one_prop_fun->onepropgetshellintegrand = NucPotGetShellIntegrand;
#endif
    head_node->one_prop_fun->onepropduplicate         = NucPotDuplicate;
    head_node->one_prop_fun->onepropdestroy           = NucPotDestroy;
    /* initializes the context of the nuclear attraction potential */
    head_node->assembled = GFALSE;
    head_node->data_type = GREALPROP;
/*FIXME: is this correct: symmetric -- even order of electronic derivatives,
  anti-symmetric -- odd order of electronic derivatives */
    head_node->sym_type = ((order_elec_deriv&1)==0) ? GSYMPROP : GANTISYMPROP;
    ierr = OnePropIdxCreate(head_node);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxCreate");
    ierr = OnePropIdxSetGeoOper(head_node, order_geo_nuc, order_geo_diporg);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetGeoOper");
    ierr = OnePropIdxSetCartMM(head_node, order_cart_mm);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetCartMM");
    ierr = OnePropIdxSetElecDeriv(head_node, order_elec_deriv);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetElecDeriv");
    head_node->next_node = NULL;
    /* returns this property */
    *one_prop = head_node;
    return GSUCCESS;
}
