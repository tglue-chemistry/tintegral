/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotDuplicate().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief duplicates the conext of nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{in} one_prop the one-electron property
    \param[OneProp:struct]{inout} new_prop the duplicated one-electron property
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotDuplicate(const OneProp one_prop, OneProp *new_prop)
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    NucPot *new_pot;  /* context of duplicated nuclear attraction potential */
    GInt inuc;        /* incremental recorder */
    /* gets the context of the nuclear attraction potential */
    nuc_pot = (NucPot *)(one_prop->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    /* allocates memory for the duplicated nuclear attraction potential */
    new_pot = (NucPot *)malloc(sizeof(NucPot));
    if (new_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot");
    }
    /* duplicates the context of the nuclear attraction potential */
    new_pot->order_mag = nuc_pot->order_mag;
    new_pot->order_mag_bra = nuc_pot->order_mag_bra;
    new_pot->order_mag_ket = nuc_pot->order_mag_ket;
    if (nuc_pot->mag_LPF!=NULL) {
        new_pot->mag_LPF = (MagLPF *)malloc(sizeof(MagLPF));
        if (new_pot->mag_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot->mag_LPF");
        }
        MagLPFDuplicate(nuc_pot->mag_LPF, new_pot->mag_LPF);
    }
    else {
        new_pot->mag_LPF = NULL;
    }
    new_pot->order_rot = nuc_pot->order_rot;
    new_pot->order_rot_bra = nuc_pot->order_rot_bra;
    new_pot->order_rot_ket = nuc_pot->order_rot_ket;
    if (nuc_pot->rot_LPF!=NULL) {
        new_pot->rot_LPF = (RotLPF *)malloc(sizeof(RotLPF));
        if (new_pot->rot_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot->rot_LPF");
        }
        RotLPFDuplicate(nuc_pot->rot_LPF, new_pot->rot_LPF);
    }
    else {
        new_pot->rot_LPF = NULL;
    }
    new_pot->RC_type = nuc_pot->RC_type;
    new_pot->DCD_type = nuc_pot->DCD_type;
    new_pot->idx_diporg = nuc_pot->idx_diporg;
    new_pot->coord_diporg[0] = nuc_pot->coord_diporg[0];
    new_pot->coord_diporg[1] = nuc_pot->coord_diporg[1];
    new_pot->coord_diporg[2] = nuc_pot->coord_diporg[2];
    new_pot->order_geo_diporg = nuc_pot->order_geo_diporg;
    new_pot->order_cart_mm = nuc_pot->order_cart_mm;
    new_pot->num_nuclei = nuc_pot->num_nuclei;
    if (nuc_pot->idx_nuclei!=NULL) {
        new_pot->idx_nuclei = (GInt *)malloc(nuc_pot->num_nuclei*sizeof(GInt));
        if (new_pot->idx_nuclei==NULL) {
            printf("NucPotDuplicate>> number of nuclei %"GINT_FMT"\n", nuc_pot->num_nuclei);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot->idx_nuclei");
        }
        for (inuc=0; inuc<nuc_pot->num_nuclei; inuc++) {
            new_pot->idx_nuclei[inuc] = nuc_pot->idx_nuclei[inuc];
        }
    }
    if (nuc_pot->charge_nuclei!=NULL) {
        new_pot->charge_nuclei = (GReal *)malloc(nuc_pot->num_nuclei*sizeof(GReal));
        if (new_pot->charge_nuclei==NULL) {
            printf("NucPotDuplicate>> number of nuclei %"GINT_FMT"\n", nuc_pot->num_nuclei);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot->charge_nuclei");
        }
        for (inuc=0; inuc<nuc_pot->num_nuclei; inuc++) {
            new_pot->charge_nuclei[inuc] = nuc_pot->charge_nuclei[inuc];
        }
    }
    if (nuc_pot->coord_nuclei!=NULL) {
        new_pot->coord_nuclei = (GReal *)malloc(3*nuc_pot->num_nuclei*sizeof(GReal));
        if (new_pot->coord_nuclei==NULL) {
            printf("NucPotDuplicate>> number of nuclei %"GINT_FMT"\n", nuc_pot->num_nuclei);
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for new_pot->coord_nuclei");
        }
        for (inuc=0; inuc<3*nuc_pot->num_nuclei; inuc++) {
            new_pot->coord_nuclei[inuc] = nuc_pot->coord_nuclei[inuc];
        }
    }
    new_pot->order_geo_nuc = nuc_pot->order_geo_nuc;
    new_pot->order_elec_deriv = nuc_pot->order_elec_deriv;
    /* sets the implementation-specific data of the duplicated one-electron property */
    if ((*new_prop)->data!=NULL) {
        GErrorExit(FILE_AND_LINE, "previous implementation-specific data not destroyed");
    }
    (*new_prop)->data = (GVoid *)new_pot;
    return GSUCCESS;
}
