/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotMagLPFCreate().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief creates the context of London phase factor for magnetic derivatives
        of the nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \param[GReal:real]{in} gauge_origin gauge origin of the magnetic vector potential
    \param[GReal:real]{in} origin_LPF origin of the London phase factor
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotMagLPFCreate(OneProp *one_prop,
                              const GReal gauge_origin[3],
                              const GReal origin_LPF[3])
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    nuc_pot = (NucPot *)((*one_prop)->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    if (nuc_pot->mag_LPF==NULL) {
        nuc_pot->mag_LPF = (MagLPF *)malloc(sizeof(MagLPF));
        if (nuc_pot->mag_LPF==NULL) {
            GErrorExit(FILE_AND_LINE, "failed to allocate memory for nuc_pot->mag_LPF");
        }
    }
    MagLPFCreate(nuc_pot->mag_LPF, gauge_origin, origin_LPF);
    return GSUCCESS;
}
