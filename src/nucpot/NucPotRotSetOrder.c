/* Gen1Int: not only one-electron integral library
   Copyright 2015 Bin Gao

   Gen1Int is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Gen1Int is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with Gen1Int. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function NucPotRotSetOrder().

   2014-06-27, Bin Gao:
   * first version
*/

#include "impls/nucpot_impl.h"

/*% \brief sets the orders of total rotational angular momentum (TRAM) derivatives
        for nuclear attraction potential
    \author Bin Gao
    \date 2014-06-27
    \param[OneProp:struct]{inout} one_prop the one-electron property
    \param[GInt:int]{in} order_rot order of the TRAM derivatives
    \param[GInt:int]{in} order_rot_bra order of partial TRAM derivatives
        on the bra center
    \param[GInt:int]{in} order_rot_ket order of partial TRAM derivatives
        on the ket center
    \return[GErrorCode:int] error information
*/
GErrorCode NucPotRotSetOrder(OneProp *one_prop,
                             const GInt order_rot,
                             const GInt order_rot_bra,
                             const GInt order_rot_ket)
{
    NucPot *nuc_pot;  /* context of nuclear attraction potential */
    GErrorCode ierr;  /* error information */
    nuc_pot = (NucPot *)((*one_prop)->data);
    if (nuc_pot==NULL) {
        GErrorExit(FILE_AND_LINE, "NULL nuc_pot");
    }
    nuc_pot->order_rot = order_rot;
    nuc_pot->order_rot_bra = order_rot_bra;
    nuc_pot->order_rot_ket = order_rot_ket;
    /* also sets the sizes of indices in the abstract one-electron property */
    ierr = OnePropIdxSetRotDeriv(*one_prop, order_rot, order_rot_bra, order_rot_ket);
    GErrorCheckCode(ierr, FILE_AND_LINE, "calling OnePropIdxSetRotDeriv");
    return GSUCCESS;
}
