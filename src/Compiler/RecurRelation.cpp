/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file builds recurrence relation expressions from given integrand.

   2020-06-09, Bin Gao:
   * first version
*/

#include <array>
#include <string>

#include "tIntegral/Symbol/RecurDirection.hpp"
#include "tIntegral/Symbol/RecurVector.hpp"
#include "tIntegral/Compiler/RecurRelation.hpp"

/* Generates codes for getting primitive HGTO integrals in the
   bottom-up procedure, from given recurrence relations and
   recursion specification; codes including:
   (1) getting Cartesian multipole moments at the London phase
       factor origin from HGTOs,
   (2) transformation from HGTOs to SGTOs,
   (3) transformation from HGTOs to CGTOs,
   (4) getting Cartesian multipole moments at the London phase
       factor origin from CGTOs,
   (5) getting magnetic derivatives,
   (6) getting derivatives w.r.t. total rotational angular momentum,
   will also be generated if the order of xyz components (given by
   the recursion specification) are not implemented in the class
   \sa{tOneIntegrand}
 */

namespace tIntegral
{
    //std::vector<RecurExpression>
    //RecurRelation::get_expression(const std::vector<std::pair<std::shared_ptr<RecurIndex>,IndexCompileOption>>& indices) noexcept
    //{
    //    std::vector<RecurExpression> expressions;
    //    for (auto const& each_index: indices) {
    //        //if (std::get<0>(each_index)<m_integrand.size()) {
    //            if (each_index.second==IndexCompileOption::Recursion) {
    //                //m_integrand[each_index.first]
    //            }
    //       // }
    //       // else {
    //       //     return false;
    //       // }
    //    }
    //}

    std::vector<RecurExpression>
    RecurRelation::get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                                  const std::shared_ptr<RecurCartMultMoment>& oper,
                                  const std::shared_ptr<RecurGaussianFunction>& ket) noexcept
    {
        /* Recurrence relations of the operator with HGTOs */
        std::vector<RecurExpression> expressions;
        expressions.reserve(3);
        /* Indices involved in the integral */
        auto idx_exponent_bra = bra->get_idx_exponent();
        auto idx_angular_bra = bra->get_idx_angular();
        auto idx_exponent_ket = ket->get_idx_exponent();
        auto idx_angular_ket = ket->get_idx_angular();
        auto idx_moment = oper->get_idx_moment();
        /* Increments and decrements of indices */
        auto zero_angular_bra = idx_angular_bra->get_copy();
        zero_angular_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_bra = idx_angular_bra->get_copy();
        dec_angular_bra->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_bra = idx_angular_bra->get_copy();
        dec2_angular_bra->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_bra = idx_angular_bra->get_copy();
        inc_angular_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_angular_ket = idx_angular_ket->get_copy();
        zero_angular_ket->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_ket = idx_angular_ket->get_copy();
        dec_angular_ket->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec_moment = idx_moment->get_copy();
        dec_moment->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_moment = idx_moment->get_copy();
        dec2_moment->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        /* Assign the recurrence relation of Cartesian multipole moments using translational invariance */
        expressions.push_back(RecurExpression(
            /* Most consecutive index comes last, indices regarding exponents
               do not need to be involved in the recurrence relation code
               generation because they are represented by std::valarray */
            std::vector<std::shared_ptr<RecurIndex>>({idx_moment, idx_angular_ket, idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurCartesianVec>(
                        std::string("oper_to_cc"),
                        bra,
                        std::string("to_centre_of_charge"),
                        std::set<std::shared_ptr<RecurSymbol>>({
                            ket,
                            std::make_shared<RecurCartesianVar>(
                                std::string("oper_origin"),
                                oper,
                                std::string("get_origin"),
                                std::set<std::shared_ptr<RecurSymbol>>(),
                                RecurDirection::X
                            )
                        }),
                        RecurDirection::X,
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({dec_moment, zero_angular_ket, zero_angular_bra})
                    )
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("inv2_total_expts"),
                        bra,
                        std::string("inv2_total_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket}),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurParentheses>(
                        std::make_shared<RecurAddition>(
                            std::make_shared<RecurMultiplication>(
                                std::make_shared<RecurIdxOrder>(idx_angular_bra),
                                std::make_shared<RecurTerm>(
                                    std::vector<std::shared_ptr<RecurIndex>>({
                                        dec_moment,
                                        zero_angular_ket,
                                        dec_angular_bra
                                    })
                                )
                            ),
                            std::make_shared<RecurAddition>(
                                std::make_shared<RecurMultiplication>(
                                    std::make_shared<RecurIdxOrder>(idx_angular_ket),
                                    std::make_shared<RecurTerm>(
                                        std::vector<std::shared_ptr<RecurIndex>>({
                                            dec_moment,
                                            dec_angular_ket,
                                            zero_angular_bra
                                        })
                                    )
                                ),
                                std::make_shared<RecurMultiplication>(
                                    std::make_shared<RecurIdxOrder>(idx_moment),
                                    std::make_shared<RecurTerm>(
                                        std::vector<std::shared_ptr<RecurIndex>>({
                                            dec2_moment,
                                            zero_angular_ket,
                                            zero_angular_bra
                                        })
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ));
        /* Assign the recurrence relation of HGTO on ket centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_angular_ket, idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurMultiplication>(
                std::make_shared<RecurSubtraction>(
                    nullptr,
                    std::make_shared<RecurScalarVec>(
                        std::string("scaled_expts"),
                        bra,
                        std::string("get_scaled_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket}),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    )
                ),
                std::make_shared<RecurTerm>(
                    std::vector<std::shared_ptr<RecurIndex>>({dec_angular_ket, inc_angular_bra})
                )
            )
        ));
        /* Assign the recurrence relation of HGTO on bra centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurSubtraction>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurCartesianVec>(
                        std::string("bra_to_cc"),
                        bra,
                        std::string("bra_to_centre_of_charge"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket}),
                        RecurDirection::X,
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(std::vector<std::shared_ptr<RecurIndex>>({dec_angular_bra}))
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurIdxOrder>(idx_angular_bra),
                        std::make_shared<RecurScalarVec>(
                            std::string("inv2_scaled_texpts"),
                            bra,
                            std::string("inv2_scaled_total_exponents"),
                            std::set<std::shared_ptr<RecurSymbol>>({ket}),
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                        )
                    ),
                    std::make_shared<RecurTerm>(std::vector<std::shared_ptr<RecurIndex>>({dec2_angular_bra}))
                )
            )
        ));
        return expressions;
    }

    std::vector<RecurExpression>
    RecurRelation::get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                                  const std::shared_ptr<RecurGaussianPotential>& oper,
                                  const std::shared_ptr<RecurGaussianFunction>& ket) noexcept
    {
        /* Recurrence relations of the operator with HGTOs */
        std::vector<RecurExpression> expressions;
        expressions.reserve(3);
        /* Indices involved in the integral */
        auto idx_exponent_bra = bra->get_idx_exponent();
        auto idx_angular_bra = bra->get_idx_angular();
        auto idx_exponent_ket = ket->get_idx_exponent();
        auto idx_angular_ket = ket->get_idx_angular();
        auto idx_radial = oper->get_idx_radial();
        auto idx_bessel = oper->get_idx_bessel();
        auto idx_geometrical = oper->get_idx_geometrical();
        /* Increments and decrements of indices */
        auto zero_angular_bra = idx_angular_bra->get_copy();
        zero_angular_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_bra = idx_angular_bra->get_copy();
        dec_angular_bra->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_bra = idx_angular_bra->get_copy();
        dec2_angular_bra->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_bra = idx_angular_bra->get_copy();
        inc_angular_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_angular_ket = idx_angular_ket->get_copy();
        zero_angular_ket->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_ket = idx_angular_ket->get_copy();
        dec_angular_ket->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_ket = idx_angular_ket->get_copy();
        dec2_angular_ket->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_ket = idx_angular_ket->get_copy();
        inc_angular_ket->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto dec_geometrical = idx_geometrical->get_copy();
        dec_geometrical->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto zero_bessel = idx_bessel->get_copy();
        zero_bessel->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto inc_bessel = idx_bessel->get_copy();
        inc_bessel->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_radial = idx_radial->get_copy();
        zero_radial->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto inc_radial = idx_radial->get_copy();
        inc_radial->set_order(RecurVector(std::array<int,3>({1,0,0})));
        /* Assign the recurrence relation of HGTO on bra centre using translational invariance */
        expressions.push_back(RecurExpression(
            /* Most consecutive index comes last, indices regarding exponents
               do not need to be involved in the recurrence relation code
               generation because they are represented by std::valarray */
            std::vector<std::shared_ptr<RecurIndex>>({idx_geometrical, idx_angular_ket, idx_angular_bra}),
            2,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("ninv_scaled_expts"),
                        bra,
                        std::string("ninv_scaled_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket}),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            zero_geometrical,
                            inc_angular_ket,
                            dec_angular_bra
                        })
                    )
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("neg_inv2_expts"),
                        bra,
                        std::string("negative_inv2_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            inc_geometrical,
                            zero_angular_ket,
                            dec_angular_bra
                        })
                    )
                )
            )
        ));
        /* Assign the recurrence relation of HGTO on ket centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_geometrical, idx_angular_ket}),
            1,
            IndexCompileOption::Recursion,
            std::make_shared<RecurSubtraction>(
                std::make_shared<RecurSubtraction>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurCartesianVec>(
                            std::string("ket_to_cc"),
                            bra,
                            std::string("ket_to_centre_of_charge"),
                            std::set<std::shared_ptr<RecurSymbol>>({ket}),
                            RecurDirection::X,
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                        ),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                zero_geometrical,
                                dec_angular_ket
                            })
                        )
                    ),
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurIdxOrder>(idx_angular_ket),
                            std::make_shared<RecurScalarVec>(
                                std::string("scaled_inv2_texpts"),
                                bra,
                                std::string("scaled_inv2_total_exponents"),
                                std::set<std::shared_ptr<RecurSymbol>>({ket}),
                                std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                            )
                        ),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                zero_geometrical,
                                dec2_angular_ket
                            })
                        )
                    )
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("inv2_total_expts"),
                        bra,
                        std::string("inv2_total_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>({ket}),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            inc_geometrical,
                            dec_angular_ket
                        })
                    )
                )
            )
        ));

        /* Assign the recurrence relation of geometrical derivatives with respect to potential centre */
        switch (oper->get_power()) {
            case 0:
                expressions.push_back(RecurExpression(
                    std::vector<std::shared_ptr<RecurIndex>>({idx_geometrical}),
                    0,
                    IndexCompileOption::Recursion,
                    std::make_shared<RecurMultiplication>(
                        ,
                        std::make_shared<RecurParentheses>(
                            std::make_shared<RecurSubtraction>(
                                std::make_shared<RecurMultiplication>(
                                    std::make_shared<RecurCartesianVec>(
                                        std::string("oper_to_cc"),
                                        bra,
                                        std::string("to_centre_of_charge"),
                                        std::set<std::shared_ptr<RecurSymbol>>({
                                            ket,
                                            std::make_shared<RecurCartesianVar>(
                                                std::string("oper_centre"),
                                                oper,
                                                std::string("get_centre"),
                                                std::set<std::shared_ptr<RecurSymbol>>(),
                                                RecurDirection::X
                                            )
                                        }),
                                        RecurDirection::X,
                                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket, idx_exponent_bra})
                                    ),
                                ),
                                std::make_shared<RecurMultiplication>(
                                    ,
                                )
                            )
                        )
                    )
                ));
                break;
            case -1:
                break;
            /* Power is equal to -2 */
            default:
                break;
        }
        return expressions;
    }

    std::vector<RecurExpression>
    RecurRelation::get_expression(const std::shared_ptr<RecurGaussianFunction>& bra,
                                  const std::shared_ptr<RecurECPNonLocal>& oper,
                                  const std::shared_ptr<RecurGaussianFunction>& ket) noexcept
    {
        /* Recurrence relations of the operator with HGTOs */
        std::vector<RecurExpression> expressions;
        expressions.reserve(5);
        /* Indices involved in the integral */
        auto idx_exponent_bra = bra->get_idx_exponent();
        auto idx_angular_bra = bra->get_idx_angular();
        auto idx_exponent_ket = ket->get_idx_exponent();
        auto idx_angular_ket = ket->get_idx_angular();
        auto idx_radial = oper->get_idx_radial();
        auto idx_bessel_bra = oper->get_idx_bessel_bra();
        auto idx_bessel_ket = oper->get_idx_bessel_ket();
        auto idx_harmonic_bra = oper->get_idx_harmonic_bra();
        auto idx_harmonic_ket = oper->get_idx_harmonic_ket();
        auto idx_geometrical = oper->get_idx_geometrical();
        /* Increments and decrements of indices */
        auto zero_angular_bra = idx_angular_bra->get_copy();
        zero_angular_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_bra = idx_angular_bra->get_copy();
        dec_angular_bra->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_bra = idx_angular_bra->get_copy();
        dec2_angular_bra->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_bra = idx_angular_bra->get_copy();
        inc_angular_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_angular_ket = idx_angular_ket->get_copy();
        zero_angular_ket->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_angular_ket = idx_angular_ket->get_copy();
        dec_angular_ket->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_angular_ket = idx_angular_ket->get_copy();
        dec2_angular_ket->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_angular_ket = idx_angular_ket->get_copy();
        inc_angular_ket->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto dec_geometrical = idx_geometrical->get_copy();
        dec_geometrical->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto zero_harmonic_bra = idx_harmonic_bra->get_copy();
        zero_harmonic_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_harmonic_bra = idx_harmonic_bra->get_copy();
        dec_harmonic_bra->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_harmonic_bra = idx_harmonic_bra->get_copy();
        dec2_harmonic_bra->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_harmonic_bra = idx_harmonic_bra->get_copy();
        inc_harmonic_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_harmonic_ket = idx_harmonic_ket->get_copy();
        zero_harmonic_ket->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto dec_harmonic_ket = idx_harmonic_ket->get_copy();
        dec_harmonic_ket->set_order(RecurVector(std::array<int,3>({-1,0,0})));
        auto dec2_harmonic_ket = idx_harmonic_ket->get_copy();
        dec2_harmonic_ket->set_order(RecurVector(std::array<int,3>({-2,0,0})));
        auto inc_harmonic_ket = idx_harmonic_ket->get_copy();
        inc_harmonic_ket->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_bessel_bra = idx_bessel_bra->get_copy();
        zero_bessel_bra->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto inc_bessel_bra = idx_bessel_bra->get_copy();
        inc_bessel_bra->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_bessel_ket = idx_bessel_ket->get_copy();
        zero_bessel_ket->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto inc_bessel_ket = idx_bessel_ket->get_copy();
        inc_bessel_ket->set_order(RecurVector(std::array<int,3>({1,0,0})));
        auto zero_radial = idx_radial->get_copy();
        zero_radial->set_order(RecurVector(std::array<int,3>({0,0,0})));
        auto inc_radial = idx_radial->get_copy();
        inc_radial->set_order(RecurVector(std::array<int,3>({1,0,0})));
        /* Assign the recurrence relation of geometrical derivatives with respect to ECP centre */
        expressions.push_back(RecurExpression(
            /* Most consecutive index comes last, indices regarding exponents
               do not need to be involved in the recurrence relation code
               generation because they are represented by std::valarray */
            std::vector<std::shared_ptr<RecurIndex>>({idx_geometrical, idx_angular_ket, idx_angular_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("bra_neg2_expts"),
                        bra,
                        std::string("negative_double_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            dec_geometrical,
                            zero_angular_ket,
                            inc_angular_bra
                        })
                    )
                ),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("ket_neg2_expts"),
                        ket,
                        std::string("negative_double_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            dec_geometrical,
                            inc_angular_ket,
                            zero_angular_bra
                        })
                    )
                )
            )
        ));
        /* Assign the recurrence relation of HGTO on ket centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({
                idx_angular_ket, idx_harmonic_ket, idx_bessel_ket, idx_radial
            }),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("ket_inv2_expts"),
                        ket,
                        std::string("inv2_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            dec_angular_ket,
                            inc_harmonic_ket,
                            zero_bessel_ket,
                            zero_radial
                        })
                    )
                ),
                std::make_shared<RecurAddition>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurCartesianVec>(
                            std::string("ket_ecp_scaled"),
                            ket,
                            std::string("from_centre_scaled"),
                            std::set<std::shared_ptr<RecurSymbol>>({
                                std::make_shared<RecurCartesianVar>(
                                    std::string("oper_centre"),
                                    oper,
                                    std::string("get_centre"),
                                    std::set<std::shared_ptr<RecurSymbol>>(),
                                    RecurDirection::X
                                )
                            }),
                            RecurDirection::X,
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket})
                        ),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                dec_angular_ket,
                                zero_harmonic_ket,
                                inc_bessel_ket,
                                inc_radial
                            })
                        )
                    ),
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurIdxOrder>(idx_angular_ket),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                dec2_angular_ket,
                                zero_harmonic_ket,
                                inc_bessel_ket,
                                inc_radial
                            })
                        )
                    )
                )
            )
        ));
        /* Assign the recurrence relation of HGTO on bra centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({
                idx_angular_bra, idx_harmonic_bra, idx_bessel_bra, idx_radial
            }),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurAddition>(
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("bra_inv2_expts"),
                        bra,
                        std::string("inv2_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_bra})
                    ),
                    std::make_shared<RecurTerm>(
                        std::vector<std::shared_ptr<RecurIndex>>({
                            dec_angular_bra,
                            inc_harmonic_bra,
                            zero_bessel_bra,
                            zero_radial
                        })
                    )
                ),
                std::make_shared<RecurAddition>(
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurCartesianVec>(
                            std::string("bra_ecp_scaled"),
                            bra,
                            std::string("from_centre_scaled"),
                            std::set<std::shared_ptr<RecurSymbol>>({
                                std::make_shared<RecurCartesianVar>(
                                    std::string("oper_centre"),
                                    oper,
                                    std::string("get_centre"),
                                    std::set<std::shared_ptr<RecurSymbol>>(),
                                    RecurDirection::X
                                )
                            }),
                            RecurDirection::X,
                            std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_bra})
                        ),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                dec_angular_bra,
                                zero_harmonic_bra,
                                inc_bessel_bra,
                                inc_radial
                            })
                        )
                    ),
                    std::make_shared<RecurMultiplication>(
                        std::make_shared<RecurIdxOrder>(idx_angular_bra),
                        std::make_shared<RecurTerm>(
                            std::vector<std::shared_ptr<RecurIndex>>({
                                dec2_angular_bra,
                                zero_harmonic_bra,
                                inc_bessel_bra,
                                inc_radial
                            })
                        )
                    )
                )
            )
        ));
        /* Assign the recurrence relation of Hermite Gaussian transformed from
           real spherical harmonics on ket centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_harmonic_ket}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurMultiplication>(
                std::make_shared<RecurNumber>(2.0),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("ket_expts"),
                        ket,
                        std::string("get_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_ket})
                    ),
                    std::make_shared<RecurSubtraction>(
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurCartesianVar>(
                                std::string("ket_to_ecp"),
                                ket,
                                std::string("to_centre"),
                                std::set<std::shared_ptr<RecurSymbol>>({
                                    std::make_shared<RecurCartesianVar>(
                                        std::string("oper_centre"),
                                        oper,
                                        std::string("get_centre"),
                                        std::set<std::shared_ptr<RecurSymbol>>(),
                                        RecurDirection::X
                                    )
                                }),
                                RecurDirection::X
                            ),
                            std::make_shared<RecurTerm>(
                                std::vector<std::shared_ptr<RecurIndex>>({dec_harmonic_ket})
                            )
                        ),
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurIdxOrder>(idx_harmonic_ket),
                            std::make_shared<RecurTerm>(
                                std::vector<std::shared_ptr<RecurIndex>>({dec2_harmonic_ket})
                            )
                        )
                    )
                )
            )
        ));
        /* Assign the recurrence relation of Hermite Gaussian transformed from
           real spherical harmonics on bra centre */
        expressions.push_back(RecurExpression(
            std::vector<std::shared_ptr<RecurIndex>>({idx_harmonic_bra}),
            0,
            IndexCompileOption::Recursion,
            std::make_shared<RecurMultiplication>(
                std::make_shared<RecurNumber>(2.0),
                std::make_shared<RecurMultiplication>(
                    std::make_shared<RecurScalarVec>(
                        std::string("bra_expts"),
                        bra,
                        std::string("get_exponents"),
                        std::set<std::shared_ptr<RecurSymbol>>(),
                        std::vector<std::shared_ptr<RecurIndex>>({idx_exponent_bra})
                    ),
                    std::make_shared<RecurSubtraction>(
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurCartesianVar>(
                                std::string("bra_to_ecp"),
                                bra,
                                std::string("to_centre"),
                                std::set<std::shared_ptr<RecurSymbol>>({
                                    std::make_shared<RecurCartesianVar>(
                                        std::string("oper_centre"),
                                        oper,
                                        std::string("get_centre"),
                                        std::set<std::shared_ptr<RecurSymbol>>(),
                                        RecurDirection::X
                                    )
                                }),
                                RecurDirection::X
                            ),
                            std::make_shared<RecurTerm>(
                                std::vector<std::shared_ptr<RecurIndex>>({dec_harmonic_bra})
                            )
                        ),
                        std::make_shared<RecurMultiplication>(
                            std::make_shared<RecurIdxOrder>(idx_harmonic_bra),
                            std::make_shared<RecurTerm>(
                                std::vector<std::shared_ptr<RecurIndex>>({dec2_harmonic_bra})
                            )
                        )
                    )
                )
            )
        ));
        return expressions;
    }
}
