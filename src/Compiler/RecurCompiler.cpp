/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the recurrence relation compiler.

   2020-09-05, Bin Gao:
   * add extra loop between the current index and its inner index

   2019-06-06, Bin Gao:
   * separate programming language specific parts from logical control parts
     (convert recurrence relations into loops), the former parts are put into
     individual virtual member functions, which can be overridden by derived
     classes

   2019-05-29, Bin Gao:
   * built on top of RecurGenerator

   2018-03-21, Bin Gao:
   * first version
*/

#include <iterator>

#include "tIntegral/Compiler/RecurConverter.hpp"
#include "tIntegral/Compiler/RecurCompiler.hpp"

namespace tIntegral
{
    bool RecurCompiler::build_integration_class(const std::shared_ptr<RecurToken>

            const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                                std::vector<RecurExpression>& expressions,
                                                const std::string& nameIntegration,
                                                const std::vector<std::string>& tparameters,
                                                const std::string& descripIntegration,
                                                const bool evalIntegral,
                                                const bool doQuadrature) noexcept
    {
        /* Prepare private members of the integration class */
        std::vector<std::tuple<std::string,std::string,std::string,bool>> integration_members;
        for (auto const& each_function: integrand) {
            integration_members.push_back(
                std::make_tuple(m_translator->recur_function_type(each_function->type_name()),
                                m_translator->member_object(each_function->get_name()),
                                each_function->get_name(),
                                true)
            );
        }
        integration_members.push_back(std::make_tuple(m_translator->recur_index_vector(),
                                                      m_translator->member_object(m_translator->all_indices_name()),
                                                      std::string(),
                                                      false));
        integration_members.push_back(std::make_tuple(m_translator->recur_buffer_type(),
                                                      m_translator->recur_buffer_object(),
                                                      std::string(),
                                                      false));
        /* Each recurrence expression needs a RecurArray */
        integration_members.push_back(std::make_tuple(m_translator->recur_relation_type(expressions.size()),
                                                      m_translator->recur_relation_object(),
                                                      std::string(),
                                                      false));
        /* Quadrature function */
        if (doQuadrature) {
            integration_members.push_back(std::make_tuple(m_translator->type_quadrature(),
                                                          m_translator->member_object(m_name_quadrature),
                                                          m_name_quadrature,
                                                          true));
            integration_members.push_back(std::make_tuple(m_translator->type_floating_point(),
                                                          m_translator->member_object(m_name_quad_abscissa),
                                                          std::string(),
                                                          false));
        }
        /* Write a RecurIntegration derived class */
        m_translator->begin_class(nameIntegration,
                                  tparameters,
                                  integration_members,
                                  descripIntegration,
                                  m_translator->recur_integration_type());

        /* Write the function for the top-down procedure */
        if (!m_topdown_render(expressions, nameIntegration, m_translator)) {
            Settings::logger->write(
                tGlueCore::MessageType::Error,
                "tIntegral::RecurCompiler::build_integration_class() called for the regular integration class"
            );
            return false;
        }
        /* Write the function for the bottom-up procedure */
        if (!build_bottom_up(integrand, expressions, nameIntegration, evalIntegral, doQuadrature)) {
            Settings::logger->write(
                tGlueCore::MessageType::Error,
                "tIntegral::RecurCompiler::build_integration_class() called for the regular integration class"
            );
            return false;
        }
        /* Declare customer-implemented top-down and bottom-up functions */
        declare_customer_function(expressions);
        return true;
    }



    {
        /* Write the function for getting input nodes */
        m_translator->begin_function(
            m_translator->input_node_function(),
            m_translator->recur_array_input_type(),
            std::vector<std::string>({
                m_translator->make_declaration(m_translator->recur_index_type(),
                                               std::string("index"),
                                               false,
                                               RecurTypeSpecifier::Const,
                                               RecurDeclarator::LvalueReference)
            }),
            std::string("Get input nodes for the integration and sorted according to the order of a given index")
        );
        m_translator->return_statement(
            m_translator->call_function(m_translator->recur_array_object(expressions.size()-1),
                                        m_translator->input_node_function(),
                                        std::vector<std::string>({std::string("index")}))
        );
        m_translator->end_function();
        return true;
    }

    bool RecurCompiler::build_bottom_up(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                        std::vector<RecurExpression>& expressions,
                                        const std::string& nameIntegration,
                                        const bool evalIntegral,
                                        const bool doQuadrature) noexcept
    {
        m_translator->begin_function(m_translator->bottom_up_function(),
                                     m_translator->type_boolean(),
                                     std::vector<std::string>(),
                                     std::string("Bottom-up procedure"));
        /* Check the buffer */
        m_translator->begin_if_statement(
            m_translator->call_function(m_translator->recur_buffer_object(),
                                        std::string("empty"),
                                        std::vector<std::string>(),
                                        RecurOperandType::PointerOperand)
        );
        m_translator->handle_error(std::vector<std::string>({
            m_translator->string_literal(
                m_translator->class_member(nameIntegration, m_translator->bottom_up_function())+" called with empty buffer"
            )
        }));
        m_translator->end_elif_statement();
        /* Perform quadrature */
        if (doQuadrature) {
            m_translator->make_quadrature_call(m_translator->member_object(m_name_quadrature),
                                               m_name_quad_integrand);
        }
        else {
            /* Call the customer-implemented function for evaluating integrals
               after all recurrence expressions */
            if (evalIntegral) {
                m_translator->call_function_statement(m_translator->eval_function(), std::vector<std::string>());
            }
            /* Call the bottom-up procedure of each recurrence expression */
            for (auto each_expression=expressions.crbegin(); each_expression!=expressions.crend(); ++each_expression) {
                m_translator->call_function_statement(
                    m_translator->idx_recur_function(each_expression->get_output_index()->get_name(), false),
                    std::vector<std::string>()
                );
            }
            m_translator->return_statement(m_translator->boolean_literal());
        }
        m_translator->end_function();
        m_translator->begin_protected_section();
        /* Write the function for the evaluation at abscissa */
        if (doQuadrature) {
            m_translator->begin_function(m_name_quad_integrand,
                                         m_translator->type_void(),
                                         std::vector<std::string>({
                                             m_translator->make_declaration(m_translator->type_floating_point(),
                                                                            m_name_quad_abscissa,
                                                                            false,
                                                                            RecurTypeSpecifier::Const)
                                         }),
                                         "Function for the evaluation at abscissa",
                                         RecurIntegrandSpecifier::MemberFunction);
            m_translator->assignment_statement(m_translator->member_object(m_name_quad_abscissa),
                                               RecurAssignment::BasicAssignment,
                                               m_name_quad_abscissa);
            /* Call the customer-implemented function for evaluating integrals
               after all recurrence expressions */
            if (evalIntegral) {
                m_translator->call_function_statement(m_translator->eval_function(), std::vector<std::string>());
            }
            /* Call the bottom-up procedure of each recurrence expression */
            for (auto each_expression=expressions.crbegin(); each_expression!=expressions.crend(); ++each_expression) {
                m_translator->call_function_statement(
                    m_translator->idx_recur_function(each_expression->get_output_index()->get_name(), false),
                    std::vector<std::string>()
                );
            }
            m_translator->end_function();
        }
        /* Write the function for each recurrence expression */
        for (m_expression=expressions.begin(); m_expression!=expressions.end(); ++m_expression) {
            /* Name of the current function */
            m_name_fexpression = m_translator->class_member(
                nameIntegration,
                m_translator->idx_recur_function(m_expression->get_output_index()->get_name(), false)
            );
            switch (m_expression->get_output_option()) {
                case IndexCompileOption::Recursion:
                    /* Write the bottom-up function of the current recurrence
                       relation */
                    m_translator->begin_function(
                        m_translator->idx_recur_function(m_expression->get_output_index()->get_name(), false),
                        m_translator->type_void(),
                        std::vector<std::string>(),
                        "Recurrence relation of "+m_expression->get_output_index()->to_string(),
                        RecurIntegrandSpecifier::MemberFunction
                    );
                    /* Get the analyzer of the current recurrence relation */
                    m_analyzer = m_expression->get_analyzer();
                    /* Declare variables for the current recurrence relation */
                    if (!m_var_processor.build(integrand, m_analyzer->get_variables())) {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "tIntegral::RecurCompiler::build_bottom_up() called for ",
                                                std::distance(expressions.begin(), m_expression),
                                                "-th recurrence relation ",
                                                m_expression->to_string());
                        return false;
                    }
                    /* Loop over LHS nodes of the current recurrence relation */
                    begin_lhs_loop(std::distance(expressions.begin(), m_expression));
                    /* Declare variables related to the current LHS node */
                    declare_lhs_var();
                    /* Declare variables related to RHS nodes */
                    declare_rhs_var();
                    /* Call recursive function to generate source code of loops over
                       different indices of the current LHS node and the recurrence
                       relation */
                    m_expression->reset_index();
                    /* Intializes indices with necessarily zeroth orders */
                    m_zero_orders.clear();
                    for (auto& each_index: m_expression->get_indices()) {
                        m_zero_orders.push_back(
                            std::make_pair(each_index, std::array<bool,3>({false, false, false}))
                        );
                    }
                    if (to_loops()) {
                        end_lhs_loop(std::distance(expressions.begin(), m_expression));
                        m_translator->end_function();
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "tIntegral::RecurCompiler::build_bottom_up() called for ",
                                                std::distance(expressions.begin(), m_expression),
                                                "-th recurrence relation ",
                                                m_expression->to_string());
                        return false;
                    }
                    break;
                case IndexCompileOption::Transfer:
                    //FIXME: to implement
                    m_translator->handle_error(std::vector<std::string>({
                        m_translator->string_literal("IndexCompileOption::Transfer not implemented yet!")
                    }));
                    break;
                case IndexCompileOption::Customer:
                    break;
                case IndexCompileOption::Skip:
                    break;
                default:
                    m_translator->handle_error(std::vector<std::string>({
                        m_translator->string_literal(
                            stringify_compile_option(m_expression->get_output_option())+" not implemented yet!"
                        )
                    }));
                    break;
            }
        }
        /* Declare the function of evaluating integrals after all recurrence expressions performed */
        if (evalIntegral) {
            m_translator->declare_function(
                m_translator->eval_function(),
                m_translator->type_void(),
                std::vector<std::string>(),
                "Customer evaluation function after all recurrence expressions performed and using "
                    + m_translator->recur_array_object(expressions.size()-1),
                RecurIntegrandSpecifier::MemberFunction
            );
        }
        return true;
    }

    //FIXME: auto rhs_decrements = m_analyzer->get_trans_decrements(index);
    //FIXME: if (rhs_decrements[irhs][ixyz].get_norm()<=0) continue;
    //FIXME: only rhs_decrements.get_norm()>0 computes idx_size
    void RecurCompiler::declare_lhs_var() noexcept
    {
        m_translator->assignment_statement(m_translator->lhs_node_offset(true),
                                           RecurAssignment::BasicAssignment,
                                           m_translator->lhs_get_offset());
        m_translator->assignment_statement(m_translator->lhs_node_value(true, false, false, m_vector_wise),
                                           RecurAssignment::BasicAssignment,
                                           m_translator->arithmetic_operation(
                                               m_translator->recur_buffer_get(),
                                               RecurArithmetic::Addition,
                                               m_translator->lhs_node_offset()
                                           ));
        auto output_index = m_expression->get_output_index();
        /* Get orders of indices of the current LHS node */
        for (auto const& each_index: m_expression->get_indices()) {
            /* For output index, the order is RHS nodes with the
               decrement -x, -y or -z */
            if (each_index->equal_to(output_index)) {
                m_translator->assignment_statement(
                    m_translator->index_order(each_index->get_name(), RecurDirection::XYZ, true),
                    RecurAssignment::BasicAssignment,
                    m_translator->arithmetic_operation(
                        m_translator->lhs_get_order(
                            m_translator->member_object(m_translator->all_indices_name()),
                            each_index->get_position()
                        ),
                        RecurArithmetic::Subtraction,
                        std::string("1")
                    )
                );
            }
            else {
                m_translator->assignment_statement(
                    m_translator->index_order(each_index->get_name(), RecurDirection::XYZ, true),
                    RecurAssignment::BasicAssignment,
                    m_translator->lhs_get_order(
                        m_translator->member_object(m_translator->all_indices_name()),
                        each_index->get_position()
                    )
                );
            }
        }
    }

    void RecurCompiler::declare_rhs_var() noexcept
    {
        m_translator->assignment_statement(m_translator->rhs_nodes_object(true),
                                           RecurAssignment::BasicAssignment,
                                           m_translator->lhs_get_rhs());
        for (unsigned int irhs=0; irhs<m_analyzer->get_num_rhs(); ++irhs) {
            /* For each RHS node, we declare three pointers to its
               value along x, y and z directions */
            m_translator->assignment_statement(
                m_translator->rhs_node_offset(irhs, RecurDirection::XYZ, true),
                RecurAssignment::BasicAssignment,
                std::string("0")
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs, RecurDirection::X, true, false, false, m_vector_wise),
                RecurAssignment::BasicAssignment,
                m_translator->null_pointer()
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs, RecurDirection::Y, true, false, false, m_vector_wise),
                RecurAssignment::BasicAssignment,
                m_translator->null_pointer()
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs, RecurDirection::Z, true, false, false, m_vector_wise),
                RecurAssignment::BasicAssignment,
                m_translator->null_pointer()
            );
        }
        for (unsigned int irhs=0; irhs<m_analyzer->get_num_rhs(); ++irhs) {
            m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
            m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
            m_translator->handle_debug(std::vector<std::string>({
                m_translator->string_literal(m_name_fexpression+" gets RHS node["+std::to_string(irhs)+"] "),
                m_translator->call_function(m_translator->rhs_nodes_at(irhs),
                                            std::string("to_string"),
                                            std::vector<std::string>({m_translator->recur_buffer_object()}),
                                            RecurOperandType::PointerOperand)
            }));
            m_translator->endif_directive();
            m_translator->assignment_statement(
                m_translator->rhs_node_offset(irhs),
                RecurAssignment::BasicAssignment,
                m_translator->rhs_get_offset(irhs)
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs),
                RecurAssignment::BasicAssignment,
                m_translator->arithmetic_operation(m_translator->recur_buffer_get(),
                                                   RecurArithmetic::Addition,
                                                   m_translator->rhs_node_offset(irhs))
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs, RecurDirection::Y),
                RecurAssignment::BasicAssignment,
                m_translator->rhs_node_value(irhs)
            );
            m_translator->assignment_statement(
                m_translator->rhs_node_value(irhs, RecurDirection::Z),
                RecurAssignment::BasicAssignment,
                m_translator->rhs_node_value(irhs)
            );
            m_translator->end_elif_statement();
        }
    }

    bool RecurCompiler::to_loops() noexcept
    {
        /* Get current index */
        auto current_index = m_expression->get_index();
        /* Take care of valid index */
        if (current_index) {
#if defined(TINTEGRAL_DEBUG)
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "tIntegral::RecurCompiler::to_loops() will process the index ",
                                    current_index->to_string());
#endif
            /* Initialize information of RHS nodes and the direction of recurrence
               relation when we encounter the first index */
            if (m_expression->is_first_index()) {
#if defined(TINTEGRAL_DEBUG)
                Settings::logger->write(
                    tGlueCore::MessageType::Debug,
                    "tIntegral::RecurCompiler::to_loops() will initialize for the first index with the number of RHS terms ",
                    m_analyzer->get_num_rhs()
                );
#endif
                if (!m_analyzer->reset_rhs_directions()) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "tIntegral::RecurCompiler::to_loops() called for the first index ",
                                            current_index->to_string(),
                                            ", and of the expression ",
                                            m_expression->to_string());
                }
                /* Important to set the recurrence relation along all x, y and
                   z directions so that pointers to RHS terms can be updated
                   correctly for outer indices */
                m_recur_direction = RecurDirection::XYZ;
            }
            /* Write debugging information before loops */
            m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
            m_translator->handle_debug(std::vector<std::string>({
                m_expression->is_output_index()
                    ? m_translator->string_literal(m_name_fexpression+" begins loops of the output index ")
                    : m_translator->string_literal(m_name_fexpression+" begins loops of the index "),
                m_translator->call_function(
                    m_translator->subscript_operation(
                        m_translator->member_object(m_translator->all_indices_name()), current_index->get_position()
                    ),
                    std::string("to_string"),
                    std::vector<std::string>(),
                    RecurOperandType::PointerOperand
                )
            }));
            m_translator->endif_directive();
            /* Get the loop category of the current index */
            auto loop_category = m_analyzer->get_loop_category(current_index);
            /* Generate codes according to the index class */
            if (current_index->type_id()==typeid(RecurTriangularIndex)) {
                if (!loop_category.is_valid()) {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurCompiler::to_loops() did not find the loop category of the index ",
                        current_index->to_string()
                    );
                    return false;
                }
                /* Output index of the current LHS node */
                if (m_expression->is_output_index()) {
                    /* Get the converter for the output index */
                    auto converter = m_converter_factory->make_output_converter(
                        m_name_fexpression, loop_category, m_analyzer
                    );
                    if (converter.size()==0) {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::to_loops() did not find a converter of loop category ",
                            loop_category.to_string(),
                            " for the output index ",
                            current_index->to_string()
                        );
                        return false;
                    }
                    /* For different recurrence relation directions */
                    for (unsigned int isnippet=0; isnippet<converter.size(); ++isnippet) {
                        m_recur_direction = converter[isnippet].first;
                        m_translator->ifdef_directive(std::string("TINTEGRAL_DEBUG"));
                        m_translator->handle_debug(std::vector<std::string>({
                            m_translator->string_literal(
                                m_name_fexpression
                                +" performs the recurrence relation along "
                                +stringify_direction(m_recur_direction)
                                +" direction"
                            )
                        }));
                        m_translator->endif_directive();
                        /* Build codes for different loop components */
                        if (!invoke_snippeter(converter[isnippet].second)) {
                            Settings::logger->write(
                                tGlueCore::MessageType::Error,
                                "tIntegral::RecurCompiler::to_loops() called with the direction ",
                                converter[isnippet].first,
                                " of the loop category ",
                                loop_category.to_string(),
                                " for the output index ",
                                current_index->to_string()
                            );
                            return false;
                        }
                    }
                }
                /* Inner and outer indices of the LHS node */
                else {
                    /* Get the converter for the current index */
                    auto converter = m_converter_factory->make_non_output_converter(
                        m_name_fexpression, loop_category, m_recur_direction, m_analyzer
                    );
                    if (converter.size()==0) {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::to_loops() did not find a converter of loop category ",
                            loop_category.to_string(),
                            " for the index ",
                            current_index->to_string()
                        );
                        return false;
                    }
                    if (converter.back().first!=-1) {
                        Settings::logger->write(
                            tGlueCore::MessageType::Error,
                            "tIntegral::RecurCompiler::to_loops() has an invalid converter ",
                            converter.back().first,
                            " of loop category ",
                            loop_category.to_string(),
                            " for the index ",
                            current_index->to_string()
                        );
                        return false;
                    }
                    /* For different switch cases */
                    if (converter.size()>1) m_translator->switch_index_order(current_index->get_name());
                    for (unsigned int isnippet=0; isnippet<converter.size(); ++isnippet) {
                        if (converter.size()>1) {
                            if (converter[isnippet].first==-1) {
                                m_translator->case_default();
                            }
                            else {
                                m_translator->case_index_order(converter[isnippet].first);
                            }
                        }
                        /* Build codes for different loop components */
                        if (!invoke_snippeter(converter[isnippet].second)) {
                            Settings::logger->write(
                                tGlueCore::MessageType::Error,
                                "tIntegral::RecurCompiler::to_loops() called with order ",
                                converter[isnippet].first,
                                " of the loop category ",
                                loop_category.to_string(),
                                " for the index ",
                                current_index->to_string()
                            );
                            return false;
                        }
                        if (converter.size()>1) m_translator->end_case();
                    }
                    if (converter.size()>1) m_translator->end_switch();
                }
            }
            else if (current_index->type_id()==typeid(RecurVectorIndex)) {
                /* Write a for loop from the 0th order */
                m_translator->begin_loop_component(
                    current_index->get_name(),
                    m_triangle_traversal[0],
                    0,
                    RecurComparison::LessEqual,
                    RecurDirection::XYZ,
                    RecurDirection::Null,
                    0,
                    std::vector<std::pair<RecurDirection,RecurIncDec>>()
                );
                /* Write extra loop between the current index and its inner index */
                write_stride_loop();
                /* Proceed the next index of the current LHS node */
                m_expression->next_index();
                if (to_loops()) {
                    m_expression->prev_index();
                    m_translator->end_for_loop();
                    /* End extra loop between the current index and its inner index */
                    if (m_expression->is_stride_needed()) m_translator->end_for_loop();
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurCompiler::to_loops() called with a vector index ",
                        current_index->to_string(),
                        " of loop category ",
                        std::to_string(loop_category[0])
                    );
                    return false;
                }
            }
            else if (current_index->type_id()==typeid(RecurScalarIndex)) {
                /* Write extra loop between the current index and its inner index */
                write_stride_loop();
                /* Proceed the next index of the current LHS node */
                m_expression->next_index();
                if (to_loops()) {
                    m_expression->prev_index();
                    /* End extra loop between the current index and its inner index */
                    if (m_expression->is_stride_needed()) m_translator->end_for_loop();
                }
                else {
                    Settings::logger->write(
                        tGlueCore::MessageType::Error,
                        "tIntegral::RecurCompiler::to_loops() called with a scalar index ",
                        current_index->to_string(),
                        " of loop category ",
                        std::to_string(loop_category[0])
                    );
                    return false;
                }
            }
            else {
                Settings::logger->write(
                    tGlueCore::MessageType::Error,
                    "tIntegral::RecurCompiler::to_loops() has not implemented for the index ",
                    current_index->to_string()
                );
                return false;
            }
        }
        /* We have taken care all indices and now write code for performing the
           recurrence relation */
        else {
#if defined(TINTEGRAL_DEBUG)
            Settings::logger->write(
                tGlueCore::MessageType::Debug,
                "tIntegral::RecurCompiler::to_loops() will write code for performing the recurrence relation"
            );
#endif
            auto contribut_rhs = m_generator.build(m_name_fexpression,
                                                   m_recur_direction,
                                                   m_expression->get_rhs(),
                                                   m_analyzer->get_rhs_directions(),
                                                   m_zero_orders,
                                                   m_vector_wise);
            if (contribut_rhs.empty()) {
                Settings::logger->write(
                    tGlueCore::MessageType::Error,
                    "tIntegral::RecurCompiler::to_loops() called along the direction ",
                    stringify_direction(m_recur_direction)
                );
                return false;
            }
            /* Update pointers to values of the LHS and RHS nodes */
            update_node_values(contribut_rhs);
        }
        return true;
    }

    void RecurCompiler::update_node_values(const std::vector<unsigned int>& contributRHS) noexcept
    {
        m_translator->inc_dec_statement(RecurIncDec::Increment, m_translator->lhs_node_value());
        for (auto const& each_rhs: contributRHS) {
            m_translator->inc_dec_statement(
                RecurIncDec::Increment, m_translator->rhs_node_value(each_rhs, m_recur_direction)
            );
        }
    }
}
