/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements recurrence relation converters.

   2019-06-29, Bin Gao:
   * moved from RecurCompiler
*/

#include "tIntegral/Translator/RecurKeywords.hpp"
#include "tIntegral/Compiler/RecurConverter.hpp"

//FIXME: remove unnecessary assigment and update of RHS
namespace tIntegral
{
    std::array<bool,3> RecurNonOutputConverter::declare_first_corner(const std::shared_ptr<RecurIndex>& index,
                                                                     const int order,
                                                                     const std::array<bool,3>& orderNeeded) noexcept
    {
        RecurVector corner_orders(order, m_triangle_traversal[0]);
        if (orderNeeded[0]) {
            m_translator->assign_component_order(index->get_name(), RecurDirection::X, corner_orders[0], true);
        }
        if (orderNeeded[1]) {
            m_translator->assign_component_order(index->get_name(), RecurDirection::Y, corner_orders[1], true);
        }
        if (orderNeeded[2]) {
            m_translator->assign_component_order(index->get_name(), RecurDirection::Z, corner_orders[2], true);
        }
        auto zero_orders = std::array<bool,3>({true, true, true});
        /* Only the order along the first corner is non-zero */
        if (order!=0) {
            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = false;
        }
        return zero_orders;
    }

    std::array<bool,3> RecurNonOutputConverter::declare_first_corner(const std::shared_ptr<RecurIndex>& index,
                                                                     const std::array<bool,3>& orderNeeded) noexcept
    {
        switch (m_triangle_traversal[0]) {
            case RecurDirection::X:
                if (orderNeeded[0]) {
                    m_translator->assign_component_order(
                        index->get_name(), RecurDirection::X, RecurDirection::XYZ, 0, true
                    );
                }
                if (orderNeeded[1]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::Y, 0, true);
                }
                if (orderNeeded[2]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::Z, 0, true);
                }
                break;
            case RecurDirection::Y:
                if (orderNeeded[0]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::X, 0, true);
                }
                if (orderNeeded[1]) {
                    m_translator->assign_component_order(
                        index->get_name(), RecurDirection::Y, RecurDirection::XYZ, 0, true
                    );
                }
                if (orderNeeded[2]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::Z, 0, true);
                }
                break;
            default:
                if (orderNeeded[0]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::X, 0, true);
                }
                if (orderNeeded[1]) {
                    m_translator->assign_component_order(index->get_name(), RecurDirection::Y, 0, true);
                }
                if (orderNeeded[2]) {
                    m_translator->assign_component_order(
                        index->get_name(), RecurDirection::Z, RecurDirection::XYZ, 0, true
                    );
                }
                break;
        }
        auto zero_orders = std::array<bool,3>({true, true, true});
        /* Only the order along the first corner is non-zero */
        zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = false;
        return zero_orders;
    }

    void RecurNonOutputConverter::update_rhs_values(const std::shared_ptr<RecurIndex>& index,
                                                    const bool traverseTriangle) noexcept
    {
        m_idx_stepsizes.push_back(std::make_pair(index, traverseTriangle));
        write_rhs_stepsizes(index, traverseTriangle, true);
    }

    void RecurNonOutputConverter::update_rhs_values() noexcept
    {
        if (!m_idx_stepsizes.empty()) {
            write_rhs_stepsizes(m_idx_stepsizes.back().first,
                                m_idx_stepsizes.back().second,
                                false);
            m_idx_stepsizes.pop_back();
        }
    }

    std::array<bool,3> RecurNonOutputConverter::update_first_edge(const std::shared_ptr<RecurIndex>& index,
                                                                  const std::array<bool,3>& orderNeeded,
                                                                  const bool lastEdge) noexcept
    {
        auto pos_first_corner = tGlueCore::to_integral(m_triangle_traversal[0]);
        if (orderNeeded[pos_first_corner]) {
            m_translator->decrease_component_order(index->get_name(), m_triangle_traversal[0]);
        }
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[1])]) {
            m_translator->inc_dec_statement(
                RecurIncDec::Increment,
                m_translator->index_order(index->get_name(), m_triangle_traversal[1])
            );
        }
        /* Skip components before a row */
        update_rhs_values(index, false);
        auto zero_orders = std::array<bool,3>({false, false, false});
        /* Order along the last corner is zero */
        zero_orders[tGlueCore::to_integral(m_triangle_traversal[2])] = true;
        /* Order along the first corner is zero if on the last edge */
        if (lastEdge) zero_orders[pos_first_corner] = true;
        return zero_orders;
    }

    void RecurNonOutputConverter::update_last_edge(const std::shared_ptr<RecurIndex>& index,
                                                   const std::array<bool,3>& orderNeeded) noexcept
    {
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[1])]) {
            m_translator->decrease_component_order(index->get_name(), m_triangle_traversal[1]);
        }
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[2])]) {
            m_translator->inc_dec_statement(
                RecurIncDec::Increment,
                m_translator->index_order(index->get_name(), m_triangle_traversal[2])
            );
        }
    }

    std::array<bool,3> RecurNonOutputConverter::assign_last_edge(const std::shared_ptr<RecurIndex>& index,
                                                                 const int order,
                                                                 const std::array<bool,3>& orderNeeded,
                                                                 const bool onEdge) noexcept
    {
        auto pos_second_corner = tGlueCore::to_integral(m_triangle_traversal[1]);
        if (orderNeeded[pos_second_corner]) {
            m_translator->assign_component_order(index->get_name(), m_triangle_traversal[1], order);
        }
        auto pos_last_corner = tGlueCore::to_integral(m_triangle_traversal[2]);
        if (orderNeeded[pos_last_corner]) {
            m_translator->assign_component_order(index->get_name(), m_triangle_traversal[2], 0);
        }
        auto pos_first_corner = tGlueCore::to_integral(m_triangle_traversal[0]);
        if (orderNeeded[pos_first_corner]) {
            m_translator->decrease_component_order(index->get_name(), m_triangle_traversal[0]);
        }
        /* Skip components before a row */
        update_rhs_values(index, false);
        auto zero_orders = std::array<bool,3>({false, false, false});
        /* The order along the last corner is zero */
        zero_orders[pos_last_corner] = true;
        /* The order along the second corner can be zero */
        if (order==0) zero_orders[pos_second_corner] = true;
        /* The order along the first corner is zero on the last edge */
        if (onEdge) zero_orders[pos_first_corner] = true;
        return zero_orders;
    }

    std::array<bool,3> RecurNonOutputConverter::assign_last_edge(const std::shared_ptr<RecurIndex>& index,
                                                                 const RecurDirection idxComponent,
                                                                 const int adjustment,
                                                                 const std::array<bool,3>& orderNeeded) noexcept
    {
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[1])]) {
            m_translator->assign_component_order(index->get_name(), m_triangle_traversal[1], idxComponent, adjustment);
        }
        auto pos_last_corner = tGlueCore::to_integral(m_triangle_traversal[2]);
        if (orderNeeded[pos_last_corner]) {
            m_translator->assign_component_order(index->get_name(), m_triangle_traversal[2], 0);
        }
        auto pos_first_corner = tGlueCore::to_integral(m_triangle_traversal[0]);
        if (orderNeeded[pos_first_corner]) {
            m_translator->decrease_component_order(index->get_name(), m_triangle_traversal[0]);
        }
        /* Skip components before a row */
        update_rhs_values(index, false);
        auto zero_orders = std::array<bool,3>({false, false, false});
        /* The order along the last corner is zero */
        zero_orders[pos_last_corner] = true;
        /* The order along the first corner is zero on the last edge */
        if (idxComponent==RecurDirection::XYZ && adjustment==0) zero_orders[pos_first_corner] = true;
        return zero_orders;
    }

    void RecurNonOutputConverter::begin_loop_triangle(const std::shared_ptr<RecurIndex>& index,
                                                      const bool endEdge,
                                                      const std::array<bool,3>& orderNeeded) noexcept
    {
        /* Skip components before loops */
        update_rhs_values(index, true);
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[0])]) {
            m_translator->begin_loop_component(
                index->get_name(),
                m_triangle_traversal[0],
                RecurDirection::XYZ,
                RecurDirection::Null,
                0,
                endEdge ? RecurComparison::GreaterEqual : RecurComparison::GreaterThan,
                0,
                std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                true
            );
        }
        else {
            m_translator->begin_loop_component(
                index->get_name(),
                m_triangle_traversal[3],
                0,
                endEdge ? RecurComparison::LessEqual : RecurComparison::LessThan,
                RecurDirection::XYZ,
                RecurDirection::Null,
                0,
                std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                true
            );
        }
        /* Skip components before a row */
        update_rhs_values(index, false);
        begin_corner_to_corner(index, false, true, 0, 0, orderNeeded, true);
    }

    void RecurNonOutputConverter::end_loop_triangle() noexcept
    {
        /* End a for loop over rows */
        end_loop_component();
        /* Skip components after a row */
        update_rhs_values();
        /* End a for loop over a triangle */
        end_loop_component();
        /* Skip components after the loop over a triangle */
        update_rhs_values();
    }

    std::array<bool,3> RecurNonOutputConverter::begin_corner_to_edge(const std::shared_ptr<RecurIndex>& index,
                                                                     const bool edgeIncluded,
                                                                     const int beginOrder,
                                                                     const int endAdjustment,
                                                                     const std::array<bool,3>& orderNeeded) noexcept
    {
        m_translator->begin_loop_component(
            index->get_name(),
            m_triangle_traversal[3],
            beginOrder,
            edgeIncluded ? RecurComparison::LessEqual : RecurComparison::LessThan,
            RecurDirection::XYZ,
            RecurDirection::Null,
            endAdjustment,
            std::vector<std::pair<RecurDirection,RecurIncDec>>(),
            true
        );
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[0])]) {
            m_translator->decrease_component_order(index->get_name(), m_triangle_traversal[0]);
        }
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[1])]) {
            m_translator->assign_component_order(index->get_name(),
                                                 m_triangle_traversal[1],
                                                 m_triangle_traversal[3],
                                                 0);
        }
        auto pos_last_corner = tGlueCore::to_integral(m_triangle_traversal[2]);
        if (orderNeeded[pos_last_corner]) {
            m_translator->assign_component_order(index->get_name(), m_triangle_traversal[2], 0);
        }
        auto zero_orders = std::array<bool,3>({false, false, false});
        zero_orders[pos_last_corner] = true;
        return zero_orders;
    }

    void RecurNonOutputConverter::write_rhs_stepsizes(const std::shared_ptr<RecurIndex>& index,
                                                      const bool traverseTriangle,
                                                      const bool beforeTraversal) const noexcept
    {
        auto rhs_decrements = m_analyzer->get_trans_decrements(index);
        if (rhs_decrements.empty()) {
            m_translator->handle_error(std::vector<std::string>({
                m_translator->string_literal("Failed to get transformed RHS decrements of index "+index->to_string())
            }));
        }
        else {
            /* Find the position of the index in the recurrence-relation indices */
            auto idx_position = m_analyzer->get_index_position(index, true);
            if (idx_position>=0) {
                /* Loop over RHS terms */
                for (unsigned int irhs=0; irhs<rhs_decrements.size(); ++irhs) {
                    auto idx_stepsize = m_translator->call_function(
                        m_translator->rhs_nodes_at(irhs),
                        std::string("get_stepsize"),
                        std::vector<std::string>({std::to_string(idx_position)}),
                        RecurOperandType::PointerOperand
                    );
                    /* Loop over directions of the recurrence relation */
                    for (unsigned int ixyz=0; ixyz<3; ++ixyz) {
                        /* Skip a RHS term if its norm of decrement <= 0 */
                        if (rhs_decrements[irhs][ixyz].get_norm()<=0) continue;
                        if (is_direction_included(m_triangle_traversal[ixyz], m_recur_direction)) {
                            if (traverseTriangle) {
                                if (beforeTraversal) {
                                    auto max_increment = std::max(rhs_decrements[irhs][ixyz][1],
                                                                  rhs_decrements[irhs][ixyz][2]);
                                    if (max_increment>0) {
                                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                                        /* The stepsize of the first index is 1 */
                                        if (index->get_position()==0) {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                std::to_string(max_increment*(max_increment+1)/2)
                                            );
                                        }
                                        else {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    std::to_string(max_increment*(max_increment+1)/2),
                                                    RecurArithmetic::Multiplication,
                                                    idx_stepsize
                                                )
                                            );
                                        }
                                        m_translator->end_elif_statement();
                                    }
                                }
                                else if (rhs_decrements[irhs][ixyz][0]>0) {
                                    int inc_stepsize = rhs_decrements[irhs][ixyz][0]
                                                     * (rhs_decrements[irhs][ixyz][1]
                                                     + rhs_decrements[irhs][ixyz][2])
                                                     + rhs_decrements[irhs][ixyz][0]
                                                     * (rhs_decrements[irhs][ixyz][0]+3)/2;
                                    if (rhs_decrements[irhs][ixyz][0]>1) {
                                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                                        /* The stepsize of the first index is 1 */
                                        if (index->get_position()==0) {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    m_translator->arithmetic_operation(
                                                        std::to_string(rhs_decrements[irhs][ixyz][0]),
                                                        RecurArithmetic::Multiplication,
                                                        m_translator->index_order(index->get_name())
                                                    ),
                                                    RecurArithmetic::Addition,
                                                    std::to_string(inc_stepsize)
                                                )
                                            );
                                        }
                                        else {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    m_translator->parenthesize_expression(
                                                        m_translator->arithmetic_operation(
                                                            m_translator->arithmetic_operation(
                                                                std::to_string(rhs_decrements[irhs][ixyz][0]),
                                                                RecurArithmetic::Multiplication,
                                                                m_translator->index_order(index->get_name())
                                                            ),
                                                            RecurArithmetic::Addition,
                                                            std::to_string(inc_stepsize)
                                                        )
                                                    ),
                                                    RecurArithmetic::Multiplication,
                                                    idx_stepsize
                                                )
                                            );
                                        }
                                        m_translator->end_elif_statement();
                                    }
                                    else {
                                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                                        /* The stepsize of the first index is 1 */
                                        if (index->get_position()==0) {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    m_translator->index_order(index->get_name()),
                                                    RecurArithmetic::Addition,
                                                    std::to_string(inc_stepsize)
                                                )
                                            );
                                        }
                                        else {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    m_translator->parenthesize_expression(
                                                        m_translator->arithmetic_operation(
                                                            m_translator->index_order(index->get_name()),
                                                            RecurArithmetic::Addition,
                                                            std::to_string(inc_stepsize)
                                                        )
                                                    ),
                                                    RecurArithmetic::Multiplication,
                                                    idx_stepsize
                                                )
                                            );
                                        }
                                        m_translator->end_elif_statement();
                                    }
                                }
                            }
                            else {
                                if (beforeTraversal) {
                                    if (rhs_decrements[irhs][ixyz][2]>0) {
                                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                                        /* The stepsize of the first index is 1 */
                                        if (index->get_position()==0) {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                std::to_string(rhs_decrements[irhs][ixyz][2])
                                            );
                                        }
                                        else {
                                            m_translator->assignment_statement(
                                                m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                                RecurAssignment::AdditionAssignment,
                                                m_translator->arithmetic_operation(
                                                    std::to_string(rhs_decrements[irhs][ixyz][2]),
                                                    RecurArithmetic::Multiplication,
                                                    idx_stepsize
                                                )
                                            );
                                        }
                                        m_translator->end_elif_statement();
                                    }
                                }
                                else if (rhs_decrements[irhs][ixyz][1]>0) {
                                    m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                                    /* The stepsize of the first index is 1 */
                                    if (index->get_position()==0) {
                                        m_translator->assignment_statement(
                                            m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                            RecurAssignment::AdditionAssignment,
                                            std::to_string(rhs_decrements[irhs][ixyz][1])
                                        );
                                    }
                                    else {
                                        m_translator->assignment_statement(
                                            m_translator->rhs_node_value(irhs, m_triangle_traversal[ixyz]),
                                            RecurAssignment::AdditionAssignment,
                                            m_translator->arithmetic_operation(
                                                std::to_string(rhs_decrements[irhs][ixyz][1]),
                                                RecurArithmetic::Multiplication,
                                                idx_stepsize
                                            )
                                        );
                                    }
                                    m_translator->end_elif_statement();
                                }
                            }
                        }
                    }
                }
            }
            else {
                m_translator->handle_error(std::vector<std::string>({
                    m_translator->string_literal("Not found the position of the index "+index->to_string())
                }));
            }
        }
    }

    std::array<bool,3> RecurOutputConverter::declare_last_corner(const std::shared_ptr<RecurIndex>& index,
                                                                 const int order,
                                                                 const std::array<bool,3>& orderNeeded) noexcept
    {
        auto zero_orders = std::array<bool,3>({true, true, true});
        /* This function is called at the end of loops of an index, so we do
           not need to declare zero orders */
        if (order!=0) {
            /* Only the order along the last corner is non-zero */
            auto pos_last_corner = tGlueCore::to_integral(m_triangle_traversal[2]);
            if (orderNeeded[pos_last_corner]) {
                m_translator->assign_component_order(index->get_name(), m_triangle_traversal[2], order, true);
            }
            zero_orders[pos_last_corner] = false;
        }
        return zero_orders;
    }

    std::array<bool,3> RecurOutputConverter::declare_last_corner(const std::shared_ptr<RecurIndex>& index,
                                                                 const std::array<bool,3>& orderNeeded) noexcept
    {
        auto zero_orders = std::array<bool,3>({true, true, true});
        /* Only the order along the last corner is non-zero */
        auto pos_last_corner = tGlueCore::to_integral(m_triangle_traversal[2]);
        if (orderNeeded[pos_last_corner]) {
            m_translator->assign_component_order(
                index->get_name(), m_triangle_traversal[2], RecurDirection::XYZ, 0, true
            );
        }
        zero_orders[pos_last_corner] = false;
        return zero_orders;
    }

    void RecurOutputConverter::begin_loop_triangle(const std::shared_ptr<RecurIndex>& index,
                                                   const bool endEdge,
                                                   const std::array<bool,3>& orderNeeded) noexcept
    {
        if (orderNeeded[tGlueCore::to_integral(m_triangle_traversal[0])]) {
            m_translator->begin_loop_component(
                index->get_name(),
                m_triangle_traversal[0],
                RecurDirection::XYZ,
                RecurDirection::Null,
                0,
                endEdge ? RecurComparison::GreaterEqual : RecurComparison::GreaterThan,
                0,
                std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                true
            );
        }
        else {
            m_translator->begin_loop_component(
                index->get_name(),
                m_triangle_traversal[3],
                0,
                endEdge ? RecurComparison::LessEqual : RecurComparison::LessThan,
                RecurDirection::XYZ,
                RecurDirection::Null,
                0,
                std::vector<std::pair<RecurDirection,RecurIncDec>>(),
                true
            );
        }
        begin_corner_to_corner(index, false, true, 0, 0, orderNeeded, true);
    }

    void RecurOutputConverter::assign_rhs_second() noexcept
    {
        /* Get decrements in RHS terms of the output index */
        auto rhs_decrements = m_analyzer->get_output_decrements();
        for (unsigned int irhs=0; irhs<rhs_decrements.size(); ++irhs) {
            if (rhs_decrements[irhs].equal_to({-1,0,0}) ||
                rhs_decrements[irhs].equal_to({0,-1,0}) ||
                rhs_decrements[irhs].equal_to({0,0,-1}) ||
                rhs_decrements[irhs].equal_to({-1,-1,0}) ||
                rhs_decrements[irhs].equal_to({-1,0,-1}) ||
                rhs_decrements[irhs].equal_to({0,-1,-1}) ||
                rhs_decrements[irhs].equal_to({-1,-1,-1})) {
                m_translator->assignment_statement(
                    m_translator->rhs_node_value(irhs, m_triangle_traversal[1]),
                    RecurAssignment::BasicAssignment,
                    m_translator->rhs_node_value(irhs, m_triangle_traversal[0])
                );
            }
            else if (rhs_decrements[irhs].equal_to({-2,0,0}) ||
                     rhs_decrements[irhs].equal_to({0,-2,0}) ||
                     rhs_decrements[irhs].equal_to({0,0,-2}) ||
                     rhs_decrements[irhs].equal_to({-2,-1,0}) ||
                     rhs_decrements[irhs].equal_to({-2,0,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-2,0}) ||
                     rhs_decrements[irhs].equal_to({-1,0,-2}) ||
                     rhs_decrements[irhs].equal_to({0,-2,-1}) ||
                     rhs_decrements[irhs].equal_to({0,-1,-2}) ||
                     rhs_decrements[irhs].equal_to({-2,-1,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-2,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-1,-2})) {
                /* The stepsize of the first index is 1 */
                if (m_analyzer->get_output_index()->get_position()==0) {
                    m_translator->assignment_statement(
                        m_translator->rhs_node_value(irhs, m_triangle_traversal[1]),
                        RecurAssignment::BasicAssignment,
                        m_translator->arithmetic_operation(
                            m_translator->rhs_node_value(irhs, m_triangle_traversal[0]),
                            RecurArithmetic::Subtraction,
                            m_translator->index_order(m_analyzer->get_output_index()->get_name())
                        )
                    );
                }
                else {
                    auto output_position = m_analyzer->get_output_position(true);
                    if (output_position>=0) {
                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                        m_translator->assignment_statement(
                            m_translator->rhs_node_value(irhs, m_triangle_traversal[1]),
                            RecurAssignment::BasicAssignment,
                            m_translator->arithmetic_operation(
                                m_translator->rhs_node_value(irhs, m_triangle_traversal[0]),
                                RecurArithmetic::Subtraction,
                                m_translator->arithmetic_operation(
                                    m_translator->index_order(m_analyzer->get_output_index()->get_name()),
                                    RecurArithmetic::Multiplication,
                                    m_translator->call_function(
                                        m_translator->rhs_nodes_at(irhs),
                                        std::string("get_stepsize"),
                                        std::vector<std::string>({std::to_string(output_position)}),
                                        RecurOperandType::PointerOperand
                                    )
                                )
                            )
                        );
                        m_translator->end_elif_statement();
                    }
                    else {
                        m_translator->handle_error(std::vector<std::string>({
                            m_translator->string_literal(
                                "Not found the position of the output index "+m_analyzer->get_output_index()->to_string()
                            )
                        }));
                    }
                }
            }
            else {
                m_translator->handle_error(std::vector<std::string>({
                    m_translator->string_literal(
                        "Decrements "+rhs_decrements[irhs].to_string()+" of the output index has not implemented"
                    )
                }));
            }
        }
    }

    void RecurOutputConverter::assign_rhs_last() noexcept
    {
        /* Get decrements in RHS terms of the output index */
        auto rhs_decrements = m_analyzer->get_output_decrements();
        for (unsigned int irhs=0; irhs<rhs_decrements.size(); ++irhs) {
            if (rhs_decrements[irhs].equal_to({-1,0,0}) ||
                rhs_decrements[irhs].equal_to({0,-1,0}) ||
                rhs_decrements[irhs].equal_to({0,0,-1}) ||
                rhs_decrements[irhs].equal_to({-1,-1,0}) ||
                rhs_decrements[irhs].equal_to({-1,0,-1}) ||
                rhs_decrements[irhs].equal_to({0,-1,-1}) ||
                rhs_decrements[irhs].equal_to({-1,-1,-1})) {
                m_translator->assignment_statement(
                    m_translator->rhs_node_value(irhs, m_triangle_traversal[2]),
                    RecurAssignment::BasicAssignment,
                    m_translator->rhs_node_value(irhs, m_triangle_traversal[1])
                );
            }
            else if (rhs_decrements[irhs].equal_to({-2,0,0}) ||
                     rhs_decrements[irhs].equal_to({0,-2,0}) ||
                     rhs_decrements[irhs].equal_to({0,0,-2}) ||
                     rhs_decrements[irhs].equal_to({-2,-1,0}) ||
                     rhs_decrements[irhs].equal_to({-2,0,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-2,0}) ||
                     rhs_decrements[irhs].equal_to({-1,0,-2}) ||
                     rhs_decrements[irhs].equal_to({0,-2,-1}) ||
                     rhs_decrements[irhs].equal_to({0,-1,-2}) ||
                     rhs_decrements[irhs].equal_to({-2,-1,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-2,-1}) ||
                     rhs_decrements[irhs].equal_to({-1,-1,-2})) {
                /* The stepsize of the first index is 1 */
                if (m_analyzer->get_output_index()->get_position()==0) {
                    m_translator->assignment_statement(
                        m_translator->rhs_node_value(irhs, m_triangle_traversal[2]),
                        RecurAssignment::BasicAssignment,
                        m_translator->arithmetic_operation(
                            m_translator->rhs_node_value(irhs, m_triangle_traversal[1]),
                            RecurArithmetic::Subtraction,
                            std::string("1")
                        )
                    );
                }
                else {
                    auto output_position = m_analyzer->get_output_position(true);
                    if (output_position>=0) {
                        m_translator->begin_if_statement(m_translator->rhs_nodes_at(irhs));
                        m_translator->assignment_statement(
                            m_translator->rhs_node_value(irhs, m_triangle_traversal[2]),
                            RecurAssignment::BasicAssignment,
                            m_translator->arithmetic_operation(
                                m_translator->rhs_node_value(irhs, m_triangle_traversal[1]),
                                RecurArithmetic::Subtraction,
                                m_translator->call_function(
                                    m_translator->rhs_nodes_at(irhs),
                                    std::string("get_stepsize"),
                                    std::vector<std::string>({std::to_string(output_position)}),
                                    RecurOperandType::PointerOperand
                                )
                            )
                        );
                        m_translator->end_elif_statement();
                    }
                    else {
                        m_translator->handle_error(std::vector<std::string>({
                            m_translator->string_literal(
                                "Not found the position of the output index "+m_analyzer->get_output_index()->to_string()
                            )
                        }));
                    }
                }
            }
            else {
                m_translator->handle_error(std::vector<std::string>({
                    m_translator->string_literal(
                        "Decrements "+rhs_decrements[irhs].to_string()+" of the output index has not implemented"
                    )
                }));
            }
        }
    }

    void RecurOutputConverter::begin_index_order_condition(const std::shared_ptr<RecurIndex>& index,
                                                           const int order,
                                                           const bool elifStatement) noexcept
    {
        m_translator->begin_if_statement(
            m_translator->comparison_operation(m_translator->index_order(index->get_name()),
                                               RecurComparison::Equal,
                                               std::to_string(order)),
            elifStatement
        );
    }

    void RecurOutputConverter::begin_index_order_condition() noexcept
    {
        m_translator->begin_else_statement();
    }

    void RecurOutputConverter::end_index_order_condition() noexcept
    {
        m_translator->end_elif_statement();
    }
}
