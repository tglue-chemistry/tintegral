/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements classes to render RecurIntegration derived class,
   top-down and bottom-up procedures with quadrature being used.

   2020-11-02, Bin Gao:
   * first version
*/

#include <iterator>

#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/SymbolVisitor/RecurAnalyzer.hpp"
#include "tIntegral/Compiler/RecurQuadratureRender.hpp"

namespace tIntegral
{
    std::vector<std::tuple<std::string,std::string,std::string,bool>>
    RecurIntegrationQuadRender::recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                                          const unsigned int numExpressions,
                                                          const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        auto class_members = RecurIntegrationRender::recur_integration_members(integrand, numExpressions, translator);
        class_members.push_back(std::make_tuple(translator->type_quadrature(),
                                                translator->member_object(quadrature_name()),
                                                quadrature_name(),
                                                true));
        class_members.push_back(std::make_tuple(translator->type_floating_point(),
                                                translator->member_object(abscissa_name()),
                                                std::string(),
                                                false));
        return class_members;
    }

    std::vector<std::string>
    RecurTopDownQuadRender::recur_assign_args(const RecurExpression& expression,
                                              const unsigned int position,
                                              const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        auto args_to_assign = RecurTopDownRender::recur_assign_args(expression, position, translator);
        /* Output nodes are saved for the first recurrence relation */
        if (position==0) args_to_assign.push_back(translator->boolean_literal(true));
        return args_to_assign;
    }

    bool RecurBottomUpQuadRender::operator()(const std::vector<RecurExpression>& expressions,
                                             const bool evalIntegral,
                                             const std::string& nameIntegration,
                                             const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        /* Begin the bottom-up function */
        translator->begin_function(bottom_up_name(),
                                   translator->type_boolean(),
                                   std::vector<std::string>(),
                                   std::string("Bottom-up procedure"));
        /* Check the buffer */
        translator->begin_if_statement(
            translator->call_function(translator->member_object(recur_buffer_name()),
                                      std::string("empty"),
                                      std::vector<std::string>(),
                                      RecurOperandType::PointerOperand)
        );
        translator->handle_error(std::vector<std::string>({
            translator->string_literal(
                translator->class_member(nameIntegration, bottom_up_name())+" called with empty buffer"
            )
        }));
        translator->end_elif_statement();
        /* Perform quadrature
           return m_quadrature(m_recur_arrays[0], [this](RealType abscissa){ this->eval_at(abscissa); }); */
        translator->return_statement(translator->call_function(
            translator->member_object(quadrature_name()),
            std::vector<std::string>({
                recur_array_object(0),
                translator->make_lambda_expression(
                    std::vector<std::string>({translator->this_pointer()}),
                    std::vector<std::string>({
                        translator->make_declaration(translator->type_floating_point(), abscissa_name())
                    }),
                    std::vector<std::string>({
                        translator->call_function(translator->this_pointer(),
                                                  quad_integrand_name(),
                                                  std::vector<std::string>({abscissa_name()}),
                                                  RecurOperandType::PointerOperand)
                    })
                )
            })
        ));
        /* Write the function for integrand evaluation at abscissa */
        translator->begin_function(quad_integrand_name(),
                                   translator->type_void(),
                                   std::vector<std::string>({
                                       translator->make_declaration(translator->type_floating_point(),
                                                                    abscissa_name(),
                                                                    false,
                                                                    RecurTypeSpecifier::Const)
                                   }),
                                   "Function for integrand evaluation at abscissa",
                                   RecurIntegrandSpecifier::MemberFunction);
        translator->assignment_statement(translator->member_object(abscissa_name()),
                                         RecurAssignment::BasicAssignment,
                                         abscissa_name());
        /* Begin the bottom-up procedure, first call the customer-implemented
           function to evaluate integrals */
        if (evalIntegral) translator->call_function_statement(eval_function_name(), std::vector<std::string>());
        /* Call the bottom-up procedure of each recurrence expression */
        for (auto each_expression=expressions.crbegin(); each_expression!=expressions.crend(); ++each_expression) {
            translator->call_function_statement(bottom_up_name(*each_expression), std::vector<std::string>());
        }
        translator->end_function();
        return true;
    }
}
