/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the recurrence relation converter factory.

   2019-06-29, Bin Gao:
   * moved from RecurCompiler
*/

#include <array>
#include <functional>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "tGlueCore/Convert.hpp"

#include "tIntegral/Compiler/RecurConverterFactory.hpp"

namespace tIntegral
{
    RecurNonOutputConverter
    RecurConverterFactory::make_non_output_converter(const std::string& nameFunction,
                                                     const RecurVector& decrement,
                                                     const RecurDirection recurDirection,
                                                     const std::shared_ptr<RecurAnalyzer> analyzer) const noexcept
    {
        RecurNonOutputConverter converter(nameFunction, m_triangle_traversal, m_translator, analyzer);
        /* Add snippeter for the decrement 0 and positive decrements */
        if (decrement.equal_to({0,0,0})) {
            converter.set(
                recurDirection,
                std::vector<RecurNonOutputSnippeter>({
                    std::make_pair(-1, std::vector<RecurSnippeter>({
                        std::make_tuple(RecurVector({0,0,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.begin_loop_triangle(index, true, orderNeeded);
                                            converter.add_debug(std::string("loops of decrement 0 and positive decrements"));
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        [&](){ converter.end_loop_triangle(); })
                    }))
                })
            );
        }
        /* Add snippeter for the decrement -x, -y or -z */
        else if (decrement.equal_to({-1,0,0}) ||
                 decrement.equal_to({0,-1,0}) ||
                 decrement.equal_to({0,0,-1}) ||
                 decrement.equal_to({-1,-1,0}) ||
                 decrement.equal_to({-1,0,-1}) ||
                 decrement.equal_to({0,-1,-1}) ||
                 decrement.equal_to({-1,-1,-1})) {
            converter.set(
                recurDirection,
                std::vector<RecurNonOutputSnippeter>({
                    /* Order of index is 0 */
                    std::make_pair(0, std::vector<RecurSnippeter>({
                        std::make_tuple(RecurVector(0, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            //converter.declare_first_corner(index, 0, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return std::array<bool,3>({true, true, true});
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    })),
                    /* Order of index is > 0 */
                    std::make_pair(-1, std::vector<RecurSnippeter>({
                        /* Component of the first corner */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the first corner"));
                                            auto zero_orders = converter.declare_first_corner(index, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* Component at the edge between the first and the second corners */
                        std::make_tuple(RecurVector({-1,-1,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_edge(index,
                                                                                              false,
                                                                                              1,
                                                                                              0,
                                                                                              orderNeeded);
                                            converter.add_debug(std::string("component at the edge between the first and the second corners"));
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        /* Components x...xy...yz...z */
                        std::make_tuple(RecurVector({-1,-1,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders =  converter.begin_corner_to_corner(index,
                                                                                                 false,
                                                                                                 false,
                                                                                                 1,
                                                                                                 0,
                                                                                                 orderNeeded);
                                            converter.add_debug(std::string("components x...xy...yz...z"));
                                            return zero_orders;
                                        },
                                        [&]() { converter.end_loop_component(); }),
                        /* Component at the edge between the first and the last corners */
                        std::make_tuple(RecurVector({-1,0,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component at the edge between the first and the last corners"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(
                                                m_triangle_traversal[1]
                                            )] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            converter.end_loop_component();
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* Component of the second corner */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the second corner"));
                                            return converter.assign_last_edge(index,
                                                                              RecurDirection::XYZ,
                                                                              0,
                                                                              orderNeeded);
                                        },
                                        std::function<void()>()),
                        /* Components at the last edge  */
                        std::make_tuple(RecurVector({0,-1,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                true,
                                                                                                false,
                                                                                                1,
                                                                                                0,
                                                                                                orderNeeded);
                                            converter.add_debug(std::string("components at the last edge"));
                                            return zero_orders;
                                        },
                                        [&]() { converter.end_loop_component(); }),
                        /* Component of the last corner */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[2]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the last corner"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({true, true, true});
                                            zero_orders[tGlueCore::to_integral(
                                                m_triangle_traversal[2]
                                            )] = false;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    }))
                })
            );
        }
        /* Add snippeter for the decrement -xx, -yy or -zz mixed with -x, -y, -z or 0 */
        else if (decrement.equal_to({-2,0,0}) ||
                 decrement.equal_to({0,-2,0}) ||
                 decrement.equal_to({0,0,-2}) ||
                 decrement.equal_to({-2,-1,0}) ||
                 decrement.equal_to({-2,0,-1}) ||
                 decrement.equal_to({-1,-2,0}) ||
                 decrement.equal_to({-1,0,-2}) ||
                 decrement.equal_to({0,-2,-1}) ||
                 decrement.equal_to({0,-1,-2}) ||
                 decrement.equal_to({-2,-1,-1}) ||
                 decrement.equal_to({-1,-2,-1}) ||
                 decrement.equal_to({-1,-1,-2})) {
            converter.set(
                recurDirection,
                std::vector<RecurNonOutputSnippeter>({
                    /* Order of index is 0 */
                    std::make_pair(0, std::vector<RecurSnippeter>({
                        std::make_tuple(RecurVector(0, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            //converter.declare_first_corner(index, 0, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return std::array<bool,3>({true, true, true});
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    })),
                    /* Order of index is 1 */
                    std::make_pair(1, std::vector<RecurSnippeter>({
                        /* x */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.declare_first_corner(index, 1, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* y */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.update_first_edge(index, orderNeeded, true);
                                        },
                                        std::function<void()>()),
                        /* z */
                        std::make_tuple(RecurVector(-1, m_triangle_traversal[2]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({true, true, true});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[2])] = false;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    })),
                    /* Order of index is 2 */
                    std::make_pair(2, std::vector<RecurSnippeter>({
                        /* xx */
                        std::make_tuple(RecurVector(-2, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.declare_first_corner(index, 2, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* xy */
                        std::make_tuple(RecurVector({-1,-1,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.update_first_edge(index, orderNeeded);
                                        },
                                        std::function<void()>()),
                        /* xz */
                        std::make_tuple(RecurVector({-1,0,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* yy */
                        std::make_tuple(RecurVector(-2, m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.assign_last_edge(index, 2, orderNeeded, true);
                                        },
                                        std::function<void()>()),
                        /* yz */
                        std::make_tuple(RecurVector({0,-1,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = true;
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        /* zz */
                        std::make_tuple(RecurVector(-2, m_triangle_traversal[2]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({true, true, true});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[2])] = false;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    })),
                    /* Order of index is 3 */
                    std::make_pair(3, std::vector<RecurSnippeter>({
                        /* xxx */
                        std::make_tuple(RecurVector(-3, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.declare_first_corner(index, 3, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* xxy */
                        std::make_tuple(RecurVector({-2,-1,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.update_first_edge(index, orderNeeded);
                                        },
                                        std::function<void()>()),
                        /* xxz */
                        std::make_tuple(RecurVector({-2,0,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* xyy */
                        std::make_tuple(RecurVector({-1,-2,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.assign_last_edge(index, 2, orderNeeded);
                                        },
                                        std::function<void()>()),
                        /* xyz */
                        std::make_tuple(RecurVector({-1,-1,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        /* xzz */
                        std::make_tuple(RecurVector({-1,0,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* yyy */
                        std::make_tuple(RecurVector(-3, m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            return converter.assign_last_edge(index, 3, orderNeeded, true);
                                        },
                                        std::function<void()>()),
                        /* yyz */
                        std::make_tuple(RecurVector({0,-2,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = true;
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        /* yzz */
                        std::make_tuple(RecurVector({0,-1,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = true;
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        /* zzz */
                        std::make_tuple(RecurVector(-3, m_triangle_traversal[2]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({true, true, true});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[2])] = false;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    })),
                    /* Order of index is > 3 */
                    std::make_pair(-1, std::vector<RecurSnippeter>({
                        /* Component of the first corner */
                        std::make_tuple(RecurVector(-4, m_triangle_traversal[0]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the first corner"));
                                            auto zero_orders = converter.declare_first_corner(index, orderNeeded);
                                            /* Skip components before loops */
                                            converter.update_rhs_values(index, true);
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* First two components after the first corner */
                        std::make_tuple(RecurVector({-3,-1,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component after the first corner and along the edge to the second corner"));
                                            return converter.update_first_edge(index, orderNeeded);
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-3,0,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component after the first corner and along the edge to the last corner"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* Three components at the third row from the first corner */
                        std::make_tuple(RecurVector({-2,-2,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("first component of the third row"));
                                            return converter.assign_last_edge(index, 2, orderNeeded);
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-2,-1,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second component of the third row"));
                                            converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-2,0,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("last component of the third row"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* From the fourth row to the third row to last */
                        std::make_tuple(RecurVector({-2,-4,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_edge(index,
                                                                                              false,
                                                                                              3,
                                                                                              -1,
                                                                                              orderNeeded);
                                            converter.add_debug(std::string("first component of a row"));
                                            /* Skip components before a row */
                                            converter.update_rhs_values(index, false);
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-2,-3,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second component of a row"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-2,-2,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                false,
                                                                                                false,
                                                                                                2,
                                                                                                -1,
                                                                                                orderNeeded);
                                            converter.add_debug(std::string("components x...xy...yz...z of a row"));
                                            return zero_orders;
                                        },
                                        [&]() { converter.end_loop_component(); }),
                        std::make_tuple(RecurVector({-2,-1,-3}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second last component of a row"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-2,0,-4}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("last component of a row"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            converter.end_loop_component();
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* Second last row */
                        std::make_tuple(RecurVector({-1,-4,0}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("first component of the second last row"));
                                            return converter.assign_last_edge(index,
                                                                              RecurDirection::XYZ,
                                                                              -1,
                                                                              orderNeeded);
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-1,-3,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second component of the second last row"));
                                            converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-1,-2,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                true,
                                                                                                false,
                                                                                                2,
                                                                                                -2,
                                                                                                orderNeeded);
                                            converter.add_debug(std::string("components of the second last row"));
                                            return zero_orders;
                                        },
                                        [&]() { converter.end_loop_component(); }),
                        std::make_tuple(RecurVector({-1,-1,-3}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second last component of the second last row"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            return std::array<bool,3>({false, false, false});
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({-1,0,-4}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("last component of the second last row"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[1])] = true;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                        }),
                        /* Components at the last edge */
                        std::make_tuple(RecurVector(-4, m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the second corner"));
                                            return converter.assign_last_edge(index,
                                                                              RecurDirection::XYZ,
                                                                              0,
                                                                              orderNeeded);
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({0,-3,-1}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second component of the last edge"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = true;
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector({0,-2,-2}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                true,
                                                                                                false,
                                                                                                2,
                                                                                                -1,
                                                                                                orderNeeded);
                                            converter.add_debug(std::string("components of the last edge"));
                                            return zero_orders;
                                        },
                                        [&]() { converter.end_loop_component(); }),
                        std::make_tuple(RecurVector({0,-1,-3}, m_triangle_traversal[0], m_triangle_traversal[1]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("second last component of the last edge"));
                                            /* Orders are already updated in the previous for loop */
                                            //converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({false, false, false});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[0])] = true;
                                            return zero_orders;
                                        },
                                        std::function<void()>()),
                        std::make_tuple(RecurVector(-4, m_triangle_traversal[2]),
                                        [&](const std::shared_ptr<RecurIndex>& index,
                                            const std::array<bool,3>& orderNeeded)
                                        {
                                            converter.add_debug(std::string("component of the last corner"));
                                            converter.update_last_edge(index, orderNeeded);
                                            auto zero_orders = std::array<bool,3>({true, true, true});
                                            zero_orders[tGlueCore::to_integral(m_triangle_traversal[2])] = false;
                                            return zero_orders;
                                        },
                                        [&]()
                                        {
                                            /* Skip components after the row */
                                            converter.update_rhs_values();
                                            /* Skip components after the triangle */
                                            converter.update_rhs_values();
                                        })
                    }))
                })
            );
        }
        return converter;
    }

    RecurOutputConverter
    RecurConverterFactory::make_output_converter(const std::string& nameFunction,
                                                 const RecurVector& decrement,
                                                 const std::shared_ptr<RecurAnalyzer> analyzer) const noexcept
    {
        RecurOutputConverter converter(nameFunction, m_triangle_traversal, m_translator, analyzer);
        /* Add snippeter for the decrement -x, -y or -z */
        if (decrement.equal_to({-1,0,0}) ||
            decrement.equal_to({0,-1,0}) ||
            decrement.equal_to({0,0,-1}) ||
            decrement.equal_to({-1,-1,0}) ||
            decrement.equal_to({-1,0,-1}) ||
            decrement.equal_to({0,-1,-1}) ||
            decrement.equal_to({-1,-1,-1})) {
            converter.set(
                std::vector<RecurOutputSnippeter>({
                    /* Along the direction of the first corner */
                    std::make_pair(
                        m_triangle_traversal[0],
                        std::vector<RecurSnippeter>({
                            /* Components except for the last edge */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[0]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.begin_loop_triangle(index,
                                                                              false,
                                                                              orderNeeded);
                                                converter.add_debug(std::string("components except for the last edge"));
                                                return std::array<bool,3>({false, false, false});
                                            },
                                            [&]()
                                            {
                                                converter.end_loop_component();
                                                converter.end_loop_component();
                                                converter.assign_rhs_second();
                                            }),
                            /* Components of the last edge */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[0]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                    true,
                                                                                                    true,
                                                                                                    0,
                                                                                                    0,
                                                                                                    orderNeeded,
                                                                                                    true);
                                                converter.add_debug(std::string("components of the last edge"));
                                                return zero_orders;
                                            },
                                            [&]() { converter.end_loop_component(); })
                        })
                    ),
            /* For the output index and decrement -xx, after the loop of x...x
               component to xz...z component along the x direction, we need
               val_rhs_y = val_rhs_x to put the pointer of y direction to the y
               corner. For the y direction, we need val_rhs_z = val_rhs_y after
               the loop of y...y component to yz...z component. */

                    /* Along the direction of the second corner */
                    std::make_pair(
                        m_triangle_traversal[1],
                        std::vector<RecurSnippeter>({
                            /* Components of the last edge except for the last corner */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[1]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                    true,
                                                                                                    false,
                                                                                                    0,
                                                                                                    0,
                                                                                                    orderNeeded,
                                                                                                    true);
                                                converter.add_debug(std::string("components of the last edge except for the last corner"));
                                                return zero_orders;
                                            },
                                            [&]()
                                            {
                                                converter.end_loop_component();
                                                converter.assign_rhs_last();
                                            }),
                            /* Component of the last corner */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[1]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.add_debug(std::string("component of the last corner"));
                                                return converter.declare_last_corner(index, orderNeeded);
                                            },
                                            std::function<void()>())
                        })
                    ),
                    /* Along the direction of the last corner */
                    std::make_pair(
                        m_triangle_traversal[2],
                        std::vector<RecurSnippeter>({
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[2]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                return converter.declare_last_corner(index, orderNeeded);
                                            },
                                            std::function<void()>())
                        })
                    )
                })
            );
        }
        /* Add snippeter for the decrement -xx, -yy or -zz mixed with -x, -y, -z or 0 */
        else if (decrement.equal_to({-2,0,0}) ||
                 decrement.equal_to({0,-2,0}) ||
                 decrement.equal_to({0,0,-2}) ||
                 decrement.equal_to({-2,-1,0}) ||
                 decrement.equal_to({-2,0,-1}) ||
                 decrement.equal_to({-1,-2,0}) ||
                 decrement.equal_to({-1,0,-2}) ||
                 decrement.equal_to({0,-2,-1}) ||
                 decrement.equal_to({0,-1,-2}) ||
                 decrement.equal_to({-2,-1,-1}) ||
                 decrement.equal_to({-1,-2,-1}) ||
                 decrement.equal_to({-1,-1,-2})) {
            converter.set(
                std::vector<RecurOutputSnippeter>({
                    /* Along the direction of the first corner */
                    std::make_pair(
                        m_triangle_traversal[0],
                        std::vector<RecurSnippeter>({
                            /* Components except for the last edge */
                            std::make_tuple(RecurVector(-2, m_triangle_traversal[0]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.begin_loop_triangle(index,
                                                                              false,
                                                                              orderNeeded);
                                                converter.add_debug(std::string("components except for the last edge"));
                                                return std::array<bool,3>({false, false, false});
                                            },
                                            [&]()
                                            {
                                                converter.end_loop_component();
                                                converter.end_loop_component();
                                                converter.assign_rhs_second();
                                            }),
                            /* Components of the last edge */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[0]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                    true,
                                                                                                    true,
                                                                                                    0,
                                                                                                    0,
                                                                                                    orderNeeded,
                                                                                                    true);
                                                converter.add_debug(std::string("components of the last edge"));
                                                return zero_orders;
                                            },
                                            [&]() { converter.end_loop_component(); })
                        })
                    ),
                    /* Along the direction of the second corner */
                    std::make_pair(
                        m_triangle_traversal[1],
                        std::vector<RecurSnippeter>({
                            /* Components of the last edge except for the last corner */
                            std::make_tuple(RecurVector(-2, m_triangle_traversal[1]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                auto zero_orders = converter.begin_corner_to_corner(index,
                                                                                                    true,
                                                                                                    false,
                                                                                                    0,
                                                                                                    0,
                                                                                                    orderNeeded,
                                                                                                    true);
                                                converter.add_debug(std::string("components of the last edge except for the last corner"));
                                                return zero_orders;
                                            },
                                            [&]()
                                            {
                                                converter.end_loop_component();
                                                converter.assign_rhs_last();
                                            }),
                            /* Component of the last corner */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[1]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.add_debug(std::string("component of the last corner"));
                                                return converter.declare_last_corner(index, orderNeeded);
                                            },
                                            std::function<void()>())
                        })
                    ),
                    /* Along the direction of the last corner */
                    std::make_pair(
                        m_triangle_traversal[2],
                        std::vector<RecurSnippeter>({
                            /* Order of RHS nodes with the decrement -x, -y or -z is 0 */
                            std::make_tuple(RecurVector(-1, m_triangle_traversal[2]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.add_debug(std::string("component of the last corner"));
                                                converter.begin_index_order_condition(index, 0);
                                                //return converter.declare_last_corner(index, 0, orderNeeded);
                                                return std::array<bool,3>({true, true, true});
                                            },
                                            [&](){ converter.end_index_order_condition(); }),
                            /* Order of RHS nodes with the decrement -x, -y or -z is > 0 */
                            std::make_tuple(RecurVector(-2, m_triangle_traversal[2]),
                                            [&](const std::shared_ptr<RecurIndex>& index,
                                                const std::array<bool,3>& orderNeeded)
                                            {
                                                converter.begin_index_order_condition();
                                                return converter.declare_last_corner(index, orderNeeded);
                                            },
                                            [&](){ converter.end_index_order_condition(); })
                        })
                    )
                })
            );
        }
        return converter;
    }
}
