/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements classes to render RecurIntegration derived class,
   top-down and bottom-up procedures with operators accumulated.

   2020-11-24, Bin Gao:
   * first version
*/

#include <iterator>

#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/SymbolVisitor/RecurAnalyzer.hpp"
#include "tIntegral/Compiler/RecurAccumulatedRender.hpp"

namespace tIntegral
{
    std::vector<std::tuple<std::string,std::string,std::string,bool>>
    RecurIntegrationAccRender::recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                                         const unsigned int numExpressions,
                                                         const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        auto class_members = RecurIntegrationRender::recur_integration_members(integrand, numExpressions, translator);
        class_members.push_back(std::make_tuple(translator->type_quadrature(),
                                                translator->member_object(quadrature_name()),
                                                quadrature_name(),
                                                true));
        class_members.push_back(std::make_tuple(translator->type_floating_point(),
                                                translator->member_object(abscissa_name()),
                                                std::string(),
                                                false));
        return class_members;
    }

    std::vector<std::string>
    RecurTopDownAccRender::recur_assign_args(const RecurExpression& expression,
                                             const unsigned int position,
                                             const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        auto args_to_assign = RecurTopDownRender::recur_assign_args(expression, position, translator);
        /* Output nodes are saved for the first recurrence relation */
        if (position==0) args_to_assign.push_back(translator->boolean_literal(true));
        return args_to_assign;
    }


    //{
    //    for (m_oneOper->begin(); !m_oneOper->end(); m_oneOper->next()) {
    //        eval();
    //        HarmonicBra_bottom_up();
    //        HarmonicKet_bottom_up();
    //    }
    //    AngBra_bottom_up();
    //    AngKet_bottom_up();
    //    OperGeo_bottom_up();
    //}

    bool RecurBottomUpAccRender::operator()(const std::vector<RecurExpression>& expressions,
                                            const bool evalIntegral,
                                            const std::string& nameIntegration,
                                            const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
            translator->begin_function(m_name_quad_integrand,
                                         translator->type_void(),
                                         std::vector<std::string>({
                                             translator->make_declaration(translator->type_floating_point(),
                                                                            m_name_quad_abscissa,
                                                                            false,
                                                                            RecurTypeSpecifier::Const)
                                         }),
                                         "Function for the evaluation at abscissa",
                                         RecurIntegrandSpecifier::MemberFunction);
            translator->assignment_statement(translator->member_object(m_name_quad_abscissa),
                                               RecurAssignment::BasicAssignment,
                                               m_name_quad_abscissa);
            /* Call the customer-implemented function for evaluating integrals
               after all recurrence expressions */
            if (evalIntegral) {
                translator->call_function_statement(translator->eval_function(), std::vector<std::string>());
            }
            /* Call the bottom-up procedure of each recurrence expression */
            for (auto each_expression=expressions.crbegin(); each_expression!=expressions.crend(); ++each_expression) {
                translator->call_function_statement(
                    translator->idx_recur_function(each_expression->get_output_index()->get_name(), false),
                    std::vector<std::string>()
                );
            }
            translator->end_function();
    }
}
