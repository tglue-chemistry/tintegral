/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements classes to render RecurIntegration derived class,
   top-down and bottom-up procedures.

   2020-11-02, Bin Gao:
   * first version
*/

#include <iterator>

#include "tIntegral/Index/IndexCompileOption.hpp"
#include "tIntegral/SymbolVisitor/RecurAnalyzer.hpp"
#include "tIntegral/Compiler/RecurRender.hpp"

namespace tIntegral
{
    bool RecurIntegrationRender::operator()(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                            const std::string& nameIntegration,
                                            const std::vector<std::string>& tparameters,
                                            const std::string& descripIntegration,
                                            const unsigned int numExpressions,
                                            const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        translator->begin_class(nameIntegration,
                                tparameters,
                                recur_integration_members(integrand, numExpressions, translator),
                                descripIntegration,
                                translator->type_recur_integration);
        return true;
    }

    std::vector<std::tuple<std::string,std::string,std::string,bool>>
    RecurIntegrationRender::recur_integration_members(const std::vector<std::shared_ptr<RecurIntegrand>>& integrand,
                                                      const unsigned int numExpressions,
                                                      const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        std::vector<std::tuple<std::string,std::string,std::string,bool>> class_members;
        for (auto const& each_function: integrand) {
            class_members.push_back(
                std::make_tuple(translator->type_shared_pointer(each_function->type_name()),
                                translator->member_object(each_function->get_name()),
                                each_function->get_name(),
                                true)
            );
        }
        class_members.push_back(
            std::make_tuple(
                translator->type_vector(translator->type_recur_index()),
                translator->member_object(all_indices_name()),
                std::string(),
                false
            )
        );
        class_members.push_back(std::make_tuple(translator->type_recur_buffer(),
                                                translator->member_object(recur_buffer_name()),
                                                std::string(),
                                                false));
        /* Each recurrence expression needs a RecurArray */
        class_members.push_back(
            std::make_tuple(translator->type_array(translator->type_recur_array(), numExpressions),
                            translator->member_object(recur_arrays_name()),
                            std::string(),
                            false)
        );
        return class_members;
    }

    bool RecurTopDownRender::operator()(const std::vector<RecurExpression>& expressions,
                                        const std::string& nameIntegration,
                                        const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        /* Begin the top-down function */
        begin_top_down();
        /* Assign arguments */
        translator->assignment_statement(translator->member_object(all_indices_name()),
                                         RecurAssignment::BasicAssignment,
                                         all_indices_name());
        translator->assignment_statement(translator->member_object(recur_buffer_name()),
                                         RecurAssignment::BasicAssignment,
                                         recur_buffer_name());
        /* Loops over each recurrence expression */
        for (auto each_expression=expressions.cbegin(); each_expression!=expressions.cend(); ++each_expression) {
            switch (each_expression->get_output_option()) {
                case IndexCompileOption::Customer:
                    /* Call the customer-implemented top-down function */
                    translator->invoke_boolean_function(
                        each_expression->get_output_index()->get_name()+"_"+top_down_name(),
                        std::vector<std::string>(),
                        "Called by "+translator->class_member(nameIntegration, top_down_name())
                    );
                    break;
                case IndexCompileOption::Recursion:
                {
                    /* Call the assign() function of RecurArray */
                    translator->invoke_boolean_function(
                        recur_array_object(
                            translator,
                            std::distance(expressions.cbegin(), each_expression)
                        ),
                        std::string("assign"),
                        recur_assign_args(
                            *each_expression,
                            std::distance(expressions.cbegin(), each_expression),
                            translator
                        ),
                        RecurOperandType::ObjectOperand,
                        "Called by "+translator->class_member(nameIntegration, top_down_name())
                    );
                    break;
                }
                case IndexCompileOption::Transfer:
                    //FIXME: to implement
                    translator->handle_error(std::vector<std::string>({
                        translator->string_literal("IndexCompileOption::Transfer not implemented yet!")
                    }));
                    break;
                case IndexCompileOption::Skip:
                    break;
                default:
                    translator->handle_error(std::vector<std::string>({
                        translator->string_literal(
                            stringify_compile_option(each_expression->get_output_option())+" not implemented yet!"
                        )
                    }));
                    break;
            }
        }
        translator->return_statement(translator->boolean_literal());
        translator->end_function();
        return true;
    }

    std::vector<std::string>
    RecurTopDownRender::recur_assign_args(const RecurExpression& expression,
                                          const unsigned int position,
                                          const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        std::vector<std::string> args_to_assign;
        if (position==0) {
            args_to_assign.push_back(translator->get_rvalue_object(output_nodes_name()));
        }
        else {
            /* Take input nodes of preceding recurrence relation as output nodes */
            args_to_assign.push_back(
                translator->call_function(
                    recur_array_object(translator, position-1),
                    std::string("get_input_nodes"),
                    std::vector<std::string>({
                        translator->subscript_operation(
                            all_indices_name(),
                            expression.get_output_index()->get_position()
                        )
                    }),
                    RecurOperandType::ObjectOperand
                )
            );
        }
        args_to_assign.push_back(all_indices_name());
        /* Argument of positions of recurrence-relation indices */
        args_to_assign.push_back(
            translator->vector_constructor(expression.get_idx_positions(true))
        );
        /* Argument of the output index */
        args_to_assign.push_back(std::to_string(expression.get_output_position(true)));
        /* Get the norms of decrements of the recurrence relation */
        args_to_assign.push_back(
            translator->jagged_array_constructor(expression.get_analyzer()->get_rhs_norms(true))
        );
        /* Argument of buffer */
        args_to_assign.push_back(translator->member_object(recur_buffer_name()));
        return args_to_assign;
    }

    bool RecurBottomUpRender::operator()(const std::vector<RecurExpression>& expressions,
                                         const bool evalIntegral,
                                         const std::string& nameIntegration,
                                         const std::shared_ptr<RecurTranslator>& translator) noexcept
    {
        /* Begin the bottom-up function */
        translator->begin_function(bottom_up_name(),
                                   translator->type_boolean(),
                                   std::vector<std::string>(),
                                   std::string("Bottom-up procedure"));
        /* Check the buffer */
        translator->begin_if_statement(
            translator->call_function(translator->member_object(recur_buffer_name()),
                                      std::string("empty"),
                                      std::vector<std::string>(),
                                      RecurOperandType::PointerOperand)
        );
        translator->handle_error(std::vector<std::string>({
            translator->string_literal(
                translator->class_member(nameIntegration, bottom_up_name())+" called with empty buffer"
            )
        }));
        translator->end_elif_statement();
        /* Begin the bottom-up procedure, first call the customer-implemented
           function to evaluate integrals */
        if (evalIntegral) translator->call_function_statement(eval_function_name(), std::vector<std::string>());
        /* Call the bottom-up procedure of each recurrence expression */
        for (auto each_expression=expressions.crbegin(); each_expression!=expressions.crend(); ++each_expression) {
            translator->call_function_statement(bottom_up_name(*each_expression), std::vector<std::string>());
        }
        translator->return_statement(translator->boolean_literal());
        translator->end_function();
        return true;
    }
}
