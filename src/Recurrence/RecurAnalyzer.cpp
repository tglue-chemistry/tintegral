/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements the analyzer for recurrence relations.

   2019-06-26, Bin Gao:
   * moved from RecurCompiler
*/

#include "tIntegral/Recurrence/RecurAnalyzer.hpp"

namespace tIntegral
{
    bool RecurAnalyzer::assign(const std::shared_ptr<RecurIndex> outputIndex,
                               const std::shared_ptr<RecurSymbol> RHS) noexcept
    {
        m_output_index = outputIndex;
        m_variables.clear();
        m_order_indices.clear();
        m_indices.clear();
        m_nnz_rhs = false;
        m_rhs_norms.clear();
        m_rhs_decrements.clear();
        m_trans_decrements.clear();
        m_rhs_directions.clear();
        /* Process right hand side of the recurrence relation */
        if (RHS->accept(this)) {
            if (m_nnz_rhs) {
                /* Set transformed RHS decrements and contributed directions */
                transform_rhs_decrements();
                /* Extra vector of directions of RHS terms is for after
                   considering all indices */
                m_rhs_directions.assign(m_indices.size()+1, std::vector<RecurDirection>());
                for (auto& each_index: m_rhs_directions) {
                    each_index.assign(get_num_rhs(), RecurDirection::XYZ);
                }
                return true;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurAnalyzer::assign() is given a recurrence relation ",
                                        RHS->to_string(),
                                        ", with the output index ",
                                        outputIndex->to_string(),
                                        ", and none of its RHS terms is non zero");
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::assign() is given a recurrence relation ",
                                    RHS->to_string(),
                                    ", with the output index ",
                                    outputIndex->to_string());
            return false;
        }
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurScalarVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a scalar variable");
#endif
        m_variables.insert(variable);
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurCartesianVar> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a Cartesian variable");
#endif
        m_variables.insert(variable);
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurScalarVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a vector of scalar variables");
#endif
        m_variables.insert(variable);
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurCartesianVec> variable) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a vector of Cartesian variables");
#endif
        m_variables.insert(variable);
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurIdxOrder> order) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process an order");
#endif
        auto idx_order = order->get_indices()[0];
        bool new_index = true;
        for (auto& each_index: m_order_indices) {
            if (idx_order->equal_to(each_index.first)) {
                each_index.second.insert(idx_order->get_direction());
                new_index = false;
                break;
            }
        }
        if (new_index) {
            m_order_indices.push_back(
                std::make_pair(idx_order, std::set<RecurDirection>({idx_order->get_direction()}))
            );
        }
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurTerm> term) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a RHS term ",
                                term->to_string());
#endif
        /* We treat the first RHS term in different manner */
        auto first_term = m_indices.empty();
        auto rhs_indices = term->get_indices();
        if (!first_term && rhs_indices.size()!=m_indices.size()) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() encounters inconsistent number of indices in the term ",
                                    term->to_string(),
                                    " which is expected to be ",
                                    m_indices.size());
            return false;
        }
        /* Check if this is a non-zero RHS term by first verifying the
           decrement of the output index */
        auto output_decrement = term->get_order(m_output_index);
        auto is_nnz_rhs = is_nnz_output(output_decrement);
        /* For the output index, only negative decrements are allowed */
        if (is_output_decrement(output_decrement)) {
            std::vector<int> idx_norms;
            idx_norms.reserve(rhs_indices.size());
            if (first_term) {
                m_indices.reserve(rhs_indices.size());
                m_rhs_decrements.reserve(rhs_indices.size());
            }
            /* Loop over each index in the RHS term */
            for (unsigned int iidx=0; iidx<rhs_indices.size(); ++iidx) {
                auto index_decrement = rhs_indices[iidx]->get_order();
                /* A non-zero RHS term should have increments of other
                   indices >= 0 */
                if (!rhs_indices[iidx]->equal_to(m_output_index) && index_decrement.any_negative()) {
                    is_nnz_rhs = false;
                }
                /* Get the norm of the decrement of the current index */
                idx_norms.push_back(index_decrement.get_norm());
                if (first_term) {
                    m_indices.push_back(rhs_indices[iidx]);
                    /* Set the loop category of the current index */
                    m_rhs_decrements.push_back(std::make_pair(index_decrement.get_prioritized(),
                                                              std::vector<RecurVector>({index_decrement})));
                }
                else {
                    if (m_indices[iidx]->equal_to(rhs_indices[iidx])) {
                        /* (Re)set the loop category of the current index */
                        auto loop_category = index_decrement.get_prioritized(m_rhs_decrements[iidx].first);
                        m_rhs_decrements[iidx].first = loop_category;
                    }
                    else {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "RecurAnalyzer::dispatch() encounters an inconsistent index ",
                                                rhs_indices[iidx]->to_string(),
                                                " in the RHS term ",
                                                term->to_string(),
                                                ", and which is expected to be ",
                                                m_indices[iidx]->to_string());
                        return false;
                    }
                    m_rhs_decrements[iidx].second.push_back(index_decrement);
                }
            }
            if (!m_nnz_rhs) m_nnz_rhs = is_nnz_rhs;
            m_rhs_norms.push_back(std::move(idx_norms));
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() encounters an invalid decrement for the output index ",
                                    m_output_index->to_string(),
                                    " in the RHS term ",
                                    term->to_string());
            return false;
        }
        return true;
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurAddition> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process an addition");
#endif
        if (operation->get_lhs()->accept(this)) {
            if (operation->get_rhs()->accept(this)) {
                if (!operation->get_name().empty()) m_variables.insert(operation);
                return true;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurAnalyzer::dispatch() called for the addend");
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() called for the augend");
            return false;
        }
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurSubtraction> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a subtraction");
#endif
        if (operation->get_lhs()->accept(this)) {
            if (operation->get_rhs()->accept(this)) {
                if (!operation->get_name().empty()) m_variables.insert(operation);
                return true;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurAnalyzer::dispatch() called for the subtrahend");
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() called for the minuend");
            return false;
        }
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurMultiplication> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a multiplication");
#endif
        if (operation->get_lhs()->accept(this)) {
            if (operation->get_rhs()->accept(this)) {
                if (!operation->get_name().empty()) m_variables.insert(operation);
                return true;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurAnalyzer::dispatch() called for the multiplicand");
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() called for the multiplier");
            return false;
        }
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurDivision> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a division");
#endif
        if (operation->get_lhs()->accept(this)) {
            if (operation->get_rhs()->accept(this)) {
                if (!operation->get_name().empty()) m_variables.insert(operation);
                return true;
            }
            else {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "RecurAnalyzer::dispatch() called for the divisor");
                return false;
            }
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() called for the dividend");
            return false;
        }
    }

    bool RecurAnalyzer::dispatch(std::shared_ptr<RecurParentheses> operation) noexcept
    {
#if defined(TINTEGRAL_DEBUG)
        Settings::logger->write(tGlueCore::MessageType::Debug,
                                "RecurAnalyzer::dispatch() will process a parenthesized expression");
#endif
        if (operation->get_expression()->accept(this)) {
            return true;
        }
        else {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "RecurAnalyzer::dispatch() called for a parenthesized expression");
            return false;
        }
    }
}
