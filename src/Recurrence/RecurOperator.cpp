/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements different operators used for recurrence relations.

   2020-06-09, Bin Gao:
   * first version
*/

#include "tIntegral/Recurrence/RecurExprVisitor.hpp"
#include "tIntegral/Recurrence/RecurOperator.hpp"

namespace tIntegral
{
    bool RecurCartMultMoment::accept(RecurExprVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurCartMultMoment::to_string() const noexcept
    {
        return "{cart-mult-moment: {name: "+get_name()
            +", type: "+type_name()
            +", order-moment: "+m_idx_moment->to_string()
            +", order-electronic: "+m_idx_el_deriv->to_string()+"}}";
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurCartMultMoment::get_indices() const noexcept
    {
        return std::vector<std::shared_ptr<RecurIndex>>({m_idx_moment, m_idx_el_deriv});
    }

    bool RecurECPUnprojected::accept(RecurExprVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurECPUnprojected::to_string() const noexcept
    {
        return "{ecp-unprojected: {name: "+get_name()
            +", type: "+type_name()
            +", power-radial: "+m_idx_radial->to_string()
            +", order-bessel: "+m_idx_bessel->to_string()
            +", order-geometrical: "+m_idx_geometrical->to_string();
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurECPUnprojected::get_indices() const noexcept
    {
        return std::vector<std::shared_ptr<RecurIndex>>({m_idx_radial, m_idx_bessel, m_idx_geometrical});
    }

    bool RecurECPProjected::accept(RecurExprVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurECPProjected::to_string() const noexcept
    {
        return "{ecp-unprojected: {name: "+get_name()
            +", type: "+type_name()
            +", power-radial: "+m_idx_radial->to_string()
            +", order-bessel-bra: "+m_idx_bessel_bra->to_string()
            +", order-bessel-ket: "+m_idx_bessel_ket->to_string()
            +", order-harmonic-bra: "+m_idx_harmonic_bra->to_string()
            +", order-harmonic-ket: "+m_idx_harmonic_ket->to_string()
            +", order-geometrical: "+m_idx_geometrical->to_string();
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurECPProjected::get_indices() const noexcept
    {
        return std::vector<std::shared_ptr<RecurIndex>>({
            m_idx_radial,
            m_idx_bessel_bra,
            m_idx_bessel_ket,
            m_idx_harmonic_bra,
            m_idx_harmonic_ket,
            m_idx_geometrical
        });
    }
}
