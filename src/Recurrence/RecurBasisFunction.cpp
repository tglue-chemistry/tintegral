/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements different basis functions used for recurrence relations.

   2020-06-11, Bin Gao:
   * first version
*/

#include "tIntegral/Recurrence/RecurExprVisitor.hpp"
#include "tIntegral/Recurrence/RecurBasisFunction.hpp"

namespace tIntegral
{
    bool RecurGaussianFunction::accept(RecurExprVisitor* visitor) noexcept
    {
        return visitor->dispatch(shared_from_this());
    }

    std::string RecurGaussianFunction::to_string() const noexcept
    {
        return "{gaussian-function: {name: "+get_name()
            +", type: "+type_name()
            +", exponent: "+m_idx_exponent->to_string()
            +", angular-momentum: "+m_idx_angular->to_string()+"}}";
    }

    std::vector<std::shared_ptr<RecurIndex>> RecurGaussianFunction::get_indices() const noexcept
    {
        return std::vector<std::shared_ptr<RecurIndex>>({m_idx_exponent, m_idx_angular});
    }
}
