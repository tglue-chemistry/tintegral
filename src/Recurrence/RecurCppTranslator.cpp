/* tIntegral: not only an integral computation library
   Copyright 2018-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file generates C++ source codes for the recurrence relation compiler
   and generator.

   2019-06-13, Bin Gao:
   * first version
*/

#include <regex>

#include "tGlueCore/Convert.hpp"

#include "tIntegral/Recurrence/RecurCppTranslator.hpp"

namespace tIntegral
{
    std::string RecurCppTranslator::error_notation() const noexcept
    {
        return std::string("??");
    }

    std::string RecurCppTranslator::end_of_statement(const bool newLine) const noexcept
    {
        return newLine ? std::string(";\n") : std::string(";");
    }

    std::string RecurCppTranslator::declaration_specifier(const RecurTypeSpecifier declSpecifier) const noexcept
    {
        switch (declSpecifier) {
            case RecurTypeSpecifier::None:
                return std::string();
            case RecurTypeSpecifier::Const:
                return std::string("const ");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(declSpecifier))+error_notation();
        }
    }

    std::string RecurCppTranslator::get_declarator(const RecurDeclarator declarator) const noexcept
    {
        switch (declarator) {
            case RecurDeclarator::None:
                return std::string();
            case RecurDeclarator::LvalueReference:
                return std::string("&");
            case RecurDeclarator::RvalueReference:
                return std::string("&&");
            case RecurDeclarator::Pointer:
                return std::string("*");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(declarator))+error_notation();
        }
    }

    std::string RecurCppTranslator::member_of_object() const noexcept
    {
        return std::string(".");
    }

    std::string RecurCppTranslator::member_of_pointer() const noexcept
    {
        return std::string("->");
    }

    std::string RecurCppTranslator::comparison_operator(const RecurComparison oper) const noexcept
    {
        switch (oper) {
            case RecurComparison::Equal:
                return std::string("==");
            case RecurComparison::LessThan:
                return std::string("<");
            case RecurComparison::LessEqual:
                return std::string("<=");
            case RecurComparison::GreaterThan:
                return std::string(">");
            case RecurComparison::GreaterEqual:
                return std::string(">=");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(oper))+error_notation();
        }
    }

    std::string RecurCppTranslator::logical_operator(const RecurLogical oper) const noexcept
    {
        switch (oper) {
            case RecurLogical::NOT:
                return std::string("!");
            case RecurLogical::AND:
                return std::string("&&");
            case RecurLogical::OR:
                return std::string("||");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(oper))+error_notation();
        }
    }

    std::string RecurCppTranslator::inc_dec_operator(const RecurIncDec oper) const noexcept
    {
        switch (oper) {
            case RecurIncDec::Increment:
                return std::string("++");
            case RecurIncDec::Decrement:
                return std::string("--");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(oper))+error_notation();
        }
    }

    std::string RecurCppTranslator::arithmetic_operator(const RecurArithmetic oper) const noexcept
    {
        switch (oper) {
            case RecurArithmetic::Negation:
                return std::string("-");
            case RecurArithmetic::Addition:
                return std::string("+");
            case RecurArithmetic::Subtraction:
                return std::string("-");
            case RecurArithmetic::Multiplication:
                return std::string("*");
            case RecurArithmetic::Division:
                return std::string("/");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(oper))+error_notation();
        }
    }

    std::string RecurCppTranslator::assignment_operator(const RecurAssignment oper) const noexcept
    {
        switch (oper) {
            case RecurAssignment::BasicAssignment:
                return std::string("=");
            case RecurAssignment::AdditionAssignment:
                return std::string("+=");
            case RecurAssignment::SubtractionAssignment:
                return std::string("-=");
            case RecurAssignment::MultiplicationAssignment:
                return std::string("*=");
            case RecurAssignment::DivisionAssignment:
                return std::string("/=");
            default:
                return error_notation()+std::to_string(tGlueCore::to_integral(oper))+error_notation();
        }
    }

    std::string RecurCppTranslator::scope_resolution_operator() const noexcept
    {
        return std::string("::");
    }

    std::string RecurCppTranslator::make_template(const std::vector<std::string>& parameters,
                                                  const std::string& indent) const noexcept
    {
        std::vector<std::string> tparameters;
        for (auto const& each_param: parameters) {
            tparameters.push_back("typename "+each_param);
        }
        return indent+"template<"+make_parameter_list(tparameters, indent)+">";
    }

    std::string RecurCppTranslator::make_comment(const std::string& comment) const noexcept
    {
        return "/* "+comment+" */";
    }

    std::string RecurCppTranslator::this_pointer() const noexcept
    {
        return std::string("this");
    }

    std::string RecurCppTranslator::make_captures(const std::vector<std::string>& captures) const noexcept
    {
        return "["+make_parameter_list(captures)+"]";
    }

    bool RecurCppTranslator::open_file(const std::string& nameIntegration) noexcept
    {
        m_header_file.open(nameIntegration+".hpp");
        m_header_indent.clear();
        m_pragma_once = true;
        return m_header_file.is_open();
    }

    void RecurCppTranslator::close_file() noexcept
    {
        m_header_file.close();
    }

    void RecurCppTranslator::ifdef_directive(const std::string& identifier) noexcept
    {
        m_header_file << "#ifdef " << identifier << "\n";
    }

    void RecurCppTranslator::endif_directive() noexcept
    {
        m_header_file << "#endif\n";
    }

    std::string RecurCppTranslator::type_boolean() const noexcept
    {
        return std::string("bool");
    }

    std::string RecurCppTranslator::boolean_literal(const bool literal) const noexcept
    {
        return literal ? std::string("true") : std::string("false");
    }

    std::string RecurCppTranslator::type_integer(const bool isUnsigned) const noexcept
    {
        return isUnsigned ? std::string("unsigned int") : std::string("int");
    }

    std::string RecurCppTranslator::type_floating_point(const bool isPointer) const noexcept
    {
        return isPointer ? m_real_type+"*" : m_real_type;
    }

    std::string RecurCppTranslator::type_void() const noexcept
    {
        return std::string("void");
    }

    std::string RecurCppTranslator::type_size() const noexcept
    {
        return std::string("std::size_t");
    }

    std::string RecurCppTranslator::null_pointer() const noexcept
    {
        return std::string("nullptr");
    }

    std::string RecurCppTranslator::type_shared_pointer(const std::string& typeObject) const noexcept
    {
        return "std::shared_ptr<"+typeObject+">";
    }

    std::string RecurCppTranslator::dereference_pointer(const std::string& pointer) const noexcept
    {
        return "*"+pointer;
    }

    std::string RecurCppTranslator::get_rvalue_object(const std::string& object) const noexcept
    {
        return call_function(std::string("std::move"), std::vector<std::string>({object}));
    }

    std::string RecurCppTranslator::string_literal(const std::string characters) const noexcept
    {
        return '"'+characters+'"';
    }

    std::string RecurCppTranslator::type_array(const std::string& typeElements,
                                               const unsigned int sizeArray) const noexcept
    {
        return "std::array<"+typeElements+","+std::to_string(sizeArray)+">";
    }

    std::string RecurCppTranslator::type_valarray(const std::string& typeElements) const noexcept
    {
        return "std::valarray<"+typeElements+">";
    }

    std::string RecurCppTranslator::type_vector(const std::string& typeElements) const noexcept
    {
        return "std::vector<"+typeElements+">";
    }

    std::string RecurCppTranslator::subscript_operation(const std::string& operand,
                                                        const unsigned int position) const noexcept
    {
        return subscript_operation(operand, std::to_string(position));
    }

    std::string RecurCppTranslator::subscript_operation(const std::string& operand,
                                                        const std::string& position) const noexcept
    {
        return operand+"["+position+"]";
    }

    std::string RecurCppTranslator::subscript_operation(const std::string& operand,
                                                        const std::vector<std::string>& position) const noexcept
    {
        std::string str_component;
        /* The last index is more continuous in memory */
        for (auto const& each_index: position) {
            str_component = subscript_operation(str_component, each_index);
        }
        return operand+str_component;
    }

    std::string RecurCppTranslator::vector_constructor(const std::string& typeElements,
                                                       const std::vector<std::string>& elements) const noexcept
    {
        std::string str_indent;
        increase_indent(str_indent);
        return type_vector(typeElements)+"({"+make_parameter_list(elements, str_indent)+"})";
    }

    std::string RecurCppTranslator::vector_constructor(const std::vector<unsigned int>& elements) const noexcept
    {
        std::vector<std::string> str_elements;
        for (auto const& each_element: elements) {
            str_elements.push_back(std::to_string(each_element));
        }
        return vector_constructor(type_integer(true), str_elements);
    }

    std::string RecurCppTranslator::jagged_array_constructor(const std::vector<std::vector<int>>& jagged_array) const noexcept
    {
        std::vector<std::string> str_arrays;
        for (auto const& each_array: jagged_array) {
            std::vector<std::string> str_elements;
            for (auto const& each_element: each_array) {
                str_elements.push_back(std::to_string(each_element));
            }
            str_arrays.push_back(vector_constructor(type_integer(), str_elements));
        }
        return vector_constructor(type_vector(type_integer()), str_arrays);
    }

    std::string RecurCppTranslator::make_declaration(const std::string& typeName,
                                                     const std::string& objectName,
                                                     const bool isStatement,
                                                     const RecurTypeSpecifier declSpecifier,
                                                     const RecurDeclarator declarator) const noexcept
    {
        auto str_declaration = declaration_specifier(declSpecifier)+typeName+get_declarator(declarator);
        if (!objectName.empty()) str_declaration += " "+objectName;
        return isStatement ? str_declaration+end_of_statement() : str_declaration;
    }

    std::string RecurCppTranslator::recur_integration_type() const noexcept
    {
        return "RecurIntegration<"+make_parameter_list(std::vector<std::string>({type_floating_point()}))+">";
    }

    std::string RecurCppTranslator::recur_node_type(const RecurTypeSpecifier declSpecifier) const noexcept
    {
        return type_shared_pointer(declaration_specifier(declSpecifier)+"RecurNode");
    }

    std::string RecurCppTranslator::recur_array_type() const noexcept
    {
        return "RecurArray<"+type_floating_point()+">";
    }

    std::string RecurCppTranslator::recur_relation_type(const unsigned int numRelations) const noexcept
    {
        return type_array(recur_array_type(), numRelations);
    }

    void RecurCppTranslator::recur_array_begin_loop(const unsigned int position) noexcept
    {
        call_function_statement(recur_array_object(position),
                                std::string("begin_loop"),
                                std::vector<std::string>(),
                                recur_array_access());
    }

    std::string RecurCppTranslator::recur_array_node_left(const unsigned int position) const noexcept
    {
        return call_function(recur_array_object(position),
                             std::string("node_left"),
                             std::vector<std::string>(),
                             recur_array_access());
    }

    std::string RecurCppTranslator::recur_array_current_node(const unsigned int position) const noexcept
    {
        return call_function(recur_array_object(position),
                             std::string("current_node"),
                             std::vector<std::string>(),
                             recur_array_access());
    }

    void RecurCppTranslator::recur_array_next_node(const unsigned int position) noexcept
    {
        call_function_statement(recur_array_object(position),
                                std::string("next_node"),
                                std::vector<std::string>(),
                                recur_array_access());
    }

    std::string RecurCppTranslator::recur_array_get_input(const unsigned int position,
                                                          const std::string& outputIndex) const noexcept
    {
        return call_function(recur_array_object(position),
                             std::string("get_input_nodes"),
                             std::vector<std::string>({outputIndex}),
                             recur_array_access());
    }

    std::string RecurCppTranslator::recur_array_input_type() const noexcept
    {
        return type_vector(type_vector(recur_node_type(RecurTypeSpecifier::None)));
    }

    std::string RecurCppTranslator::recur_buffer_type() const noexcept
    {
        return type_shared_pointer("RecurBuffer<"+type_floating_point()+">");
    }

    std::string RecurCppTranslator::recur_buffer_get() const noexcept
    {
        return call_function(recur_buffer_object(),
                             std::string("get_element"),
                             std::vector<std::string>(),
                             recur_buffer_access());
    }

    std::string RecurCppTranslator::recur_index_type() const noexcept
    {
        return type_shared_pointer(std::string("RecurIndex"));
    }

    std::string RecurCppTranslator::recur_index_vector() const noexcept
    {
        return type_vector(recur_index_type());
    }

    std::string RecurCppTranslator::lhs_node_type() const noexcept
    {
        return recur_node_type(RecurTypeSpecifier::Const);
    }

    std::string RecurCppTranslator::lhs_get_offset() const noexcept
    {
        return call_function(lhs_node_object(),
                             std::string("get_offset"),
                             std::vector<std::string>(),
                             lhs_node_access());
    }

    std::string RecurCppTranslator::lhs_get_order(const std::string& indices,
                                                  const unsigned int position) const noexcept
    {
        return call_function(lhs_node_object(),
                             std::string("get_order"),
                             std::vector<std::string>({subscript_operation(indices, position)}),
                             lhs_node_access());
    }

    std::string RecurCppTranslator::lhs_get_rhs() const noexcept
    {
        return call_function(lhs_node_object(),
                             std::string("get_rhs"),
                             std::vector<std::string>(),
                             lhs_node_access());
    }

    std::string RecurCppTranslator::rhs_nodes_type() const noexcept
    {
        return type_vector(recur_node_type(RecurTypeSpecifier::Const));
    }

    std::string RecurCppTranslator::rhs_nodes_size() const noexcept
    {
        return call_function(rhs_nodes_object(),
                             std::string("size"),
                             std::vector<std::string>(),
                             rhs_nodes_access());
    }

    std::string RecurCppTranslator::rhs_get_offset(const std::string& position) const noexcept
    {
        return call_function(rhs_nodes_at(position),
                             std::string("get_offset"),
                             std::vector<std::string>(),
                             rhs_node_access());
    }

    void RecurCppTranslator::inc_dec_statement(const RecurIncDec oper, const std::string& var) noexcept
    {
        m_header_file << m_header_indent << inc_dec_operation(oper, var) << end_of_statement();
    }

    std::string RecurCppTranslator::parenthesize_expression(const std::string& expression) const noexcept
    {
        return "("+expression+")";
    }

    void RecurCppTranslator::assignment_statement(const std::string& lhs,
                                                  const RecurAssignment oper,
                                                  const std::string& rhs,
                                                  const bool rvalueRHS) noexcept
    {
        if (rvalueRHS) {
            m_header_file << m_header_indent
                          << assignment_operation(
                                 lhs,
                                 oper,
                                 call_function(std::string("std::move"), std::vector<std::string>({rhs}))
                             )
                          << end_of_statement();
        }
        else {
            m_header_file << m_header_indent
                          << assignment_operation(lhs, oper, rhs)
                          << end_of_statement();
        }
    }

    void RecurCppTranslator::write_declaration_assignment(const std::string& typeName,
                                                          const std::string& objectName,
                                                          const RecurAssignment oper,
                                                          const std::string& rhs,
                                                          const RecurTypeSpecifier declSpecifier,
                                                          const RecurDeclarator declarator,
                                                          const bool rvalueRHS) noexcept
    {
        assignment_statement(make_declaration(typeName, objectName, false, declSpecifier, declarator),
                             oper,
                             rhs,
                             rvalueRHS);
    }

    void RecurCppTranslator::write_comment(const std::string& comment) noexcept
    {
        m_header_file << m_header_indent << make_comment(comment) << "\n";
    }

    void RecurCppTranslator::include_header_files(const std::vector<std::string>& headerFiles,
                                                  const bool systemHeaders) noexcept
    {
        m_header_file << "\n";
        if (m_pragma_once) {
            m_header_file << "#pragma once\n\n";
            m_pragma_once = false;
        }
        if (systemHeaders) {
            for (auto const& each_header: headerFiles) {
                m_header_file << "#include <" << each_header << ">\n";
            }
        }
        else {
            for (auto const& each_header: headerFiles) {
                m_header_file << "#include \"" << each_header << ".hpp\"\n";
            }
        }
    }

    void RecurCppTranslator::begin_namespace(const std::string& scopeName,
                                             const std::string& description,
                                             const std::vector<std::string>& typeNames,
                                             const bool doQuadrature) noexcept
    {
        m_header_file << m_header_indent << make_comment(description) << "\n";
        /* Include system headers */
        include_header_files(std::vector<std::string>({std::string("array"),
                                                       std::string("cmath"),
                                                       std::string("memory"),
                                                       std::string("utility"),
                                                       std::string("valarray"),
                                                       std::string("vector")}),
                             true);
        if (doQuadrature) include_header_files(std::vector<std::string>({std::string("functional")}), true);
        /* Include headers from tGlueCore, tIntegral libraries */
        include_header_files(
            std::vector<std::string>({std::string("tGlueCore/Logger"),
                                      std::string("tIntegral/Settings"),
                                      std::string("tIntegral/Recurrence/RecurBuffer"),
                                      std::string("tIntegral/Recurrence/RecurIndex"),
                                      std::string("tIntegral/Recurrence/RecurNode"),
                                      std::string("tIntegral/Recurrence/RecurArray"),
                                      std::string("tIntegral/Recurrence/RecurIntegration")})
        );
        /* Include headers of different types */
        if (typeNames.size()>0) {
            std::vector<std::string> type_headers;
            /* Replace all :: by /, and add tIntegral/ if needed */
            for (auto const& each_type: typeNames) {
                auto each_header =  std::regex_replace(each_type, std::regex("::"), "/");
                if (each_header.find("/")==std::string::npos) {
                    /* Ignore some known basic types, indeed those used as template parameters */
                    if (each_header.compare(type_boolean())!=0 &&
                        each_header.compare(type_integer())!=0 &&
                        each_header.compare(type_integer(true))!=0 &&
                        each_header.compare(type_floating_point())!=0) {
                        type_headers.push_back("tIntegral/"+each_header);
                    }
                }
                else {
                    type_headers.push_back(each_header);
                }
            }
            include_header_files(type_headers);
        }
        m_header_file << "\n" << m_header_indent << "namespace " << scopeName << "\n{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_namespace() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    std::string RecurCppTranslator::namespace_member(const std::string& scopeName,
                                                     const std::string& memberName) const noexcept
    {
        return scopeName+scope_resolution_operator()+memberName;
    }

    void RecurCppTranslator::begin_class(const std::string& className,
                                         const std::vector<std::string>& tparameters,
                                         const std::vector<std::tuple<std::string,std::string,std::string,bool>>& classMembers,
                                         const std::string& description,
                                         const std::string& baseClass) noexcept
    {
        m_header_file << m_header_indent << make_comment(description) << "\n";
        if (tparameters.size()>0) {
            m_header_file << make_template(tparameters, m_header_indent) << "\n";
        }
        if (baseClass.empty()) {
            m_header_file << m_header_indent << "class " << className << " final\n";
        }
        else {
            m_header_file << m_header_indent << "class " << className << " final: public " << baseClass << "\n";
        }
        m_header_file << m_header_indent << "{\n";
        increase_indent(m_header_indent);
        /* Class member declaration */
        begin_private_section();
        std::vector<std::string> constructor_args;
        std::vector<std::string> constructor_init;
        for (auto const& member: classMembers) {
            m_header_file << m_header_indent
                          << std::get<0>(member) << " " << std::get<1>(member) << end_of_statement();
            /* Got a constructor argument or a initialization value */
            if (!std::get<2>(member).empty()) {
                if (std::get<3>(member)) {
                    constructor_args.push_back(std::get<0>(member)+" "+std::get<2>(member));
                }
                constructor_init.push_back(std::get<1>(member)+"("+std::get<2>(member)+")");
            }
        }
        end_class_section();
        begin_public_section();
        /* Write the constructor initialization */
        m_header_file << call_function("explicit "+className, constructor_args, m_header_indent);
        if (constructor_init.empty()) {
            m_header_file << " noexcept = default" << end_of_statement();
        }
        else {
            m_header_file << " noexcept:";
            increase_indent(m_header_indent);
            m_header_file << make_parameter_list(constructor_init, m_header_indent, true) << " {}\n";
            decrease_indent(m_header_indent);
        }
        /* Class destructor */
        m_header_file << call_function("~"+className, std::vector<std::string>(), m_header_indent)
                      << " noexcept = default"
                      << end_of_statement();
    }

    void RecurCppTranslator::end_class() noexcept
    {
        end_class_section();
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "};\n";
    }

    std::string RecurCppTranslator::class_member(const std::string& className,
                                                 const std::string& memberName) const noexcept
    {
        return className+scope_resolution_operator()+memberName;
    }

    void RecurCppTranslator::begin_public_section() noexcept
    {
        end_class_section();
        m_header_file << m_header_indent << "public:\n";
        increase_indent(m_header_indent);
        m_section_on = true;
    }

    void RecurCppTranslator::begin_protected_section() noexcept
    {
        end_class_section();
        m_header_file << m_header_indent << "protected:\n";
        increase_indent(m_header_indent);
        m_section_on = true;
    }

    void RecurCppTranslator::begin_private_section() noexcept
    {
        end_class_section();
        m_header_file << m_header_indent << "private:\n";
        increase_indent(m_header_indent);
        m_section_on = true;
    }

    void RecurCppTranslator::end_class_section() noexcept
    {
        if (m_section_on) decrease_indent(m_header_indent);
        m_section_on = false;
    }

    void RecurCppTranslator::declare_function(const std::string& functionName,
                                              const std::string& returnType,
                                              const std::vector<std::string>& parameters,
                                              const std::string& description,
                                              const RecurFunctionSpecifier specifier) noexcept
    {
        /* Write the description and the function declaration in the header file */
        m_header_file << m_header_indent << make_comment(description) << "\n"
                      << call_function(returnType+" "+functionName, parameters, m_header_indent);
        if (specifier==RecurFunctionSpecifier::OverrideFunction) {
            m_header_file << " noexcept override" << end_of_statement();
        }
        else {
            m_header_file << " noexcept" << end_of_statement();
        }
    }

    void RecurCppTranslator::begin_function(const std::string& functionName,
                                            const std::string& returnType,
                                            const std::vector<std::string>& parameters,
                                            const std::string& description,
                                            const RecurFunctionSpecifier specifier) noexcept
    {
        /* Write the description and the function declaration in the header file */
        m_header_file << m_header_indent << make_comment(description) << "\n"
                      << call_function(returnType+" "+functionName, parameters, m_header_indent);
        if (specifier==RecurFunctionSpecifier::OverrideFunction) {
            m_header_file << " noexcept override\n";
        }
        else {
            m_header_file << " noexcept\n";
        }
        m_header_file << m_header_indent << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_function() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    std::string RecurCppTranslator::call_function(const std::string& functionName,
                                                  const std::vector<std::string>& parameters,
                                                  const std::string& indent,
                                                  const bool isTemplate) const noexcept
    {
        std::string str_indent = indent;
        increase_indent(str_indent);
        return isTemplate ? indent+"template "+functionName+"("+make_parameter_list(parameters, str_indent)+")"
            : indent+functionName+"("+make_parameter_list(parameters, str_indent)+")";
    }

    std::string RecurCppTranslator::call_function(const std::string& operandName,
                                                  const std::string& functionName,
                                                  const std::vector<std::string>& parameters,
                                                  const RecurOperandType operandType,
                                                  const std::string& indent,
                                                  const bool isTemplate) const noexcept
    {
        auto function_name = isTemplate ? "template "+functionName : functionName;
        std::string str_function;
        switch (operandType) {
            case RecurOperandType::ClassOperand:
                str_function = class_member(operandName, function_name);
                break;
            case RecurOperandType::ObjectOperand:
                str_function = operandName+member_of_object()+function_name;
                break;
            case RecurOperandType::PointerOperand:
                str_function = operandName+member_of_pointer()+function_name;
                break;
            default:
                str_function = operandName
                             + error_notation()
                             + std::to_string(tGlueCore::to_integral(operandType))
                             + error_notation()
                             + function_name;
        }
        return call_function(str_function, parameters, indent);
    }

    void RecurCppTranslator::call_function_statement(const std::string& functionName,
                                                     const std::vector<std::string>& parameters,
                                                     const bool isTemplate) noexcept
    {
        m_header_file << call_function(functionName, parameters, m_header_indent, isTemplate)
                      << end_of_statement();
    }

    void RecurCppTranslator::call_function_statement(const std::string& operandName,
                                                     const std::string& functionName,
                                                     const std::vector<std::string>& parameters,
                                                     const RecurOperandType operandType,
                                                     const bool isTemplate) noexcept
    {
        m_header_file << call_function(operandName, functionName, parameters, operandType, m_header_indent,isTemplate)
                      << end_of_statement();
    }

    void RecurCppTranslator::return_statement(const std::string& expression) noexcept
    {
        m_header_file << m_header_indent << "return " << expression << end_of_statement();
    }

    void RecurCppTranslator::handle_error(const std::vector<std::string>& message) noexcept
    {
        if (!message.empty()) {
            auto parameters = message;
            parameters.insert(
                parameters.begin(),
                class_member(namespace_member(std::string("tGlueCore"), std::string("MessageType")),
                             std::string("Error"))
            );
            call_function_statement(namespace_member(std::string("Settings"), std::string("logger")),
                                    std::string("write"),
                                    parameters,
                                    RecurOperandType::PointerOperand);
        }
        return_statement(boolean_literal(false));
    }

    void RecurCppTranslator::handle_debug(const std::vector<std::string>& message) noexcept
    {
        auto parameters = message;
        parameters.insert(
            parameters.begin(),
            class_member(namespace_member(std::string("tGlueCore"), std::string("MessageType")),
                         std::string("Debug"))
        );
        if (parameters.size()==1) parameters.push_back(string_literal(std::string("debugging ...")));
        call_function_statement(namespace_member(std::string("Settings"), std::string("logger")),
                                std::string("write"),
                                parameters,
                                RecurOperandType::PointerOperand);
    }

    void RecurCppTranslator::begin_if_statement(const std::string& condition, const bool elifStatement) noexcept
    {
        auto str_if_statement = elifStatement ? m_header_indent+"else if (" : m_header_indent+"if (";
        m_header_file << str_if_statement
                      << std::regex_replace(condition,
                                            std::regex("\n"),
                                            "\n"+std::string(str_if_statement.size(), ' '))
                      << ")\n"
                      << m_header_indent
                      << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::begin_else_statement() noexcept
    {
        m_header_file << m_header_indent << "else\n" << m_header_indent << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_elif_statement() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    //FIXME: use make_parameter_list()
    void RecurCppTranslator::begin_for_loop(const std::string& typeInitialization,
                                            const std::vector<std::pair<std::string,std::string>>& varInitialization,
                                            const std::string& condition,
                                            const std::vector<std::string>& iteration) noexcept
    {
        if (typeInitialization.empty()) {
            m_header_file << m_header_indent << "for (";
        }
        else {
            m_header_file << m_header_indent << "for (" << typeInitialization << " ";
        }
        for (unsigned int ivar=0; ivar<varInitialization.size(); ++ivar) {
            if (ivar!=0) m_header_file << ",";
            m_header_file << assignment_operation(varInitialization[ivar].first,
                                                  RecurAssignment::BasicAssignment,
                                                  varInitialization[ivar].second);
        }
        m_header_file << "; " << condition << "; ";
        for (unsigned int iter=0; iter<iteration.size(); ++iter) {
            if (iter!=0) m_header_file << ",";
            m_header_file << iteration[iter];
        }
        m_header_file << ")\n" << m_header_indent << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_for_loop() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    void RecurCppTranslator::begin_while_loop(const std::string& condition) noexcept
    {
        m_header_file << m_header_indent << "while (" << condition << ")\n" << m_header_indent << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_while_loop() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    void RecurCppTranslator::switch_index_order(const std::string& idxName) noexcept
    {
        m_header_file << m_header_indent << "switch (" << index_order(idxName) << ")\n" << m_header_indent << "{\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_switch() noexcept
    {
        decrease_indent(m_header_indent);
        m_header_file << m_header_indent << "}\n";
    }

    void RecurCppTranslator::case_index_order(const int order) noexcept
    {
        m_header_file << m_header_indent << "case " << order << ":\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::case_default() noexcept
    {
        m_header_file << m_header_indent << "default:\n";
        increase_indent(m_header_indent);
    }

    void RecurCppTranslator::end_case() noexcept
    {
        m_header_file << m_header_indent << "break" << end_of_statement();
        decrease_indent(m_header_indent);
    }

    std::string RecurCppTranslator::type_quadrature() const noexcept
    {
        return "std::function<"+type_boolean()+"("
            + make_parameter_list(std::vector<std::string>({
                  make_declaration(recur_array_type(),
                                   std::string(),
                                   false,
                                   RecurTypeSpecifier::None,
                                   RecurDeclarator::LvalueReference),
                  "std::function<"+type_void()+"("
                  + make_declaration(type_floating_point(),
                                     std::string(),
                                     false,
                                     RecurTypeSpecifier::Const)
                  + ")>"
              }))
            + ")>";
    }

    void RecurCppTranslator::make_quadrature_call(const std::string quadrature, const std::string integrand) noexcept
    {
        return_statement(
            call_function(quadrature,
                          std::vector<std::string>({
                              recur_array_object(0),
                              make_captures(std::vector<std::string>({this_pointer()}))
                              + "("+make_declaration(type_floating_point(), std::string("abscissa"))+")"
                              + "{ "
                              + call_function(this_pointer(),
                                              integrand,
                                              std::vector<std::string>({std::string("abscissa")}),
                                              RecurOperandType::PointerOperand)
                              + end_of_statement(false)
                              + " }"
                          }))
        );
    }
}
